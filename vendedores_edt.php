<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

$ls_asignados = '';

if (isset($_GET['vendedor']) && $_GET['vendedor'] != '') {
	require("libs/conexion.php");

	$vendedores = $db
		->where('Id_v', $_GET['vendedor'])
		->objectBuilder()->get('vendedores');

	if ($db->count > 0) {
		$rsc = $vendedores[0];
		$nombre    = $rsc->nombre_v;
		$direccion = $rsc->direccion_v;
		$telefono  = $rsc->telefono_v;
		$correo   = $rsc->correo_v;

		$asignados = $db
			->where('vendedor_vc', $rsc->Id_v)
			->objectBuilder()->get('vendedores_cursos');

		if ($db->count > 0) {
			foreach ($asignados as $rasg) {
				$ls_cursos = '';

				$cursos = $db
					->where('activo_ct', 1)
					->orderBy('nombre', 'ASC')
					->objectBuilder()->get('certificaciones');

				foreach ($cursos as $rcu) {
					$ls_cursos .= '<option value="' . $rcu->Id_ct . '" ' . ($rcu->Id_ct == $rasg->curso_vc ? "selected" : "") . ' data-competencia="' . $rcu->competencia_ct . '">' . $rcu->nombre . '</option>';
				}

				$ls_competencias = '';

				$competencias = $db
					->where('activo_cp', 1)
					->orderBy('nombre_cp', 'ASC')
					->objectBuilder()->get('competencias');

				foreach ($competencias as $rcp) {
					$ls_competencias .= '<option value="' . $rcp->alias_cp . '" ' . ($rcp->alias_cp == $rasg->competencia_vc ? "selected" : "") . '>' . $rcp->nombre_cp . '</option>';
				}

				$ls_asignados .= '<div class="temp"> <hr>
										<a class="quitar">Eliminar</a>
										<div class="Registro-der">
											<label>Competencia *</label>
											<select name="vendedor[competencia][]" class="competencia" required>
												<option value="">Seleccione</option>
												' . $ls_competencias . '
											</select>
											<label>Precio *</label>
											<input type="text" placeholder="Precio" name="vendedor[precio][]" value="' . $rasg->precio_vc . '" required>
										</div>
										<div class="Registro-der">
											<label>Curso *</label>
											<select name="vendedor[curso][]" class="Sel-cursos cursos_ad" required>
												<option value="">Seleccione</option>
												' . $ls_cursos . '
											</select>
										</div>
									</div>';
			}
		}
	} else {
		header('Location: vendedores');
	}
} else {
	header('Location: vendedores');
}

$ls_cursos = '';

$cursos = $db
	->where('activo_ct', 1)
	->orderBy('nombre', 'ASC')
	->objectBuilder()->get('certificaciones');

foreach ($cursos as $rcu) {
	$ls_cursos .= '<option value="' . $rcu->Id_ct . '" data-competencia="' . $rcu->competencia_ct . '" style="display:none">' . $rcu->nombre . '</option>';
}

$ls_competencias = '';

$competencias = $db
	->where('activo_cp', 1)
	->orderBy('nombre_cp', 'ASC')
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$ls_competencias .= '<option value="' . $rcp->alias_cp . '">' . $rcp->nombre_cp . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Entrenadores | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Editar Vendedor</h2>
				<form id="editar-vendedor">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombre" name="vendedor[nombre]" value="<?php echo $nombre ?>" required>
							<label>Dirección *</label>
							<input type="text" placeholder="Dirección" name="vendedor[direccion]" value="<?php echo $direccion ?>" required>
						</div>
						<div class="Registro-izq">
							<label>Teléfono *</label>
							<input type="text" placeholder="Teléfono" name="vendedor[telefono]" value="<?php echo $telefono ?>" required>
							<label>Correo Electrónico *</label>
							<input type="text" placeholder="Correo Electrónico" name="vendedor[correo]" value="<?php echo $correo ?>" required>
						</div>
						<br>
						<div class="cursos_adicionales">
							<?php echo $ls_asignados ?>
						</div>
						<br>
						<br>
						<div class="Registro-cent">
							<button type="button" class="placa" id="agr_curso">Adicionar Curso</button>
						</div>
						<br>
						<br>
						<input type="hidden" name="vendedor[idvendedor]" value="<?php echo $_GET['vendedor'] ?>">
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
		</div>
		<div style="display: none;">
			<select class="ls_cursos">
				<?php echo $ls_cursos ?>
			</select>
			<select class="ls_competencias">
				<?php echo $ls_competencias ?>
			</select>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/vendedores_edt.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
