<div class="Contenido-admin-izq listado-eliminados" style="display: none">
	<h2>Usuarios Eliminados</h2>
	<hr>
	<p></p>
	<br>
	<div class="Listar-personas">
		<div class="Tabla-listar">
			<table>
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Correo</th>
					</tr>
				</thead>
				<tbody id="resultados_eliminados">
				</tbody>
			</table>
		</div>
	</div>
	<div id="lista_loader" class="lista_loader">
		<div class="loader2">Cargando...</div>
	</div>
	<div id="paginacion" class="paginacion_eliminados"></div>
</div>
