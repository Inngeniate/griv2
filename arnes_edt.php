<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
else {
	require("libs/conexion.php");

	$iden = $_GET['certificado'];

	$ensayo = $db
		->where('Id_ca', $iden)
		->objectBuilder()->get('certificado_arnes');

	$res = $ensayo[0];
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
		}

		.img-prev {
			width: 450px;
			height: 100px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.img-prev2 {
			width: 320px;
			height: 220px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.img-prev-sg {
			width: 330px;
			height: 210px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		.ui-autocomplete {
			max-height: 200px;
			overflow-y: auto;
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
			/* add padding to account for vertical scrollbar */
			z-index: 1000 !important;
		}

		table input[type="radio"] {
			height: auto !important;
			width: auto !important;
		}

		table td {
			text-align: left;
		}

		table th {
			background: #004d80;
			font-family: Open-sans-b;
			font-size: 13px;
			text-align: center;
			color: #fff;
			padding: 0.5em 0.3em 0.5em 0.3em;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="seguridad">
				<div class="Contenido-admin-izq">
					<h2>Editar Certificados</h2>
					<form id="ed_certificado">
						<div class="Registro">
							<hr>
							<label>Datos Generales</label>
							<div class="Registro-der">
								<input type="hidden" name="certifica[tipo]" class="tipocer" value="1">
								<label>Propietario</label>
								<input type="text" placeholder="Propietario" name="certifica[propietario]" value="<?php echo $res->propietario ?>" required>
								<label>Dirección</label>
								<input type="text" placeholder="Dirección" name="certifica[direccion]" value="<?php echo $res->direccion ?>" required>
								<label>Teléfono</label>
								<input type="text" placeholder="Teléfono" name="certifica[telefono]" value="<?php echo $res->telefono ?>" required>
							</div>
							<div class="Registro-der">
								<label>Ubicación</label>
								<input type="text" placeholder="Ubicación" name="certifica[ubicacion]" class="auto_ciu" value="<?php echo $res->ubicacion ?>" required>
								<label>Área de trabajo</label>
								<input type="text" placeholder="Área de trabajo" name="certifica[atrabajo]" value="<?php echo $res->atrabajo ?>" required>
								<label>Nombre Contacto </label>
								<input type="text" placeholder="Nombre contacto" name="certifica[contacto]" value="<?php echo $res->contacto ?>">
							</div>
							<hr>
							<label>Descripción del equipo</label>
							<div class="Registro-der">
								<label>Nombre</label>
								<input type="text" placeholder="Nombre" name="certifica[equipo]" value="<?php echo $res->equipo ?>">
							</div>
							<div class="Registro-der">
								<label>Marca</label>
								<input type="text" placeholder="Marca" name="certifica[marca]" value="<?php echo $res->marca ?>">
							</div>
							<hr>
							<label>Identificación del equipo</label>
							<div class="Registro-der">
								<label>Serie</label>
								<input type="text" placeholder="Serie" name="certifica[serie]" value="<?php echo $res->serie ?>">
								<label>Modelo</label>
								<input type="text" placeholder="Modelo" name="certifica[modelo]" value="<?php echo $res->modelo ?>" required>
								<label>N° Inspección</label>
								<input type="text" placeholder="N° Inspección" name="certifica[ninspeccion]" value="<?php echo $res->ninspeccion ?>" required>
								<label>FF</label>
								<input type="date" placeholder="FF" name="certifica[ff]" value="<?php echo $res->ffabricacion ?>" required>
								<label>Primer Uso</label>
								<input type="text" placeholder="Primer Uso" name="certifica[puso]" value="<?php echo $res->pri_uso ?>" required>
							</div>
							<div class="Registro-der">
								<label>Referencia</label>
								<input type="text" placeholder="Referencia" name="certifica[referencia]" value="<?php echo $res->referencia ?>" required>
								<label>Lote</label>
								<input type="text" placeholder="Lote" name="certifica[lote]" value="<?php echo $res->lote ?>" required>
								<label>Talla</label>
								<input type="text" placeholder="Talla" name="certifica[talla]" value="<?php echo $res->talla ?>" required>
								<label>Fecha Compra</label>
								<input type="date" placeholder="Fecha Compra" name="certifica[fcompra]" value="<?php echo $res->fcompra ?>" required>
							</div>
							<hr>
							<label>VERIFICACIÓN VISUAL DE LOS COMPONENTES</label>
							<div class="Registro-der">
								<label>Estado general de las etiquetas</label>
								<select name="certifica[visual][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->visual1 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
								<label>Estado general de las reatas</label>
								<select name="certifica[visual][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->visual2 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
								<label>Estado general de los testigos de impacto</label>
								<select name="certifica[visual][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->visual3 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
								<label>Estado general de las costuras</label>
								<select name="certifica[visual][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->visual4 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
							</div>
							<div class="Registro-der">
								<label>Estado general de las argollas en “d”</label>
								<select name="certifica[visual][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->visual5 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
								<label>Estado general de los pasadores y hebillas de ajuste</label>
								<select name="certifica[visual][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->visual6 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
								<label>Estado general del pad plastico</label>
								<select name="certifica[visual][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->visual7 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
							</div>
							<hr>
							<label>VERIFICACIÓN FUNCIONAL DE LOS COMPONENTES</label>
							<div class="Registro-der">
								<label>Las reatas se deslizan facilmente por las hebillas de ajuste</label>
								<select name="certifica[funcional][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->funcional1 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
								<label>Ergonomía y maleabilidad de las reatas</label>
								<select name="certifica[funcional][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->funcional2 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
							</div>
							<div class="Registro-der">
								<label>Los pasadores cierran y se ajustan correctamente</label>
								<select name="certifica[funcional][]">
									<?php
									$sel_op = array('C', 'NC', 'RM', 'NA');
									$opciones = '';
									foreach ($sel_op as $key => $value) {
										$opciones .= '<option value="' . $value . '" ' . ($res->funcional3 == $value ? 'selected' : '') . '>' . $value . '</option>';
									}
									echo $opciones;
									?>
								</select>
							</div>
							<hr>
							<label>Observaciones </label>
							<textarea name="certifica[observaciones]"><?php echo $res->observaciones ?></textarea>
							<hr>
							<label>Concepto de inspección</label>
							<div class="Registro-der">
								<label>Aprobado</label>
								<select name="certifica[aprobado]" required>
									<?php
									($res->aprobado == 1 ? $aprobado = '<option value="1" selected>SI</option><option value="0">NO</option>' : $aprobado =  '<option value="1">SI</option><option value="0" selected>NO</option>');
									echo $aprobado;
									?>
								</select>
							</div>
							<div class="Registro-der">
								<label>Código de aprobacion gri</label>
								<input type="text" placeholder="Código de aprobacion gri" name="certifica[codaprobacion]" value="<?php echo $res->codaprobacion ?>">
							</div>
							<hr>
							<label>Control de fechas de inspección</label>
							<div class="Registro-der">
								<label>Control 1</label>
								<input type="date" placeholder="Control 1" name="certifica[control1]" value="<?php echo $res->control1 ?>">
							</div>
							<div class="Registro-der">
								<label>Control 2</label>
								<input type="date" placeholder="Control 2" name="certifica[control2]" value="<?php echo $res->control2 ?>">
							</div>
							<div class="Registro-der">
								<label>Control 3</label>
								<input type="date" placeholder="Control 3" name="certifica[control3]" value="<?php echo $res->control3 ?>">
							</div>
							<div class="Registro-der">
							</div>
							<br>
							<br>
							<hr>
							<label>Registro Fotográfico</label>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev-sg" src="<?php echo substr($res->etiqueta, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto1">ETIQUETA</button>
								</div>
							</div>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev-sg" src="<?php echo substr($res->estadoequipo, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto2">EST. GENERAL EQUIPO</button>
								</div>
							</div>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev-sg" src="<?php echo substr($res->padplastico, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto3">PAD PLASTICO</button>
								</div>
							</div>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev-sg" src="<?php echo substr($res->inspeccioncert, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto4">INSPEC. CERTIFICACIÓN</button>
								</div>
							</div>
							<input type="hidden" name="certifica[tipo]" value="1">
							<input type="hidden" name="certifica[id]" value="<?php echo $iden ?>">
							<input type="submit" value="Guardar Certificado">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/arnes_edt.js"></script>
</body>

</html>
