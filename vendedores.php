<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");

$ls_cursos = '';

$cursos = $db
	->where('activo_ct', 1)
	->orderBy('nombre', 'ASC')
	->objectBuilder()->get('certificaciones');

foreach ($cursos as $rcu) {
	$ls_cursos .= '<option value="' . $rcu->Id_ct . '" data-competencia="' . $rcu->competencia_ct . '" style="display:none">' . $rcu->nombre . '</option>';
}


$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 11)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}

$ls_competencias = '';

$competencias = $db
	->where('activo_cp', 1)
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$ls_competencias .= '<option value="' . $rcp->alias_cp . '">' . $rcp->nombre_cp . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Vendedores | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<style>
		.quitar {
			cursor: pointer;
			float: right;
		}
	</style>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<?php
			//if ($_SESSION['usutipoggri'] == 'administrador') {
			?>
			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="listar" class="activo">Listar Vendedores</a></li>
						<li><a href="javascript://" id="crear">Crear Vendedores</a></li>
					</ul>
				</div>
			</div>
			<div class="Contenido-admin-izq listado-vendedores">
				<h2>Listar Vendedores</h2>
				<hr>
				<p>En esta sección podrás listar y buscar vendedores.</p>
				<br>
				<form id="buscar">
					<label>Nombre: </label>
					<input type="text" name="nom" placeholder="Nombre">
					<input type="submit" value="Buscar">
				</form>
				<br>
				<div class="Listar-personas">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Dirección</th>
									<th>Teléfono</th>
									<th>Correo</th>
									<th colspan="2">Cursos</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody id="resultados">
							</tbody>
						</table>
					</div>
				</div>
				<div id="lista_loader">
					<div class="loader2">Cargando...</div>
				</div>
				<div id="paginacion"></div>
			</div>
			<div class="Contenido-admin-izq crear-vendedores" style="display: none">
				<h2>Crear Vendedor</h2>
				<form id="nuevo-vendedor">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombre" name="vendedor[nombre]" required>
							<label>Dirección *</label>
							<input type="text" placeholder="Dirección" name="vendedor[direccion]" required>
						</div>
						<div class="Registro-izq">
							<label>Teléfono *</label>
							<input type="text" placeholder="Teléfono" name="vendedor[telefono]" required>
							<label>Correo Electrónico *</label>
							<input type="text" placeholder="Correo Electrónico" name="vendedor[correo]" required>
						</div>
						<br>
						<br>
						<div class="cursos_adicionales"></div>
						<br>
						<br>
						<div class="Registro-cent">
							<button type="button" class="placa" id="agr_curso">Adicionar Curso</button>
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
			<?php
			// } else {
			// 	echo '<p>No tiene permiso para acceder a esta sección.</p>';
			// }
			?>
		</div>
		<div style="display: none;">
			<select class="ls_cursos">
				<?php echo $ls_cursos ?>
			</select>
			<select class="ls_competencias">
				<?php echo $ls_competencias ?>
			</select>
		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/vendedores.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
