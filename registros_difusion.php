<?php
require "libs/conexion.php";
@$informacion = array();
$data = $_REQUEST['registro'];

@$carpeta_firmas = '../alturas';

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

$inicio = $data['inicio'];
$fin    = $data['final'];

$registros = $db
	->where('fecha_al', array($inicio, $fin), 'BETWEEN')
	->objectBuilder()->get('registros_alturas');

if ($db->count > 0) {
	require_once 'libs/tcpdf.php';

	class MYPDF extends TCPDF
	{
		public function Header()
		{

			$estilo = '<style>
						table{
							font-size: 11px;
							font-family: arial;
						}
						.tmax{
							font-size: 12px;
						}
						.tmin2{
							font-size: 11px;
						}
					</style>';

			$bMargin = $this->getBreakMargin();
			$auto_page_break = $this->AutoPageBreak;
			$this->SetAutoPageBreak(false, 0);
			$this->SetAutoPageBreak($auto_page_break, $bMargin);
			$this->setPageMark();

			$head = '<table cellspacing="0" cellpadding="0" border="0" class="certificado1">
								<tr>
									<td height="20"><br><br><img src="images/logo.png" width="120px"><br></td>
								</tr>
								<tr>
									<td class="tmax"><b>FORMATO DE DIFUSION Y ACEPTACION DEL CODIGO DE BUEN COMPORTAMIENTO</b></td>
								</tr>
							</table>
							<table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="99%">
								<tr>
									<td colspan="3"></td>
									<td class="tmax" align="right"><br><br>FED 013 <br></td>
								</tr>
							</table>';

			$this->writeHTML($estilo . $head, true, false, false, false, '');
		}
	}

	// create new PDF document
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
	$pdf->setPageOrientation('l');

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// PDF_MARGIN_TOP
	$pdf->SetMargins(15, 45, 15);
	// $pdf->SetPrintHeader(false);
	// $pdf->setPrintFooter(false);

	// set auto page breaks
	// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->SetAutoPageBreak(true, 0);

	// set image scale factor
	// $pdf->setImageScale(1.53);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
		require_once dirname(__FILE__) . '/lang/eng.php';
		$pdf->setLanguageArray($l);
	}

	$pdf->AddPage();
	$arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 11);

	$estilo = '<style>
					table{
						font-size: 11px;
						font-family: arial;
					}
					.tmax{
						font-size: 12px;
					}
					.tmin2{
						font-size: 11px;
					}
					.gris{
						background-color: #dadada;
					}
				</style>';

	$html = '<table cellspacing="0" cellpadding="5" border="1" class="certificado1" width="99%">
				<tr>
					<td colspan="4" class="gris" align="center">FORMATO DE DIFUSIÓN Y ACEPTACIÓN</td>
				</tr>
				<tr>
					<td class="gris" align="center">OBJETIVO</td>
					<td colspan="3">Socializar y estar a conformidad con el código de buen comportamiento impartido en el centro de formación teórico / practico, si está de acuerdo (favor proceda a firmar el formato)</td>
				</tr>
				<tr>
					<td class="gris" align="center" width="300">SUPERVISOR DE FORMACION Y/O INSTRUCTOR</td>
					<td width="350"></td>
					<td class="gris" width="80" align="center">FECHA</td>
					<td width="144.3"></td>
				</tr>
			</table>';


	$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
				<tr>
					<td align="center" height="30"></td>
				</tr>
			</table>';

	$html .= '<table cellspacing="0" cellpadding="5" border="1" class="certificado1" width="99%">
				<tr>
					<td align="center" width="40" class="gris">ITEM</td>
					<td align="center" width="250" class="gris">NOMBRE</td>
					<td align="center" class="gris">CEDULA</td>
					<td align="center" class="gris">EMPRESA</td>
					<td align="center" class="gris">TELEFONO</td>
					<td align="center" class="gris">FIRMA DEL ESTUDIANTE</td>
				</tr>';

	$item = 1;

	foreach ($registros as $rg) {
		$html .= '<tr>
                    <td align="center"><br><br>' . $item . '</td>
                    <td><br><br>' . $rg->nombre_primero_al . ' ' . $rg->nombre_segundo_al . ' ' . $rg->apellidos_al . '</td>
                    <td><br><br>' . $rg->documento_al . '</td>
                    <td><br><br>' . $rg->empresa_al . '</td>
                    <td><br><br>' . $rg->telefono_al . '</td>
                    <td><img src="' . $rg->firma_al . '"></td>
                </tr>';

		$item++;
	}

	$html .= '	<tr>
					<td colspan="6" class="gris">OBSERVACIONES</td>
				</tr>
				<tr>
					<td colspan="6" height="60"></td>
				</tr>
			</table>';


	$pdf->writeHTML($estilo . $html, true, false, false, false, '');

	$pdf->Output('Formato_de_difusion.pf', 'I');
} else {
	echo 'No se encontraron resultados';
}
