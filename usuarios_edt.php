<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

if (isset($_GET['usuario']) && $_GET['usuario'] != '') {
	require("libs/conexion.php");

	$usuarios = $db
		->where('id', $_GET['usuario'])
		->objectBuilder()->get('usuarios');

	if ($db->count > 0) {
		$rsc = $usuarios[0];
		$nombre = $rsc->nombre;
		$apellido = $rsc->apellido;
		$correo = $rsc->login;

		$ls_estados = '';
		$estados = ['Activo' => 1, 'Inactivo' => 0];
		foreach ($estados as $key => $value) {
			$ls_estados .= '<option ' . ($value == $rsc->estado ? "Selected" : "") . ' value="' . $value . '">' . $key . '</option>';
		}
	} else {
		header('Location: listar-usuarios');
	}

	$ls_menu = '';

	$menu = $db
		->where('Id_m', 4, '!=')
		->objectBuilder()->get('menu');

	foreach ($menu as $item) {
		$permiso = '';

		$permisos = $db
			->where('usuario_up', $_GET['usuario'])
			->where('modulo_up', $item->Id_m)
			->where('permiso_up', 1)
			->objectBuilder()->get('usuarios_permisos');

		if ($db->count > 0) {
			$permiso = 'checked';
		}

		$ls_menu .= '<tr>
						<td>
							' . $item->nombre_m . '
						</td>
						<td>
							<input type="checkbox" name="usuario[permiso][' . $item->Id_m . ']" value="1" ' . $permiso . '>
						</td>
					</tr>';
	}
} else {
	header('Location: listar-usuarios');
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Usuarios | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		table tbody {
			text-align: left;
		}

		input[type="checkbox"] {
			width: 20px;
			height: 20px;
			margin: 0 10px 0px 20px;
		}

		.Registro input[type="checkbox"]:focus {
			border: none;
			-webkit-box-shadow: none;
			box-shadow: none;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Editar Usuario</h2>
				<form id="editar-usuario">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombres" name="usuario[nombre]" value="<?php echo $nombre ?>" required>
							<label>Apellido *</label>
							<input type="text" placeholder="Apellidos" name="usuario[apellido]" value="<?php echo $apellido ?>" required>
							<label>Estado</label>
							<select name="usuario[estado]">
								<?php echo $ls_estados ?>
							</select>
							<input type="hidden" name="usuario[idusuario]" value="<?php echo $_GET['usuario'] ?>">
						</div>
						<div class="Registro-der">
							<label>Correo *</label>
							<input type="email" placeholder="Correo" name="usuario[correo]" value="<?php echo $correo ?>" required>
							<label>Password </label>
							<input type="password" placeholder="Password" name="usuario[password]">
						</div>
						<hr>
						<div class="Registro-cent Tabla-listar">
							<table>
								<thead>
									<th>Módulo</th>
									<th>Acción</th>
								</thead>
								<tbody>
									<?php echo $ls_menu ?>
								</tbody>
							</table>
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/usuarios_edt.js?<?php echo time() ?>"></script>
</body>

</html>
