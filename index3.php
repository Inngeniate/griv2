<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Gricompany Gestión del Riesgo Integral</title>
	<!-- <link rel="stylesheet" href="css/slider.css" /> -->
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" href="css/main.css" />
</head>
<body>
<?php include_once "analyticstracking.php"?>
<div class="Contenedor">
	<header>
		<?php include "menu.php";?>
	</header>
</div>
<div id="fix-posit"></div>
<section id="mainslider" class="parallax">
	<!--<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>-->
	<div class="video-background-container">
		<video preload="auto" autoplay loop poster="bag1.jpg" class="Contenedor-video-bag">
   			<source src="video/car.webm" type="video/webm">
    		<source src="video/car.mp4" type="video/mp4">
		</video>
	</div>
	<!-- slider -->
	<div class="slider-wrapper">
		<div class="responisve-container">
			<div class="slider">
				<div class="fs_loader"></div>
				<div class="slide">
					<img  src="images/slide/11.png" alt="" width="420" height="720" data-position="75,624" data-in="left" data-delay="0" data-out="fade">
					<p class="fs_dark" data-position="180,0" data-in="right" data-step="0" data-out="left"><strong>Bienvenidos a</strong></p>
					<p class="fs_light" data-position="272,0" data-in="right" data-step="0" data-delay="400">Gricompany</p>
					<p class="fs_btn" data-position="390,0" data-in="right" data-step="0" data-delay="1000">
						<a href="nosotros" class="theme_btn " >Saber Mas</a>
					</p>
				</div>

				<div class="slide">
					<img src="images/slide/cert.png" alt="" width="600" height="450" data-position="52,580" data-in="top" data-delay="0" data-out="bottom">
					<p class="fs_dark" data-position="180,0" data-in="right" data-step="0" data-out="left"><strong>Certificate con</strong></p>
					<p class="fs_light" data-position="272,0" data-in="right" data-step="0" data-delay="400">Nosotros</p>
					<p class="fs_btn" data-position="390,0" data-in="right" data-step="0" data-delay="1000">
						<a href="portafolio" class="theme_btn " >Nuestros Servicios</a>
					</p>
				</div>

				<div class="slide">
					<!-- <img  src="images/slide/0.png" alt="" width="480" height="320" data-position="140,540" data-in="fade" data-delay="0" data-out="right"> -->
					<p class="fs_dark" data-position="180,0" data-in="right" data-step="0" data-out="left"><strong>Contáctanos para</strong></p>
					<p class="fs_light" data-position="272,0" data-in="right" data-step="0" data-delay="400">saber mas </p>
					<p class="fs_btn" data-position="390,0" data-in="right" data-step="0" data-delay="1000">
						<a href="contacto" class="theme_btn" >Contáctanos</a>
					</p>
				</div>
			</div>
		</div>
	</div>

		<!-- fin -->
		<!-- <div class="wide-container">
		    <div id="slides">
		      <ul class="slides-container">
		        <li>
		          <img src="images/bag1.jpg">
		          <div class="context">
		            <h3>TRABAJO SEGURO EN ALTURAS</h3><a href="#" class="button">Ver Mas</a>
		            </div>
		        </li>
		        <li>
		          <img src="images/bag2.jpg">
		          <div class="context">
		            <h3>TRABAJO Y RESCATE EN ESPACIOS CONFINADOS</h3><a href="#" class="button">Ver Mas</a>
		            </div>
		        </li>
		        <li>
		          <img src="images/bag3.jpg">
		          <div class="context">
		            <h3>CONTROL DE DERRAME DE HIDROCARBUROS</h3><a href="#" class="button">Ver Mas</a>
		            </div>
		        </li>

		      </ul>
		    </div>
		  </div> -->
</section>

<div id="preloader">
    <div id="loader"></div>
</div>

 <!--<script>
    // $(function() {
    //   $('#slides').superslides({
    //     inherit_width_from: '.wide-container',
    //     inherit_height_from: '.wide-container',
    //     play: 5000,
    //     animation: 'fade'
    //   });
    // });
  </script>-->

<!-- </section> -->
<section class="Blanco">
	<div class="Principal-text">
	<div class="Principal-text-parrafo">
	<br>
		<h2>Gestión del Riesgo Integral</h2>
		<p>GRI COMPANY S.A.S es una empresa legalmente constituida que cuenta con convenios institucionales diversificando los frentes y campos de trabajo según la necesidad de nuestros clientes campos avalados mediante resolución SENA Nº 198 de 2013, para realizar capacitaciones en los diferentes niveles de Trabajo Seguro en Alturas, que estableció los requisitos técnicos de infraestructura de los centros de entrenamiento. Además cuenta con certificación de la Secretaria de Salud Departamental, mediante resolución No. 2278/2014, para dictar cursos de Brigada Integral de Emergencia, Primeros Auxilios, Sistema en Gestión de Seguridad y Salud ocupacional.</p>
		</div>
	</div>
	<div class="Divisor"><hr class="divi2"></div>
	<div class="Iconos-centro" >
		<div class="Iconos-centro-iteam zoom">
			<a href="certificados"><img src="images/certificado.png" alt="Gricompany"><figcaption><h3>Consulta tu certificado</h3></figcaption></a>
		</div>
		<div class="Iconos-centro-iteam zoom">
			<a href="nosotros"><img src="images/alcance.png" alt="Gricompany"><figcaption><h3>Nuestro alcance</h3></figcaption></a>
		</div>
		<div class="Iconos-centro-iteam zoom">
			<a href="portafolio"><img src="images/servicios.png" alt="Gricompany"><figcaption><h3>Certificate con nosotros</h3></figcaption></a>
		</div>
	</div>
</section>
<section>
<?php include "convenios.php";?>
</section>


<!--//<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
<?php include "redes.php";?>
<footer>
    <?php include "footer.php";?>
</footer>
	<!--<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>-->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script type='text/javascript' src='js/jquery.fractionslider.min.js'></script>
	<script type='text/javascript' src="js/main.js"></script>
	<script type="text/javascript" src="js/script-menu.js"></script>
</body>
</html>