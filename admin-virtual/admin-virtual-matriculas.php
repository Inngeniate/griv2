<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: ../admin');

require_once "libs/conexion.php";
$ls_programas = '';

$programas = $db
    ->where('estado_pr', 1)
    ->objectBuilder()->get('programas');

foreach ($programas as $programa) {
    $ls_programas .= '<option value="' . $programa->Id_pr . '">' . $programa->nombre_pr . '</option>';
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador Aulas Virtuales</title>
    <link rel="stylesheet" href="css/stylesheet.css?v<?php echo date('YmdHis') ?>" />
    <link rel="stylesheet" href="css/materialize.css" />
    <link href="css/noty.css" rel="stylesheet">
    <link href="css/relax.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <header>
        <div class="Virtual-top">
            <div class="Virtual-top-int">
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <a href="javascript:void(0)" id="Drop" class="Menu-drop"><img src="assets/images/menu_hamburgesa.png" class="Menu-ham"></a>
                        <h2 class="Titulo-mediano">Administrador de Aulas</h2>
                    </div>
                </div>
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <!-- <a href="" class="Menu-drop"><i class="icon-user"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="Virtual-contenedor-principal">
            <div class="Virtual-contenedor-principal-izq Virtual-contenedor-principal-izq-min">
                <?php include("menu-leftadmin.php") ?>
            </div>
            <div class="Virtual-contenedor-principal-der">
                <div class="Virtual-contenedor-principal-der-int">

                    <div class="Virtual-contenedor-principal-titulo">
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <h2 class="Titulo-seccion">Matriculas</h2>
                        </div>
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <div class="Flotante-crear">
                                <a href="#" data-target="seleccionar-matricula" class="modal-trigger btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons">add</i></a>
                            </div>
                        </div>
                    </div>
                    <?php //include("menu-supadmin-tab-programas.php");
                    ?>
                    <section>
                        <div class="Contendor-filtros">
                            <form id="busqueda">
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <select id="Programa-bus" class="select-programas">
                                                <option value="" selected>Seleccionar</option>
                                                <?php echo $ls_programas?>
                                            </select>
                                            <label>Programa</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <input id="Ident-bus" type="text">
                                            <label for="Ident-bus" class="">Buscar por número identificación</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                            <input type="submit" class="waves-button-input" value="Buscar" name="">
                                        </i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <section>
                        <div class="Contenedor-tabla">
                            <div class="Contenedor-tabla-int">
                                <table class="Table-virtual striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Identificación</th>
                                            <th>Correo</th>
                                            <th>Programa</th>
                                            <th>Cursos</th>
                                            <th>Grupo</th>
                                            <th colspan="2" class="center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="Listado-matriculas">
                                    </tbody>
                                </table>
                                <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                                    <div class="preloader-wrapper big active">
                                        <div class="spinner-layer">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="gap-patch">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="listado-paginacion">
                                    <ul class="pagination">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <div id="seleccionar-matricula" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Selecciona el metodo de matricula</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="nuevo">
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col m6 center">
                                    <a href="admin-virtual-matriculas-estudiante" class="waves-effect waves-light btn blue-grey darken-4">Estudiante</a><br><br>
                                    <span>Matrícula un estudiante de forma individual.</span>
                                </div>
                                <div class="input-field col m6 center">
                                    <a href="admin-virtual-matriculas-programa" class="waves-effect waves-light btn blue-grey darken-4">Programa</a><br><br>
                                    <span>Matrícula por grupo de estudiantes.</span>
                                </div>
                                <div class="input-field col m6 center">
                                    <a href="admin-virtual-matriculas-multiple" class="waves-effect waves-light btn blue-grey darken-4">Archivo plano</a><br><br>
                                    <span>Matrícula de estudiantes mediante archivo plano</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/noty.min.js"></script>
    <script src="js/date-espanol.js"></script>
    <script src="js/menu-slide.js"></script>
    <script src="js/admin-matriculas.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
