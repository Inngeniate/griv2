<?php

require_once "conexion.php";
$data = $_REQUEST['programa'];
$informacion = array();

switch ($data['opc']) {
    case 'Programa-nuevo':
        $comprobar = $db
            ->where('nombre_pr', $data['nombre'])
            ->objectBuilder()->get('programas');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg'] = 'El programa ya existe.';
        } else {
            $datos = array(
                'nombre_pr' => $data['nombre'],
                'descripcion_pr' => $data['descripcion'],
                'duracion_pr' => $data['duracion'],
                'estado_pr' => $data['estado']
            );

            $nuevo = $db
                ->insert('programas', $datos);

            if ($nuevo) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Programa creado.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'El programa no se pudo crear.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Programa-listado':
        include_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['nombre'] == '' ? $data['nombre'] = '%' : '');


        $programas = $db
            ->where('nombre_pr', '%' . $data['nombre'] . '%', 'LIKE')
            ->objectBuilder()->get('programas');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $programas = $db
                ->where('nombre_pr', '%' . $data['nombre'] . '%', 'LIKE')
                ->orderBy('Id_pr', 'DESC')
                ->objectBuilder()->paginate('programas', $page);

            foreach ($programas as $programa) {
                $btn_activo = '';

                if ($programa->estado_pr == 0) {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Btn-activar Activar"><i class="icon-checkmark"></i>Activar</a>';
                } else {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Inactivar"><i class="icon-blocked"></i>Inactivar</a>';
                }

                $content .= '<tr id="Pr-' . $programa->Id_pr . '">
                                <td>' . $programa->Id_pr . '</td>
                                <td>' . $programa->nombre_pr . '</td>
                                <td>' . $programa->descripcion_pr . '</td>
                                <td>' . $programa->duracion_pr . '</td>
                                <td>' . ($programa->estado_pr == 0 ? "Inactivo" : "Activo") . '</td>
                                <td class="center"><a href="javascript://" data-target="editar" class="Btn-editar modal-trigger"><i class="icon-pencil"></i>Editar</a></td>
                                <td class="center">' . $btn_activo . '</td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="6">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Programa-info':
        $idprograma = explode('-', $data['idprograma']);

        $programas = $db
            ->where('Id_pr', $idprograma[1])
            ->objectBuilder()->get('programas');

        if ($db->count > 0) {
            $informacion['info'] = $programas[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El programa no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Programa-editar':
        $idprograma = explode('-', $data['idprograma']);

        $comprobar = $db
            ->where('nombre_pr', $data['nombre'])
            ->where('Id_pr', $idprograma[1], '!=')
            ->objectBuilder()->get('programas');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg'] = 'El programa ya existe.';
        } else {
            $datos = array(
                'nombre_pr' => $data['nombre'],
                'descripcion_pr' => $data['descripcion'],
                'duracion_pr' => $data['duracion'],
                'estado_pr' => $data['estado']
            );

            $nuevo = $db
                ->where('Id_pr', $idprograma[1])
                ->update('programas', $datos);

            if ($nuevo) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Programa editado.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'El programa no se pudo editar.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Programa-activacion':
        $idprograma = explode('-', $data['idprograma']);

        $programas = $db
            ->where('Id_pr', $idprograma[1])
            ->objectBuilder()->get('programas');

        if ($db->count > 0) {
            $activar = $db
                ->where('Id_pr', $idprograma[1])
                ->update('programas', ['estado_pr' => $db->not()]);

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El programa no existe.';
        }

        echo json_encode($informacion);
        break;
}
