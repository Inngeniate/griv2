<?php

require_once "conexion.php";
$data = $_REQUEST['matricula'];
$informacion = array();
date_default_timezone_set("America/Bogota");

switch ($data['opc']) {
    case 'Estudiantes':
        $existe = 0;

        $comprobar = $db
            ->where('numero_identificacion', $data['identificacion'])
            ->objectBuilder()->get('aspirantes');

        if ($db->count > 0) {
            $existe = 1;

            $info = array();
            $info['id'] = $comprobar[0]->Id_as;
            $info['nombre'] = $comprobar[0]->aspirantes_nombres;
            $info['apellido'] = $comprobar[0]->aspirante_apellidos;
            $info['correo'] = $comprobar[0]->aspirante_correo;
            $info['identificacion'] = $comprobar[0]->numero_identificacion;

            if ($comprobar[0]->aspirante_celular != '') {
                $info['telefono'] = $comprobar[0]->aspirante_celular;
            } else {
                $info['telefono'] = $comprobar[0]->aspirante_telefono;
            }
        } else {
            $comprobar = $db
                ->where('REPLACE(numero_ident, ",", "" )', $data['identificacion'])
                ->objectBuilder()->get('registros');
            if ($db->count > 0) {
                $existe = 1;

                $info = array();
                $info['nombre'] = $comprobar[0]->nombres;
                $info['apellido'] = $comprobar[0]->apellidos;
                $info['correo'] = $comprobar[0]->correo;
                $info['identificacion'] = $comprobar[0]->numero_ident;
                $info['telefono'] = $comprobar[0]->telefono;
            }
        }

        if ($existe == 1) {
            $informacion['status'] = true;
            $informacion['info'] = $info;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'No se encontró el estudiante';
        }

        echo json_encode($informacion);
        break;
    case 'Estudiantes2':
        $existe = 0;
        $info = array();
        $nombre = array();
        $apellido = array();
        $correo = array();
        $telefono = array();
        $identificacion = array();

        for ($i = 0; $i < count($data['identificacion']); $i++) {
            $comprobar = $db
                ->where('numero_identificacion', $data['identificacion'][$i])
                ->objectBuilder()->get('aspirantes');

            if ($db->count > 0) {
                $existe = 1;
                $telefono = '';

                if ($comprobar[0]->aspirante_celular != '') {
                    $telefono = $comprobar[0]->aspirante_celular;
                } else {
                    $telefono = $comprobar[0]->aspirante_telefono;
                }

                $info[] = array('nombre' => $comprobar[0]->aspirantes_nombres, 'apellido' => $comprobar[0]->aspirante_apellidos, 'correo' =>  $comprobar[0]->aspirante_correo, 'identificacion' => $comprobar[0]->numero_identificacion, 'telefono' => $telefono);
            } else {
                $comprobar = $db
                    ->where('REPLACE(numero_ident, ",", "" )', $data['identificacion'][$i])
                    ->objectBuilder()->get('registros');
                if ($db->count > 0) {
                    $existe = 1;

                    // $info = array();
                    $info[] = array('nombre' => $comprobar[0]->nombres, 'apellido' => $comprobar[0]->apellidos, 'correo' =>  $comprobar[0]->correo, 'identificacion' => $comprobar[0]->numero_ident, 'telefono' => $comprobar[0]->telefono);
                }
            }
        }

        if ($existe == 1) {
            $informacion['status'] = true;
            $informacion['info'] = $info;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'No se encontró el estudiante';
        }

        echo json_encode($informacion);
        break;
    case 'Matricula-estudiante':
        $nombre = '';
        $apellido = '';
        $correo = '';
        $telefono = '';
        $existe = 0;

        $comprobar = $db
            ->where('numero_identificacion', $data['estudiante'])
            ->objectBuilder()->get('aspirantes');

        if ($db->count > 0) {
            $existe = 1;

            $nombre = $comprobar[0]->aspirantes_nombres;
            $apellido = $comprobar[0]->aspirante_apellidos;
            $correo = $comprobar[0]->aspirante_correo;
            if ($comprobar[0]->aspirante_celular != '') {
                $telefono = $comprobar[0]->aspirante_celular;
            } else {
                $telefono = $comprobar[0]->aspirante_telefono;
            }
        } else {
            $comprobar = $db
                ->where('REPLACE(numero_ident, ",", "" )', $data['estudiante'])
                ->objectBuilder()->get('registros');
            if ($db->count > 0) {
                $existe = 1;

                $nombre = $comprobar[0]->nombres;
                $apellido = $comprobar[0]->apellidos;
                $correo = $comprobar[0]->correo;
                $telefono = $comprobar[0]->telefono;
            }
        }

        if ($existe == 1) {
            $datos = [
                'identificacion_ma' => $data['estudiante'],
                'nombre_ma' => $nombre,
                'apellido_ma' => $apellido,
                'correo_ma' => $correo,
                'telefono_ma' => $telefono,
                'programa_ma' => $data['programa'],
                'creacion_ma' => $db->now(),
            ];

            $nueva = $db
                ->insert('matriculas', $datos);

            if ($nueva) {
                $pass = password_hash($data['estudiante'], PASSWORD_BCRYPT);

                $datos = array(
                    'nombre_us' => $nombre . ' ' . $apellido,
                    'login_us' => $correo,
                    'password_us' => $pass,
                    'tipo_us' => 2,
                    'Id_tipo' => $nueva,
                    'imagen_us' => '',
                );

                $usuario = $db
                    ->insert('usuarios_app', $datos);

                $informacion['status'] = true;
                $informacion['msg'] = 'Matricula registrada';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La matricula no se pudo registrar';
            }

            if ($nueva) {
                for ($i = 0; $i < count($data['curso']); $i++) {
                    $datos = [
                        'Id_ma'  => $nueva,
                        'curso_md' => $data['curso'][$i],
                        'grupo_md' => $data['grupo'][$i],
                    ];

                    $detalle = $db
                        ->insert('matriculas_detalles', $datos);
                }
            }
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'La matricula no se pudo registrar';
        }

        echo json_encode($informacion);
        break;
    case 'Matricula-grupo':
        $nombre = '';
        $apellido = '';
        $correo = '';
        $telefono = '';
        $existe = 0;
        $no_existe = array();


        for ($i = 0; $i < count($data['identificacion']); $i++) {
            $comprobar = $db
                ->where('numero_identificacion', $data['identificacion'][$i])
                ->objectBuilder()->get('aspirantes');

            if ($db->count > 0) {
                $existe = 1;

                $nombre = $comprobar[0]->aspirantes_nombres;
                $apellido = $comprobar[0]->aspirante_apellidos;
                $correo = $comprobar[0]->aspirante_correo;
                if ($comprobar[0]->aspirante_celular != '') {
                    $telefono = $comprobar[0]->aspirante_celular;
                } else {
                    $telefono = $comprobar[0]->aspirante_telefono;
                }
            } else {
                $comprobar = $db
                    ->where('REPLACE(numero_ident, ",", "" )', $data['identificacion'][$i])
                    ->objectBuilder()->get('registros');
                if ($db->count > 0) {
                    $existe = 1;

                    $nombre = $comprobar[0]->nombres;
                    $apellido = $comprobar[0]->apellidos;
                    $correo = $comprobar[0]->correo;
                    $telefono = $comprobar[0]->telefono;
                }
            }

            if ($existe == 1) {
                $datos = [
                    'identificacion_ma' => $data['identificacion'][$i],
                    'nombre_ma' => $nombre,
                    'apellido_ma' => $apellido,
                    'correo_ma' => $correo,
                    'telefono_ma' => $telefono,
                    'programa_ma' => $data['programa'],
                    'creacion_ma' => $db->now(),
                ];

                $nueva = $db
                    ->insert('matriculas', $datos);

                if ($nueva) {
                    for ($j = 0; $j < count($data['curso']); $j++) {
                        $datos = [
                            'Id_ma'  => $nueva,
                            'curso_md' => $data['curso'][$j],
                            'grupo_md' => $data['grupo'][$j],
                        ];

                        $detalle = $db
                            ->insert('matriculas_detalles', $datos);
                    }
                }
                $existe = 0;
            } else {
                $no_existe[] = $data['identificacion'][$i];
            }
        }

        if (count($no_existe) == count($data['identificacion'])) {
            $informacion['status'] = false;
            $informacion['msg'] = 'Los estudiantes no existen';
        } else {
            if (count($no_existe) == 0) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Matricula registrada';
            } else {
                $informacion['status'] = true;
                $informacion['msg'] = 'Algunos estudiantes no se pudieron registrar';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Matricula-multiple':
        $nombre = '';
        $apellido = '';
        $correo = '';
        $telefono = '';
        $existe = 0;

        for ($i = 0; $i < count($data['estudiante']); $i++) {
            $comprobar = $db
                ->where('numero_identificacion', $data['estudiante'][$i])
                ->objectBuilder()->get('aspirantes');

            if ($db->count > 0) {
                $existe = 1;

                $nombre = $comprobar[0]->aspirantes_nombres;
                $apellido = $comprobar[0]->aspirante_apellidos;
                $correo = $comprobar[0]->aspirante_correo;
                if ($comprobar[0]->aspirante_celular != '') {
                    $telefono = $comprobar[0]->aspirante_celular;
                } else {
                    $telefono = $comprobar[0]->aspirante_telefono;
                }
            } else {
                $comprobar = $db
                    ->where('REPLACE(numero_ident, ",", "" )', $data['estudiante'][$i])
                    ->objectBuilder()->get('registros');
                if ($db->count > 0) {
                    $existe = 1;

                    $nombre = $comprobar[0]->nombres;
                    $apellido = $comprobar[0]->apellidos;
                    $correo = $comprobar[0]->correo;
                    $telefono = $comprobar[0]->telefono;
                }
            }

            if ($existe == 1) {
                $datos = [
                    'identificacion_ma' => $data['estudiante'][$i],
                    'nombre_ma' => $nombre,
                    'apellido_ma' => $apellido,
                    'correo_ma' => $correo,
                    'telefono_ma' => $telefono,
                    'programa_ma' => $data['programa'],
                    'creacion_ma' => $db->now(),
                ];

                $nueva = $db
                    ->insert('matriculas', $datos);

                if ($nueva) {
                    $pass = password_hash($data['estudiante'][$i], PASSWORD_BCRYPT);

                    $datos = array(
                        'nombre_us' => $nombre . ' ' . $apellido,
                        'login_us' => $correo,
                        'password_us' => $pass,
                        'tipo_us' => 2,
                        'Id_tipo' => $nueva,
                        'imagen_us' => '',
                    );

                    $usuario = $db
                        ->insert('usuarios_app', $datos);

                    $informacion['status'] = true;
                    $informacion['msg'] = 'Matricula registrada';
                } else {
                    $informacion['status'] = false;
                    $informacion['msg'] = 'La matricula no se pudo registrar';
                }

                if ($nueva) {
                    for ($j = 0; $j < count($data['curso']); $j++) {
                        $datos = [
                            'Id_ma'  => $nueva,
                            'curso_md' => $data['curso'][$j],
                            'grupo_md' => $data['grupo'][$j],
                        ];

                        $detalle = $db
                            ->insert('matriculas_detalles', $datos);
                    }
                }
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'La matricula no se pudo registrar';
            }
        }


        echo json_encode($informacion);
        break;
    case 'Matricula-listado':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['identificacion'] == '' ? $data['identificacion'] = '%' : '');


        $matriculas = $db
            ->where('programa_ma', $data['programa'], 'LIKE')
            ->where('identificacion_ma', $data['identificacion'], 'LIKE')
            ->objectBuilder()->get('matriculas');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $matriculas = $db
                ->where('programa_ma', $data['programa'], 'LIKE')
                ->where('identificacion_ma', $data['identificacion'], 'LIKE')
                ->orderBy('Id_ma', 'DESC')
                ->objectBuilder()->paginate('matriculas', $page);


            foreach ($matriculas as $matricula) {
                $nm_programa = '';

                $programas = $db
                    ->where('Id_pr', $matricula->programa_ma)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $ls_cursos = '';
                $ls_grupos = '';

                $detalles = $db
                    ->where('Id_ma', $matricula->Id_ma)
                    ->objectBuilder()->get('matriculas_detalles');

                foreach ($detalles as $detalle) {
                    $cursos = $db
                        ->where('Id_cu', $detalle->curso_md)
                        ->objectBuilder()->get('cursos');
                    if ($db->count > 0) {
                        $ls_cursos .= '<p>' . $cursos[0]->nombre_cu . '</p>';
                    }

                    $grupos = $db
                        ->where('Id_gr', $detalle->grupo_md)
                        ->objectBuilder()->get('grupos');
                    if ($db->count > 0) {
                        $ls_grupos .= '<p>' . $grupos[0]->codigo_gr . '</p>';
                    }
                }

                $content .= ' <tr id="Ma-' . $matricula->Id_ma . '">
                                <td>' . $matricula->Id_ma . '</td>
                                <td>' . $matricula->nombre_ma . '</td>
                                <td>' . $matricula->apellido_ma . '</td>
                                <td>' . $matricula->identificacion_ma . '</td>
                                <td>' . $matricula->correo_ma . '</td>
                                <td>' . $nm_programa . '</td>
                                <td>' . $ls_cursos . '</td>
                                <td>' . $ls_grupos . '</td>
                                <td class="center"></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="6">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
}
