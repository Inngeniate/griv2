<?php

require_once "conexion.php";
$data = $_REQUEST['corte'];
$informacion = array();

switch ($data['opc']) {
    case 'Corte-nuevo':
        $comprobar = $db
            ->where('Id_pr', $data['programa'])
            ->where('periodo_co', $data['periodo'])
            ->objectBuilder()->get('cortes');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg'] = 'El programa ya tiene cortes creados.';
        } else {
            for ($i = 0; $i < count($data['porcentaje']); $i++) {
                $datos = array(
                    'Id_pr' => $data['programa'],
                    'periodo_co' => $data['periodo'],
                    'porcentaje_co' => $data['porcentaje'][$i]
                );

                $nuevo = $db
                    ->insert('cortes', $datos);
            }

            $informacion['status'] = true;
            $informacion['msg'] = 'Cortes creados.';
        }

        echo json_encode($informacion);
        break;
    case 'Corte-listado':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['periodo'] == '' ? $data['periodo'] = '%' : '');

        $cortes = $db
            ->where('Id_pr', $data['programa'], 'LIKE')
            ->where('periodo_co', $data['periodo'], 'LIKE')
            ->groupBy('periodo_co')
            ->groupBy('Id_pr')
            ->objectBuilder()->get('cortes');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $cortes = $db
                ->where('Id_pr', $data['programa'], 'LIKE')
                ->where('periodo_co', $data['periodo'], 'LIKE')
                ->groupBy('periodo_co')
                ->groupBy('Id_pr')
                ->orderBy('periodo_co', 'DESC')
                ->objectBuilder()->paginate('cortes', $page);

            foreach ($cortes as $corte) {
                $nm_programa = '';

                $programas = $db
                    ->where('Id_pr', $corte->Id_pr)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $total_cortes = 0;
                $ls_porcentajes = '';

                $detalles = $db
                    ->where('Id_pr', $corte->Id_pr)
                    ->where('periodo_co', $corte->periodo_co)
                    ->objectBuilder()->get('cortes');

                $total_cortes = $db->count;

                foreach ($detalles as $detalle) {
                    $ls_porcentajes .= '<p>' . $detalle->porcentaje_co . ' %</p>';
                }


                $content .= ' <tr id="Co-' . $corte->Id_co . '">
                                <td>' . $corte->periodo_co . '</td>
                                <td>' . $nm_programa . '</td>
                                <td>' . $total_cortes . '</td>
                                <td>' . $ls_porcentajes . '</td>
                                <td class="center"><a href="#" data-target="editar" class="Btn-editar modal-trigger"><i class="icon-pencil"></i> Editar</a></td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="5">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Corte-info':
        $idcorte = explode('-', $data['idcorte']);

        $cortes = $db
            ->where('Id_co', $idcorte[1])
            ->objectBuilder()->get('cortes');

        if ($db->count > 0) {
            $informacion['info']['programa'] = $cortes[0]->Id_pr;
            $informacion['info']['periodo'] = $cortes[0]->periodo_co;

            $detalles = $db
                ->where('periodo_co', $cortes[0]->periodo_co)
                ->where('Id_pr', $cortes[0]->Id_pr)
                ->objectBuilder()->get('cortes');

            $informacion['info']['cantidad'] = $db->count;

            foreach ($detalles as $detalle) {
                $informacion['info']['porcentajes'][] = $detalle->porcentaje_co;
            }

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'No existen cortes.';
        }

        echo json_encode($informacion);
        break;
    case 'Corte-editar':
        $idcorte = explode('-', $data['idcorte']);

        $borrar = $db
            ->where('Id_pr', $data['programa'])
            ->where('periodo_co', $data['periodo'])
            ->delete('cortes');

        for ($i = 0; $i < count($data['porcentaje']); $i++) {
            $datos = array(
                'Id_pr' => $data['programa'],
                'periodo_co' => $data['periodo'],
                'porcentaje_co' => $data['porcentaje'][$i]
            );

            $nuevo = $db
                ->insert('cortes', $datos);
        }

        $informacion['status'] = true;
        $informacion['msg'] = 'Cortes ingresados.';


        echo json_encode($informacion);
        break;
}
