<?php

require_once "conexion.php";
$data = $_REQUEST['grupo'];
$informacion = array();

switch ($data['opc']) {
    case 'Grupo-nuevo':
        $datos = array(
            'Id_pr' => $data['programa'],
            'Id_cu' => $data['curso'],
            'Id_do' => $data['docente'],
            'codigo_gr' => '',
            'estudiantes_gr' => $data['estudiantes'],
            'inicio_gr' => $data['inicio'],
            'fin_gr' => $data['fin'],
            'estado_gr' => $data['estado'],
        );

        $nuevo = $db
            ->insert('grupos', $datos);

        if ($nuevo) {
            $codigo = substr(date('Y'), -2) . '-' . $data['programa'] . '-' . $data['curso'] . '-' . $nuevo;

            $db
                ->where('Id_gr', $nuevo)
                ->update('grupos', ['codigo_gr' => $codigo]);

            $informacion['status'] = true;
            $informacion['msg'] = 'Grupo creado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no se pudo crear.';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-listado':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['curso'] == '' ? $data['curso'] = '%' : '');
        ($data['codigo'] == '' ? $data['codigo'] = '%' : '');


        $grupos = $db
            ->where('Id_pr', $data['programa'], 'LIKE')
            ->where('Id_cu', $data['curso'], 'LIKE')
            ->where('codigo_gr', $data['codigo'], 'LIKE')
            ->objectBuilder()->get('grupos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $grupos = $db
                ->where('Id_pr', $data['programa'], 'LIKE')
                ->where('Id_cu', $data['curso'], 'LIKE')
                ->where('codigo_gr', $data['codigo'], 'LIKE')
                ->orderBy('Id_gr', 'DESC')
                ->objectBuilder()->paginate('grupos', $page);


            foreach ($grupos as $grupo) {
                $nm_programa = '';
                $nm_curso = '';
                $nm_docente = '';

                $programas = $db
                    ->where('Id_pr', $grupo->Id_pr)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $cursos = $db
                    ->where('Id_cu', $grupo->Id_cu)
                    ->objectBuilder()->get('cursos');

                if ($db->count > 0) {
                    $nm_curso = $cursos[0]->nombre_cu;
                }

                $docentes = $db
                    ->where('Id_do', $grupo->Id_do)
                    ->objectBuilder()->get('docentes');

                if ($db->count > 0) {
                    $nm_docente = $docentes[0]->nombre_do . ' ' . $docentes[0]->apellido_do;
                }

                $btn_activo = '';

                if ($grupo->estado_gr == 0) {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Btn-activar Activar"><i class="icon-checkmark"></i>Activar</a>';
                } else {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Inactivar"><i class="icon-blocked"></i>Inactivar</a>';
                }

                $content .= '<tr id="Gr-' . $grupo->Id_gr . '">
                                <td>' . $nm_programa . '</td>
                                <td>' . $nm_curso . '</td>
                                <td>' . $nm_docente . '</td>
                                <td>' . $grupo->codigo_gr . '</td>
                                <td>' . $grupo->estudiantes_gr . '</td>
                                <td>' . $grupo->inicio_gr . '</td>
                                <td>' . $grupo->fin_gr . '</td>
                                <td>' . ($grupo->estado_gr == 0 ? "Inactivo" : "Activo") . '</td>
                                <td class="center"><a href="#" data-target="editar" class="Btn-editar modal-trigger"><i class="icon-pencil"></i> Editar</a></td>
                                <td class="center">' . $btn_activo . '</td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="8">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-info':
        $idgrupo = explode('-', $data['idgrupo']);

        $grupos = $db
            ->where('Id_gr', $idgrupo[1])
            ->objectBuilder()->get('grupos');

        if ($db->count > 0) {
            $informacion['info'] = $grupos[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-editar':
        $idgrupo = explode('-', $data['idgrupo']);
        $codigo = substr(date('Y'), -2) . '-' . $data['programa'] . '-' . $data['curso'] . '-' . $idgrupo[1];

        $datos = array(
            'Id_pr' => $data['programa'],
            'Id_cu' => $data['curso'],
            'Id_do' => $data['docente'],
            'codigo_gr' => $codigo,
            'estudiantes_gr' => $data['estudiantes'],
            'inicio_gr' => $data['inicio'],
            'fin_gr' => $data['fin'],
            'estado_gr' => $data['estado'],
        );

        $nuevo = $db
            ->where('Id_gr', $idgrupo[1])
            ->update('grupos', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Grupo editado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no se pudo editar.';
        }

        echo json_encode($informacion);
        break;
    case 'Grupo-activacion':
        $idgrupo = explode('-', $data['idgrupo']);

        $grupos = $db
            ->where('Id_gr', $idgrupo[1])
            ->objectBuilder()->get('grupos');

        if ($db->count > 0) {
            $activar = $db
                ->where('Id_gr', $idgrupo[1])
                ->update('grupos', ['estado_gr' => $db->not()]);

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El grupo no existe.';
        }

        echo json_encode($informacion);
        break;
}
