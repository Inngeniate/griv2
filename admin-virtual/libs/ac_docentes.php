<?php

require_once "conexion.php";
$data = $_REQUEST['docente'];
$informacion = array();

switch ($data['opc']) {
    case 'Docente-nuevo':
        $comprobar = $db
            ->where('identificacion_do', $data['identifica'])
            ->objectBuilder()->get('docentes');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg'] = 'El docente ya existe.';
        } else {
            $datos = array(
                'identificacion_do' => $data['identifica'],
                'correo_do' => $data['correo'],
                'nombre_do' => $data['nombre'],
                'apellido_do' => $data['apellido'],
                'telefono_do' => $data['telefono'],
                'estado_do' => $data['estado'],
            );

            $nuevo = $db
                ->insert('docentes', $datos);

            if ($nuevo) {
                include_once 'Password.php';

                $pass = password_hash($data['identifica'], PASSWORD_BCRYPT);

                $datos = array(
                    'nombre_us' => $data['nombre'].' '.$data['apellido'],
                    'login_us' => $data['correo'],
                    'password_us' => $pass,
                    'tipo_us' => 1,
                    'Id_tipo' => $nuevo,
                    'imagen_us' => '',
                );

                $usuario = $db
                    ->insert('usuarios_app', $datos);

                $informacion['status'] = true;
                $informacion['msg'] = 'Docente creado.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'El docente no se pudo crear.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Docente-listado':
        include_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['nombre'] == '' ? $data['nombre'] = '%' : '');
        ($data['identifica'] == '' ? $data['identifica'] = '%' : '');


        $docentes = $db
            ->where('nombre_do', '%' . $data['nombre'] . '%', 'LIKE')
            ->where('identificacion_do', $data['identifica'], 'LIKE')
            ->objectBuilder()->get('docentes');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $docentes = $db
                ->where('nombre_do', '%' . $data['nombre'] . '%', 'LIKE')
                ->where('identificacion_do', $data['identifica'], 'LIKE')
                ->orderBy('Id_do', 'DESC')
                ->objectBuilder()->paginate('docentes', $page);

            foreach ($docentes as $docente) {
                $btn_activo = '';

                if ($docente->estado_do == 0) {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Btn-activar Activar"><i class="icon-checkmark"></i>Activar</a>';
                } else {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Inactivar"><i class="icon-blocked"></i>Inactivar</a>';
                }

                $content .= '<tr id="Pr-' . $docente->Id_do . '">
                                <td>' . $docente->identificacion_do . '</td>
                                <td>' . $docente->nombre_do . '</td>
                                <td>' . $docente->apellido_do . '</td>
                                <td>' . $docente->correo_do . '</td>
                                <td>' . $docente->telefono_do . '</td>
                                <td>' . ($docente->estado_do == 0 ? "Inactivo" : "Activo") . '</td>
                                <td class="center"><a href="javascript://" data-target="editar" class="Btn-editar modal-trigger"><i class="icon-pencil"></i>Editar</a></td>
                                <td class="center">' . $btn_activo . '</td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="7">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Docente-info':
        $iddocente = explode('-', $data['iddocente']);

        $docentes = $db
            ->where('Id_do', $iddocente[1])
            ->objectBuilder()->get('docentes');

        if ($db->count > 0) {
            $informacion['info'] = $docentes[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El docente no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Docente-editar':
        $iddocente = explode('-', $data['iddocente']);

        $comprobar = $db
            ->where('identificacion_do', $data['identifica'])
            ->where('Id_do', $iddocente[1], '!=')
            ->objectBuilder()->get('docentes');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg'] = 'El docente ya existe.';
        } else {
            $datos = array(
                'identificacion_do' => $data['identifica'],
                'correo_do' => $data['correo'],
                'nombre_do' => $data['nombre'],
                'apellido_do' => $data['apellido'],
                'telefono_do' => $data['telefono'],
                'estado_do' => $data['estado'],
            );

            $nuevo = $db
                ->where('Id_do', $iddocente[1])
                ->update('docentes', $datos);

            if ($nuevo) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Docente editado.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'El docente no se pudo editar.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Docente-activacion':
        $iddocente = explode('-', $data['iddocente']);

        $docentes = $db
            ->where('Id_do', $iddocente[1])
            ->objectBuilder()->get('docentes');

        if ($db->count > 0) {
            $activar = $db
                ->where('Id_do', $iddocente[1])
                ->update('docentes', ['estado_do' => $db->not()]);

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El docente no existe.';
        }

        echo json_encode($informacion);
        break;
}
