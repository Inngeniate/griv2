<?php

require_once "conexion.php";
$data = $_REQUEST['curso'];
$informacion = array();

switch ($data['opc']) {
    case 'Curso-nuevo':
        $comprobar = $db
            ->where('Id_pr', $data['programa'])
            ->where('nombre_cu', $data['nombre'])
            ->objectBuilder()->get('cursos');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg'] = 'El curso ya existe.';
        } else {
            $prerrequisitos = '';

            if (isset($data['prerrequisitos'])) {
                $prerrequisitos = implode(',', $data['prerrequisitos']);
            }

            $datos = array(
                'Id_pr' => $data['programa'],
                'nombre_cu' => $data['nombre'],
                'duracion_cu' => $data['duracion'],
                'descripcion_cu' => $data['descripcion'],
                'prerrequisito_cu' => $prerrequisitos,
                'estado_cu' => $data['estado']
            );

            $nuevo = $db
                ->insert('cursos', $datos);

            if ($nuevo) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Curso creado.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'El curso no se pudo crear.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Curso-listado':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['nombre'] == '' ? $data['nombre'] = '%' : '');


        $cursos = $db
            ->where('Id_pr', $data['programa'], 'LIKE')
            ->where('nombre_cu', '%' . $data['nombre'] . '%', 'LIKE')
            ->objectBuilder()->get('cursos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $cursos = $db
                ->where('Id_pr', $data['programa'], 'LIKE')
                ->where('nombre_cu', '%' . $data['nombre'] . '%', 'LIKE')
                ->orderBy('Id_cu', 'DESC')
                ->objectBuilder()->paginate('cursos', $page);


            foreach ($cursos as $curso) {
                $nm_programa = '';

                $programas = $db
                    ->where('Id_pr', $curso->Id_pr)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $prerrequisito = 'No';
                $ls_precursos = '';

                if ($curso->prerrequisito_cu != '') {
                    $prerrequisito = 'Si';
                    $prerrequisitos = explode(',', $curso->prerrequisito_cu);
                    for ($i = 0; $i < count($prerrequisitos); $i++) {
                        $cursos = $db
                            ->where('Id_cu', $prerrequisitos[$i])
                            ->objectBuilder()->get('cursos');
                        if ($db->count > 0) {
                            $ls_precursos .= '<p>' . $cursos[0]->nombre_cu . '</p>';
                        }
                    }
                }

                $btn_activo = '';

                if ($curso->estado_cu == 0) {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Btn-activar Activar"><i class="icon-checkmark"></i>Activar</a>';
                } else {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Inactivar"><i class="icon-blocked"></i>Inactivar</a>';
                }

                $content .= ' <tr id="Cu-' . $curso->Id_cu . '">
                                <td>' . $curso->Id_cu . '</td>
                                <td>' . $nm_programa . '</td>
                                <td>' . $curso->nombre_cu . '</td>
                                <td>' . $curso->descripcion_cu . '</td>
                                <td>' . $prerrequisito . ' ' . $ls_precursos . '</td>
                                <td>' . $curso->duracion_cu . '</td>
                                <td>' . ($curso->estado_cu == 0 ? "Inactivo" : "Activo") . '</td>
                                <td class="center"><a href="#" data-target="editar" class="Btn-editar modal-trigger"><i class="icon-pencil"></i> Editar</a></td>
                                <td class="center">' . $btn_activo . '</td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="7">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Curso-info':
        $idcurso = explode('-', $data['idcurso']);

        $cursos = $db
            ->where('Id_cu', $idcurso[1])
            ->objectBuilder()->get('cursos');

        if ($db->count > 0) {
            $informacion['info'] = $cursos[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El curso no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Curso-editar':
        $idcurso = explode('-', $data['idcurso']);

        $comprobar = $db
            ->where('Id_pr', $data['programa'])
            ->where('nombre_cu', $data['nombre'])
            ->where('Id_cu', $idcurso[1], '!=')
            ->objectBuilder()->get('cursos');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg'] = 'El curso ya existe.';
        } else {
            $prerrequisitos = '';

            if (isset($data['prerrequisitos'])) {
                $prerrequisitos = implode(',', $data['prerrequisitos']);
            }

            $datos = array(
                'Id_pr' => $data['programa'],
                'nombre_cu' => $data['nombre'],
                'duracion_cu' => $data['duracion'],
                'descripcion_cu' => $data['descripcion'],
                'prerrequisito_cu' => $prerrequisitos,
                'estado_cu' => $data['estado']
            );

            $nuevo = $db
                ->where('Id_cu', $idcurso[1])
                ->update('cursos', $datos);

            if ($nuevo) {
                $informacion['status'] = true;
                $informacion['msg'] = 'Curso editado.';
            } else {
                $informacion['status'] = false;
                $informacion['msg'] = 'El curso no se pudo editar.';
            }
        }

        echo json_encode($informacion);
        break;
    case 'Curso-activacion':
        $idcurso = explode('-', $data['idcurso']);

        $cursos = $db
            ->where('Id_cu', $idcurso[1])
            ->objectBuilder()->get('cursos');

        if ($db->count > 0) {
            $activar = $db
                ->where('Id_cu', $idcurso[1])
                ->update('cursos', ['estado_cu' => $db->not()]);

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El curso no existe.';
        }

        echo json_encode($informacion);
        break;
}
