<?php

require_once "conexion.php";
$data = $_REQUEST['modulo'];
$informacion = array();

switch ($data['opc']) {
    case 'Modulo-nuevo':
        $datos = array(
            'Id_pr' => $data['programa'],
            'Id_cu' => $data['curso'],
            'nombre_mo' => $data['nombre'],
            'descripcion_mo' => $data['descripcion'],
            'estado_mo' => $data['estado'],
        );

        $nuevo = $db
            ->insert('modulos', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Modulo creado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El modulo no se pudo crear.';
        }

        echo json_encode($informacion);
        break;
    case 'Modulo-listado':
        require_once 'Paginacion.php';
        $page = $data['pagina'];
        $results_pg = 50;
        $adjacent = 2;

        ($data['programa'] == '' ? $data['programa'] = '%' : '');
        ($data['curso'] == '' ? $data['curso'] = '%' : '');
        ($data['nombre'] == '' ? $data['nombre'] = '%' : '');


        $modulos = $db
            ->where('Id_pr', $data['programa'], 'LIKE')
            ->where('Id_cu', $data['curso'], 'LIKE')
            ->where('nombre_mo', '%' . $data['nombre'] . '%', 'LIKE')
            ->objectBuilder()->get('modulos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            $content = '';
            $db->pageLimit = $results_pg;

            $modulos = $db
                ->where('Id_pr', $data['programa'], 'LIKE')
                ->where('Id_cu', $data['curso'], 'LIKE')
                ->where('nombre_mo', '%' . $data['nombre'] . '%', 'LIKE')
                ->orderBy('Id_mo', 'DESC')
                ->objectBuilder()->paginate('modulos', $page);


            foreach ($modulos as $modulo) {
                $nm_programa = '';
                $nm_curso = '';

                $programas = $db
                    ->where('Id_pr', $modulo->Id_pr)
                    ->objectBuilder()->get('programas');

                if ($db->count > 0) {
                    $nm_programa = $programas[0]->nombre_pr;
                }

                $cursos = $db
                    ->where('Id_cu', $modulo->Id_cu)
                    ->objectBuilder()->get('cursos');

                if ($db->count > 0) {
                    $nm_curso = $cursos[0]->nombre_cu;
                }

                $btn_activo = '';

                if ($modulo->estado_mo == 0) {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Btn-activar Activar"><i class="icon-checkmark"></i>Activar</a>';
                } else {
                    $btn_activo = '<a href="javascript://" class="Btn-bloquear Inactivar"><i class="icon-blocked"></i>Inactivar</a>';
                }

                $content .= '<tr id="Mo-' . $modulo->Id_mo . '">
                                <td>' . $modulo->Id_mo . '</td>
                                <td>' . $nm_programa . '</td>
                                <td>' . $nm_curso . '</td>
                                <td>' . $modulo->nombre_mo . '</td>
                                <td>' . $modulo->descripcion_mo . '</td>
                                <td>' . ($modulo->estado_mo == 0 ? "Inactivo" : "Activo") . '</td>
                                <td class="center"><a href="#" data-target="editar" class="Btn-editar modal-trigger"><i class="icon-pencil"></i> Editar</a></td>
                                <td class="center">' . $btn_activo . '</td>
                            </tr>';
            }

            $informacion['list'] = $content;
            $pagconfig = array(
                'pagina' => $page,
                'totalrows' => $db->totalPages,
                'ultima_pag' => $numpgs,
                'resultados_pag' => $results_pg,
                'adyacentes' => $adjacent
            );
            $paginate = new Paginacion($pagconfig);
            $informacion['pagination'] = $paginate->crearlinks();
        } else {
            $informacion['list'] = '<tr>
                                <td colspan="8">No hay registros</td>
                            </tr>';
            $informacion['pagination'] = '';
        }

        echo json_encode($informacion);
        break;
    case 'Modulo-info':
        $idmodulo = explode('-', $data['idmodulo']);

        $modulos = $db
            ->where('Id_mo', $idmodulo[1])
            ->objectBuilder()->get('modulos');

        if ($db->count > 0) {
            $informacion['info'] = $modulos[0];
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El modulo no existe.';
        }

        echo json_encode($informacion);
        break;
    case 'Modulo-editar':
        $idmodulo = explode('-', $data['idmodulo']);

        $datos = array(
            'Id_pr' => $data['programa'],
            'Id_cu' => $data['curso'],
            'nombre_mo' => $data['nombre'],
            'descripcion_mo' => $data['descripcion'],
            'estado_mo' => $data['estado'],
        );

        $nuevo = $db
            ->where('Id_mo', $idmodulo[1])
            ->update('modulos', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['msg'] = 'Modulo editado.';
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El modulo no se pudo editar.';
        }

        echo json_encode($informacion);
        break;
    case 'Modulo-activacion':
        $idmodulo = explode('-', $data['idmodulo']);

        $cursos = $db
            ->where('Id_mo', $idmodulo[1])
            ->objectBuilder()->get('modulos');

        if ($db->count > 0) {
            $activar = $db
                ->where('Id_mo', $idmodulo[1])
                ->update('modulos', ['estado_mo' => $db->not()]);

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'El modulo no existe.';
        }

        echo json_encode($informacion);
        break;
}
