<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: ../admin');

require_once "libs/conexion.php";
$ls_programas = '';
$ls_cursos = '';
$ls_grupos = '';

$programas = $db
    ->where('estado_pr', 1)
    ->objectBuilder()->get('programas');

foreach ($programas as $programa) {
    $ls_programas .= '<option value="' . $programa->Id_pr . '">' . $programa->nombre_pr . '</option>';
}

$cursos = $db
    ->where('estado_cu', 1)
    ->objectBuilder()->get('cursos');

foreach ($cursos as $curso) {
    $ls_cursos .= '<option value="' . $curso->Id_cu . '" class="Pr-' . $curso->Id_pr . '" >' . $curso->nombre_cu . '</option>';
}

$grupos = $db
    ->where('estado_gr', 1)
    ->objectBuilder()->get('grupos');

foreach ($grupos as $grupo) {
    $ls_grupos .= '<option value="' . $grupo->Id_gr . '" class="Pr-' . $grupo->Id_pr . ' Cu-' . $grupo->Id_cu . '" >' . $grupo->codigo_gr . '</option>';
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador Aulas Virtuales</title>
    <link rel="stylesheet" href="css/stylesheet.css?v<?php echo date('YmdHis') ?>" />
    <link rel="stylesheet" href="css/materialize.css" />
    <link href="css/noty.css" rel="stylesheet">
    <link href="css/relax.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <header>
        <div class="Virtual-top">
            <div class="Virtual-top-int">
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <a href="javascript:void(0)" id="Drop" class="Menu-drop"><img src="assets/images/menu_hamburgesa.png" class="Menu-ham"></a>
                        <h2 class="Titulo-mediano">Administrador de Aulas</h2>
                    </div>
                </div>
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="Virtual-contenedor-principal">
            <div class="Virtual-contenedor-principal-izq Virtual-contenedor-principal-izq-min">
                <?php include("menu-leftadmin.php") ?>
            </div>
            <div class="Virtual-contenedor-principal-der">
                <div class="Virtual-contenedor-principal-der-int">
                    <div class="Virtual-contenedor-principal-titulo">
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <h2 class="Titulo-seccion">Matricular estudiante</h2>
                        </div>
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <div class="Flotante-crear">
                            </div>
                        </div>
                    </div>
                    <section>
                        <div class="Texto-introductorio">
                            <p>Para matricular un estudiante recuerde que debe estar previamente inscrito, luego de esto
                                solo ingrese el numero de cedula y realice la matrícula.</p>
                        </div>
                    </section>
                    <section>
                        <div class="Contendor-filtros">
                            <form id="busqueda">
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <input id="Ident-bus" name="matricula[identificacion]" type="text" required>
                                            <label for="Ident-bus" class="">Buscar por número identificación</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                            <input type="submit" class="waves-button-input" value="Buscar" name="">
                                        </i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <section>
                        <div class="Contenedor-formularios">
                            <div class="Contenedor-formularios-int">
                                <div class="Contendor-frag F-cuatro">
                                    <label class="Lab-completo">Nombre:</label>
                                    <span class="Estudiante-nombre"></span>
                                </div>
                                <div class="Contendor-frag F-cuatro">
                                    <label class="Lab-completo">Apellido:</label>
                                    <span class="Estudiante-apellido"></span>
                                </div>
                                <div class="Contendor-frag F-cuatro">
                                    <label class="Lab-completo">Correo electrónico:</label>
                                    <span class="Estudiante-correo"></span>
                                </div>
                                <div class="Contendor-frag F-cuatro">
                                    <label class="Lab-completo">Teléfono:</label>
                                    <span class="Estudiante-telefono"></span>
                                </div>
                            </div>
                        </div>
                        <div class="Texto-introductorio">
                            <p>A continuación seleccione el programa y los cursos para que pueda asignar los grupos
                                donde realizará la matrícula del estudiante.</p>
                        </div>
                        <form id="nuevo">
                            <div class="Contenedor-formularios">
                                <div class="Contenedor-formularios-int">
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <select name="matricula[programa]" class="select-programa">
                                                <option value="" selected>Seleccionar</option>
                                                <?php echo $ls_programas ?>
                                            </select>
                                            <label>Programa</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="Contenedor-formularios-int">
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <select name="matricula[curso][]" class="select-curso">
                                                <option value="" selected>Seleccionar curso</option>
                                            </select>
                                            <label>Curso</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <select name="matricula[grupo][]" class="select-grupo">
                                                <option value="" selected>Seleccionar</option>
                                            </select>
                                            <label>Grupo</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="Temp-content">
                            </div>
                            <div class="Contenedor-formularios">
                                <div class="Contenedor-formularios-int">
                                    <div class="Contendor-frag F-tres">
                                        <a class="btn-floating btn-large waves-effect waves-light blue-grey darken-4 Add-curso"><i class="material-icons">add</i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="Contenedor-formularios">
                                <div class="Contenedor-formularios-int">
                                    <div class="Texto-introductorio">
                                        <p>Verifica que la información sea correcta antes de guardar y recuerda que puede
                                            modificarlo desde el módulo de Matrículas. </p>
                                    </div>
                                </div>

                                <div class="Contenedor-formularios Forms-virtual">
                                    <div class="Contenedor-formularios-int">
                                        <div class="Contendor-frag F-tres">
                                            <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                                <input type="submit" class="waves-button-input Texto-blanco" value="GUARDAR" name="">
                                            </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <div style="display: none" id="cursos-oculto">
        <select>
            <?php echo $ls_cursos; ?>
        </select>
    </div>
    <div style="display: none" id="grupos-oculto">
        <select>
            <?php echo $ls_grupos; ?>
        </select>
    </div>

    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/noty.min.js"></script>
    <script src="js/date-espanol.js"></script>
    <script src="js/menu-slide.js"></script>
    <script src="js/admin-matricula-estudiante.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
