<?php
    session_start();
	if (!$_SESSION['usuloggri']) header('Location: ../admin');
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador Aulas Virtuales</title>
    <link rel="stylesheet" href="css/stylesheet.css?v<?php echo date('YmdHis') ?>" />
    <link rel="stylesheet" href="css/materialize.css" />
    <link href="css/noty.css" rel="stylesheet">
    <link href="css/relax.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <header>
        <div class="Virtual-top">
            <div class="Virtual-top-int">
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <a href="javascript:void(0)" id="Drop" class="Menu-drop"><img src="assets/images/menu_hamburgesa.png" class="Menu-ham"></a>
                        <h2 class="Titulo-mediano">Administrador de Aulas</h2>
                    </div>
                </div>
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <!-- <a href="" class="Menu-drop"><i class="icon-user"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="Virtual-contenedor-principal">
            <div class="Virtual-contenedor-principal-izq Virtual-contenedor-principal-izq-min">
                <?php include("menu-leftadmin.php") ?>
            </div>
            <div class="Virtual-contenedor-principal-der">
                <div class="Virtual-contenedor-principal-der-int">

                    <div class="Virtual-contenedor-principal-titulo">
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <h2 class="Titulo-seccion">Programas</h2>
                        </div>
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <div class="Flotante-crear">
                                <a href="#" data-target="crear" class="modal-trigger btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons">add</i></a>
                            </div>
                        </div>
                    </div>
                    <?php include("menu-supadmin-tab-programas.php"); ?>
                    <section>
                        <div class="Contendor-filtros">
                            <form id="busqueda">
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <input id="Nombre-bus" type="text">
                                            <label for="Nombre-bus" class="">Buscar programa por nombre</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                            <input type="submit" class="waves-button-input" value="Buscar" name="">
                                        </i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <section>
                        <div class="Contenedor-tabla">
                            <div class="Contenedor-tabla-int">
                                <table class="Table-virtual striped">
                                    <thead>
                                        <tr>
                                            <th>Cod</th>
                                            <th>Programa</th>
                                            <th>Descripción</th>
                                            <th>Duración</th>
                                            <th>Estado</th>
                                            <th colspan="2" class="center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="Listado-programas">
                                    </tbody>
                                </table>
                                <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                                    <div class="preloader-wrapper big active">
                                        <div class="spinner-layer">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="gap-patch">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="listado-paginacion">
                                    <ul class="pagination">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <div id="crear" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Crear Programa</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="nuevo">
                    <div class="row">
                        <div class="col s12">
                            <div class="input-field">
                                <input id="nombre" name="programa[nombre]" type="text" class="validate" required="">
                                <label for="nombre">Nombre de programa</label>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="duracion-pr" name="programa[duracion]" type="text" class="validate" required="">
                                    <label for="duracion-pr">Duración</label>
                                </div>
                                <div class="input-field col m6">
                                    <select name="programa[estado]" required="">
                                        <option value="" disabled selected>Seleccionar estado</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <label>Seleccionar estado</label>
                                </div>
                            </div>
                            <div class="input-field">
                                <textarea id="descripcion-pr" name="programa[descripcion]" class="materialize-textarea validate" required=""></textarea>
                                <label for="descripcion-pr">Descripción</label>
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Para editar -->
    <div id="editar" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Editar Programa</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="editado">
                    <div class="row">
                        <div class="col s12">
                            <div class="input-field">
                                <input id="nombre-edi" name="programa[nombre]" type="text" class="validate" required="">
                                <label for="nombre-edi">Nombre de programa</label>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="duracion-pr-edi" name="programa[duracion]" type="text" class="validate" required="">
                                    <label for="duracion-pr-edi">Duración</label>
                                </div>
                                <div class="input-field col m6">
                                    <select id="estado-edi" name="programa[estado]" required="">
                                        <option value="" disabled selected>Seleccionar estado</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <label>Seleccionar estado</label>
                                </div>
                            </div>
                            <div class="input-field">
                                <textarea id="descripcion-pr-edi" name="programa[descripcion]" class="materialize-textarea validate" required=""></textarea>
                                <label for="descripcion-pr-edi">Descripción</label>
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/noty.min.js"></script>
    <script src="js/menu-slide.js"></script>
    <script src="js/admin-programas.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
