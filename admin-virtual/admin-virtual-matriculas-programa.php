<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: ../admin');

require_once "libs/conexion.php";
$ls_programas = '';
$ls_cursos = '';
$ls_grupos = '';

$programas = $db
    ->where('estado_pr', 1)
    ->objectBuilder()->get('programas');

foreach ($programas as $programa) {
    $ls_programas .= '<option value="' . $programa->Id_pr . '">' . $programa->nombre_pr . '</option>';
}

$cursos = $db
    ->where('estado_cu', 1)
    ->objectBuilder()->get('cursos');

foreach ($cursos as $curso) {
    $ls_cursos .= '<option value="' . $curso->Id_cu . '" class="Pr-' . $curso->Id_pr . '" >' . $curso->nombre_cu . '</option>';
}

$grupos = $db
    ->where('estado_gr', 1)
    ->objectBuilder()->get('grupos');

foreach ($grupos as $grupo) {
    $ls_grupos .= '<option value="' . $grupo->Id_gr . '" class="Pr-' . $grupo->Id_pr . ' Cu-' . $grupo->Id_cu . '" >' . $grupo->codigo_gr . '</option>';
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador Aulas Virtuales</title>
    <link rel="stylesheet" href="css/stylesheet.css?v<?php echo date('YmdHis') ?>" />
    <link rel="stylesheet" href="css/materialize.css" />
    <link href="css/noty.css" rel="stylesheet">
    <link href="css/relax.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <header>
        <div class="Virtual-top">
            <div class="Virtual-top-int">
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <a href="javascript:void(0)" id="Drop" class="Menu-drop"><img src="assets/images/menu_hamburgesa.png" class="Menu-ham"></a>
                        <h2 class="Titulo-mediano">Administrador de Aulas</h2>
                    </div>
                </div>
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="Virtual-contenedor-principal">
            <div class="Virtual-contenedor-principal-izq Virtual-contenedor-principal-izq-min">
                <?php include("menu-leftadmin.php") ?>
            </div>
            <div class="Virtual-contenedor-principal-der">
                <div class="Virtual-contenedor-principal-der-int">
                    <div class="Virtual-contenedor-principal-titulo">
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <h2 class="Titulo-seccion">Matricular por grupo de estudiantes</h2>
                        </div>
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <div class="Flotante-crear">
                            </div>
                        </div>
                    </div>
                    <section>
                        <div class="Texto-introductorio">
                            <p>En este módulo podrá realizar matriculas de forma masiva.<br> <strong>Nota:</strong> debe tener en cuenta que el límite de estudiantes lo determina la capacidad habilitada por grupo y tenga en cuenta que si va a matricular en más de un curso todos deben tener como mínimo la misma cantidad de cupos habilitados.</p>
                        </div>
                    </section>
                    <form id="nuevo">
                        <section>
                            <div class="Contendor-filtros">
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-cuatro input-field col">
                                        <select name="matricula[programa]" class="select-programa" required>
                                            <option value="" disabled selected>Seleccionar</option>
                                            <?php echo $ls_programas ?>
                                        </select>
                                        <label>Programa</label>
                                    </div>
                                </div>
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-cuatro input-field col">
                                        <select name="matricula[curso][]" class="select-curso" required>
                                            <option value="" disabled selected>Seleccionar</option>
                                        </select>
                                        <label>Curso</label>
                                    </div>
                                    <div class="Contendor-frag F-cuatro input-field col">
                                        <select name="matricula[grupo][]" class="select-grupo" required>
                                            <option value="" disabled selected>Seleccionar</option>
                                        </select>
                                        <label>Grupo</label>
                                    </div>
                                    <div class="Contendor-frag F-cuatro input-field col">
                                    </div>
                                </div>
                                <div class="Temp-content-curso">
                                </div>
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-cuatro input-field col">
                                        <a class="btn-floating btn-large waves-effect waves-light blue-grey darken-4 Add-curso"><i class="material-icons">add</i></a>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <hr class="Divipro">
                        <section>
                            <div class="Texto-introductorio">
                                <p>A continuación ingrese el número de identificación por cada estudiante a matricular.</p>
                            </div>
                            <div class="Contenedor-formularios">
                                <div class="Contenedor-formularios-int">
                                    <table class="striped">
                                        <thead>
                                            <tr>
                                                <th>Nº</th>
                                                <th>Identificación</th>
                                                <th>Nombre</th>
                                                <th>Apellido</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody class="ls-estudiantes">
                                            <tr class="Temp-content-estudiante">
                                                <td>1</td>
                                                <td>
                                                    <div class="input-field col m6">
                                                        <input id="cantidad" name="matricula[identificacion][]" type="number" class="validate Estudiante-identifica" required="">
                                                        <label for="cantidad">Numero de identificación</label>
                                                    </div>
                                                </td>
                                                <td class="Estudiante-nombre"></td>
                                                <td class="Estudiante-apellido"></td>
                                                <td>
                                                    <a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons Del-estudiante">delete</i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="Contenedor-formularios">
                                    <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                        <div class="Contendor-frag F-cuatro input-field col">
                                            <a class="btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons Add-estudiante">add</i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="Contenedor-formularios">
                                    <div class="Contenedor-formularios-int">
                                        <div class="Texto-introductorio">
                                            <p>Verifica que la información sea correcta antes de guardar y recuerda que puede modificarlo desde el módulo de Matrículas. </p>
                                        </div>
                                    </div>
                                    <div class="Contenedor-formularios Forms-virtual">
                                        <div class="Contenedor-formularios-int">
                                            <div class="Contendor-frag F-tres">
                                                <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                                    <input type="submit" class="waves-button-input Texto-blanco" value="GUARDAR" name="">
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </div>
    </section>
     <div style="display: none" id="cursos-oculto">
        <select>
            <?php echo $ls_cursos; ?>
        </select>
    </div>
    <div style="display: none" id="grupos-oculto">
        <select>
            <?php echo $ls_grupos; ?>
        </select>
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/noty.min.js"></script>
    <script src="js/date-espanol.js"></script>
    <script src="js/menu-slide.js"></script>
     <script src="js/admin-matricula-grupo.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
