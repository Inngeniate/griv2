<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: ../admin');

require_once "libs/conexion.php";
$ls_programas = '';


$programas = $db
    ->objectBuilder()->get('programas');

foreach ($programas as $programa) {
    $ls_programas .= '<option value="' . $programa->Id_pr . '">' . $programa->nombre_pr . '</option>';
}

$ls_ans = '';

for ($i= 2018; $i < 2027; $i++) {
   $ls_ans .= '<option value="' . $i . '">' . $i . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador Aulas Virtuales</title>
    <link rel="stylesheet" href="css/stylesheet.css?v<?php echo date('YmdHis') ?>" />
    <link rel="stylesheet" href="css/materialize.css" />
    <link href="css/noty.css" rel="stylesheet">
    <link href="css/relax.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <header>
        <div class="Virtual-top">
            <div class="Virtual-top-int">
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <a href="javascript:void(0)" id="Drop" class="Menu-drop"><img src="assets/images/menu_hamburgesa.png" class="Menu-ham"></a>
                        <h2 class="Titulo-mediano">Administrador de Aulas</h2>
                    </div>
                </div>
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <!-- <a href="" class="Menu-drop"><i class="icon-user"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="Virtual-contenedor-principal">
            <div class="Virtual-contenedor-principal-izq Virtual-contenedor-principal-izq-min">
                <?php include("menu-leftadmin.php") ?>
            </div>
            <div class="Virtual-contenedor-principal-der">
                <div class="Virtual-contenedor-principal-der-int">

                    <div class="Virtual-contenedor-principal-titulo">
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <h2 class="Titulo-seccion">Cortes por Programa</h2>
                        </div>
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <div class="Flotante-crear">
                                <a href="#" data-target="crear" class="modal-trigger btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons">add</i></a>
                            </div>
                        </div>
                    </div>

                    <?php include("menu-supadmin-tab-programas.php"); ?>
                    <section>
                        <div class="Contendor-filtros">
                            <form id="busqueda">
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <select id="Programa-bus">
                                                <option value="" selected>Seleccionar</option>
                                                <?php echo $ls_programas; ?>
                                            </select>
                                            <label>Programa</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <select id="Periodo-bus">
                                                <option value="" selected>Seleccionar</option>
                                                <?php echo $ls_ans ?>
                                            </select>
                                            <label>Periodo</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                            <input type="submit" class="waves-button-input" value="Buscar" name="">
                                        </i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <section>
                        <div class="Contenedor-tabla">
                            <div class="Contenedor-tabla-int">
                                <table class="Table-virtual striped">
                                    <thead>
                                        <tr>
                                            <th>Año</th>
                                            <th>Nombre del programa</th>
                                            <th>Numero de cortes</th>
                                            <th>Porcentaje por corte</th>
                                            <th colspan="2" class="center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="Listado-cortes">
                                    </tbody>
                                </table>
                                <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                                    <div class="preloader-wrapper big active">
                                        <div class="spinner-layer">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="gap-patch">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="listado-paginacion">
                                    <ul class="pagination">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <div id="crear" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Crear Cortes</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="nuevo">
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col m6">
                                    <select name="corte[periodo]" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php echo $ls_ans ?>
                                    </select>
                                    <label>Seleccionar año</label>
                                </div>
                                <div class="input-field col m6">
                                    <select name="corte[programa]" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php echo $ls_programas; ?>
                                    </select>
                                    <label>Seleccionar programa</label>
                                </div>
                                <div class="input-field col m6">
                                    <a class="waves-effect waves-light btn Menos-corte"><i class="material-icons left"></i>-</a>
                                    <input name="corte[cantidad]" type="number" value="1" min="1" step="1" readonly="" class="Cantidad-corte" required="" style="width: 65%; text-align:center;">
                                    <label for="cantidad">Cantidad</label>
                                    <a class="waves-effect waves-light btn Mas-corte"><i class="material-icons left"></i>+</a>
                                </div>
                            </div>
                            <div class="row Div-pocentajes">
                                <div class="input-field col m6 Content-porcentaje">
                                    <input id="porcentaje" name="corte[porcentaje][]" type="number" class="validate" required="">
                                    <label for="porcentaje">Porcentaje</label>
                                </div>
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Para editar -->
    <div id="editar" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Editar Cortes</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="editado">
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col m6">
                                    <select name="corte[periodo]" id="select-periodo-ed" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php echo $ls_ans ?>
                                    </select>
                                    <label>Seleccionar año</label>
                                </div>
                                <div class="input-field col m6">
                                    <select name="corte[programa]" id="select-programa-ed" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php echo $ls_programas; ?>
                                    </select>
                                    <label>Seleccionar programa</label>
                                </div>
                                <div class="input-field col m6">
                                    <a class="waves-effect waves-light btn Menos-corte"><i class="material-icons left"></i>-</a>
                                    <input name="corte[cantidad]" type="number" value="1" min="1" step="1" readonly="" class="Cantidad-corte" id="cantidad-ed" required="" style="width: 65%; text-align:center;">
                                    <label for="cantidad">Cantidad</label>
                                    <a class="waves-effect waves-light btn Mas-corte"><i class="material-icons left"></i>+</a>
                                </div>
                            </div>
                            <div class="row Div-pocentajes">
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/menu-slide.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/noty.min.js"></script>
    <script src="js/admin-cortes.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
