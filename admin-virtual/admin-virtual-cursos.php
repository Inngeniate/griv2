<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: ../admin');

require_once "libs/conexion.php";
$ls_programas = '';
$ls_cursos = '';

$programas = $db
    ->objectBuilder()->get('programas');

foreach ($programas as $programa) {
    $ls_programas .= '<option value="' . $programa->Id_pr . '">' . $programa->nombre_pr . '</option>';
}

$cursos = $db
    ->objectBuilder()->get('cursos');

foreach ($cursos as $curso) {
    $ls_cursos .= '<option value="' . $curso->Id_cu . '" class="Pr-' . $curso->Id_pr . '" >' . $curso->nombre_cu . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador Aulas Virtuales</title>
    <link rel="stylesheet" href="css/stylesheet.css" />
    <link rel="stylesheet" href="css/materialize.css" />
    <link href="css/noty.css" rel="stylesheet">
    <link href="css/relax.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <header>
        <div class="Virtual-top">
            <div class="Virtual-top-int">
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <a href="javascript:void(0)" id="Drop" class="Menu-drop"><img src="assets/images/menu_hamburgesa.png" class="Menu-ham"></a>
                        <h2 class="Titulo-mediano">Administrador de Aulas</h2>
                    </div>
                </div>
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <!-- <a href="" class="Menu-drop"><i class="icon-user"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="Virtual-contenedor-principal">

            <div class="Virtual-contenedor-principal-izq Virtual-contenedor-principal-izq-min">
                <?php include("menu-leftadmin.php") ?>
            </div>

            <div class="Virtual-contenedor-principal-der">
                <div class="Virtual-contenedor-principal-der-int">

                    <div class="Virtual-contenedor-principal-titulo">
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <h2 class="Titulo-seccion">Cursos</h2>
                        </div>
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <div class="Flotante-crear">
                                <a href="#" data-target="crear" class="modal-trigger btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons">add</i></a>
                            </div>
                        </div>
                    </div>
                    <?php include("menu-supadmin-tab-programas.php"); ?>

                    <section>
                        <div class="Contendor-filtros">
                            <form id="busqueda">
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contenedor-frag F-tres">
                                        <div class="input-field">
                                            <select id="Programa-bus">
                                                <option value="" selected>Seleccionar</option>
                                                <?php echo $ls_programas; ?>
                                            </select>
                                            <label>Programa</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <div class="input-field">
                                            <input id="Nombre-bus" type="text">
                                            <label for="Nombre-bus" class="">Buscar curso por nombre</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-tres">
                                        <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                            <input type="submit" class="waves-button-input" value="Buscar" name="">
                                        </i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>

                    <section>
                        <div class="Contenedor-tabla">
                            <div class="Contenedor-tabla-int">
                                <table class="Table-virtual striped">
                                    <thead>
                                        <tr>
                                            <th>Cod</th>
                                            <th>Programa</th>
                                            <th>Curso</th>
                                            <th>Descripción</th>
                                            <th>Prerrequisito</th>
                                            <th>Duración</th>
                                            <th>Estado</th>
                                            <th colspan="2" class="center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="Listado-cursos">
                                    </tbody>
                                </table>
                                <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                                    <div class="preloader-wrapper big active">
                                        <div class="spinner-layer">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="gap-patch">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="listado-paginacion">
                                    <ul class="pagination">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <div id="crear" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Crear Curso</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="nuevo">
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col m6">
                                    <select name="curso[programa]" class="select-programas" required="">
                                        <option value="" disabled selected>Seleccionar</option>
                                        <?php echo $ls_programas; ?>
                                    </select>
                                    <label>Programa</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="nombre" name="curso[nombre]" type="text" class="validate" required="">
                                    <label for="nombre">Nombre del curso</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="duracion-cr" name="curso[duracion]" type="text" class="validate" required="">
                                    <label for="duracion-cr">Duración</label>
                                </div>
                                <div class="input-field col m6">
                                    <select name="curso[estado]" required="">
                                        <option value="" disabled selected>Seleccionar</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <label>Estado</label>
                                </div>
                                <div class="input-field col m6">
                                    <select class="prerequisito" required="">
                                        <option value="" disabled selected>Seleccionar</option>
                                        <option value="0">No</option>
                                        <option value="1">Si</option>
                                    </select>
                                    <label>Tiene Prerrequisito</label>
                                </div>
                                <div class="input-field col m6">
                                    <select name="curso[prerrequisitos][]" class="select-precursos" disabled multiple>
                                        <option value="" disabled selected>Seleccionar</option>
                                    </select>
                                    <label>Curso</label>
                                </div>
                            </div>
                            <div class="input-field">
                                <textarea id="descripcion-cr" name="curso[descripcion]" class="materialize-textarea validate" required=""></textarea>
                                <label for="descripcion-cr">Descripción</label>
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Para editar -->
    <div id="editar" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Editar Curso</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="editado">
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col m6">
                                    <select name="curso[programa]" id="programa-cu-ed" class="select-programas" required="">
                                        <option value="" disabled selected>Seleccionar</option>
                                        <?php echo $ls_programas; ?>
                                    </select>
                                    <label>Programa</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="nombre-cu-ed" name="curso[nombre]" type="text" class="validate" required="">
                                    <label for="nombre-cu-ed">Nombre del curso</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="duracion-cu-ed" name="curso[duracion]" type="text" class="validate" required="">
                                    <label for="duracion-cu-ed">Duración</label>
                                </div>
                                <div class="input-field col m6">
                                    <select id="estado-cu-ed" name="curso[estado]" required="">
                                        <option value="" disabled selected>Seleccionar</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <label>Estado</label>
                                </div>
                                <div class="input-field col m6">
                                    <select class="prerequisito" id="prerequisito-ed" required="">
                                        <option value="" disabled selected>Seleccionar</option>
                                        <option value="0">No</option>
                                        <option value="1">Si</option>
                                    </select>
                                    <label>Tiene Prerrequisito</label>
                                </div>
                                <div class="input-field col m6">
                                    <select id="prerrequisitos-cu-ed" name="curso[prerrequisitos][]" class="select-precursos" disabled multiple>
                                        <option value="" disabled selected>Seleccionar</option>
                                    </select>
                                    <label>Curso</label>
                                </div>
                            </div>
                            <div class="input-field">
                                <textarea id="descripcion-cu-ed" name="curso[descripcion]" class="materialize-textarea validate" required=""></textarea>
                                <label for="descripcion-cu-ed">Descripción</label>
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="display: none" id="cursos-oculto">
        <select>
            <?php echo $ls_cursos; ?>
        </select>
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/menu-slide.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/noty.min.js"></script>
    <script src="js/admin-cursos.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
