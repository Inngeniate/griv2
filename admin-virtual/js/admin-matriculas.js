$(document).ready(function () {
    $('.modal').modal();
    $('select').formSelect();
    var identificacion = '',
        programa = '';

    listado(1);

    function listado(pg) {
        $('#Listado-matriculas').empty().fadeOut(0);
        $.post('libs/ac_matriculas', {
            'matricula[opc]': 'Matricula-listado',
            'matricula[identificacion]': identificacion,
            'matricula[programa]': programa,
            'matricula[pagina]': pg
        }, function (data) {
            $('.listado-loader').hide();
            $('#Listado-matriculas').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('#Listado-matriculas').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        programa = $('#Programa-bus').val();
        identificacion = $('#Ident-bus').val();
        listado(1);
    });

});
