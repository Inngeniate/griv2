$(document).ready(function () {
    $('.modal').modal();
    $('select').formSelect();
    var bsq_nombre = '',
        bsq_programa = '';
    bsq_curso = '';

    listado(1);

    $('.select-programas').on('change', function () {
        programa_selected = $(this).val();
        select_cursos = $(this).closest('form').find('.select-cursos');
        $('option:not(:first)', select_cursos).remove();
        select_cursos.formSelect('destroy');
        $.each($('option', '#cursos-oculto'), function () {
            if ($(this).hasClass('Pr-' + programa_selected)) {
                $(this).clone().appendTo(select_cursos);
            }
        });

        select_cursos.formSelect();
    });

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        select_cursos = $(this).find('.select-cursos');
        data = $(this).serializeArray();
        data.push({
            name: 'modulo[opc]',
            value: 'Modulo-nuevo'
        });
        $.post('libs/ac_modulos', data, function (data) {
            if (data.status == true) {
                $('input', '#nuevo').not('[type=submit]').val('');
                $("select", '#nuevo').val('').attr('selected', true);
                $('textarea', '#nuevo').val('');
                select_cursos.prop('selectedIndex', 0);
                $('option:not(:first)', select_cursos).remove();
                select_cursos.formSelect('destroy');
                window.M.updateTextFields();
                $('select').formSelect();
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_nombre = $('#Nombre-bus').val();
        bsq_programa = $('#Programa-bus').val();
        bsq_curso = $('#Curso-bus').val();
        listado(1);
    });

    $('body').on('click', '.Btn-editar', function () {
        modulo_editar = $(this).closest('tr').prop('id');
        data = $(this).serializeArray();
        data.push({
            name: 'modulo[opc]',
            value: 'Modulo-info'
        }, {
            name: 'modulo[idmodulo]',
            value: modulo_editar
        });
        $.post('libs/ac_modulos', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $('#programa-mo-ed').val(data.info.Id_pr).change();
                $('#curso-mo-ed').val(data.info.Id_cu);
                $('#estado-mo-ed').val(data.info.estado_mo);
                $('#nombre-ed').val(data.info.nombre_mo);
                $('#cantidad-ed').val(data.info.actividades_mo);
                $('#descripcion-ed').val(data.info.descripcion_mo);
                window.M.updateTextFields();
                $('select').formSelect();
                $('#editar').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('#editado').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'modulo[opc]',
            value: 'Modulo-editar'
        }, {
            name: 'modulo[idmodulo]',
            value: modulo_editar
        });
        $.post('libs/ac_modulos', data, function (data) {
            if (data.status == true) {
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

     $('body').on('click', '.Btn-bloquear', function () {
        modulo_activar = $(this).closest('tr').prop('id');
        if ($(this).hasClass('Activar')) {
            tipo_activacion = 'Activar';
        } else {
            tipo_activacion = 'Inactivar';
        }
        var n = new Noty({
            text: 'Desea ' + tipo_activacion + ' el modulo?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('libs/ac_modulos', {
                        'modulo[opc]': 'Modulo-activacion',
                        'modulo[idmodulo]': modulo_activar,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, {id: 'button1', 'data-status': 'ok'}),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-modulos').empty().fadeOut(0);
        $.post('libs/ac_modulos', {
            'modulo[opc]': 'Modulo-listado',
            'modulo[programa]': bsq_programa,
            'modulo[curso]': bsq_curso,
            'modulo[nombre]': bsq_nombre,
            'modulo[pagina]': pg
        }, function (data) {
            $('#Listado-modulos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-modulos').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
