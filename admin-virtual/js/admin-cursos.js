$(document).ready(function () {
    $('.modal').modal();
    $('select').formSelect();
    var bsq_nombre = '',
        bsq_programa = '';

    listado(1);

    $('.select-programas').on('change', function () {
        programa_selected = $(this).val();
        select_cursos = $(this).closest('form').find('.select-precursos');
        $('option:not(:first)', select_cursos).remove();
        select_cursos.formSelect('destroy');
        $.each($('option', '#cursos-oculto'), function () {
            if ($(this).hasClass('Pr-' + programa_selected)) {
                $(this).clone().appendTo(select_cursos);
            }
        });

        select_cursos.formSelect();
    });

    $('.prerequisito').on('change', function () {
        select_cursos = $(this).closest('form').find('.select-precursos');
        if ($(this).val() == 1) {
            select_cursos.attr('disabled', false);
        } else {
            select_cursos.prop('selectedIndex', 0);
            select_cursos.attr('disabled', true);
        }

        select_cursos.formSelect();
    });

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        select_cursos = $(this).find('.select-precursos');
        data = $(this).serializeArray();
        data.push({
            name: 'curso[opc]',
            value: 'Curso-nuevo'
        });
        $.post('libs/ac_cursos', data, function (data) {
            if (data.status == true) {
                $('input', '#nuevo').not('[type=submit]').val('');
                $("select", '#nuevo').val('').attr('selected', true);
                $('textarea', '#nuevo').val('');
                select_cursos.prop('selectedIndex', 0);
                select_cursos.attr('disabled', true);
                $('option:not(:first)', select_cursos).remove();
                select_cursos.formSelect('destroy');
                window.M.updateTextFields();
                $('select').formSelect();
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_nombre = $('#Nombre-bus').val();
        bsq_programa = $('#Programa-bus').val();
        listado(1);
    });

    $('body').on('click', '.Btn-editar', function () {
        curso_editar = $(this).closest('tr').prop('id');
        data = $(this).serializeArray();
        data.push({
            name: 'curso[opc]',
            value: 'Curso-info'
        }, {
            name: 'curso[idcurso]',
            value: curso_editar
        });
        $.post('libs/ac_cursos', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $('#programa-cu-ed').val(data.info.Id_pr).change();
                $('#nombre-cu-ed').val(data.info.nombre_cu);
                $('#duracion-cu-ed').val(data.info.duracion_cu);
                $('#estado-cu-ed').val(data.info.estado_cu);
                if (data.info.prerrequisito_cu != '') {
                    $('#prerequisito-ed').val(1).change();
                    prerequiere = data.info.prerrequisito_cu.split(',');
                    $.each(prerequiere, function (i, dat) {
                        $.each($('#prerrequisitos-cu-ed option'), function () {
                            if ($(this).val() == dat) {
                                $(this).attr('selected', true);
                            }
                        });
                    });
                } else {
                    $('#prerequisito-ed').val(0).change();
                }
                $('#descripcion-cu-ed').val(data.info.descripcion_cu);
                window.M.updateTextFields();
                $('select').formSelect();
                $('#editar').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('#editado').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'curso[opc]',
            value: 'Curso-editar'
        }, {
            name: 'curso[idcurso]',
            value: curso_editar
        });
        $.post('libs/ac_cursos', data, function (data) {
            if (data.status == true) {
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-bloquear', function () {
        curso_activar = $(this).closest('tr').prop('id');
        if ($(this).hasClass('Activar')) {
            tipo_activacion = 'Activar';
        } else {
            tipo_activacion = 'Inactivar';
        }
        var n = new Noty({
            text: 'Desea ' + tipo_activacion + ' el curso?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('libs/ac_cursos', {
                        'curso[opc]': 'Curso-activacion',
                        'curso[idcurso]': curso_activar,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, {id: 'button1', 'data-status': 'ok'}),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-cursos').empty().fadeOut(0);
        $.post('libs/ac_cursos', {
            'curso[opc]': 'Curso-listado',
            'curso[programa]': bsq_programa,
            'curso[nombre]': bsq_nombre,
            'curso[pagina]': pg
        }, function (data) {
            $('#Listado-cursos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-cursos').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }
});
