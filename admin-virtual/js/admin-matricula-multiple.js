$(document).ready(function () {
    $('select').formSelect();
    $('.modal').modal();
    estudiante = '';
    programa = '';
    curso = '';
    identificaciones = [];


    $("#cargar-archivo").on("submit", function (e) {
        e.preventDefault();
        $("#dvExcel").empty();
        //Reference the FileUpload element.
        var fileUpload = $("#fileUpload")[0];

        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();

                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer(fileUpload.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
        }
    });

    function ProcessExcel(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        //Create a HTML Table element.
        var table = $("<table />");
        table[0].border = "1";

        //Add the header row.
        var row = $(table[0].insertRow(-1));

        //Add the header cells.
        var headerCell = $("<th />");
        headerCell.html("Identificacion");
        row.append(headerCell);

        var headerCell = $("<th />");
        headerCell.html("Apellido");
        row.append(headerCell);

        var headerCell = $("<th />");
        headerCell.html("Correo electrónico");
        row.append(headerCell);

        var headerCell = $("<th />");
        headerCell.html("Teléfono");
        row.append(headerCell);

        data = [];

        data.push({
            name: 'matricula[opc]',
            value: 'Estudiantes2',
        });


        //Add the data rows from Excel file.
        for (var i = 0; i < excelRows.length; i++) {
            data.push({
                name: 'matricula[identificacion][]',
                value: excelRows[i].Identificacion,
            });
        }

        $.getJSON("libs/ac_matriculas", data, function (data) {
            if (data.status == true) {
                //Add the data row.
                $.each(data.info, function (i, dat) {
                    row = $(table[0].insertRow(-1));

                    var cell = $("<td />");
                    cell.html(dat.nombre);
                    row.append(cell);

                    var cell = $("<td />");
                    cell.html(dat.apellido);
                    row.append(cell);

                    var cell = $("<td />");
                    cell.html(dat.correo);
                    row.append(cell);

                    var cell = $("<td />");
                    cell.html(dat.telefono);
                    row.append(cell);

                    identificaciones.push(dat.identificacion);
                });


                //  estudiante = data.info.identificacion;
            } else {
                notification('error', data.msg);
            }
        });

        var dvExcel = $("#dvExcel");
        dvExcel.html("");
        dvExcel.append(table);
    };



    $('.Add-curso').on('click', function () {
        $('.Temp-content').append('<div class="Contenedor-formularios">' +
            '<div class="Contenedor-formularios-int">' +
            '<div class="Contendor-frag F-tres">' +
            '<div class="input-field">' +
            '<select name="matricula[curso][]" class="select-curso temp-curso">' +
            '<option value="" selected>Seleccionar curso</option>' +
            '</select>' +
            '<label>Curso</label>' +
            '</div>' +
            '</div>' +
            '<div class="Contendor-frag F-tres">' +
            '<div class="input-field">' +
            '<select name="matricula[grupo][]" class="select-grupo temp-grupo">' +
            '<option value="" selected>Seleccionar</option>' +
            '</select>' +
            '<label>Grupo</label>' +
            '</div>' +
            '</div>' +
            '<div class="Contendor-frag F-tres">' +
            '<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons Del-curso">delete</i></a>' +
            '</div>' +
            '</div>' +
            '</div>');

        $('select').formSelect();
        select_cursos = $('.temp-curso');
        select_grupo = $('.temp-grupo');
        $('option:not(:first)', select_grupo).remove();
        select_grupo.formSelect('destroy');

        if (programa != '') {
            $.each($('#cursos-oculto option'), function () {
                if ($(this).hasClass('Pr-' + programa)) {
                    $(this).clone().appendTo(select_cursos);
                }
            });
        }

        $('select').formSelect();

    });

    $('body').on('click', '.Del-curso', function () {
        $(this).closest('.Contenedor-formularios').remove();
    });

    $('.select-programa').on('change', function () {
        programa = $(this).val();
        $.each($('.select-curso'), function () {
            select_cursos = $(this);
            $('option:not(:first)', select_cursos).remove();
            select_cursos.formSelect('destroy');
            select_grupo = select_cursos.closest('.Contenedor-formularios').find('.select-grupo');
            $('option:not(:first)', select_grupo).remove();
            select_grupo.formSelect('destroy');

            if (programa != '') {
                $.each($('#cursos-oculto option'), function () {
                    if ($(this).hasClass('Pr-' + programa)) {
                        $(this).clone().appendTo(select_cursos);
                    }
                });
            }
        });

        $('select').formSelect();
    });

    $('body').on('change', '.select-curso', function () {
        curso = $(this).val();
        select_grupo = $(this).closest('.Contenedor-formularios').find('.select-grupo');
        $('option:not(:first)', select_grupo).remove();
        select_grupo.formSelect('destroy');
        if (programa != '' && curso != '') {
            $.each($('#grupos-oculto option'), function () {
                if ($(this).hasClass('Pr-' + programa) && $(this).hasClass('Cu-' + curso)) {
                    $(this).clone().appendTo(select_grupo);
                }
            });
        }

        select_grupo.formSelect();
    });

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'matricula[opc]',
            value: 'Matricula-multiple'
        });

        $.each(identificaciones, function (i, dat) {
            data.push({
                name: 'matricula[estudiante][]',
                value: dat
            });
        });

        select_cursos = $(this).find('.select-curso');
        select_grupo = $(this).find('.select-grupo');

        $.post('libs/ac_matriculas', data, function (data) {
            if (data.status == true) {
                $('.Temp-content').empty();
                estudiante = '';
                $('input', '#busqueda').not('[type=submit]').val('');
                $('.Estudiante-nombre').html('');
                $('.Estudiante-apellido').html('');
                $('.Estudiante-correo').html('');
                $('.Estudiante-telefono').html('');
                $("select", '#nuevo').val('').attr('selected', true);
                select_cursos.prop('selectedIndex', 0);
                select_grupo.prop('selectedIndex', 0);
                $('option:not(:first)', select_cursos).remove();
                $('option:not(:first)', select_grupo).remove();
                select_cursos.formSelect('destroy');
                select_grupo.formSelect('destroy');
                $('select').formSelect();
                identificaciones = [];
                $("#fileUpload").val('');
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
