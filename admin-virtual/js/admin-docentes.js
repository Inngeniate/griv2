$(document).ready(function () {
    $('.modal').modal();
    $('select').formSelect();
    var bsq_nombre = '',
        bsq_identifica = '',
        docentes_editar = '';

    listado(1);

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'docente[opc]',
            value: 'Docente-nuevo'
        });
        $.post('libs/ac_docentes', data, function (data) {
            if (data.status == true) {
                $('input', '#nuevo').not('[type=submit]').val('');
                $("select option[value='']", '#nuevo').attr('selected', true);
                $('textarea', '#nuevo').val('');
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-editar', function () {
        docentes_editar = $(this).closest('tr').prop('id');
        data = $(this).serializeArray();
        data.push({
            name: 'docente[opc]',
            value: 'Docente-info'
        }, {
            name: 'docente[iddocente]',
            value: docentes_editar
        });
        $.post('libs/ac_docentes', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $('#identifica-edi').val(data.info.identificacion_do);
                $('#email-edi').val(data.info.correo_do);
                $('#nombre-edi').val(data.info.nombre_do);
                $('#apellido-edi').val(data.info.apellido_do);
                $('#telefono-edi').val(data.info.telefono_do);
                $('#estado-edi').val(data.info.estado_do);
                window.M.updateTextFields();
                M.FormSelect.init(document.querySelectorAll("select"));
                $('#editar').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('#editado').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'docente[opc]',
            value: 'Docente-editar'
        }, {
            name: 'docente[iddocente]',
            value: docentes_editar
        });
        $.post('libs/ac_docentes', data, function (data) {
            if (data.status == true) {
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-bloquear', function () {
        docente_activar = $(this).closest('tr').prop('id');
        if ($(this).hasClass('Activar')) {
            tipo_activacion = 'Activar';
        } else {
            tipo_activacion = 'Inactivar';
        }
        var n = new Noty({
            text: 'Desea ' + tipo_activacion + ' el docente?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('libs/ac_docentes', {
                        'docente[opc]': 'Docente-activacion',
                        'docente[iddocente]': docente_activar,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, { id: 'button1', 'data-status': 'ok' }),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_nombre = $('#Nombre-bus').val();
        bsq_identifica = $('#Identi-bus').val();
        listado(1);
    });

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-docentes').empty().fadeOut(0);
        $.post('libs/ac_docentes', {
            'docente[opc]': 'Docente-listado',
            'docente[nombre]': bsq_nombre,
            'docente[identifica]': bsq_identifica,
            'docente[pagina]': pg
        }, function (data) {
            $('#Listado-docentes').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-docentes').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
