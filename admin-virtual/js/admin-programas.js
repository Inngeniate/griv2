$(document).ready(function () {
    $('.modal').modal();
    $('select').formSelect();
    var bsq_nombre = '',
        programa_editar = '';

    listado(1);

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'programa[opc]',
            value: 'Programa-nuevo'
        });
        $.post('libs/ac_programas', data, function (data) {
            if (data.status == true) {
                $('input', '#nuevo').not('[type=submit]').val('');
                $("select option[value='']", '#nuevo').attr('selected', true);
                $('textarea', '#nuevo').val('');
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function listado(pg) {
        $('#Listado-programas').empty().fadeOut(0);
        $.post('libs/ac_programas', {
            'programa[opc]': 'Programa-listado',
            'programa[nombre]': bsq_nombre,
            'programa[pagina]': pg
        }, function (data) {
            $('#Listado-programas').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('#Listado-programas').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_nombre = $('#Nombre-bus').val();
        listado(1);
    });

    $('body').on('click', '.Btn-editar', function () {
        programa_editar = $(this).closest('tr').prop('id');
        data = $(this).serializeArray();
        data.push({
            name: 'programa[opc]',
            value: 'Programa-info'
        }, {
            name: 'programa[idprograma]',
            value: programa_editar
        });
        $.post('libs/ac_programas', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $('#nombre-edi').val(data.info.nombre_pr);
                $('#duracion-pr-edi').val(data.info.duracion_pr);
                $('#estado-edi').val(data.info.estado_pr);
                $('#descripcion-pr-edi').val(data.info.descripcion_pr);
                window.M.updateTextFields();
                M.FormSelect.init(document.querySelectorAll("select"));
                $('#editar').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('#editado').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'programa[opc]',
            value: 'Programa-editar'
        }, {
            name: 'programa[idprograma]',
            value: programa_editar
        });
        $.post('libs/ac_programas', data, function (data) {
            if (data.status == true) {
                listado(1);
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Btn-bloquear', function () {
        programa_activar = $(this).closest('tr').prop('id');
        if ($(this).hasClass('Activar')) {
            tipo_activacion = 'Activar';
        } else {
            tipo_activacion = 'Inactivar';
        }
        var n = new Noty({
            text: 'Desea ' + tipo_activacion + ' el programa?',
            buttons: [
                Noty.button('Si', 'btn btn-success', function () {
                    $.post('libs/ac_programas', {
                        'programa[opc]': 'Programa-activacion',
                        'programa[idprograma]': programa_activar,
                    }, function (data) {
                        n.close();
                        if (data.status == true) {
                            listado(1);
                        } else {
                            notification('error', data.msg);
                        }
                    }, 'json');
                }, {id: 'button1', 'data-status': 'ok'}),

                Noty.button('No', 'btn btn-error', function () {
                    n.close();
                })
            ],
            layout: 'center',
            theme: 'relax',
            modal: true,
        });
        n.show();
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-programas').empty().fadeOut(0);
        $.post('libs/ac_programas', {
            'programa[opc]': 'Programa-listado',
            'programa[nombre]': bsq_nombre,
            'programa[pagina]': pg
        }, function (data) {
            $('#Listado-programas').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-programas').fadeIn();
        }, 'json');
    }

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
