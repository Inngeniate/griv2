$(document).ready(function () {
    $('.modal').modal();
    $('select').formSelect();
    var bsq_programa = '',
        bsq_periodo = '';
    listado(1);

    $('.Mas-corte').on('click', function () {
        cortes = $(this).parent().find('.Cantidad-corte');
        cortes.val(Number(cortes.val()) + 1);

        $(this).closest('form').find('.Div-pocentajes').append('<div class="input-field col m6 Content-porcentaje">' +
            '<input id="porcentaje" name="corte[porcentaje][]" type="number" class="validate" required="">' +
            '<label for="porcentaje">Porcentaje</label>' +
            '</div>');
    });

    $('.Menos-corte').on('click', function () {
        cortes = $(this).parent().find('.Cantidad-corte');
        if (cortes.val() > 1) {
            cortes.val(Number(cortes.val()) - 1);
            $(this).closest('form').find('.Div-pocentajes')
            $('.Content-porcentaje').last().remove();
        }
    });

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        total_porcentaje = 0;
        $.each($('.Content-porcentaje', '#nuevo'), function () {
            total_porcentaje += Number($(this).find('input').val());
        });

        if (total_porcentaje == 100) {
            data = $(this).serializeArray();
            data.push({
                name: 'corte[opc]',
                value: 'Corte-nuevo'
            });
            $.post('libs/ac_cortes', data, function (data) {
                if (data.status == true) {
                    $('input', '#nuevo').not('[type=submit]').val('');
                    $("select", '#nuevo').val('').attr('selected', true);
                    window.M.updateTextFields();
                    listado(1);
                    notification('success', data.msg);
                } else {
                    notification('error', data.msg);
                }
            }, 'json');
        } else {
            notification('error', 'El total del porcentaje debe ser 100');
        }
    });

    $('body').on('click', '.Btn-editar', function () {
        $('.Div-pocentajes', '#editar').empty();
        corte_editar = $(this).closest('tr').prop('id');
        data = $(this).serializeArray();
        data.push({
            name: 'corte[opc]',
            value: 'Corte-info'
        }, {
            name: 'corte[idcorte]',
            value: corte_editar
        });
        $.post('libs/ac_cortes', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $('#select-periodo-ed').val(data.info.periodo);
                $('#select-programa-ed').val(data.info.programa);
                $('#cantidad-ed').val(data.info.cantidad);
                $.each(data.info.porcentajes, function (i, dat) {
                    $('.Div-pocentajes', '#editar').append('<div class="input-field col m6 Content-porcentaje">' +
                        '<input id="porcentaje" name="corte[porcentaje][]" type="number" class="validate" value="' + dat + '" required="">' +
                        '<label for="porcentaje">Porcentaje</label>' +
                        '</div>');
                });
                window.M.updateTextFields();
                $('select').formSelect();
                $('#editar').modal('open');
            } else if (data.status == false) {
                notification('error', data.msg);
            }
        }, 'json');
    });


    $('#editado').on('submit', function (e) {
        e.preventDefault();
        total_porcentaje = 0;
        $.each($('.Content-porcentaje', '#editar'), function () {
            total_porcentaje += Number($(this).find('input').val());
        });

        if (total_porcentaje == 100) {
            data = $(this).serializeArray();
            data.push({
                name: 'corte[opc]',
                value: 'Corte-editar'
            }, {
                name: 'corte[idcorte]',
                value: corte_editar
            });
            $.post('libs/ac_cortes', data, function (data) {
                if (data.status == true) {
                    listado(1);
                    notification('success', data.msg);
                } else {
                    notification('error', data.msg);
                }
            }, 'json');
        } else {
            notification('error', 'El total del porcentaje debe ser 100');
        }
    });


    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        bsq_programa = $('#Programa-bus').val();
        bsq_periodo = $('#Periodo-bus').val();
        listado(1);
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#Listado-cortes').empty().fadeOut(0);
        $.post('libs/ac_cortes', {
            'corte[opc]': 'Corte-listado',
            'corte[programa]': bsq_programa,
            'corte[periodo]': bsq_periodo,
            'corte[pagina]': pg
        }, function (data) {
            $('#Listado-cortes').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#Listado-cortes').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
