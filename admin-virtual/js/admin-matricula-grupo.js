$(document).ready(function () {
    $('select').formSelect();
    $('.modal').modal();
    programa = '';
    curso = '';

    $('.Add-curso').on('click', function () {
        $('.Temp-content-curso').append('<div class="Contendor-filtros-int Ali-inicial Forms-virtual">' +
            '<div class="Contendor-frag F-cuatro input-field col">' +
            '<select name="matricula[curso][]" class="select-curso temp-curso" required>' +
            '<option value="" disabled selected>Seleccionar</option>' +
            '</select>' +
            '<label>Curso</label>' +
            '</div>' +
            '<div class="Contendor-frag F-cuatro input-field col">' +
            '<select name="matricula[grupo][]" class="select-grupo temp-grupo" required>' +
            '<option value="" disabled selected>Seleccionar</option>' +
            '</select>' +
            '<label>Grupo</label>' +
            '</div>' +
            '<div class="Contendor-frag F-cuatro input-field col">' +
            '<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons Del-curso">delete</i></a>' +
            '</div>' +
            '</div>');

        $('select').formSelect();
        select_cursos = $('.temp-curso');
        select_grupo = $('.temp-grupo');
        $('option:not(:first)', select_grupo).remove();
        select_grupo.formSelect('destroy');

        if (programa != '') {
            $.each($('#cursos-oculto option'), function () {
                if ($(this).hasClass('Pr-' + programa)) {
                    $(this).clone().appendTo(select_cursos);
                }
            });
        }

        $('select').formSelect();

    });

    $('body').on('click', '.Del-curso', function () {
        $(this).closest('.Contendor-filtros-int').remove();
    });

    $('.select-programa').on('change', function () {
        programa = $(this).val();
        $.each($('.select-curso'), function () {
            select_cursos = $(this);
            $('option:not(:first)', select_cursos).remove();
            select_cursos.formSelect('destroy');
            select_grupo = select_cursos.closest('.Contendor-filtros-int').find('.select-grupo');
            $('option:not(:first)', select_grupo).remove();
            select_grupo.formSelect('destroy');

            if (programa != '') {
                $.each($('#cursos-oculto option'), function () {
                    if ($(this).hasClass('Pr-' + programa)) {
                        $(this).clone().appendTo(select_cursos);
                    }
                });
            }
        });

        $('select').formSelect();
    });

    $('body').on('change', '.select-curso', function () {
        curso = $(this).val();
        select_grupo = $(this).closest('.Contendor-filtros-int').find('.select-grupo');
        $('option:not(:first)', select_grupo).remove();
        select_grupo.formSelect('destroy');
        if (programa != '' && curso != '') {
            $.each($('#grupos-oculto option'), function () {
                if ($(this).hasClass('Pr-' + programa) && $(this).hasClass('Cu-' + curso)) {
                    $(this).clone().appendTo(select_grupo);
                }
            });
        }

        select_grupo.formSelect();
    });

    n_estudiante = 1;

    $('.Add-estudiante').on('click', function () {
        n_estudiante++;
        $('.ls-estudiantes').append('<tr class="Temp-content-estudiante">' +
            '<td>' + n_estudiante + '</td>' +
            '<td>' +
            '<div class="input-field col m6">' +
            '<input id="cantidad" name="matricula[identificacion][]" type="number" class="validate Estudiante-identifica" required="">' +
            '<label for="cantidad">Numero de identificación</label>' +
            '</div>' +
            '</td>' +
            '<td class="Estudiante-nombre"></td>' +
            '<td class="Estudiante-apellido"></td>' +
            '<td>' +
            '<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons Del-estudiante">delete</i></a>' +
            '</td>' +
            '</tr>');
    });

    $('body').on('click', '.Del-estudiante', function () {
        $(this).closest('.Temp-content-estudiante').remove();
        n_estudiante--;
    });



    $('body').on('keypress', '.Estudiante-identifica', function (e) {
        if (e.which == 13) {
            e.preventDefault();

            nombre = $(this).closest('tr').find('.Estudiante-nombre');
            apellido = $(this).closest('tr').find('.Estudiante-apellido');

            nombre.html('');
            apellido.html('');

            $.getJSON("libs/ac_matriculas", {
                'matricula[identificacion]': $(this).val(),
                'matricula[opc]': 'Estudiantes'
            }, function (data) {
                if (data.status == true) {
                    nombre.html(data.info.nombre);
                    apellido.html(data.info.apellido);
                } else {
                    notification('error', data.msg);
                }
            });
        }
    });

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'matricula[opc]',
            value: 'Matricula-grupo'
        });
        select_cursos = $(this).find('.select-curso');
        select_grupo = $(this).find('.select-grupo');
        $.post('libs/ac_matriculas', data, function (data) {
            if (data.status == true) {
                $('.Temp-content-curso').empty();
                $('.Temp-content-estudiante').remove();
                $("select", '#nuevo').val('').attr('selected', true);
                select_cursos.prop('selectedIndex', 0);
                select_grupo.prop('selectedIndex', 0);
                $('option:not(:first)', select_cursos).remove();
                $('option:not(:first)', select_grupo).remove();
                select_cursos.formSelect('destroy');
                select_grupo.formSelect('destroy');
                $('select').formSelect();
                n_estudiante = 1;
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
