$(document).ready(function () {
    $('select').formSelect();
    $('.modal').modal();
    estudiante = '';
    programa = '';
    curso = '';

    $('#busqueda').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'matricula[opc]',
            value: 'Estudiantes',
        });

        $('.Estudiante-nombre').html('');
        $('.Estudiante-apellido').html('');
        $('.Estudiante-correo').html('');
        $('.Estudiante-telefono').html('');

        $.getJSON("libs/ac_matriculas", data, function (data) {
            if (data.status == true) {
                estudiante = data.info.identificacion;
                $('.Estudiante-nombre').html(data.info.nombre);
                $('.Estudiante-apellido').html(data.info.apellido);
                $('.Estudiante-correo').html(data.info.correo);
                $('.Estudiante-telefono').html(data.info.telefono);
            } else {
                notification('error', data.msg);
            }
        });
    });

    $('.Add-curso').on('click', function () {
        $('.Temp-content').append('<div class="Contenedor-formularios">' +
            '<div class="Contenedor-formularios-int">' +
            '<div class="Contendor-frag F-tres">' +
            '<div class="input-field">' +
            '<select name="matricula[curso][]" class="select-curso temp-curso">' +
            '<option value="" selected>Seleccionar curso</option>' +
            '</select>' +
            '<label>Curso</label>' +
            '</div>' +
            '</div>' +
            '<div class="Contendor-frag F-tres">' +
            '<div class="input-field">' +
            '<select name="matricula[grupo][]" class="select-grupo temp-grupo">' +
            '<option value="" selected>Seleccionar</option>' +
            '</select>' +
            '<label>Grupo</label>' +
            '</div>' +
            '</div>' +
            '<div class="Contendor-frag F-tres">' +
            '<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons Del-curso">delete</i></a>' +
            '</div>' +
            '</div>' +
            '</div>');

        $('select').formSelect();
        select_cursos = $('.temp-curso');
        select_grupo = $('.temp-grupo');
        $('option:not(:first)', select_grupo).remove();
        select_grupo.formSelect('destroy');

        if (programa != '') {
            $.each($('#cursos-oculto option'), function () {
                if ($(this).hasClass('Pr-' + programa)) {
                    $(this).clone().appendTo(select_cursos);
                }
            });
        }

        $('select').formSelect();

    });

    $('body').on('click', '.Del-curso', function () {
        $(this).closest('.Contenedor-formularios').remove();
    });

    $('.select-programa').on('change', function () {
        programa = $(this).val();
        $.each($('.select-curso'), function () {
            select_cursos = $(this);
            $('option:not(:first)', select_cursos).remove();
            select_cursos.formSelect('destroy');
            select_grupo = select_cursos.closest('.Contenedor-formularios').find('.select-grupo');
            $('option:not(:first)', select_grupo).remove();
            select_grupo.formSelect('destroy');

            if (programa != '') {
                $.each($('#cursos-oculto option'), function () {
                    if ($(this).hasClass('Pr-' + programa)) {
                        $(this).clone().appendTo(select_cursos);
                    }
                });
            }
        });

        $('select').formSelect();
    });

    $('body').on('change', '.select-curso', function () {
        curso = $(this).val();
        select_grupo = $(this).closest('.Contenedor-formularios').find('.select-grupo');
        $('option:not(:first)', select_grupo).remove();
        select_grupo.formSelect('destroy');
        if (programa != '' && curso != '') {
            $.each($('#grupos-oculto option'), function () {
                if ($(this).hasClass('Pr-' + programa) && $(this).hasClass('Cu-' + curso)) {
                    $(this).clone().appendTo(select_grupo);
                }
            });
        }

        select_grupo.formSelect();
    });

    $('#nuevo').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        data.push({
            name: 'matricula[opc]',
            value: 'Matricula-estudiante'
        }, {
            name: 'matricula[estudiante]',
            value: estudiante
        });
        select_cursos = $(this).find('.select-curso');
        select_grupo = $(this).find('.select-grupo');
        $.post('libs/ac_matriculas', data, function (data) {
            if (data.status == true) {
                $('.Temp-content').empty();
                estudiante = '';
                $('input', '#busqueda').not('[type=submit]').val('');
                $('.Estudiante-nombre').html('');
                $('.Estudiante-apellido').html('');
                $('.Estudiante-correo').html('');
                $('.Estudiante-telefono').html('');
                $("select", '#nuevo').val('').attr('selected', true);
                select_cursos.prop('selectedIndex', 0);
                select_grupo.prop('selectedIndex', 0);
                $('option:not(:first)', select_cursos).remove();
                $('option:not(:first)', select_grupo).remove();
                select_cursos.formSelect('destroy');
                select_grupo.formSelect('destroy');
                $('select').formSelect();
                notification('success', data.msg);
            } else {
                notification('error', data.msg);
            }
        }, 'json');
    });

    function notification(t, m) {
        var n = new Noty({
            type: t,
            text: m,
            layout: 'center',
            theme: 'relax',
            modal: true,
            closeWith: ['click'],
            progressBar: false,
            timeout: 1500,
        });
        n.show();
    }

});
