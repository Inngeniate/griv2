<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: ../admin');
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador Aulas Virtuales</title>
    <link rel="stylesheet" href="css/stylesheet.css?v<?php echo date('YmdHis') ?>" />
    <link rel="stylesheet" href="css/materialize.css" />
    <link href="css/noty.css" rel="stylesheet">
    <link href="css/relax.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <header>
        <div class="Virtual-top">
            <div class="Virtual-top-int">
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <a href="javascript:void(0)" id="Drop" class="Menu-drop"><img src="assets/images/menu_hamburgesa.png" class="Menu-ham"></a>
                        <h2 class="Titulo-mediano">Administrador de Aulas</h2>
                    </div>
                </div>
                <div class="Virtual-top-int-sec">
                    <div class="Virtual-top-int-sec-cont">
                        <!-- <a href="" class="Menu-drop"><i class="icon-user"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="Virtual-contenedor-principal">
            <div class="Virtual-contenedor-principal-izq Virtual-contenedor-principal-izq-min">
                <?php include("menu-leftadmin.php") ?>
            </div>
            <div class="Virtual-contenedor-principal-der">
                <div class="Virtual-contenedor-principal-der-int">

                    <div class="Virtual-contenedor-principal-titulo">
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <h2 class="Titulo-seccion">Docentes</h2>
                        </div>
                        <div class="Virtual-contenedor-principal-titulo-int">
                            <div class="Flotante-crear">
                                <a href="#" data-target="crear" class="modal-trigger btn-floating btn-large waves-effect waves-light blue-grey darken-4"><i class="material-icons">add</i></a>
                            </div>
                        </div>
                    </div>
                    <section>
                        <div class="Contendor-filtros">
                            <form id="busqueda">
                                <div class="Contendor-filtros-int Ali-inicial Forms-virtual">
                                    <div class="Contendor-frag F-cuatro">
                                        <div class="input-field">
                                            <input id="Nombre-bus" type="text">
                                            <label for="Nombre-bus" class="">Buscar por nombre</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-cuatro">
                                        <div class="input-field">
                                            <input id="Identi-bus" type="text">
                                            <label for="Identi-bus" class="">Nº de Identificación</label>
                                        </div>
                                    </div>
                                    <div class="Contendor-frag F-cuatro">
                                        <i class="btn Btn-azul waves-effect waves-light btn-large Form-sub waves-input-wrapper" style="">
                                            <input type="submit" class="waves-button-input" value="Buscar" name="">
                                        </i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <section>
                        <div class="Contenedor-tabla">
                            <div class="Contenedor-tabla-int">
                                <table class="Table-virtual striped">
                                    <thead>
                                        <tr>
                                            <th>Identificación</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Email</th>
                                            <th>Teléfono</th>
                                            <th>Estado</th>
                                            <th colspan="2" class="center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="Listado-docentes">
                                    </tbody>
                                </table>
                                <div class="listado-loader" style="text-align: center; margin-top: 50px;">
                                    <div class="preloader-wrapper big active">
                                        <div class="spinner-layer">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="gap-patch">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="listado-paginacion">
                                    <ul class="pagination">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <div id="crear" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Crear Docente</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="nuevo">
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="email" name="docente[correo]" type="email" class="validate" required="">
                                    <label for="email">Email / Usuario</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="identifica" name="docente[identifica]" type="text" class="validate" required="">
                                    <label for="identifica">Nº Identificación / Contraseña</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="nombre" name="docente[nombre]" type="text" class="validate" required="">
                                    <label for="nombre">Nombre</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="apellido" name="docente[apellido]" type="text" class="validate" required="">
                                    <label for="apellido">Apellido</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="telefono" name="docente[telefono]" type="text" class="validate" required="">
                                    <label for="telefono">Teléfono</label>
                                </div>
                                <div class="input-field col m6">
                                    <select id="Curso-bus" name="docente[estado]" class="select-cursos">
                                        <option value="" selected>Seleccionar</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <label>Estado</label>
                                </div>
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Para editar -->
    <div id="editar" class="modal modal-form">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col s12">
                        <button type="button" class="btn-floating modal-close waves-effect waves-light btn right blue-grey darken-4">
                            &times;
                        </button>
                        <h3 class="center-align Titulo-grande">Editar Docente</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="editado">
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="identifica-edi" name="docente[identifica]" type="text" class="validate" required="">
                                    <label for="identifica-edi">Nº Identificación</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="email-edi" name="docente[correo]" type="email" class="validate" required="">
                                    <label for="email-edi">Email / Usuario</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="nombre-edi" name="docente[nombre]" type="text" class="validate" required="">
                                    <label for="nombre-edi">Nombre</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="apellido-edi" name="docente[apellido]" type="text" class="validate" required="">
                                    <label for="apellido-edi">Apellido</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="telefono-edi" name="docente[telefono]" type="text" class="validate" required="">
                                    <label for="telefono-edi">Teléfono</label>
                                </div>
                                <div class="input-field col m6">
                                    <select id="estado-edi" name="docente[estado]" class="select-cursos">
                                        <option value="" selected>Seleccionar</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <label>Estado</label>
                                </div>
                            </div>
                            <div class="row center-align">
                                <button class="btn waves-effect waves-light btn blue-grey darken-4" type="submit" name="action">Guardar
                                </button>
                                <br />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/menu-slide.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/noty.min.js"></script>
    <script src="js/admin-docentes.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
