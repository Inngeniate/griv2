<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
else {
	require("libs/conexion.php");
	$idre = $_GET['reporte'];

	$reporte = $db
		->where('Id_re', $idre)
		->objectBuilder()->get('certificado_reporte');

	$res = $reporte[0];
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
		}

		.img-prev {
			width: 450px;
			height: 100px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.img-prev2 {
			width: 320px;
			height: 220px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="reporte">
				<div class="Contenido-admin-izq">
					<h2>Editar Reporte</h2>
					<form id="ed_certificado">
						<div class="Registro">
							<div class="Registro-der">
								<label>Reporte Nº *</label>
								<input type="text" placeholder="Reporte Nº" name="certifica[reporte]" value="<?php echo $res->reporte ?>" class="ncertificado" readonly>
								<label>Calibración Nº*</label>
								<input type="text" placeholder="Calibración Nº" name="certifica[calibracion]" value="<?php echo $res->calibracion ?>" required>
								<label>Fecha Calibración *</label>
								<input type="date" placeholder="Fecha Calibración" name="certifica[fechacalibra]" value="<?php echo $res->fechacalibra ?>" required>
								<label>Serie *</label>
								<input type="text" placeholder="Serie" name="certifica[serie]" value="<?php echo $res->serie ?>" required>
								<label>Ciudad *</label>
								<input type="text" placeholder="Ciudad" name="certifica[ciudad]" value="<?php echo $res->ciudad ?>" class="auto_ciu" required>
								<label>Fecha *</label>
								<input type="date" placeholder="Fecha" name="certifica[fecha]" value="<?php echo $res->fecha ?>" required>
								<label>Placa *</label>
								<input type="text" placeholder="Placa" name="certifica[placa]" value="<?php echo $res->placa ?>" required>
								<label>Propietario *</label>
								<input type="text" placeholder="Propietario" name="certifica[propietario]" value="<?php echo $res->propietario ?>" required>
								<label>Capacidad Total *</label>
								<input type="text" placeholder="Capacidad Total" name="certifica[capacidadtotal]" value="<?php echo $res->capacidadtotal ?>" required>
								<label>Capacidad BLS *</label>
								<input type="text" placeholder="Capacidad BLS" name="certifica[capacidadbls]" value="<?php echo $res->capacidadbls ?>" required>
								<label>Cantidad de rompe olas *</label>
								<input type="text" placeholder="Cantidad de rompe olas" name="certifica[rompeolas]" value="<?php echo $res->rompeolas ?>" required>
								<label>Compartimientos *</label>
								<input type="text" placeholder="Compartimientos" name="certifica[compartimientos]" value="<?php echo $res->compartimientos ?>" required>
							</div>
							<div class="Registro-der">
								<label>Nº ejes *</label>
								<input type="text" placeholder="Nº ejes" name="certifica[ejes]" value="<?php echo $res->ejes ?>" required>
								<label>Material de tanque *</label>
								<input type="text" placeholder="Material de tanque" name="certifica[materialtanque]" value="<?php echo $res->materialtanque ?>" required>
								<label>Marca *</label>
								<input type="text" placeholder="Marca" name="certifica[marca]" value="<?php echo $res->marca ?>" required>
								<label>Valvulas de descarque *</label>
								<input type="text" placeholder="Valvulas de descarque" name="certifica[valvulas]" value="<?php echo $res->valvulas ?>" required>
								<label>Capacidad compartimiento 1 *</label>
								<input type="text" placeholder="Capacidad compartimiento 1" name="certifica[compartimiento1]" value="<?php echo $res->compartimiento1 ?>">
								<label>Capacidad Bls *</label>
								<input type="text" placeholder="Capacidad Bls" name="certifica[comp1capacidad]" value="<?php echo $res->comp1capacidad ?>">
								<label>Capacidad compartimiento 2 *</label>
								<input type="text" placeholder="Capacidad compartimiento 2" name="certifica[compartimiento2]" value="<?php echo $res->compartimiento2 ?>">
								<label>Capacidad Bls *</label>
								<input type="text" placeholder="Capacidad Bls" name="certifica[comp2capacidad]" value="<?php echo $res->comp2capacidad ?>">
								<label>Capacidad compartimiento 3 *</label>
								<input type="text" placeholder="Capacidad compartimiento 2" name="certifica[compartimiento3]" value="<?php echo $res->compartimiento3 ?>">
								<label>Capacidad Bls *</label>
								<input type="text" placeholder="Capacidad Bls" name="certifica[comp3capacidad]" value="<?php echo $res->comp3capacidad ?>">
								<label>Valido hasta *</label>
								<input type="date" placeholder="Valido hasta" name="certifica[validez]" value="<?php echo $res->validez ?>">
								<?php
								$lista_inspectores = '';

								$inspectores = $db
									->objectBuilder()->get('inspectores');

								foreach ($inspectores as $resin) {
									if ($resin->Id_ins == $res->inspector)
										$lista_inspectores .= '<option value="' . $resin->Id_ins . '" selected>' . $resin->nombre_ins . '</option>';
									else
										$lista_inspectores .= '<option value="' . $resin->Id_ins . '" >' . $resin->nombre_ins . '</option>';
								}
								?>
								<label>Inspector *</label>
								<select name="certifica[inspector]" required>
									<option>Selecciona</option>
									<?php echo $lista_inspectores ?>
								</select>
							</div>
							<div class="Registro-cent">
								<label>Restricciones *</label>
								<textarea placeholder="Restricciones" name="certifica[restricciones]" required><?php echo $res->restricciones ?></textarea>
							</div>
							<br>
							<br>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev2" src="<?php echo substr($res->fotoplaca, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto1">Placa Cabezote</button>
								</div>
								<div class="img-placa">
									<img class="img-prev2" src="<?php echo substr($res->fototanquetrasero, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto2">Parte Trasera Tanque</button>
								</div>
							</div>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev2" src="<?php echo substr($res->fototanquelateral, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto3">Parte Lateral Tanque</button>
								</div>
							</div>
							<input type="hidden" name="certifica[id]" value="<?php echo $idre ?>">
							<input type="submit" value="Guardar Reporte">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/reporte_edt.js"></script>
</body>

</html>
