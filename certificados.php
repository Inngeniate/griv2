<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Certificaciones | Gricompany Gestión del Riesgo Integral</title>
<link rel="stylesheet" href="css/slider.css" />
<link rel="stylesheet" href="css/stylesheet.css" />
<link rel="stylesheet" href="css/style-menu.css" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<link rel="stylesheet" type="text/css" href="css/msj.css" />
<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
<script src="js/modernizr.custom.js"></script>
</head>
<body>
<?php include_once("analyticstracking.php") ?>
<div class="Contenedor">
	<header>
	<?php include("menu.php"); ?>
	</header>
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="js/script-menu.js"></script>
</div>
<div class="Masshead"></div>

<div class="Top-imagen-fondo2">
</div>
<section>
<div class="Principal-text">
	<div class="Principal-text-parrafo">
	<h2>Certificaciones e Inspecciones</h2>
	<p>En esta sección podrás consultar tu certificado ingresando numero de identificación (Cedula)</p>
	<br>
	</div>
	</div>
	<div class="Mostrar-certificado">
		<form id="consulta">
			<br>
			<label>Ingresar numero de cedula para consultar tu certificado</label>
			<input type="text" placeholder="Numero" name="identifica" required>
			<input type="submit" value="Consultar">
		</form>
		<div id="resultados">
		</div>
	</div>
	<hr>
	<div class="Mostrar-certificado">
		<form id="consulta-inspec">
			<br>
			<label>Ingresar la placa del vehiculo para consultar las inspecciones</label>
			<input type="text" placeholder="Numero" name="placa" required>
			<input type="submit" value="Consultar">
		</form>
		<div id="resultados-inspec">
		</div>
	</div>
	<hr>
	<div class="Mostrar-certificado">
		<form id="consulta-seguridad">
			<br>
			<label>Ingresar el serial de tu equipo para consultar el certificado de seguridad</label>
			<input type="text" placeholder="Serial" name="serial" required>
			<input type="submit" value="Consultar">
		</form>
		<div id="resultados-segur">
		</div>
	</div>
</section>
<section>

</section>
<section>
<?php include("convenios.php"); ?>
</section>
<script src="js/toucheffects.js"></script>
<!-- //<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
<?php include("redes.php"); ?>
<footer>
    <?php include("footer.php"); ?>
</footer>
<script type="text/javascript" src="js/certificados.js"></script>
<script src="js/jquery.modal.min.js"></script>
</body>
</html>