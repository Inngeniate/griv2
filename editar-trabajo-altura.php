<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

if ($_SESSION['usutipoggri'] == 'auditor') {
	header('Location: trabajo-altura');
}

require("libs/conexion.php");

$registros = $db
	->where('Id_al', $_REQUEST['reg'])
	->objectBuilder()->get('registros_alturas');

if ($db->count > 0) {
	$rg = $registros[0];

	$competencia = '';

	$ls_cursos = '';

	$cursos = $db
		->where('activo_ct', 1)
		->orderBy('nombre', 'ASC')
		->objectBuilder()->get('certificaciones');

	foreach ($cursos as $rsc) {
		if ($rsc->Id_ct == $rg->curso_al) {
			$competencia = $rsc->competencia_ct;
		}

		$ls_cursos .= '<option value="' . $rsc->Id_ct . '" ' . ($rsc->Id_ct == $rg->curso_al ? "selected" : "") . ' data-competencia="' . $rsc->competencia_ct . '">' . $rsc->nombre . '</option>';
	}

	$ls_competencias = '';

	$competencias = $db
		->where('activo_cp', 1)
		->objectBuilder()->get('competencias');

	foreach ($competencias as $rcp) {
		$ls_competencias .= '<option value="' . $rcp->Id_cp . '" data-nombre="' . $rcp->alias_cp . '" ' . ($rcp->alias_cp == $competencia ? "selected" : "") . '>' . $rcp->nombre_cp . '</option>';
	}

	$ls_formacion = '';

	$formacion  = array('AV', 'REE', 'COOR', 'BAS OP', 'OTRO');
	foreach ($formacion as $value) {
		$ls_formacion .= '<option value="' . $value . '" ' . ($value == $rg->formacion_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ls_sector = '';

	$sector  = array('AGROPECUARIO', 'COMERCIO', 'COMERCIO Y SERVICIOS', 'COMUNICACIONES', 'CONSTRUCCIÓN', 'EDUACION', 'FINANCIERO', 'HIDROCARBUROS', 'INDUSTRIAL', 'MINERO Y ENERGETICO', 'TELECOMUNICACIONES', 'TRANSPORTE');

	foreach ($sector as $value) {
		$ls_sector .= '<option value="' . $value . '" ' . ($value == $rg->sector_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$option1 = '';
	$option2 = '';
	$option3 = '';
	$option4 = '';
	$option5 = '';
	$option6 = '';

	switch ($rg->tipo_doc_al) {
		case 'CC':
			$option1 = 'selected';
			$option2 = '';
			$option3 = '';
			$option4 = '';
			$option5 = '';
			$option6 = '';
			break;
		case 'TI':
			$option1 = '';
			$option2 = 'selected';
			$option3 = '';
			$option4 = '';
			$option5 = '';
			$option6 = '';
			break;
		case 'CE':
			$option1 = '';
			$option2 = '';
			$option3 = 'selected';
			$option4 = '';
			$option5 = '';
			$option6 = '';
			break;
		case 'PP':
			$option1 = '';
			$option2 = '';
			$option3 = '';
			$option4 = 'selected';
			$option5 = '';
			$option6 = '';
			break;
		case 'PEP':
			$option1 = '';
			$option2 = '';
			$option3 = '';
			$option4 = '';
			$option5 = 'selected';
			$option6 = '';
			break;
		case 'NIUP':
			$option1 = '';
			$option2 = '';
			$option3 = '';
			$option4 = '';
			$option5 = '';
			$option6 = 'selected';
			break;
	}

	$ls_lectoescritura = '';

	$lectoescritura = array('ANALFABETA', 'ALFABETA');

	foreach ($lectoescritura as $value) {
		$ls_lectoescritura .= '<option value="' . $value . '" ' . ($value == $rg->lectoescritura_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ls_alergias = '';

	$alergias = array('SI', 'NO');

	foreach ($alergias as $value) {
		$ls_alergias .= '<option value="' . $value . '" ' . ($value == $rg->alergias_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ls_medicamentos = '';

	$medicamentos = array('SI', 'NO');

	foreach ($medicamentos as $value) {
		$ls_medicamentos .= '<option value="' . $value . '" ' . ($value == $rg->medicamentos_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ls_educativo = '';

	$educativo = array('Primaria', 'Bachiller', 'Profesional', 'Especialización', 'Maestría');

	foreach ($educativo as $value) {
		$ls_educativo .= '<option value="' . $value . '" ' . ($value == $rg->nv_educativo_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ls_lesiones = '';

	$lesiones = array('SI', 'NO');

	foreach ($lesiones as $value) {
		$ls_lesiones .= '<option value="' . $value . '" ' . ($value == $rg->lesiones_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ver_lesion_cual = 'class="lesiones-cual disabled" disabled';
	if ($rg->lesioncual_al != '') {
		$ver_lesion_cual = 'class="lesiones-cual"';
	}

	$ls_enfermedades = '';

	$enfermedades = array('SI', 'NO');

	foreach ($enfermedades as $value) {
		$ls_enfermedades .= '<option value="' . $value . '" ' . ($value == $rg->enfermedades_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ver_enfermedad_cual = 'class="enfermedad-cual disabled" disabled';
	if ($rg->enfermedadcual_al != '') {
		$ver_enfermedad_cual = 'class="enfermedad-cual"';
	}

	$eval_1 = '';

	$respuestas = array('A' => 'A) No es importante porque las personas nunca se accidentan', 'B' => 'B) Es importante ya que la capacitación es una medida preventiva que ayuda a disminuir la probabilidad de accidentarse', 'C' => 'C) Es muy importante porque tomando la capacitación a las personas no les pasa nada', 'D' => 'D) Ninguna de las anteriores');

	foreach ($respuestas as $key => $value) {
		$eval_1 .= '<option value="' . $key . '" ' . ($key == $rg->perfil_1_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$eval_2 = '';

	$respuestas = array('A' => 'A) Es un conjunto de palabras y comportamientos que se deben poner en función una vez ocurra un accidente.', 'B' => 'B) Es un juego de mesa', 'C' => 'C) Es un reglamento de seguridad para realizar labores en Alturas que incluye las medidas de prevención y de Protección contra caídas', 'D' => 'D) Ninguna de las anteriores');

	foreach ($respuestas as $key => $value) {
		$eval_2 .= '<option value="' . $key . '" ' . ($key == $rg->perfil_2_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$eval_3 = '';

	$respuestas = array('A' => 'A) Toda actividad que realiza un trabajador que ocasione la suspensión y/o desplazamiento, en el que se vea expuesto a un riesgo de caída, mayor a 2.0 metros, con relación del plano de los pies del trabajador al plano horizontal inferior más cercano a él.', 'B' => 'B) Actividades que se realizan por encima de 20 metros', 'C' => 'C) Actividades que se realizan a partir de 5 metros');

	foreach ($respuestas as $key => $value) {
		$eval_3 .= '<option value="' . $key . '" ' . ($key == $rg->perfil_3_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$firma = '';
	$firma_ver = 'style="display:none;margin:30px;width: 300px"';
	$firma_btn = '';

	/* $firma_in = '';
	$firma_ver_in = 'style="display:none;margin:30px;width: 300px"';
	$firma_btn_in = ''; */

	if ($rg->firma_al != '') {
		$firma = 'src="' . $rg->firma_al . '"';
		$firma_ver = 'style="display:block;margin:30px;width: 300px"';
		$firma_btn = '<a href="javascript://" class="Btn-gris-normal borrar-firma"><i class="icon-bin"> </i>Borrar Firma</a>';
	}

	/* if ($rg->finstructor_al != '') {
		$firma_in = 'src="' . $rg->finstructor_al . '"';
		$firma_ver_in = 'style="display:block;margin:30px;width: 300px"';
		$firma_btn_in = '<a href="javascript://" class="Btn-gris-normal borrar-firma-in"><i class="icon-bin"> </i>Borrar Firma</a>';
	} */

	$ls_sesiones = '';
	$total_ses = 0;
	$inp_sesiones_der = '';
	$inp_sesiones_izq = '';

	$sesiones = $db
		->where('Id_al', $_REQUEST['reg'])
		->objectBuilder()->get('registro_alturas_sesiones');

	if ($db->count > 0) {
		$total_ses = $db->count;

		foreach ($sesiones as $rs) {
			if ($rs->sesion_as % 2 != 0) {
				$inp_sesiones_der .= '<label class="s-' . $rs->sesion_as . '">Sesión ' . $rs->sesion_as . '</label><input type="date" class="tmp-sesion s-' . $rs->sesion_as . '" name="registro[sesion][' . $rs->sesion_as . ']" value="' . $rs->fecha_as . '" required>';
			} else {
				$inp_sesiones_izq .= '<label class="s-' . $rs->sesion_as . '">Sesión ' . $rs->sesion_as . '</label><input type="date" class="tmp-sesion s-' . $rs->sesion_as . '" name="registro[sesion][' . $rs->sesion_as . ']" value="' . $rs->fecha_as . '" required>';
			}
		}
	}

	for ($i = 1; $i < 51; $i++) {
		$ls_sesiones .= '<option value="' . $i . '" ' . ($i == $total_ses ? "selected" : "") . '>' . $i . '</option>';
	}

	$ls_aprobo = '';

	$respuestas = array('S' => 'Si', 'N' => 'No');

	foreach ($respuestas as $key => $value) {
		$ls_aprobo .= '<option value="' . $key . '" ' . ($key == $rg->aprobo_al ? "selected" : "") . '>' . $value . '</option>';
	}

	$ls_eps = '';
	$ls_arl = '';

	$entidades = $db
		->objectBuilder()->get('eps');

	foreach ($entidades as $rent) {
		$ls_eps .= '<option value="' . $rent->nombre_eps . '" ' . ($rent->nombre_eps == $rg->eps_al ? "selected" : "") . '>' . $rent->nombre_eps . '</option>';
	}

	$entidades = $db
		->where('activo_arl', 1)
		->orderBy('orden_arl', 'ASC')
		->objectBuilder()->get('arl');

	foreach ($entidades as $rent) {
		$ls_arl .= '<option value="' . $rent->nombre_arl . '" ' . ($rent->nombre_arl == $rg->arl_al ? "selected" : "") . '>' . $rent->nombre_arl . '</option>';
	}

	$ls_codigos = '';

	$codigos_fichas = $db
		->where('Id', $rg->codigo_ficha)
		->objectBuilder()->get('fichas_ministerio');

	foreach ($codigos_fichas as $rcp) {
		$ls_codigos = '<option value="' . $rcp->Id . '" data-competencia="' . $rcp->competencia . '" data-curso="' . $rcp->niveles . '"  selected>' . $rcp->codigo . '</option>';
	}
} else {
	header('Location: trabajo-altura');
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		input[type=checkbox] {
			width: auto;
			padding: 0;
			margin: 0 5px 10px 0;
			height: auto;
		}

		ul {
			text-align: left;
			list-style: none;
			color: #000;
		}

		.disabled {
			background-color: #e6e6e6;
		}

		.ui-autocomplete {
			max-height: 200px;
			overflow-y: auto;
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
			/* add padding to account for vertical scrollbar */
			z-index: 1000 !important;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Ficha de Inscripción</h2>
				<form id="registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Código del ministerio*</label>
							<select class="Sel-codigo" disabled>
								<?php echo $ls_codigos; ?>
							</select>
						</div>
						<div class="Registro-der">
						</div>
						<div class="Registro-der">
							<label>Competencia</label>
							<select name="competencia" class="competencia" required>
								<option value="">Seleccione</option>
								<?php echo $ls_competencias ?>
							</select>
						</div>
						<div class="Registro-der">
							<label>Curso</label>
							<select name="registro[curso]" class="curso" required>
								<option value="">Seleccione</option>
								<?php echo $ls_cursos; ?>
							</select>
						</div>
						<div class="Registro-der">
							<label>Ciudad *</label>
							<input type="text" placeholder="Ciudad" name="registro[ciudad]" class="auto_ciu" value="<?php echo $rg->ciudad_al; ?>">
							<label>Fecha de Inicio*</label>
							<input type="date" placeholder="Fecha" name="registro[fecha]" value="<?php echo $rg->fecha_al; ?>" required>
							<label>Segundo Nombre</label>
							<input type="text" placeholder="Segundo Nombre" name="registro[segundo_nombre]" value="<?php echo $rg->nombre_segundo_al; ?>">
							<label>País de nacimiento</label>
							<input type="text" placeholder="País de nacimiento" name="registro[nacionalidad]" value="<?php echo $rg->nacionalidad_al; ?>" class="nacionalidad">
							<label>Documento de Identidad *</label>
							<input type="text" placeholder="Documento de Identidad" name="registro[identificacion]" value="<?php echo $rg->documento_al; ?>" class="ntxt" required>
							<label>Fecha de Nacimiento*</label>
							<input type="date" placeholder="Fecha de nacimiento" name="registro[fnacimiento]" value="<?php echo $rg->fnacimiento_al; ?>" class="Fecha-nacimiento" required>
							<label>RH </label>
							<select name="registro[rh]">
								<option value="">Seleccione</option>
								<option value="O-" <?php echo ('O-' == $rg->rh_al ? "selected" : "") ?>>O-</option>
								<option value="O+" <?php echo ('O+' == $rg->rh_al ? "selected" : "") ?>>O+</option>
								<option value="A-" <?php echo ('A-' == $rg->rh_al ? "selected" : "") ?>>A-</option>
								<option value="A+" <?php echo ('A+' == $rg->rh_al ? "selected" : "") ?>>A+</option>
								<option value="B-" <?php echo ('B-' == $rg->rh_al ? "selected" : "") ?>>B-</option>
								<option value="B+" <?php echo ('B+' == $rg->rh_al ? "selected" : "") ?>>B+</option>
								<option value="AB-" <?php echo ('AB-' == $rg->rh_al ? "selected" : "") ?>>AB-</option>
								<option value="AB+" <?php echo ('AB+' == $rg->rh_al ? "selected" : "") ?>>AB+</option>
							</select>
							<label>Teléfono *</label>
							<input type="text" placeholder="Teléfono" name="registro[telefono]"" value=" <?php echo $rg->telcontacto_al; ?>" required>
							<label>Email *</label>
							<input type="text" placeholder="Email" name="registro[email]" value="<?php echo $rg->email_al; ?>" required>
							<label>Nivel Educativo *</label>
							<select name="registro[nveducacion]" required>
								<option value="">Seleccione</option>
								<?php echo $ls_educativo; ?>
							</select>
							<label>Estado Laboral *</label>
							<select name="registro[estado_laboral]" class="estado-laboral" required>
								<option value="">Seleccione</option>
								<option value="EMPLEADO" <?php echo ('EMPLEADO' == $rg->estadolaboral_al ? "selected" : "") ?>>EMPLEADO</option>
								<option value="DESEMPLEADO" <?php echo ('DESEMPLEADO' == $rg->estadolaboral_al ? "selected" : "") ?>>DESEMPLEADO</option>
							</select>
							<label>ARL</label>
							<select name="registro[arl]" class="arl">
								<option value="">Seleccione</option>
								<?php echo $ls_arl ?>
							</select>
							<label>Alergias *</label>
							<select name="registro[alergias]" required>
								<option value="">Seleccione</option>
								<?php echo $ls_alergias; ?>
							</select>
							<label>Lesiones Recientes *</label>
							<select name="registro[lesiones]" id="lesiones" required>
								<option value="">Seleccione</option>
								<?php echo $ls_lesiones; ?>
							</select>
							<label>Enfermedades *</label>
							<select name="registro[enfermedades]" id="enfermedad" required>
								<option value="">Seleccione</option>
								<?php echo $ls_enfermedades; ?>
							</select>
							<label>Verificación de documentos *</label>
							<input type="text" placeholder="Verificación de documentos" name="registro[vrfdocumentos]" value="<?php echo $rg->verificaciondoc_al; ?>">
							<label>Cumple con los requisitos de perfil de ingreso aprendiz: </label>
							<ul>
								<li><input type="checkbox" placeholder="Fot cedula" name="registro[rqcedula]" value="1" <?php echo ($rg->reqcedula_al == '1' ? "checked" : "") ?> class="reqcedula">Fot. Cedula</li>
								<li><input type="checkbox" placeholder="Certificado anterior si aplica" name="registro[rqcertificadoant]" value="1" <?php echo ($rg->reqcertificado_al == '1' ? "checked" : "") ?> class="reqcertificado">Soportes adicionales</li>
								<li><input type="checkbox" placeholder="Valoración medica" name="registro[rqvaloracionmedica]" value="1" <?php echo ($rg->reqvaloracion_al == '1' ? "checked" : "") ?> class="reqvaloracion">Valoración medica</li>
								<li><input type="checkbox" placeholder="Seguridad social" name="registro[rqsegsocial]" value="1" <?php echo ($rg->reqsegsocial_al == '1' ? "checked" : "") ?> class="reqsegsocial">Seguridad social</li>
								<!--<li><input type="checkbox" placeholder="Certificación laboral si aplica" name="registro[rqcertificadolab]" value="1" </?php echo ($rg->reqcertlaboral_al == '1' ? "checked" : "") ?> class="reqcertlaboral">Certificación laboral si aplica</li>-->
								<li style="height: 12px"></li>
								<li style="height: 12px"></li>
							</ul>
						</div>
						<div class="Registro-der">
							<label>Sector Económico *</label>
							<select name="registro[sector]" required>
								<option value="">Seleccione</option>
								<?php echo $ls_sector; ?>
							</select>
							<label>Primer Nombre *</label>
							<input type="text" placeholder="Primer Nombre" name="registro[primer_nombre]" value="<?php echo $rg->nombre_primero_al ?>" required>
							<label>Apellidos *</label>
							<input type="text" placeholder="Apellidos" name="registro[apellidos]" value="<?php echo $rg->apellidos_al ?>" required>
							<label>Tipo de Identificación *</label>
							<select name="registro[tipo_id]" required>
								<option <?php echo $option1 ?>>CC</option>
								<option <?php echo $option2 ?>>TI</option>
								<option <?php echo $option3 ?>>CE</option>
								<option <?php echo $option4 ?>>PP</option>
								<option <?php echo $option5 ?>>PEP</option>
								<option <?php echo $option6 ?>>NIUP</option>
							</select>
							<label>Lugar de Expedición *</label>
							<input type="text" placeholder="De" name="registro[identifica_origen]" value="<?php echo $rg->documento_de_al; ?>" class="nacionalidad" required>
							<label>Edad </label>
							<input type="text" placeholder="Edad" name="registro[edad]" value="<?php echo $rg->edad_al; ?>" class="Edad" required>
							<label>Teléfono del Aprendiz *</label>
							<input type="text" placeholder="Teléfono del aprendiz" name="registro[tlaprendiz]" value="<?php echo $rg->telefono_al; ?>" required>
							<label>Contacto de Emergencia *</label>
							<input type="text" placeholder="Contacto de Emergencia" name="registro[ctemergencia]" value="<?php echo $rg->contacto_em_al; ?>" required>
							<label>Nivel Lectoescritura *</label>
							<select name="registro[lectoescritura]" required>
								<option value="">Seleccione</option>
								<?php echo $ls_lectoescritura; ?>
							</select>
							<label>Profesión u Oficio *</label>
							<input type="text" placeholder="Profesión u Oficio" name="registro[profesion]" value="<?php echo $rg->profesion_al; ?>" required>
							<label>Empresa</label>
							<input type="text" placeholder="Empresa" name="registro[empresa]" class="empresa" value="<?php echo $rg->empresa_al; ?>">
							<label>EPS</label>
							<select name="registro[eps]" class="eps">
								<option value="">Seleccione</option>
								<?php echo $ls_eps ?>
							</select>
							<label>Consumo reciente de medicamentos *</label>
							<select name="registro[medicamentos]" required>
								<option value="">Seleccione</option>
								<?php echo $ls_medicamentos; ?>
							</select>
							<label>Cual *</label>
							<input type="text" placeholder="Cual" name="registro[lesioncual]" <?php echo $ver_lesion_cual; ?> value="<?php echo $rg->lesioncual_al; ?>">
							<label>Cual *</label>
							<input type="text" placeholder="Cual" name="registro[enfermedadcual]" <?php echo $ver_enfermedad_cual; ?> value="<?php echo $rg->enfermedadcual_al; ?>">
							<!--<label>Medio de verificación </label>
							<input type="text" placeholder="Medio de verificación " name="registro[vrfmedio]" value="</?php echo $rg->verificamedio_al; ?>">-->

						</div>
						<hr>
						<label>PERFIL DE INGRESO</label>
						<div class="Registro-cent">
							<label>¿Qué piensa usted acerca de que las personas que trabajan realizando labores en Alturas se capaciten?</label>
							<select name="registro[eval1]" required>
								<option value="">Seleccione</option>
								<?php echo $eval_1; ?>
							</select>
							<label>¿Que sabe usted de la resolución 4272 del 2021?</label>
							<select name="registro[eval2]" required>
								<option value="">Seleccione</option>
								<?php echo $eval_2; ?>
							</select>
							<label>¿En Colombia que se considera trabajo en Alturas?</label>
							<select name="registro[eval3]" required>
								<option value="">Seleccione</option>
								<?php echo $eval_3; ?>
							</select>
						</div>
						<hr>
						<div class="Registro-der">
							<label>Sesiones</label>
							<select name="registro[sesiones]" id="sesiones" required>
								<option value="">Seleccione</option>
								<?php echo $ls_sesiones; ?>
							</select>
						</div>
						<div class="Registro-der">
						</div>
						<div class="Registro-der tmp-der">
							<?php echo $inp_sesiones_der; ?>
						</div>
						<div class="Registro-izq tmp-izq">
							<?php echo $inp_sesiones_izq; ?>
						</div>
						<hr>
						<label>Aprendiz</label>
						<div class="Registro-der">
							<br><br>
							<label>Firma por pantalla</label>
							<button type="button" class="placa" id="firma">Firma Aprendiz</button>
							<img id="canvasimg" <?php echo $firma . $firma_ver ?>>
							<?php echo $firma_btn; ?>
						</div>
						<div class="Registro-der">
							<!-- <br><br>
							<label>Adjuntar firma (imagen)</label>
							<input type="file" id="firma_adjunta" value=""> -->
						</div>
						<hr>
						<br>
						<br>
						<input type="hidden" name="registro[idregistro]" value="<?php echo $_REQUEST['reg']; ?>">
						<input type="submit" value="Guardar Registro">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/moment-2.18.min.js"></script>
	<script type="text/javascript" src="js/registrar_alturas_editar.js?<?php echo time()  ?>"></script>
</body>

</html>
