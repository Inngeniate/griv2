<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
require("libs/conexion.php");

$fecha = date('d-m-Y');
$ls_preguntas = '';
$ls_empleados = '';

$preguntas = $db
	->objectBuilder()->get('preguntas_covid');

foreach ($preguntas as $rpr) {
	$ls_preguntas .= '<label>' . $rpr->pregunta_pc . '</label>
						<select name="asistencia[pregunta][]" data-puntaje="' . $rpr->puntaje_pc . '" required>
							<option value="">Seleccione</option>
							<option value="Si">Si</option>
							<option value="No">No</option>
						</select>';
}

$empleados = $db
	->where('tipo_em', 'E')
	->objectBuilder()->get('empleados');

foreach ($empleados as $rem) {
	$ls_empleados .= '<option value="' . $rem->Id_em . '">' . $rem->nombre_em . '</option>';
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Asistencias | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		input[type=checkbox] {
			width: auto;
			padding: 0;
			margin: 0 5px 10px 0;
			height: auto;
		}

		ul {
			text-align: left;
			list-style: none;
			color: #000;
		}

		.disabled {
			background-color: #e6e6e6;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Registro de asistencia <?php echo $fecha; ?></h2>
				<form id="registro" class="Form-registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Tipo</label>
							<select name="asistencia[tipo]" class="select-tipo" required>
								<option value="">Seleccione</option>
								<option value="E">Empleado</option>
								<option value="X">Externo</option>
							</select>
						</div>
						<div class="Registro-izq">
							<label>Identificación *</label>
							<input type="number" step="0" min="1" name="asistencia[identifica]" class="identifica" placeholder="Identificación" disabled>
						</div>
						<button type="button" class="placa Identificar-empleado">Buscar</button>
						<div class="crear-externo" style="display: none">
							<hr>
							<label>Información de registro</label>
							<div class="Registro-der">
								<label>Nombre *</label>
								<input type="text" placeholder="Nombre" name="asistencia[nombre]" class="Nombre-empleado" required>
							</div>
							<div class="Registro-izq">
								<label>Identificación *</label>
								<input type="text" placeholder="Identificación" name="asistencia[identifica]" class="Identifica-empleado" required>
							</div>
							<div class="Registro-der">
								<label>Teléfono *</label>
								<input type="text" placeholder="Teléfono" name="asistencia[telefono]" class="Telefono-empleado" required>
							</div>
							<div class="Registro-izq">
							</div>
							<div class="Div-sesiones" style="display: none">
								<label>Número de sesiones</label>
								<select name="asistencia[sesiones]" class="sesiones" required>
									<option value="">Seleccione</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
							</div>
						</div>
						<hr>
						<div class="Registro-sesiones" style="display: none">
						</div>
						<label></label>
						<div class="Listado-preguntas" style="display: none">
							<div class="Registro-cent">
								<?php echo $ls_preguntas; ?>
							</div>
							<hr>
						</div>
						<div class="Registro-temperatura" style="display: none">
							<div class="Registro-der">
								<label>Tipo de temperatura</label>
								<select name="asistencia[tipo_temperatura]" required>
									<option value="">Seleccione</option>
									<option value="E">Entrada</option>
									<option value="D">Durante</option>
									<option value="S">Salida</option>
								</select>
							</div>
							<div class="Registro-izq">
								<label>Temperatura *</label>
								<input type="number" min="1" step="0.1" name="asistencia[temperatura]" placeholder="Temperatura" required>
							</div>
							<hr>
						</div>
						<div class="Registro-firma" style="display: none">
							<div class="Registro-der">
								<br><br>
								<label>Firma</label>
								<button type="button" class="placa" id="firma">Firma</button>
								<img id="canvasimg" style="display:none;margin:30px;width: 300px">
							</div>
							<div class="Registro-izq">
							</div>
							<label>Quien realiza la encuesta</label>
							<div class="Registro-der">
								<select name="asistencia[responsable]" required>
									<option value="">Seleccione</option>
									<?php echo $ls_empleados; ?>
								</select>
							</div>
							<div class="Registro-izq">
							</div>
						</div>
						<div class="Btn-guardar" style="display: none">
							<br>
							<br>
							<input type="submit" value="Guardar Registro">
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/registrar_asistencia.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
