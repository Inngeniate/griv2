<div class="Contenido-admin-izq crear-usuarios" style="display: none">
	<h2>Crear Usuario</h2>
	<form id="nuevo-usuario">
		<div class="Registro">
			<div class="Registro-der">
				<label>Nombre *</label>
				<input type="text" placeholder="Nombre" name="usuario[nombre]" required>
				<label>Apellido *</label>
				<input type="text" placeholder="Apellido" name="usuario[apellido]" required>
			</div>
			<div class="Registro-der">
				<label>Correo *</label>
				<input type="email" placeholder="Correo" name="usuario[correo]" required>
				<label>Password </label>
				<input type="password" placeholder="Password" name="usuario[password]" required>
			</div>
			<hr>
			<div class="Registro-cent Tabla-listar">
				<table>
					<thead>
						<th>Módulo</th>
						<th>Acción</th>
					</thead>
					<tbody>
						<?php echo $ls_menu ?>
					</tbody>
				</table>
			</div>
			<br>
			<br>
			<input type="submit" value="Guardar">
		</div>
	</form>
</div>
