<?php

require_once 'libs/tcpdf.php';

class MYPDF extends TCPDF
{
	public function Header()
	{

		$estilo = '<style>
					table{
						font-size: 11px;
						font-family: arial;
					}
					.tmax{
						font-size: 12px;
					}
					.tmin2{
						font-size: 11px;
					}
				</style>';

		$bMargin = $this->getBreakMargin();
		$auto_page_break = $this->AutoPageBreak;
		$this->SetAutoPageBreak(false, 0);
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		$this->setPageMark();
		// $this->SetMargins(15, 50, 15, true);

		$head = '<table cellspacing="0" cellpadding="0" border="0" class="certificado1">
							<tr>
								<td height="20"><br><br><img src="images/logo.png" width="120px"><br></td>
							</tr>
							<tr>
								<td align="center" class="tmax"><b>CONTROL DE ASISTENCIA A ACTIVIDADES DE CAPACITACIÓN,RECREACIÓN,CULTURA O DEPORTE</b></td>
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="99%">
							<tr>
								<td colspan="3"></td>
								<td class="tmax" align="right"><br><br><br>FTH 005 <br></td>
							</tr>
						</table>';

		$this->writeHTML($estilo . $head, true, false, false, false, '');
	}

	public function Footer()
	{
		$this->SetY(-45);
		$estilo = '<style>
						table{
							color: #999999;
							font-size: 12px;
						}
						.link{
							color: #495de6;
						}
					</style>';
		switch ($this->page) {
			case 1:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td align="right">Versión 01</td>
							</tr>
							<tr>
								<td align="right">	1 de 2 | Página</td>
							</tr>
						</table>';

				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
			case 2:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td align="right">Versión 01</td>
							</tr>
							<tr>
								<td align="right">	2 de 2 | Página</td>
							</tr>
						</table>';
				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
		}
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
$pdf->setPageOrientation('p');

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// PDF_MARGIN_TOP
$pdf->SetMargins(15, 50, 15);
// $pdf->SetPrintHeader(false);
// $pdf->setPrintFooter(false);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(true, 45);

// set image scale factor
// $pdf->setImageScale(1.53);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
	require_once dirname(__FILE__) . '/lang/eng.php';
	$pdf->setLanguageArray($l);
}

$pdf->AddPage();
$arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 10);

$estilo = '<style>
    			table{
    				font-size: 11px;
    				font-family: arial;
    			}
    			.tmax{
    				font-size: 12px;
    			}
    			.tmin2{
    				font-size: 11px;
    			}
			</style>';

$html = '<table cellspacing="4" cellpadding="2" border="0"  width="100%">
			<tr>
				<td width="50">FECHA:</td>
				<td width="80" style="border-bottom:1px solid #000"></td>
				<td width="70">DURACIÓN:</td>
				<td width="250" style="border-bottom:1px solid #000"></td>
				<td width="100">CONSECUTIVO:</td>
				<td width="93" style="border-bottom:1px solid #000"></td>
			</tr>
			<tr>
				<td colspan="3" width="150">NOMBRE DE LA ACTIVIDAD:</td>
				<td style="border-bottom:1px solid #000"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="108%">
			<tr>
				<td height="20"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="100%">
			<tr>
				<td width="20" style="border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000"></td>
				<td width="170"> CAPACITACIÓN LABORAL</td>
				<td width="20" style="border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000"></td>
				<td width="100"> RECREATIVA</td>
				<td width="20" style="border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000"></td>
				<td width="100"> CULTURAL</td>
				<td width="20" style="border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000"></td>
				<td width="70"> DEPORTE</td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="108%">
			<tr>
				<td height="20"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="100%">
			<tr>
				<td align="center">OBJETIVOS DE LA CAPACITACIÓN</td>
				<td align="center">TEMARIO A TRATAR EN LA CAPACITACIÓN</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="108%">
			<tr>
				<td height="20"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="110%">
			<tr>
				<td width="25" align="center"><b>#</b></td>
				<td width="230" align="center">NOMBRE</td>
				<td width="90" align="center">CARGO</td>
				<td width="75" align="center">CEDULA</td>
				<td width="150" align="center">EMPRESA</td>
				<td width="90" align="center">FIRMA</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">2</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">3</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">4</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">5</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">6</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">7</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">8</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">9</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">10</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">11</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">12</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">13</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">14</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">15</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">16</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">17</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>';



$pdf->writeHTML($estilo . $html, true, false, false, false, '');
$pdf->AddPage();

$html = '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="110%">
			<tr>
				<td width="25" align="center"><b>18</b></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="108%">
			<tr>
				<td height="30"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="5" border="0" width="99%">
			<tr>
				<td width="110">ÁREA ASISTENTE:</td>
				<td style="border-bottom:1px solid #000" width="250"></td>
				<td width="55"> LUGAR:</td>
				<td style="border-bottom:1px solid #000"></td>
			</tr>
			<tr>
				<td colspan="2" width="100">DIRIGIDO POR:</td>
				<td colspan="2" style="border-bottom:1px solid #000"  width="350"></td>
			</tr>
		</table>';

$pdf->writeHTML($estilo . $html, true, false, false, false, '');


$pdf->Output('FTH005.pdf', 'I');
