<?php

require_once 'libs/tcpdf.php';

class MYPDF extends TCPDF
{
	public function Header()
	{

		$estilo = '<style>
					table{
						font-size: 11px;
						font-family: arial;
					}
					.tmax{
						font-size: 12px;
					}
					.tmin2{
						font-size: 11px;
					}
				</style>';

		$bMargin = $this->getBreakMargin();
		$auto_page_break = $this->AutoPageBreak;
		$this->SetAutoPageBreak(false, 0);
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		$this->setPageMark();
		// $this->SetMargins(15, 50, 15, true);

		$head = '<table cellspacing="0" cellpadding="0" border="0" class="certificado1">
							<tr>
								<td height="20"><br><br><img src="images/logo.png" width="120px"><br></td>
							</tr>
							<tr>
								<td class="tmax"><b>FORMATO PLAN DE CLASE</b></td>
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="99%">
							<tr>
								<td colspan="3"></td>
								<td class="tmax" align="right"><br><br><br>FED 008 <br></td>
							</tr>
						</table>';

		$this->writeHTML($estilo . $head, true, false, false, false, '');
	}

	public function Footer()
	{
		$this->SetY(-45);
		$estilo = '<style>
					table{
						color: #999999;
						font-size: 12px;
					}
					.link{
						color: #495de6;
					}
				</style>';
		switch ($this->page) {
			case 1:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td align="right">Versión 01</td>
						</tr>
						<tr>
							<td align="right">	1 de 3 | Página</td>
						</tr>
					</table>';

				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
			case 2:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td align="right">Versión 01</td>
						</tr>
						<tr>
							<td align="right">	2 de 3 | Página</td>
						</tr>
					</table>';
				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
			case 3:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td align="right">Versión 01</td>
						</tr>
						<tr>
							<td align="right">	3 de 3 | Página</td>
						</tr>
					</table>';
				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
		}
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
$pdf->setPageOrientation('p');

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// PDF_MARGIN_TOP
$pdf->SetMargins(15, 50, 15);
// $pdf->SetPrintHeader(false);
// $pdf->setPrintFooter(false);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(true, 45);

// set image scale factor
// $pdf->setImageScale(1.53);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
	require_once dirname(__FILE__) . '/lang/eng.php';
	$pdf->setLanguageArray($l);
}

$pdf->AddPage();
$arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 11);

$estilo = '<style>
			table{
				font-size: 11px;
				font-family: arial;
			}
			.tmax{
				font-size: 12px;
			}
			.tmin2{
				font-size: 11px;
			}
		</style>';

$html = '<table cellspacing="0" cellpadding="4" border="0" width="99%">
			<tr>
				<td align="center" height="30">PLAN DE CLASE</td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" class="certificado1" width="99%">
			<tr>
				<td><b>ENTRENADOR:</b></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="6"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" class="certificado1" width="99%">
			<tr>
				<td><b>CLIENTE:</b></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="6"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" class="certificado1" width="99%">
			<tr>
				<td><b>EMPRESA:</b></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="6"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" class="certificado1" width="99%">
			<tr>
				<td height="40"><b>NOMBRE DE LA CLASE:</b></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="6"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="0" class="certificado1" width="99%">
			<tr>
				<td style="border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000">FECHA:</td>
				<td style="border-top:1px solid #000;border-bottom:1px solid #000"></td>
				<td style="border-top:1px solid #000;border-bottom:1px solid #000">DURACION:</td>
				<td style="border-top:1px solid #000;border-bottom:1px solid #000;border-right:1px solid #000"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="6"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="99%">
			<tr>
				<td height="60" colspan="4">OBJETIVO GENERAL:</td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="0" width="99%">
			<tr>
				<td height="30"><br><br>OBJETIVOS DE DESEMPEÑO:<br></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" width="99%">
			<tr>
				<td height="50">1:</td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="10"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" width="99%">
			<tr>
				<td height="50">2:</td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="10"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" width="99%">
			<tr>
				<td height="50">3:</td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="10"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" width="99%">
			<tr>
				<td height="50">4:</td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
			<tr>
				<td align="center" height="10"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="2" border="1" width="99%">
			<tr>
				<td height="50">5:</td>
			</tr>
		</table>';

$pdf->writeHTML($estilo . $html, true, false, false, false, '');


$pdf->SetMargins(15, 70, 15);
$pdf->AddPage();

$html = '<table cellspacing="0" cellpadding="6" border="1" width="99%">
			<tr>
				<td align="center" width="100">HORARIO</td>
				<td align="center" width="350">ACTIVIDAD/CONTENIDO</td>
				<td align="center" width="200">RECURSOS NECESARIOS</td>
			</tr>
			<tr>
				<td></td>
				<td>Bienvenida y presentación del entrenador</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>Breve resumen de lo que se va a hacer</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>Verificación previa del conocimiento de los estudiantes respecto al tema</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td height="140">Desarrollo de Contenidos de la Clase:</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>Breve resumen retrospectivo de la clase</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>Comentarios finales respecto al cambio adquirido por el estudiante en la clase</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>Evaluación de la clase</td>
				<td></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="0" width="99%">
			<tr>
				<td align="center" height="30"><br><br>Nota: Las actividades/contenidos deben ser congruentes con los objetivos de desempeño<br></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="6" border="1" width="99%">
			<tr>
				<td width="600">TIPO DE EVALUACION A UTILIZAR</td>
				<td width="50">Si/No</td>
			</tr>
			<tr>
				<td>Taller Integral Pedagógico</td>
				<td></td>
			</tr>
			<tr>
				<td>Mapa Conceptual</td>
				<td></td>
			</tr>
			<tr>
				<td>Cuestionario de 10 preguntas</td>
				<td></td>
			</tr>
			<tr>
				<td>Experiencia de aprendizaje</td>
				<td></td>
			</tr>
			<tr>
				<td>Dramatización</td>
				<td></td>
			</tr>
		</table>';


$pdf->writeHTML($estilo . $html, true, false, false, false, '');

$pdf->AddPage();

$html = '<table cellspacing="0" cellpadding="6" border="1" width="99%">
			<tr>
				<td width="600">Simulacro</td>
				<td width="50"></td>
			</tr>
			<tr>
				<td>Otro / Cual?</td>
				<td></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="0" width="99%">
			<tr>
				<td align="justify" height="30"><br><br><br>Nota: <i>La evaluación debe ser congruente con los objetivos de desempeño y por tal motivo con los contenidos de la clase. El documento soporte de evaluación de la clase debe ir anexo al plan.</i><br></td>
			</tr>
		</table>';


$pdf->writeHTML($estilo . $html, true, false, false, false, '');

$pdf->Output('FED005.pdf', 'I');
