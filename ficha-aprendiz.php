<?php

require_once 'libs/conexion.php';

$Id = $_GET['id'];
// $Id = 1;

$registro = $db
	->where('Id_al', $Id)
	->objectBuilder()->get('registros_alturas');

if ($db->count > 0) {
	$rg = $registro[0];
	$nombre = $rg->nombre_primero_al . ' ' . $rg->nombre_segundo_al . ' ' . $rg->apellidos_al;
	$identificacion = $rg->documento_al;
	$firma = $rg->firma_al;
	$firma_in = $rg->finstructor_al;
	$curso = '';

	$av = $re = $coor = $bas = $otro = '';

	switch ($rg->formacion_al) {
		case 'AV':
			$av = 'X';
			break;
		case 'REE':
			$re = 'X';
			break;
		case 'COOR':
			$coor = 'X';
			break;
		case 'BAS OP':
			$bas = 'X';
			break;
	}

	if ($rg->formacion_otro_al != '') {
		$otro = $rg->formacion_otro_al;
	}

	$competencia = '';
	$curso = '';

	if ($rg->competencia_al != '' && $rg->competencia_al != null && $rg->competencia_al != 0) {
		$competencias = $db
			->where('Id_cp', $rg->competencia_al)
			->objectBuilder()->get('competencias');

		if ($db->count > 0) {
			$rcp = $competencias[0];
			$competencia = $rcp->nombre_cp;
		}
	}

	$cursos = $db
		->where('Id_ct', $rg->curso_al)
		->objectBuilder()->get('certificaciones');

	if ($db->count > 0) {
		$rsc = $cursos[0];
		$curso = $rsc->nombre;

		if ($competencia == '') {
			$competencias = $db
				->where('alias_cp', $rsc->competencia_ct)
				->objectBuilder()->get('competencias');

			if ($db->count > 0) {
				$rcp = $competencias[0];
				$competencia = $rcp->nombre_cp;
			}
		}
	}

	$construccion = $telecomunicaciones = $hidrocarburos = '';
	$sector = $rg->sector_al;

	/* switch ($rg->sector_al) {
		case 'CONSTRUCCIÓN':
			$construccion = 'X';
			break;
		case 'TELECOMUNICACIONES':
			$telecomunicaciones = 'X';
			break;
		case 'HIDROCARBUROS':
			$hidrocarburos = 'X';
			break;
	} */

	$analfabeta = $alfabeta = '';

	switch ($rg->lectoescritura_al) {
		case 'ANALFABETA':
			$analfabeta = 'X';
			break;
		case 'ALFABETA':
			$alfabeta = 'X';
			break;
	}

	$alergias_si = $alergias_no = '';

	switch ($rg->alergias_al) {
		case 'SI':
			$alergias_si = 'X';
			break;
		case 'NO':
			$alergias_no = 'X';
			break;
	}

	$medicamentos_si = $medicamentos_no = '';

	switch ($rg->medicamentos_al) {
		case 'SI':
			$medicamentos_si = 'X';
			break;
		case 'NO':
			$medicamentos_no = 'X';
			break;
	}

	$primaria = $bachiller = $profesional = $especializacion = $maestria = '';

	switch ($rg->nv_educativo_al) {
		case 'Primaria':
			$primaria = 'X';
			break;
		case 'Bachiller':
			$bachiller = 'X';
			break;
		case 'Profesional':
			$profesional = 'X';
			break;
		case 'Especialización':
			$especializacion = 'X';
			break;
		case 'Maestría':
			$maestria = 'X';
			break;
	}

	$reqcedula = ($rg->reqcedula_al == 0 ? "" : "X");
	$reqcertificado = ($rg->reqcertificado_al == 0 ? "" : "X");
	$reqvaloracion = ($rg->reqvaloracion_al == 0 ? "" : "X");
	$reqsegsocial = ($rg->reqsegsocial_al == 0 ? "" : "X");
	$reqcertlaboral = ($rg->reqcertlaboral_al == 0 ? "" : "X");

	$ciudad = $rg->ciudad_al;
	$fecha = $rg->fecha_al;
	$documentode = $rg->documento_de_al;
	$nacimiento = $rg->fnacimiento_al;
	$edad = $rg->edad_al;
	$telefono_ap = $rg->telefono_al;
	// $gs = $rg->gs_al;
	$rh = $rg->rh_al;
	$eps = $rg->eps_al;
	$arl = $rg->arl_al;
	$contacto = $rg->contacto_em_al;
	$telcontacto = $rg->telcontacto_al;
	$email = $rg->email_al;
	$profesion = $rg->profesion_al;
	$empresa = $rg->empresa_al;
	$lesiones = $rg->lesiones_al;
	$lesioncual = $rg->lesioncual_al;
	$enfermedades = $rg->enfermedades_al;
	$enfermedadcual = $rg->enfermedadcual_al;
	$verificadoc = $rg->verificaciondoc_al;
	$verficamed = $rg->verificamedio_al;
	$perfil_1 = $rg->perfil_1_al;
	$perfil_2 = $rg->perfil_2_al;
	$perfil_3 = $rg->perfil_3_al;

	$cal_teorica = $rg->califica_teorica_al;
	$cal_practica = $rg->califica_practica_al;
	$cal_total = $rg->califica_total_al;

	$aprobo_s = $aprobo_n = '';

	switch ($rg->aprobo_al) {
		case 'S':
			$aprobo_s = 'X';
			break;
		case 'N':
			$aprobo_n = 'X';
			break;
	}

	$respuestas_1 = array('A' => 'A) No es importante porque las personas nunca se accidentan', 'B' => 'B) Es importante ya que la capacitación es una medida preventiva que ayuda a disminuir la probabilidad de accidentarse', 'C' => 'C) Es muy importante porque tomando la capacitación a las personas no les pasa nada', 'D' => 'D) Ninguna de las anteriores');

	if ($perfil_1 != '') {
		$perfil_1 = $respuestas_1[$perfil_1];
	}


	$respuestas_2 = array('A' => 'A) Es un conjunto de palabras y comportamientos que se deben poner en función una vez ocurra un accidente.', 'B' => 'B) Es un juego de mesa', 'C' => 'C) Es un reglamento de seguridad para realizar labores en Alturas que incluye las medidas de prevención y de Protección contra caídas', 'D' => 'D) Ninguna de las anteriores');

	if ($perfil_2 != '') {
		$perfil_2 = $respuestas_2[$perfil_2];
	}


	$respuestas_3 = array('A' => 'A) Toda actividad que realiza un trabajador que ocasione la suspensión y/o desplazamiento, en el que se vea expuesto a un riesgo de caída, mayor a 2.0 metros, con relación del plano de los pies del trabajador al plano horizontal inferior más cercano a él.', 'B' => 'B) Actividades que se realizan por encima de 20 metros', 'C' => 'C) Actividades que se realizan a partir de 5 metros');

	if ($perfil_3 != '') {
		$perfil_3 = $respuestas_3[$perfil_3];
	}

	$cursos = $db
		->where('Id_ct', $rg->curso_al)
		->objectBuilder()->get('certificaciones');

	if ($db->count > 0) {
		$rc = $cursos[0];
		$curso = $rc->nombre;
	}


	require_once 'libs/tcpdf.php';
	class MYPDF extends TCPDF
	{
		public function Header()
		{

			$estilo = '<style>
						table{
							font-size: 11px;
						}
						.tmax{
							font-size: 12px;
						}
						.tmin2{
							font-size: 11px;
						}
					</style>';

			$bMargin = $this->getBreakMargin();
			$auto_page_break = $this->AutoPageBreak;
			$this->SetAutoPageBreak(false, 0);
			$this->SetAutoPageBreak($auto_page_break, $bMargin);
			$this->setPageMark();

			$head = '<table cellspacing="0" cellpadding="5" border="0" class="certificado1">
								<tr>
									<td height="20" width="180" align="center"><br><br><img src="images/logo.png" width="150px"><br></td>
									<td class="tmax" align="center" width="420"><br><br><br><br><strong>FICHA DE INSCRIPCION APRENDICES</strong></td>
									<td class="tmax" align="center" width="100"><br><br><br><br><strong>FED 006</strong><br></td>
								</tr>
							</table>';

			$this->writeHTML($estilo . $head, true, false, false, false, '');
			$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(222, 48, 53));

			$this->Line(10, 35, $this->w - 7, 35, $style);
		}

		public function Footer()
		{
			$this->SetY(-35);
			$estilo = '<style>
						table{
							color: #999999;
							font-size: 12px;
						}
						.link{
							color: #495de6;
						}
					</style>';

			$pagenumtxt = $this->getAliasNumPage() . ' de ' . $this->getAliasNbPages() . ' | Página';

			$cur_y = $this->y;
			$this->SetY($cur_y);

			$this->Cell(0, 5, 'Versión 01', 0, false, 'R', 0, '', 0, false, 'T', 'M');
			$this->Cell(12, 12, $pagenumtxt, 0, false, 'R', 0, '', 0, false, 'T', 'M');
		}
	}

	// create new PDF document
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
	$pdf->setPageOrientation('p');

	// set header and footer fonts
	$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// PDF_MARGIN_TOP
	$pdf->SetMargins(10, 40, 10);

	// set auto page breaks
	$pdf->SetAutoPageBreak(true, 35);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
		require_once dirname(__FILE__) . '/lang/eng.php';
		$pdf->setLanguageArray($l);
	}

	$pdf->AddPage();

	$estilo = '<style>
					table{
						font-size: 11px;
					}
					.tmax{
						font-size: 12px;
					}
					.tmin2{
						font-size: 11px;
					}
					.sesion{
						background-color: #dfeac6
					}
					.sesion-fecha{
						color: #afafaf
					}
				</style>';


	$html = '<table cellpadding="3" border="1" width="99%">
				<tr>
					<td width="130">COMPETENCIA</td>
					<td colspan="3">' . $competencia . '</td>
					<td align="center">CURSO</td>
					<td colspan="9" width="380">' . $curso . '</td>
				</tr>
				<tr>
					<td>SECTOR</td>
					<td colspan="13">' . $sector . '</td>
				</tr>
				<tr>
					<td>CIUDAD</td>
					<td colspan="8">' . $ciudad . '</td>
					<td width="50">FECHA</td>
					<td colspan="4" width="161">' . $fecha . '</td>
				</tr>
			</table>';

	$html .= '<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td height="40"></td>
			</tr>
		</table>';

	$html .= '<table cellpadding="3" border="1" width="99%">
				<tr>
					<td width="130">Nombre Completo:</td>
					<td colspan="13" width="577">' . $nombre . '</td>
				</tr>
				<tr>
					<td>Documento de Identidad:</td>
					<td colspan="8" width="375">' . $identificacion . '</td>
					<td width="30">De:</td>
					<td colspan="4" width="172">' . $documentode . '</td>
				</tr>
				<tr>
					<td>Fecha de Nacimiento:</td>
					<td colspan="9" width="405">' . $nacimiento . '</td>
					<td width="60">Edad:</td>
					<td colspan="3" width="112">' . $edad . '</td>
				</tr>
				<tr>
					<td>Teléfono del Aprendiz:</td>
					<td colspan="5" width="245">' . $telefono_ap . '</td>
					<td width="40">RH:</td>
					<td>' . $rh . '</td>
					<td width="30"></td>
					<td></td>
					<td colspan="4"></td>
				</tr>
				<tr>
					<td>EPS:</td>
					<td colspan="13">' . $eps . '</td>
				</tr>
				<tr>
					<td>ARL:</td>
					<td colspan="13">' . $arl . '</td>
				</tr>
				<tr>
					<td>Contaco de Emergencia:</td>
					<td colspan="9">' . $contacto . '</td>
					<td>Teléfono:</td>
					<td colspan="3">' . $telcontacto . '</td>
				</tr>
				<tr>
					<td>Email:</td>
					<td colspan="9">' . $email . '</td>
					<td colspan="2">ANALFABETA</td>
					<td colspan="2">ALFABETA</td>
				</tr>
				<tr>
					<td>Nivel Lectoescritura:</td>
					<td colspan="9"></td>
					<td colspan="2" align="center">' . $analfabeta . '</td>
					<td colspan="2" align="center">' . $alfabeta . '</td>
				</tr>
				<tr>
					<td>Profesión u Oficio:</td>
					<td colspan="5">' . $profesion . '</td>
					<td colspan="4">Empresa o Particular:</td>
					<td colspan="4">' . $empresa . '</td>
				</tr>
				<tr>
					<td>Alergias:</td>
					<td>SI</td>
					<td width="50" align="center">' . $alergias_si . '</td>
					<td width="30">NO</td>
					<td width="50" align="center">' . $alergias_no . '</td>
					<td colspan="5" width="208">Consumo reciente de medicamentos:</td>
					<td>SI</td>
					<td align="center">' . $medicamentos_si . '</td>
					<td width="55">NO</td>
					<td align="center">' . $medicamentos_no . '</td>
				</tr>
				<tr>
					<td>NIVEL EDUCATIVO</td>
					<td></td>
					<td width="50">Primaria</td>
					<td align="center">' . $primaria . '</td>
					<td width="50">Bachiller</td>
					<td align="center">' . $bachiller . '</td>
					<td width="85" colspan="2">Profesional</td>
					<td align="center">' . $profesional . '</td>
					<td colspan="2" width="100">Especialización</td>
					<td align="center">' . $especializacion . '</td>
					<td width="55">Maestría</td>
					<td align="center">' . $maestria . '</td>
				</tr>
				<tr>
					<td rowspan="2">CUMPLE CON LOS REQUISITOS DE PERFIL DE INGRESO APRENDIZ:</td>
					<td rowspan="2">Fot. Cedula</td>
					<td rowspan="2" align="center">' . $reqcedula . '</td>
					<td rowspan="2" colspan="2">Certificado anterior si aplica</td>
					<td rowspan="2" align="center">' . $reqcertificado . '</td>
					<td rowspan="2" colspan="2">Valoración medica</td>
					<td rowspan="2" align="center">' . $reqvaloracion . '</td>
					<td colspan="2">Seguridad social</td>
					<td align="center">' . $reqsegsocial . '</td>
					<td>CUMPLE</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2">Certificación laboral si aplica</td>
					<td align="center">' . $reqcertlaboral . '</td>
					<td>NO CUMPLE</td>
					<td></td>
				</tr>
				<tr>
					<td>Lesiones Recientes:</td>
					<td>' . $lesiones . '</td>
					<td>Cual:</td>
					<td colspan="3">' . ($lesioncual == '' ? "---" : $lesioncual) . '</td>
					<td colspan="2">Enfermedades:</td>
					<td>' . $enfermedades . '</td>
					<td>Cuál:</td>
					<td colspan="4">' . ($enfermedadcual == '' ? "---" : $enfermedadcual) . '</td>
				</tr>
				<tr>
					<td>Verificación de documentos</td>
					<td colspan="5">' . $verificadoc . '</td>
					<td colspan="3">Medio de verificación:</td>
					<td colspan="5">' . $verficamed . '</td>
				</tr>
			</table>';

	$html .= '<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td height="50"></td>
				</tr>
			</table>';


	$html .= '<table cellspacing="5" border="0" width="99%">
				<tr>
					<td colspan="2" align="center"><strong>PERFIL DE INGRESO</strong></td>
				</tr>
				<tr>
					<td align="center" height="10"></td>
				</tr>
				<tr>
					<td colspan="2"><strong>¿Qué piensa usted acerca de que las personas que trabajan realizando labores en Alturas se capaciten?</strong></td>
				</tr>
				<tr>
					<td colspan="2">' . $perfil_1 . '</td>
				</tr>
				<tr>
					<td colspan="2"><strong>¿Que sabe usted de la resolución 4272 del 2021?</strong></td>
				</tr>
				<tr>
					<td colspan="2">' . $perfil_2 . '</td>
				</tr>
				<tr>
					<td colspan="2"><strong>¿En Colombia que se considera trabajo en Alturas?</strong></td>
				</tr>
				<tr>
					<td colspan="2">' . $perfil_3 . '</td>
				</tr>
				<tr>
					<td align="center" height="20"></td>
				</tr>
			</table>';

	$html .= '<table cellspacing="0" border="0" width="101%">
				<tr>
					<td>
						<p>YO, <strong>' . $nombre . '</strong>, Identificado con cédula de Ciudadanía No. <strong>' . $identificacion . '</strong>, quien en adelante me  denominare <strong>APRENDIZ</strong>,  matriculado en el programa de formación denominado: <strong>' . $curso . '</strong>, me comprometo con  <strong>GRI COMPANY S.A.S</strong>,</p>
						<p>1. Cumplir y promover las disposiciones contempladas en el Reglamento del estudiante <strong>GRI COMPANY S.A.S</strong>, del cual tengo conocimiento, así como las establecidas en las Normas de Convivencia y de las derivadas de mi situación geográfica, entorno tecnológico y cultural del Centro de Formación.</p>
						<p>2. Desarrollar el programa de formación en el cual me he matriculado, a través de la formación por proyectos como estrategia metodológica institucional, asumiendo mi rol como gestor de mi propio proceso de aprendizaje, en el marco del aprendizaje autónomo, haciendo uso de la infraestructura disponible en la entidad y las condiciones tecnológicas de <strong>GRI COMPANY S.A.S</strong>, en particular para el programa de formación, lo cual me fue explicado durante mi inducción.</p>
						<p>3. Participar con responsabilidad en todas las actividades curriculares y complementarias o de profundización relacionadas con el programa de formación sean virtuales o presenciales, sean estas programadas en el Centro o en instalaciones físicas diferentes al mismo. Mi retiro de la acción de formación antes de su culminación total no genera obligación para <strong>GRI COMPANY S.A.S</strong> de devolución de los dineros ya pagados a la entidad.</p>
						<p>4. Presentar siempre las mejores condiciones de aseo y pulcritud en mi imagen personal.</p>
						<p>5. Respetar las normas de seguridad  y salud en el trabajo de todas las actividades que se realicen.</p>
						<p>6. Utilizar la indumentaria y los elementos de protección personal establecidos para el ingreso al ambiente de aprendizaje respectivo.</p>
						<p>7. Proyectar decorosamente la imagen corporativa de <strong>GRI COMPANY S.A.S</strong>  en mis actuaciones, dentro y fuera de la Entidad, asumiendo una actitud ética en cada una de mis acciones.</p>
						<p>8. Cuidar y no atentar contra las instalaciones, infraestructura,  equipos, muebles y todos los elementos de propiedad de la Entidad, así como asumir y compartir la responsabilidad en caso de pérdida o daño de los mismos y de los materiales de formación que se requieren para el aprendizaje.</p>
						<p>9. Portar en todo momento el carné de identificación institucional en sitio visible.</p>
						<p>10. Respetar la diversidad de género, edad, etnia, credo, religión, ideología, procedencia y ocupación, de todos los integrantes de la comunidad educativa, manteniendo un trato cordial.</p>
						<p>11. Registrar y mantener actualizados mis datos personales en los aplicativos informáticos que  <strong>GRI COMPANY S.A.S</strong>  determine y actuar como veedor del registro oportuno de las situaciones académicas que se presenten.</p>
						<p>12. Aceptar las directrices de comportamiento y respeto en el uso de las tecnologías de la información y comunicación ( TIC) de <strong>GRI COMPANY S.A.S</strong>.</p>
						<p>13. Acatar las normas de comportamiento, manteniendo en todo momento y espacio institucional un trato respetuoso, sin exceder bajo ninguna circunstancia los límites de las expresiones físico-afectivas y socio-afectivas.</p>
						<p>14. No realizar ni apoyar actos que limiten o afecten el derecho a la educación o la locomoción de la comunidad educativa de <strong>GRI COMPANY S.A.S</strong>, como impedir el acceso a funcionarios y aprendices a los centros de formación y demás instalaciones del <strong>GRI COMPANY S.A.S</strong>.</p>
						<p>15. aceptar que el pago de las acciones de formación que se ha realizado no constituye obligación de certificación para <strong>GRI COMPANY S.A.S</strong> sin haber logrado los objetivos educacionales y ganado las evaluaciones que me sean aplicadas.</p>
						<p>16. A presentar soporte valido en caso de no asistencia por motivos de fuerza mayor.</p>
						<p>17. A pagar por la no asistencia sin justificación un cobro adicional conocido.</p>
						<p>18. Manifiesto estar informado y acepto el tratamiento de datos establecido por <strong>GRI COMPANY S.A.S</strong>.</p>
						<p><strong>NOTA:</strong><br>
						<strong>1 GRI COMPANY S.A.S</strong> cuenta con una política de tratamiento y protección de datos personales que está a su disposición en el momento que lo requiera dicha política está conforme a lo dispuesto en la ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen.
						</p>
						<p><strong>2. El APRENDIZ</strong> da fe sobre su vigente  afiliación a  EPS y ARL ya que entiende y acepta que <strong>GRI COMPANY S.A.S</strong> en cumplimiento con todos los requisitos exigidos por la normatividad vigente para trabajo seguro en alturas (Resolución 4272 del 2021  y demás disposiciones legales aplicables) debe  verificar y garantizar su cumplimiento.</p>
						<p><strong>3. GRI COMPANY S.A.S</strong> no responde por accidentes causados  por actos y comportamientos inseguros del aprendiz dentro de la institución realizados por cuenta y riesgo de este o sin aprobación del entrenador.</p>
						<p>La institución ha establecido unas normas de convivencia y seguridad, enmarcadas en un Código del Buen Comportamiento, el cual me han comunicado claramente, lo entendí  y soy consciente  de la responsabilidad del cumplimiento del mismo.<br>
							Si el estudiante no cumple con el Código de Buen Comportamiento, será motivo de suspensión de las clases y se informará a la Empresa donde labora.</p>
						<p><strong>Acepto.</strong></p>
						<p style="text-align:left"><strong>FIRMA DEL APRENDIZ:</strong></p>
					</td>
				</tr>
				<tr>
					<td  height="150">
						<img src="' . $firma . '" alt="" width="400">
					</td>
				</tr>
			</table>';

	$html .= '<table cellspacing="0" cellpadding="0" border="1">';

	$sesiones = $db
		->where('Id_al', $Id)
		->objectBuilder()->get('registro_alturas_sesiones');

	if ($db->count > 0) {
		foreach ($sesiones as $rs) {
			$fecha = explode('-', $rs->fecha_as);
			$html .= '<tr>
					<td align="center" width="100" class="sesion"><br><br><strong>SESION ' . $rs->sesion_as . '</strong><br></td>
					<td align="center" width="50" ><br><br><strong>' . $fecha[2] . '</strong><br></td>
					<td align="center" width="70" ><br><br><strong>' . $fecha[1] . '</strong><br></td>
					<td align="center" width="80" ><br><br><strong>' . $fecha[0] . '</strong><br></td>
					<td width="350"><img src="' . $firma . '" alt="" width="150"></td>
				</tr>';
		}
	}

	$html .= '</table>';

	$html .= '<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td height="40"></td>
				</tr>
				<tr>
					<td align="center"><strong><u>CALIFICACIÓN CURSO REALIZADO</u></strong></td>
				</tr>
				<tr>
					<td height="6"></td>
				</tr>
			</table>';


	$html .= '<table cellspacing="0" cellpadding="0" border="1">
				<tr>
					<td align="center" class="sesion" width="85"></td>
					<td align="center" class="sesion" width="70"><br><br><strong>TEÓRICA</strong><br></td>
					<td align="center" class="sesion" width="70"><br><br><strong>PRÁCTICA</strong><br></td>
					<td align="center" class="sesion" width="70"><br><br><strong>TOTAL</strong><br></td>
					<td align="center" class="sesion" width="70"><br><br><strong>APROBO</strong><br></td>
					<td align="center" class="sesion" width="140"><br><br><strong>FIRMA<br>INSTRUCTOR</strong><br></td>
					<td align="center" class="sesion" width="140"><br><br><strong>FIRMA DEL <br>APRENDIZ</strong><br></td>
				</tr>
				<tr>
					<td align="center" class="sesion" rowspan="2"><br><br><strong>EVALUACIÓN</strong><br></td>
					<td rowspan="2" align="center"><br><br>' . $cal_teorica . '</td>
					<td rowspan="2" align="center"><br><br>' . $cal_practica . '</td>
					<td rowspan="2" align="center"><br><br>' . $cal_total . '</td>
					<td><br>  SI:  ' . $aprobo_s . '<br></td>
					<td rowspan="2">
						<img src="' . $firma_in . '" alt="">
					</td>
					<td rowspan="2">
						<img src="' . $firma . '" alt="">
					</td>
				</tr>
				<tr>
					<td><br>  NO:  ' . $aprobo_n . '<br></td>
				</tr>
			</table>';


	$pdf->writeHTML($estilo . $html, true, false, false, false, '');

	$pdf->Output('Ficha_aprendiz.pdf', 'I');
} else {
	print_r('No se encuentra el registro');
}
