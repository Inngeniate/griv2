<?php
require "libs/conexion.php";
$registro = $_GET['registro'];
$registro = explode('-', $registro);
$id = $registro[0];
$identifi = $registro[1];

$bus = $db
    ->where('Id', $id)
    ->where('numero_ident', $identifi)
    ->objectBuilder()->get('registros');

$res = $bus[0];

if ($res->foto_ct != '' || $res->capacitacion == 'ALTURAS') {
    $nombre     = $res->nombres . ' ' . $res->apellidos;
    $documento  = $res->numero_ident;
    $expedicion = $res->fecha_inicio;
    $expedicion = date_create($expedicion);
    $expedicion = date_format($expedicion, 'd-m-Y');
    $vigencia   = '';
    if ($res->fecha_vigencia != '0000-00-00') {
        $vigencia   = $res->fecha_vigencia;
        $vigencia   = date_create($vigencia);
        $vigencia   = date_format($vigencia, 'd-m-Y');
    }

    $formacion = '';

    $cursos = $db
        ->where('Id_ct', $res->certificado)
        ->objectBuilder()->get('certificaciones');

    if ($db->count > 0) {
        $formacion = $cursos[0]->nombre;
    }

    require_once 'libs/tcpdf.php';
    require_once 'libs/fpdi/fpdi.php';

    $exa = new FPDI();
    $exa->setSourceFile('libs/pl_carnet_altura.pdf');
    $tplIdx = $exa->importPage(1, '/MediaBox');
    $exa->SetPrintHeader(false);

    $exa->addFont('conthrax', '', 'conthrax.php');
    $exa->addFont('ubuntucondensed', '', 'ubuntucondensed.php');
    $exa->SetFont('conthrax', '', 8);
    // $exa->SetFont('arial', '', 6);
    $estilo = '<style>
                .nm{
                    color: #000;
                    font-size: 5.5
                }
                .bl{
                    color: #000;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    color: #000;
                    font-family: arial;
                    font-size: 5;
                }
            </style>';

    $exa->AddPage();
    $exa->useTemplate($tplIdx);
    $exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $exa->setJPEGQuality(100);

    /// girar el contenido
    $exa->SetXY(50, 40);
    // $exa->StartTransform();
    // $exa->Rotate(90);
    /////

    $txt = '<table border="0" width="200px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">MIN.TRABAJO 08SE2019220000000045994</strong></td></tr></table>';

    $exa->SetXY(33, 26);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="200px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">ICONTEC N° CS-CER709434</strong></td></tr></table>';

    $exa->SetXY(33, 29);
    $exa->WriteHTML($estilo . $txt);
    $txt = '<table border="0" width="200px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">MIN.EDUCACION  1795</strong></td></tr></table>';

    $exa->SetXY(33, 32);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="260px" cellpadding="-1" cellspacing="0"><tr><td><strong class="nm">' . $nombre . '</strong></td></tr></table>';

    $exa->SetXY(44, 38.5);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $documento . '</strong></td></tr></table>';

    $exa->SetXY(44, 42.5);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="250px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl">' . $formacion . '</strong></td></tr></table>';

    $exa->SetXY(43, 46);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $expedicion . '</strong></td></tr></table>';

    $exa->SetXY(53, 52);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $vigencia . '</strong></td></tr></table>';

    $exa->SetXY(95, 52);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="250px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl2">Lic. seguridad y salud en el trabajo Res. 3753 de 2013</strong></td></tr></table>';

    $exa->SetXY(54, 58);
    $exa->WriteHTML($estilo . $txt);



    if ($res->foto_ct != '') {
        $exa->Image(substr($res->foto_ct, 3), 41.6, 47, 20);
    }

    $exa->StopTransform();
    // $exa->setSourceFile('libs/pl_carnet_altss.pdf');
    $tplIdx = $exa->importPage(2, '/MediaBox');
    $exa->AddPage();
    $exa->useTemplate($tplIdx);

    $exa->Output('carnet.pdf', 'I');
} else {
    echo 'Error al generar el carnet: Falta foto en el registro.';
}
