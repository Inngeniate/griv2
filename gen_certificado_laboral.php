<?php
require "libs/conexion.php";
$registro = $_REQUEST['id'];

$bus = $db
    ->where('Id_cl', $registro)
    ->objectBuilder()->get('certificados_laborales');

$res = $bus[0];

$nombre    = $res->nombre_cl;
$documento = $res->identificacion_cl;
$cargo     = $res->cargo_cl;
$salario   = $res->salario_cl;
$salario_f = number_format($salario, 0, '', '.');
$labora    = $res->labora_cl;
$hasta     = $res->hasta_cl;
$expedida  = $res->expedida_cl;
$identificacion = number_format($documento, 0, '', '.');

$V = new EnLetras();
$salio_letras = $V->ValorEnLetras($salario, "pesos");

require_once 'libs/tcpdf.php';
require_once 'libs/fpdi/fpdi.php';

$exa = new FPDI();
$exa->setSourceFile('libs/plantilla_certificado_laboral.pdf');


$tplIdx = $exa->importPage(1, '/MediaBox');
$exa->SetPrintHeader(false);

$fontname = TCPDF_FONTS::addTTFfont('libs/fonts/arial-bold.ttf');
$exa->addFont('arialb', '', 10, '', false);
$exa->SetFont('arial', '', 10);
$estilo = '<style>
                .bl{
                    color: #000;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    color: #000;
                    font-family: arial;
                    font-size: 5;
                }
                .b{
                    font-family: arialb;
                }

            </style>';

$exa->AddPage();
$exa->useTemplate($tplIdx);
$exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
$exa->setJPEGQuality(100);

$meses['01'] = 'Enero';
$meses['02'] = 'Febrero';
$meses['03'] = 'Marzo';
$meses['04'] = 'Abril';
$meses['05'] = 'Mayo';
$meses['06'] = 'Junio';
$meses['07'] = 'Julio';
$meses['08'] = 'Agosto';
$meses['09'] = 'Septiembre';
$meses['10'] = 'Octubre';
$meses['11'] = 'Noviembre';
$meses['12'] = 'Diciembre';

$txt = '<table border="0" width="495px" cellpadding="-1" cellspacing="0"><tr><td>Villavicencio, ' . date('d') . ' de ' . $meses[date('m')] . ' de ' . date('Y') . ' </td></tr></table>';

$exa->SetXY(30, 30);
$exa->WriteHTML($estilo . $txt);

$labora = explode('-', $labora);
$labora = $labora[2] . ' de ' . $meses[$labora[1]] . ' del ' . $labora[0];

if ($hasta != '' && $hasta < date('Y-m-d')) {
    $hasta = explode('-', $hasta);
    $hasta = $hasta[2] . ' de ' . $meses[$hasta[1]] . ' del ' . $hasta[0];

    $labora = 'laboró en la empresa  desde  el ' . $labora . '  hasta el ' . $hasta;
} else {
    $labora = 'labora  en la empresa  desde  el ' . $labora . ' a la fecha';
}

$txt = '<table border="0" width="495px" cellpadding="-1" cellspacing="0"><tr><td>Certificamos que el (la) señor (a) <b class="b">' . $nombre . '</b>, identificado (a)  con  cedula  de  ciudadanía <b class="b">No. ' . $identificacion . '</b> expedida  en  la  ciudad  de ' . $expedida . ', ' . $labora . ', desempeñando el cargo  de <b class="b">' . $cargo . '</b> devengando  un  salario  mensual  de <b class="b">$' . $salario_f . ' (' . $salio_letras . ') m/cte.</b></td></tr></table>';

$exa->SetXY(30, 75);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="495px" cellpadding="-1" cellspacing="0"><tr><td>Según solicitud realizada por (la) señor (a) <b class="b">' . $nombre . '</b>con <b class="b">C.C. No. ' . $identificacion . '</b></td></tr></table>';

$exa->SetXY(30, 105);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="495px" cellpadding="-1" cellspacing="0"><tr><td>Este certificado ha sido generado a través del sistema auto consulta web de la compañía, por  lo  tanto,  la  utilización  de  la  información  en  el,  es  responsabilidad  del  titular.  Para  su verificación debe comunicarse al Tel: +57 8 6826839, Cel.: 3143257703 de lunes a jueves en un horario de 8 –12 am.</td></tr></table>';

$exa->SetXY(30, 125);
$exa->WriteHTML($estilo . $txt);


$txt = '<table border="0" width="495px" cellpadding="-1" cellspacing="0"><tr><td>Sugerimos al solicitante del certificado, conserve el original y acreditar su experiencia con una copia.</td></tr></table>';

$exa->SetXY(30, 146);
$exa->WriteHTML($estilo . $txt);


$exa->Output('certificado_laboral_' . $documento . '.pdf', 'I');

class EnLetras
{
    public $Void = "";
    public $SP   = " ";
    public $Dot  = ".";
    public $Zero = "0";
    public $Neg  = "Menos";

    public function ValorEnLetras($x, $Moneda)
    {
        $s     = "";
        $Ent   = "";
        $Frc   = "";
        $Signo = "";

        if (floatVal($x) < 0) {
            $Signo = $this->Neg . " ";
        } else {
            $Signo = "";
        }

        if (intval(number_format($x, 2, '.', '')) != $x) //<- averiguar si tiene decimales
        {
            $s = number_format($x, 2, '.', '');
        } else {
            $s = number_format($x, 2, '.', '');
        }

        $Pto = strpos($s, $this->Dot);

        if ($Pto === false) {
            $Ent = $s;
            $Frc = $this->Void;
        } else {
            $Ent = substr($s, 0, $Pto);
            $Frc = substr($s, $Pto + 1);
        }

        if ($Ent == $this->Zero || $Ent == $this->Void) {
            $s = "Cero ";
        } elseif (strlen($Ent) > 7) {
            $s = $this->SubValLetra(intval(substr($Ent, 0, strlen($Ent) - 6))) .
                "Millones " . $this->SubValLetra(intval(substr($Ent, -6, 6)));
        } else {
            $s = $this->SubValLetra(intval($Ent));
        }

        if (substr($s, -9, 9) == "Millones " || substr($s, -7, 7) == "Millón ") {
            $s = $s . "de ";
        }

        $s = $s . $Moneda;

        if ($Frc != $this->Void) {
            $s = $s . " ";
            // $s = $s . " " . $Frc . "/100";
        }
        $letrass = $Signo . $s . " M.N.";
        return ($Signo . $s);
    }

    public function SubValLetra($numero)
    {
        $Ptr = "";
        $n   = 0;
        $i   = 0;
        $x   = "";
        $Rtn = "";
        $Tem = "";

        $x = trim("$numero");
        $n = strlen($x);

        $Tem = $this->Void;
        $i   = $n;

        while ($i > 0) {
            $Tem = $this->Parte(intval(substr($x, $n - $i, 1) .
                str_repeat($this->Zero, $i - 1)));
            if ($Tem != "Cero") {
                $Rtn .= $Tem . $this->SP;
            }

            $i = $i - 1;
        }

        //--------------------- GoSub FiltroMil ------------------------------
        $Rtn = str_replace(" Mil Mil", " Un Mil", $Rtn);
        while (1) {
            $Ptr = strpos($Rtn, "Mil ");
            if (!($Ptr === false)) {
                if (!(strpos($Rtn, "Mil ", $Ptr + 1) === false)) {
                    $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr);
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        //--------------------- GoSub FiltroCiento ------------------------------
        $Ptr = -1;
        do {
            $Ptr = strpos($Rtn, "Cien ", $Ptr + 1);
            if (!($Ptr === false)) {
                $Tem = substr($Rtn, $Ptr + 5, 1);
                if ($Tem == "M" || $Tem == $this->Void);
                else {
                    $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr);
                }
            }
        } while (!($Ptr === false));

        //--------------------- FiltroEspeciales ------------------------------
        $Rtn = str_replace("Diez Un", "Once", $Rtn);
        $Rtn = str_replace("Diez Dos", "Doce", $Rtn);
        $Rtn = str_replace("Diez Tres", "Trece", $Rtn);
        $Rtn = str_replace("Diez Cuatro", "Catorce", $Rtn);
        $Rtn = str_replace("Diez Cinco", "Quince", $Rtn);
        $Rtn = str_replace("Diez Seis", "Dieciseis", $Rtn);
        $Rtn = str_replace("Diez Siete", "Diecisiete", $Rtn);
        $Rtn = str_replace("Diez Ocho", "Dieciocho", $Rtn);
        $Rtn = str_replace("Diez Nueve", "Diecinueve", $Rtn);
        $Rtn = str_replace("Veinte Un", "Veintiun", $Rtn);
        $Rtn = str_replace("Veinte Dos", "Veintidos", $Rtn);
        $Rtn = str_replace("Veinte Tres", "Veintitres", $Rtn);
        $Rtn = str_replace("Veinte Cuatro", "Veinticuatro", $Rtn);
        $Rtn = str_replace("Veinte Cinco", "Veinticinco", $Rtn);
        $Rtn = str_replace("Veinte Seis", "Veintiseís", $Rtn);
        $Rtn = str_replace("Veinte Siete", "Veintisiete", $Rtn);
        $Rtn = str_replace("Veinte Ocho", "Veintiocho", $Rtn);
        $Rtn = str_replace("Veinte Nueve", "Veintinueve", $Rtn);

        //--------------------- FiltroUn ------------------------------
        if (substr($Rtn, 0, 1) == "M") {
            $Rtn = "Un " . $Rtn;
        }

        //--------------------- Adicionar Y ------------------------------
        for ($i = 65; $i <= 88; $i++) {
            if ($i != 77) {
                $Rtn = str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn);
            }
        }
        $Rtn = str_replace("*", "a", $Rtn);
        return ($Rtn);
    }

    public function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr)
    {
        $x = substr($x, 0, $Ptr) . $NewWrd . substr($x, strlen($OldWrd) + $Ptr);
    }

    public function Parte($x)
    {
        $Rtn = '';
        $t   = '';
        $i   = '';
        do {
            switch ($x) {
                case 0:
                    $t = "Cero";
                    break;
                case 1:
                    $t = "Un";
                    break;
                case 2:
                    $t = "Dos";
                    break;
                case 3:
                    $t = "Tres";
                    break;
                case 4:
                    $t = "Cuatro";
                    break;
                case 5:
                    $t = "Cinco";
                    break;
                case 6:
                    $t = "Seis";
                    break;
                case 7:
                    $t = "Siete";
                    break;
                case 8:
                    $t = "Ocho";
                    break;
                case 9:
                    $t = "Nueve";
                    break;
                case 10:
                    $t = "Diez";
                    break;
                case 20:
                    $t = "Veinte";
                    break;
                case 30:
                    $t = "Treinta";
                    break;
                case 40:
                    $t = "Cuarenta";
                    break;
                case 50:
                    $t = "Cincuenta";
                    break;
                case 60:
                    $t = "Sesenta";
                    break;
                case 70:
                    $t = "Setenta";
                    break;
                case 80:
                    $t = "Ochenta";
                    break;
                case 90:
                    $t = "Noventa";
                    break;
                case 100:
                    $t = "Cien";
                    break;
                case 200:
                    $t = "Doscientos";
                    break;
                case 300:
                    $t = "Trescientos";
                    break;
                case 400:
                    $t = "Cuatrocientos";
                    break;
                case 500:
                    $t = "Quinientos";
                    break;
                case 600:
                    $t = "Seiscientos";
                    break;
                case 700:
                    $t = "Setecientos";
                    break;
                case 800:
                    $t = "Ochocientos";
                    break;
                case 900:
                    $t = "Novecientos";
                    break;
                case 1000:
                    $t = "Mil";
                    break;
                case 1000000:
                    $t = "Millón";
                    break;
            }

            if ($t == $this->Void) {
                if ($i == '') {
                    $i = 0;
                }
                $i = $i + 1;
                $x = $x / 1000;
                if ($x == 0) {
                    $i = 0;
                }
            } else {
                break;
            }
        } while ($i != 0);

        $Rtn = $t;
        switch ($i) {
            case 0:
                $t = $this->Void;
                break;
            case 1:
                $t = " Mil";
                break;
            case 2:
                $t = " Millones";
                break;
            case 3:
                $t = " Billones";
                break;
        }
        return ($Rtn . $t);
    }
}
