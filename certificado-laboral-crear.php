<?php
	session_start();
	if (!$_SESSION['usuloggri']) header('Location: admin');
	require("libs/conexion.php");

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Certificado Laboral | Gricompany</title>
		<link rel="stylesheet" href="css/slider.css" />
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/style-menu.css" />
		<link rel="stylesheet" type="text/css" href="css/default.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="css/cropper.css" />
		<link rel="stylesheet" href="css/msj.css" />
		<script src="js/modernizr.custom.js"></script>
	</head>
	<body>
		<div class="Contenedor">
			<header>
				<?php include("menu2.php"); ?>
			</header>
			<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
			<script type="text/javascript" src="js/script-menu.js"></script>
		</div>
		<section>
			<div class="Contenido-admin">
				<div class="Contenido-admin-izq">
					<h2>Certificado Laboral</h2>
					<form id="registro">
						<div class="Registro">
							<div class="Registro-der">
								<label>Nombre *</label>
								<input type="text" placeholder="Nombre" name="registro[nombre]" class="nnumb" required>
								<label>Lugar expedición cedula de ciudadania *</label>
								<input type="text" placeholder="Lugar expedición cedula de ciudadania" name="registro[expedicion]" required>
								<label>Cargo *</label>
								<input type="text" placeholder="Cargo" name="registro[cargo]" required>
							</div>
							<div class="Registro-der">
								<label>Documento de Identidad *</label>
								<input type="text" placeholder="Documento de Identidad" name="registro[identificacion]"
								class="ntext" required>
								<label>Labora desde *</label>
								<input type="date" placeholder="Labora desde" name="registro[labora]" required>
								<label>Salario *</label>
								<input type="text" placeholder="Salario" name="registro[salario]" class="ntext" required>
							</div>
							<hr>
							<br>
							<br>
							<input type="submit" value="Generar Certificado">
						</div>
					</form>
				</div>
			</div>
		</section>
		<script type="text/javascript" src="js/cropper.min.js"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script type="text/javascript" src="js/crear_certificado_laboral.js?<?php echo time()  ?>"></script>
	</body>
</html>
