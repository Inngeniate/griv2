<?php
	session_start();
	if (isset($_SESSION['usuloggri'])) {
		header('Location: home');
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
<meta name="keywords" lang="es" content="">
<meta name="robots" content="All">
<meta name="description" lang="es" content="">
<title>Certificaciones | Gricompany</title>
<link rel="stylesheet" href="css/slider.css" />
<link rel="stylesheet" href="css/stylesheet.css" />
<link rel="stylesheet" href="css/style-menu.css" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<link rel="stylesheet" href="css/msj.css" />
<script src="js/modernizr.custom.js"></script>
</head>
<body>
<div class="Logueo">
	<div class="Logueo-interno">
		<img src="images/logo.png">
		<h2>Iniciar Sesión</h2>
		<p>Ingrese usuario y contraseña para continuar.</p>
		<form id="logueo">
			<label>Usuario:</label>
			<input type="text" placeholder="Usuario" name="nombre_u"  required>
			<label>Contraseña:</label>
			<input type="password" placeholder="Contraseña" name="pass_u" required>
			<input type="submit" value="Iniciar Sesión">
		</form>
	</div>
</div>
<!-- //<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/script_login.js"></script>
</body>
</html>