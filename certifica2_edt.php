<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
else {
	require("libs/conexion.php");

	$iden = $_GET['certificado'];

	$ensayo = $db
		->where('Id_en', $iden)
		->objectBuilder()->get('certificado_ensayo');

	$res = $ensayo[0];

	preg_match_all('!\d+!', $res->certificado_en, $numero);

	$numero = implode('', $numero[0]);

	$lista_certificado = '';
	for ($i = 1; $i < 101; $i++) {
		if (strlen((string)$i) < 2)
			$i = '0' . $i;

		if ($i == $numero)
			$option = 'selected';
		else
			$option = '';
		$lista_certificado .= '<option ' . $option . '>' . $i . '</option>';
	}

	$lista_vendedores = '';

	$vendedores = $db
		->orderBy('nombre_v', 'ASC')
		->objectBuilder()->get('vendedores');

	if ($db->count > 0) {
		foreach ($vendedores as $rsv) {
			$lista_vendedores .= '<option value="' . $rsv->Id_v . '" ' . ($rsv->Id_v == $res->vendedor_en ? "selected" : "") . ' >' . $rsv->nombre_v . '</option>';
		}
	}
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
		}

		.img-prev {
			width: 450px;
			height: 100px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.img-prev2 {
			width: 320px;
			height: 220px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="ensayo">
				<div class="Contenido-admin-izq">
					<h2>Editar Certificado</h2>
					<form id="ed_certificado">
						<div class="Registro">
							<div class="Registro-cent">
								<label>Tipo *</label>
								<select name="certifica[tipo]" id="tipo-certi" required>
									<?php
									switch ($res->tipo_en) {
										case '1':
											$option1 = 'selected';
											$option2 = '';
											$option3 = '';
											break;
										case '2':
											$option1 = '';
											$option2 = 'selected';
											$option3 = '';
											break;
										case '3':
											$option1 = '';
											$option2 = '';
											$option3 = 'selected';
											break;
									}
									?>
									<option value="1" <?php echo $option1 ?>>De ensayo no destructivo Quinta Rueda</option>
									<option value="2" <?php echo $option2 ?>>De ensayo no destructivo Linea de Vida</option>
									<option value="3" <?php echo $option3 ?>>De ensayo no destructivo king Pin</option>
								</select>
								<label>Reporte Nº *</label>
								<input type="text" placeholder="Reporte Nº" name="certifica[reporte]" value="<?php echo $res->reporte_en ?>" class="ncertificado" readonly>
								<label>Certificado *</label>
								<select name="certifica[certificado]" id="sel_certificado" required>
									<?php echo $lista_certificado ?>
								</select>
							</div>
							<div class="Registro-der">
								<label>Empresa *</label>
								<input type="text" placeholder="Empresa" name="certifica[empresa]" value="<?php echo $res->empresa_en ?>" required>
								<label>Propietario *</label>
								<input type="text" placeholder="Propietario" name="certifica[propietario]" value="<?php echo $res->propietario_en ?>" required>
								<label>Contacto *</label>
								<input type="text" placeholder="Contacto" name="certifica[contacto]" value="<?php echo $res->contacto_en ?>" required>
								<label>Ciudad *</label>
								<input type="text" placeholder="Ciudad" name="certifica[ciudad]" value="<?php echo $res->ciudad_en ?>" class="auto_ciu" required>
								<label>Fecha Inspección*</label>
								<input type="date" placeholder="Fecha" name="certifica[fecha]" value="<?php echo $res->fecha_en ?>" required>
								<label>Hora de inspeccion *</label>
								<input type="text" placeholder="Hora de inspeccion" name="certifica[hinspeccion]" value="<?php echo $res->hinspeccion_en ?>" required>
								<label>Horas Stand By *</label>
								<input type="text" placeholder="Horas Stand By" name="certifica[hstandby]" value="<?php echo $res->hstandby_en ?>" required>
								<?php
								if ($res->tipo_en == 1) {
									if ($res->cadenas_en == 1) {
										$cadena1 = 'selected';
										$cadena2 = '';
									} else {
										$cadena1 = '';
										$cadena2 = 'selected';
									}
								?>
									<div class="ct_kp">
										<label>CADENAS</label>
										<select name="certifica[cadenas]">
											<option value="1" <?php echo $cadena1  ?>>Si</option>
											<option value="0" <?php echo $cadena2  ?>>No</option>
										</select>
									</div>
								<?php
								}
								?>
								<label>Revisado por *</label>
								<input type="text" placeholder="Revisado por" name="certifica[revisado]" value="<?php echo $res->revisado_en ?>" required>
								<?php
								$lista_inspectores = '';

								$inspectores = $db
									->objectBuilder()->get('inspectores');

								foreach ($inspectores as $resin) {
									if ($resin->Id_ins == $res->inspector_en)
										$lista_inspectores .= '<option value="' . $resin->Id_ins . '" selected>' . $resin->nombre_ins . '</option>';
									else
										$lista_inspectores .= '<option value="' . $resin->Id_ins . '" >' . $resin->nombre_ins . '</option>';
								}
								?>
								<label>Inspector *</label>
								<select name="certifica[inspector]" required>
									<option>Selecciona</option>
									<?php echo $lista_inspectores ?>
								</select>
								<label>Vendedor *</label>
								<select name="certifica[vendedor]" required>
									<option>Selecciona</option>
									<?php echo $lista_vendedores ?>
								</select>
							</div>
							<div class="Registro-der">
								<label>Placa Remolque*</label>
								<input type="text" placeholder="Placa Remolque" name="certifica[placaremolque]" value="<?php echo $res->placaremolque_en ?>" required>
								<label>Serie *</label>
								<input type="text" placeholder="Serie" name="certifica[serie]" value="<?php echo $res->serie_en ?>" required>
								<label>Marca *</label>
								<input type="text" placeholder="Marca" name="certifica[marca]" value="<?php echo $res->marca_en ?>" required>
								<label>Modelo *</label>
								<input type="text" placeholder="Modelo" name="certifica[modelo]" value="<?php echo $res->modelo_en ?>" required>
								<label>Tipo de Sitema*</label>
								<input type="text" placeholder="Tipo de Sitema" name="certifica[sistema]" value="<?php echo $res->sistema_en ?>" required>
								<label>DIM King Pin </label>
								<input type="text" placeholder="DIM King Pin" name="certifica[kingpin]" value="<?php echo $res->kingpin_en ?>">
								<label>Placa *</label>
								<input type="text" placeholder="Placa" name="certifica[placa]" value="<?php echo $res->placa_en ?>" required>
								<?php
								if ($res->tipo_en == 1) {
									if ($res->ratches_en == 1) {
										$ratches1 = 'selected';
										$ratches2 = '';
									} else {
										$ratches1 = '';
										$ratches2 = 'selected';
									}
								?>
									<div class="ct_kp">
										<label>RATCHES </label>
										<select name="certifica[ratches]">
											<option value="1" <?php echo $ratches1 ?>>Si</option>
											<option value="0" <?php echo $ratches2 ?>>No</option>
										</select>
									</div>
								<?php
								}
								?>
								<label>Recibido por *</label>
								<input type="text" placeholder="Recibido por" name="certifica[recibido]" value="<?php echo $res->recibido_en ?>" required>
								<label>Fecha de Vencimiento</label>
								<input type="date" placeholder="Fecha" name="certifica[vencimiento]" value="<?php echo $res->vencimiento_en ?>" required>
							</div>
							<div class="Registro-cent">
								<label>Restricciones *</label>
								<textarea placeholder="Restricciones" name="certifica[restricciones]"><?php echo $res->restricciones_en ?></textarea>
							</div>
							<br>
							<br>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev" src="<?php echo substr($res->fotoplaca_en, 3) . '?' . time()  ?>">
									<button type="button" class="placa" id="foto1">PLACA CABEZOTE</button>
								</div>
								<div class="img-placa">
									<img class="img-prev" src="<?php echo substr($res->fotoquinta_en, 3) . '?' . time()   ?>">
									<button type="button" class="placa" id="foto2">FOTO QUINTA RUEDA</button>
								</div>
							</div>
							<div class="Registro-der">
								<div class="img-placa">
									<img class="img-prev" src="<?php echo substr($res->fototanque_en, 3) . '?' . time()   ?>">
									<button type="button" class="placa" id="foto3">PLACA TANQUE</button>
								</div>
							</div>
							<input type="hidden" name="certifica[id]" value="<?php echo $iden ?>">
							<input type="submit" value="Guardar Certificado">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/certifica_edt.js"></script>
</body>

</html>
