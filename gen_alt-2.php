<?php
require "libs/conexion.php";
$registro = $_GET['registro'];

$bus = $db
    ->where('Id', $registro)
    ->objectBuilder()->get('registros');

$res = $bus[0];

if ($res->foto_ct != '' || $res->capacitacion == 'ALTURAS' || $res->capacitacion == 'ESPACIOS CONFINADOS' || $res->capacitacion == 'ALTURAS RES. 4272') {
    $nombre = $res->nombre_primero . ' ' . $res->nombre_segundo . ' ' . $res->apellidos;
    $tipoid     = $res->tipo_ident;
    $documento  = $tipoid . '. ' . $res->numero_ident;
    $expedicion = $res->fecha_inicio;
    $expedicion = date_create($expedicion);
    $expedicion = date_format($expedicion, 'd-m-Y');
    $vigencia   = '';

    if ($res->fecha_vigencia != '0000-00-00') {
        $vigencia   = $res->fecha_vigencia;
        $vigencia   = date_create($vigencia);
        $vigencia   = date_format($vigencia, 'd-m-Y');
    }

    $formacion = '';

    $cursos = $db
        ->where('Id_ct', $res->certificado)
        ->objectBuilder()->get('certificaciones');

    if ($db->count > 0) {
        $rsc       = $cursos[0];
        $formacion = $rsc->nombre;
    }

    require_once 'libs/tcpdf.php';
    require_once 'libs/fpdi/fpdi.php';

    $exa = new FPDI();


    if ($res->cargafoto == 1 && $res->foto_ct != '') {
        if ($vigencia  != '') {
            $exa->setSourceFile('libs/pl_carnet_alt_c.pdf');
        } else {
            $exa->setSourceFile('libs/pl_carnet_alt_d.pdf');
        }
    } else {
        if ($vigencia  != '') {
            $exa->setSourceFile('libs/pl_carnet_alt_a.pdf');
        } else {
            $exa->setSourceFile('libs/pl_carnet_alt_b.pdf');
        }
    }

    $tplIdx = $exa->importPage(1, '/MediaBox');
    $exa->SetPrintHeader(false);

    $exa->addFont('conthrax', '', 'conthrax.php');
    $exa->addFont('ubuntucondensed', '', 'ubuntucondensed.php');
    $exa->SetFont('conthrax', '', 8);

    $estilo = '<style>
                .nm{
                    color: #000;
                    font-size: 5.5;
                    font-family: arial;
                }
                .bl{
                    color: #000;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    font-family: arial;
                    font-size: 5;
                }
                .rojo{
                    color: #d40e22;
                }
            </style>';

    $exa->SetMargins(0, 0, 0);
    $exa->SetAutoPageBreak(true, 0);

    $exa->AddPage();
    $exa->useTemplate($tplIdx, null, null, 0, 0, true);
    $exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $exa->setJPEGQuality(100);

    /// girar el contenido
    $exa->SetXY(50, 40);
    /////

    $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="nm">' . $nombre . '</strong></td></tr></table>';

    $exa->SetXY(17.5, 25.9);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">' . $documento . '</strong></td></tr></table>';

    $exa->SetXY(7.5, 28);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl rojo">' . $formacion . '</strong></td></tr></table>';

    $exa->SetXY(7.5, 35);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $expedicion . '</strong></td></tr></table>';

    $exa->SetXY(21.5, 41.9);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $vigencia . '</strong></td></tr></table>';

    $exa->SetXY(49.5, 41.9);
    $exa->WriteHTML($estilo . $txt);


    if ($res->foto_ct != '') {
        $exa->Image(substr(str_replace(' ', '', $res->foto_ct), 3), 62, 12, 20);

        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(222, 48, 53));

        $exa->Line(61.75, 12, 82.25, 12, $style);
        $exa->Line(62, 12, 62, 39, $style);

        $exa->Line(61.75, 39, 82.25, 39, $style);
        $exa->Line(82, 12, 82, 39, $style);
    }

    $exa->StopTransform();

    $tplIdx = $exa->importPage(2, '/MediaBox');
    $exa->AddPage();
    // $exa->useTemplate($tplIdx);
    $exa->useTemplate($tplIdx, '', '', '', '', true);

    $exa->Output('carnet.pdf', 'I');
} else {
    echo 'Error al generar el carnet: Falta foto en el registro.';
}
