<div class="ensayo">
	<div class="Contenido-admin-izq">
		<h2>Crear Certificados</h2>
		<form class="cre_certificado">
			<div class="Registro">
				<div class="Registro-cent">
					<label>Tipo *</label>
					<select name="certifica[tipo]" id="tipo-certi" required>
						<option value="1">De ensayo no destructivo Quinta Rueda</option>
						<option value="2">De ensayo no destructivo Linea de Vida</option>
						<option value="3">De ensayo no destructivo king Pin</option>
					</select>
					<label>Reporte Nº *</label>
					<input type="text" placeholder="Reporte Nº" name="certifica[reporte]" value="<?php echo $nensayo ?>" class="ncertificado" readonly>
					<label>Certificado *</label>
					<select name="certifica[certificado]" id="sel_certificado" required>
						<?php echo $lista_certificado ?>
					</select>
				</div>
				<div class="Registro-der">
					<label>Empresa *</label>
					<input type="text" placeholder="Empresa" name="certifica[empresa]" required>
					<label>Propietario *</label>
					<input type="text" placeholder="Propietario" name="certifica[propietario]" required>
					<label>Contacto *</label>
					<input type="text" placeholder="Contacto" name="certifica[contacto]" required>
					<label>Ciudad *</label>
					<input type="text" placeholder="Ciudad" name="certifica[ciudad]" class="auto_ciu" required>
					<label>Fecha Inspección*</label>
					<input type="date" placeholder="Fecha" name="certifica[fecha]" required>
					<label>Hora de inspeccion *</label>
					<input type="text" placeholder="Hora de inspeccion" name="certifica[hinspeccion]" required>
					<label>Horas Stand By *</label>
					<input type="text" placeholder="Horas Stand By" name="certifica[hstandby]" required>
					<div class="ct_kp">
						<label>CADENAS</label>
						<select name="certifica[cadenas]">
							<option value="1">Si</option>
							<option value="0">No</option>
						</select>
					</div>
					<label>Revisado por *</label>
					<input type="text" placeholder="Revisado por" name="certifica[revisado]" required>
					<label>Inspector *</label>
					<select name="certifica[inspector]" required>
						<option>Selecciona</option>
						<?php echo $lista_inspectores ?>
					</select>
					<label>Vendedor *</label>
					<select name="certifica[vendedor]" required>
						<option>Selecciona</option>
						<?php echo $lista_vendedores ?>
					</select>
				</div>
				<div class="Registro-der">
					<label>Placa Remolque*</label>
					<input type="text" placeholder="Placa Remolque" name="certifica[placaremolque]" required>
					<label>Serie *</label>
					<input type="text" placeholder="Serie" name="certifica[serie]" required>
					<label>Marca *</label>
					<input type="text" placeholder="Marca" name="certifica[marca]" required>
					<label>Modelo *</label>
					<input type="text" placeholder="Modelo" name="certifica[modelo]" required>
					<label>Tipo de Sitema*</label>
					<input type="text" placeholder="Tipo de Sitema" name="certifica[sistema]" required>
					<label>DIM King Pin </label>
					<input type="text" placeholder="DIM King Pin" name="certifica[kingpin]" >
					<label>Placa *</label>
					<input type="text" placeholder="Placa" name="certifica[placa]" required>
					<div class="ct_kp">
						<label>RATCHES </label>
						<select name="certifica[ratches]">
							<option value="1">Si</option>
							<option value="0">No</option>
						</select>
					</div>

					<label>Recibido por *</label>
					<input type="text" placeholder="Recibido por" name="certifica[recibido]" required>
					<label>Valido hasta *</label>
					<input type="date" placeholder="Fecha" name="certifica[vencimiento]" required>
				</div>
				<div class="Registro-cent">
					<label>Restricciones *</label>
					<textarea placeholder="Restricciones" name="certifica[restricciones]"></textarea>
				</div>
				<br>
				<br>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev">
						<button type="button" class="placa" id="foto1">PLACA CABEZOTE</button>
					</div>
					<div class="img-placa">
						<img class="img-prev">
						<button type="button" class="placa" id="foto2">FOTO QUINTA RUEDA</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev">
						<button type="button" class="placa" id="foto3">PLACA TANQUE</button>
					</div>
				</div>
				<input type="hidden" name="certifica[accion]" value="ensayo">
				<input type="submit" value="Guardar Certificado">
			</div>
		</form>
	</div>
</div>