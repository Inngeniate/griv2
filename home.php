<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");

$ls_entrenadores = '';

$entrenadores = $db
	->where('estado_en', 1)
	->orderBy('nombre_en', 'ASC')
	->objectBuilder()->get('entrenadores');

if ($db->count > 0) {
	foreach ($entrenadores as $entrenador) {
		$ls_entrenadores .= '<option value="' . $entrenador->Id_en . '" data-tipo="' . $entrenador->competencia_en . '">' . $entrenador->nombre_en . ' ' . $entrenador->apellido_en . '</option>';
	}
}

$ls_vendedores = '';

$vendedores = $db
	->orderBy('nombre_v', 'ASC')
	->objectBuilder()->get('vendedores');

if ($db->count > 0) {
	foreach ($vendedores as $vendedor) {
		$cursos = $db
			->where('vendedor_vc', $vendedor->Id_v)
			->objectBuilder()->get('vendedores_cursos');

		if ($db->count > 0) {
			$ls_vendedores .= '<option value="' . $vendedor->nombre_v . '" id="Ve-' . $vendedor->Id_v . '" data-direccion="' . $vendedor->direccion_v . '"  data-telefono="' . $vendedor->telefono_v . '" data-correo="' . $vendedor->correo_v . '">' . $vendedor->nombre_v . '</option>';
		}
	}
}

$ls_cursos = '';

$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 1)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos');

	$menu = $db
		->where('Id_m', $permisos_usuario[0]->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}


$ls_competencias = '';

$competencias = $db
	->where('activo_cp', 1)
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$ls_competencias .= '<option value="' . trim($rcp->nombre_cp) . '" data-nombre="' . $rcp->alias_cp . '">' . $rcp->nombre_cp . '</option>';
}

$ls_codigos = '';

$codigos_fichas = $db
	->where('en_uso < cantidad')
	->orWhere('competencia', 1)
	->objectBuilder()->get('fichas_ministerio');

foreach ($codigos_fichas as $rcf) {
	$competencias = $db
		->where('Id_cp', $rcf->competencia)
		->objectBuilder()->get('competencias');

	$rcp = $competencias[0];

	$disponibles = $rcf->cantidad - $rcf->en_uso;

	$ls_codigos .= '<option value="' . $rcf->Id . '" data-competencia="' . $rcp->nombre_cp . '" data-curso="' . $rcf->niveles . '" data-entrenador="' . $rcf->entrenador . '" data-inicio="' . $rcf->inicio . '" data-fin="' . $rcf->fin . '">' . $rcf->codigo . ' - ' . $rcp->nombre_cp . ' - ' . $disponibles . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/select2.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		.label-check {
			/* display: inline-block; */
			float: left;
		}

		.Error-espacio {
			border: 2px solid red !important;
		}

		input[type="checkbox"] {
			/* display: inline-block; */
			width: 20px;
			height: 20px;
			float: left;
			margin: 0 10px 0px 20px;
		}

		.Competencia-oculto {
			display: none;
		}

		/* .Sel-foto {
			display: none;
		} */

		.Competencia-alturas4272 {
			display: none;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Crear Registro</h2>
				<form id="registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Código del ministerio*</label>
							<select name="codigo_ficha" class="Sel-codigo" required>
								<option value="">Seleccione el código</option>
								<?php echo $ls_codigos; ?>
							</select>
						</div>
						<div class="Registro-der">
							<div class="CBasico-ciudad"></div>
						</div>
						<div class="Registro-der">
							<label>Primer Nombre *</label>
							<input type="text" placeholder="Primer Nombre" name="primer_nombre" class="No-espacio Nombre-primero" required>
							<label>Apellidos *</label>
							<input type="text" placeholder="Apellidos" name="apellido" required class="No-espacio Apellidos">
							<label>Tipo de Identificación *</label>
							<select name="tipo_id" class="Tipo-identificacion" required>
								<option>CC</option>
								<option>TI</option>
								<option>CE</option>
								<option>PP</option>
								<option>PEP</option>
								<option>NIUP</option>
							</select>
							<div class="Competencia-oculto">
								<label>Lugar de Expedición *</label>
								<input type="text" placeholder="De" name="registro[identifica_origen]" class="Ciudades Expedicion">
							</div>
							<div class="Competencia-oculto">
								<label>Fecha de Nacimiento*</label>
								<input type="date" placeholder="Fecha de nacimiento" name="registro[fnacimiento]" class="Fecha-nacimiento">
							</div>
							<label>Sexo *</label>
							<select name="sexo" required>
								<option>M</option>
								<option>F</option>
							</select>
							<label>RH </label>
							<select name="rh" class="RH" required>
								<option value="">Seleccione</option>
								<option value="O-">O-</option>
								<option value="O+">O+</option>
								<option value="A-">A-</option>
								<option value="A+">A+</option>
								<option value="B-">B-</option>
								<option value="B+">B+</option>
								<option value="AB-">AB-</option>
								<option value="AB+">AB+</option>
							</select>
							<div class="Competencia-oculto">
								<label>Nivel Educativo *</label>
								<select name="registro[nveducacion]" class="Nivel-educativo">
									<option value="">Seleccione</option>
									<option value="Primaria">Primaria</option>
									<option value="Bachiller">Bachiller</option>
									<option value="Profesional">Profesional</option>
									<option value="Especialización">Especialización</option>
									<option value="Maestría">Maestría</option>
								</select>
							</div>
							<label>Email *</label>
							<input type="email" placeholder="Email" class="vendedor_correo" name="email" required>
							<label>Cargo </label>
							<input type="text" placeholder="Cargo" name="cargo" class="Cargo">
							<div class="Ocultar-content">
								<label class="label-check">Vencimiento - Carnet</label>
								<input type="checkbox" value="" class="Aplica-check"><label>No aplica</label>
								<input type="date" placeholder="Vencimiento" name="vencimiento" class="Aplica-disabled">
							</div>
							<label>Fecha de inicio *</label>
							<input type="date" name="f_inicio" class="f_inicio" required>
							<div class="Ocultar-content">
								<label class="label-check">Fecha de Vigencia *</label>
								<input type="checkbox" value="" class="Aplica-check"><label>No aplica</label>
								<input type="date" name="f_vigencia" class="Aplica-disabled">
							</div>
							<!-- <label>Tipo de pago *</label>
							<select name="tpago" required>
								<option value="">Seleccione</option>
								<option value="cr">Crédito</option>
								<option value="ct">Contado</option>
							</select> -->
							<div class="Competencia-alturas4272">
								<label>Razón Social *</label>
								<input type="text" placeholder="Razón Social" class="cliente-razon">
								<input type="hidden" name="cliente[id]" class="cliente-id">
							</div>
							<div class="Competencia-alturas4272">
								<label>Representante Legal *</label>
								<input type="text" placeholder="Representante Legal" name="cliente[representante]" class="cliente-representante" disabled>
							</div>
						</div>
						<div class="Registro-der">
							<label>Segundo Nombre</label>
							<input type="text" placeholder="Segundo Nombre" name="segundo_nombre" class="No-espacio Nombre-segundo">
							<label>Vendedor *</label>
							<select name="vendedor" class="Sel-vendedor" required>
								<option value="">Seleccione</option>
								<?php echo $ls_vendedores ?>
							</select>
							<label>Competencia</label>
							<select class="capacitacion" disabled>
								<option value="">Seleccione</option>
								<?php echo $ls_competencias ?>
							</select>
							<input type="hidden" name="capacitacion" class="h-capacitacion">
							<div id="ver-entrenadores" style="display: none">
								<label>Entrenadores *</label>
								<select class="entrenadores" disabled>
									<option value="">Seleccione</option>
									<?php echo $ls_entrenadores ?>
								</select>
								<input type="hidden" name="entrenador" class="h-entrenadores">
							</div>
							<label>Identificación *</label>
							<input type="text" placeholder="Identificación" name="identifica" required class="ntext No-espacio Registro-identificacion">
							<div class="Competencia-oculto">
								<label>País de nacimiento</label>
								<input type="text" placeholder="País de nacimiento" class="nacionalidad Pais-nacimiento">
								<input type="hidden" name="registro[nacionalidad]">
							</div>
							<div class="Competencia-oculto">
								<label>Edad </label>
								<input type="text" placeholder="Edad" name="registro[edad]" class="Edad">
							</div>
							<label>Certificación a realizar *</label>
							<select name="certificado" class="ls_cursos" required>
								<option value="">Seleccione</option>
								<?php echo $ls_cursos ?>
							</select>
							<label>Dirección *</label>
							<input type="text" placeholder="Dirección" class="vendedor_direccion" name="direccion" required>
							<div class="Competencia-oculto">
								<label>Sector Económico</label>
								<select name="registro[sector]" class="Sector">
									<option value="">Seleccione</option>
									<option>AGROPECUARIO</option>
									<option>COMERCIO</option>
									<option>COMERCIO Y SERVICIOS</option>
									<option>COMUNICACIONES</option>
									<option>CONSTRUCCIÓN</option>
									<option>EDUACION</option>
									<option>FINANCIERO</option>
									<option>HIDROCARBUROS</option>
									<option>INDUSTRIAL</option>
									<option>MINERO Y ENERGETICO</option>
									<option>TELECOMUNICACIONES</option>
									<option>TRANSPORTE</option>
								</select>
							</div>
							<label>Empresa </label>
							<input type="text" placeholder="Empresa" name="empresa" class="cliente-empresa">
							<label>Teléfono de contacto *</label>
							<input type="text" placeholder="Teléfono" class="vendedor_telefono" name="telefono" required>
							<div class="Licencia-oculto">
								<label class="label-check">Licencia *</label>
								<input type="checkbox" value="" class="Aplica-check"><label>No aplica</label>
								<input type="text" placeholder="Licencia" name="licencia" class="Aplica-disabled" required>
							</div>
							<label>Horas *</label>
							<input type="text" placeholder="Horas" name="horas" class="Sel-horas" required>
							<div class="Fecha-finaliza">
								<label>Fecha de finalización</label>
								<input type="date" name="f_fin" class="f_fin">
							</div>
							<!-- <label>Precio *</label>
							<input type="text" placeholder="Precio" name="precio" class="Precio" required> -->
							<div class="Competencia-alturas4272">
								<label>Nit *</label>
								<input type="text" placeholder="Nit" name="cliente[nit]" class="cliente-nit" disabled>
							</div>
							<div class="Competencia-alturas4272">
								<label>ARL *</label>
								<input type="text" placeholder="ARL" name="cliente[arl]" class="cliente-arl" readonly>
							</div>
						</div>
						<div class="Registro-der Carga-foto">
							<div class="Sel-foto">
								<label>Cargar foto *</label>
								<select name="carga_foto">
									<option value="1" selected>Si</option>
									<option value="0">No</option>
								</select>
							</div>
							<div class="img-placa">
								<img class="img-prev" src="images/marca_agua.jpg">
								<button type="button" class="placa" id="foto">FOTO CARNET</button>
							</div>
						</div>
						<div class="Registro-der">
						</div>
						<br><br>
						<div id="cursos_adicionales">
						</div>
						<div class="Registro-cent">
							<button type="button" id="agr_curso">Adicionar Curso</button>
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar Registro">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/select2.min.js"></script>
	<script src="js/moment-2.18.min.js"></script>
	<script type="text/javascript" src="js/registrar.js?<?php echo time()  ?>"></script>
</body>

</html>
