<div class="Top">
	<div class="Top-izq">
		<a href="/"><img src="images/logo.png" alt="Logo Gricompany"></a>
	</div>
	<div class="Top-der">
		<nav>
			<div class="container">
				<a class="toggleMenu" href="#">Menu</a>
				<ul class="nav">
					<li><a href="/">Home</a></li>
					<li><a href="nosotros">Nosotros</a></li>
					<li><a href="portafolio">Servicios</a></li>
					<li><a href="certificados">Certificados</a></li>
					<li><a href="galeria">Galeria</a></li>
					<li><a href="contacto">Contáctanos</a></li>
				</ul>
			</div>
		</nav>
	</div>
</div>
