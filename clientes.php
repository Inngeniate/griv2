<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");


$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 21)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Cliente | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<style>
		.quitar {
			cursor: pointer;
			float: right;
		}
	</style>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<?php
			//if ($_SESSION['usutipoggri'] == 'administrador') {
			?>
			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="listar" class="activo">Listar Clientes</a></li>
						<li><a href="javascript://" id="crear">Crear Clientes</a></li>
					</ul>
				</div>
			</div>
			<div class="Contenido-admin-izq listado-clientes">
				<h2>Listar Clientes</h2>
				<hr>
				<p>En esta sección podrás listar y buscar clientes.</p>
				<br>
				<form id="buscar">
					<label>Nombre: </label>
					<input type="text" name="nom" placeholder="Nombre">
					<input type="submit" value="Buscar">
				</form>
				<br>
				<div class="Listar-personas">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th>Razón Social</th>
									<th>NIT</th>
									<th>Representante Legal</th>
									<th>ARL</th>
									<th>SECTOR</th>
									<th colspan="2">Acciones</th>
								</tr>
							</thead>
							<tbody id="resultados">
							</tbody>
						</table>
					</div>
				</div>
				<div id="lista_loader">
					<div class="loader2">Cargando...</div>
				</div>
				<div id="paginacion"></div>
			</div>
			<div class="Contenido-admin-izq crear-clientes" style="display: none">
				<h2>Crear Cliente</h2>
				<form id="nuevo-cliente">
					<div class="Registro">
						<div class="Registro-der">
							<label>Razón Social *</label>
							<input type="text" placeholder="Razon Social" name="cliente[razon]" required>
							<label>NIT *</label>
							<input type="text" placeholder="NIT" name="cliente[nit]" required>
							<label>Sector Económico</label>
							<select name="cliente[sector]">
								<option value="">Seleccione</option>
								<option>AGROPECUARIO</option>
								<option>COMERCIO</option>
								<option>COMERCIO Y SERVICIOS</option>
								<option>COMUNICACIONES</option>
								<option>CONSTRUCCIÓN</option>
								<option>EDUACION</option>
								<option>FINANCIERO</option>
								<option>HIDROCARBUROS</option>
								<option>INDUSTRIAL</option>
								<option>MINERO Y ENERGETICO</option>
								<option>TELECOMUNICACIONES</option>
								<option>TRANSPORTE</option>
							</select>
						</div>
						<div class="Registro-izq">
							<label>Representante Legal *</label>
							<input type="text" placeholder="Representante Legal" name="cliente[representante]" required>
							<label>ARL *</label>
							<input type="text" placeholder="ARL" name="cliente[arl]" required>
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
			<?php
			//  } else {
			// 	echo '<p>No tiene permiso para acceder a esta sección.</p>';
			// }
			?>
		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/clientes.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
