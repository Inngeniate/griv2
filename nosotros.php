<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Nosotros | Gricompany Gestión del Riesgo Integral</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<style type="text/css" media="screen">
		.vision,
		.calidad,
		.seguridad,
		.objetivos {
			display: none;
		}
	</style>
</head>

<body>
	<?php include_once("analyticstracking.php") ?>
	<div class="Contenedor">
		<header>
			<?php include("menu.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<div class="Masshead"></div>
	<div class="Tab-slide">
		<div class="Tab-slide-inside">
			<ul id="navegador">
				<li><a href="javascript://" id="mision">Misión</a></li>
				<li><a href="javascript://" id="vision">Visión</a></li>
				<li><a href="javascript://" id="calidad">Política de calidad</a></li>
				<li><a href="javascript://" id="seguridad">Política de Seguridad</a></li>
				<li><a href="javascript://" id="objetivos">Objetivos</a></li>
			</ul>
		</div>
	</div>
	<div class="Top-imagen-fondo1">
	</div>
	<section>
		<div class="Principal-text">
			<div class="Principal-text-parrafo mision">
				<h2>Misión</h2>
				<p>GRI COMPANY SAS, se dedica a la prestación de servicios con los mejores estándares de calidad, a través del recurso humano idóneo y capacitado, permitiendo que los clientes enfoquen sus esfuerzos en su desarrollo y crecimiento, delegando a nuestra empresa la responsabilidad de capacitar continuamente sus empleados para la correcta ejecución de cada uno de los procesos, satisfaciendo las necesidades y expectativas de quienes soliciten nuestros servicios.</p>
			</div>
			<div class="Principal-text-parrafo vision">
				<h2>Visión</h2>
				<p>GRI COMPANY SAS pretende Posicionarse en el año 2020 como empresa líder en los diferentes servicios que ofrecemos con altos estándares de calidad y profesionalismo, basados en actividades innovadoras, diseñadas para satisfacer las necesidades de nuestros clientes, minimizando el número de accidentes laborales, en los procesos que se realizan en las diferentes áreas de trabajo de las entidades públicas o privadas, manteniendo siempre relaciones estables con nuestros clientes, contribuyendo a su crecimiento, para mantener el reconocimiento social y empresarial.</p>
			</div>
			<div class="Principal-text-parrafo calidad">
				<h2>POLITICA DE CALIDAD</h2>
				<p>Mantener una reafirmación constante con nuestros clientes, basado en el compromiso de innovación y respaldo, mediante servicios integrales y el suministro de elementos de protección personal que cumplan con altos estándares de calidad, bajo el respaldo de un recurso humano idóneo y comprometido con el mejoramiento continuo de nuestra empresa, desarrollando todas sus actividades teniendo en cuenta la implementación de Gestión de Calidad de la norma ISO 9001:2008.</p>
			</div>
			<div class="Principal-text-parrafo seguridad">
				<h2>POLÍTICA DE SEGURIDAD</h2>
				<p>Nuestra política de seguridad está basada en la detección, evaluación y control oportuno de todos los factores de riesgo existentes en cada una de las áreas de trabajo, dando la más alta prioridad a la prevención de incidentes mediante condiciones seguras de trabajo a nuestro personal, contratistas, clientes, visitantes y en general a todas las partes que se puedan ver afectadas en el desarrollo de nuestras labores.</p>
			</div>
			<div class="Principal-text-parrafo objetivos">
				<h2>OBJETIVOS</h2>
				<ul class="Izquierda">
					<li>Cumplir con las políticas de calidad, el manual de procedimientos y con todas las leyes y regulaciones locales que se deban aplicar en esta actividad.</li>
					<li>Mantener permanente comunicación con nuestros empleados, contratistas, clientes, la comunidad y todas las personas que tengan relación con nuestra empresa.</li>
					<li>Proporcionar los recursos necesarios para la instrucción, la capacitación y supervisión, garantizando la seguridad y salud de los trabajadores en el ejercicio de su capacitación. </li>
					<li>Planificar, revisar y evaluar nuestros resultados en salud y seguridad promoviendo la mejora continua.</li>
					<br>
				</ul>
			</div>
		</div>
	</section>
	<section>
		<?php include("convenios.php"); ?>
	</section>
	<?php include("redes.php"); ?>
	<footer>
		<?php include("footer.php"); ?>
	</footer>

	<script>
		$('#navegador li').on('click', function() {
			tab = $('a', this).prop('id');
			$.each($('.Principal-text div'), function() {
				if ($(this).hasClass(tab))
					$(this).fadeIn();
				else
					$(this).fadeOut(0);
			});
		});
	</script>

</body>

</html>
