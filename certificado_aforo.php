<div class="aforo"  style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Crear Certificado de Aforo</h2>
		<form class="cre_certificado">
			<div class="Registro">
				<div class="Registro-der">
					<label>Reporte Nº *</label>
					<input type="text" placeholder="Reporte Nº" name="certifica[reporte]" value="<?php echo $naforo ?>" class="ncertificadoaf" readonly>
					<label>Ciudad *</label>
					<input type="text" placeholder="Ciudad" name="certifica[ciudad]" class="auto_ciu" required>
					<label>Fecha *</label>
					<input type="date" placeholder="Fecha" name="certifica[fecha]" required>
					<label>Placa *</label>
					<input type="text" placeholder="Placa" name="certifica[placa]" required>
					<label>Propietario *</label>
					<input type="text" placeholder="Propietario" name="certifica[propietario]" required>
					<label>Capacidad Total *</label>
					<input type="text" placeholder="Capacidad Total" name="certifica[capacidadtotal]" required>
					<label>Capacidad BLS *</label>
					<input type="text" placeholder="Capacidad BLS" name="certifica[capacidadbls]" required>
					<label>Cantidad de rompe olas *</label>
					<input type="text" placeholder="Cantidad de rompe olas" name="certifica[rompeolas]" required>
					<label>Nº Compartimientos *</label>
					<input type="text" placeholder="Compartimientos" name="certifica[compartimientos]" required>
				</div>
				<div class="Registro-der">
					<label>Nº ejes *</label>
					<input type="text" placeholder="Nº ejes" name="certifica[ejes]" required>
					<label>Material de tanque *</label>
					<input type="text" placeholder="Material de tanque" name="certifica[materialtanque]" required>
					<label>Marca *</label>
					<input type="text" placeholder="Marca" name="certifica[marca]" required>
					<label>Valvulas de descarque *</label>
					<input type="text" placeholder="Valvulas de descarque" name="certifica[valvulas]" required>
					<label>Contenido *</label>
					<input type="text" placeholder="Contenido" name="certifica[contenido]" required>
					<label>Certificador *</label>
					<input type="text" placeholder="Certificador" name="certifica[certificador]" required>
					<label>Valido hasta *</label>
					<input type="date" placeholder="Valido hasta" name="certifica[validez]" >
					<label>Inspector *</label>
					<select name="certifica[inspector]" required>
						<option>Selecciona</option>
						<?php echo $lista_inspectores ?>
					</select>
					<label>Vendedor *</label>
					<select name="certifica[vendedor]" required>
						<option>Selecciona</option>
						<?php echo $lista_vendedores ?>
					</select>
				</div>
				<div class="Registro-cent">
					<label>Restricciones *</label>
					<textarea placeholder="Restricciones" name="certifica[restricciones]" required></textarea>
				</div>
				<div id="compartimientos">
				</div>
				<div class="Registro-der">
					<button type="button" id="agr_comp">Agregar Compartiemiento</button>
				</div>
				<br>
				<br>
				<input type="hidden" name="certifica[accion]" value="aforo">
				<input type="submit" value="Guardar Certificado">
			</div>
		</form>
	</div>
</div>