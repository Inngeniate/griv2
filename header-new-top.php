<header id="header">
	<div class="Top-sup">
		<div class="Top-sup-int">
			<p>
				<span>
					<a href="#" class="Form-pqrs">PQRS</a>
					<a href="#" class="Calificar"><i class="icon-pencil"></i></a>
					<a href="https://www.facebook.com/GRICOMPANYSAS/" target="_blank"><i class="icon-facebook2"></i></a>
					<a href="https://www.instagram.com/gri_companysas/" target="_blank"><i class="icon-instagram"></i></a>
					<a href="#"><i class="icon-google3"></i></a>
					<a href="https://wa.me/573143257703" target="_blank"><i class="icon-whatsapp"></i></a>
					<a href="#"><i class="icon-twitter"></i></a>
				</span>
			</p>
		</div>
	</div>
	<div class="Top-med">
		<div class="Top-med-int">
			<div class="Top-med-int-sec">
				<a href="#">
					<img src="images/logo.png">
				</a>
			</div>
			<div class="Top-med-int-sec Ocul">
				<div class="Top-med-int-sec-int">
					<!-- <span>A</span> -->
				</div>
				<div class="Top-med-int-sec-int">
					<p><span class="Icon-rojo"> <i class="icon-phone"></i></span><span class="Beba">314-3257703 / 314-2114658</span> <br> gerencia@gricompany.co</p>
				</div>
			</div>
			<div class="Top-med-int-sec Ocul">
				<div class="Top-med-int-sec-int">
					<!-- <span>A</span> -->
				</div>
				<div class="Top-med-int-sec-int">
					<p><span class="Icon-rojo"> <i class="icon-home"></i></span><span class="Beba">Meta - Villavicencio</span> <br> Cra 37 # 26C – 57</p>
				</div>
			</div>
			<div class="Top-med-int-sec Ocul">
				<div class="Top-med-int-sec-int">
					<!-- <span>A</span> -->
				</div>
				<div class="Top-med-int-sec-int">
					<p><span class="Icon-rojo"> <i class="icon-clock2"></i></span><span class="Beba">Lun- Sab: 7AM - 9PM</span> <br> Horario</p>
				</div>
			</div>
		</div>
	</div>
	<div class="Top-inf">
		<div class="Top-inf-int">
			<nav>
				<ul class="navegador">
					<li class="header"><a href="#"><i class="icon-home"> </i>Home</a></li>
					<li class="nosotros"><a href="#"><i class="icon-users"> </i>Nosotros</a></li>
					<li class="servicios"><a href="#"><i class="icon-address-book"> </i>Servicios</a></li>
					<li class="certificados"><a href="#"><i class="icon-file-text"> </i>Certificados</a></li>
					<li><a href="evaluacion-tsa"><i class="icon-pencil"> </i>Institucional</a></li>
					<li class="contactanos"><a href="#"><i class="icon-envelop"> </i>Contactanos</a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>
