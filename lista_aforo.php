<div class="aforo" style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Listar Certificados Aforo</h2>
		<form id="buscar3">
			<label>N° Reporte: </label>
			<input type="text" name="certifica[reporte]" id="baforo" placeholder="N° Reporte">
			<label>Placa: </label>
			<input type="text" name="certifica[placa]" id="bplaca3" placeholder="Placa">
			<input type="hidden" name="certifica[accion]" value="lista_aforo">
			<input type="hidden" name="pagina" value="0">
			<input type="submit" value="Buscar">
		</form>
		<div class="Listar-personas">
			<div class="Tabla-listar">
				<table>
					<thead>
						<tr>
							<th>N° Reporte</th>
							<th>Fecha</th>
							<th>Placa</th>
							<th>Propietario</th>
							<th>Marca</th>
							<th>Vendedor</th>
							<th>Editar</th>
							<th>Eliminar</th>
						</tr>
					</thead>
					<tbody id="resultados3">
					</tbody>
				</table>
			</div>
		</div>
		<div id="lista_loader3">
			<div class="loader2">Cargando...</div>
		</div>
		<div id="paginacion" class="pagina3"></div>
	</div>
</div>
