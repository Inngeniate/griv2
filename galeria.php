<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Galeria | Gricompany Gestión del Riesgo Integral</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/pgwslideshow_light.css">
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<?php include_once("analyticstracking.php") ?>
	<div class="Contenedor">
		<header>
			<?php include("menu.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<div class="Masshead"></div>

	<!-- <div class="Top-imagen-fondo2">
</div> -->
	<script type="text/javascript" src="js/pgwslideshow.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			pgwSlideshow = $('.pgwSlideshowLight').pgwSlideshow({
				mainClassName: 'pgwSlideshowLight',
				transitionEffect: 'fading',
				autoSlide: true
			});
		});
	</script>
	<section>
		<br><br>
		<div style="margin-top: -40px">
			<ul class="pgwSlideshowLight">
				<li><img src="images/img1.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img2.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img3.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img4.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img5.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img6.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img7.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img8.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img9.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img10.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img11.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img12.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img13.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img14.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img15.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img16.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img17.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img18.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img19.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img20.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<!-- <li><img src="images/img21.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li> -->
				<li><img src="images/img22.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img23.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img24.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img25.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img26.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img27.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img28.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img29.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img30.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img31.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img32.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img33.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img34.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img35.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img36.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img37.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img38.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img39.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img40.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img41.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img42.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img43.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img44.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img45.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img46.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img47.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img48.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img41.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img42.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img43.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img44.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img45.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img46.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img47.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img48.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img49.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img50.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img51.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img52.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img53.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img54.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img55.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img56.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>
				<li><img src="images/img57.JPG" alt="Gricompany Gestión de Riesgos Integrales" data-description="Gricompany"></li>



			</ul>
		</div>
	</section>
	<section>
		<?php include("convenios.php"); ?>
	</section>
	<script src="js/toucheffects.js"></script>
	<!-- //<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
	<?php include("redes.php"); ?>
	<footer>
		<?php include("footer.php"); ?>
	</footer>
	<script type="text/javascript" src="js/certificados.js"></script>
	<script src="js/jquery.modal.min.js"></script>
</body>

</html>
