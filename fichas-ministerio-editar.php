<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
require("libs/conexion.php");

$competencia = '';
$entrenador = '';
$nivel = '';
$codigo = '';
$inicio = '';
$fin = '';
$cantidad = '';
$ciudad = '';

if ($_REQUEST['ficha'] && !empty($_REQUEST['ficha'])) {
	$registro = $db
		->where('Id', $_REQUEST['ficha'])
		->objectBuilder()->get('fichas_ministerio');

	if ($db->count > 0) {
		$res = $registro[0];

		$competencia = $res->competencia;
		$entrenador = $res->entrenador;
		$nivel = $res->niveles;
		$codigo = $res->codigo;
		$inicio = $res->inicio;
		$fin = $res->fin;
		$cantidad = $res->cantidad;
		$ciudad = $res->ciudad;
	} else {
		header('Location: home');
	}
} else {
	header('Location: home');
}


$ls_competencias = '';
$alias = '';

$competencias = $db
	->where('activo_cp', 1)
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$ls_competencias .= '<option value="' . $rcp->Id_cp . '" data-alias="' . $rcp->alias_cp . '" ' . ($rcp->Id_cp == $competencia ? "selected" : "") . '>' . $rcp->nombre_cp . '</option>';

	if ($rcp->Id_cp == $competencia) {
		$alias = $rcp->alias_cp;
	}
}

$ls_entrenadores = '';

$entrenadores = $db
	->where('estado_en', 1)
	->orderBy('nombre_en', 'ASC')
	->objectBuilder()->get('entrenadores');

foreach ($entrenadores as $rsc) {
	$ls_entrenadores .= '<option value="' . $rsc->Id_en . '" data-tipo="' . $rsc->competencia_en . '" ' . ($rsc->Id_en == $entrenador ? "selected" : "") . ' ' . ($rsc->competencia_en != $alias ? 'style="display:none"' : "") . '>' . $rsc->nombre_en . ' ' . $rsc->apellido_en . '</option>';
}

$ls_cursos = '';

$cursos = $db
	->where('activo_ct', 1)
	->orderBy('nombre', 'ASC')
	->objectBuilder()->get('certificaciones');

foreach ($cursos as $rsc) {
	$ls_cursos .= '<option value="' . $rsc->Id_ct . '" data-competencia="' . $rsc->competencia_ct . '" ' . ($rsc->Id_ct == $nivel ? "selected" : "") . ' ' . ($rsc->competencia_ct != $alias ? 'style="display:none"' : "") . '>' . $rsc->nombre . '</option>';
}

$ls_ciudades = '';

$ciudades = [
	'cucuta' => 'Cúcuta',
	'villavicencio' => 'Villavicencio',
	'' => 'Seleccione'
];

foreach ($ciudades as $key => $value) {
	$ls_ciudades .= '<option value="' . $key . '" ' . ($key == $ciudad ? "selected" : "") . '>' . $value . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Administración Fichas Ministerio | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Editar Fichas</h2>
				<form id="registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Competencia *</label>
							<select name="registro[competencia]" class="Sel-competencia" required>
								<option value="">Seleccione</option>
								<?php echo $ls_competencias ?>
							</select>
							<label>Niveles *</label>
							<select name="registro[niveles]" class="Sel-curso" required>
								<option value="">Seleccione</option>
								<?php echo $ls_cursos; ?>
							</select>
							<label>Fecha inicio *</label>
							<input type="date" placeholder="Fecha inicio" name="registro[inicio]" class="Fecha-inicio" value="<?php echo $inicio; ?>" required>
							<label>Cantidad *</label>
							<input type="number" step="1" min="1" placeholder="Cantidad" name="registro[cantidad]" class="Cantidad" value="<?php echo $cantidad; ?>" required>
						</div>
						<div class="Registro-der">
							<label>Entrenador *</label>
							<select name="registro[entrenador]" class="Sel-entrenador" required>
								<option value="">Seleccione</option>
								<?php echo $ls_entrenadores ?>
							</select>
							<label>Código *</label>
							<input type="text" placeholder="Código" name="registro[codigo]" value="<?php echo $codigo; ?>" required>
							<label>Fecha finalización *</label>
							<input type="date" placeholder="Fecha finalización" name="registro[fin]" class="Fecha-fin" value="<?php echo $fin; ?>" required>
							<label>Ciudad *</label>
							<select name="registro[ciudad]" class="Sel-ciudad" required>
								<?php echo $ls_ciudades; ?>
							</select>
						</div>
						<hr>
						<br>
						<br>
						<input type="hidden" name="registro[Idficha]" value="<?php echo $_REQUEST['ficha']; ?>">
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/administrar-fichas-ministerio-ed.js?<?php echo time()  ?>"></script>
</body>

</html>
