<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

if (isset($_GET['entrenador']) && $_GET['entrenador'] != '') {
	require("libs/conexion.php");

	$entrenadores = $db
		->where('Id_en', $_GET['entrenador'])
		->objectBuilder()->get('entrenadores');

	if ($db->count > 0) {
		$rsc = $entrenadores[0];
		$nombre   = $rsc->nombre_en;
		$apellido = $rsc->apellido_en;

		$tsa = $rsc->tsa_reg;
		// $sena = $rsc->sena_reg;
		$licencia = $rsc->licencia_sst;
		$nfpa = $rsc->nfpa_proboard_number;
		$escon_reg = $rsc->escon_reg;

		$ls_estados = '';
		$estados = ['Activo' => 1, 'Inactivo' => 0];

		foreach ($estados as $key => $value) {
			$ls_estados .= '<option ' . ($value == $rsc->estado_en ? "Selected" : "") . ' value="' . $value . '">' . $key . '</option>';
		}

		$ls_competencias = '';
		// $competencias = ['alturas' => 'ALTURAS', 'cea' => 'CEA', 'espaciosconfinados' => 'ESPACIOS CONFINADOS'];

		$competencias = $db
			->where('activo_cp', 1)
			->orderBy('nombre_cp', 'ASC')
			->objectBuilder()->get('competencias');

		foreach ($competencias as $rcp) {
			$ls_competencias .= '<option value="' . $rcp->alias_cp . '" ' . ($rcp->alias_cp == $rsc->competencia_en ? "Selected" : "") . '>' . $rcp->nombre_cp . '</option>';
		}

		// foreach ($competencias as $key => $value) {
		// 	$ls_competencias .= '<option ' . ($key == $rsc->competencia_en ? "Selected" : "") . ' value="' . $key . '">' . $value . '</option>';
		// }

		$firma = '';
		$firma_ver = 'style="display:none;margin:30px;width: 300px"';
		$firma_btn = '';

		if ($rsc->firma_en != '') {
			$firma = 'src="' . $rsc->firma_en . '"';
			$firma_ver = 'style="display:block;margin:30px;width: 300px"';
			$firma_btn = '<a href="javascript://" class="Btn-gris-normal borrar-firma"><i class="icon-bin"> </i>Borrar Firma</a>';
		}
	} else {
		header('Location: entrenadores');
	}
} else {
	header('Location: entrenadores');
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Entrenadores | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		.disabled {
			background-color: #e6e6e6;
		}

		.Content-rescate {
			display: none;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Editar Entrenador</h2>
				<form id="editar-entrenador">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombres" name="entrena[nombre]" value="<?php echo $nombre ?>" required>
							<label>Entrenador <span class="Tipo-entrenador"></span> Reg.*</label>
							<input type="text" placeholder="Entrenador Reg." name="entrena[tsa]" value="<?php echo $tsa ?>" required>
							<label>Licencia SST*</label>
							<input type="text" placeholder="Licencia SST" name="entrena[licencia]" value="<?php echo $licencia ?>" required>
							<div class="Content-rescate">
								<label>NFPA PROBOARD NUMBER</label>
								<input type="text" placeholder="NFPA PROBOARD NUMBER" name="entrena[nfpa]" value="<?php echo $nfpa ?>">
							</div>
							<label>Firma (imagen)</label>
							<input type="file" id="firma">
							<input type="hidden" name="entrena[identrenador]" value="<?php echo $_GET['entrenador'] ?>">
						</div>
						<div class="Registro-der">
							<label>Apellido *</label>
							<input type="text" placeholder="Apellidos" name="entrena[apellido]" value="<?php echo $apellido ?>" required>
							<!-- <label>Evaluador de Competencias laborales Sena Reg.*</label>
							<input type="text" placeholder="Evaluador de Competencias laborales Sena Reg." name="entrena[sena]" value="<?php //echo $sena ?>" required> -->
							<label>Estado</label>
							<select name="entrena[estado]">
								<?php echo $ls_estados ?>
							</select>
							<label>Competencia*</label>
							<select name="entrena[capacitacion]" class="capacitacion" required="">
								<?php echo $ls_competencias ?>
							</select>
							<div class="Content-rescate">
								<label>Entrenador ESCON REG</label>
								<input type="text" placeholder="Entrenador ESCON REG" name="entrena[escon_reg]" value="<?php echo $escon_reg ?>">
							</div>
							<label>Firma por pantalla</label>
							<button type="button" class="placa" id="firma-digital">Firma Entrenador</button>
							<img id="canvasimg" <?php echo $firma . $firma_ver ?>>
							<?php echo $firma_btn; ?>
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/entrenadores_edt.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
