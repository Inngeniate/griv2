<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="google-site-verification" content="oc9E4yBVx2u8uUHzYyL0_dqaWXeQipnb0i2jfsMEHaA" />
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Gricompany Gestión del Riesgo Integral</title>
	<!-- <link rel="stylesheet" href="css/slider.css" /> -->
	<link rel="stylesheet" href="css/landing.css?v<?php echo date('YmdHis') ?>" />
	<link rel="stylesheet" href="css/slider.css?v<?php echo date('YmdHis') ?>" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<style type="text/css" media="screen">
		.modal-title {
			background-color: #b42d32 !important;
		}

		table td {
			padding: 4px;
		}

		#map {
			width: 100%;
			height: 400px;
		}

		.modal-inner input[type=text],
		.modal-inner input[type=email] {
			background: #fff;
			border: 1px solid #656571;
		}

		.modal-inner input[type=email] {
			font-family: Roboto-Regular;
			padding: 5px 15px;
			height: 45px;
			width: 100%;
			outline: none;
			margin-bottom: 10px;
			-webkit-transition: 0.3s;
			-moz-transition: 0.3s;
			-o-transition: 0.3s;
			-ms-transition: 0.3s;
			transition: 0.3s;
		}

		.btn-none {
			display: none !important;
		}
	</style>
	<link rel="stylesheet" href="css/aos.css" />
	<link rel="stylesheet" href="css/modaal.css?v<?php echo date('YmdHis') ?>" />
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-206122393-1">
	</script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-206122393-1');
	</script>
</head>

<body>
	<?php include_once("header-new-top.php") ?>
	<section>
		<div class="Slider">
			<div class="Slider-imagen">
				<div class="wide-container">
					<div id="slides">
						<ul class="slides-container">
							<li>
								<img src="images/slide1.jpg" alt="">
								<div class="context">
									<h2>INSCRIPCIONES ABIERTAS</h2>
									<a href="registro-aspirantes">Registrar</a>
								</div>
							</li>
							<li>
								<img src="images/slide2.jpg" alt="">
								<div class="context">
									<h2>INSCRIPCIONES ABIERTAS</h2>
									<a href="registro-aspirantes">Registrar</a>
								</div>
							</li>
							<li>
								<img src="images/slide3.jpg" alt="">
								<div class="context">
									<h2>INSCRIPCIONES ABIERTAS</h2>
									<a href="registro-aspirantes">Registrar</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="Seccion1">
			<div class="Seccion1-int">
				<h2><span class="Rojo">\\\</span>Gestión Integral del Riesgo<span class="Rojo">\\\</span></h2>
				<p>El Instituto GRI COMPANY SAS, se creó como entidad educativa para brindar capacitación y
					orientación técnica laboral en forma integral, ofreciendo sus servicios y soporte de manera
					presencial y virtual a partir de la identificación de los requerimientos de la empresa nacional y el
					contexto internacional, para lo cual integra recursos físicos, financieros y humanos de tal forma
					que contribuya a la formación empresarial con sentido social. Cuenta con convenios
					institucionales diversificando los frentes y campos de trabajo según la necesidad de nuestros
					clientes en búsqueda de ofrecer soluciones eficaces contribuyendo con el desarrollo y
					mejoramiento en toda región y el país.</p>
			</div>
		</div>
	</section>
	<section>
		<div class="Seccion2" id="nosotros" style="background-attachment:fixed;">
			<div class="Seccion2-int">
				<h2><span class="Rojo">\\\</span>Nosotros<span class="Rojo">\\\</span></h2>
				<div class="Seccion2-int-sec v-nosotros">
					<div class="Boton-nosotros" id="v-mision">
						<h2 class="Rojo-text"><span class="Rojo">\\\</span>Misión<span class="Rojo">\\\</span></h2>
					</div>
					<div class="Boton-nosotros" id="v-vision">
						<h2 class="Rojo-text"><span class="Rojo">\\\</span>Visión<span class="Rojo">\\\</span></h2>
					</div>
					<div class="Boton-nosotros" id="v-pcalidad">
						<h2 class="Rojo-text"><span class="Rojo">\\\</span>Política de calidad<span class="Rojo">\\\</span></h2>
					</div>
					<div class="Boton-nosotros" id="v-pseguridad">
						<h2 class="Rojo-text"><span class="Rojo">\\\</span>Política de seguridad<span class="Rojo">\\\</span></h2>
					</div>
					<div class="Boton-nosotros" id="v-objetivo">
						<h2 class="Rojo-text"><span class="Rojo">\\\</span>Objetivos<span class="Rojo">\\\</span></h2>
					</div>
				</div>
				<div class="Seccion2-int-sec">
					<div class="C-blanco v-describe">
						<div class="v-mision">
							<h3>Misión</h3>
							<p>Somos una Institución de Educación para el Trabajo y Desarrollo Humano, que responde a las necesidades de las partes interesadas, con un grupo idóneo y comprometido en aplicar los principios de calidad, integridad, responsabilidad, espíritu, servicio y cumplimiento, para que nuestros asociados puedan alcanzar su más alto potencial tanto en su carrera como en lo personal.</p>
						</div>
						<div class="v-vision" style="display: none">
							<h3>Visión</h3>
							<p>Para el año 2025 el instituto de educación para el trabajo y desarrollo humano GRI COMPANY S.A.S, se consolidara como una organización líder en educación vial en Colombia. Por su excelente nivel académico en todos sus programas, contando con una infraestructura propia, moderna, un equipo de trabajo idóneo y comprometido con la satisfacción de nuestros clientes y la mejora continua de nuestros procesos.</p>
						</div>
						<div class="v-pcalidad" style="display: none">
							<h3>Política de calidad</h3>
							<p>Mantener una reafirmación constante con nuestros clientes, basado en el compromiso de innovación y respaldo, mediante servicios integrales y el suministro de elementos de protección personal que cumplan con altos estándares de calidad, bajo el respaldo de un recurso humano idóneo y comprometido con el mejoramiento continuo de nuestra empresa, desarrollando todas sus actividades teniendo en cuenta la implementación de Gestión de Calidad de la norma ISO 9001:2008.</p>
						</div>
						<div class="v-pseguridad" style="display: none">
							<h3>Política de seguridad</h3>
							<p>Nuestra política de seguridad está basada en la detección, evaluación y control oportuno de todos los factores de riesgo existentes en cada una de las áreas de trabajo, dando la más alta prioridad a la prevención de incidentes mediante condiciones seguras de trabajo a nuestro personal, contratistas, clientes, visitantes y en general a todas las partes que se puedan ver afectadas en el desarrollo de nuestras labores.</p>
						</div>
						<div class="v-objetivo" style="display: none">
							<h3>Objetivos</h3>
							<p>- Cumplir con las políticas de calidad, el manual de procedimientos y con todas las leyes y regulaciones locales que se deban aplicar en esta actividad.</p>
							<p>- Mantener permanente comunicación con nuestros empleados, contratistas, clientes, la comunidad y todas las personas que tengan relación con nuestra empresa.</p>
							<p>- Proporcionar los recursos necesarios para la instrucción, la capacitación y supervisión, garantizando la seguridad y salud de los trabajadores en el ejercicio de su capacitación.</p>
							<p>- Planificar, revisar y evaluar nuestros resultados en salud y seguridad promoviendo la mejora continua.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="Seccíon3" id="servicios" style="padding-bottom: 26px">
			<div class="Seccíon3-int">
				<h2><span class="Rojo">\\\</span>Nuestros Servicios<span class="Rojo">\\\</span></h2>
				<div class="">
					<div class="Contenedor-desc">
						<div class="Contenedor-desc-int">
							<section class="Nav_tabs">
								<div class="Nav_tabs_back">
									<nav>
										<ul class="Tabs navegador-servicios">
											<li class="Nav-6"><a href="javascript:void(0)">Instalación línea de vida y puntos de anclaje certificados</a></li>
											<li class="Nav-1"><a href="javascript:void(0)">Cursos Cortos y de Profundización</a></li>
											<li class="Nav-2"><a href="javascript:void(0)">Sistemas de Gestión Integrados</a></li>
											<li class="Nav-3"><a href="javascript:void(0)">Servicios ARL</a></li>
											<li class="Nav-4"><a href="javascript:void(0)">Ensayos No Destructivos para el Sector Transporte</a></li>
											<li class="Nav-5"><a href="javascript:void(0)">Inspección Y Certificación de Equipos Para Trabajo Seguro en Alturas</a></li>
										</ul>
									</nav>
								</div>
							</section>
							<section>
								<div class="Contendor-descripcion v-servicios">
									<div class="sv-1">
										<h3>Instalación línea de vida y puntos de anclaje certificados</h3>
										<p>Las líneas de vida son un Sistema de protección para trabajos en altura diseñado para dos funciones fundamentales:</p>
										<li>Restricción: Evita que lleguemos a un a zona con riesgo de caída.</li>
										<li>Antiácidas: Detiene con total seguridad a uno o varios usuarios, si se produce una caída accidental</li>
										<p>Gri Company SAS realiza el Diseño, Cálculo, Suministro, Instalación y Certificación de Sistemas de Protección Anti – Caídas, Líneas de vida y sistemas de protección de borde así como la Inspección / Mantenimiento preventivo anual de los mismos.</p>
										<p>Este departamento está compuesto por un equipo pluridisciplinar de Ingenieros y Técnicos en Prevención Especializados y Capacitados para proporcionar las mejores soluciones para reducir la accidentalidad por caídas en altura y en el acceso a sus instalaciones, con sistemas completamente homologados y certificados.</p>
										<h3>Puntos de Anclaje</h3>
										<p>Instalamos y certificamos puntos de anclaje según Resolución 1409 de 2012, artículo 22, numeral 2.</p>
										<p>Para prevenir riesgos de caídas al ejecutar trabajos en altura se utilizan dispositivos de anclaje, que se aplican tanto en las construcciones nuevas de cualquier tipo de uso, como en edificios existentes durante intervenciones de renovación de la cubierta. Dichos dispositivos deben poseer los requisitos previstos por la norma EN 795 «Equipos de protección individual contra caídas. Dispositivos de anclaje», por las normas EN contenidas en ésta</p>
										<p>Suministro e instalación de anclajes químicos, mecánicos, varilla corrugada y sus componentes, utilizamos productos HILTI.</p>
										<p>Suministro e instalación conector de anclaje doble perno o sencillo (argolla D en acero inoxidable).</p>

										<div class="Content-img sv-1">
											<div class="Content-img-int sv-1">
												<div class="Content-img-int-sec sv-1">
													<img src="images/imglinea1.jpg" />
												</div>
												<div class="Content-img-int-sec sv-1">
													<img src="images/imglinea2.jpg" />
												</div>
												<div class="Content-img-int-sec sv-1">
													<img src="images/imglinea3.jpg" />
												</div>
											</div>
										</div>

									</div>
									<div class="sv-2" style="display: none">
										<h3>Cursos Cortos y de Profundización</h3>
										<li>Curso para trabajo seguro en alturas – TSA (Nivel administrativo, avanzado, reentrenamiento y
											coordinador) Autorización Min. Trabajo. <br>
											<img src="images/mintrabajo.jpeg" style="width: 230px; margin: 12px 0px;">
										</li>
										<li>Curso obligatorio para el transporte de mercancías peligrosas (resolución 1223 de 2014)
											Autorización Min. Transportes. <br>
											<img src="images/mintransporte.png" style="width: 230px; margin: 12px 0px;">
										</li>
										<li>Evaluación y certificación de conductores por competencias laborales</li>
										<li>Curso de manejo preventivo y defensivo en vehículos</li>
										<li>Curso primeros auxilios y primer respondiente</li>
										<li>Curso control de incendios y manejo de extintores</li>
										<li>Curso mecánica básica automotriz</li>
										<li>Curso transportes de cargas Extra dimensionadas (amarre seguro de cargas)</li>
										<li>Curso operador de montacargas</li>
										<li>Curso normas de tránsito y seguridad vial</li>
										<li>Cursos técnicas manipulación de alimentos</li>
									</div>
									<div class="sv-3" style="display: none">
										<h3>Sistemas de Gestión integrados:</h3>
										<p>Ofrecemos una amplia gama de servicios de asesoría para la implementación y certificación de Sistemas de Gestión ISO, independientemente de que su organización está en la fase inicial de implementación o si está a mitad de camino o se está acercando a la auditoría de certificación o su sistema está abandonado, nuestros servicios de consultoría ISO agregarán valor a la eficacia general en su sistema de gestión ISO, permitiendo alcanzar una ventaja competitiva sostenible.</p>
										<li>ISO 9001</li>
										<li>ISO 14001</li>
										<li>OHSAS 18001</li>
										<li>HACCP</li>
										<li>ISO 22000 e ISO 27001</li>
										<li>ISO 45001</li>
									</div>
									<div class="sv-4" style="display: none">
										<h3>Servicios ARL</h3>
										<p>Esta unidad de negocio pretende ofrecer soluciones al segmento de Administradoras de Riesgos Laborales, que requieren atender a sus clientes mediante asesores externos.</p>
										<p> <img src="images/axa.png" style="width: 124px;"> <img src="images/positiva.png" style="width: 238px;margin-left:  10px;"> <img src="images/seguros_bolivar.png" style="width: 193px;margin-left:  10px;"> </p>
									</div>
									<div class="sv-5" style="display: none">
										<h3>Ensayos no destructivos para el sector transporte</h3>
										<p>Servicio de inspección y certificación por medio de líquidos penetrantes o partículas magnéticas exigida por las petroleras para todo tipo de vehículos pesados, Esta metodología cumple con las normas ASTM E-1417 Y E-165, (CP-189, NAS-410, ISO-9712)</p>
										<p>Nuestra empresa cuenta con personal Calificado y equipos totalmente certificados bajo los más altos estándares, normas y procesos a nivel mundial y nacional.</p>
										<p><strong>INSPECCION DE AREAS DE ESFUERZO</strong></p>
										<p><strong>QUINTA RUEDAS</strong></p>
										<p>Estructura superior Mordazas</p>
										<p>Estructura inferior Pasadores</p>
										<p><strong>KING PIN</strong></p>
										<p>Tornamesa King pin</p>
										<p><strong>GRUAS</strong></p>
										<p>Gatos estabilizadores Soldadurasz</p>
										<p>Pasteca Tornameza</p>
										<p>Secciones de la pluma Estructura</p>
										<p><strong>CARRO MACHOS</strong></p>
										<p>Plumas Soportes</p>
										<p>Ganchos Poleas</p>
										<p>Pasadores Cadenas</p>
										<p><strong>PRUEBA HIDROSTATICA</strong></p>
										<p>La prueba hidrostatica es una aplicación de presión a un equipo, tanque o línea de tubería, que tiene como objetivo detectar fugas, verificar la resistencia mecánica, probar la hermeticidad de los accesorios y las soldaduras.</p>
										<p><strong>TABLA DE AFORO:</strong></p>
										<p>La precisión en el cálculo de las dimensiones de un tanque es un factor muy importante para la determinación del volumen del líquido, La medición de un tanque puede ejecutarse por uno de los siguientes métodos:</p>
										<p><strong>GEOMÉTRICO:</strong></p>
										<p>El método geométrico consiste en una medición directa o indirecta de las dimensiones exteriores o interiores del tanque.</p>
										<p><strong>VOLUMETRICO:</strong></p>
										<p>Se realiza midiendo el tiempo de llenado de un recipiente de volumen conocido.</p>
									</div>
									<div class="sv-6" style="display: none">
										<h3>INSPECCION Y CERTIFICACION DE EQUIPOS PARA TRABAJO SEGURO EN ALTURAS</h3>
										<p>Inspección anual (certificación) de equipos de trabajo en alturas o de protección contra caídas (Arnés, Anclajes, Eslingas y Conectores) de las marcas Arseg, Capital Safety, Protecta y DBI Salas.</p>
										<div class="Content-img sv-6">
											<div class="Content-img-int sv-6">
												<div class="Content-img-int-sec sv-6">
													<img src="images/imgaltu1.jpg" />
												</div>
												<div class="Content-img-int-sec sv-6">
													<img src="images/imgaltu2.jpg" />
												</div>
												<div class="Content-img-int-sec sv-6">
													<img src="images/imgaltu3.jpg" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="Seccíon3">
			<div class="Seccíon3-int">
				<h2><span class="Rojo">\\\</span>Cursos para Trabajo Seguro en Alturas<span class="Rojo">\\\</span></h2>
				<div class="Mod-servi">
					<div class="Mod-servi-int">
						<div class="Mod-separador">
							<a href="javascript:void(0)" class="inline-modal">
								<div class="Mod-servi-int-sec" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
									<div class="Mod-servi-int-sec-dentro">
										<img src="images/basico.jpeg" class="Mod-efecto">
										<div class="Mod-efecto-medio">
											<div class="Mod-texto-medio">Leer Mas</div>
										</div>
										<div class="Mod-oculto" style="display: none">
											<div class="Curso-bloque">
												<div class="Curso-bloque-p">
													<h2>Básico Administrativo</h2>
													<div class="Curso-bloque-int">
														<div class="Curso-bloque-int-sec">
															<img src="images/basico.jpeg">
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Descripción</h3>
															<p>Talento humano que administra procesos relacionados con el trabajo seguro en alturas.</p>
															<h3>Requisitos</h3>
															<p>Fotocopia de la cédula al 150%.</p>
															<p>Seguridad social vigente (ARL, EPS, o SISBEN).</p>
															<h3>Entregables</h3>
															<p>Carnet y Certificado con Holograma válido por 1 año</p>
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Metodología</h3>
															<p>Teórico - Práctico</p>
															<h3>Cantidad de horas</h3>
															<p>10</p>
															<h3>Nivel del curso</h3>
															<p>Básico</p>
															<h3>Formación</h3>
															<p>Horario estipulado</p>
														</div>
													</div>
												</div>
											</div>

										</div>
										<h3>Básico Administrativo</h3>
									</div>
								</div>
							</a>
							<a href="javascript:void(0)" class="inline-modal">
								<div class="Mod-servi-int-sec" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
									<div class="Mod-servi-int-sec-dentro">
										<img src="images/basico2.jpg" class="Mod-efecto">
										<div class="Mod-efecto-medio">
											<div class="Mod-texto-medio">Leer Mas</div>
										</div>
										<div class="Mod-oculto" style="display: none">
											<div class="Curso-bloque">
												<div class="Curso-bloque-p">
													<h2>Básico Operativo</h2>
													<div class="Curso-bloque-int">
														<div class="Curso-bloque-int-sec">
															<img src="images/basico2.jpg">
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Descripción</h3>
															<p>Trabajadores expuestos a riesgo de caídas por trabajo en alturas superiores a (1.50 Mts), que trabajen en plataformas de acceso a los sitios de alturas protegidas por barandas de seguridad.</p>
															<h3>Requisitos</h3>
															<p>Fotocopia de la cédula al 150%.</p>
															<p>Seguridad social vigente (ARL, EPS, o SISBEN).</p>
															<p>Examen médico ocupacional de aptitud para trabajo seguro en alturas</p>
															<h3>Entregables</h3>
															<p>Carnet y Certificado con Holograma válido por 1 año</p>
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Metodología</h3>
															<p>Teórico - Práctico</p>
															<h3>Cantidad de horas</h3>
															<p>8</p>
															<h3>Nivel del curso</h3>
															<p>Básico</p>
															<h3>Formación</h3>
															<p>Horario estipulado</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<h3>Básico Operativo</h3>
									</div>
								</div>
							</a>
							<a href="javascript:void(0)" class="inline-modal">
								<div class="Mod-servi-int-sec" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
									<div class="Mod-servi-int-sec-dentro">
										<img src="images/reeentrenados.jpg" class="Mod-efecto">
										<div class="Mod-efecto-medio">
											<div class="Mod-texto-medio">Leer Mas</div>
										</div>
										<div class="Mod-oculto" style="display: none">
											<div class="Curso-bloque">
												<div class="Curso-bloque-p">
													<h2>Reentrenamiento</h2>
													<div class="Curso-bloque-int">
														<div class="Curso-bloque-int-sec">
															<img src="images/reeentrenados.jpg">
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Descripción</h3>
															<p>Proceso anual obligatorio, por el cual se actualizan conocimientos y se entrenan habilidades y destrezas en prevención y protección contra caídas. Debe realizarse anualmente o cuando el trabajador autorizado ingrese como nuevo en la empresa, o cambie de tipo de trabajo en alturas o haya cambiado las condiciones de operaciones o su actividad.</p>
															<h3>Requisitos</h3>
															<p>Fotocopia de la cédula al 150%.</p>
															<p>Seguridad social vigente (ARL, EPS, o SISBEN).</p>
															<p>Examen médico ocupacional de aptitud para trabajo seguro en alturas.</p>
															<p>Fotocopia del curso a renovar, no mayor a un (1) año de vencido.</p>
															<h3>Entregables</h3>
															<p>Carnet y Certificado con Holograma válido por 1 año</p>
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Metodología</h3>
															<p>Teórico - Práctico</p>
															<h3>Cantidad de horas</h3>
															<p>20</p>
															<h3>Nivel del curso</h3>
															<p>Reentrenamiento</p>
															<h3>Formación</h3>
															<p>Horario estipulado</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<h3>Reentrenamiento</h3>
									</div>
								</div>
						</div>
						</a>
						<div class="Mod-separador">
							<a href="javascript:void(0)" class="inline-modal">
								<div class="Mod-servi-int-sec" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
									<div class="Mod-servi-int-sec-dentro">
										<img src="images/reeentrena.jpg" class="Mod-efecto">
										<div class="Mod-efecto-medio">
											<div class="Mod-texto-medio">Leer Mas</div>
										</div>
										<div class="Mod-oculto" style="display: none">
											<div class="Curso-bloque">
												<div class="Curso-bloque-p">
													<h2>Avanzado</h2>
													<div class="Curso-bloque-int">
														<div class="Curso-bloque-int-sec">
															<img src="images/reeentrena.jpg">
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Descripción</h3>
															<p>Según lo establecido en la resolución 1409/12. Todo trabajador que realice labores en alturas (1,50 mts),con riesgo de caída, según lo establecido en esta resolución, que realice desplazamiento horizontales y/o verticales por las estructuras, incluidas las técnicas de suspensión, utilizando diferentes equipos de protección contra caídas según el tipo de aplicación y sistemas de anclajes portátil.</p>
															<h3>Requisitos</h3>
															<p>Fotocopia de la cédula al 150%.</p>
															<p>Seguridad social vigente (ARL, EPS, o SISBEN).</p>
															<p>Examen médico ocupacional de aptitud para trabajo seguro en alturas.</p>
															<h3>Entregables</h3>
															<p>Carnet y Certificado con Holograma válido por 1 año</p>
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Metodología</h3>
															<p>Teórico - Práctico</p>
															<h3>Cantidad de horas</h3>
															<p>40</p>
															<h3>Nivel del curso</h3>
															<p>Avanzado</p>
															<h3>Formación</h3>
															<p>Horario estipulado</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<h3>Avanzado</h3>
									</div>
								</div>
							</a>
							<a href="javascript:void(0)" class="inline-modal">
								<div class="Mod-servi-int-sec" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
									<div class="Mod-servi-int-sec-dentro">
										<img src="images/coordinador.jpg" class="Mod-efecto">
										<div class="Mod-efecto-medio">
											<div class="Mod-texto-medio">Leer Mas</div>
										</div>
										<div class="Mod-oculto" style="display: none">
											<div class="Curso-bloque">
												<div class="Curso-bloque-p">
													<h2>Coordinador</h2>
													<div class="Curso-bloque-int">
														<div class="Curso-bloque-int-sec">
															<img src="images/coordinador.jpg">
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Descripción</h3>
															<p>Trabajador designado por el empleador, capaz de identificar peligrosos en el sitio en donde se realiza trabajo en alturas, relacionados con el ambiente o condiciones de trabajo y que tienen su autorización para aplicar medidas correctivas inmediatas para controlar los riesgos asociados a dichos peligros.</p>
															<h3>Requisitos</h3>
															<p>Fotocopia de la cédula al 150%.</p>
															<p>Seguridad social vigente (ARL, EPS, o SISBEN).</p>
															<p>Examen médico ocupacional de aptitud para trabajo seguro en alturas.</p>
															<p>Carta generada donde se certifique que la persona lleva laborando 1 año en el campo relacionado con seguridad y salud en el trabajo.</p>
															<h3>Entregables</h3>
															<p>Carnet y Certificado con Holograma</p>
														</div>
														<div class="Curso-bloque-int-sec">
															<h3>Metodología</h3>
															<p>Teórico - Práctico</p>
															<h3>Cantidad de horas</h3>
															<p>80</p>
															<h3>Nivel del curso</h3>
															<p>Coordinador</p>
															<h3>Formación</h3>
															<p>Horario estipulado</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<h3>Coordinador</h3>
									</div>
								</div>
							</a>

						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="Seccíon3-int">
			<div class="Mostrar-cert">
				<div class="Mostrar-cert-int">
					<h2><span class="Rojo">\\\</span>Formación entrenamiento y calidad<span class="Rojo">\\\</span></h2>
					<div class="Mostrar-cert-content">
						<p>Actualmente contamos con la licencia Nº 1500-56.03 -1795 de la Secretaría de Educación de Villavicencio que nos acredita como Institución Educativa de Control y Educación Vial</p>
						<p>
							<a href="docs/resoluciongricompany.pdf" target="_blank" class="Btn-licencia">Ver Resolución</a>
							<a href="docs/LicenciadeFuncionamiento1765ITDHGRICOMPANYSAS-1-4.pdf" target="_blank" class="Btn-licencia">Ver Licencia</a>
							<a href="docs/Autorizacion_Decreto_4908_EducacionnoformalITDHGRICOMPANYSAS.pdf" target="_blank" class="Btn-licencia">Ver Res. Educación</a>
							<a href="docs/CertificadoNTC6072Icontec.pdf" target="_blank" class="Btn-licencia">Ver Certificado</a>
						</p>

					</div>
				</div>
			</div>
		</div>

	</section>

	<section>
		<div class="Seccion4" id="certificados">
			<div class="Seccion4-int">
				<div class="Seccion4-int-sec"></div>
				<div class="Seccion4-int-sec">
					<h2><span class="Rojo">\\\</span>Certificados<span class="Rojo">\\\</span></h2>
					<div class="Seccion4-form">
						<p>En esta sección podrás consultar tu certificado ingresando número de identificación (Cedula).</p>
						<form id="v-certificados">
							<input type="text" placeholder="Número" name="consulta[identificacion]">
							<label>Ingresar la placa del vehículo para consultar las inspecciones</label>
							<input type="text" placeholder="Número" name="consulta[placa]">
							<label>Ingresar el serial de tu equipo para consultar el certificado de seguridad</label>
							<input type="text" placeholder="Serial" name="consulta[serial]">
							<label>Ingresar el número de identificación (Cedula) para consultar el certificado de alturas</label>
							<input type="text" placeholder="Número" name="consulta[ccaltura]" class="certalturas">
							<input type="submit" class="Btn-rojo" value="Consultar" name="">
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section style=" background-image:url(images/bg33.jpg); background-repeat: no-repeat; background-size: cover; background-position: center center; background-attachment:fixed;">
		<div class="Seccion5" id="contactanos">
			<div class="Seccion5-int">
				<h2><span class="Rojo">\\\</span>Contactanos<span class="Rojo">\\\</span></h2>
				<div class="Seccion5-int-sec">
					<div class="Contacto">
						<form id="contacto">
							<input type="text" placeholder="Nombre" name="nombre_c">
							<input type="email" placeholder="Email" name="email_c">
							<textarea placeholder="Mensaje" name="mensaje_c"></textarea>
							<input type="submit" class="Btn-rojo" value="Enviar mensaje" name="">
						</form>
					</div>
				</div>
				<div class="Seccion5-int-sec">
					<div class="Seccion5-int-sec-contact">
						<p>Encuéntranos en:</p>
						<h3>Teléfonos</h3>
						<p>Cel: 314-2114658 / 314-3257703</p>
						<h3>Email:</h3>
						<p>gricompanysas@gmail.com</p>
						<h3>Dirección:</h3>
						<p>Carrera 37 # 26c – 57 Barrio 7 de Agosto Villavicencio - Meta </p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="Seccion5">
			<div class="Seccion5-int">
				<h2><span class="Rojo">\\\</span>Certificados<span class="Rojo">\\\</span></h2>
			</div>
			<div class="CertIcon">
				<img src="images/logoicontect.jpg">
			</div>
		</div>
	</section>
	<section>
		<div class="Mapa">
			<div id="map"></div>
		</div>
	</section>
	<div class="Conten-aula-virtual">
		<div class="Conten-aula-virtual-int">
			<a href="https://appgrivirtual.gricompany.co/login" target="_blank"><i class="icon-pencil"></i> Aulas virtuales</a>
		</div>
	</div>
	<footer>
		<div class="Footer">
			<div class="Footer-int">
				<p>Copyright © Gricompany S.A.S, 2019. Todos los derechos reservados diseñado por <a href="https://www.inngeniate.com/">Inngeniate.com</a></p>
			</div>
		</div>
	</footer>
	<script src="https://code.tidio.co/jsvxqljs0uyxokrpa5zwliixzkeqaxzp.js"></script>
	<script>
		function initMap() {
			var chicago = new google.maps.LatLng(4.141357, -73.636318);
			var map = new google.maps.Map(document.getElementById('map'), {
				center: chicago,
				zoom: 16
			});
			var coordInfoWindow = new google.maps.InfoWindow();
			coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
			coordInfoWindow.setPosition(chicago);
			coordInfoWindow.open(map);
			map.addListener('zoom_changed', function() {
				coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
				coordInfoWindow.open(map);
			});
		}
		var TILE_SIZE = 256;

		function createInfoWindowContent(latLng, zoom) {
			var scale = 4 << zoom;
			var worldCoordinate = project(latLng);
			var pixelCoordinate = new google.maps.Point(
				Math.floor(worldCoordinate.x * scale),
				Math.floor(worldCoordinate.y * scale));
			var tileCoordinate = new google.maps.Point(
				Math.floor(worldCoordinate.x * scale / TILE_SIZE),
				Math.floor(worldCoordinate.y * scale / TILE_SIZE));
			return [
				'Gricompany, Villavicencio, Meta'
				// 'LatLng: ' + latLng,
				// 'Zoom level: ' + zoom,
				// 'World Coordinate: ' + worldCoordinate,
				// 'Pixel Coordinate: ' + pixelCoordinate,
				// 'Tile Coordinate: ' + tileCoordinate
			].join('<br>');
		}
		// The mapping between latitude, longitude and pixels is defined by the web
		// mercator projection.
		function project(latLng) {
			var siny = Math.sin(latLng.lat() * Math.PI / 180);
			// Truncating to 0.9999 effectively limits latitude to 89.189. This is
			// about a third of a tile past the edge of the world tile.
			siny = Math.min(Math.max(siny, -0.9999), 0.9999);
			return new google.maps.Point(
				TILE_SIZE * (0.5 + latLng.lng() / 360),
				TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
		}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
	<!-- <script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&signed_in=true&callback=initMap">
		</script> -->
	<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script>
		$(function() {
			$('#slides').superslides({
				inherit_width_from: '.wide-container',
				inherit_height_from: '.wide-container',
				play: 5000,
				animation: 'fade'
			});
		});
	</script>
	<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/contacto.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/principal.js?v<?php echo date('YmdHis') ?>" type="text/javascript" charset="utf-8"></script>
	<script src="js/aos.js"></script>
	<script src="js/modaal.min.js" type="text/javascript"></script>
	<script>
		AOS.init();
		$(function() {
			$('#slides').superslides({
				inherit_width_from: '.wide-container',
				inherit_height_from: '.wide-container',
				play: 3000,
				animation: 'fade'
			});
		});
		$('.inline-modal').on('click', function() {
			content = $(this).find('.Mod-oculto').html();
			$(this).modaal({
				content_source: content,
				start_open: true
			});
		});
	</script>
</body>

</html>