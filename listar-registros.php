<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

else {
	require("libs/conexion.php");
	$notifica = $db
		->orderBy('tiempo_v', 'ASC')
		->objectBuilder()->get('vencimientos');

	$tnot = $db->count;

	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('modulo_up', 2)
		->where('permiso_up', 1)
		->objectBuilder()->get('usuarios_permisos');

	if ($db->count == 0) {
		$permisos_usuario = $db
			->where('usuario_up', $_SESSION['usuloggri'])
			->where('permiso_up', 1)
			->orderBy('Id_up', 'ASC')
			->objectBuilder()->get('usuarios_permisos');

		$menu = $db
			->where('Id_m', $permisos_usuario[0]->modulo_up)
			->objectBuilder()->get('menu');

		header('Location: ' . $menu[0]->link_m);
	}
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany Gestión de Riesgos Integrales</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/jquery.modal.theme-xenon.css" />
	<script src="js/modernizr.custom.js"></script>
	<style type="text/css" media="screen">
		#acta input[type=checkbox] {
			width: auto;
			margin: 10px;
			height: auto;
		}

		#acta p {
			margin: 0;
			text-align: left;
		}

		.Acta-cursos {
			padding-bottom: 25px;
		}

		table {
			border-collapse: collapse;
		}

		.Sede-cucuta {
			background: #88f9888f;
		}

		.Sede-villavicencio {
			background: #f971716e;
		}

		.generar-codigo {
			margin-left: 20px;
			border: none;
			border-radius: 0.2em;
			padding: 0.5em;
			cursor: pointer;
			-webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
			font-family: Open-sans-b;
			font-size: 14px;
			background: #b42d32;
			color: #fff !important;
			-webkit-transition: 0.3s;
			-moz-transition: 0.3s;
			-o-transition: 0.3s;
			-ms-transition: 0.3s;
			transition: 0.3s;
			margin-bottom: 20px;
		}
	</style>
</head>

<body>
	<?php include_once("analyticstracking.php") ?>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin" style="width:1200px">
			<div class="Contenido-admin-izq">
				<h2>Listar Registros</h2>
				<?php
				if ($_SESSION['usutipoggri'] == 'administrador' || $_SESSION['usuloggri'] == 7 || $_SESSION['usutipoggri'] == 'registros') {
					if ($_SESSION['usutipoggri'] == 'administrador' || $_SESSION['usuloggri'] == 7) {
				?>
						<a href="notificaciones" target="_blank" class="btn"><?php echo $tnot ?> - Registros a vencer</a>
						<a href="javascript:void(0)" class="btn solicitudes-altura">Solicitudes altura</a>
					<?php } ?>
					<hr>
					<p>Descargar la base de datos de registros entre fechas dadas.</p>
					<br>
					<form action="libs/acc_registros" target="_blank" method="POST" id="informe">
						<label>Desde: </label>
						<input type="date" name="inicio">
						<label>Hasta: </label>
						<input type="date" name="final">
						<input type="hidden" id="tipo" name="accion" value="informe">
						<input type="submit" value="Generar">
					</form>
				<?php } ?>
				<hr>
				<p>En esta sección podrás editar la información de registro, buscar por numero de cedula y nombre.</p>
				<br>
				<form id="buscar">
					<label>ID: </label>
					<input type="text" name="idregistro" placeholder="ID">
					<label>N° Identificación: </label>
					<input type="text" name="id" placeholder="N° Identificación">
					<label>Nombre: </label>
					<input type="text" name="nom" placeholder="Nombre">
					<label>Apellido: </label>
					<input type="text" name="apell" placeholder="Apellido">
					<input type="submit" value="Buscar">
				</form>
				<br>
				<button class="generar-codigo" type="button">Generar código</button>
				<br>
				<div class="Listar-personas">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th></th>
									<th>ID</th>
									<th>N° identificación</th>
									<th>Certificado</th>
									<th>Primer Nombre</th>
									<th>Segundo Nombre</th>
									<th>Apellidos</th>
									<th>Fecha inicio</th>
									<th>Fecha vigencia</th>
									<th>Horas</th>
									<th>Vendedor</th>
									<th>Precio</th>
									<th>Sede</th>
									<th>Editar</th>
									<th>Certificado</th>
									<th>Carnet</th>
									<th>Acta</th>
									<th>Otras Acciones</th>
								</tr>
							</thead>
							<tbody id="resultados">
							</tbody>
						</table>
					</div>
				</div>
				<div id="lista_loader">
					<div class="loader2">Cargando...</div>
				</div>
				<div id="paginacion"></div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/listado.js?v<?php echo date('YmdHis') ?>"></script>
	<script src="js/jquery.modal.min.js"></script>
</body>

</html>
