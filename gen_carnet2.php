<?php
require "libs/conexion.php";
$registro = $_GET['registro'];

$bus = $db
    ->where('Id', $registro)
    ->objectBuilder()->get('registros');

$res = $bus[0];

if ($res->foto_ct != '') {
    $nombre      = $res->nombres . ' ' . $res->apellidos;
    $documento   = $res->numero_ident;
    $expedicion  = $res->fecha_inicio;
    $expedicion  = date_create($expedicion);
    $expedicion  = date_format($expedicion, 'd-m-Y');
    $vencimiento = $res->vencimiento_ct;
    $vencimiento = date_create($vencimiento);
    $vencimiento = date_format($vencimiento, 'd-m-Y');
    $rh          = $res->rh;
    $licencia    = $res->licencia;
    $formacion   = $res->certificado;

    $horas = $res->horas;

    require_once 'libs/tcpdf.php';
    require_once 'libs/fpdi/fpdi.php';

    $exa = new TCPDF();
    $exa->addFont('conthrax', '', 'conthrax.php');
    $exa->SetFont('conthrax', '', 8);
    // $exa->SetFont('arial', '', 6);
    $estilo = '<style>
                    .nm{
                        color: #9e090f;
                    }
                    .bl{
                        color: #fff;
                        font-family: arial;
                    }
                    .bl2{
                        color: #fff;
                        font-family: arial;
                        font-size: 6;
                    }
                </style>';
    $exa->AddPage();
    $exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $exa->setJPEGQuality(100);
    $exa->Image("images/carnetfondo.jpg");

    $txt = '<table border="0" width="40%" cellpadding="0" cellspacing="0"><tr><td><strong class="bl2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convenio autorizado mediante resolución sena 189 - 2015</strong></td></tr><tr><td><strong class="nm">' . $nombre . '</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Documento: ' . $documento . '</strong></td></tr></table>';

    $exa->SetXY(13.5, 80);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="40%" cellpadding="2" cellspacing="0"><tr><td><br><strong class="bl">&nbsp;&nbsp;Expedición:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vencimiento:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RH:</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;' . $expedicion . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $vencimiento . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $rh . '</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duración:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Licencia:</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $horas . ' Hrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $licencia . '</strong></td></tr></table>';

    $exa->SetXY(13.5, 91);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="40%" cellpadding="2" cellspacing="0"><tr><td><strong class="bl2">Lic.Salud Ocupacional Res. 3753 de 2013</strong></td></tr></table>';

    $exa->SetXY(13.5, 111);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="30%" cellpadding="2" cellspacing="0"><tr><td><strong class="bl2">Formación:' . $formacion . '</strong></td></tr></table>';

    $exa->SetXY(13.5, 113.5);
    $exa->WriteHTML($estilo . $txt);
    //$exa->SetFont('arial', '', 9);
    // $txt = '<strong class="bl">Documento: '.$documento.'</strong>';
    // $exa->SetXY(25, 86);
    // $exa->WriteHTML($estilo.$txt);
    $exa->Image(substr($res->foto_ct, 3), 32, 42, 26.5);
    $exa->Output('carnet.pdf', 'I');

    // $pdf = new FPDI();
    // $pdf->setSourceFile('libs/pl_carnet.pdf');
    // $tplIdx = $pdf->importPage(1, '/MediaBox');
    // $pdf->SetPrintHeader(false);
    // $pdf->AddPage();
    // $pdf->useTemplate($tplIdx);

    // $utf8text = 'prueba';

    // $pdf->SetFont('calibrib', '', 8);
    // $pdf->SetTextColor(0, 0, 0);
    // $pdf->SetXY(31.5, 24.5);
    // $pdf->Write(0, $nombre);
    // $pdf->SetFont('calibri', '', 10);
    // $pdf->SetTextColor(0, 0, 0);
    // $pdf->SetXY(53, 29);
    // $pdf->Write(0, $documento);
    // $pdf->SetXY(53, 33);
    // $pdf->Write(0, $expedicion);
    // $pdf->SetXY(53, 37);
    // $pdf->Write(0, $vencimiento);
    // $pdf->SetXY(53, 40.5);
    // $pdf->Write(0, $rh);
    // $pdf->SetXY(53, 44.5);
    // $pdf->Write(0, $licencia);
    // $pdf->SetXY(77, 44.5);
    // $pdf->Write(0, $horas . ' Hrs');

    // $n_formacion = '';

    // $l_formacion = explode(' ', $formacion);
    // $t_formacion = count($l_formacion);
    // if ($t_formacion > 10) {
    //     for ($i = 0; $i < 10; $i++) {
    //         $n_formacion .= $l_formacion[$i] . ' ';
    //     }

    //     $pdf->SetXY(46, 57);
    //     $pdf->SetFont('calibrib', '', 5.3);
    //     $pdf->SetTextColor(255, 255, 255);
    //     $pdf->Write(0, $n_formacion);

    //     $n_formacion2 = '';
    //     for ($i = 10; $i < $t_formacion; $i++) {
    //         $n_formacion2 .= $l_formacion[$i] . ' ';
    //     }
    //     $pdf->SetXY(46, 59);
    //     $pdf->SetFont('calibrib', '', 5.3);
    //     $pdf->SetTextColor(255, 255, 255);
    //     $pdf->Write(0, $n_formacion2);

    // } else {
    //     $n_formacion = $formacion;
    //     $pdf->SetXY(46, 58);
    //     $pdf->SetFont('calibrib', '', 6);
    //     $pdf->SetTextColor(255, 255, 255);
    //     $pdf->Write(0, $n_formacion);
    // }

    // $pdf->Image(substr($res->foto_ct, 3), 89.5, 21, 25);

    // $pdf->Output('Carnet-' . $res->Id . '_' . $documento . '.pdf');
} else {
    echo 'Error al generar el carnet: Falta foto en el registro.';
}
