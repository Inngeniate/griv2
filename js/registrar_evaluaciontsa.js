$(document).ready(function() {

	$('body').on('keydown', '.ntxt', function(e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}

	});

	$('#registro-comprobar').on('submit', function(e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'evaluacion[accion]',
			'value': 'comprobar_evaluacion'
		});

		$.post('libs/acc_evaluaciontsa', data, function(data) {
			if (data.status == true) {
				$('#fondo').remove();
				$('.comprobar-registro').empty();
				$('.comprobar-registro').after(data.form_evaluacion);
				$('#identifica').val(data.identificacion);
			} else {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>" + data.error + "</span></div>");
				setTimeout(function() {
					$('#fondo').fadeIn('fast', function() {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function() {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, 'json');
	});

	$('body').on('submit', '#registro-evaluacion', function(e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'evaluacion[accion]',
			'value': 'nuevo_evaluacion'
		});

		$.post('libs/acc_evaluaciontsa', data, function(data) {
			if (data.status == true) {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
				setTimeout(function() {
					$('#fondo').fadeIn('fast', function() {
						$('#rp').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function() {
					$("#rp").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 2000);
				$('#registro-evaluacion').remove();
				$('.comprobar-registro').append('<label>Su evaluación ha sido registrada con éxito.</label>');
			} else {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>" + data.error + "</span></div>");
				setTimeout(function() {
					$('#fondo').fadeIn('fast', function() {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function() {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, 'json');
	});
});