$(document).ready(function () {
    tempid = 0;
    temnom = '';
    bsq_nombre = '';
    cambiar_pag('1');

    $('#nueva-competencia').on('submit', function (e) {
        e.preventDefault();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 400);
        data = $(this).serializeArray();
        if (tempid == 0) {
            data.push({
                'name': 'accion',
                'value': 'nueva_competencia'
            });
        } else {
            data.push({
                'name': 'accion',
                'value': 'editar_competencia'
            }, {
                'name': 'idcompetencia',
                'value': tempid
            });
        }

        $.post('libs/acc_competencias', data, function (data) {
            $('#fondo').remove();
            if (data.status == 'Correcto') {
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
                setTimeout(function () {
                    $('#fondo').fadeIn('fast', function () {
                        $('#rp').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function () {
                    $("#rp").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 2000);
                tempid = 0;
                $('input').not('input[type=submit]').val('');
                cambiar_pag('1');
            } else if (data.status == 'error-1') {
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
                setTimeout(function () {
                    $('#fondo').fadeIn('fast', function () {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function () {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            } else if (data.status == 'error-2') {
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>Ya existe una competencia con ese nombre</span></div>");
                setTimeout(function () {
                    $('#fondo').fadeIn('fast', function () {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function () {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            }
        }, 'json');
    });

    $('body').on('click', '.editar', function () {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 400);
        tempid = $(this).closest('tr').prop('id');
        temnom = $(this).attr('data-nombre');
        $('#nombre').val(temnom);
        $('#fondo').remove();
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#nueva-competencia").offset().top - 200
        }, 100);
    });

    $('body').on('click', '.eliminar', function () {
        registro = $(this);
        modal({
            type: 'confirm',
            title: 'Eliminar Competencia',
            text: 'Desea eliminar la competencia ?',
            size: 'small',
            callback: function (result) {
                if (result === true) {
                    id = registro.closest('tr').prop('id');
                    registro.closest('tr').fadeOut(500, function () {
                        registro.remove();
                    });
                    $.post('libs/acc_competencias', {
                        accion: 'eliminar_competencia',
                        idcompetencia: id
                    });
                }
            },
            closeClick: false,
            theme: 'xenon',
            animate: true,
            buttonText: {
                yes: 'Confirmar',
                cancel: 'Cancelar'
            }
        });
    });
});

$('#buscar').on('submit', function (e) {
    e.preventDefault();
    bsq_nombre = $('.nombre_bsq').val();
    cambiar_pag(1);
});

$('body').on('keydown', '.ntext', function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
    }

    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function cambiar_pag(pag_id) {
    $('#lista_loader').show();
    $('#resultados').empty();
    data = 'pagina=' + pag_id + '&nombre=' + bsq_nombre + '&accion=listar';
    $.post('libs/acc_competencias', data, function (data) {
        $('#lista_loader').hide();
        $('#resultados').html(data.registros);
        $('#paginacion').html(data.paginacion);
    }, "json");
}

