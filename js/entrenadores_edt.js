$(document).ready(function () {
	sp_signature = '';

	setTimeout(function () {
		$('.capacitacion').change();
	}, 200);

	$('body').on('click', '#firma-digital', function () {
		modal({
			type: 'inverted',
			title: 'Firma Entrenador',
			text: '<canvas id="can"  style="border:1px solid #ccc;"></canvas>',
			size: 'normal',
			buttons: [{
				text: 'Limpiar',
				val: 'clear',
				eKey: true,
				addClass: 'btn-light',
				onClick: function (dialog) {
					var m = confirm("Desea borrar el contenido ?");
					if (m) {
						ctx.clearRect(0, 0, w, h);
					}
				}
			}, {
				text: 'Guardar',
				val: 'ok',
				eKey: true,
				addClass: 'btn-light-green',
				onClick: function (dialog) {
					var dataURL = canvas.toDataURL();
					sp_signature = canvas.toDataURL();
					$('#canvasimg').attr('src', dataURL);
					$('#canvasimg').css('display', 'block');
					$('.borrar-firma').remove();
					$('#canvasimg').after('<a href="javascript://" class="Btn-gris-normal borrar-firma"><i class="icon-bin"> </i>Borrar Firma</a>');
					return true;
				}
			}, {
				text: 'Cancelar',
				val: 'cancel',
				eKey: true,
				addClass: 'btn-light-red',
				onClick: function (dialog) {
					return true;
				}
			}],
			onShow: function (r) {
				canvas_init();
			},
			closeClick: false,
			animate: true,
		});
	});

	var canvas, ctx, flag = false,
		prevX = 0,
		currX = 0,
		prevY = 0,
		currY = 0,
		dot_flag = false;

	var x = "black",
		y = 2;

	function canvas_init() {
		canvas = document.getElementById('can');
		ctx = canvas.getContext("2d");
		canvas.width = $('.modal-text').width() - 5;
		canvas.height = $('.modal-text').height();
		w = canvas.width;
		h = canvas.height;

		$('#can').on('mousemove', function (e) {
			findxy('move', e);
		});
		$('#can').on('mousedown', function (e) {
			findxy('down', e);
		});
		$('#can').on('mouseup', function (e) {
			findxy('up', e);
		});
		$('#can').on('mouseout', function (e) {
			findxy('out', e);
		});

		can.addEventListener("touchstart", touchDown, false);
		can.addEventListener("touchmove", touchXY, true);
		can.addEventListener("touchend", touchUp, false);

		document.body.addEventListener("touchcancel", touchUp, false);

	}

	function touchDown(e) {
		mouseIsDown = 1;
		prevX = currX;
		prevY = currY;
		rect = canvas.getBoundingClientRect();
		currX = e.targetTouches[0].clientX - rect.left;
		currY = e.targetTouches[0].clientY - rect.top;
		touchXY();
	}

	function touchUp() {
		mouseIsDown = 0;
		draw();
	}

	function touchXY(e) {
		if (!e) {
			e = event;
		}
		e.preventDefault();
		if (mouseIsDown) {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.targetTouches[0].clientX - rect.left;
			currY = e.targetTouches[0].clientY - rect.top;
			draw();
		}
	}

	function findxy(res, e) {
		if (res == 'down') {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.clientX - rect.left;
			currY = e.clientY - rect.top;
			flag = true;
			dot_flag = true;
			if (dot_flag) {
				ctx.beginPath();
				ctx.fillStyle = x;
				ctx.fillRect(currX, currY, 2, 2);
				ctx.closePath();
				dot_flag = false;
			}
		}
		if (res == 'up' || res == "out") {
			flag = false;
		}
		if (res == 'move') {
			if (flag) {
				prevX = currX;
				prevY = currY;
				rect = canvas.getBoundingClientRect();
				currX = e.clientX - rect.left;
				currY = e.clientY - rect.top;
				draw();
			}
		}
	}

	function draw() {
		ctx.beginPath();
		ctx.moveTo(prevX, prevY);
		ctx.lineTo(currX, currY);
		ctx.strokeStyle = x;
		ctx.lineWidth = y;
		ctx.stroke();
		ctx.closePath();
	}

	$('body').on('click', '.borrar-firma', function () {
		$('#canvasimg').css('display', 'none').attr('src', '');
		$(this).remove();
		sp_signature = 1;
	});

	//// firma adjunta

	$('#firma').on('change', function () {
		if ($(this).val() != '') {
			var files = document.getElementById('firma').files;
			if (files.length > 0) {
				$.each(files, function (i, obj) {
					if (/^image\/\w+/.test(obj.type)) {
						getBase64(files[0]);
					} else {
						$('#firma').val('');
						$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp2'><span>La firma no tiene un formato válido</span></div>");
						setTimeout(function () {
							$('#fondo').fadeIn('fast', function () {
								$('#rp2').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function () {
							$("#rp2").fadeOut();
							$('#fondo').fadeOut('fast');
						}, 3000);
					}
				});
			}
		}
	});

	function getBase64(file) {
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function () {
			sp_signature = reader.result;
		};
		reader.onerror = function (error) {
			sp_signature = '';
			('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append("<div class='rp' id='rp2'><span>" + error + "</span></div>");
			setTimeout(function () {
				$('#fondo').fadeIn('fast', function () {
					$('#rp2').animate({
						'top': '50%'
					}, 50).fadeIn();
				});
			}, 400);
			setTimeout(function () {
				$("#rp2").fadeOut();
				$('#fondo').fadeOut('fast');
			}, 3000);
		};
	}

	$('.capacitacion').on('change', function () {
		$('.Content-rescate').hide();

		switch ($(this).val()) {
			case 'cea':
				$('.Tipo-entrenador').html('CEA');
				$('.Content-rescate input').removeAttr('required').val('');
				break;
			case 'espaciosconfinados':
				$('.Tipo-entrenador').html('EC');
				$('.Content-rescate input').removeAttr('required').val('');
				break;
			case 'rescateindustrial':
				$('.Content-rescate').show();
				$('.Tipo-entrenador').html('TA');
				$('.Content-rescate input').attr('required', 'required');
				break;
			default:
				$('.Tipo-entrenador').html('TA');
				$('.Content-rescate input').removeAttr('required').val('');
				break;
		}
	})

	$('#editar-entrenador').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		var data = new FormData();
		/*
		$.each($('#firma'), function(i, obj) {
			$.each(obj.files, function(j, file) {
				data.append('firma', file);
			});
		});
		*/

		data.append('accion', 'editar_entrenador');

		otradata = $(this).serializeArray();

		$.each(otradata, function (key, input) {
			data.append(input.name, input.value);
		});

		data.append('entrena[firma]', sp_signature);

		$.ajax({
			url: 'libs/acc_entrenadores',
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Registro editado</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
					data = new FormData();
					setTimeout(function () {
						location.reload();
					}, 2300);
				} else {
					if (data.status == false) {
						msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.';
					}
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});
});
