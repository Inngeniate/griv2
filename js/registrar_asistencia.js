$(document).ready(function () {
	sp_signature = '';
	idempleado = '';


	$('.select-tipo').on('change', function () {
		if ($(this).val() != '') {
			$('.identifica').attr('disabled', false);
		} else {
			$('.identifica').attr('disabled', true).val('');
		}
	});

	$('.Identificar-empleado').on('click', function () {
		$('.crear-externo').hide();
		$('.Listado-preguntas').hide();
		$('.Registro-temperatura').hide();
		$('.Div-sesiones').hide();
		$('.Registro-firma').hide();
		$('select', '.Listado-preguntas').attr('required', true);
		$('select', '.Div-sesiones').attr('required', false);
		$('select', '.Registro-firma').attr('required', true);
		$('.Registro-sesiones').empty().hide();
		if ($('.select-tipo').val() == '' || $('.identifica').val() == '') {
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append("<div class='rp' id='rp2'><span>Debe seleccionar el tipo e ingresar un número identificación</span></div>");
			setTimeout(function () {
				$('#fondo').fadeIn('fast', function () {
					$('#rp2').animate({
						'top': '50%'
					}, 50).fadeIn();
				});
			}, 400);
			setTimeout(function () {
				$("#rp2").fadeOut();
				$('#fondo').fadeOut('fast');
			}, 4000);
		} else {
			$('input', '.crear-externo').not('input[type=submit]').val('');
			$('input', '.Registro-temperatura').not('input[type=submit]').val('');
			$('select', '.Listado-preguntas').val('');
			$('select', '.Registro-temperatura').val('');
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="loader-inner ball-beat">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function () {
				$('#fondo').fadeIn('fast');
			}, 400);

			data = [];

			data.push({
				'name': 'asistencia[accion]',
				'value': 'Comprobar'
			}, {
				'name': 'asistencia[identifica]',
				'value': $('.identifica').val()
			}, {
				'name': 'asistencia[tipo]',
				'value': $('.select-tipo').val()
			});

			$.post('libs/acc_asistencia', data, function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('.Nombre-empleado').val(data.info.nombre_em);
					$('.Identifica-empleado').val(data.info.identificacion_em);
					$('.Correo-empleado').val(data.info.correo_em);
					$('.Telefono-empleado').val(data.info.telefono_em);
					$('.crear-externo').show(data.info.nombre_em);
					idempleado = data.info.Id_em;
					if (data.encuesta == false && data.info.tipo_em != 'X') {
						$('.Listado-preguntas').show();
						$('.Registro-temperatura').show();
						$('.Registro-firma').show();
					} else {
						$('select', '.Listado-preguntas').attr('required', false);
						$('input', '.Registro-temperatura').attr('required', false);
						$('select', '.Registro-temperatura').attr('required', false);
					}

					if (data.encuesta == true && data.info.tipo_em == 'E') {
						$('.Registro-temperatura').show();
						$('select', '.Registro-firma').attr('required', false);
					}

					if (data.info.tipo_em == 'X') {
						$('.Div-sesiones').show();
						$('.Registro-sesiones').show();
						$('select', '.Registro-firma').attr('required', false);
					}

					$('.Btn-guardar').show();
				} else {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + data.msg + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
						if ($('.select-tipo').val() == 'X') {
							$('.crear-externo').show();
							// $('.Listado-preguntas').show();
							// $('.Registro-temperatura').show();
							$('.Registro-sesiones').show();
							$('select', '.Listado-preguntas').attr('required', false);
							$('select', '.Registro-temperatura').attr('required', false);
							$('input', '.Registro-temperatura').attr('required', false);
							$('input', '.Div-sesiones').attr('required', true);
							$('select', '.Registro-firma').attr('required', false);
							$('.Div-sesiones').show();
							$('.Btn-guardar').show();
						}
					}, 3000);
				}
			}, 'json');
		}
	});

	$('.sesiones').on('change', function () {
		$('.Registro-sesiones').empty();
		if ($(this).val() != '') {
			for (i = 0; i < $(this).val(); i++) {
				$('.Registro-sesiones').append('<div class="Registro-der">' +
					'<input type="datetime-local" name="asistencia[fecha][]" required>' +
					'</div>');
			}
		}

	});


	$('body').on('click', '#firma', function () {
		modal({
			type: 'inverted',
			title: 'Firma',
			text: '<canvas id="can"  style="border:1px solid #ccc;"></canvas>',
			size: 'normal',
			buttons: [{
				text: 'Limpiar',
				val: 'clear',
				eKey: true,
				addClass: 'btn-light',
				onClick: function (dialog) {
					var m = confirm("Desea borrar el contenido ?");
					if (m) {
						ctx.clearRect(0, 0, w, h);
					}
				}
			}, {
				text: 'Guardar',
				val: 'ok',
				eKey: true,
				addClass: 'btn-light-green',
				onClick: function (dialog) {
					var dataURL = canvas.toDataURL();
					sp_signature = canvas.toDataURL();
					$('#canvasimg').attr('src', dataURL);
					$('#canvasimg').css('display', 'block');
					$('.borrar-firma').remove();
					$('#canvasimg').after('<a href="javascript://" class="Btn-gris-normal borrar-firma"><i class="icon-bin"> </i>Borrar Firma</a>');
					return true;
				}
			}, {
				text: 'Cancelar',
				val: 'cancel',
				eKey: true,
				addClass: 'btn-light-red',
				onClick: function (dialog) {
					return true;
				}
			}],
			onShow: function (r) {
				canvas_init();
			},
			closeClick: false,
			animate: true,
		});
	});

	var canvas, ctx, flag = false,
		prevX = 0,
		currX = 0,
		prevY = 0,
		currY = 0,
		dot_flag = false;

	var x = "black",
		y = 2;

	function canvas_init() {
		canvas = document.getElementById('can');
		ctx = canvas.getContext("2d");
		canvas.width = $('.modal-text').width() - 5;
		canvas.height = $('.modal-text').height();
		w = canvas.width;
		h = canvas.height;

		$('#can').on('mousemove', function (e) {
			findxy('move', e);
		});
		$('#can').on('mousedown', function (e) {
			findxy('down', e);
		});
		$('#can').on('mouseup', function (e) {
			findxy('up', e);
		});
		$('#can').on('mouseout', function (e) {
			findxy('out', e);
		});

		can.addEventListener("touchstart", touchDown, false);
		can.addEventListener("touchmove", touchXY, true);
		can.addEventListener("touchend", touchUp, false);

		document.body.addEventListener("touchcancel", touchUp, false);

	}

	function touchDown(e) {
		mouseIsDown = 1;
		prevX = currX;
		prevY = currY;
		rect = canvas.getBoundingClientRect();
		currX = e.targetTouches[0].clientX - rect.left;
		currY = e.targetTouches[0].clientY - rect.top;
		touchXY();
	}

	function touchUp() {
		mouseIsDown = 0;
		draw();
	}

	function touchXY(e) {
		if (!e) {
			e = event;
		}
		e.preventDefault();
		if (mouseIsDown) {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.targetTouches[0].clientX - rect.left;
			currY = e.targetTouches[0].clientY - rect.top;
			draw();
		}
	}

	function findxy(res, e) {
		if (res == 'down') {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.clientX - rect.left;
			currY = e.clientY - rect.top;
			flag = true;
			dot_flag = true;
			if (dot_flag) {
				ctx.beginPath();
				ctx.fillStyle = x;
				ctx.fillRect(currX, currY, 2, 2);
				ctx.closePath();
				dot_flag = false;
			}
		}
		if (res == 'up' || res == "out") {
			flag = false;
		}
		if (res == 'move') {
			if (flag) {
				prevX = currX;
				prevY = currY;
				rect = canvas.getBoundingClientRect();
				currX = e.clientX - rect.left;
				currY = e.clientY - rect.top;
				draw();
			}
		}
	}

	function draw() {
		ctx.beginPath();
		ctx.moveTo(prevX, prevY);
		ctx.lineTo(currX, currY);
		ctx.strokeStyle = x;
		ctx.lineWidth = y;
		ctx.stroke();
		ctx.closePath();
	}

	$('body').on('click', '.borrar-firma', function () {
		$('#canvasimg').css('display', 'none').attr('src', '');
		$(this).remove();
		sp_signature = 1;
	});

	$('#registro').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		data = $(this).serializeArray();

		data.push({
			'name': 'asistencia[accion]',
			'value': 'Registro'
		});

		formData = new FormData();

		$.each(data, function (key, input) {
			formData.append(input.name, input.value);
		});

		formData.append('asistencia[firma]', sp_signature);
		formData.append('asistencia[idempleado]', idempleado);

		$.ajax('libs/acc_asistencia', {
			method: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					if (data.externo == 1) {
						window.location.href = "registro-asistencia-encuesta?as=" + data.id;
					}
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
					formData = new FormData();
					sp_signature = 1;
					$('input').not('input[type=submit]').val('');
					$('select').val('');
					$('#canvasimg').css('display', 'none').attr('src', '');
					$('.borrar-firma').remove();
					$('.crear-externo').hide();
					$('.Listado-preguntas').hide();
					$('select', '.Listado-preguntas').attr('required', true);
					$('.Registro-temperatura').hide();
					$('.Div-sesiones').hide();
					$('.Registro-sesiones').hide();
					$('.Registro-sesiones').empty();
					$('.Btn-guardar').hide();
				} else {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>No se pudo realizar el registro</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});

});
