$(document).ready(function() {

    var formData = new FormData();
    tcompart = $('.temp').length;

    $('.auto_ciu').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: 'libs/certifica2.php',
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function(data) {
                    if (!data.length) {
                        result = [{
                            label: 'No se han encontrado resultados',
                            value: response.term
                        }];
                        response(result);
                    } else {
                        response($.map(data, function(item) {
                            code = item.split("|");
                            return {
                                value: code[1],
                                data: item
                            };
                        }));
                    }
                }
            });
        },
        autoFocus: true,
        minLength: 2,
        select: function(event, ui) {
            if (ui.item.label != 'No se han encontrado resultados') {
                names = ui.item.data.split("|");
            } else {
                event.preventDefault();
            }
        }
    });

    $('#ed_certificado').on('submit', function(e) {
        e.preventDefault();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);

        data = $(this).serializeArray();

        $.each(data, function(key, input) {
            formData.append(input.name, input.value);
        });

        formData.append('certifica[accion]', 'editar');
        formData.append('certifica[tipoen]', 'aforo');

        $.ajax('libs/certifica2', {
            method: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'Correcto') {
                    form = $('<form action="libs/certifica2" method="post">' +
                        '<input type="hidden" name="certifica[accion]" value="generar">' +
                        '<input type="hidden" name="certifica[certificado]" value="' + data.certificado + '">' +
                        '<input type="hidden" name="certifica[id]" value="' + data.certificadoid + '">' +
                        '</form>');
                    form.appendTo($('body')).submit().remove();
                    // $('#fondo').remove();
                    // $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    // $('#fondo').append("<div class='rp' id='rp'><span>Editado Correctamente</span></div>");
                    // setTimeout(function() {
                    // 	$('#fondo').fadeIn('fast', function() {
                    // 		$('#rp').animate({
                    // 			'top': '50%'
                    // 		}, 50).fadeIn();
                    // 	});
                    // }, 400);
                    // setTimeout(function() {
                    // 	$("#rp").fadeOut();
                    // 	$('#fondo').fadeOut('fast');
                    // }, 2000);
                } else {
                    $('#fondo').remove();
                    $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    $('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
                    setTimeout(function() {
                        $('#fondo').fadeIn('fast', function() {
                            $('#rp2').animate({
                                'top': '50%'
                            }, 50).fadeIn();
                        });
                    }, 400);
                    setTimeout(function() {
                        $("#rp2").fadeOut();
                        $('#fondo').fadeOut('fast');
                    }, 3000);
                }
            },
            error: function() {
                console.log('error subiendo');
            }
        });
    });

    $('#agr_comp').on('click', function() {
        if (tcompart < 4) {
            $('#compartimientos').append('<div class="temp"><hr>' +
                '<a class="quitar">Eliminar</a><div class="Registro-der">' +
                '<label>Capacidad Operacional GLS *</label>' +
                '<input type="text" placeholder="Capacidad Operacional GLS" name="certifica[capacidadope][]" required>' +
                '<label>Capacidad Nominal BLS *</label>' +
                '<input type="text" placeholder="Capacidad Nominal BLS" name="certifica[capacidadnom][]" required>' +
                '<label>Altura Operacional  (m)*</label>' +
                '<input type="text" placeholder="Altura Operacional" name="certifica[alturaopera][]" required>' +
                '<label>Altura Referencial (m)*</label>' +
                '<input type="text" placeholder="Altura Referencial " name="certifica[alturarefe][]" required>' +
                '</div>' +
                '<div class="Registro-der">' +
                '<label>Dimensión del tanque </label>' +
                '<label>Diámetro (m3)*</label>' +
                '<input type="text" placeholder="Diámetro" name="certifica[diametro][]" required>' +
                '<label>Alto (m)*</label>' +
                '<input type="text" placeholder="Alto" name="certifica[alto][]" required>' +
                '<label>Ancho (m)*</label>' +
                '<input type="text" placeholder="Ancho" name="certifica[ancho][]" required>' +
                '<label>Largo (m)*</label>' +
                '<input type="text" placeholder="Largo" name="certifica[largo][]" required>' +
                '</div></div>');
            tcompart++;
        }
    });

    $('body').on('click', '.quitar', function() {
        $(this).parent().remove();
        tcompart--;
    });

});
