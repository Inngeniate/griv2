$(document).ready(function () {
    cambiar_pag1('0');

    $('#navegador li').on('click', function () {
        tab = $('a', this).prop('id');
        if (tab == 'ensayo') {
            $('#reporte, #aforo, #seguridad, #inspeccion').removeClass('activo');
            $('#ensayo').addClass('activo');
            $('.ensayo').fadeIn();
            $('.reporte, .aforo, .seguridad, .inspeccion').fadeOut(0);
            cambiar_pag1('0');
        } else if (tab == 'reporte') {
            $('#ensayo, #aforo, #seguridad, #inspeccion').removeClass('activo');
            $('#reporte').addClass('activo');
            $('.reporte').fadeIn();
            $('.ensayo, .aforo, .seguridad, .inspeccion').fadeOut(0);
            cambiar_pag2('0');
        } else if (tab == 'aforo') {
            $('#ensayo, #reporte, #seguridad, #inspeccion').removeClass('activo');
            $('#aforo').addClass('activo');
            $('.aforo').fadeIn();
            $('.ensayo, .reporte, .seguridad, .inspeccion').fadeOut(0);
            cambiar_pag3('0');
        } else if (tab == 'seguridad') {
            $('#ensayo, #aforo, #reporte, #inspeccion').removeClass('activo');
            $('#seguridad').addClass('activo');
            $('.seguridad').fadeIn();
            $('.ensayo, .aforo, .reporte, .inspeccion').fadeOut(0);
            cambiar_pag4('0');
        } else if (tab == 'inspeccion') {
            $('#ensayo, #aforo, #reporte, #seguridad').removeClass('activo');
            $('#inspeccion').addClass('activo');
            $('.inspeccion').fadeIn();
            $('.ensayo, .aforo, .reporte, .seguridad').fadeOut(0);
            cambiar_pag5('0');
        }
    });

    $('#buscar1').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        $('#lista_loader1').show();
        $('#resultados1').empty();
        $.post('libs/certifica2.php', data, function (data) {
            $('#lista_loader1').hide();
            $('#resultados1').html(data.registros);
            $('.pagina1').html(data.paginacion1);
        }, "json");
    });

    $('#buscar2').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        $('#lista_loader2').show();
        $('#resultados2').empty();
        $.post('libs/certifica2.php', data, function (data) {
            $('#lista_loader2').hide();
            $('#resultados2').html(data.registros);
            $('.pagina2').html(data.paginacion1);
        }, "json");
    });

    $('#buscar3').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        $('#lista_loader3').show();
        $('#resultados3').empty();
        $.post('libs/certifica2.php', data, function (data) {
            $('#lista_loader3').hide();
            $('#resultados3').html(data.registros);
            $('.pagina3').html(data.paginacion1);
        }, "json");
    });

    // $('#ls_tipo').on('change', function() {
    //     cambiar_pag4(1);
    // });

    $('#buscar4').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        $('#lista_loader4').show();
        $('#resultados4').empty();
        $.post('libs/certifica2.php', data, function (data) {
            $('#lista_loader4').hide();
            $('#resultados4').html(data.registros);
            $('.pagina4').html(data.paginacion);
        }, "json");
    });

    $('#buscar5').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeArray();
        $('#lista_loader5').show();
        $('#resultados5').empty();
        $.post('libs/certifica2.php', data, function (data) {
            $('#lista_loader5').hide();
            $('#resultados5').html(data.registros);
            $('.pagina5').html(data.paginacion1);
        }, "json");
    });

    $('body').on('click', '#resultados1 tr td', function () {
        if ($(this).index() != 8 && $(this).index() != 9) {
            id = $(this).parent().prop('id');
            form = $('<form action="libs/certifica2" method="get" target="_blank">' +
                '<input type="hidden" name="certifica[accion]" value="generar">' +
                '<input type="hidden" name="certifica[certificado]" value="ensayo">' +
                '<input type="hidden" name="certifica[id]" value="' + id + '">' +
                '</form>');
            form.appendTo($('body')).submit().remove();
        }
    });

    $('body').on('click', '#resultados2 tr td:not(:last-child)', function () {
        id = $(this).parent().prop('id');
        form = $('<form action="libs/certifica2" method="get" target="_blank">' +
            '<input type="hidden" name="certifica[accion]" value="generar">' +
            '<input type="hidden" name="certifica[certificado]" value="reporte">' +
            '<input type="hidden" name="certifica[id]" value="' + id + '">' +
            '</form>');
        form.appendTo($('body')).submit().remove();
    });

    $('body').on('click', '#resultados3 tr td', function () {
        if ($(this).index() != 6) {
            id = $(this).parent().prop('id');
            form = $('<form action="libs/certifica2" method="get" target="_blank">' +
                '<input type="hidden" name="certifica[accion]" value="generar">' +
                '<input type="hidden" name="certifica[certificado]" value="aforo">' +
                '<input type="hidden" name="certifica[id]" value="' + id + '">' +
                '</form>');
            form.appendTo($('body')).submit().remove();
        }
    });

    $('body').on('click', '#resultados4 tr td:not(:last-child)', function () {
        if ($('#ls_tipo').val() == 1) {
            gen = 'arnes';
        } else {
            gen = 'eslinga';
        }
        id = $(this).parent().prop('id');
        form = $('<form action="libs/certifica2" method="get" target="_blank">' +
            '<input type="hidden" name="certifica[accion]" value="generar">' +
            '<input type="hidden" name="certifica[certificado]" value="' + gen + '">' +
            '<input type="hidden" name="certifica[id]" value="' + id + '">' +
            '</form>');
        form.appendTo($('body')).submit().remove();
    });

    $('body').on('click', '#resultados5 tr td:not(:last-child)', function () {
        id = $(this).parent().prop('id');
        form = $('<form action="libs/certifica2" method="get" target="_blank">' +
            '<input type="hidden" name="certifica[accion]" value="generar">' +
            '<input type="hidden" name="certifica[certificado]" value="inspeccion">' +
            '<input type="hidden" name="certifica[id]" value="' + id + '">' +
            '</form>');
        form.appendTo($('body')).submit().remove();
    });

    $('body').on('click', '.eliminar', function () {
        registro = $(this);
        modal({
            type: 'confirm',
            title: 'Eliminar Certificado',
            text: 'Desea eliminar el certificado ?',
            size: 'small',
            callback: function (result) {
                if (result === true) {
                    id = registro.closest('tr').prop('id');
                    registro.closest('tr').fadeOut(500, function () {
                        $(this).remove();
                    });
                    $.post('libs/certifica2.php', {
                        'certifica[accion]': 'eliminar',
                        'certifica[id]': id,
                        'certifica[tipo]': registro.attr('data-tipo'),
                    });
                }
            },
            closeClick: false,
            theme: 'xenon',
            animate: true,
            buttonText: {
                yes: 'Confirmar',
                cancel: 'Cancelar'
            }
        });
    });

});

function cambiar_pag1(pag_id) {
    $('#lista_loader1').show();
    $('#resultados1').empty();
    reporte = $('#breporte').val();
    placa = $('#bplaca').val();
    data = 'pagina=' + pag_id + '&certifica[accion]=lista_ensayo&certifica[reporte]=' + reporte + '&certifica[placa]=' + placa;
    $.post('libs/certifica2.php', data, function (data) {
        $('#lista_loader1').hide();
        $('#resultados1').html(data.registros);
        $('.pagina1').html(data.paginacion);
    }, "json");
}

function cambiar_pag2(pag_id) {
    $('#lista_loader2').show();
    $('#resultados2').empty();
    reporte = $('#breporte2').val();
    placa = $('#bplaca2').val();
    data = 'pagina=' + pag_id + '&certifica[accion]=lista_reporte&certifica[reporte]=' + reporte + '&certifica[placa]=' + placa;
    $.post('libs/certifica2.php', data, function (data) {
        $('#lista_loader2').hide();
        $('#resultados2').html(data.registros);
        $('.pagina2').html(data.paginacion);
    }, "json");
}

function cambiar_pag3(pag_id) {
    $('#lista_loader3').show();
    $('#resultados3').empty();
    reporte = $('#baforo').val();
    placa = $('#bplaca3').val();
    data = 'pagina=' + pag_id + '&certifica[accion]=lista_aforo&certifica[reporte]=' + reporte + '&certifica[placa]=' + placa;
    $.post('libs/certifica2.php', data, function (data) {
        $('#lista_loader3').hide();
        $('#resultados3').html(data.registros);
        $('.pagina3').html(data.paginacion);
    }, "json");
}

function cambiar_pag4(pag_id) {
    $('#lista_loader4').show();
    $('#resultados4').empty();
    reporte = $('#ls_tipo').val();
    data = 'pagina=' + pag_id + '&certifica[accion]=lista_seguridad&certifica[reporte]=' + reporte;
    $.post('libs/certifica2.php', data, function (data) {
        $('#lista_loader4').hide();
        $('#resultados4').html(data.registros);
        $('.pagina4').html(data.paginacion);
    }, "json");
}

function cambiar_pag5(pag_id) {
    $('#lista_loader5').show();
    $('#resultados5').empty();
    placav = $('#placav').val();
    data = 'pagina=' + pag_id + '&certifica[accion]=lista_inspeccion&certifica[placa]=' + placav;
    $.post('libs/certifica2.php', data, function (data) {
        $('#lista_loader5').hide();
        $('#resultados5').html(data.registros);
        $('.pagina5').html(data.paginacion);
    }, "json");
}
