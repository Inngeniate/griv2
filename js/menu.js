$(document).ready(function() {
	$('.Contenido-sub-menu').on('click', function() {
		if ($(this).hasClass('activo')) {
			$(this).next('ul').hide('100');
			$(this).removeClass('activo');
		} else {
			$(this).next('ul').show('50');
			$(this).addClass('activo');
		}
	});

	$('.Contenedor-principal-izq').toggleClass('Conten-oculto');

	$('#Drop').bind('click', function() {
		$('.Contenedor-principal-izq').toggleClass('Conten-oculto');
	});
});