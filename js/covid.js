$(document).ready(function () {
	moment.locale('es');
	finicio = moment().isoWeekday(1).format('DD/MM/YYYY');
	ffin = moment().isoWeekday(6).format('DD/MM/YYYY');
	data = [];
	cambiar_pag('0');

	//listar

	$('#buscar').on('submit', function (e) {
		e.preventDefault();
		data = $(this).serializeArray();
		cambiar_pag('0');
	});

	$('.Exportar').on('click', function () {
		modal({
			type: 'inverted',
			title: '',
			text: '<div class="Registro"><label>Semana</label><input type="text" class="Semana"><label>N° identificación</label><input type="text" class="Usuario"></div>',
			size: 'normal',
			buttons: [{
				text: 'Exportar',
				val: 'ok',
				eKey: true,
				addClass: 'btn-light-green',
				onClick: function (dialog) {
					$('<form action="asistencia_informe" method="post" target="_blank" style="display:none"><input type="text" name="asistencia[semana]" value="' + $('.Semana').val() + '"><input type="text" name="asistencia[usuario]"  value="' + $('.Usuario').val() + '"></form>').appendTo('body').submit().remove();
					return true;
				}
			}, {
				text: 'Cancelar',
				val: 'cancel',
				eKey: true,
				addClass: 'btn-light-red',
				onClick: function (dialog) {
					return true;
				}
			}],
			closeClick: false,
			animate: true,
		});


		$('.Semana').daterangepicker({
			singleDatePicker: true,
			"opens": "center",
			"showDropdowns": true,
			"autoApply": true,
			"linkedCalendars": false,
			autoUpdateInput: false,
			"locale": {
				"fromLabel": "Desde",
				"toLabel": "Hasta",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Diciembre"
				],
			},
			isInvalidDate: function (date) {
				return (date.day() == 0 || date.day() == 7);
			}
		});

	});

	$('body').on('apply.daterangepicker', '.Semana', function (ev, picker) {
		var date = moment(picker.startDate);
		dini = (date.subtract((date.day()) - 1, 'days')).format('DD/MM/YYYY');
		dfin = (date.add(6 - (date.day()), 'days')).format('DD/MM/YYYY');
		$(this).val(dini + ' - ' + dfin);
		finicio = dini;
		ffin = dfin;
	});

	$('.Semana').on('keyup', function () {
		if ($(this).val().trim() === '') {
			finicio = r_finicio;
			ffin = r_ffin;
		}
	});

	$('body').on('click', '#resultados tr', function () {
		if ($(this).data('tipo') == 'X') {
			window.location.href = "registro-asistencia-encuesta?as=" + $(this).prop('id');
		}
	});


});

function cambiar_pag(pag_id) {
	$('#lista_loader').show();
	$('#resultados').empty();
	data.push({
		name: 'asistencia[accion]',
		value: 'listar'
	}, {
		name: 'asistencia[pagina]',
		value: pag_id
	})
	$.post('libs/acc_asistencia', data, function (data) {
		$('#lista_loader').hide();
		$('#resultados').html(data.registros);
		$('#paginacion').html(data.paginacion);
	}, "json");
}
