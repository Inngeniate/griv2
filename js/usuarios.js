$(document).ready(function () {

	tempnom = '';
	cambiar_pag('0');

	$('#navegador li').on('click', function () {
		tab = $('a', this);
		$('#navegador a').removeClass('activo');
		tab.addClass('activo');

		if (tab.prop('id') == 'listar') {
			cambiar_pag('0');
			$('.listado-usuarios').fadeIn();
			$('.crear-usuarios').fadeOut(0);
			$('.listado-eliminados').fadeOut(0);
		} else if (tab.prop('id') == 'crear') {
			$('.crear-usuarios').fadeIn();
			$('.listado-usuarios').fadeOut(0);
			$('.listado-eliminados').fadeOut(0);
		} else if (tab.prop('id') == 'eliminados') {
			eliminados('0');
			$('.listado-eliminados').fadeIn();
			$('.listado-usuarios').fadeOut(0);
			$('.crear-usuarios').fadeOut(0);
		}
	});

	//listar

	$('#buscar').on('submit', function (e) {
		e.preventDefault();
		tempnom = $('[name=nom]').val();
		data = $(this).serializeArray();
		data.push({
			'name': 'usuario[opc]',
			'value': 'listar'
		});
		data.push({
			'name': 'pagina',
			'value': '0'
		});
		$('#lista_loader').show();
		$('#resultados').empty();
		$.post('libs/acc_usuarios', data, function (data) {
			$('#lista_loader').hide();
			$('#resultados').html(data.registros);
			$('#paginacion_usuarios').html(data.paginacion);
		}, "json");
	});

	$('#buscar input').on('keyup', function () {
		innom = $('.nom').val();
		if (!innom) {
			tempnom = '';
			cambiar_pag('0');
		}
	});

	function cambiar_pag(pag_id) {
		$('#lista_loader').show();
		$('#resultados').empty();
		data = 'pagina=' + pag_id + '&usuario[opc]=listar&usuario[nom]=' + tempnom;
		$.post('libs/acc_usuarios', data, function (data) {
			$('#lista_loader').hide();
			$('#resultados').html(data.registros);
			$('#paginacion').html(data.paginacion);
		}, "json");
	}


	///crear

	$('#nuevo-usuario').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'usuario[opc]',
			'value': 'nuevo_usuario'
		});

		$.post('libs/acc_usuarios', data, function (data) {
			if (data.status == 'Correcto') {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 2000);
				$('input').not('input[type=submit]').val('');
			} else {
				if (data.status == 'Error1') {
					msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.';
				}
				if (data.status == 'Error2') {
					msj = 'El correo ya se encuentra registrado';
				}
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, 'json');
	});

	$('body').on('click', '.btn-eliminar', function () {
		registro = $(this);
		modal({
			type: 'confirm',
			title: 'Eliminar Usuario',
			text: 'Desea eliminar el usuario ?',
			size: 'small',
			callback: function (result) {
				if (result === true) {
					id = registro.closest('tr').prop('id');
					registro.closest('tr').fadeOut(500, function () {
						registro.remove();
					});
					$.post('libs/acc_usuarios', {
						'usuario[opc]': 'eliminar_usuario',
						'usuario[idusuario]': id
					});
				}
			},
			closeClick: false,
			theme: 'xenon',
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	})

	function eliminados(pag_id) {
		$('.lista_loader').show();
		$('#resultados_eliminados').empty();
		data = 'pagina=' + pag_id + '&usuario[opc]=listar_eliminados&usuario[nom]=' + tempnom;
		$.post('libs/acc_usuarios', data, function (data) {
			$('.lista_loader').hide();
			$('#resultados_eliminados').html(data.registros);
			$('#paginacion_eliminados').html(data.paginacion);
		}, "json");
	}
});
