$(document).ready(function () {
	sp_signature = '';
	tempnom = '';
	cambiar_pag('0');

	$('#navegador li').on('click', function () {
		tab = $('a', this).prop('id');
		if (tab == 'listar') {
			cambiar_pag('0');
			$('#crear').removeClass('activo');
			$('#listar').addClass('activo');
			$('.listado-empleados').fadeIn();
			$('.crear-empleados').fadeOut(0);
		} else if (tab == 'crear') {
			$('#listar').removeClass('activo');
			$('#crear').addClass('activo');
			$('.crear-empleados').fadeIn();
			$('.listado-empleados').fadeOut(0);
		}
	});

	//listar

	$('#buscar').on('submit', function (e) {
		e.preventDefault();
		tempnom = $('[name=nom]').val();
		data = $(this).serializeArray();
		data.push({
			'name': 'accion',
			'value': 'listar'
		});
		data.push({
			'name': 'empleado[pagina]',
			'value': '0'
		});
		$('#lista_loader').show();
		$('#resultados').empty();
		$.post('libs/acc_empleados', data, function (data) {
			$('#lista_loader').hide();
			$('#resultados').html(data.registros);
			$('#paginacion').html(data.paginacion);
		}, "json");
	});

	$('#buscar input').on('keyup', function () {
		innom = $('[name=nom]').val();
		if (!innom) {
			tempnom = '';
			cambiar_pag('0');
		}
	});

	///crear

	$('body').on('click', '#firma', function () {
		modal({
			type: 'inverted',
			title: 'Firma',
			text: '<canvas id="can"  style="border:1px solid #ccc;"></canvas>',
			size: 'normal',
			buttons: [{
				text: 'Limpiar',
				val: 'clear',
				eKey: true,
				addClass: 'btn-light',
				onClick: function (dialog) {
					var m = confirm("Desea borrar el contenido ?");
					if (m) {
						ctx.clearRect(0, 0, w, h);
					}
				}
			}, {
				text: 'Guardar',
				val: 'ok',
				eKey: true,
				addClass: 'btn-light-green',
				onClick: function (dialog) {
					var dataURL = canvas.toDataURL();
					sp_signature = canvas.toDataURL();
					$('#canvasimg').attr('src', dataURL);
					$('#canvasimg').css('display', 'block');
					$('.borrar-firma').remove();
					$('#canvasimg').after('<a href="javascript://" class="Btn-gris-normal borrar-firma"><i class="icon-bin"> </i>Borrar Firma</a>');
					return true;
				}
			}, {
				text: 'Cancelar',
				val: 'cancel',
				eKey: true,
				addClass: 'btn-light-red',
				onClick: function (dialog) {
					return true;
				}
			}],
			onShow: function (r) {
				canvas_init();
			},
			closeClick: false,
			animate: true,
		});
	});

	var canvas, ctx, flag = false,
		prevX = 0,
		currX = 0,
		prevY = 0,
		currY = 0,
		dot_flag = false;

	var x = "black",
		y = 2;

	function canvas_init() {
		canvas = document.getElementById('can');
		ctx = canvas.getContext("2d");
		canvas.width = $('.modal-text').width() - 5;
		canvas.height = $('.modal-text').height();
		w = canvas.width;
		h = canvas.height;

		$('#can').on('mousemove', function (e) {
			findxy('move', e);
		});
		$('#can').on('mousedown', function (e) {
			findxy('down', e);
		});
		$('#can').on('mouseup', function (e) {
			findxy('up', e);
		});
		$('#can').on('mouseout', function (e) {
			findxy('out', e);
		});

		can.addEventListener("touchstart", touchDown, false);
		can.addEventListener("touchmove", touchXY, true);
		can.addEventListener("touchend", touchUp, false);

		document.body.addEventListener("touchcancel", touchUp, false);

	}

	function touchDown(e) {
		mouseIsDown = 1;
		prevX = currX;
		prevY = currY;
		rect = canvas.getBoundingClientRect();
		currX = e.targetTouches[0].clientX - rect.left;
		currY = e.targetTouches[0].clientY - rect.top;
		touchXY();
	}

	function touchUp() {
		mouseIsDown = 0;
		draw();
	}

	function touchXY(e) {
		if (!e) {
			e = event;
		}
		e.preventDefault();
		if (mouseIsDown) {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.targetTouches[0].clientX - rect.left;
			currY = e.targetTouches[0].clientY - rect.top;
			draw();
		}
	}

	function findxy(res, e) {
		if (res == 'down') {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.clientX - rect.left;
			currY = e.clientY - rect.top;
			flag = true;
			dot_flag = true;
			if (dot_flag) {
				ctx.beginPath();
				ctx.fillStyle = x;
				ctx.fillRect(currX, currY, 2, 2);
				ctx.closePath();
				dot_flag = false;
			}
		}
		if (res == 'up' || res == "out") {
			flag = false;
		}
		if (res == 'move') {
			if (flag) {
				prevX = currX;
				prevY = currY;
				rect = canvas.getBoundingClientRect();
				currX = e.clientX - rect.left;
				currY = e.clientY - rect.top;
				draw();
			}
		}
	}

	function draw() {
		ctx.beginPath();
		ctx.moveTo(prevX, prevY);
		ctx.lineTo(currX, currY);
		ctx.strokeStyle = x;
		ctx.lineWidth = y;
		ctx.stroke();
		ctx.closePath();
	}

	$('body').on('click', '.borrar-firma', function () {
		$('#canvasimg').css('display', 'none').attr('src', '');
		$(this).remove();
		sp_signature = 1;
	});

	$('#nuevo-empleado').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		data = $(this).serializeArray();
		data.push({
			name: 'accion',
			value: 'nuevo_empleado'
		});

		formData = new FormData();

		$.each(data, function (key, input) {
			formData.append(input.name, input.value);
		});

		formData.append('empleado[firma]', sp_signature);

		$.ajax('libs/acc_empleados', {
			method: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
					formData = new FormData();
					sp_signature = 1;
					$('input').not('input[type=submit]').val('');
					$('#canvasimg').css('display', 'none').attr('src', '');
					$('.borrar-firma').remove();
				} else {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + data.msg + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});

	$('body').on('click', '.eliminar', function () {
		registro = $(this);
		modal({
			type: 'confirm',
			title: 'Eliminar Registro',
			text: 'Desea eliminar el registo ?',
			size: 'small',
			callback: function (result) {
				if (result === true) {
					id = registro.closest('tr').prop('id');
					registro.closest('tr').fadeOut(500, function () {
						registro.remove();
					});
					$.post('libs/acc_empleados', {
						accion: 'eliminar_empleado',
						'empleado[idempleado]': id
					});
				}
			},
			closeClick: false,
			theme: 'xenon',
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});
});

function cambiar_pag(pag_id) {
	$('#lista_loader').show();
	$('#resultados').empty();
	$.post('libs/acc_empleados', {
		'accion': 'listar',
		'empleado[pagina]': pag_id,
		'nom': tempnom
	}, function (data) {
		$('#lista_loader').hide();
		$('#resultados').html(data.registros);
		$('#paginacion').html(data.paginacion);
	}, "json");
}
