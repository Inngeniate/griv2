cambiar_pag('0');

function cambiar_pag(pagina) {
	$('#lista_loader').show();
	$('#resultados').empty();
	data = 'pagina=' + pagina + '&repo[accion]=listar';
	$.post('libs/acc_repositorio.php', data, function(data) {
		$('#lista_loader').hide();
		$('#resultados').html(data.registros);
		$('#paginacion').html(data.paginacion);
	}, "json");

}
$(document).ready(function() {

	$('#visor').elfinder({
		url: 'finder/php/connector.minimal.php',
		lang: 'es',
		contextmenu: {
			navbar: ['open', '|', 'download', '|', 'sort', 'info'],
			cwd: ['reload', 'back', '|', '|', 'upload', 'mkdir', '|', 'info'],
			files: ['open', 'quicklook', '|', 'download', 'rename', 'rm','|', 'info']
		},
		uiOptions: {
			toolbar: [
				['back', 'forward'],
				['mkdir', 'upload'],
				['rm'],
				['rename'],
				['reload'],
				['home', 'up'],
				['open', 'download'],
				['info'],
				['quicklook'],
				['search'],
				['sort'],
				['view']
			]
		},
		sort: 'name',
		sortDirect: 'desc',
		dragUploadAllow: 'true',
		commandsOptions: {
			getfile: {
				onlyURL: true,
				multiple: false,
				folders: false
			}
		},
		getFileCallback: function(file) {
			window.open(file, '_blank');
		},
		height: 600
	});

	$('#nuevo-documento').on('submit', function(e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);

		var data = new FormData();
		$.each($('#documento'), function(i, obj) {
			$.each(obj.files, function(j, file) {
				data.append('adjunto', file);
			});
		});

		otradata = $('#nuevo-documento').serializeArray();
		$.each(otradata, function(key, input) {
			data.append(input.name, input.value);
		});

		data.append('repo[accion]', 'documento-ingresar');

		$.ajax('libs/acc_repositorio', {
			method: "POST",
			data: data,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function(data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Documento ingresado</span></div>");
					setTimeout(function() {
						$('#fondo').fadeIn('fast', function() {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function() {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
					$('input').not('input[type=submit]').val('');
					cambiar_pag('0');
				} else {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
					setTimeout(function() {
						$('#fondo').fadeIn('fast', function() {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function() {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});

});
