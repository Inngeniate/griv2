$(document).ready(function() {
    tempid = 0;
    temnom = '';
    cambiar_pag('1');

    $('#nuevo-curso').on('submit', function(e) {
        e.preventDefault();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);
        data = $(this).serializeArray();
        if (tempid == 0) {
            data.push({
                'name': 'accion',
                'value': 'nuevo_curso'
            });
        } else {
            data.push({
                'name': 'accion',
                'value': 'editar_curso'
            }, {
                'name': 'idcurso',
                'value': tempid
            });
        }

        $.post('libs/acc_cursos_trabajos_a', data, function(data) {
            $('#fondo').remove();
            if (data.status == 'Correcto') {
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
                setTimeout(function() {
                    $('#fondo').fadeIn('fast', function() {
                        $('#rp').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function() {
                    $("#rp").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 2000);
                tempid = 0;
                $('input').not('input[type=submit]').val('');
                cambiar_pag('1');
            } else if (data.status == 'error-1') {
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
                setTimeout(function() {
                    $('#fondo').fadeIn('fast', function() {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function() {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            } else if (data.status == 'error-2') {
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>Ya existe un curso con ese nombre</span></div>");
                setTimeout(function() {
                    $('#fondo').fadeIn('fast', function() {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function() {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            }
        }, 'json');
    });

    $('body').on('click', '.editar', function() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);
        tempid = $(this).closest('tr').prop('id');
        temnom = $(this).closest('tr').find('.cr-nombre').html();
        $('#nombre').val(temnom);
        $('#fondo').remove();
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#nuevo-curso").offset().top - 200
        }, 100);
    });

});

function cambiar_pag(pag_id) {
    $('#lista_loader').show();
    $('#resultados').empty();
    data = 'pagina=' + pag_id + '&accion=listar';
    $.post('libs/acc_cursos_trabajos_a', data, function(data) {
        $('#lista_loader').hide();
        $('#resultados').html(data.registros);
        $('#paginacion').html(data.paginacion);
    }, "json");
}