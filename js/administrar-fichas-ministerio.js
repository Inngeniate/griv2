$(document).ready(function () {
	tempnom = '';
	cambiar_pag('0');

	$('#navegador li').on('click', function () {
		tab = $('a', this).prop('id');
		if (tab == 'listar') {
			cambiar_pag('0');
			$('#crear').removeClass('activo');
			$('#listar').addClass('activo');
			$('.listado-usuarios').fadeIn();
			$('.crear-usuarios').fadeOut(0);
		} else if (tab == 'crear') {
			$('#listar').removeClass('activo');
			$('#crear').addClass('activo');
			$('.crear-usuarios').fadeIn();
			$('.listado-usuarios').fadeOut(0);
		}
	});

	//listar

	$('#buscar').on('submit', function (e) {
		e.preventDefault();
		tempnom = $('[name=nom]').val();
		cambiar_pag('0');
	});

	$('#buscar input').on('keyup', function () {
		innom = $('[name=nom]').val();
		if (!innom) {
			tempnom = '';
			cambiar_pag('0');
		}
	});



	$('.Sel-competencia').on('change', function () {
		competencia = $('option:selected', this).attr('data-alias');

		$('.Sel-entrenador').val('');

		$('.Sel-entrenador option:not(:first)').hide();

		$.each($('option', '.Sel-entrenador'), function () {
			if ($(this).attr('data-tipo') == competencia) {
				$(this).show();
			}
		});

		$('.Sel-curso').val('');

		$('.Sel-curso option:not(:first)').hide();

		$.each($('option', '.Sel-curso'), function () {
			if ($(this).attr('data-competencia') == competencia) {
				$(this).show();
			}
		});

		$('.Sel-entrenador').attr('required', true);
		$('.Sel-todos').remove();
		$('.Fecha-inicio').attr({
			'required': true,
			'disabled': false
		});
		$('.Fecha-fin').attr({
			'required': true,
			'disabled': false
		});
		$('.Cantidad').attr({
			'required': true,
			'disabled': false
		});

		if ($(this).val() == 1) {
			$('.Sel-entrenador').attr('required', false);
			$('.Sel-curso').append('<option selected class="Sel-todos" value="00">Todos</option>');
			$('.Fecha-inicio').attr({
				'required': false,
				'disabled': true
			});
			$('.Fecha-fin').attr({
				'required': false,
				'disabled': true
			});
			$('.Cantidad').attr({
				'required': false,
				'disabled': true
			});
		}

	});

	$('#registro').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'registro[accion]',
			'value': 'nueva_ficha'
		});

		formData = new FormData();

		$.each(data, function (key, input) {
			formData.append(input.name, input.value);
		});


		$.ajax('libs/acc_fichas_ministerio', {
			method: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
					formData = new FormData();
					$('select').val('');
					$('input').not('input[type=submit]').val('');
					$('.Sel-entrenador').attr('required', true);
					$('.Sel-todos').remove();
					$('.Fecha-inicio').attr({
						'required': true,
						'disabled': false
					});
					$('.Fecha-fin').attr({
						'required': true,
						'disabled': false
					});
					$('.Cantidad').attr({
						'required': true,
						'disabled': false
					});
				} else {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + data.msg + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});

	$('body').on('keydown', '.ntext', function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$('body').on('keydown', '.nnumb', function (e) {
		if (e.shiftKey || e.ctrlKey || e.altKey) {
			e.preventDefault();
		} else {
			var key = e.keyCode;
			if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
				e.preventDefault();
			}
		}
	});

	$('body').on('click', '.eliminar', function () {
		registro = $(this);
		modal({
			type: 'confirm',
			title: 'Eliminar Ficha',
			text: 'Desea eliminar la ficha seleccionada?',
			size: 'small',
			callback: function (result) {
				if (result === true) {
					id = registro.closest('tr').prop('id');

					$.post('libs/acc_fichas_ministerio', {
						'registro[accion]': 'eliminar_ficha',
						'registro[Idficha]': id,
					}, function (data) {
						if (data.status == true) {
							registro.closest('tr').fadeOut(500, function () {
								registro.remove();
							});
						} else {
							$('#fondo').remove();
							$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
							$('#fondo').append("<div class='rp' id='rp2'><span>" + data.msg + "</span></div>");
							setTimeout(function () {
								$('#fondo').fadeIn('fast', function () {
									$('#rp2').animate({
										'top': '50%'
									}, 50).fadeIn();
								});
							}, 400);
							setTimeout(function () {
								$("#rp2").fadeOut();
								$('#fondo').fadeOut('fast');
							}, 3000);
						}
					}
						, 'json');
				}
			},
			closeClick: false,
			theme: 'xenon',
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

});


function cambiar_pag(pag_id) {
	$('#lista_loader').show();
	$('#resultados').empty();
	$.post('libs/acc_fichas_ministerio', {
		'registro[accion]': 'listar',
		'registro[pagina]': pag_id,
		'registro[nom]': tempnom
	}, function (data) {
		$('#lista_loader').hide();
		$('#resultados').html(data.registros);
		$('#paginacion').html(data.paginacion);
	}, "json");
}
