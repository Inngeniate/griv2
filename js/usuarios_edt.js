$(document).ready(function() {

	$('#editar-usuario').on('submit', function(e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'usuario[opc]',
			'value': 'editar_usuario'
		});

		$.post('libs/acc_usuarios', data, function(data) {
			if (data.status == 'Correcto') {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp'><span>Usuario editado</span></div>");
				setTimeout(function() {
					$('#fondo').fadeIn('fast', function() {
						$('#rp').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function() {
					$("#rp").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 2000);
			} else {
				if (data.status == 'Error1') {
					msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.';
				}
				if (data.status == 'Error2') {
					msj = 'El correo ya se encuentra registrado';
				}
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
				setTimeout(function() {
					$('#fondo').fadeIn('fast', function() {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function() {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, 'json');
	});
});
