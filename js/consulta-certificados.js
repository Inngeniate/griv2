$(document).ready(function () {
	$('body').on('change', '#tipo-certificado', function () {
		$('#v-certificados div').hide().find('input').attr('required', false);
		$('#v-certificados input').not('[type=submit], .Consulta-telefono').val('');

		switch ($(this).val()) {
			case '1':
				$('.Consulta-identificacion').show().find('input').attr('required', true);
				break;
			case '2':
				$('.Consulta-placa').show().find('input').attr('required', true);
				break;
			case '3':
				$('.Consulta-serial').show().find('input').attr('required', true);
				break;
			case '4':
				$('.Consulta-ccaltura').show().find('input').attr('required', true);
				break;
		}
	});

	$('#v-certificados').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		$('#fondo').remove();
		if ($('.certalturas').val() != '') {
			datosrequeridos = '<div class="Seccion4-form">' +
				'<form id="solicitud-alturas">' +
				'<label>N° de Identificación</label>' +
				'<input type="text" placeholder="Número" name="consulta[identificacionaltuas]" value="' + $('.certalturas').val() + '" required>' +
				'<label>Celular</label>' +
				'<input type="text" placeholder="Número" name="consulta[celular]" required>' +
				'<label>Correo Electrónico</label>' +
				'<input type="email" placeholder="Correo Electrónico" name="consulta[correo]" required>' +
				'<input type="hidden" name="consulta[tcertificado]" value="alturas">' +
				'<input type="submit" class="Btn-rojo" value="Consultar" name="">' +
				'</form>' +
				'</div>';
			modal({
				type: 'primary',
				title: 'Solicitud consulta certificado de alturas',
				text: datosrequeridos,
				size: 'normal',
				center: true,
				autoClose: false,
				closeClick: false,
				buttons: [{
					text: 'Solicitar',
					val: 'ok',
					eKey: true,
					addClass: 'btn-none',
					onClick: function (dialog) {
						console.log(dialog);
						return true;
					}
				},],
			});
		} else {
			$.post('libs/consultas.php', data, function (data) {
				if (data.status == 'Correcto') {
					$('#fondo').remove();
					modal({
						type: 'primary',
						title: 'Resultado de la consulta',
						text: data.certificados,
						size: 'normal',
						center: true,
						autoClose: false,
						closeClick: false,
					});
				} else if (data.status == 'Error') {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>No se han encontrado resultados.</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}, "json");
		}
	});

	$('body').on('submit', '#solicitud-alturas', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		$.post('libs/consultas.php', data, function (data) {
			if (data.status == 'Correcto') {
				$('#fondo').remove();
				$('.modal-close-btn').click();
				$('input').not('input[type=submit]').val('');
				if (data.alturas == true) {
					datosrequeridos = '<div class="Seccion4-form" style="text-align:center">' +
						'<p><a href="consulta-actas?registro=' + data.reg + '" target="_blank" class="Btn-rojo">Consultar Acta</a></p><br>' +
						'<p><a href="consulta-carnets?registro=' + data.reg + '" target="_blank" class="Btn-rojo">Consultar Carnet</a></p>' +
						'</div>';
					modal({
						type: 'primary',
						title: 'Consultar',
						text: datosrequeridos,
						size: 'normal',
						center: true,
						autoClose: false,
						closeClick: false,
						buttons: [{
							text: 'Solicitar',
							val: 'ok',
							eKey: true,
							addClass: 'btn-none',
							onClick: function (dialog) {
								console.log(dialog);
								return true;
							}
						},],
					});
				} else {
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Solicitud realizada</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
				}
			} else if (data.status == 'Error') {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>Error! No se pudo enviar la solicitud.</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, "json");
	});

});
