$(document).ready(function(){

	$("#contacto").on('submit',function(e){
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">'+
					        '<div class="loader-inner ball-beat">'+
					         '<div></div>'+
					          '<div></div>'+
					          '<div></div>'+
					       '</div>'+
					      '</div>');
    	setTimeout(function() {
        	$('#fondo').fadeIn('fast');
        }, 400);
		data = $(this).serializeArray();
        $.post('libs/contactar.php',data).done(function(data){
        	if(data == 'Correcto') {
        		$('#fondo').remove();
        		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp'><span>Su mensaje ha sido enviado</span></div>");
				setTimeout(function() {
		        	$('#fondo').fadeIn('fast',function(){
		            	$('#rp').animate({'top':'50%'},50).fadeIn();
		         	});
		        }, 400);
		        setTimeout(function() {
		            $("#rp").fadeOut();
		            $('#fondo').fadeOut('fast');
		        }, 3000);
		         $('input,textarea').not('input[type=submit]').val('');
		    }
		    else if(data == 'Error'){
		    	$('#fondo').remove();
		    	$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		    	$('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
				setTimeout(function() {
		        	$('#fondo').fadeIn('fast',function(){
		           		$('#rp2').animate({'top':'50%'},50).fadeIn();
		         	});
		        }, 400);
		        setTimeout(function() {
		            $("#rp2").fadeOut();
		            $('#fondo').fadeOut('fast');
		        }, 3000);
		    }
        });
	});
});