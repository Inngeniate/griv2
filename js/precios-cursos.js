$(document).ready(function() {

	tempnom = '';
	cambiar_pag('0');

	$('#navegador li').on('click', function() {
		tab = $('a', this).prop('id');
		if (tab == 'listar') {
			cambiar_pag('0');
			$('#crear').removeClass('activo');
			$('#listar').addClass('activo');
			$('.listado-precios').fadeIn();
			$('.crear-curso').fadeOut(0);
		} else if (tab == 'crear') {
			$('#listar').removeClass('activo');
			$('#crear').addClass('activo');
			$('.crear-curso').fadeIn();
			$('.listado-precios').fadeOut(0);
		}
	});

	//listar

	$('#buscar').on('submit', function(e) {
		e.preventDefault();
		tempnom = $('[name=nom]').val();
		data = $(this).serializeArray();
		data.push({
			'name': 'curso[opc]',
			'value': 'listar'
		});
		data.push({
			'name': 'pagina',
			'value': '0'
		});
		$('#lista_loader').show();
		$('#resultados').empty();
		$.post('libs/acc_precios', data, function(data) {
			$('#lista_loader').hide();
			$('#resultados').html(data.registros);
			$('#paginacion').html(data.paginacion);
		}, "json");
	});

	$('#buscar input').on('keyup', function() {
		innom = $('.nom').val();
		if (!innom) {
			tempnom = '';
			cambiar_pag('0');
		}
	});

	function cambiar_pag(pag_id) {
		$('#lista_loader').show();
		$('#resultados').empty();
		data = 'pagina=' + pag_id + '&curso[opc]=listar&curso[nom]=' + tempnom;
		$.post('libs/acc_precios', data, function(data) {
			$('#lista_loader').hide();
			$('#resultados').html(data.registros);
			$('#paginacion').html(data.paginacion);
		}, "json");
	}


	///crear

	$('#nuevo-curso').on('submit', function(e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'curso[opc]',
			'value': 'nuevo_curso'
		});

		$.post('libs/acc_precios', data, function(data) {
			if (data.status == true) {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
				setTimeout(function() {
					$('#fondo').fadeIn('fast', function() {
						$('#rp').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function() {
					$("#rp").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 2000);
				$('input').not('input[type=submit]').val('');
			} else {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>" + data.msj + "</span></div>");
				setTimeout(function() {
					$('#fondo').fadeIn('fast', function() {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function() {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, 'json');
	});

	$('body').on('click', '.eliminar', function () {
        registro = $(this);
        modal({
            type: 'confirm',
            title: 'Eliminar Registro',
            text: 'Desea eliminar el registro ?',
            size: 'small',
            callback: function (result) {
                if (result === true) {
                    id = registro.closest('tr').prop('id');
                    registro.closest('tr').fadeOut(500, function () {
                        registro.remove();
                    });
                    $.post('libs/acc_precios', {
                        'curso[opc]': 'eliminar',
                        'curso[idCurso]': id
                    });
                }
            },
            closeClick: false,
            theme: 'xenon',
            animate: true,
            buttonText: {
                yes: 'Confirmar',
                cancel: 'Cancelar'
            }
        });
    });
});
