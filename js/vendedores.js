$(document).ready(function () {

	tempnom = '';
	cambiar_pag('0');

	$('#navegador li').on('click', function () {
		tab = $('a', this).prop('id');
		if (tab == 'listar') {
			cambiar_pag('0');
			$('#crear').removeClass('activo');
			$('#listar').addClass('activo');
			$('.listado-vendedores').fadeIn();
			$('.crear-vendedores').fadeOut(0);
		} else if (tab == 'crear') {
			$('#listar').removeClass('activo');
			$('#crear').addClass('activo');
			$('.crear-vendedores').fadeIn();
			$('.listado-vendedores').fadeOut(0);
		}
	});

	//listar

	$('#buscar').on('submit', function (e) {
		e.preventDefault();
		tempnom = $('[name=nom]').val();
		data = $(this).serializeArray();
		data.push({
			'name': 'accion',
			'value': 'listar'
		});
		data.push({
			'name': 'vendedor[pagina]',
			'value': '0'
		});
		$('#lista_loader').show();
		$('#resultados').empty();
		$.post('libs/acc_vendedores', data, function (data) {
			$('#lista_loader').hide();
			$('#resultados').html(data.registros);
			$('#paginacion').html(data.paginacion);
		}, "json");
	});

	$('#buscar input').on('keyup', function () {
		innom = $('[name=nom]').val();
		if (!innom) {
			tempnom = '';
			cambiar_pag('0');
		}
	});

	///crear

	$('#agr_curso').on('click', function () {
		$('.cursos_adicionales').append('<div class="temp" > <hr> ' +
			'<a class="quitar">Eliminar</a>' +
			'<div class="Registro-der">' +
			'<label>Competencia *</label>' +
			'<select name="vendedor[competencia][]" class="competencia competencia_ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'<label>Precio *</label>' +
			'<input type="text" placeholder="Precio" name="vendedor[precio][]" required>' +
			'</div>' +
			'<div class="Registro-der">' +
			'<label>Curso *</label>' +
			'<select name="vendedor[curso][]" class="Sel-cursos cursos_ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'</div></div>');

		$.each($('.ls_cursos option'), function () {
			$(this).clone().attr('selected', false).appendTo($('.cursos_ad'));
		});

		$.each($('.ls_competencias option'), function () {
			$(this).clone().attr('selected', false).appendTo($('.competencia_ad'));
		});

		$('.cursos_ad').removeClass('cursos_ad');
		$('.competencia_ad').removeClass('competencia_ad');
	});

	$('body').on('change', '.competencia', function () {
		sel_cursos = $(this).closest('.temp').find('.Sel-cursos');
		sel_cursos.val('');
		$('option', sel_cursos).not(':first').hide();
		competencia = $(this).val();

		$.each($('option', sel_cursos), function () {
			if ($(this).attr('data-competencia') == competencia) {
				$(this).show();
			}
		});
	});

	$('body').on('click', '.quitar', function () {
		$(this).parent().remove();
	});

	$('#nuevo-vendedor').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		var data = new FormData();
		$.each($('#firma'), function (i, obj) {
			$.each(obj.files, function (j, file) {
				data.append('firma', file);
			});
		});

		data.append('accion', 'nuevo_vendedor');

		otradata = $(this).serializeArray();

		$.each(otradata, function (key, input) {
			data.append(input.name, input.value);
		});

		$.ajax({
			url: 'libs/acc_vendedores',
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
					$('input').not('input[type=submit]').val('');
					$('select').val('');
					$('.cursos_adicionales').empty();
				} else {
					if (data.status == false) {
						msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.';
					}
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});

	$('body').on('click', '.eliminar', function () {
		registro = $(this);
		modal({
			type: 'confirm',
			title: 'Eliminar Registro',
			text: 'Desea eliminar el registo ?',
			size: 'small',
			callback: function (result) {
				if (result === true) {
					id = registro.closest('tr').prop('id');
					registro.closest('tr').fadeOut(500, function () {
						registro.remove();
					});
					$.post('libs/acc_vendedores', {
						accion: 'eliminar_vendedor',
						'vendedor[idvendedor]': id
					});
				}
			},
			closeClick: false,
			theme: 'xenon',
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});
});

function cambiar_pag(pag_id) {
	$('#lista_loader').show();
	$('#resultados').empty();
	$.post('libs/acc_vendedores', {
		'accion': 'listar',
		'vendedor[pagina]': pag_id,
		'nom': tempnom
	}, function (data) {
		$('#lista_loader').hide();
		$('#resultados').html(data.registros);
		$('#paginacion').html(data.paginacion);
	}, "json");
}
