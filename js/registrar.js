$(document).ready(function () {

	codigo_competencia = '';
	codigo_curso = '';
	codigo_entrenador = '';
	codigo_inicio = '';
	codigo_fin = '';
	tipo_competencia = '';

	$('.Sel-codigo').on('change', function () {
		codigo_competencia = $('option:selected', this).attr('data-competencia').trim();
		codigo_curso = $('option:selected', this).attr('data-curso');
		codigo_entrenador = $('option:selected', this).attr('data-entrenador');
		codigo_inicio = $('option:selected', this).attr('data-inicio');
		codigo_fin = $('option:selected', this).attr('data-fin');

		$('.Sel-vendedor').val('').change();
		$('.ls_cursos option').not(':first').remove();

		$('.f_inicio').val('').attr('readonly', false);
		$('.f_fin').val('').attr('readonly', false);

		setTimeout(function () {
			$('.capacitacion').val(codigo_competencia).change();
			$('.h-capacitacion').val(codigo_competencia);
			$('.h-entrenadores').val(codigo_entrenador);

			if (codigo_competencia == 'ALTURAS') {
				$('.f_inicio').val(codigo_inicio).attr('readonly', true);
				$('.f_fin').val(codigo_fin).attr('readonly', true);
			}

			if (codigo_competencia == 'ALTURAS' || codigo_competencia == 'ESPACIOS CONFINADOS' || codigo_competencia == 'ALTURAS RES. 4272' || codigo_competencia == 'RESCATE INDUSTRIAL') {
				$('.Competencia-oculto').show();

				$('.f_inicio').val(codigo_inicio).attr('readonly', true);
				$('.f_fin').val(codigo_fin).attr('readonly', true);

				$('.Carga-foto').hide();
				$('.Licencia-oculto').hide().find('[type=text]').attr('required', false);
			} else {
				$('.Competencia-oculto').hide().find('input, select').val('');
				$('.Carga-foto').show();
				$('.Licencia-oculto').show().find('[type=text]').attr('required', true);
			}
		}, 100);

		$('.CBasico-ciudad').empty();

		if (codigo_competencia == 'CURSO BASICO') {
			$('.CBasico-ciudad').append('<label>Ciudad *</label>' +
				'<select name="ciudad" class="Sel-ciudad" required>' +
				'<option value="cucuta">Cúcuta</option>' +
				'<option value="villavicencio" selected>Villavicencio</option>' +
				'</select>');
		}
	});

	$('.capacitacion').on('change', function () {
		$('.Fecha-finaliza').show();
		$('.Ocultar-content').show();
		$('#ver-entrenadores').hide();
		$('option', '.entrenadores').not(':first').hide();
		$('.Competencia-alturas4272').hide();

		tipo_competencia = $('option:selected', this).attr('data-nombre');

		if (tipo_competencia == 'cbasico') {
			$('.Fecha-finaliza').hide();
		}

		if (tipo_competencia == 'alturas' || tipo_competencia == 'cea' || tipo_competencia == 'espaciosconfinados' || tipo_competencia == 'rescateindustrial') {
			$('[data-tipo=' + tipo_competencia + ']', '.entrenadores').show();
			$('.entrenadores').val('');
			$('#ver-entrenadores').show();
			$('.Fecha-finaliza').show();

			if (tipo_competencia == 'cea' || tipo_competencia == 'espaciosconfinados') {
				$('.img-placa').show();
			}

			$('.entrenadores').val(codigo_entrenador);
		}

		if (tipo_competencia == 'alturasres.4272' || tipo_competencia == 'espaciosconfinados' || tipo_competencia == 'rescateindustrial') {
			$('.Ocultar-content').hide();
			$('.Competencia-alturas4272').show();
		}

		if ($('.Sel-vendedor').val() != '') {
			CargaCursos();
		}
	});

	$('.Sel-vendedor').on('change', function () {
		if ($(this).val() != '') {
			if ($('.capacitacion').val() != '') {
				CargaCursos();
			}
		}
	});

	function CargaCursos() {
		idvendedor = $('option:selected', '.Sel-vendedor').prop('id').split('-');
		$('.vendedor_direccion').val('');
		$('.vendedor_telefono').val('');
		$('.vendedor_correo').val('');

		$('.vendedor_direccion').val($('option:selected', '.Sel-vendedor').data('direccion'));
		$('.vendedor_telefono').val($('option:selected', '.Sel-vendedor').data('telefono'));
		$('.vendedor_correo').val($('option:selected', '.Sel-vendedor').data('correo'));

		$('.ls_cursos option').not(':first').remove();

		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		$.post('libs/acc_registros', {
			'accion': 'cargar-cursos',
			'vendedor': idvendedor[1],
			'capacitacion': $('.capacitacion option:selected').attr('data-nombre')
		}, function (data) {
			$('#fondo').remove();
			if (data != '') {
				$('.ls_cursos').append(data);

				setTimeout(function () {
					$('.ls_cursos').val(codigo_curso).change();
				}, 100);
			}
		}, 'json');
	}

	$('body').on('change', '.ls_cursos', function () {
		$('.Sel-horas').val($('option:selected', $('.ls_cursos')).attr('data-horas'));
	});

	var cropper;
	var formData = new FormData();
	w = 113;
	h = 152;

	$('.placa').on('click', function () {
		btn = $(this);
		modal_titulo = $(this).text();
		modal({
			type: 'primary',
			title: modal_titulo,
			text: '<input type="file"><div class="contenedor-imagen"><br><br><img id="imagen-placa" src="images/img_fondo.jpg"></div>',
			size: 'small',
			center: true,
			autoClose: false,
			closeClick: false,
			buttons: [{
				text: '+',
				val: 'zoomin',
				eKey: true,
				addClass: 'btn-circle btn-blue btn-zoom',
				onClick: function () {
					if (cropper)
						cropper.zoom(0.1);
				}
			}, {
				text: '-',
				val: 'zoomout',
				eKey: true,
				addClass: 'btn-circle btn-blue btn-zoom',
				onClick: function () {
					if (cropper)
						cropper.zoom(-0.1);
				}
			}, {
				text: 'Ok',
				val: 'ok',
				eKey: true,
				addClass: 'btn-large',
				onClick: function () {
					cropper.getCroppedCanvas().toBlob(function (blob) {
						url = URL.createObjectURL(blob);
						btn.prev('img').attr('src', url);
						ext = blob.type.split('/');
						formData.delete(btn.prop('id'));
						formData.append(btn.prop('id'), blob, btn.prop('id') + '.' + ext[1]);
					});
					return true;
				}
			},],
		});
	});

	$('body').on('change', 'input[type=file]', function () {
		var files = this.files;
		var file;

		if (files.length > 0) {
			file = files[0];
			if (/^image\/\w+/.test(file.type)) {
				if (this.url) {
					URL.revokeObjectURL(this.url);
				}
				this.url = URL.createObjectURL(file);
				img = $(this).next('div').find('img');
				img.attr({
					src: this.url
				});
				cropper = new Cropper($('#imagen-placa').get(0), {
					dragMode: 'move',
					cropBoxResizable: false,
					built: function () {
						this.cropper.setCropBoxData({
							"left": 100,
							"top": 100,
							"width": w,
							"height": h
						});
					}

				});
			} else {
				modal({
					type: 'error',
					title: 'Formato Incorrecto',
					text: 'Debe seleccionar una imagen válida'
				});
			}
		}
	});

	$('#agr_curso').on('click', function () {
		$('#cursos_adicionales').append('<div class="temp" > <hr> ' +
			'<a class="quitar">Eliminar</a>' +
			'<div class="Registro-der">' +
			'<label>Competencia</label>' +
			'<select name="capacitacion_ad[]" class="capacitacion-adicional capacitacion_ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'<div class="Ocultar-content-ad">' +
			'<label>Vencimiento - Carnet</label>' +
			'<input type="date" placeholder="Vencimiento" name="vencimiento_ad[]">' +
			'<label>Fecha de Vigencia *</label>' +
			'<input type="date" name="f_vigencia_ad[]">' +
			'</div>' +
			/* '<label>Precio *</label>' +
			'<input type="text" placeholder="Precio" name="precio_ad[]" class="Precio-ad" required>' + */
			'</div>' +
			'<div class="Registro-der">' +
			'<label>Certificación a realizar *</label>' +
			'<select name="certificado_ad[]" class="cursos_ad Sel-curso-ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'<label>Fecha de inicio *</label>' +
			'<input type="date" name="f_inicio_ad[]" required>' +
			'<label>Horas *</label>' +
			'<input type="text" placeholder="Horas" name="horas_ad[]" required>' +
			'</div></div>');

		$.each($('.ls_cursos option').not(':first'), function () {
			$(this).clone().attr('selected', false).appendTo($('.cursos_ad'));
		});

		$.each($('.capacitacion option').not(':first'), function () {
			$(this).clone().attr('selected', false).appendTo($('.capacitacion_ad'));
		});

		$('.cursos_ad').removeClass('cursos_ad');
		$('.capacitacion_ad').removeClass('capacitacion_ad');
	});

	$('body').on('change', '.capacitacion-adicional', function () {
		if ($('.Sel-vendedor').val() != '') {
			idvendedor = $('option:selected', '.Sel-vendedor').prop('id').split('-');
			select_curso_ad = $(this).closest('.temp').find('.Sel-curso-ad');
			$('option', select_curso_ad).not(':first').remove();
			// $(this).closest('.temp').find('.Precio').val('');
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="loader-inner ball-beat">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function () {
				$('#fondo').fadeIn('fast');
			}, 400);

			$.post('libs/acc_registros', {
				'accion': 'cargar-cursos',
				'vendedor': idvendedor[1],
				'capacitacion': $('option:selected', this).attr('data-nombre')
			}, function (data) {
				$('#fondo').remove();
				if (data != '') {
					select_curso_ad.append(data);
				}
			}, 'json');

			$('.Ocultar-content-ad').show();

			if ($('option:selected', this).attr('data-nombre') == 'alturas' || $('option:selected', this).attr('data-nombre') == 'espaciosconfinados') {
				$('.Ocultar-content-ad').hide();
			}
		}
	});

	$('body').on('change', '.Sel-curso-ad', function () {
		$(this).closest('.temp').find('.Precio-ad').val($('option:selected', this).data('precio'));
	});

	$('body').on('click', '.quitar', function () {
		$(this).parent().remove();
	});

	$('.Aplica-check').on('click', function () {
		if ($(this).is(':checked')) {
			$(this).next('label').next('.Aplica-disabled').attr('disabled', true).val('');
		} else {
			$(this).next('label').next('.Aplica-disabled').attr('disabled', false).val('');
		}
	});

	$('.nacionalidad').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: 'libs/acc_registros',
				dataType: "json",
				data: {
					'accion': 'Nacionalidades',
					'nom': request.term
				},
				success: function (data) {
					if (!data.length) {
						result = [{
							label: 'No se han encontrado resultados',
							value: response.term
						}];
						response(result);
					} else {
						response($.map(data, function (item) {
							code = item.split("|");
							return {
								value: code[2],
								data: item
							};
						}));
					}
				}
			});
		},
		autoFocus: true,
		minLength: 2,
		select: function (event, ui) {
			if (ui.item.label != 'No se han encontrado resultados') {
				names = ui.item.data.split("|");
				$('.nacionalidad').next('input').val(names[1])
			} else {
				event.preventDefault();
			}
		}
	});

	$('.Ciudades').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: 'libs/acc_registros',
				dataType: "json",
				data: {
					'accion': 'Ciudades',
					'nom': request.term
				},
				success: function (data) {
					if (!data.length) {
						result = [{
							label: 'No se han encontrado resultados',
							value: response.term
						}];
						response(result);
					} else {
						response($.map(data, function (item) {
							code = item.split("|");
							return {
								value: code[1],
								data: item
							};
						}));
					}
				}
			});
		},
		autoFocus: true,
		minLength: 3,
		// select: function (event, ui) {
		// 	if (ui.item.label != 'No se han encontrado resultados') {
		// 		names = ui.item.data.split("|");
		// 		$('.nacionalidad').next('input').val(names[1])
		// 	} else {
		// 		event.preventDefault();
		// 	}
		// }
	});

	moment.locale('es');

	$('.Fecha-nacimiento').on('change', function () {
		isoDate = new Date($(this).val()).toISOString();
		edad = moment().diff(moment(isoDate), 'years', false);
		$('.Edad').val(Math.round(edad));
	});

	// razon social
	$('.cliente-razon').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: 'libs/acc_clientes',
				dataType: "json",
				data: {
					'accion': 'buscar_cliente',
					'cliente[razon]': request.term
				},
				success: function (data) {
					if (!data.length) {
						result = [{
							label: 'No se han encontrado resultados',
							value: response.term
						}];
						$('.cliente-id').val('');
						$('.cliente-nit').val('');
						$('.cliente-representante').val('');
						$('.cliente-arl').val('');
						$('.Sector').val('');
						response(result);
					} else {
						response($.map(data, function (item) {
							code = item.split("|");
							return {
								value: code[1],
								data: item
							};
						}));
					}
				}
			});
		},
		autoFocus: true,
		minLength: 2,
		select: function (event, ui) {
			$('.cliente-id').val('');
			$('.cliente-nit').val('');
			$('.cliente-representante').val('');
			$('.cliente-arl').val('');
			$('.cliente-empresa').val('');
			$('.Sector').val('');

			if (ui.item.label != 'No se han encontrado resultados') {
				names = ui.item.data.split("|");
				$('.cliente-id').val(names[0]);
				$('.cliente-nit').val(names[2]);
				$('.cliente-representante').val(names[3]);
				$('.cliente-arl').val(names[4]);
				$('.cliente-empresa').val(names[1]);
				$('.Sector').val(names[5]);
			} else {
				event.preventDefault();
				$('.cliente-id').val('');
				$('.cliente-nit').val('');
				$('.cliente-representante').val('');
				$('.cliente-arl').val('');
				$('.cliente-empresa').val('');
				$('.Sector').val('');
			}
		}
	});

	$('.cliente-razon').on('keyup', function () {
		if ($(this).val().trim() == '') {
			$('.cliente-id').val('');
			$('.cliente-nit').val('');
			$('.cliente-representante').val('');
			$('.cliente-arl').val('');
			$('.cliente-empresa').val('');
		}
	});

	$('.No-espacio').on('keyup', function () {
		if ($(this).val().trim() == $(this).val()) {
			$(this).removeClass('Error-espacio');
		}
	})

	$('.Registro-identificacion').on('blur', function () {
		if($('.capacitacion').val() == 'ALTURAS RES. 4272'){
			$.post('libs/acc_registros', {
				'accion': 'cargar-registro',
				'identifica': $(this).val()
			},function (data) {
				if(data.info != ''){
					$('.Nombre-primero').val(data.info.nombre_primero_al);
					$('.Nombre-segundo').val(data.info.nombre_segundo_al);
					$('.Apellidos').val(data.info.apellidos_al);
					$('.Expedicion').val(data.info.documento_de_al);
					$('.Edad').val(data.info.edad_al);
					$('.vendedor_correo').val(data.info.email_al);
					$('.Fecha-nacimiento').val(data.info.fnacimiento_al);
					$('.Pais-nacimiento').val(data.info.nacionalidad_al);
					$('.Nivel-educativo').val(data.info.nv_educativo_al);
					$('.RH').val(data.info.rh_al);
					$('.Sector').val(data.info.sector_al);
					$('.vendedor_telefono').val(data.info.telefono_al);
					$('.Tipo-identificacion').val(data.info.tipo_doc_al);
					$('.Cargo').val(data.info.profesion_al);
					$('.cliente-empresa').val(data.info.empresa_al);
				}
			},'json');
		}
	})

	$('#registro').on('submit', function (e) {
		e.preventDefault();

		var error = false;

		$.each($('.No-espacio'), function () {
			var value = $(this).val().trim();

			if (value !== $(this).val()) {
				$(this).addClass('Error-espacio');
				error = true;
			} else {
				$(this).removeClass('Error-espacio');
			}
		})

		if (error == false) {
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="loader-inner ball-beat">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function () {
				$('#fondo').fadeIn('fast');
			}, 400);
			data = $(this).serializeArray();
			data.push({
				'name': 'accion',
				'value': 'nuevo_reg'
			}, {
				'name': 'capacitacion_alias',
				'value': tipo_competencia
			});

			$.each(data, function (key, input) {
				formData.append(input.name, input.value);
			});

			$.ajax('libs/acc_registros', {
				method: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				dataType: 'json',
				success: function (data) {
					if (data.status == 'Correcto') {
						$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
						setTimeout(function () {
							$('#fondo').fadeIn('fast', function () {
								$('#rp').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function () {
							$("#rp").fadeOut();
							$('#fondo').fadeOut('fast');
						}, 2000);

						reset();
						$('#cursos_adicionales').empty();
						$('input').not('input[type=submit]').val('');
						$('select').val('');
						$('#ver-entrenadores').hide();
						$('.Competencia-oculto').hide().find('input, select').val('');
						setTimeout(function () {
							location.reload();
						}, 2500);
					} else {
						if (data.status == 'Error') {
							msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.'
						}
						if (data.status == 'Error1') {
							msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.'
						}
						if (data.status == 'Error2') {
							msj = 'La fecha de inicio de este certificado ya se encuentra registrada !';
						}

						$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
						setTimeout(function () {
							$('#fondo').fadeIn('fast', function () {
								$('#rp2').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function () {
							$("#rp2").fadeOut();
							$('#fondo').fadeOut('fast');
						}, 3000);
					}
				}
			});
		}else{
			$('html, body').animate({ scrollTop: 0 }, 'fast');
		}
	});

	$('body').on('keydown', '.ntext', function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$('body').on('keydown', '.nnumb', function (e) {
		if (e.shiftKey || e.ctrlKey || e.altKey) {
			e.preventDefault();
		} else {
			var key = e.keyCode;
			if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
				e.preventDefault();
			}
		}
	});

	$('.Sel-vendedor').select2({
		language: "es",
	});

	$('.ls_cursos').select2({
		language: "es",
	});

	function reset() {
		formData = new FormData();
		$.each($('.img-prev'), function () {
			$(this).attr('src', 'images/marca_agua.jpg');
		});
	}

});
