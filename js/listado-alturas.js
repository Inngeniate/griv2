$(document).ready(function () {
    cambiar_pag('0');

    $('#buscar').on('submit', function (e) {
        e.preventDefault();
        cambiar_pag('0');
    });

    $('body').on('click', '#resultados td:not(:last-child)', function () {
        idregistro = $(this).parent().prop('id');
        if (!$(this).hasClass('ver-ficha')) {
            $('<form action="ver-trabajo-altura" method="POST" target="_blank">' +
                '<input type="hidden" name="idregistro" value="' + idregistro + '">' +
                '</form>').appendTo('body').submit().remove();
        }
    });

    $('body').on('click', '.eliminar', function () {
        registro = $(this);
        modal({
            type: 'confirm',
            title: 'Eliminar Ficha',
            text: 'Desea eliminar la ficha ?',
            size: 'small',
            callback: function (result) {
                if (result === true) {
                    id = registro.closest('tr').prop('id');
                    registro.closest('tr').fadeOut(500, function () {
                        registro.remove();
                    });
                    $.post('libs/acc_registros_alturas.php', {
                        'registro[accion]': 'eliminar',
                        'registro[id]': id
                    });
                }
            },
            closeClick: false,
            theme: 'xenon',
            animate: true,
            buttonText: {
                yes: 'Confirmar',
                cancel: 'Cancelar'
            }
        });
    });

});

function cambiar_pag(pag_id) {
    $('#lista_loader').show();
    $('#resultados').empty();
    data = 'registro[pagina]=' + pag_id + '&registro[accion]=listar&registro[nom]=' + $('.nombrereg').val() + '&registro[identificacion]=' + $('.identificareg').val() + '&registro[id]=' + $('.idregistro').val();
    $.post('libs/acc_registros_alturas.php', data, function (data) {
        $('#lista_loader').hide();
        $('#resultados').html(data.registros);
        $('#paginacion').html(data.paginacion);
    }, "json");
}
