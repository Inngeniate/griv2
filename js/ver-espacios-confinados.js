$(document).ready(function () {
	$('#anexar').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		var data = new FormData();
		$.each($('.adjunto'), function (i, obj) {
			nom = $(this).prop('id');
			$.each(obj.files, function (j, file) {
				data.append(nom, file);
			});
		});

		data.append('registro[idregistro]', $('#idregistro').val());
		data.append('registro[accion]', 'anexos');

		$.ajax({
			url: 'libs/acc_registros_espacios_confinados',
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' style='display: none; text-align: center' id='rp'><b>Acción realizada con éxito</b></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '350px'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2500);
					$('input', '#anexar').not('[type=submit]').val('');
					setTimeout(function () {
						location.reload();
					}, 3000);
				} else if (data.status == false) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' style='display: none; text-align: center; background: rgb(238, 92, 92)' id='rp'><span>Error! Tu almacenamiento esta lleno, no podras almacenar archivos.</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '350px'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 6000);
				} else if (data.error != '') {
					error = data.error;
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' style='display: none; text-align: center; background: rgb(238, 92, 92)' id='rp'><span>Error! en los anexos: " + error + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '350px'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
					$('input', '#anexar').not('[type=submit]').val('');
				}
			}
		});
	});

	$('.el-anexo').on('click', function () {
		idanexo = $(this);
		modal({
			type: 'confirm',
			title: 'Eliminar Anexo',
			text: 'Desea eliminar el anexo ?',
			size: 'small',
			callback: function (result) {
				if (result === true) {
					id = idanexo.prop('id');
					idanexo.parent().remove();
					$.post('libs/acc_registros_espacios_confinados', {
						'registro[accion]': 'eliminar-anexo',
						'registro[idanexo]': id
					});
				}
			},
			closeClick: false,
			theme: 'xenon',
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	$('#ver-anexos').on('click', function () {
		$('.anexos').slideToggle();
		$('.flecha').toggleClass('flecha-abajo').toggleClass('flecha-arriba');
	});
});
