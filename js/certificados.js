$(document).ready(function() {

    $('#consulta').on('submit', function(e) {
        e.preventDefault();
        $('#resultados').empty();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);
        data = $(this).serializeArray();
        data.push({
            'name': 'accion',
            'value': 'consulta'
        });
        $.post('libs/acc_registros.php', data, function(data) {
            if (data.status == 'Correcto') {
                $('#fondo').remove();
                $('#resultados').append(data.certificados);
            } else if (data.status == 'Error') {
                $('#fondo').remove();
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>No se han encontrado resultados.</span></div>");
                setTimeout(function() {
                    $('#fondo').fadeIn('fast', function() {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function() {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            }
        }, "json");
    });

    $('body').on('click', '.descarga', function() {
        $('<form action="libs/certificado" method="POST" target="_blank">' +
            '<input type="hidden" name="id" value="' + $(this).parent().prop('id') + '">' +
            '</form>').appendTo('body').submit().remove();
    });

    ///////inspecciones
    $('#consulta-inspec').on('submit', function(e) {
        e.preventDefault();
        $('#resultados-inspec').empty();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);
        data = $(this).serializeArray();
        data.push({
            'name': 'certifica[accion]',
            'value': 'inspecciones'
        });
        $.post('libs/certifica2.php', data, function(data) {
            if (data.status == 'Correcto') {
                $('#fondo').remove();
                $('#resultados-inspec').append(data.certificados);
            } else if (data.status == 'Error') {
                $('#fondo').remove();
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>No se han encontrado resultados.</span></div>");
                setTimeout(function() {
                    $('#fondo').fadeIn('fast', function() {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function() {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            }
        }, "json");
    });

    $('body').on('click', '.descarga-inspec', function() {
        id = $(this).parent().prop('id');
        form = $('<form action="libs/certifica2" method="post" target="_blank">' +
            '<input type="hidden" name="certifica[accion]" value="generar">' +
            '<input type="hidden" name="certifica[certificado]" value="ensayo">' +
            '<input type="hidden" name="certifica[id]" value="' + id + '">' +
            '</form>');
        form.appendTo($('body')).submit().remove();
    });

    ///////seguridad
    $('#consulta-seguridad').on('submit', function(e) {
        e.preventDefault();
        $('#resultados-segur').empty();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);
        data = $(this).serializeArray();
        data.push({
            'name': 'certifica[accion]',
            'value': 'certi-seguridad'
        });
        $.post('libs/certifica2.php', data, function(data) {
            if (data.status == 'Correcto') {
                $('#fondo').remove();
                $('#resultados-segur').append(data.certificados);
            } else if (data.status == 'Error') {
                $('#fondo').remove();
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>No se han encontrado resultados.</span></div>");
                setTimeout(function() {
                    $('#fondo').fadeIn('fast', function() {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function() {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            }
        }, "json");
    });

});