$(document).ready(function() {
    cambiar_pag(1);

    function cambiar_pag(pag_id) {
        $('#lista_loader').show();
        $('#resultados').empty();
        data = 'pagina=' + pag_id + '&registro[accion]=listar';
        $.post('libs/acc_aspirantes.php', data, function(data) {
            $('#lista_loader').hide();
            $('#resultados').html(data.registros);
            $('#paginacion').html(data.paginacion);
        }, "json");
    }

});