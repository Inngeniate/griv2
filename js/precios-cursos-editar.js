$(document).ready(function () {

	$('#editar-curso').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'curso[opc]',
			'value': 'editar_curso'
		});

		$.post('libs/acc_precios', data, function (data) {
			if (data.status == true) {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 2000);
				// setTimeout(function () {
				// 	location.reload();
				// }, 2300);
			} else {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>" + data.msj + "</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, 'json');
	});
});
