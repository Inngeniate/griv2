$(document).ready(function () {

	$('#agr_curso').on('click', function () {
		$('.cursos_adicionales').append('<div class="temp" > <hr> ' +
			'<a class="quitar">Eliminar</a>' +
			'<div class="Registro-der">' +
			'<label>Competencia *</label>' +
			'<select name="vendedor[competencia][]" class="competencia competencia_ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'<label>Precio *</label>' +
			'<input type="text" placeholder="Precio" name="vendedor[precio][]" required>' +
			'</div>' +
			'<div class="Registro-der">' +
			'<label>Curso *</label>' +
			'<select name="vendedor[curso][]" class="Sel-cursos cursos_ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'</div></div>');

		$.each($('.ls_cursos option'), function () {
			$(this).clone().attr('selected', false).appendTo($('.cursos_ad'));
		});

		$.each($('.ls_competencias option'), function () {
			$(this).clone().attr('selected', false).appendTo($('.competencia_ad'));
		});

		$('.cursos_ad').removeClass('cursos_ad');
		$('.competencia_ad').removeClass('competencia_ad');
	});

	$('body').on('change', '.competencia', function () {
		sel_cursos = $(this).closest('.temp').find('.Sel-cursos');
		sel_cursos.val('');
		$('option', sel_cursos).not(':first').hide();
		competencia = $(this).val();

		$.each($('option', sel_cursos), function () {
			if ($(this).attr('data-competencia') == competencia) {
				$(this).show();
			}
		});
	});

	$('body').on('click', '.quitar', function () {
		$(this).parent().remove();
	});

	$('#editar-vendedor').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		var data = new FormData();

		data.append('accion', 'editar_vendedor');

		otradata = $(this).serializeArray();

		$.each(otradata, function (key, input) {
			data.append(input.name, input.value);
		});

		$.ajax({
			url: 'libs/acc_vendedores',
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Registro editado</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
				} else {
					if (data.status == false) {
						msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.';
					}
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});
});
