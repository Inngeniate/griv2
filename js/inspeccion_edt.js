$(document).ready(function() {

    $('#ed_certificado').on('submit', function(e) {
        e.preventDefault();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);

        data = $(this).serializeArray();

        data.push({
            'name': 'certifica[accion]',
            'value': 'editar'
        });
        data.push({
            'name': 'certifica[tipoen]',
            'value': 'inspeccion'
        });

        $.post('libs/certifica2', data, function(data) {
            if (data.status == 'Correcto') {
                form = $('<form action="libs/certifica2" method="post">' +
                    '<input type="hidden" name="certifica[accion]" value="generar">' +
                    '<input type="hidden" name="certifica[certificado]" value="' + data.certificado + '">' +
                    '<input type="hidden" name="certifica[id]" value="' + data.certificadoid + '">' +
                    '</form>');
                form.appendTo($('body')).submit().remove();
            } else {
                $('#fondo').remove();
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
                setTimeout(function() {
                    $('#fondo').fadeIn('fast', function() {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function() {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            }
        }, 'json');
    });

    function reset() {
        $('input', '#ed_certificado').not('[type=submit], [type=hidden], [readonly], [type=radio]').val('');
        $('textarea').val('');
        formData = new FormData();
        $.each($('.img-prev'), function() {
            $(this).removeAttr('src');
        });
        $.each($('.img-prev2'), function() {
            $(this).removeAttr('src');
        });
    }

});