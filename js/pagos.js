$(document).ready(function () {
	var val_plan = 0;

	$('.Sel-curso').on('change', function () {
		$.post('libs/acc_pagos', {
			'curso[opc]': 'valor_curso',
			'curso[curso]': $(this).val(),
		}, function (data) {
			if (data.status == true) {
				val_plan = data.precio;
				$('.Valor-curso').html('$ ' + moneyFormat.to(Number(data.precio)));
			}
		}, 'json');
	});

	$('#registro-pago').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		validar($(this));
		if (completo === true) {
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="loader-inner ball-beat">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function () {
				$('#fondo').fadeIn('fast');
			}, 400);

			data = $(this).serializeArray();
			data.push({
				name: 'curso[opc]',
				value: 'registrar_pago'
			});
			$.post('libs/acc_pagos', data, function (data) {
				$('#fondo').remove();
				if (data.status == true) {
					var handler = ePayco.checkout.configure({
						key: data.infopago.pago_key,
						test: false
					});

					var datapago = {
						//Parametros compra (obligatorio)
						name: data.infopago.curso,
						description: data.infopago.descripcion,
						currency: "cop",
						amount: data.infopago.monto,
						tax_base: "0",
						tax: "0",
						country: "co",
						lang: "es",

						//Onpage="false" - Standard="true"
						external: "false",


						//Atributos opcionales
						extra1: data.infopago.referencia,
						// extra2: "extra2",
						// extra3: "extra3",
						confirmation: "https://admin.gricompany.co/confirmar-pago",
						response: "https://admin.gricompany.co/respuesta-pago",

						// //Atributos cliente
						email_billing: data.infopago.email_billing,
						name_billing: data.infopago.name_billing,
						address_billing: data.infopago.address_billing,
						type_doc_billing: "cc",
						mobilephone_billing: data.infopago.mobilephone_billing,
						number_doc_billing: data.infopago.number_doc_billing
					};
					handler.open(datapago);
				} else {
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + data.msj + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}, 'json');
		} else {
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append("<div class='rp' id='rp2'><span>Verifica la información ingresada.</span></div>");
			setTimeout(function () {
				$('#fondo').fadeIn('fast', function () {
					$('#rp2').animate({
						'top': '50%'
					}, 50).fadeIn();
				});
			}, 400);
			setTimeout(function () {
				$("#rp2").fadeOut();
				$('#fondo').fadeOut('fast');
			}, 3000);
		}
	});

	function validar(f) {
		error = 0;
		$.each($('.required', f), function () {
			if ($(this).val().trim() === '') {
				$(this).css('borderColor', '#ff0000');
				error++;
			}
			if ($(this).prop('type') == 'email') {
				if (!valida_email($(this).val().trim())) {
					$(this).css('borderColor', '#ff0000');
					error++;
				}
			}
			if ($(this).prop('type') == 'checkbox') {
				if (!$(this).is(':checked')) {
					error++;
				}

			}
		});

		if (error > 0) {
			completo = false;
		} else {
			completo = true;
		}
	}

	function valida_email(email) {
		filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test(email))
			return true;
		else
			return false;
	}

	$('.required').on('keyup change', function () {
		if ($(this).val().trim() !== '') {
			$(this).removeAttr('style');
		}
	});

	var moneyFormat = wNumb({
		mark: ',',
		thousand: '.',
		prefix: ''
	});

});
