$(document).ready(function () {
	sp_signature = '';

	$('body').on('click', '#firma', function () {
		modal({
			type: 'inverted',
			title: 'Firma Aprendiz',
			text: '<canvas id="can"  style="border:1px solid #ccc;"></canvas>',
			size: 'normal',
			buttons: [{
				text: 'Limpiar',
				val: 'clear',
				eKey: true,
				addClass: 'btn-light',
				onClick: function (dialog) {
					var m = confirm("Desea borrar el contenido ?");
					if (m) {
						ctx.clearRect(0, 0, w, h);
					}
				}
			}, {
				text: 'Guardar',
				val: 'ok',
				eKey: true,
				addClass: 'btn-light-green',
				onClick: function (dialog) {
					var dataURL = canvas.toDataURL();
					sp_signature = canvas.toDataURL();
					$('#canvasimg').attr('src', dataURL);
					$('#canvasimg').css('display', 'block');
					$('.borrar-firma').remove();
					$('#canvasimg').after('<a href="javascript://" class="Btn-gris-normal borrar-firma"><i class="icon-bin"> </i>Borrar Firma</a>');
					return true;
				}
			}, {
				text: 'Cancelar',
				val: 'cancel',
				eKey: true,
				addClass: 'btn-light-red',
				onClick: function (dialog) {
					return true;
				}
			}],
			onShow: function (r) {
				canvas_init();
			},
			closeClick: false,
			animate: true,
		});
	});

	var canvas, ctx, flag = false,
		prevX = 0,
		currX = 0,
		prevY = 0,
		currY = 0,
		dot_flag = false;

	var x = "black",
		y = 2;

	function canvas_init() {
		canvas = document.getElementById('can');
		ctx = canvas.getContext("2d");
		canvas.width = $('.modal-text').width() - 5;
		canvas.height = $('.modal-text').height();
		w = canvas.width;
		h = canvas.height;

		$('#can').on('mousemove', function (e) {
			findxy('move', e);
		});
		$('#can').on('mousedown', function (e) {
			findxy('down', e);
		});
		$('#can').on('mouseup', function (e) {
			findxy('up', e);
		});
		$('#can').on('mouseout', function (e) {
			findxy('out', e);
		});

		can.addEventListener("touchstart", touchDown, false);
		can.addEventListener("touchmove", touchXY, true);
		can.addEventListener("touchend", touchUp, false);

		document.body.addEventListener("touchcancel", touchUp, false);

	}

	function touchDown(e) {
		mouseIsDown = 1;
		prevX = currX;
		prevY = currY;
		rect = canvas.getBoundingClientRect();
		currX = e.targetTouches[0].clientX - rect.left;
		currY = e.targetTouches[0].clientY - rect.top;
		touchXY();
	}

	function touchUp() {
		mouseIsDown = 0;
		draw();
	}

	function touchXY(e) {
		if (!e) {
			e = event;
		}
		e.preventDefault();
		if (mouseIsDown) {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.targetTouches[0].clientX - rect.left;
			currY = e.targetTouches[0].clientY - rect.top;
			draw();
		}
	}

	function findxy(res, e) {
		if (res == 'down') {
			prevX = currX;
			prevY = currY;
			rect = canvas.getBoundingClientRect();
			currX = e.clientX - rect.left;
			currY = e.clientY - rect.top;
			flag = true;
			dot_flag = true;
			if (dot_flag) {
				ctx.beginPath();
				ctx.fillStyle = x;
				ctx.fillRect(currX, currY, 2, 2);
				ctx.closePath();
				dot_flag = false;
			}
		}
		if (res == 'up' || res == "out") {
			flag = false;
		}
		if (res == 'move') {
			if (flag) {
				prevX = currX;
				prevY = currY;
				rect = canvas.getBoundingClientRect();
				currX = e.clientX - rect.left;
				currY = e.clientY - rect.top;
				draw();
			}
		}
	}

	function draw() {
		ctx.beginPath();
		ctx.moveTo(prevX, prevY);
		ctx.lineTo(currX, currY);
		ctx.strokeStyle = x;
		ctx.lineWidth = y;
		ctx.stroke();
		ctx.closePath();
	}

	$('body').on('click', '.borrar-firma', function () {
		$('#canvasimg').css('display', 'none').attr('src', '');
		$(this).remove();
		sp_signature = 1;
	});

	//// firma adjunta
	$('#firma_adjunta').on('change', function () {
		if ($(this).val() != '') {
			var files = document.getElementById('firma_adjunta').files;
			if (files.length > 0) {
				$.each(files, function (i, obj) {
					if (/^image\/\w+/.test(obj.type)) {
						getBase64(files[0]);
					} else {
						$('#firma_adjunta').val('');
						$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp2'><span>La firma no tiene un formato válido</span></div>");
						setTimeout(function () {
							$('#fondo').fadeIn('fast', function () {
								$('#rp2').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function () {
							$("#rp2").fadeOut();
							$('#fondo').fadeOut('fast');
						}, 3000);
					}
				});
			}
		}
	});

	function getBase64(file) {
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function () {
			sp_signature = reader.result;
			// console.log(reader.result);
		};
		reader.onerror = function (error) {
			sp_signature = '';
			('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append("<div class='rp' id='rp2'><span>" + error + "</span></div>");
			setTimeout(function () {
				$('#fondo').fadeIn('fast', function () {
					$('#rp2').animate({
						'top': '50%'
					}, 50).fadeIn();
				});
			}, 400);
			setTimeout(function () {
				$("#rp2").fadeOut();
				$('#fondo').fadeOut('fast');
			}, 3000);
			// console.log('Error: ', error);
		};
	}

	////

	/* $('#formacion').on('change', function () {
		if ($(this).val() == 'OTRO') {
			$('.formacion-otro').attr('disabled', false).removeClass('disabled');
		} else {
			$('.formacion-otro').attr('disabled', true).val('').addClass('disabled');
		}
	}); */

	$('.competencia').on('change', function () {
		$('option', '.curso').hide();
		competencia = $('option:selected', this).attr('data-nombre');
		$.each($('option', '.curso'), function () {
			if ($(this).attr('data-competencia') == competencia) {
				$(this).show();
			}
		});
	});

	$('body').on('change', '.curso', function () {
		$('.reqcedula').prop('checked', false);
		$('.reqcertificado').prop('checked', false);
		$('.reqvaloracion').prop('checked', false);
		$('.reqsegsocial').prop('checked', false);
		$('.reqcertlaboral').prop('checked', false);
		$('.reqtrabajadorentrante').prop('checked', false);
		$('.reqtituloprofesional').prop('checked', false);
		$('.reqlicencia').prop('checked', false);
		$('.reqvigiaseguridad').prop('checked', false);
		if (competencia == 'espaciosconfinados') {
			switch ($(this).val()) {
				case '35':
					$('.reqcedula').prop('checked', true);
					$('.reqcertlaboral').prop('checked', true);
					$('.reqsegsocial').prop('checked', true);
					break;
				case '142':
					$('.reqcedula').prop('checked', true);
					$('.reqsegsocial').prop('checked', true);
					$('.reqcertlaboral').prop('checked', true);
					$('.reqvaloracion').prop('checked', true);
					$('.reqtrabajadorentrante').prop('checked', true);
					$('.reqvigiaseguridad').prop('checked', true);
					break;
				case '160':
					$('.reqcedula').prop('checked', true);
					$('.reqsegsocial').prop('checked', true);
					$('.reqcertlaboral').prop('checked', true);
					$('.reqvaloracion').prop('checked', true);
					$('.reqtrabajadorentrante').prop('checked', true);
					break;
				case '161':
					$('.reqcedula').prop('checked', true);
					$('.reqtituloprofesional').prop('checked', true);
					$('.reqlicencia').prop('checked', true);
					break;
				case '162':
					$('.reqcedula').prop('checked', true);
					$('.reqsegsocial').prop('checked', true);
					$('.reqcertlaboral').prop('checked', true);
					$('.reqvaloracion').prop('checked', true);
					break;
			}
		}
	});

	$('#formacion').on('change', function () {
	});

	moment.locale('es');

	$('.Fecha-nacimiento').on('change', function () {
		isoDate = new Date($(this).val()).toISOString();
		console.log(isoDate)
		console.log(moment(isoDate))
		console.log(moment())
		edad = moment().diff(moment(isoDate), 'years', false);
		$('.Edad').val(Math.round(edad));
	});

	$('.estado-laboral').on('change', function () {
		if ($(this).val() == 'EMPLEADO') {
			$('.eps').attr('disabled', false).removeClass('disabled');
			$('.arl').attr('disabled', false).removeClass('disabled');
			$('.empresa').attr('disabled', false).removeClass('disabled');
		}

		if ($(this).val() == 'DESEMPLEADO') {
			$('.eps').attr('disabled', false).removeClass('disabled');
			$('.arl').attr('disabled', true).val('').addClass('disabled');
			$('.empresa').attr('disabled', true).val('').addClass('disabled');
		}
	});

	$('#lesiones').on('change', function () {
		if ($(this).val() == 'SI') {
			$('.lesiones-cual').attr('disabled', false).removeClass('disabled');
		} else {
			$('.lesiones-cual').attr('disabled', true).val('').addClass('disabled');
		}
	});

	$('#enfermedad').on('change', function () {
		if ($(this).val() == 'SI') {
			$('.enfermedad-cual').attr('disabled', false).removeClass('disabled');
		} else {
			$('.enfermedad-cual').attr('disabled', true).val('').addClass('disabled');
		}
	});

	$('#sesiones').on('change', function () {
		if ($(this).val() !== '') {
			sesiones = $(this).val();
			tmpses = $('.tmp-sesion').length;
			if (tmpses == 0) {
				for (var i = 1; i <= sesiones; i++) {
					if (i % 2 != 0) {
						$('.tmp-der').append('<label class="s-' + i + '">Sesión ' + i + '</label><input type="date" class="tmp-sesion s-' + i + '" name="registro[sesion][' + i + ']" required>');
					} else {
						$('.tmp-izq').append('<label class="s-' + i + '">Sesión ' + i + '</label><input type="date" class="tmp-sesion s-' + i + '" name="registro[sesion][' + i + ']" required>');
					}
				}
			} else {
				if (sesiones < tmpses) {
					dif = tmpses - sesiones;
					for (var i = 1; i <= dif; i++) {
						$('.s-' + tmpses).remove();
						tmpses = tmpses - 1;
					}
				} else {
					for (var i = (tmpses + 1); i <= sesiones; i++) {
						if (i % 2 != 0) {
							$('.tmp-der').append('<label class="s-' + i + '">Sesión ' + i + '</label><input type="date" class="tmp-sesion s-' + i + '" name="registro[sesion][' + i + ']" required>');
						} else {
							$('.tmp-izq').append('<label class="s-' + i + '">Sesión ' + i + '</label><input type="date" class="tmp-sesion s-' + i + '" name="registro[sesion][' + i + ']" required>');
						}
					}
				}
			}
		} else {
			$('.tmp-der').empty();
			$('.tmp-izq').empty();
		}
	});

	$('body').on('keydown', '.ntxt', function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$('.auto_ciu').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: 'libs/certifica2.php',
				dataType: "json",
				data: {
					term: request.term
				},
				success: function (data) {
					if (!data.length) {
						result = [{
							label: 'No se han encontrado resultados',
							value: response.term
						}];
						response(result);
					} else {
						response($.map(data, function (item) {
							code = item.split("|");
							return {
								value: code[1],
								data: item
							};
						}));
					}
				}
			});
		},
		autoFocus: true,
		minLength: 2,
		select: function (event, ui) {
			if (ui.item.label != 'No se han encontrado resultados') {
				names = ui.item.data.split("|");
			} else {
				event.preventDefault();
			}
		}
	});

	$('.nacionalidad').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: 'libs/acc_registros_alturas',
				dataType: "json",
				data: {
					'registro[accion]': 'Nacionalidades',
					'registro[term]': request.term
				},
				success: function (data) {
					if (!data.length) {
						result = [{
							label: 'No se han encontrado resultados',
							value: response.term
						}];
						response(result);
					} else {
						response($.map(data, function (item) {
							code = item.split("|");
							return {
								value: code[1],
								data: item
							};
						}));
					}
				}
			});
		},
		autoFocus: true,
		minLength: 2,
		select: function (event, ui) {
			if (ui.item.label != 'No se han encontrado resultados') {
				names = ui.item.data.split("|");
			} else {
				event.preventDefault();
			}
		}
	});

	$('#registro').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		data.push({
			'name': 'registro[accion]',
			'value': 'nuevo_reg'
		});

		formData = new FormData();

		$.each(data, function (key, input) {
			formData.append(input.name, input.value);
		});

		formData.append('registro[firma]', sp_signature);

		$.ajax('libs/acc_registros_espacios_confinados', {
			method: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function (data) {
				if (data.status == true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Registro realizado</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
					formData = new FormData();
					sp_signature = 1;
					$('input').not('input[type=submit], [type=checkbox]').val('');
					$('select').val('');
					$('[type=checkbox]').attr('checked', false);
					$('#canvasimg').css('display', 'none').attr('src', '');
					$('.borrar-firma').remove();
					$('<form action="ficha-aprendiz-ec" method="get" target="_blank" style="display:none"><input name="id" value="' + data.registro + '"></form>').appendTo('body').submit().remove();
					$('.tmp-der').empty();
					$('.tmp-izq').empty();
				} else {
					msj = data.msg;
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}
		});
	});

});
