$(document).ready(function() {

    var cropper;
    var formData = new FormData();
    w = 400;
    h = 100;

    $('#navegador li').on('click', function() {
        tab = $('a', this).prop('id');
        if (tab == 'ensayo') {
            $('#reporte').removeClass('activo');
            $('#aforo').removeClass('activo');
            $('#seguridad').removeClass('activo');
            $('#inspeccion').removeClass('activo');
            $('#ensayo').addClass('activo');
            $('.ensayo').fadeIn();
            $('.reporte').fadeOut(0);
            $('.aforo').fadeOut(0);
            $('.seguridad').fadeOut(0);
            $('.inspeccion').fadeOut(0);
            w = 400;
            h = 100;
            reset();
        } else if (tab == 'seguridad') {
            $('#reporte').removeClass('activo');
            $('#aforo').removeClass('activo');
            $('#ensayo').removeClass('activo');
            $('#inspeccion').removeClass('activo');
            $('#seguridad').addClass('activo');
            $('.seguridad').fadeIn();
            $('.ensayo').fadeOut(0);
            $('.reporte').fadeOut(0);
            $('.aforo').fadeOut(0);
            $('.inspeccion').fadeOut(0);
            w = 330;
            h = 210;
            reset();
        } else if (tab == 'reporte') {
            $('#ensayo').removeClass('activo');
            $('#aforo').removeClass('activo');
            $('#seguridad').removeClass('activo');
            $('#inspeccion').removeClass('activo');
            $('#reporte').addClass('activo');
            $('.reporte').fadeIn();
            $('.ensayo').fadeOut(0);
            $('.aforo').fadeOut(0);
            $('.seguridad').fadeOut(0);
            $('.inspeccion').fadeOut(0);
            w = 400;
            h = 260;
            reset();
        } else if (tab == 'aforo') {
            $('#ensayo').removeClass('activo');
            $('#reporte').removeClass('activo');
            $('#seguridad').removeClass('activo');
            $('#inspeccion').removeClass('activo');
            $('#aforo').addClass('activo');
            $('.aforo').fadeIn();
            $('.ensayo').fadeOut(0);
            $('.reporte').fadeOut(0);
            $('.seguridad').fadeOut(0);
            $('.inspeccion').fadeOut(0);
        } else if (tab == 'inspeccion') {
            $('#ensayo').removeClass('activo');
            $('#reporte').removeClass('activo');
            $('#seguridad').removeClass('activo');
            $('#aforo').removeClass('activo');
            $('#inspeccion').addClass('activo');
            $('.inspeccion').fadeIn();
            $('.ensayo').fadeOut(0);
            $('.reporte').fadeOut(0);
            $('.seguridad').fadeOut(0);
            $('.aforo').fadeOut(0);
        }
    });

    $('.auto_ciu').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: 'libs/certifica2.php',
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function(data) {
                    if (!data.length) {
                        result = [{
                            label: 'No se han encontrado resultados',
                            value: response.term
                        }];
                        response(result);
                    } else {
                        response($.map(data, function(item) {
                            code = item.split("|");
                            return {
                                value: code[1],
                                data: item
                            };
                        }));
                    }
                }
            });
        },
        autoFocus: true,
        minLength: 2,
        select: function(event, ui) {
            if (ui.item.label != 'No se han encontrado resultados') {
                names = ui.item.data.split("|");
            } else {
                event.preventDefault();
            }
        }
    });

    $('.cpt').on('keyup', function() {
        $(this).next('label').next('.bls').val((parseInt($(this).val()) / 42).toFixed(2));
        console.log(parseInt($(this).val()) / 42);
    });

    $('.placa').on('click', function() {
        btn = $(this);
        modal_titulo = $(this).text();
        modal({
            type: 'primary',
            title: 'Foto ' + modal_titulo,
            text: '<input type="file"><div class="contenedor-imagen"><br><br><img id="imagen-placa" src="images/img_fondo.jpg"></div>',
            size: 'large',
            center: true,
            autoClose: false,
            closeClick: false,
            buttons: [{
                text: '+',
                val: 'zoomin',
                eKey: true,
                addClass: 'btn-circle btn-blue btn-zoom',
                onClick: function() {
                    if (cropper)
                        cropper.zoom(0.1);
                }
            }, {
                text: '-',
                val: 'zoomout',
                eKey: true,
                addClass: 'btn-circle btn-blue btn-zoom',
                onClick: function() {
                    if (cropper)
                        cropper.zoom(-0.1);
                }
            }, {
                text: 'Ok',
                val: 'ok',
                eKey: true,
                addClass: 'btn-large',
                onClick: function() {
                    cropper.getCroppedCanvas().toBlob(function(blob) {
                        url = URL.createObjectURL(blob);
                        btn.prev('img').attr('src', url);
                        ext = blob.type.split('/');
                        formData.delete(btn.prop('id'));
                        formData.append(btn.prop('id'), blob, btn.prop('id') + '.' + ext[1]);
                    });
                    return true;
                }
            }, ],
        });
    });

    $('body').on('change', 'input[type=file]', function() {
        var files = this.files;
        var file;

        if (files.length > 0) {
            file = files[0];
            if (/^image\/\w+/.test(file.type)) {
                if (this.url) {
                    URL.revokeObjectURL(this.url);
                }
                this.url = URL.createObjectURL(file);
                img = $(this).next('div').find('img');
                img.attr({
                    src: this.url
                });
                cropper = new Cropper($('#imagen-placa').get(0), {
                    dragMode: 'move',
                    cropBoxResizable: false,
                    built: function() {
                        this.cropper.setCropBoxData({
                            "left": 100,
                            "top": 100,
                            "width": w,
                            "height": h
                        });
                    }

                });
            } else {
                modal({
                    type: 'error',
                    title: 'Formato Incorrecto',
                    text: 'Debe seleccionar una imagen válida'
                });
            }
        }
    });

    $('#tipo-certi').on('change', function() {
        if ($(this).val() == 1) {
            $('.ct_kp').fadeIn(0, function() {
                $(this).find('select').attr({
                    disabled: false
                });
            });
            $('#foto2').html('FOTO QUINTA RUEDA');
        } else if ($(this).val() == 2) {
            $('.ct_kp').fadeOut(0, function() {
                $(this).find('select').attr({
                    disabled: true
                });
            });
            $('#foto2').html('FOTO PASARELA');
        } else if ($(this).val() == 3) {
            $('.ct_kp').fadeOut(0, function() {
                $(this).find('select').attr({
                    disabled: true
                });
            });
            $('#foto2').html('FOTO KING PIN');
        }
    });

    $('.cre_certificado').on('submit', function(e) {
        tipoform = $(this);
        e.preventDefault();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);

        data = $(this).serializeArray();

        $.each(data, function(key, input) {
            formData.append(input.name, input.value);
        });

        $.ajax('libs/certifica2', {
            method: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'Correcto') {
                    form = $('<form action="libs/certifica2" method="post" target="_blank">' +
                        '<input type="hidden" name="certifica[accion]" value="generar">' +
                        '<input type="hidden" name="certifica[certificado]" value="' + data.certificado + '">' +
                        '<input type="hidden" name="certifica[id]" value="' + data.certificadoid + '">' +
                        '</form>');
                    form.appendTo($('body')).submit().remove();
                    $('#fondo').remove();
                    $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    $('#fondo').append("<div class='rp' id='rp'><span>Creado Correctamente</span></div>");
                    setTimeout(function() {
                        $('#fondo').fadeIn('fast', function() {
                            $('#rp').animate({
                                'top': '50%'
                            }, 50).fadeIn();
                        });
                    }, 400);
                    setTimeout(function() {
                        $("#rp").fadeOut();
                        $('#fondo').fadeOut('fast');
                    }, 2000);
                    reset();
                    if (data.certificado == 'aforo') {
                        ncertificado = $('.ncertificadoaf').val().split('-');
                        s = "0000" + (parseInt(ncertificado[1]) + 1);
                        ncertificado = ncertificado[0] + '-' + s.substr(s.length - 5);
                        $('.ncertificadoaf').val(ncertificado);
                    } else if (data.certificado == 'inspeccion') {
                        $('[type=radio]').attr('checked', false);
                    } else {
                        // ncertificado = $('.ncertificado').val().split('-');
                        // s = "00000" + (parseInt(ncertificado[1]) + 1);
                        // ncertificado = ncertificado[0] + '-' + s.substr(s.length - 6);
                        // $('.ncertificado').val(ncertificado);
                    }
                    $('.temp').remove();
                } else {
                    $('#fondo').remove();
                    $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    $('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
                    setTimeout(function() {
                        $('#fondo').fadeIn('fast', function() {
                            $('#rp2').animate({
                                'top': '50%'
                            }, 50).fadeIn();
                        });
                    }, 400);
                    setTimeout(function() {
                        $("#rp2").fadeOut();
                        $('#fondo').fadeOut('fast');
                    }, 3000);
                }
            },
            error: function() {
                console.log('error subiendo');
            }
        });
    });

    function reset() {
        $('input', '.cre_certificado').not('[type=submit], [type=hidden], [readonly], [type=radio]').val('');
        $('textarea').val('');
        formData = new FormData();
        $.each($('.img-prev'), function() {
            $(this).removeAttr('src');
        });
        $.each($('.img-prev2'), function() {
            $(this).removeAttr('src');
        });
        $.each($('.img-prev-sg'), function() {
            $(this).removeAttr('src');
        });
    }

    // $('#generar').on('click', function() {
    //  form = $('<form action="libs/t" method="post" target="_blank"></form>');
    //  form.appendTo($('body')).submit().remove();
    // });


    // crop: function(e) {
    //  var json = [
    //      '{"x":' + e.detail.x,
    //      '"y":' + e.detail.y,
    //      '"height":' + e.detail.height,
    //      '"width":' + e.detail.width,
    //      '"rotate":' + e.detail.rotate + '}'
    //  ].join();
    //  $('[name=imagen1_data]').val(json);
    // }


    /*$('').on('click', function() {
        cropper1.getCroppedCanvas().toBlob(function(blob) {
            // var newImg = document.createElement("img"),
            // var newinput = document.createElement("input"),
            // url = URL.createObjectURL(blob);
            // newinput.onload = function() {
            //  URL.revokeObjectURL(url);
            // };
            // newinput.name = 'recorte';
            // newinput.setAttribute('value', url);
            // $('#test').append(newinput);
            // document.body.appendChild(newinput);
            // newImg.src = url;
            // document.body.appendChild(newImg);

            // $('#imagen2').attr('src', blob);
            // var myReader = new FileReader();
            // myReader.addEventListener("loadend", function(e){
            //  console.log(e.srcElement.result); //prints a string
            // });
            // myReader.readAsText(blob);
            ext = blob.type.split('/');
            formData.delete('recorte');
            formData.append('recorte', blob, 'recorte.' + ext[1]);
            cropper1.destroy();
        });

    });*/

    tcompart = 0;

    $('#agr_comp').on('click', function() {
        if (tcompart < 4) {
            $('#compartimientos').append('<div class="temp"><hr>' +
                '<a class="quitar">Eliminar</a><div class="Registro-der">' +
                '<label>Capacidad Operacional GLS *</label>' +
                '<input type="text" placeholder="Capacidad Operacional GLS" name="certifica[capacidadope][]" required>' +
                '<label>Capacidad Nominal BLS *</label>' +
                '<input type="text" placeholder="Capacidad Nominal BLS" name="certifica[capacidadnom][]" required>' +
                '<label>Altura Operacional  (m)*</label>' +
                '<input type="text" placeholder="Altura Operacional" name="certifica[alturaopera][]" required>' +
                '<label>Altura Referencial (m)*</label>' +
                '<input type="text" placeholder="Altura Referencial " name="certifica[alturarefe][]" required>' +
                '</div>' +
                '<div class="Registro-der">' +
                '<label>Dimensión del tanque </label>' +
                '<label>Diámetro (m3)*</label>' +
                '<input type="text" placeholder="Diámetro" name="certifica[diametro][]" required>' +
                '<label>Alto (m)*</label>' +
                '<input type="text" placeholder="Alto" name="certifica[alto][]" required>' +
                '<label>Ancho (m)*</label>' +
                '<input type="text" placeholder="Ancho" name="certifica[ancho][]" required>' +
                '<label>Largo (m)*</label>' +
                '<input type="text" placeholder="Largo" name="certifica[largo][]" required>' +
                '</div></div>');
            tcompart++;
        }
    });

    $('body').on('click', '.quitar', function() {
        $(this).parent().remove();
        tcompart--;
    });

    $('#tipo-seg').on('change', function() {
        if ($(this).val() == 1) {
            $('.cer_eslinga').fadeOut(0);
            $('.cer_arnes').fadeIn();
        } else {
            $('.cer_arnes').fadeOut(0);
            $('.cer_eslinga').fadeIn();
        }
        $('.tipocer').val($(this).val());
    });

});