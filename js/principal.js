$(document).ready(function () {
	$('.navegador li').on('click', function () {
		cl_nav = $(this).attr('class');
		$('html, body').animate({
			scrollTop: $('#' + cl_nav).offset().top
		}, 2000);
	});

	$('.v-nosotros div').on('click', function () {
		$('.v-describe div').fadeOut(0);
		$('.' + $(this).prop('id')).fadeIn();
	});

	$('.navegador-servicios li').on('click', function () {
		$('.v-servicios div').fadeOut(0);
		switch ($(this).index()) {
			case 0:
				$('.sv-1').fadeIn();
				break;
			case 1:
				$('.sv-2').fadeIn();
				break;
			case 2:
				$('.sv-3').fadeIn();
				break;
			case 3:
				$('.sv-4').fadeIn();
				break;
			case 4:
				$('.sv-5').fadeIn();
				break;
			case 5:
				$('.sv-6').fadeIn();
				break;
		}
	});

	$('#v-certificados input').on('keyup', function () {
		b = $(this).val();
		$('#v-certificados input').not('[type=submit]').val('');
		$(this).val(b);
	});

	$('#v-certificados').on('submit', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		$('#fondo').remove();
		if ($('.certalturas').val() != '') {
			datosrequeridos = '<div class="Seccion4-form">' +
				'<form id="solicitud-alturas">' +
				'<label>N° de Identificación</label>' +
				'<input type="text" placeholder="Número" name="consulta[identificacionaltuas]" value="' + $('.certalturas').val() + '" required>' +
				'<label>Celular</label>' +
				'<input type="text" placeholder="Número" name="consulta[celular]" required>' +
				'<label>Correo Electrónico</label>' +
				'<input type="email" placeholder="Correo Electrónico" name="consulta[correo]" required>' +
				'<input type="hidden" name="consulta[tcertificado]" value="alturas">' +
				'<input type="submit" class="Btn-rojo" value="Consultar" name="">' +
				'</form>' +
				'</div>';
			modal({
				type: 'primary',
				title: 'Solicitud consulta certificado de alturas',
				text: datosrequeridos,
				size: 'normal',
				center: true,
				autoClose: false,
				closeClick: false,
				buttons: [{
					text: 'Solicitar',
					val: 'ok',
					eKey: true,
					addClass: 'btn-none',
					onClick: function (dialog) {
						console.log(dialog);
						return true;
					}
				},],
			});
		} else {
			$.post('libs/consultas.php', data, function (data) {
				if (data.status == 'Correcto') {
					$('#fondo').remove();
					modal({
						type: 'primary',
						title: 'Resultado de la consulta',
						text: data.certificados,
						size: 'normal',
						center: true,
						autoClose: false,
						closeClick: false,
					});
				} else if (data.status == 'Error') {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp2'><span>No se han encontrado resultados.</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp2').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp2").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 3000);
				}
			}, "json");
		}
	});

	$('body').on('submit', '#solicitud-alturas', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		$.post('libs/consultas.php', data, function (data) {
			if (data.status == 'Correcto') {
				$('#fondo').remove();
				$('.modal-close-btn').click();
				$('input').not('input[type=submit]').val('');
				if (data.alturas == true) {
					datosrequeridos = '<div class="Seccion4-form" style="text-align:center">' +
						'<p><a href="consulta-actas?registro=' + data.reg + '" target="_blank" class="Btn-rojo">Consultar Acta</a></p><br>' +
						'<p><a href="consulta-carnets?registro=' + data.reg + '" target="_blank" class="Btn-rojo">Consultar Carnet</a></p>' +
						'</div>';
					modal({
						type: 'primary',
						title: 'Consultar',
						text: datosrequeridos,
						size: 'normal',
						center: true,
						autoClose: false,
						closeClick: false,
						buttons: [{
							text: 'Solicitar',
							val: 'ok',
							eKey: true,
							addClass: 'btn-none',
							onClick: function (dialog) {
								console.log(dialog);
								return true;
							}
						},],
					});
				} else {
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append("<div class='rp' id='rp'><span>Solicitud realizada</span></div>");
					setTimeout(function () {
						$('#fondo').fadeIn('fast', function () {
							$('#rp').animate({
								'top': '50%'
							}, 50).fadeIn();
						});
					}, 400);
					setTimeout(function () {
						$("#rp").fadeOut();
						$('#fondo').fadeOut('fast');
					}, 2000);
				}
			} else if (data.status == 'Error') {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>Error! No se pudo enviar la solicitud.</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, "json");
	});


	///// Calificar

	$('.Calificar').on('click', function () {
		inform = '<div class="Seccion4-form">' +
			'<form id="solicitud-calificar">' +
			'<label>N° de Identificación</label>' +
			'<input type="text" placeholder="Número" name="consulta[identificacion]" required>' +
			'<input type="submit" class="Btn-rojo" value="Verificar" name="">' +
			'</form>' +
			'</div>';
		modal({
			type: 'primary',
			title: 'Realizar evaluación del curso',
			text: inform,
			size: 'normal',
			center: true,
			autoClose: false,
			closeClick: false,
			buttons: [{
				text: 'Solicitar',
				val: 'ok',
				eKey: true,
				addClass: 'btn-none',
				onClick: function (dialog) {
					return true;
				}
			},],
		});
	});

	$('body').on('submit', '#solicitud-calificar', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		$.post('libs/verificar', data, function (data) {
			if (data.status == true) {
				$('#fondo').remove();
				$('.modal-close-btn').click();
				$('input').not('input[type=submit]').val('');
				window.location.href = data.redirec;
			} else if (data.status == false) {
				$('#fondo').remove();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>Error! No se pudo verificar la información.</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, "json");
	});

	///// PQRS

	$('.Form-pqrs').on('click', function () {
		inform = '<div class="Seccion4-form">' +
			'<p>Por favor diligencie todos los campos.</p>' +
			'<form id="enviar-pqrs">' +
			'<label>Nombre y Apellido</label>' +
			'<input type="text" placeholder="Nombre y Apellido" name="pqrs[nombre]" required>' +
			'<label>Celular</label>' +
			'<input type="text" placeholder="Celular" name="pqrs[celular]" required>' +
			'<label>Motivo del mensaje</label>' +
			'<input type="text" placeholder="Motivo del mensaje" name="pqrs[motivo]" required>' +
			'<label>Comentarios</label>' +
			'<textarea placeholder="Comentarios" style="width:100%" rows="4" name="pqrs[comentario]"></textarea><br><br>' +
			'<input type="submit" class="Btn-rojo" value="Enviar" name="">' +
			'</form>' +
			'</div>';
		modal({
			type: 'primary',
			title: 'PQRS',
			text: inform,
			size: 'normal',
			center: true,
			autoClose: false,
			closeClick: false,
			buttons: [{
				text: 'Solicitar',
				val: 'ok',
				eKey: true,
				addClass: 'btn-none',
				onClick: function (dialog) {
					return true;
				}
			},],
		});
	});

	$('body').on('submit', '#enviar-pqrs', function (e) {
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $(this).serializeArray();
		$.post('libs/pqrs-envio', data, function (data) {
			$('#fondo').remove();
			if (data.status == true) {
				$('.modal-close-btn').click();
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp'><span>PQRS Enviada</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 2000);
			} else if (data.status == false) {
				$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
				$('#fondo').append("<div class='rp' id='rp2'><span>Error! No se enviar la PQRS.</span></div>");
				setTimeout(function () {
					$('#fondo').fadeIn('fast', function () {
						$('#rp2').animate({
							'top': '50%'
						}, 50).fadeIn();
					});
				}, 400);
				setTimeout(function () {
					$("#rp2").fadeOut();
					$('#fondo').fadeOut('fast');
				}, 3000);
			}
		}, "json");
	});
});
