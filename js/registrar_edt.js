$(document).ready(function () {
	setTimeout(function () {
		// $('.capacitacion').change();
	}, 200);

	$('.capacitacion').on('change', function () {
		$('.Sel-foto').hide();
		$('.Fecha-finaliza').show();
		$('#ver-entrenadores').hide();
		$('option', '.entrenadores').not(':first').hide();
		$('.img-placa').hide();

		tipo_competencia = $('option:selected', this).attr('data-nombre');

		if (tipo_competencia == 'cbasico') {
			$('.Sel-foto').show();
			$('.Fecha-finaliza').hide();
			$('.img-placa').show();
		}

		if (tipo_competencia == 'alturas' || tipo_competencia == 'cea' || tipo_competencia == 'espaciosconfinados') {
			$('[data-tipo=' + tipo_competencia + ']', '.entrenadores').show();
			$('.entrenadores').val('');
			$('#ver-entrenadores').show();
			$('.Fecha-finaliza').show();

			if (tipo_competencia == 'cea' || tipo_competencia == 'espaciosconfinados') {
				$('.img-placa').show();
			}
		}

		if ($('.Sel-vendedor').val() != '') {
			CargaCursos();
		}
	});

	$('.Sel-vendedor').on('change', function () {
		if ($('.capacitacion').val() != '') {
			CargaCursos();
		}
	});

	function CargaCursos() {
		idvendedor = $('option:selected', '.Sel-vendedor').prop('id').split('-');
		$('.vendedor_direccion').val('');
		$('.vendedor_telefono').val('');
		$('.vendedor_correo').val('');

		$('.vendedor_direccion').val($('option:selected', '.Sel-vendedor').data('direccion'));
		$('.vendedor_telefono').val($('option:selected', '.Sel-vendedor').data('telefono'));
		$('.vendedor_correo').val($('option:selected', '.Sel-vendedor').data('correo'));
		$('.ls_cursos').val('');
		$('.ls_cursos option').not(':first').remove();
		$('.Precio').val('');
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 400);

		$.post('libs/acc_registros', {
			'accion': 'cargar-cursos',
			'vendedor': idvendedor[1],
			'capacitacion': $('.capacitacion option:selected').attr('data-nombre')
		}, function (data) {
			$('#fondo').remove();
			if (data != '') {
				$('.ls_cursos').append(data);
			}
		}, 'json');
	}

	$('body').on('change', '.ls_cursos', function () {
		$('.Precio').val($('option:selected', $('.ls_cursos')).data('precio'));
		$('.Sel-horas').val($('option:selected', $('.ls_cursos')).attr('data-horas'));
	});


	var cropper;
	var formData = new FormData();
	w = 113;
	h = 152;

	$('.placa').on('click', function () {
		btn = $(this);
		modal_titulo = $(this).text();
		modal({
			type: 'primary',
			title: modal_titulo,
			text: '<input type="file"><div class="contenedor-imagen"><br><br><img id="imagen-placa" src="images/img_fondo.jpg"></div>',
			size: 'small',
			center: true,
			autoClose: false,
			closeClick: false,
			buttons: [{
				text: '+',
				val: 'zoomin',
				eKey: true,
				addClass: 'btn-circle btn-blue btn-zoom',
				onClick: function () {
					if (cropper)
						cropper.zoom(0.1);
				}
			}, {
				text: '-',
				val: 'zoomout',
				eKey: true,
				addClass: 'btn-circle btn-blue btn-zoom',
				onClick: function () {
					if (cropper)
						cropper.zoom(-0.1);
				}
			}, {
				text: 'Ok',
				val: 'ok',
				eKey: true,
				addClass: 'btn-large',
				onClick: function () {
					cropper.getCroppedCanvas().toBlob(function (blob) {
						url = URL.createObjectURL(blob);
						btn.prev('img').attr('src', url);
						ext = blob.type.split('/');
						formData.delete(btn.prop('id'));
						formData.append(btn.prop('id'), blob, btn.prop('id') + '.' + ext[1]);
					});
					return true;
				}
			},],
		});
	});

	$('body').on('change', 'input[type=file]', function () {
		var files = this.files;
		var file;

		if (files.length > 0) {
			file = files[0];
			if (/^image\/\w+/.test(file.type)) {
				if (this.url) {
					URL.revokeObjectURL(this.url);
				}
				this.url = URL.createObjectURL(file);
				img = $(this).next('div').find('img');
				img.attr({
					src: this.url
				});
				cropper = new Cropper($('#imagen-placa').get(0), {
					dragMode: 'move',
					cropBoxResizable: false,
					built: function () {
						this.cropper.setCropBoxData({
							"left": 100,
							"top": 100,
							"width": w,
							"height": h
						});
					}

				});
			} else {
				modal({
					type: 'error',
					title: 'Formato Incorrecto',
					text: 'Debe seleccionar una imagen válida'
				});
			}
		}
	});

	$('#agr_curso').on('click', function () {
		$('#cursos_adicionales').append('<div class="temp" > <hr> ' +
			'<a class="quitar">Eliminar</a>' +
			'<div class="Registro-der">' +
			'<label>Nombre Capacitación</label>' +
			'<select name="capacitacion_ad[]" class="capacitacion-adicional capacitacion_ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'<label>Vencimiento - Carnet</label>' +
			'<input type="date" placeholder="Vencimiento" name="vencimiento_ad[]" required>' +
			'<label>Fecha de Vigencia *</label>' +
			'<input type="date" name="f_vigencia_ad[]" required>' +
			'<label>Precio *</label>' +
			'<input type="text" placeholder="Precio" name="precio_ad[]" required>' +
			'</div>' +
			'<div class="Registro-der">' +
			'<label>Certificación a realizar *</label>' +
			'<select name="certificado_ad[]" class="cursos_ad" required>' +
			'<option value="">Seleccione</option>' +
			'</select>' +
			'<label>Fecha de inicio *</label>' +
			'<input type="date" name="f_inicio_ad[]" required>' +
			'<label>Horas *</label>' +
			'<input type="text" placeholder="Horas" name="horas_ad[]" required>' +
			'</div></div>');

		$.each($('.ls_cursos option'), function () {
			$(this).clone().attr('selected', false).appendTo($('.cursos_ad'));
		});

		$.each($('.capacitacion option').not(':first'), function () {
			$(this).clone().attr('selected', false).appendTo($('.capacitacion_ad'));
		});

		$('.cursos_ad').removeClass('cursos_ad');
		$('.capacitacion_ad').removeClass('capacitacion_ad');
	});

	$('body').on('change', '.capacitacion-adicional', function () {
		if ($('.Sel-vendedor').val() != '') {
			idvendedor = $('option:selected', '.Sel-vendedor').prop('id').split('-');
			select_curso_ad = $(this).closest('.temp').find('.Sel-curso-ad');
			$('option', select_curso_ad).not(':first').remove();
			$(this).closest('.temp').find('.Precio').val('');
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="loader-inner ball-beat">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function () {
				$('#fondo').fadeIn('fast');
			}, 400);

			$.post('libs/acc_registros', {
				'accion': 'cargar-cursos',
				'vendedor': idvendedor[1],
				'capacitacion': $('option:selected', this).attr('data-nombre')
			}, function (data) {
				$('#fondo').remove();
				if (data != '') {
					select_curso_ad.append(data);
				}
			}, 'json');
		}
	});

	$('body').on('change', '.Sel-curso-ad', function () {
		$(this).closest('.temp').find('.Precio-ad').val($('option:selected', this).data('precio'));
	});

	$('body').on('click', '.quitar', function () {
		$(this).parent().remove();
	});

	$('.Aplica-check').on('click', function () {
		if ($(this).is(':checked')) {
			$(this).next('label').next('.Aplica-disabled').attr('disabled', true).val('');
		} else {
			$(this).next('label').next('.Aplica-disabled').attr('disabled', false).val('');
		}
	});

	$('.No-espacio').on('keyup', function () {
		if ($(this).val().trim() == $(this).val()) {
			$(this).removeClass('Error-espacio');
		}
	})

	$('#registro').on('submit', function (e) {
		e.preventDefault();

		var error = false;

		$.each($('.No-espacio'), function () {
			var value = $(this).val().trim();

			if (value !== $(this).val()) {
				$(this).addClass('Error-espacio');
				error = true;
			} else {
				$(this).removeClass('Error-espacio');
			}
		})

		if (error == false) {
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="loader-inner ball-beat">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function () {
				$('#fondo').fadeIn('fast');
			}, 400);
			data = $(this).serializeArray();
			data.push({
				'name': 'accion',
				'value': 'editar2'
			});

			$.each(data, function (key, input) {
				formData.append(input.name, input.value);
			});

			$.ajax('libs/acc_registros', {
				method: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				dataType: 'json',
				success: function (data) {
					if (data.status == 'Correcto') {
						$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp'><span>Registro editado</span></div>");
						setTimeout(function () {
							$('#fondo').fadeIn('fast', function () {
								$('#rp').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function () {
							$("#rp").fadeOut();
							$('#fondo').fadeOut('fast');
							// open(location, '_self').close();
						}, 3000);
						$('#cursos_adicionales').empty();
					} else {
						if (data.status == 'Error1')
							msj = 'Ha ocurrido un error, intentelo de nuevo más tarde.';
						if (data.status == 'Error2')
							msj = 'La fecha de inicio de este certificado ya se encuentra registrada !';
						$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp2'><span>" + msj + "</span></div>");
						setTimeout(function () {
							$('#fondo').fadeIn('fast', function () {
								$('#rp2').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function () {
							$("#rp2").fadeOut();
							$('#fondo').fadeOut('fast');
						}, 3000);
					}
				}
			});
		} else {
			$('html, body').animate({ scrollTop: 0 }, 'fast');
		}
	});

	$('.Sel-vendedor').select2({
		language: "es",
	});

	$('.ls_cursos').select2({
		language: "es",
	});

	function reset() {
		formData = new FormData();
		$.each($('.img-prev'), function () {
			$(this).attr('src', 'images/marca_agua.jpg');
		});
	}

});
