$(document).ready(function () {
    tempid = '';
    tempnom = '';
    cambiar_pag('0');

    $('#buscar').on('submit', function (e) {
        e.preventDefault();
        tempnom = $('[name=nom]').val();
        tempid = $('[name=id]').val();
        data = $(this).serializeArray();
        data.push({
            'name': 'accion',
            'value': 'listar'
        });
        data.push({
            'name': 'pagina',
            'value': '0'
        });
        $('#lista_loader').show();
        $('#resultados').empty();
        $.post('libs/acc_registros.php', data, function (data) {
            $('#lista_loader').hide();
            $('#resultados').html(data.registros);
            $('#paginacion').html(data.paginacion);
        }, "json");
    });

    $('#buscar input').not('[name=idregistro]').on('keyup', function () {
        innom = $('[name=nom]').val();
        inid = $('[name=id]').val();
        if (!innom && !inid) {
            tempid = '';
            tempnom = '';
            cambiar_pag('0');
        }
    });

    $('body').on('click', '.editar', function () {
        $(this).removeClass('editar');
        fila = $(this).closest('tr');
        $.each($('td', fila), function () {
            if ($(this).index() == 1 || $(this).index() == 3 || $(this).index() == 4 || $(this).index() == 5 || $(this).index() == 8 || $(this).index() == 10) {
                content = $(this).find('p').html();
                $(this).html('<input type="text" value="' + content + '" >');
            }

            if ($(this).index() == 6 || $(this).index() == 7) {
                content = $(this).find('p').html();
                $(this).html('<input type="date" value="' + content + '" >');
            }
        });
        fila.addClass('editando');
        $(this).next('a').addClass('guardar');
    });

    $('body').on('click', '.guardar', function () {
        $(this).removeClass('guardar');
        fila = $(this).closest('tr');
        data = new FormData();

        $.each($('td', fila), function () {
            if ($(this).index() == 1 || $(this).index() == 3 || $(this).index() == 4 || $(this).index() == 5 || $(this).index() == 6 || $(this).index() == 7 || $(this).index() == 8 || $(this).index() == 10) {
                content = $(this).find('input').val();
                $(this).html('<p>' + content + '</p>');
                if ($(this).index() == 1)
                    data.append('identifica', content);
                if ($(this).index() == 3)
                    data.append('primer_nombre', content);
                if ($(this).index() == 4)
                    data.append('segundo_nombre', content);
                if ($(this).index() == 5)
                    data.append('apellido', content);
                if ($(this).index() == 6)
                    data.append('f_inicio', content);
                if ($(this).index() == 7)
                    data.append('f_vigencia', content);
                if ($(this).index() == 8)
                    data.append('horas', content);
                if ($(this).index() == 10)
                    data.append('precio', content);
            }
        });

        fila.removeClass('editando');
        $(this).prev('a').addClass('editar');
        data.append('id', fila.prop('id'));
        data.append('accion', 'editar');

        $.ajax({
            url: 'libs/acc_registros',
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            dataType: 'json',
            success: function (data) {
                if (data.status == 'Correcto')
                    console.log('ok');
                else
                    console.log('error');
            }
        });
    });

    $('body').on('click', '.eliminar', function () {
        registro = $(this);
        modal({
            type: 'confirm',
            title: 'Eliminar Registro',
            text: 'Desea eliminar el registro ?',
            size: 'small',
            callback: function (result) {
                if (result === true) {
                    id = registro.closest('tr').prop('id');
                    registro.closest('tr').fadeOut(500, function () {
                        registro.remove();
                    });
                    $.post('libs/acc_registros.php', {
                        accion: 'eliminar',
                        id: id
                    });
                }
            },
            closeClick: false,
            theme: 'xenon',
            animate: true,
            buttonText: {
                yes: 'Confirmar',
                cancel: 'Cancelar'
            }
        });
    });

    $('body').on('click', '.gen-acta', function () {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 400);

        id = $(this).prop('id').split('-');
        id = id[1];

        $.getJSON('libs/acc_registros', {
            'accion': 'acta-cursos',
            'id': id
        }, function (data) {
            $('#fondo').remove();
            content_acta = '<div class="Registro">' +
                '<form id="acta">' +
                '<div class="Acta-cursos">' +
                '<label>Cursos</label>' +
                data.cursos +
                '</div>' +
                '<label>Fecha de expedición</label>' +
                '<input type="date" id="fexpide" required>' +
                '<input type="submit" style="display:none">' +
                '</div>' +
                '</form>';
            modal({
                type: 'confirm',
                title: 'ACTA CERTIFICACION',
                text: content_acta,
                size: 'normal',
                callback: function (result) {
                    if (result === true) {
                        $('[type=submit]', '#acta').click();
                        return false;
                    }
                },
                closeClick: false,
                theme: 'xenon',
                animate: true,
                buttonText: {
                    yes: 'Confirmar',
                    cancel: 'Cancelar'
                }
            });
        });

    });

    $('body').on('submit', '#acta', function (e) {
        e.preventDefault();
        ls_cursos = '<input name="curso_ver[]" value="">';
        $.each($('.curso_ver', '#acta'), function () {
            if ($(this).is(':checked')) {
                ls_cursos += '<input name="curso_ver[]" value="' + $(this).val() + '">';
            }
        });
        $('<form action="actas" method="GET" style="display:none" target="_blank"><input name="iden" value="' + id + '"><input name="expide" value="' + $('#fexpide').val() + '">' + ls_cursos + '</form>').appendTo('body').submit().remove();
        $('.modal-close-btn').click();
    });

    $('.solicitudes-altura').on('click', function () {
        content_solicitud = '<div class="Registro">' +
            '<form id="solicitudes">' +
            '<input type="submit" style="display:none">' +
            '</div>' +
            '</form>';
        modal({
            type: 'confirm',
            title: 'Solicitudes Altura',
            text: content_solicitud,
            size: 'normal',
            callback: function (result) {
                if (result === true) {
                    $('[type=submit]', '#solicitudes').click();
                    return false;
                }
            },
            closeClick: false,
            theme: 'xenon',
            animate: true,
            buttonText: {
                yes: 'Confirmar',
                cancel: 'Cancelar'
            }
        });
    });

    $('body').on('submit', '#solicitudes', function (e) {
        e.preventDefault();
        $('<form action="libs/solicitudes-realizadas" method="post" style="display:none" target="_blank"><input name="solicitud[inicio]" value="' + $('.solicitud-inicio').val() + '"><input name="solicitud[fin]" value="' + $('.solicitud-final').val() + '"></form>').appendTo('body').submit().remove();
        $('.modal-close-btn').click();
    });

    /* $('body').on('click', '#resultados td', function () {
        filag = $(this).closest('tr');
        if ($(this).index() < 12) {
            vigencia = filag.find('td').eq(7).find('p').text();
            horas = filag.find('td').eq(8).find('p').text();
            cursoCompetencia = filag.attr('data-tipo');

            if (horas !== '') {
                if (cursoCompetencia == 'espaciosconfinados') {
                    $('<form action="libs/certificado-espacios" method="GET" target="_blank">' +
                        '<input type="hidden" name="id" value="' + $(this).parent().prop('id') + '">' +
                        '</form>').appendTo('body').submit().remove();
                } else if (cursoCompetencia == 'alturasres.4272') {
                    $('<form action="libs/certificado-alturas4272" method="GET" target="_blank">' +
                        '<input type="hidden" name="id" value="' + $(this).parent().prop('id') + '">' +
                        '</form>').appendTo('body').submit().remove();
                } else {
                    $('<form action="libs/certificado" method="GET" target="_blank">' +
                        '<input type="hidden" name="id" value="' + $(this).parent().prop('id') + '">' +
                        '</form>').appendTo('body').submit().remove();
                }
            } else {
                if (!$(this).parent().hasClass('editando')) {
                    modal({
                        type: 'warning',
                        title: '¡ Atención !',
                        text: 'Para generar el certificado debes ingresar todos los datos.'
                    });
                }
            }
        }
    }); */

    certificados_exportar = [];

    $('body').on('click', '.check-generar', function (e) {
        if ($(this).is(':checked')) {
            certificados_exportar.push($(this).closest('tr').prop('id'));
        } else {
            certificados_exportar.splice(certificados_exportar.indexOf($(this).closest('tr').prop('id')), 1);
        }
    });

    $('.generar-codigo').on('click', function () {
        $('#fondo').remove();

        if (certificados_exportar.length > 0) {
            $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
            $('#fondo').append('<div class="loader">' +
                '<div class="loader-inner ball-beat">' +
                '<div></div>' +
                '<div></div>' +
                '<div></div>' +
                '</div>' +
                '</div>');
            setTimeout(function () {
                $('#fondo').fadeIn('fast');
            }, 400);

            $.getJSON('libs/acc_registros', {
                'accion': 'generar-codigo',
                'certificados_exportar': certificados_exportar
            }, function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    form = '<div class="Registro">' +
                        '<p>No recargue o salga de esta página hasta ingresar el código, de lo contrario se debe generar un nuevo código</p>' +
                        '<form id="Form-codigo">' +
                        '<input type="text" name="certificados-codigo" id="certificados-codigo" required>' +
                        '<input type="submit" style="display:none">' +
                        '</form>' +
                        '</div>';

                    modal({
                        type: 'inverted',
                        title: 'Ingresar Código',
                        text: form,
                        size: 'small',
                        buttons: [{
                            text: 'OK',
                            val: 'ok',
                            eKey: true,
                            addClass: 'btn-light-green',
                            onClick: function (dialog) {
                                $('[type=submit]', '#Form-codigo').click();
                                return true;
                            }
                        },],
                        closeClick: false,
                        closable: false,
                        theme: 'xenon',
                        animate: true,
                        buttonText: {
                            yes: 'Confirmar',
                            cancel: 'Cancelar'
                        }
                    });

                    certificados_exportar = [];
                } else {
                    $('#fondo').remove();
                    $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    $('#fondo').append("<div class='rp' id='rp2'><span>Error! No se pudo generar el Código</span></div>");
                    setTimeout(function () {
                        $('#fondo').fadeIn('fast', function () {
                            $('#rp2').animate({
                                'top': '50%'
                            }, 50).fadeIn();
                        });
                    }, 400);
                    setTimeout(function () {
                        $("#rp2").fadeOut();
                        $('#fondo').fadeOut('fast');
                    }, 3000);
                }
            });
        } else {
            $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
            $('#fondo').append("<div class='rp' id='rp2'><span>No hay certificados seleccionados</span></div>");
            setTimeout(function () {
                $('#fondo').fadeIn('fast', function () {
                    $('#rp2').animate({
                        'top': '50%'
                    }, 50).fadeIn();
                });
            }, 400);
            setTimeout(function () {
                $("#rp2").fadeOut();
                $('#fondo').fadeOut('fast');
            }, 3000);
        }
    })

    $('body').on('submit', '#Form-codigo', function (e) {
        e.preventDefault();

        $.post('libs/acc_registros', {
            'accion': 'comprobar-codigo',
            'certificados-codigo': $('#certificados-codigo').val()
        }, function (data) {
            if (data.status == true) {
                $('.modal-close-btn').click();
                $('#modal-window').remove();
                $('body').removeAttr('style');
                cambiar_pag(1);
            } else {
                $('#fondo').remove();
                $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                $('#fondo').append("<div class='rp' id='rp2'><span>Código incorrecto</span></div>");
                setTimeout(function () {
                    $('#fondo').fadeIn('fast', function () {
                        $('#rp2').animate({
                            'top': '50%'
                        }, 50).fadeIn();
                    });
                }, 400);
                setTimeout(function () {
                    $("#rp2").fadeOut();
                    $('#fondo').fadeOut('fast');
                }, 3000);
            }
        }, "json");

    });

});

function cambiar_pag(pag_id) {
    $('#lista_loader').show();
    $('#resultados').empty();
    data = 'pagina=' + pag_id + '&accion=listar&nom=' + tempnom + '&id=' + tempid;
    $.post('libs/acc_registros.php', data, function (data) {
        $('#lista_loader').hide();
        $('#resultados').html(data.registros);
        $('#paginacion').html(data.paginacion);
    }, "json");
}
