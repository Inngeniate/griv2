$(document).ready(function() {

	$('.fecha').flatpickr({
		locale: 'es',
		disableMobile: "true"
	});

	$('.mayus').on('keyup', function() {
		$(this).val($(this).val().toUpperCase());
	});

	$('.mayus-1').on('keyup', function() {
		txt = $(this).val();
		$(this).val(txt.substr(0, 1).toUpperCase() + txt.substr(1));
	});

	$('#fecha-nacimiento').on('change', function() {
		nacimiento = $(this).val();
		if (val_an(nacimiento) < 13) {
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append("<div class='rp' id='rp2'><span>La edad mínima para la inscripción es de 13 años</span></div>");
			setTimeout(function() {
				$('#fondo').fadeIn('fast', function() {
					$('#rp2').animate({
						'top': '50%'
					}, 50).fadeIn();
				});
			}, 400);
			setTimeout(function() {
				$("#rp2").fadeOut();
				$('#fondo').fadeOut('fast');
				$('#fondo').remove();
			}, 3000);
			$(this).closest('.Registro-asp-int-sec').find('input[type=submit]').attr('disabled', true);
		} else {
			$(this).closest('.Registro-asp-int-sec').find('input[type=submit]').attr('disabled', false);
		}
	});

	function val_an(nacimiento) {
		var now = new Date();
		var past = new Date(nacimiento);
		var diff = now - past;
		edad = Math.floor(diff / 31536000000);
		return edad;
	}

	tipo_programa = '';

	$('#programa').on('change', function() {
		tipo_programa = $(this).val();
		$.getJSON('libs/acc_aspirantes', {
			'registro[accion]': 'programas',
			'registro[tipo]': $(this).val()
		}, function(data) {
			$('#cursos').empty();
			$('#cursos').append('<label>Cursos*</label>');
			if (data.cursos != '') {
				arr = [];
				$.each(data.cursos, function(i, dat) {
					arr.push('<option value="' + dat.id + '">' + dat.nombre + '</option>');
				});
				$('#cursos').append('<select name="registro[curso]" class="requiere" required><option value="">Seleccione</option>' + arr.join('') + '</select>');
			}
		});
	});

	$('.colegio').autocomplete({
		source: function(request, response) {
			$.ajax({
				url: "https://www.datos.gov.co/resource/xax6-k7eu.json?$where=nombreestablecimiento like '%25" + $('.colegio').val() + "%25'",
				dataType: "json",
				success: function(data) {
					if (!data.length) {
						result = [{
							label: 'No se han encontrado resultados',
							value: response.term
						}];
						response(result);
					} else {
						response($.map(data.slice(0, 10), function(item) {
							return {
								value: item.nombreestablecimiento,
								data: item
							};
						}));
					}
				}
			});
		},
		autoFocus: true,
		minLength: 2,
	});

	$('.municipio').on('focus click', function() {
		municipio = $(this);
		municipio.autocomplete({
			source: function(request, response) {
				$.ajax({
					url: "https://www.datos.gov.co/resource/p95u-vi7k.json?$where=municipio like '%25" + municipio.val() + "%25'",
					dataType: "json",
					success: function(data) {
						if (!data.length) {
							result = [{
								label: 'No se han encontrado resultados',
								value: response.term
							}];
							response(result);
						} else {
							response($.map(data.slice(0, 10), function(item) {
								return {
									value: item.municipio + ' - ' + item.departamento,
									data: item
								};
							}));
						}
					}
				});
			},
			autoFocus: true,
			minLength: 2,
		});
	});

	$('body').on('keydown', '.ntxt', function(e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}

		cl = $(this).attr('class').split(' ');
		cl = cl[1].split('-');
		cl = cl[1];
		if ($(this).val().length >= cl) {
			e.preventDefault();
		}
	});

	function val_correo() {
		if ($('.correo_2').val().trim() != $('.correo_1').val().trim()) {
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append("<div class='rp' id='rp2'><span>La confirmación del correo electrónico no concuerda</span></div>");
			setTimeout(function() {
				$('#fondo').fadeIn('fast', function() {
					$('#rp2').animate({
						'top': '50%'
					}, 50).fadeIn();
				});
			}, 400);
			setTimeout(function() {
				$("#rp2").fadeOut();
				$('#fondo').fadeOut('fast');
				$('#fondo').remove();
			}, 3000);
			return false;
		} else {
			return true;
		}
	}

	$('.anterior').on('click', function() {
		var c = $(this).closest('.inscripcion-aspirantes');
		c.hide();
		c.prev('.inscripcion-aspirantes').show();
	});

	$('.inscripcion-aspirantes').on('submit', function(e) {
		e.preventDefault();
		if (validar($(this)) == true) {
			if ($('input[type=submit]', this).hasClass('val-correo')) {
				if (val_correo() == true) {
					envio_datos($(this));
				}
			} else {
				envio_datos($(this));
			}
		} else {
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append("<div class='rp' id='rp2'><span>Lo campos con asterisco (*) son requeridos</span></div>");
			setTimeout(function() {
				$('#fondo').fadeIn('fast', function() {
					$('#rp2').animate({
						'top': '50%'
					}, 50).fadeIn();
				});
			}, 400);
			setTimeout(function() {
				$("#rp2").fadeOut();
				$('#fondo').fadeOut('fast');
				$('#fondo').remove();
			}, 3000);
		}
	});

	function validar(f) {
		var error = 0;
		$.each($('.requiere', f), function() {
			if ($(this).val().trim() == 0) {
				error++;
			}
		});
		if (error > 0) {
			return false;
		} else {
			return true;
		}
	}

	function envio_datos(form) {
		if (form.hasClass('inscripcion-inicio')) {
			$('#tipo_identificacion2').val($('#tipo-identificacion').val());
			$('#numero_identificacion2').val($('#numero-identificacion').val());
			$('#fecha_nacimiento2').val($('#fecha-nacimiento').val());
		}

		if (!form.hasClass('inscripcion-final')) {
			form.hide();
			form.next('.inscripcion-aspirantes').show();
			if (form.next('.inscripcion-aspirantes').hasClass('inscripcion-icfes') && tipo_programa == 'CC') {
				form.next('.inscripcion-aspirantes').find('.Registro-icfes').html('<p><strong>Este paso no es requerido puede continuar.</strong></p>');
			}
		}

		if (form.hasClass('inscripcion-final')) {
			formData = new FormData();
			$.each($('.inscripcion-aspirantes'), function() {
				data = $(this).serializeArray();
				$.each(data, function(key, input) {
					formData.append(input.name, input.value);
				});
			});

			formData.append('registro[accion]', 'registro');
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="loader-inner ball-beat">' +
				'<div></div>' +
				'<div></div>' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function() {
				$('#fondo').fadeIn('fast');
			}, 400);
			$.ajax('libs/acc_aspirantes', {
				method: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				dataType: 'json',
				success: function(data) {
					$('#fondo').remove();
					if (data.status == true) {
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp'><span>Gracias, su inscripción ha sido realizada</span></div>");
						setTimeout(function() {
							$('#fondo').fadeIn('fast', function() {
								$('#rp').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function() {
							$("#rp").fadeOut();
							$('#fondo').fadeOut('fast');
							$('<form action="formulario-registro" method="post" target="_blank" style="display:none""><input type="hidden" name="formulario" value="' + data.formulario + '"></form>').appendTo('body').submit().remove();
						}, 2000);
						$('.anterior', form).remove();
						$('input[type=submit]', form).val('Finalizado').attr('disabled', true);
						$('.inscripcion-aspirantes').not(form).remove();
						form.removeClass('inscripcion-aspirantes');
						$('input').not('input[type=submit]', 'input[type=button]').val('');
					} else if (data.status == false) {
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
						setTimeout(function() {
							$('#fondo').fadeIn('fast', function() {
								$('#rp2').animate({
									'top': '50%'
								}, 50).fadeIn();
							});
						}, 400);
						setTimeout(function() {
							$("#rp2").fadeOut();
							$('#fondo').fadeOut('fast');
						}, 3000);
					}
				}
			});
		}
	}
});