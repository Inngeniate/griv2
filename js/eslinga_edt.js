$(document).ready(function() {

    var cropper;
    var formData = new FormData();
    w = 330;
    h = 210;

    $('.auto_ciu').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: 'libs/certifica2.php',
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function(data) {
                    if (!data.length) {
                        result = [{
                            label: 'No se han encontrado resultados',
                            value: response.term
                        }];
                        response(result);
                    } else {
                        response($.map(data, function(item) {
                            code = item.split("|");
                            return {
                                value: code[1],
                                data: item
                            };
                        }));
                    }
                }
            });
        },
        autoFocus: true,
        minLength: 2,
        select: function(event, ui) {
            if (ui.item.label != 'No se han encontrado resultados') {
                names = ui.item.data.split("|");
            } else {
                event.preventDefault();
            }
        }
    });


    $('.placa').on('click', function() {
        btn = $(this);
        modal_titulo = $(this).text();
        modal({
            type: 'primary',
            title: modal_titulo,
            text: '<input type="file"><div class="contenedor-imagen"><br><br><img id="imagen-placa" src="images/img_fondo.jpg"></div>',
            size: 'large',
            center: true,
            autoClose: false,
            closeClick: false,
            buttons: [{
                text: '+',
                val: 'zoomin',
                eKey: true,
                addClass: 'btn-circle btn-blue btn-zoom',
                onClick: function() {
                    if (cropper)
                        cropper.zoom(0.1);
                }
            }, {
                text: '-',
                val: 'zoomout',
                eKey: true,
                addClass: 'btn-circle btn-blue btn-zoom',
                onClick: function() {
                    if (cropper)
                        cropper.zoom(-0.1);
                }
            }, {
                text: 'Ok',
                val: 'ok',
                eKey: true,
                addClass: 'btn-large',
                onClick: function() {
                    cropper.getCroppedCanvas().toBlob(function(blob) {
                        url = URL.createObjectURL(blob);
                        btn.prev('img').attr('src', url);
                        ext = blob.type.split('/');
                        formData.delete(btn.prop('id'));
                        formData.append(btn.prop('id'), blob, btn.prop('id') + '.' + ext[1]);
                    });
                    return true;
                }
            }, ],
        });
    });

    $('body').on('change', 'input[type=file]', function() {
        var files = this.files;
        var file;

        if (files.length > 0) {
            file = files[0];
            if (/^image\/\w+/.test(file.type)) {
                if (this.url) {
                    URL.revokeObjectURL(this.url);
                }
                this.url = URL.createObjectURL(file);
                img = $(this).next('div').find('img');
                img.attr({
                    src: this.url
                });
                cropper = new Cropper($('#imagen-placa').get(0), {
                    dragMode: 'move',
                    cropBoxResizable: false,
                    built: function() {
                        this.cropper.setCropBoxData({
                            "left": 100,
                            "top": 100,
                            "width": w,
                            "height": h
                        });
                    }

                });
            } else {
                modal({
                    type: 'error',
                    title: 'Formato Incorrecto',
                    text: 'Debe seleccionar una imagen válida'
                });
            }
        }
    });

    $('#ed_certificado').on('submit', function(e) {
        e.preventDefault();
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader">' +
            '<div class="loader-inner ball-beat">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
            '</div>' +
            '</div>');
        setTimeout(function() {
            $('#fondo').fadeIn('fast');
        }, 400);

        data = $(this).serializeArray();

        $.each(data, function(key, input) {
            formData.append(input.name, input.value);
        });

        formData.append('certifica[accion]', 'editar');
        formData.append('certifica[tipoen]', 'seguridad');

        $.ajax('libs/certifica2', {
            method: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'Correcto') {
                    form = $('<form action="libs/certifica2" method="post">' +
                        '<input type="hidden" name="certifica[accion]" value="generar">' +
                        '<input type="hidden" name="certifica[certificado]" value="' + data.certificado + '">' +
                        '<input type="hidden" name="certifica[id]" value="' + data.certificadoid + '">' +
                        '</form>');
                    form.appendTo($('body')).submit().remove();
                } else {
                    $('#fondo').remove();
                    $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
                    $('#fondo').append("<div class='rp' id='rp2'><span>Ha ocurrido un error, intentelo de nuevo más tarde.</span></div>");
                    setTimeout(function() {
                        $('#fondo').fadeIn('fast', function() {
                            $('#rp2').animate({
                                'top': '50%'
                            }, 50).fadeIn();
                        });
                    }, 400);
                    setTimeout(function() {
                        $("#rp2").fadeOut();
                        $('#fondo').fadeOut('fast');
                    }, 3000);
                }
            },
            error: function() {
                console.log('error subiendo');
            }
        });
    });

    function reset() {
        $('input', '#ed_certificado').not('[type=submit], [type=hidden], [readonly]').val('');
        $('textarea').val('');
        formData = new FormData();
        $.each($('.img-prev'), function() {
            $(this).removeAttr('src');
        });
        $.each($('.img-prev2'), function() {
            $(this).removeAttr('src');
        });
    }

});