$(document).ready(function(){

	$("#logueo").submit(function(e){
		e.preventDefault();
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">'+
					        '<div class="loader-inner ball-beat">'+
					         '<div></div>'+
					          '<div></div>'+
					          '<div></div>'+
					       '</div>'+
					      '</div>');
		setTimeout(function() {
        	$('#fondo').fadeIn('fast');
        }, 400);

		data = $(this).serializeArray();
		$.ajax({
			data: data,
			type: 'POST',
			dataType: 'json',
			url: 'libs/logueo.php',
			success: function(data)
			{
				if(data == 'error')
					{
		    			$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp2'><span>El usuario y/o contraseña no son correctos</span></div>");
						setTimeout(function() {
				        	$('#fondo').fadeIn('fast',function(){
				            $('#rp2').animate({'top':'50%'},50).fadeIn();
				         	});
				        }, 400);
				        setTimeout(function() {
				            $("#rp2").fadeOut();
				            $('#fondo').fadeOut('fast');
				        }, 3000);
					}
				else
					{
		    			$('#fondo').remove();
						$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
						$('#fondo').append("<div class='rp' id='rp'><span>Ingresando. . . </span></div>");
						setTimeout(function() {
				        	$('#fondo').fadeIn('fast',function(){
				            $('#rp').animate({'top':'50%'},50).fadeIn();
				         	});
				        }, 400);
				        setTimeout(function() {
				            $("#rp").fadeOut();
				            $('#fondo').fadeOut('fast');
				        }, 2000);
						setTimeout(function() {
						window.location.href = data['redirec'];
						}, 2500);
					}
			}
		});
	});

});