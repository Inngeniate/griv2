<?php
require "libs/conexion.php";
@$informacion = array();
$data         = $_REQUEST['registro'];

@$carpeta_firmas = '../alturas';

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

$inicio = $data['inicio'];
$fin    = $data['final'];

require_once 'libs/tcpdf.php';

$registros = $db
    ->where('fecha_al', array($inicio, $fin), 'BETWEEN')
    ->objectBuilder()->get('registros_espacios_confinados');

if ($db->count > 0) {
    class MYPDF extends TCPDF
    {
        public function Header()
        {

            $estilo = '<style>
                        table{
                            font-size: 11px;
                            font-family: arial;
                        }
                        .tmax{
                            font-size: 12px;
                        }
                        .tmin2{
                            font-size: 11px;
                        }
                    </style>';

            $bMargin         = $this->getBreakMargin();
            $auto_page_break = $this->AutoPageBreak;
            $this->SetAutoPageBreak(false, 0);
            $this->SetAutoPageBreak($auto_page_break, $bMargin);
            $this->setPageMark();

            $head = '<table cellspacing="0" cellpadding="0" border="0" class="certificado1">
                                <tr>
                                    <td height="20"><br><br><img src="images/logo.png" width="120px"><br></td>
                                </tr>
                                <tr>
                                    <td class="tmax"><b>REGISTRO DE ASISTENCIA FORMACION TRABAJO EN ESPACIOS CONFINADOS</b></td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="99%">
                                <tr>
                                    <td colspan="3"></td>
                                    <td class="tmax" align="right"><br><br><br>FED 012 <br></td>
                                </tr>
                            </table>';

            $this->writeHTML($estilo . $head, true, false, false, false, '');
        }
    }

    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
    $pdf->setPageOrientation('l');

    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // PDF_MARGIN_TOP
    $pdf->SetMargins(15, 50, 15);
    // $pdf->SetPrintHeader(false);
    // $pdf->setPrintFooter(false);

    // set auto page breaks
    // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->SetAutoPageBreak(true, 45);

    // set image scale factor
    // $pdf->setImageScale(1.53);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once dirname(__FILE__) . '/lang/eng.php';
        $pdf->setLanguageArray($l);
    }

    $pdf->AddPage();
    $arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 11);

    $estilo = '<style>
                    table{
                        font-size: 11px;
                        font-family: arial;
                    }
                    .tmax{
                        font-size: 12px;
                    }
                    .tmin2{
                        font-size: 11px;
                    }
                    .gris{
                        background-color: #dadada;
                    }
                </style>';

    $html = '<table cellspacing="0" cellpadding="5" border="1" class="certificado1" width="99%">
                <tr>
                    <td class="gris">LUGAR:</td>
                    <td></td>
                    <td class="gris">FECHA:</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="gris">NIVEL DE ENTRENAMIENTO:</td>
                    <td></td>
                    <td class="gris">INTENSIDAD HORARIA:</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="gris">ENTRENADOR:</td>
                    <td></td>
                    <td class="gris">SUPERVISOR DE FORMACIÓN:</td>
                    <td></td>
                </tr>
            </table>';

    $html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
                <tr>
                    <td align="center" height="30"></td>
                </tr>
            </table>';

    $html .= '<table cellspacing="0" cellpadding="5" border="1" class="certificado1" width="99%">
                <tr>
                    <td align="center" width="40" class="gris">ITEM</td>
                    <td align="center" width="250" class="gris">NOMBRE</td>
                    <td align="center" class="gris">CEDULA</td>
                    <td align="center" class="gris">EMPRESA</td>
                    <td align="center" class="gris">TELEFONO</td>
                    <td align="center" class="gris">FIRMA DEL ESTUDIANTE</td>
                </tr>';

    $item = 1;

    foreach ($registros as $rg) {
        $firma = '';
        if ($rg->firma_al != '' && file_exists($rg->firma_al)) {
            $firma = '<img src="' . $rg->firma_al . '">';
        }
        $html .= '<tr>
                    <td align="center"><br><br>' . $item . '</td>
                    <td><br><br>' . $rg->nombre_primero_al . ' ' . $rg->nombre_segundo_al . ' ' . $rg->apellidos_al . '</td>
                    <td><br><br>' . $rg->documento_al . '</td>
                    <td><br><br>' . $rg->empresa_al . '</td>
                    <td><br><br>' . $rg->telefono_al . '</td>
                    <td>' . $firma . '</td>
                </tr>';

        $item++;
    }

    $html .= '</table>';

    $html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
                <tr>
                    <td align="center" height="40"></td>
                </tr>
            </table>';

    $html .= '<table cellspacing="0" cellpadding="5" border="0" width="99%">
                <tr>
                    <td>Coordinador TSA: _______________________________________</td>
                </tr>
            </table>';

    $pdf->writeHTML($estilo . $html, true, false, false, false, '');

    $pdf->Output('Registro_de_asistencia.pdf', 'I');
} else {
    echo 'No se encontraron resultados';
}
