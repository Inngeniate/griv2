<?php
require "conexion.php";
@$informacion = array();
@$data        = $_REQUEST['usuario'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

switch ($data['opc']) {
    case 'nuevo_usuario':
        $bus = $db
            ->where('login', $data['correo'])
            ->objectBuilder()->get('usuarios');

        if ($db->count == 0) {
            $pass  = crypt_blowfish_bycarluys($data['password']);

            $datos = [
                'nombre' => $data['nombre'],
                'apellido' => $data['apellido'],
                'login' => $data['correo'],
                'password' => $pass,
                'tipo' => 'empleado',
                'estado'  => 1,
                'eliminado' => 0
            ];

            $nuevo = $db
                ->insert('usuarios', $datos);

            if ($nuevo) {
                $menu = $db
                    ->objectBuilder()->get('menu');

                foreach ($menu as $item) {
                    $permiso = 0;

                    if (isset($data['permiso'][$item->Id_m])) {
                        $permiso = 1;
                    }

                    $datos = [
                        'usuario_up' => $nuevo,
                        'modulo_up' => $item->Id_m,
                        'permiso_up' => $permiso
                    ];

                    $permisos = $db
                        ->insert('usuarios_permisos', $datos);
                }

                $informacion['status'] = 'Correcto';
            } else {
                $informacion['status'] = 'Error';
            }
        } else {
            $informacion['status'] = 'Error2';
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        session_start();
        @$buscar_nom = $data['nom'];

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        $list = $db
            ->where('nombre', '%' . $buscar_nom . '%', 'LIKE')
            ->where('tipo', 'administrador', '!=')
            ->where('eliminado', 0)
            ->objectBuilder()->get('usuarios');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($_POST['pagina']) ? $_POST['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $filas     = '';
        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $btn_eliminar = '';

        if ($_SESSION['usutipoggri'] == 'administrador') {
            $btn_eliminar = '<a href="javascript:void(0)" class="btn-eliminar"><span class="icon-bin"></span></a>';
        }

        $registros = $db
            ->where('nombre', '%' . $buscar_nom . '%', 'LIKE')
            ->where('tipo', 'administrador', '!=')
            ->where('eliminado', 0)
            ->orderBy('nombre', 'ASC')
            ->objectBuilder()->paginate('usuarios', $pagina);

        if ($total > 0) {
            foreach ($registros as $registro) {
                $filas .= '<tr id="' . $registro->id . '">
                                <td><p>' . $registro->nombre . '</p></td>
                                <td><p>' . $registro->apellido . '</p></td>
                                <td><p>' . $registro->login . '</p></td>
                                <td><p>' . ($registro->estado == 1 ? "Activo" : "Inactivo") . '</p></td>
                                <td align="center"><a href="usuarios_edt?usuario=' . $registro->id . '" target="_blank" title="Editar" class="editar"><span class="icon-editar"></span></a>' . $btn_eliminar . '</td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;

        echo json_encode($informacion);
        break;
    case 'editar_usuario':
        $sql = '';

        $comprobar = $db
            ->where('login', $data['correo'])
            ->where('id', $data['idusuario'], '!=')
            ->objectBuilder()->get('usuarios');

        if ($db->count > 0) {
            $informacion['status'] = 'Error2';
        } else {
            $datos = [
                'nombre' => $data['nombre'],
                'apellido' => $data['apellido'],
                'login' => $data['correo'],
                'estado' => $data['estado']
            ];

            if ($data['password'] != '') {
                $pass = crypt_blowfish_bycarluys($data['password']);
                $datos['password'] = $pass;
            }

            $act = $db
                ->where('id', $data['idusuario'])
                ->update('usuarios', $datos);

            if ($act) {
                $permisos = $db
                    ->where('usuario_up', $data['idusuario'])
                    ->delete('usuarios_permisos');

                $menu = $db
                    ->objectBuilder()->get('menu');

                foreach ($menu as $item) {
                    $permiso = 0;

                    if (isset($data['permiso'][$item->Id_m])) {
                        $permiso = 1;
                    }

                    $datos = [
                        'usuario_up' => $data['idusuario'],
                        'modulo_up' => $item->Id_m,
                        'permiso_up' => $permiso
                    ];

                    $permisos = $db
                        ->insert('usuarios_permisos', $datos);
                }

                $informacion['status'] = 'Correcto';
            } else {
                $informacion['status'] = 'Error';
            }
        }

        echo json_encode($informacion);
        break;
    case 'eliminar_usuario':
        $act = $db
            ->where('id', $data['idusuario'])
            ->update('usuarios', ['eliminado' => 1]);

        if ($act) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'listar_eliminados':
        @$buscar_nom = $data['nom'];

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        $list = $db
            ->where('nombre', '%' . $buscar_nom . '%', 'LIKE')
            ->where('eliminado', 1)
            ->objectBuilder()->get('usuarios');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($_POST['pagina']) ? $_POST['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $filas     = '';
        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('nombre', '%' . $buscar_nom . '%', 'LIKE')
            ->where('eliminado', 1)
            ->orderBy('nombre', 'ASC')
            ->objectBuilder()->paginate('usuarios', $pagina);

        if ($total > 0) {
            foreach ($registros as $registro) {
                $filas .= '<tr id="' . $registro->id . '">
                                <td><p>' . $registro->nombre . '</p></td>
                                <td><p>' . $registro->apellido . '</p></td>
                                <td><p>' . $registro->login . '</p></td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;

        echo json_encode($informacion);
        break;
}

function crypt_blowfish_bycarluys($password, $digito = 7)
{
    $set_salt = './1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $salt     = sprintf('$2a$%02d$', $digito);
    for ($i = 0; $i < 22; $i++) {
        $salt .= $set_salt[mt_rand(0, 63)];
    }
    return crypt($password, $salt);
}
