<?php
require("conexion.php");

$data = $_POST['evaluacion'];

switch ($data['accion']) {
	case 'comprobar_evaluacion':
		$identifica = str_replace('.', '', $data['identificacion']);
		$identifica = str_replace(',', '', $identifica);
		$identifica = (int)$identifica;

		$comprobar = $db
			->where('identificacion_ev', $identifica)
			->objectBuilder()->get('evaluaciones_tsa');

		if ($db->count == 0) {
			$informacion['status'] = true;
			$informacion['identificacion'] = $identifica;
			$informacion['form_evaluacion'] = '<form id="registro-evaluacion">
												<div class="Registro">
													<p><strong>SELECCIÓN MÚLTIPLE CON ÚNICA RESPUESTA</strong></p>
													<hr>
													<label>1) Se define como la combinación de la probabilidad y las consecuencias a:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev1]" value="a" required>a) Accidente de trabajo <br>
														<input type="radio" name="evaluacion[ev1]" value="b">b) Riesgo <br>
														<input type="radio" name="evaluacion[ev1]" value="c">c) Peligro <br>
														<input type="radio" name="evaluacion[ev1]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>2) Se define como la fuente, situación o acto con potencial de daño a:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev2]" value="a" required>a) Los riesgos <br>
														<input type="radio" name="evaluacion[ev2]" value="b">b) Los actos inseguros <br>
														<input type="radio" name="evaluacion[ev2]" value="c">c) Peligro <br>
														<input type="radio" name="evaluacion[ev2]" value="d">d) Condición insegura <br>
													</div>
													<br>
													<label>3) La resolución conocida como el “reglamento de seguridad para protección contra caídas en trabajo en alturas” es:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev3]" value="a" required>a) Resolución 3673/2009 <br>
														<input type="radio" name="evaluacion[ev3]" value="b">b) Resolución 1409/2012 <br>
														<input type="radio" name="evaluacion[ev3]" value="c">c) Resolución 070/2010 <br>
														<input type="radio" name="evaluacion[ev3]" value="d">d) Resolución 1223/2014 <br>
													</div>
													<br>
													<label>4) Se define como la obligación que recae sobre una persona de reparar el daño que ha causado a otro en un equivalente monetario o un bien:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev4]" value="a" required>a) Responsabilidad civil <br>
														<input type="radio" name="evaluacion[ev4]" value="b">b) Responsabilidad administrativa <br>
														<input type="radio" name="evaluacion[ev4]" value="c">c) Responsabilidad solidaria <br>
														<input type="radio" name="evaluacion[ev4]" value="d">d) Responsabilidad penal <br>
													</div>
													<br>
													<label>5) Equipo cuya función es adsorber la fuerza de impacto en el cuerpo del trabajador y el punto de anclaje:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev5]" value="a" required>a) Eslinga en Y <br>
														<input type="radio" name="evaluacion[ev5]" value="b">b) Polipasto de 4 en 1 <br>
														<input type="radio" name="evaluacion[ev5]" value="c">c) Adsorvedor de choque <br>
														<input type="radio" name="evaluacion[ev5]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>6) Según la legislación anclaje es:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev6]" value="a" required>a) Un ancla que va desde el arnes a la línea de vida <br>
														<input type="radio" name="evaluacion[ev6]" value="b">b) Punto seguro en el que se puede conectar un equipo personal de protección contra caídas con resistencia mínima de 5000lbs (2272kg), por cada dos personas conectadas. <br>
														<input type="radio" name="evaluacion[ev6]" value="c">c) Punto seguro al que se puede conectar un equipo de protección contra caídas con una resistencia mínima de 5000lbs por cada persona conectada <br>
														<input type="radio" name="evaluacion[ev6]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>7) Distancia vertical total requerida para detener una caída incluyen la distancia de caída libre, desaceleración y de activación:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev7]" value="a" required>a) Distancia vertical libre <br>
														<input type="radio" name="evaluacion[ev7]" value="b">b) Distancia mayor <br>
														<input type="radio" name="evaluacion[ev7]" value="c">c) Distancia de detención <br>
														<input type="radio" name="evaluacion[ev7]" value="d">d) Todas las anteriores <br>
													</div>
													<br>
													<label>8) Según legislación distancia de caída libre es:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev8]" value="a" required>a) Cuando se cae el trabajador sobre la superficie del suelo <br>
														<input type="radio" name="evaluacion[ev8]" value="b">b) Desde el punto en donde quedan izados los pies del trabajador hasta la superficie del suelo <br>
														<input type="radio" name="evaluacion[ev8]" value="c">c) Desde el momento en quien se suspende para trabajar <br>
														<input type="radio" name="evaluacion[ev8]" value="d">d) Va desde el inicio de la caída hasta cuando se detiene o comienza a activarse el absorbente de choque <br>
													</div>
													<br>
													<label>9) Los controles que se implementan sobre los peligros se realizan en su respectivo orden así:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev9]" value="a" required>a) En el trabajador, fuente y el medio <br>
														<input type="radio" name="evaluacion[ev9]" value="b">b) La fuente, el medio, el individuo <br>
														<input type="radio" name="evaluacion[ev9]" value="c">c) En las maquinas, las personas, el medio <br>
														<input type="radio" name="evaluacion[ev9]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>10) Son instrumentos de identificación de peligros:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev10]" value="a" required>a) El procedimiento de trabajo <br>
														<input type="radio" name="evaluacion[ev10]" value="b">b) La elaboración de ATS (análisis de trabajo seguro en alturas) y permiso de trabajo en alturas <br>
														<input type="radio" name="evaluacion[ev10]" value="c">c) Elementos de protección personal y herramientas de trabajo <br>
														<input type="radio" name="evaluacion[ev10]" value="d">d) Realizar el programa de protección contra caídas <br>
													</div>
													<br>
													<label>11) La delimitación del área, señalización, utilización e barandas, control de accesos, manejo de desniveles u y orificios, inspector de seguridad hace  parte de:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev11]" value="a" required>a) Medidas restrictivas <br>
														<input type="radio" name="evaluacion[ev11]" value="b">b) Medidas colecticas de prevención <br>
														<input type="radio" name="evaluacion[ev11]" value="c">c) Medidas de protección <br>
														<input type="radio" name="evaluacion[ev11]" value="d">d) Medidas protectivas <br>
													</div>
													<br>
													<label>12) Según la legislación vigente los cascos que se deben utilizar son:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev12]" value="a" required>a) Clase m: cascos con barboquejo de 4 puntos de fijación <br>
														<input type="radio" name="evaluacion[ev12]" value="b">b) Clase b: aislamiento eléctrico, contra impactos, contra penetración e inflamabilidad así como barboquejo mínimo tres puntos de fijación <br>
														<input type="radio" name="evaluacion[ev12]" value="c">c) Clase X: para rescate y auto rescate <br>
														<input type="radio" name="evaluacion[ev12]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>13) Las gafas de seguridad deben estar certificadas bajo el estándar de:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev13]" value="a" required>a) ANSI Z359.1 <br>
														<input type="radio" name="evaluacion[ev13]" value="b">b) ANSI Z1486 <br>
														<input type="radio" name="evaluacion[ev13]" value="c">c) ANSI Z87 <br>
														<input type="radio" name="evaluacion[ev13]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>14) La inspección de los elementos de protección personal debe realizarse por el trabajador:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev14]" value="a" required>a) Cada año <br>
														<input type="radio" name="evaluacion[ev14]" value="b">b) Cada dos meses <br>
														<input type="radio" name="evaluacion[ev14]" value="c">c) Antes de ir al trabajo <br>
														<input type="radio" name="evaluacion[ev14]" value="d">d) Diario antes de iniciar la actividad laboral <br>
													</div>
													<br>
													<label>15) Los riesgos asociados al trabajo en alturas son:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev15]" value="a" required>a) Caídas, electrocución, atrapamiento, deshidratación, golpes, lumbalgias, etc. <br>
														<input type="radio" name="evaluacion[ev15]" value="b">b) No existen riesgos <br>
														<input type="radio" name="evaluacion[ev15]" value="c">c) Físicos, ergonómicos, caseros <br>
														<input type="radio" name="evaluacion[ev15]" value="d">d) Asfixia e intoxicación <br>
													</div>
													<br>
													<label>16) El procedimiento de rescate debe ser:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev16]" value="a" required>a) Escrito por el jefe de recursos humanos y el gerente <br>
														<input type="radio" name="evaluacion[ev16]" value="b">b) No es necesario para la activación del rescate <br>
														<input type="radio" name="evaluacion[ev16]" value="c">c) Documento escrito por personal competente, socializado con los trabajadores, así como la realización de simulacros periódicos para medir su efectividad <br>
														<input type="radio" name="evaluacion[ev16]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>17) Según la legislación vigente son equipos de la protección activa:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev17]" value="a" required>a) Redes de seguridad <br>
														<input type="radio" name="evaluacion[ev17]" value="b">b) Arnes, puntos de anclaje, mecanismos de anclaje, todos los accesorios que sean manipulados por el trabajador <br>
														<input type="radio" name="evaluacion[ev17]" value="c">c) Mosquetones y eslingas <br>
														<input type="radio" name="evaluacion[ev17]" value="d">d) Camión grua, canasta y escalera <br>
													</div>
													<br>
													<label>18) Son las que involucran la participación del trabajador. Incluyen los siguientes componentes; punto de anclaje, mecanismos de anclaje, conectores, soporte corporal y plan de rescate:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev18]" value="a" required>a) Protección pasiva <br>
														<input type="radio" name="evaluacion[ev18]" value="b">b) Protección necesaria <br>
														<input type="radio" name="evaluacion[ev18]" value="c">c) Protección activa <br>
														<input type="radio" name="evaluacion[ev18]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>19) Están diseñadas para detener o capturar al trabajador en el trayecto de su caia, sin permitir impacto contra estructuras o elementos, requieren poca o ninguna intervención del trabajador:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev19]" value="a" required>a) Protección general <br>
														<input type="radio" name="evaluacion[ev19]" value="b">b) Protección activa <br>
														<input type="radio" name="evaluacion[ev19]" value="c">c) Protección pasiva <br>
														<input type="radio" name="evaluacion[ev19]" value="d">d) Todas las anteriores <br>
													</div>
													<br>
													<label>20) El punto de anclaje, mecanismo de anclaje, líneas de vida, conectores y protección corporal se conoce como los:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev20]" value="a" required>a) Sistemas de protección pasiva <br>
														<input type="radio" name="evaluacion[ev20]" value="b">b) Componentes del sistema de protección contra caídas. (protección activa) <br>
														<input type="radio" name="evaluacion[ev20]" value="c">c) Sistema de trabajo inactivo <br>
														<input type="radio" name="evaluacion[ev20]" value="d">d) Componentes pasivos <br>
													</div>
													<br>
													<label>21) Para garantizar la estabilidad de los andamios debemos cumplir la regla de:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev21]" value="a" required>a) Estabilidad superior <br>
														<input type="radio" name="evaluacion[ev21]" value="b">b) Del 5:1 <br>
														<input type="radio" name="evaluacion[ev21]" value="c">c) Aseguramiento inferior <br>
														<input type="radio" name="evaluacion[ev21]" value="d">d) Del 4:1 <br>
													</div>
													<br>
													<label>22) En los trabajo de suspensión se debe garantizar por seguridad y comodidad el uso de:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev22]" value="a" required>a) Audífonos <br>
														<input type="radio" name="evaluacion[ev22]" value="b">b) Sillas reclinables <br>
														<input type="radio" name="evaluacion[ev22]" value="c">c) Sillas de suspensión <br>
														<input type="radio" name="evaluacion[ev22]" value="d">d) Todas las anteriores <br>
													</div>
													<br>
													<label>23) El trabajador designado por el empleador, capaz de identificar peligros en el sitio de trabajo en alturas, relacionados con el ambiente o condiciones de trabajo y que tiene su autorización para aplicar medidas correctivas inmediatas es:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev23]" value="a" required>a) Jefe de área <br>
														<input type="radio" name="evaluacion[ev23]" value="b">b) Gerente del proyecto <br>
														<input type="radio" name="evaluacion[ev23]" value="c">c) Entrenador <br>
														<input type="radio" name="evaluacion[ev23]" value="d">d) Coordinador de trabajo en alturas <br>
													</div>
													<br>
													<label>24) Son sistemas de protección pasiva:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev24]" value="a" required>a) Redes de seguridad <br>
														<input type="radio" name="evaluacion[ev24]" value="b">b) Arnés de seguridad y líneas de vida <br>
														<input type="radio" name="evaluacion[ev24]" value="c">c) Equipos de protección personal <br>
														<input type="radio" name="evaluacion[ev24]" value="d">d) Todas las anteriores <br>
													</div>
													<br>
													<label>25) La norma que certifica los equipos de protección contra caídas es:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev25]" value="a" required>a) Decreto ley 1295 <br>
														<input type="radio" name="evaluacion[ev25]" value="b">b) ANSI Z359.1 <br>
														<input type="radio" name="evaluacion[ev25]" value="c">c) IRAM 320 <br>
														<input type="radio" name="evaluacion[ev25]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>26) A partir de que altura se determina que es trabajo en alturas de acuerdo a la Res. 1409 de 2012:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev26]" value="a" required>a) 1 metro <br>
														<input type="radio" name="evaluacion[ev26]" value="b">b) 2 metros <br>
														<input type="radio" name="evaluacion[ev26]" value="c">c) 1.50 metros <br>
														<input type="radio" name="evaluacion[ev26]" value="d">d) 1.80 metros <br>
													</div>
													<br>
													<label>27) Mencione los niveles de capacitación en alturas según resolución 1409 de 2012:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev27]" value="a" required>a) Avanzado, medio, básico <br>
														<input type="radio" name="evaluacion[ev27]" value="b">b) Avanzado, básico operativo, administrativo para jefes de área (virtual y presencial), coordinador, reentrenamiento en nivel avanzado, entrenador <br>
														<input type="radio" name="evaluacion[ev27]" value="c">c) Básico, reentrenamiento en nivel avanzado, avanzado, coordinador, entrenador <br>
														<input type="radio" name="evaluacion[ev27]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>28) Qué tipo de arenes es el más recomendable para trabajo en alturas:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev28]" value="a" required>a) Arnés de cuerpo entero tipo H con cuatro argollas <br>
														<input type="radio" name="evaluacion[ev28]" value="b">b) Arnés pélvico con tres argollas <br>
														<input type="radio" name="evaluacion[ev28]" value="c">c) Arnés multipropósito <br>
														<input type="radio" name="evaluacion[ev28]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>29) Cuál es la vigencia de los equipos de protección contra caídas:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev29]" value="a" required>a) 10 años <br>
														<input type="radio" name="evaluacion[ev29]" value="b">b) 4 meses <br>
														<input type="radio" name="evaluacion[ev29]" value="c">c) Lo determina la inspección (diaria, anual, semestral) y/o fabricante <br>
														<input type="radio" name="evaluacion[ev29]" value="d">d) Ninguna de las anteriores <br>
													</div>
													<br>
													<label>30) Que resistencia como mínimo deben tener los mosquetones para trabajo en alturas:</label>
													<div class="eval-respuesta">
														<input type="radio" name="evaluacion[ev30]" value="a" required>a) 5000 libras <br>
														<input type="radio" name="evaluacion[ev30]" value="b">b) 22.2 kilonewtons <br>
														<input type="radio" name="evaluacion[ev30]" value="c">c) 2272kg <br>
														<input type="radio" name="evaluacion[ev30]" value="d">d) Todas las anteriores <br>
													</div>
													<br><br>
													<input type="hidden" name="evaluacion[identificacion]" id="identifica">
													<input type="submit" value="Guardar Registro">
												</div>
											</form>';
		} else {
			$informacion['status'] = false;
			$informacion['error'] = 'Ya ha realizado esta evaluación.';
		}

		echo json_encode($informacion);
		break;
	case 'nuevo_evaluacion':
		date_default_timezone_set("America/Bogota");
		$fecha = date('Y-m-j');

		$identifica = str_replace('.', '', $data['identificacion']);
		$identifica = str_replace(',', '', $identifica);
		$identifica = (int)$identifica;

		$datos = [
			'identificacion_ev' => $identifica,
			'fecha_ev' => $fecha,
			'pregunta1_ev' => $data['ev1'],
			'pregunta2_ev' => $data['ev2'],
			'pregunta3_ev' => $data['ev3'],
			'pregunta4_ev' => $data['ev4'],
			'pregunta5_ev' => $data['ev5'],
			'pregunta6_ev' => $data['ev6'],
			'pregunta7_ev' => $data['ev7'],
			'pregunta8_ev' => $data['ev8'],
			'pregunta9_ev' => $data['ev9'],
			'pregunta10_ev' => $data['ev10'],
			'pregunta11_ev' => $data['ev11'],
			'pregunta12_ev' => $data['ev12'],
			'pregunta13_ev' => $data['ev13'],
			'pregunta14_ev' => $data['ev14'],
			'pregunta15_ev' => $data['ev15'],
			'pregunta16_ev' => $data['ev16'],
			'pregunta17_ev' => $data['ev17'],
			'pregunta18_ev' => $data['ev18'],
			'pregunta19_ev' => $data['ev19'],
			'pregunta20_ev' => $data['ev20'],
			'pregunta21_ev' => $data['ev21'],
			'pregunta22_ev' => $data['ev22'],
			'pregunta23_ev' => $data['ev23'],
			'pregunta24_ev' => $data['ev24'],
			'pregunta25_ev' => $data['ev25'],
			'pregunta26_ev' => $data['ev26'],
			'pregunta27_ev' => $data['ev27'],
			'pregunta28_ev' => $data['ev28'],
			'pregunta29_ev' => $data['ev29'],
			'pregunta30_ev' => $data['ev30'],
		];

		$evaluar = $db
			->insert('evaluaciones_tsa', $datos);

		if ($evaluar) {
			$informacion['status'] = true;
		} else {
			$informacion['status'] = false;
			$informacion['error'] = 'Se produjo un error, intentalo de nuevo.';
		}

		echo json_encode($informacion);
		break;
}
