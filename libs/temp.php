<?php
	require_once('tcpdf.php');
	class MYPDF extends TCPDF {
        public function Header() {
		    switch($this->page)
			{
				case 1:
				 	$bMargin = $this->getBreakMargin();
		            $auto_page_break = $this->AutoPageBreak;
		            $this->SetAutoPageBreak(false, 0);
		            // $image_file = '../images/marca_agua.jpg';
		            // $this->Image($image_file, 0, 0, 280, 216, '', '', '', false, 300, '', false, false, 0);
		            $this->SetAutoPageBreak($auto_page_break, $bMargin);
		            $this->setPageMark();
					$this->SetMargins(5, 5, 5, true);
					break;
			}
        }
    }

     // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LEGAL', true, 'UTF-8', false);
    $pdf->setPageOrientation('P');

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // PDF_MARGIN_TOP
    $pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);

    // $pdf->SetPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set auto page breaks
    // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->SetAutoPageBreak(TRUE, 0);

    // set image scale factor
    // $pdf->setImageScale(1.53);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

     $pdf->AddPage();

    // $pdf->Write(13, '', '', 0, 'L', true, 0, false, false, 0);
    $calibri = $pdf->addTTFfont('fonts/calibri.ttf', 'TrueTypeUnicode', '', 32);
    $algerian = $pdf->addTTFfont('fonts/algerian.php');

    $estilo = '<style>
					table{
						font-size: 8px;
						font-weight : bold;
					}
					.certificado1{
						text-align: center;
					}
					.certificado1 img{
						position: relative;
						top : 10px;
					}
					.encab1{
						color: rgb(10, 61, 93);
						font-size: 12px;
					}
					.encab2{
						color: #fff;
						font-size: 10px;
						background-color: rgb(10, 61, 93);
					}
					.encab4{
						color: rgb(10, 61, 93);
						font-size: 10px;
					}
					.encab3{
						font-size: 10px;
					}
					.biz{
						border-left: 1px solid #000;
					}
					.bde{
						border-right: 1px solid #000;
					}
					.bar{
						border-top: 1px solid #000;
					}
					.bab{
						border-bottom: 1px solid #000;
					}
					.amarillo {
						background-color: #ff0;
					}
					.rojo {
						color: #fe0707;
					}
					.nobr{
						border-top: 0px solid #fff;
						border-bottom: 0px solid #fff;
						border-right: 1px solid #000;
						border-left: 0px solid #fff;
					}
				</style>';

	// $pdf->StartTransform();

	// $pdf->SetXY(15, 120);
	// $pdf->Rotate(90);
	// // $pdf->Cell(0,0,'Cabina Vehicular',1,1,'L',0,'');
	// $pdf->write(2,'Cabina Vehicular');
	// $pdf->StopTransform();

	$html = '<table cellspacing="0" cellpadding="0" border="1" class="certificado1" width="730">
   				<tr>
					<td rowspan="5" height="60"><br><br><img src="../images/logo_final.jpg" width="150px"></td>
					<td rowspan="4" width="245" class="encab1"><br><br><b>INSPECCION Y PREALISTAMIENTO<br>GENERAL VEHICULO TRACTO CAMION</b></td>
					<td width="150" class="encab2">Vigencia:</td>
					<td width="150" class="encab2">23-feb-17</td>
				</tr>
				<tr>
					<td class="encab3">Código:</td>
					<td class="encab3">GM-FT-007</td>
				</tr>
				<tr>
					<td class="encab2">Versión:</td>
					<td class="encab2">1</td>
				</tr>
				<tr>
					<td class="encab3">Pagina:</td>
					<td class="encab3">1 de 1</td>
				</tr>
				<tr>
					<td colspan="3" class="encab2" style="font-size:2px"></td>
				</tr>
				<tr>
					<td colspan="4"><div style="font-size:10px"> </div></td>
				</tr>
   			</table>';

   	$html .=  '<table cellspacing="0" cellpadding="3" border="1" width="730">
   				<tr>
   					<td width="90">Fecha de Inspección</td>
   					<td></td>
   					<td>Placa Vehículo</td>
   					<td width="70"></td>
   					<td>Placa Tráiler</td>
   					<td width="70"></td>
   					<td>Tipo de Vehículo</td>
   					<td width="132.5"></td>
   				</tr>
    		</table>';

    $html .=  '<table cellspacing="0" cellpadding="3" border="1">
   				<tr>
   					<td width="105">Nombre Conductor</td>
   					<td colspan="2"></td>
   					<td width="30">C.C.:</td>
   					<td></td>
   					<td width="160">Nombre del propietario/empresa</td>
   					<td colspan="2" width="172.5"></td>
   				</tr>
   				<tr>
   					<td colspan="8"><p>A continuación se verificará si el vehículo cuenta con los siguientes elementos así como su estado: Marque con una "X" si el estado es Bueno (B) o Malo (M). Si algún elemento No Aplica (NA) trazar una línea horizontal  en el estado (--) e identificarlo como un No aplica en el campo observaciones y/o recomendaciones:</p></td>
   				</tr>
   				<tr>
					<td colspan="8"><div style="font-size:2px"> </div></td>
				</tr>
    		</table>';

	$html .=  '<table cellspacing="0" cellpadding="3" border="1">
				<tr>
					<td class="encab2" colspan="2" rowspan="2" align="center" width="185">1. Zona y/o elemento del vehículo a inspeccionar</td>
					<td class="encab2" colspan="2" align="center" width="60">Estado</td>
					<td class="encab2" rowspan="2" align="center" width="220">Observaciones y/o recomendaciones</td>
					<td rowspan="2" width="18" class="nobr"></td>
					<td class="encab2" rowspan="2" align="center" width="185">2. EPP (Elementos de protección personal)</td>
					<td class="encab2" colspan="2" align="center" width="60">Estado</td>
				</tr>
				<tr>
					<td class="encab2" align="center">B</td>
					<td class="encab2" align="center">M</td>
					<td class="encab2" align="center">B</td>
					<td class="encab2" align="center">M</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Estado de la cabina, limpio y ordenado</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>Casco de seguridad</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Cinturón de seguridad y apoya cabezas</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>Protector auditivo</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Cabina limpia y en orden</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>Gafas de seguridad</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Silla suficientemente alta, sin superficies duras o salientes</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>Mascarilla</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Seguridad en puertas</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>Chaleco Reflectivo</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Vidrio Panorámico y limpia Brisas</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>Botas de seguridad</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Sistema de luces (altas, medias, bajas)*</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>Guantes (Neopreno o Vaqueta según aplique)</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Sistema de luces traseras Stop *</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3" rowspan="2">Mencione si falta algún EPP:</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Sistema de luces direccionales</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Sistema de frenos</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Luces de Freno</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td class="encab2">3. Se practicó prueba de Alcohol?</td>
					<td>Si_</td>
					<td>No_</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Luces Estacionarias</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3" align="center">Resultado: Positivo___     Negativo___</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Luces de reversa</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td class="encab2" align="center">4. Medio Ambiente</td>
					<td class="encab2">Si</td>
					<td class="encab2">No</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Sistema de encendido</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>El vehículo presenta fuga de Aceite?</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Sistema Airbags (bolsas de aire)</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td class="encab2">Fecha de Vencimiento Extintor 1</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Estado de la carpa* (si aplica)</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td class="encab2">Fecha de Vencimiento Extintor 2</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Estado de la carrocería</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3" class="encab2" align="center">5. El Botiquín presenta algún elemento vencido? <br>Si ___ No ___</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Estado del remolque (verifique el estado de cinchas, palos, lazos y cadenas) En caso que aplique.</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3">Mencione los elementos vencidos:</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Estado de  tornamesa, King Ping y quinta rueda</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Malla de seguridad (entre el espacio del pasajero y la carga)</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td class="encab2" colspan="3" align="center"><p>6. Transporta mercancías peligrosas?<br> Si ___ No ___</p></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Placa visible</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td class="encab2" align="center">En caso que la respuesta sea Si, responder las siguiente preguntas:</td>
					<td class="encab2">Si</td>
					<td class="encab2">No</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Estado de llantas en vehículos pesados 3 mm)</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>El conductor cuenta con curso de mercancías peligrosas?*</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>2 llantas de repuesto (gravado, presión y estado en general)</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>El conductor cuenta con la hoja de seguridad del producto a transportar?*</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Presenta el vehículo escapes de aire</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>El vehículo se encuentra debidamente señalizado y rotulado de acuerdo a la hoja de seguridad del producto?*</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Espejo lateral izquierdo, lateral derecho y retrovisores *</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td>El vehículo cuenta con Kit anti derrame?*</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Espejos convexos para puntos ciegos (vehículos pesados)</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Alarma Auditiva de reversa</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3" class="encab2" align="center">7. Descripción especifica del estado o daño encontrado</td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Reencauche de llantas (curado en frio, solo se puede usar en los ejes traseros)</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
					<td colspan="3" rowspan="4"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Caja de herramientas </p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Señalización</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
				</tr>
				<tr>
					<td width="30"></td>
					<td width="155"><p>Equipo de carretera *</p></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="nobr"></td>
				</tr>
				<tr>
					<td colspan="9"><div style="font-size:2px"> </div></td>
				</tr>
				<tr>
					<td colspan="9" align="center"><p>Si algunos de los elementos que tiene el signo (*) se encuentran en mal estado o no lo tiene, es motivo para NO APROBAR que el vehículo inspeccionado realice cargue.</p>
					<p>Aprobado para cargar (marcar con una X):  SI    _____     NO ____</p></td>
				</tr>
		</table>';

	$html .= '<table cellspacing="0" cellpadding="3" border="1" width="728">
				<tr>
					<td class="encab2" align="center"><p>ELABORADO POR</p><p>(Solo aplica cuando no lo diligencia el conductor)</p></td>
					<td class="encab2" align="center">ACEPTACION DEL CONDUCTOR</td>
				</tr>
				<tr>
					<td><br><p>Nombre:_________________________________________</p>
					<p>Firma:_______________________________________________</p><br></td>
					<td align="center"><br><p>Como conductor y persona que realiza la inspección de este vehículo, declaro bajo la gravedad de juramento que la información escrita en este documento es veras y confiable.</p>
					<p>Firma:_______________________________________________</p><br></td>
				</tr>
		</table>';

    $pdf->writeHTML($estilo.$html, true, false, false, false, '');

    $pdf->Output('certificado.pdf', 'I');
?>