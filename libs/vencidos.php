<?php

require "conexion.php";

$notifica = $db
    ->orderBy('tiempo_v', 'ASC')
    ->objectBuilder()->get('vencimientos');

$res = $db->count;

if ($res > 0) {
    if (PHP_SAPI == 'cli') {
        die('Este archivo solo se puede ver desde un navegador web');
    }

    require_once 'PHPExcel/PHPExcel.php';
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("")
        ->setLastModifiedBy("")
        ->setTitle("Registros a vencer")
        ->setSubject("Registros a vencer excel")
        ->setDescription("Registros a vencer")
        ->setKeywords("Registros a vencer")
        ->setCategory("Reporte excel");

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', '#')
        ->setCellValue('B1', 'Certificado')
        ->setCellValue('C1', 'Cliente')
        ->setCellValue('D1', 'Teléfono')
        ->setCellValue('E1', 'Vendedor')
        ->setCellValue('F1', 'Fecha Vigencia')
        ->setCellValue('G1', 'Tiempo Restante');

    $cont = 1;
    $j    = 2;

    foreach ($notifica as $fila) {
        $registro = $db
            ->where('Id', $fila->Id_rg)
            ->objectBuilder()->get('registros');

        $rreg = $registro[0];

        $curso = '';

        $certificaciones = $db
            ->where('Id_ct', $rreg->certificado)
            ->objectBuilder()->get('certificaciones');

        if ($db->count > 0) {
            $curso = $certificaciones[0]->nombre;
        }

        $vendedor = $rreg->vendedor;

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $j, $cont)
            ->setCellValue('B' . $j, $curso)
            ->setCellValue('C' . $j, $rreg->nombre_primero . ' ' . $rreg->nombre_segundo . ' ' . $rreg->apellidos)
            ->setCellValue('D' . $j, $rreg->telefono)
            ->setCellValue('E' . $j, $vendedor)
            ->setCellValue('F' . $j, $rreg->fecha_vigencia)
            ->setCellValue('G' . $j, $fila->tiempo_v . ' d');
        $j++;
        $cont++;
    }

    $estiloTituloColumnas = array(
        'font' => array(
            'name'  => 'Calibri',
            'bold'  => true,
            'size'  => 11,
            'color' => array(
                'rgb' => 'ffffff',
            ),
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
            'startcolor' => array(
                'rgb' => '6085FC',
            ),
            'endcolor'   => array(
                'argb' => '6085FC',
            ),
        ),
    );

    $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($estiloTituloColumnas);
    for ($i = 'A'; $i <= 'G'; $i++) {
        $objPHPExcel->setActiveSheetIndex(0)
            ->getColumnDimension($i)->setAutoSize(true);
    }

    $objPHPExcel->getActiveSheet()->setTitle('Registros a Vencer');

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe-Registros.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
} else {
    print_r('No hay resultados para mostrar');
}
