<?php
require("conexion.php");
ini_set('max_execution_time', 300);

$dl = $db
	->delete('vencimientos');

$bs = $db
	->where('fecha_vigencia', '0000-00-00', '!=')
	->objectBuilder()->get('registros', null, 'Id, TIMESTAMPDIFF(DAY,CURDATE(),fecha_vigencia) AS diferencia');

foreach ($bs as $rbs) {
	$i = 0;
	if ($rbs->diferencia >= 0 && $rbs->diferencia <= 20) {
		$i = 1;
	}
	if ($i == 1) {
		$datos = [
			'fecha_v' => $db->now(),
			'Id_rg' => $rbs->Id,
			'tiempo_v' => $rbs->diferencia,
		];

		$in = $db
			->insert('vencimientos', $datos);
	}
}
