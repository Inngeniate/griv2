<?php
require "conexion.php";
$informacion = array();
$data        = $_REQUEST['asistencia'];

$carpeta_firmas = '../Asistencias/';

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-d H:i:s');
$fecha = date('Y-m-d');

switch ($data['accion']) {
    case 'Comprobar':
        $bsq = mysql_query("SELECT * FROM empleados WHERE identificacion_em LIKE '$data[identifica]' AND tipo_em = '$data[tipo]' ");
        if (mysql_num_rows($bsq) > 0) {
            $rbs = mysql_fetch_object($bsq);

            $encuesta = mysql_query("SELECT * FROM asistencias_encuesta WHERE Id_em LIKE '$rbs->Id_em' AND fecha_ae = '$fecha' ");
            if (mysql_num_rows($encuesta) > 0) {
                $ren = mysql_fetch_object($encuesta);
                $rbs->Id_em = $ren->Id_em;
                $informacion['encuesta'] = true;
            } else {
                $informacion['encuesta'] = false;
            }

            $informacion['info'] = $rbs;
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg'] = 'Registro no encontrado';
        }

        echo json_encode($informacion);
        break;
    case 'Registro':
        if ($data['tipo'] == 'X') {
            $bsq = mysql_query("SELECT * FROM empleados WHERE identificacion_em = '$data[identifica]' AND tipo_em = 'X' ");
            if (mysql_num_rows($bsq) == 0) {
                $nuevo = mysql_query("INSERT INTO empleados (nombre_em, identificacion_em, cargo_em, correo_em, telefono_em, firma_em, tipo_em) VALUES ('$data[nombre]', '$data[identifica]', '', '', '$data[telefono]','', 'X') ");

                $data['idempleado'] = mysql_insert_id();
            } else {
                $act = mysql_query("UPDATE empleados SET nombre_em = '$data[nombre]', telefono_em = '$data[telefono]' WHERE Id_em ='$data[idempleado]' ");
            }
        }

        $comprobar = mysql_query("SELECT * FROM asistencias WHERE fecha_as = '$nfecha' AND Id_em = '$data[idempleado]' ");
        if (mysql_num_rows($comprobar) == 0) {
            $firma = '';

            if (isset($data['firma']) && $data['firma'] != '' && $data['firma'] != 1) {
                $imgData = base64_decode(substr($data['firma'], 22));
                $file    = $carpeta_firmas . date('YmdHis') . '.png';

                $fp = fopen($file, 'w');
                if (fwrite($fp, $imgData)) {
                    $firma = substr($file, 3);
                }
            }

            $sesiones = '';
            $externo = 0;

            if (isset($data['sesiones']) && $data['sesiones'] != '') {
                $sesiones = $data['sesiones'];
                $sesion = date('YmdHis') . '-' . $sesiones;
                $externo = 1;
            } else {
                $sesion = date('YmdHis') . '-0';
            }

            if ($data['tipo'] != 'X') {
                $asistencia = mysql_query("INSERT INTO asistencias (fecha_as, Id_em, sesion_as, tipo_as, temperatura_as, firma_as,responsable_as) VALUES ('$nfecha', '$data[idempleado]', '$sesion', '$data[tipo_temperatura]', '$data[temperatura]', '$firma', '$data[responsable]') ");

                $idasistencia = mysql_insert_id();

                if (isset($data['pregunta'])) {
                    for ($i = 0; $i < count($data['pregunta']); $i++) {
                        $n = $i + 1;

                        if ($data['pregunta'][$i] != '') {
                            $encuesta =  mysql_query("INSERT INTO asistencias_encuesta (Id_as, fecha_ae, Id_em, sesion_as, pregunta_ae, respuesta_ae) VALUES ('$idasistencia','$fecha','$data[idempleado]','$sesion','$n','{$data['pregunta'][$i]}') ");
                        }
                    }
                }
            } else {
                if (isset($data['fecha'])) {
                    $total_preguntas = 11;

                    for ($i = 0; $i < count($data['fecha']); $i++) {
                        if ($data['fecha'][$i] != '') {
                            $data['fecha'][$i] = date('Y-m-d H:i:s', strtotime($data['fecha'][$i]));

                            $asistencia = mysql_query("INSERT INTO asistencias (fecha_as, Id_em, sesion_as, tipo_as, temperatura_as, firma_as,responsable_as) VALUES ('{$data['fecha'][$i]}', '$data[idempleado]', '$sesion', 'E', '', '', '0') ");

                            $asistencia = mysql_query("INSERT INTO asistencias (fecha_as, Id_em, sesion_as, tipo_as, temperatura_as, firma_as,responsable_as) VALUES ('{$data['fecha'][$i]}', '$data[idempleado]', '$sesion', 'S', '', '', '0') ");

                            if ($asistencia) {
                                $idasistencia = mysql_insert_id();

                                for ($j = 0; $j < $total_preguntas; $j++) {
                                    $n = $j + 1;

                                    $ingreso = mysql_query("INSERT INTO asistencias_encuesta (Id_as, sesion_as, fecha_ae, Id_em, pregunta_ae, respuesta_ae) VALUES ('$idasistencia', '$sesion', '{$data['fecha'][$i]}', '$data[idempleado]', '$n','') ");
                                }
                            } else {
                                echo "INSERT INTO asistencias (fecha_as, Id_em, sesiones_as, tipo_as, temperatura_as, firma_as,responsable_as) VALUES ('{$data['fecha'][$i]}', '$data[idempleado]', '$sesion', '', '', '', '0') " . '<br>';
                            }
                        }
                    }
                }
            }


            if ($asistencia) {
                $informacion['status'] = true;
                $informacion['externo'] = $externo;
                $informacion['id'] = $idasistencia;
            } else {
                $informacion['status'] = false;
            }
        }

        echo json_encode($informacion);
        break;
    case 'Registro-externo':
        $asistencia = mysql_query("SELECT * FROM asistencias WHERE Id_as = '$data[idas]' GROUP BY DATE(fecha_as)  ");
        if (mysql_num_rows($asistencia) > 0) {
            $ras = mysql_fetch_object($asistencia);

            $total_preguntas = 11;
            $fechas_sesiones = array();

            $encuestas = mysql_query("SELECT * FROM asistencias WHERE sesion_as = '$ras->sesion_as'  GROUP BY DATE(fecha_as) ");
            if (mysql_num_rows($encuestas) > 0) {
                $total_sesiones = mysql_num_rows($encuestas);

                while ($ren = mysql_fetch_object($encuestas)) {
                    $fecha = explode(' ',  $ren->fecha_as);

                    $fechas_sesiones[] = $fecha[0];
                }
            }

            for ($k = 0; $k < count($fechas_sesiones); $k++) {
                $j = $k;

                $entrada = $data['temperatura-' . $fechas_sesiones[$k]]['E'];
                $salida = $data['temperatura-' . $fechas_sesiones[$k]]['S'];

                $hora_entrada = $data['ingreso-' . $fechas_sesiones[$k]]['E'];
                $hora_salida = $data['salida-' . $fechas_sesiones[$k]]['S'];

                if ($entrada != '') {
                    $comprobar = mysql_query("SELECT * FROM asistencias WHERE fecha_as LIKE '$fechas_sesiones[$k]%' AND sesion_as = '$ras->sesion_as' AND tipo_as = 'E'  ");

                    $rcom = mysql_fetch_object($comprobar);

                    $fecha_asistencia = explode(' ', $rcom->fecha_as);

                    $fecha_asistencia[0] = $fecha_asistencia[0] . ' ' . $hora_entrada;

                    $sql_fecha_entrada = "";

                    // if ($fecha_asistencia[1] == '00:00:00') {
                        $sql_fecha_entrada = ", fecha_as = '$fecha_asistencia[0]' ";
                    // }

                    $encuestas = mysql_query("UPDATE asistencias SET temperatura_as = '$entrada' $sql_fecha_entrada WHERE tipo_as = 'E' AND fecha_as LIKE '$fechas_sesiones[$k]%' AND sesion_as = '$ras->sesion_as' ");
                }

                if ($salida != '') {
                    $comprobar = mysql_query("SELECT * FROM asistencias WHERE fecha_as LIKE '$fechas_sesiones[$k]%' AND sesion_as = '$ras->sesion_as' AND tipo_as = 'S'  ");

                    $rcom = mysql_fetch_object($comprobar);

                    $fecha_asistencia = explode(' ', $rcom->fecha_as);

                    $fecha_asistencia[0] = $fecha_asistencia[0] . ' ' . $hora_salida;

                    $sql_fecha_salida = "";

                    // if ($fecha_asistencia[1] == '00:00:00') {
                        $sql_fecha_salida = ", fecha_as = '$fecha_asistencia[0]' ";
                    // }

                    $encuestas = mysql_query("UPDATE asistencias SET temperatura_as = '$salida' $sql_fecha_salida WHERE tipo_as = 'S' AND fecha_as LIKE '$fechas_sesiones[$k]%' AND sesion_as = '$ras->sesion_as' ");
                }

                for ($i = 0; $i < $total_preguntas; $i++) {
                    $respuesta = $data['pregunta'][$j];
                    $npregunta = $i + 1;

                    $actualiza = mysql_query("UPDATE asistencias_encuesta SET respuesta_ae = '$respuesta' WHERE  fecha_ae = '$fechas_sesiones[$k]' AND sesion_as = '$ras->sesion_as' AND pregunta_ae = '$npregunta' ");

                    $j += $total_sesiones;
                }
            }
        }

        $informacion['status'] = true;

        echo json_encode($informacion);
        break;
    case 'listar':
        if (!isset($data['inicio']) || empty($data['inicio'])) {
            $data['inicio'] = '1001-01-01 00:00:00';
        } else {
            $data['inicio'] = $data['inicio'] . ' 00:00:00';
        }

        if (!isset($data['fin']) || empty($data['fin'])) {
            $data['fin'] = '9999-01-01 23:59:00';
        } else {
            $data['fin'] = $data['fin'] . ' 23:59:00';
        }

        $sqql = '';

        if (isset($data['identifica']) && $data['identifica'] != '') {
            $sqql = "AND Id_em IN (SELECT Id_em FROM empleados WHERE identificacion_em = '$data[identifica]' )";
        }

        $list         = mysql_query("SELECT * FROM asistencias WHERE fecha_as BETWEEN '$data[inicio]' AND '$data[fin]' $sqql GROUP BY sesion_as ");
        $total        = mysql_num_rows($list);
        $adyacentes   = 2;
        $registro_pag = 100;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';
        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javacript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }

            // $paginacion.= "</div>";
        }
        $registros = mysql_query("SELECT * FROM asistencias WHERE fecha_as BETWEEN '$data[inicio]' AND '$data[fin]' $sqql GROUP BY sesion_as  ORDER BY fecha_as DESC LIMIT $inicio, $registro_pag ");
        $total     = mysql_num_rows($registros);
        $filas     = '';

        if ($total > 0) {
            while ($res = mysql_fetch_object($registros)) {
                $empleados = mysql_query("SELECT * FROM empleados WHERE Id_em = '$res->Id_em' ");
                $rem = mysql_fetch_object($empleados);

                $temperatura = ($res->temperatura_as != '' ?  $res->temperatura_as . ' °C' : "");
                $fecha = explode(' ', $res->fecha_as);
                $fecha = $fecha[0];

                $filas .= '<tr id="' . $res->Id_as . '" data-tipo="' . $rem->tipo_em . '">
                                <td><p>' . $rem->identificacion_em . '</p></td>
                                <td><p>' . $rem->nombre_em . '</p></td>
                                <td><p>' . $rem->correo_em . '</p></td>
                                <td><p>' . $rem->telefono_em . '</p></td>
                                <td><p>' . $fecha . '</p></td>
                                <td><p>' . $temperatura . '</p></td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="6"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;
        echo json_encode($informacion);
        break;
}
