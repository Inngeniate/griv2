<?php
require "conexion.php";
$data = $_REQUEST['consulta'];

$bus = $db
    ->where('numero_ident', $data['identificacion'])
    ->where('numero_ident', 0, '!=')
    ->objectBuilder()->get('registros');

if ($db->count > 0) {
    $informacion['status'] = true;
    $informacion['redirec'] = 'https://forms.gle/jQFcCMXHFsiNASAj9';
} else {
    $bus = $db
        ->where('documento_al', $data['identificacion'])
        ->where('documento_al', 0, '!=')
        ->objectBuilder()->get('registros_alturas');

    if ($db->count > 0) {
        $informacion['status'] = true;
        $informacion['redirec'] = 'https://forms.gle/jQFcCMXHFsiNASAj9';
    } else {
        $informacion['status'] = false;
    }
}

echo json_encode($informacion);
