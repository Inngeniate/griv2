<?php
require "conexion.php";
$usuario = $_POST['nombre_u'];
$pswd    = $_POST['pass_u'];
$answer  = array();

$sel = $db
    ->where('login', $usuario)
    ->where('estado', 1)
    ->objectBuilder()->get('usuarios');

if ($db->count > 0) {
    $pass = $sel[0]->password;

    if (@crypt($pswd, $pass) == $pass) {
        session_start();
        $_SESSION['usuloggri'] = $sel[0]->id;
        $_SESSION['usutipoggri'] = $sel[0]->tipo;


        if ($sel[0]->tipo == 'auditor') {
            $answer['redirec'] = 'trabajo-altura';
        } elseif ($sel[0]->tipo == 'alturas') {
            $answer['redirec'] = 'trabajo-altura';
        } elseif ($sel[0]->tipo == 'registros') {
            $answer['redirec'] = 'listar-registros';
        } else {
            $permisos_usuario = $db
                ->where('usuario_up', $_SESSION['usuloggri'])
                ->where('modulo_up', 1)
                ->where('permiso_up', 1)
                ->objectBuilder()->get('usuarios_permisos');

            if ($db->count > 0) {
                $answer['redirec'] = 'home';
            } else {
                $permisos_usuario = $db
                    ->where('usuario_up', $_SESSION['usuloggri'])
                    ->where('permiso_up', 1)
                    ->orderBy('Id_up', 'ASC')
                    ->objectBuilder()->get('usuarios_permisos', 1);

                $menu = $db
                    ->where('Id_m', $permisos_usuario[0]->modulo_up)
                    ->objectBuilder()->get('menu');

                $answer['redirec'] = $menu[0]->link_m;
            }
        }
    } else {
        $answer = 'error';
    }
} else {
    $answer = 'error';
}
echo json_encode($answer);
