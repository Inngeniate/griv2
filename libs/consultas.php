<?php
require "conexion.php";

$data = $_REQUEST['consulta'];

date_default_timezone_set("America/Bogota");
$nfecha = date('Y-m-j');

if (!isset($data['identificacion'])) {
    $data['identificacion'] = '';
}

if (!isset($data['placa'])) {
    $data['placa'] = '';
}

if (!isset($data['serial'])) {
    $data['serial'] = '';
}

if (!isset($data['identificacionaltuas'])) {
    $data['identificacionaltuas'] = '';
}

if (!isset($data['pagina'])) {
    $data['pagina'] = '';
}

if ($data['identificacion'] != '' && $data['identificacion'] != 0) {
    $registro = '';

    $bus = $db
        ->where('numero_ident', $data['identificacion'])
        ->orderBy('Id', 'DESC')
        ->objectBuilder()->get('registros', null, 'Id, certificado, CONCAT(nombre_primero," ",nombre_segundo) as nombre, apellidos,horas,fecha_vigencia');

    if ($db->count > 0) {
        if (isset($data['telefono'])) {
            $nfecha = date('Y-m-d H:i:s');

            $datos = [
                'identificacion_sa' => $data['identificacion'],
                'celular_sa' => $data['telefono'],
                'fecha_sa' => $nfecha,
                'tipo' => 'Certificaciones',
            ];

            $solicitud = $db
                ->insert('solicitud_alturas', $datos);
        }

        foreach ($bus as $res) {
            $curso = '';

            $certificaciones = $db
                ->where('Id_ct', $res->certificado)
                ->objectBuilder()->get('certificaciones');

            if ($db->count > 0) {
                $curso = $certificaciones[0]->nombre;
            }

            $registro .= '<div class="Mostrar-certificado-detalles" id="' . $res->Id . '">';
            $registro .= '<table>
									<tbody>
										<tr>
											<td><strong>Certificado:</strong></td>
											<td class="Mayuscula">' . $curso . '</td>
										</tr>
										<tr>
											<td><strong>Nombre:</strong></td>
											<td class="Mayuscula">' . $res->nombre . ' ' .  $res->apellidos . '</td>
										</tr>
										<tr>
											<td><strong>Duración:</strong></td>
											<td class="Mayuscula">' . $res->horas . ' Hrs</td>
										</tr>
										<tr>
											<td><strong>Vigencia hasta:</strong></td>
											<td class="Mayuscula">' . ($res->fecha_vigencia == '0000-00-00' ? "" : $res->fecha_vigencia) . '</td>
										</tr>
									</tbody>
								</table>
								';
            $registro .= '</div>';
        }

        $informacion['certificados'] = $registro;
        $informacion['status']       = 'Correcto';
    } else {
        $informacion['status'] = 'Error';
    }

    echo json_encode($informacion);
} elseif ($data['placa'] != '') {
    $registro = '';

    $bus = $db
        ->where('placa_en', $data['placa'])
        ->orWhere('placaremolque_en', $data['placa'])
        ->objectBuilder()->get('certificado_ensayo');

    if ($db->count != 0) {
        if (isset($data['telefono'])) {
            $nfecha = date('Y-m-d H:i:s');

            $datos = [
                'identificacion_sa' => $data['placa'],
                'celular_sa' => $data['telefono'],
                'fecha_sa' => $nfecha,
                'tipo' => 'Inspecciones',
            ];

            $solicitud = $db
                ->insert('solicitud_alturas', $datos);
        }

        foreach ($bus as $res) {
            switch ($res->tipo_en) {
                case '1':
                    $tipo = 'QUINTA RUEDA';
                    break;
                case '2':
                    $tipo = 'LINEA DE VIDA';
                    break;
                case '3':
                    $tipo = 'KING PIN';
                    break;
            }

            $registro .= '<div class="Mostrar-certificado-detalles" id="' . $res->Id_en . '">';
            $registro .= '<table>
									<tbody>
										<tr>
											<td><strong>Empresa:</strong></td>
											<td class="Mayuscula">' . $res->empresa_en . '</td>
										</tr>
										<tr>
											<td><strong>Propietario:</strong></td>
											<td class="Mayuscula">' . $res->propietario_en . '</td>
										</tr>
										<tr>
											<td><strong>Marca:</strong></td>
											<td class="Mayuscula">' . $res->marca_en . '</td>
										</tr>
										<tr>
											<td><strong>Modelo:</strong></td>
											<td class="Mayuscula">' . $res->modelo_en . '</td>
										</tr>
										<tr>
											<td><strong>Tipo:</strong></td>
											<td class="Mayuscula">' . $tipo . '</td>
										</tr>
									</tbody>
								</table>
								<hr>
								';
            $registro .= '</div>';
        }

        $informacion['certificados'] = $registro;
        $informacion['status']       = 'Correcto';
    } else {
        $informacion['status'] = 'Error';
    }

    echo json_encode($informacion);
} elseif ($data['serial'] != '') {
    $registro = '';
    $existe = 0;

    $bus = $db
        ->where('codaprobacion', $data['serial'])
        ->objectBuilder()->get('certificado_arnes');

    if ($db->count != 0) {
        $existe = 1;
    } else {
        $bus = $db
            ->where('codaprobacion', $data['serial'])
            ->objectBuilder()->get('certificado_eslinga');

        if ($db->count != 0) {
            $existe = 1;
        }
    }

    if ($existe != 0) {
        if (isset($data['telefono'])) {
            $nfecha = date('Y-m-d H:i:s');

            $datos = [
                'identificacion_sa' => $data['serial'],
                'celular_sa' => $data['telefono'],
                'fecha_sa' => $nfecha,
                'tipo' => 'Certificado Seguridad',
            ];

            $solicitud = $db
                ->insert('solicitud_alturas', $datos);
        }

        foreach ($bus as $res) {
            $registro .= '<div class="Mostrar-certificado-detalles">
								<table>
									<tbody>
										<tr>
											<td><strong>Propietario:</strong></td>
											<td class="Mayuscula">' . $res->propietario . '</td>
										</tr>
										<tr>
											<td><strong>Equipo:</strong></td>
											<td class="Mayuscula">' . $res->equipo . ' Hrs</td>
										</tr>
										<tr>
											<td><strong>Marca:</strong></td>
											<td class="Mayuscula">' . $res->marca . '</td>
										</tr>
										<tr>
											<td><strong>Modelo:</strong></td>
											<td class="Mayuscula">' . $res->modelo . '</td>
										</tr>
									</tbody>
								</table>
							</div>';
        }

        $informacion['certificados'] = $registro;
        $informacion['status']       = 'Correcto';
    } else {
        $informacion['status'] = 'Error';
    }

    echo json_encode($informacion);
} elseif ($data['identificacionaltuas'] != '' && $data['identificacionaltuas'] != 0) {
    $nfecha = date('Y-m-d H:i:s');

    $datos = [
        'identificacion_sa' => $data['identificacionaltuas'],
        'celular_sa' => $data['celular'],
        'correo_sa' => $data['correo'],
        'fecha_sa' => $nfecha,
        'tipo' => 'Alturas',
    ];

    $solicitud = $db
        ->insert('solicitud_alturas', $datos);

    if ($solicitud) {
        $registros = $db
            ->where('numero_ident', $data['identificacionaltuas'])
            ->where('capacitacion', 'ALTURAS')
            ->groupBy('certificado')
            ->orderBy('Id', 'DESC')
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            $rreg = $registros[0];

            $informacion['reg'] = $rreg->Id . '-' . str_replace('.', '', trim($data['identificacionaltuas']));
            $informacion['alturas'] = true;
        }

        require("class.phpmailer.php");
        $mail = new PHPMailer(true);
        $mail->Host = "localhost";

        $mail->FromName = "Nueva Solicitud Consulta Certificado de alturas - GRI COMPANY S.A.S";
        $mail->Subject = 'Mensaje Solicitud Consulta Certificado de alturas';
        $mail->AddAddress("sgi.gricompany@gmail.com");
        $mail->AddAddress("gricompanysas@gmail.com");
        $mail->AddAddress("operaciones.gricompany@gmail.com");
        $mail->AddAddress("operaciones1.gricompany@gmail.com");
        $body = "<strong>Nuevo mensaje desde www.gricompany.co:</strong><br><br>";
        $body .= "Datos <br>";
        $body .= "No Cedula: $data[identificacionaltuas]<br>";
        $body .= "Celular: $data[celular]<br>";
        $body .= "Correo: $data[correo]<br>";
        $mail->Body = $body;
        $mail->IsHTML(true);

        if ($mail->send()) {
        }


        $informacion['status'] = 'Correcto';
    } else {
        $informacion['status'] = 'Error';
    }

    echo json_encode($informacion);
} elseif ($data['pagina'] != '') {
    $list = $db
        ->objectBuilder()->get('solicitud_alturas');

    $total        = $db->count;
    $adyacentes   = 2;
    $registro_pag = 100;
    $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
    $pagina       = ($pagina == 0 ? 1 : $pagina);
    $inicio       = ($pagina - 1) * $registro_pag;

    $siguiente  = $pagina + 1;
    $anterior   = $pagina - 1;
    $ultima_pag = ceil($total / $registro_pag);
    $penultima  = $ultima_pag - 1;

    $paginacion = '';

    if ($ultima_pag > 1) {
        if ($pagina > 1) {
            $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
        } else {
            $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
        }

        if ($pagina > 1) {
            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
        } else {
            $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
        }

        if ($ultima_pag < 7 + ($adyacentes * 2)) {
            for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                if ($contador == $pagina) {
                    $paginacion .= "<span class='actual'>$contador</span>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                }
            }
        } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
            if ($pagina < 1 + ($adyacentes * 2)) {
                for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
                $paginacion .= "...";
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
            } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                $paginacion .= "...";
                for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
                $paginacion .= "..";
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
            } else {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                $paginacion .= "..";
                for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            }
        }
        if ($pagina < $contador - 1) {
            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
        } else {
            $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
        }

        if ($pagina < $ultima_pag) {
            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
        } else {
            $paginacion .= "<span class='disabled'>Última &raquo;</span>";
        }
    }

    $registros = $db
        ->orderBy('Id_sa', 'DESC')
        ->objectBuilder()->paginate('solicitud_alturas', $pagina);

    $total     = $db->count;
    $filas     = '';

    if ($total > 0) {
        foreach ($registros as $res) {
            $selector = '';

            switch ($res->tipo) {
                case 'Certificaciones':
                    $selector = 'Identificación';
                    break;
                case 'placa':
                    $selector = 'Placa';
                    break;
                case 'serial':
                    $selector = 'Serial';
                    break;
                case 'Alturas':
                    $selector = 'Identificación';
                    break;
            }

            $filas .= '<tr id="' . $res->Id_sa . '">
                            <td><p>' . $res->tipo . '</p></td>
                            <td><p>' . $selector . '</p></td>
                            <td><p>' . $res->identificacion_sa . '</p></td>
                            <td><p>' . $res->celular_sa . '</p></td>
                            <td><p>' . $res->correo_sa . '</p></td>
                            <td><p>' . $res->fecha_sa . '</p></td>
                        </tr>';
        }
    } else {
        $filas = '<tr>
                    <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                </tr>';
    }

    $informacion['registros']  = $filas;
    $informacion['paginacion'] = $paginacion;

    echo json_encode($informacion);
} else {
    $informacion['status'] = 'Error';
    echo json_encode($informacion);
}
