<?php
require "conexion.php";
@$informacion = array();
@$data        = $_REQUEST['curso'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

switch ($data['opc']) {
    case 'nuevo_curso':
        $bus = $db
            ->where('nombre', $data['nombre'])
            ->objectBuilder()->get('precios');

        if ($db->count == 0) {
            $datos = [
                'nombre' => $data['nombre'],
                'iva' => $data['iva'],
                'sin_iva' => $data['sinIva'],
                'total' => $data['total'],
            ];

            $nuevo = $db
                ->insert('precios', $datos);

            if ($nuevo) {
                $informacion['status'] = true;
            } else {
                $informacion['status'] = false;
                $informacion['msj'] = 'Error, no se pudo realizar el registro';
            }
        } else {
            $informacion['status'] = false;
            $informacion['msj'] = 'Existe un curso con ese nombre';
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        @$buscar_nom = $data['nom'];

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        $list = $db
            ->where('nombre', '%' . $buscar_nom . '%', 'LIKE')
            ->objectBuilder()->get('precios');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($_POST['pagina']) ? $_POST['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $filas     = '';
        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('nombre', '%' . $buscar_nom . '%', 'LIKE')
            ->orderBy('Id', 'DESC')
            ->objectBuilder()->paginate('precios', $pagina);

        if ($total > 0) {
            foreach ($registros as $registro) {
                $filas .= '<tr id="' . $registro->Id . '">
                                <td><p>' . $registro->nombre . '</p></td>
                                <td><p> ' . $registro->iva . '</p></td>
                                <td><p>$ ' . $registro->total . '</p></td>
                                <td><a href="precio-curso_edt?rg=' . $registro->Id . '" target="_blank" title="Editar" class="editar"><span class="icon-editar"></span></a><a href="javascript://" title="Eliminar" class="eliminar"><span class="icon-eliminar"></span></a></td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="4"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;

        echo json_encode($informacion);
        break;
    case 'editar_curso':
        $sql = '';

        $comprobar = $db
            ->where('nombre', $data['nombre'])
            ->where('Id', $data['idCurso'], '!=')
            ->objectBuilder()->get('precios');

        if ($db->count > 0) {
            $informacion['status'] = false;
            $informacion['msg']    = 'El nombre ya existe';
        } else {
            $datos = [
                'nombre' => $data['nombre'],
                'iva' => $data['iva'],
                'sin_iva' => $data['sinIva'],
                'total' => $data['total'],
            ];

            $act = $db
                ->where('Id', $data['idCurso'])
                ->update('precios', $datos);

            if ($act) {
                $informacion['status'] = true;
            } else {
                $informacion['status'] = false;
                $informacion['msg']    = 'Error al actualizar';
            }
        }

        echo json_encode($informacion);
        break;
    case 'eliminar':
        $comprobar = $db
            ->where('Id', $data['idCurso'])
            ->delete('precios');
        break;
}

function crypt_blowfish_bycarluys($password, $digito = 7)
{
    $set_salt = './1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $salt     = sprintf('$2a$%02d$', $digito);
    for ($i = 0; $i < 22; $i++) {
        $salt .= $set_salt[mt_rand(0, 63)];
    }
    return crypt($password, $salt);
}
