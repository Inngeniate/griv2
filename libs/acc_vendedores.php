<?php
require "conexion.php";
@$informacion = array();
$opc          = $_REQUEST['accion'];
$data         = $_POST['vendedor'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

switch ($opc) {
    case 'nuevo_vendedor':
        $datos = [
            'nombre_v' => $data['nombre'],
            'direccion_v' => $data['direccion'],
            'telefono_v' => $data['telefono'],
            'correo_v' => $data['correo']
        ];

        $nuevo = $db
            ->insert('vendedores', $datos);

        if ($nuevo) {
            if (count($data['curso'])) {
                for ($i = 0; $i < count($data['curso']); $i++) {
                    $datos = [
                        'vendedor_vc' => $nuevo,
                        'curso_vc' => $data['curso'][$i],
                        'competencia_vc' => $data['competencia'][$i],
                        'precio_vc' => $data['precio'][$i]
                    ];

                    $cursos = $db
                        ->insert('vendedores_cursos', $datos);
                }
            }

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        @$buscar_nom = $_POST['nom'];

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        $list = $db
            ->where('nombre_v', '%' . $buscar_nom . '%', 'LIKE')
            ->objectBuilder()->get('vendedores');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('nombre_v', '%' . $buscar_nom . '%', 'LIKE')
            ->orderBy('Id_v', 'DESC')
            ->objectBuilder()->paginate('vendedores', $pagina);

        $total     = $db->count;
        $filas     = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $ls_cursos = '';
                $ls_precios = '';

                $asignados = $db
                    ->where('vendedor_vc', $res->Id_v)
                    ->objectBuilder()->get('vendedores_cursos');

                if ($db->count > 0) {
                    foreach ($asignados as $rasg) {
                        $cursos = $db
                            ->where('Id_ct', $rasg->curso_vc)
                            ->where('activo_ct', 1)
                            ->objectBuilder()->get('certificaciones');

                        if ($db->count > 0) {
                            $rcu = $cursos[0];
                            $ls_cursos .= '<p>' . $rcu->nombre  . '</p>';
                            $ls_precios .= '<p style="white-space: nowrap">$ ' . $rasg->precio_vc . '</p>';
                        }
                    }
                }

                $filas .= '<tr id="' . $res->Id_v . '">
                                <td><p>' . $res->nombre_v . '</p></td>
                                <td><p>' . $res->direccion_v . '</p></td>
                                <td><p>' . $res->telefono_v . '</p></td>
                                <td><p>' . $res->correo_v . '</p></td>
                                <td>' . $ls_cursos . '</td>
                                <td>' . $ls_precios . '</td>
                                <td>
                                <a href="vendedores_edt?vendedor=' . $res->Id_v . '" target="_blank" title="Editar" class="editar"><span class="icon-editar"></span></a>
                                <a href="javascript://" title="Eliminar" class="eliminar"><span class="icon-eliminar"></span></a>
                                </td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;

        echo json_encode($informacion);
        break;
    case 'editar_vendedor':
        $datos = [
            'nombre_v' => $data['nombre'],
            'direccion_v' => $data['direccion'],
            'telefono_v' => $data['telefono'],
            'correo_v' => $data['correo']
        ];

        $act = $db
            ->where('Id_v', $data['idvendedor'])
            ->update('vendedores', $datos);

        if ($act) {
            $eliminar = $db
                ->where('vendedor_vc', $data['idvendedor'])
                ->delete('vendedores_cursos');

            if (count($data['curso'])) {
                for ($i = 0; $i < count($data['curso']); $i++) {
                    $datos = [
                        'vendedor_vc' => $data['idvendedor'],
                        'curso_vc' => $data['curso'][$i],
                        'competencia_vc' => $data['competencia'][$i],
                        'precio_vc' => $data['precio'][$i]
                    ];

                    $cursos = $db
                        ->insert('vendedores_cursos', $datos);
                }
            }


            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'eliminar_vendedor':
        $elim = $db
            ->where('Id_v', $data['idvendedor'])
            ->delete('vendedores');

        if ($elim) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
}
