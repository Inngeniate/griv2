<?php
require "conexion.php";

// php tcpdf_addfont.php

$id = $_REQUEST['id'];
// $fuente = $_REQUEST['fuente'];

include 'TCPDF/tcpdf.php';

$fuente = 'questrial';
$fuente_1 = 'dizhitlextrabi';
$fuente_2 = 'dizhitlextrabi';
$fuente_3 = 'bevietnamproblack';
$fuente_4 = 'poppins';
$cursos = '';
$intensidad = '';
$cont   = 0;
$meses  = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

/* $comprobar = $db
    ->where('registro_ced', $id)
    ->where('comprobado_ced', 1)
    ->objectBuilder()->get('codigos_exportados_certificados');

if ($db->count > 0 || $id < 105346) { */

    $registros = $db
        ->where('Id', $id)
        ->groupBy('certificado')
        ->orderBy('Id', 'ASC')
        ->objectBuilder()->get('registros');

    $total = $db->count;

    foreach ($registros as $rsg) {
        $nombres   = trim($rsg->nombre_primero) . ' ' . trim($rsg->nombre_segundo);
        $apellidos = trim($rsg->apellidos);

        $nombre     = strtoupper($nombres . ' ' . $apellidos);
        $tipoid     = $rsg->tipo_ident;
        $identifica = $rsg->numero_ident;
        // $expedido   = date('d-m-Y', strtotime($rsg->fecha_inicio));
        $expedido   = '';
        $inicio     = $rsg->fecha_inicio;
        $finalizo   = $rsg->fecha_fin;

        if ($inicio != '' & $inicio != '0000-00-00') {
            $inicio  = explode('-', $inicio);
            $inicio  = $inicio[2] . ' de ' . $meses[$inicio[1] - 1] . ' de ' . $inicio[0];
        }

        if ($finalizo != '' & $finalizo != '0000-00-00') {
            $finalizo  = explode('-', $finalizo);
            $finalizo  = $finalizo[2] . ' de ' . $meses[$finalizo[1] - 1] . ' de ' . $finalizo[0];

            $expedido   = date('d-m-Y', strtotime($rsg->fecha_fin));
        }

        $GLOBALS['codid'] = $rsg->Id;

        if ($rsg->fecha_vigencia != '0000-00-00') {
            $GLOBALS['vigencia'] = date('d-m-Y', strtotime($rsg->fecha_vigencia));
        } else {
            $GLOBALS['vigencia'] = '';
        }

        $GLOBALS['capacitacion'] = $rsg->capacitacion;
        $GLOBALS['certificado'] = $rsg->certificado;

        if ($cont == 0) {
            $c1 = 'border-top:1px solid #000;';
        } else {
            $c1 = '';
        }

        if ($cont == ($total - 1)) {
            $c2 = 'border-bottom:1px solid #000;';
        } else {
            $c2 = '';
        }

        $ls_curso = '';

        $certificaciones = $db
            ->where('Id_ct', $rsg->certificado)
            ->objectBuilder()->get('certificaciones');

        if ($db->count > 0) {
            $rsc      = $certificaciones[0];
            $ls_curso = $rsc->nombre;
        }

        $cont++;

        $vigencia = '';

        if ($rsg->fecha_vigencia != '0000-00-00' && $rsg->fecha_vigencia != null && $rsg->fecha_vigencia != '') {
            $vigencia = date('d-m-Y', strtotime($rsg->fecha_vigencia));
        }

        $qrcode = $rsg->Id . ' ' . $nombre . ' ' . $tipoid . ' ' . $identifica . ' ' . $ls_curso . ' ' . $vigencia . ' ' . 'http://www.gricompany.co/' . ' ' . '3143257703';

        $tipo_entrenador = 'TA';

        if ($rsg->entrenador != 0) {
            $entrenadores = $db
                ->where('Id_en', $rsg->entrenador)
                ->objectBuilder()->get('entrenadores');

            if ($db->count > 0) {
                $rent = $entrenadores[0];

                $entrenador = $rent->nombre_en . ' ' . $rent->apellido_en;
                $tsa = $rent->tsa_reg;
                $sena = $rent->sena_reg;
                $licencia = $rent->licencia_sst;

                if ($rent->firma_en != '') {
                    $firma = '../' . $rent->firma_en;
                } else {
                    $firma = '';
                }

                $tipo_entrenador = 'EC';
            }
        }

        $cliente_razon = '';
        $cliente_nit = '';
        $cliente_representante = '';
        $cliente_arl = '';

        $clientes = $db
            ->where('Id', $rsg->cliente)
            ->objectBuilder()->get('clientes');

        if ($db->count > 0) {
            $rcl = $clientes[0];

            $cliente_razon = $rcl->razon;
            $cliente_nit = $rcl->nit;
            $cliente_representante = $rcl->representante;
            $cliente_arl = $rcl->arl;
        }

        $ciudad_expedicion = '';
        $direccion = '';

        if ($rsg->codigo_ficha != '') {
            $fichas = $db
                ->where('Id', $rsg->codigo_ficha)
                ->objectBuilder()->get('fichas_ministerio');

            if ($db->count > 0) {
                $rf = $fichas[0];

                $ciudades = [
                    'cucuta' => 'Cúcuta',
                    'villavicencio' => 'Villavicencio',
                    '' => ''
                ];

                $ciudad_expedicion = $ciudades[$rf->ciudad];
            }
        }

        if ($rf->ciudad == 'villavicencio') {
            $txt2 = 'Certificado de capacitación y entrenamiento para trabajo en altura';
            $direccion = 'Carrera 37 # 26 C 35 Barrio 7 de Agosto';
        }else{
            $direccion = 'AV 6 #K-31-1B-11 SAN GERARDO';
        }

        $GLOBALS['ciudad_expedicion'] = $ciudad_expedicion;


        $txt3 = '<br><span style="font-family:' . $fuente . ';font-size: 10.8px;">Realizado en la ciudad de ' . $ciudad_expedicion . ', del ' . $inicio . ' a ' . $finalizo . '</span>';
        $GLOBALS['expide'] = date('d-m-Y');
        $GLOBALS['txt4'] = 'Expedido';

        $cursos .= '<span style="font-family:' . $fuente_2 . ';font-size: 18.35px;color:#d40e22"><i>' . $ls_curso . '</i></span>';

        $intensidad = '<span style="font-family:' . $fuente . ';font-size: 10.5px;">Con Una Intensidad De (' . $rsg->horas . ') Horas</span>' . $txt3;
    }


    class MYPDF extends TCPDF
    {
        public function Header()
        {
            $bMargin         = $this->getBreakMargin();
            $auto_page_break = $this->AutoPageBreak;
            $this->SetAutoPageBreak(false, 0);
            // if ($GLOBALS['capacitacion'] == 'ALTURAS' || $GLOBALS['capacitacion'] == 'ESPACIOS CONFINADOS' || $GLOBALS['capacitacion'] == 'CEA') {

            if ($GLOBALS['ciudad_expedicion'] == 'Cúcuta') {
                $image_file = '../images/certificado_alturas4272_cucuta.jpg';
            } else {
                $image_file = '../images/certificado_alturas4272.jpg';
            }

            $this->Image($image_file, 0, -2, 279.2, 220, '', '', '', false, 300, '', false, false, 0);

            $this->SetAutoPageBreak($auto_page_break, $bMargin);
            $this->setPageMark();
        }
        public function Footer()
        {
        }
    }

    // create new PDF document

    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
    $pdf->setPageOrientation('l');

    // $pdf->SetProtection(array('modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), '', null, 3, null);

    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // PDF_MARGIN_TOP
    $pdf->SetMargins(20, 20, 8);
    // $pdf->SetPrintHeader(false);
    // $pdf->setPrintFooter(false);

    // set auto page breaks
    // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->SetAutoPageBreak(true, 0);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->AddPage();



    $logo = '../images/logoalturas.png';
    $logo_w = 280;
    $txt1 = 'CERTIFICA QUE';
    $style1 = 'font-family:' . $fuente . ';font-size: 16x;';
    $txt2 = 'Curso y aprobó satisfactoriamente la acción de formación y entrenamiento:';
    $txt3 = '';

    $validez = '';

    if ($GLOBALS['vigencia'] != '') {
        $validez = ' ';
    }

    $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center" style="' . $style1 . '">' . $txt1 . '</td>
            </tr>
        </table>';

    $pdf->SetXY(12, 55);
    $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center"><p style="font-family:' . $fuente_1 . ';font-size: 26px;">' . $nombre . '</p></td>
            </tr>
        </table>';

    $pdf->SetXY(12, 63);
    $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center"><span style="font-family:' . $fuente . ';font-size: 16px;">' . $tipoid . ' ' . $identifica . '</span></td>
            </tr>
        </table>';

    $pdf->SetXY(12, 72);
    $pdf->WriteHTML($html);


    $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center" style="font-size:12px"><strong>Enviado por: </strong><span style="font-family:' . $fuente . ';">' . $cliente_razon . '</span><strong> NIT: </strong><span style="font-family:' . $fuente . ';">' . $cliente_nit . '</span><br><strong>REPRESENTANTE LEGAL: </strong><span style="font-family:' . $fuente . ';">' . $cliente_representante . '</span><br><strong>ARL: </strong><span style="font-family:' . $fuente . ';">' . $cliente_arl . '</span></td>
            </tr>
        </table>';

    $pdf->SetXY(12, 78);
    $pdf->WriteHTML($html);


    $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center"><span style="font-family:' . $fuente . ';font-size: 10.5px;">' . $txt2 . '</span></td>
            </tr>
        </table>';

    $pdf->SetXY(12, 92);
    $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="2" border="0" width="700px">
            <tr>
                <td align="center">' . $cursos . '</td>
            </tr>
            <tr>
                <td align="center">' . $intensidad . '</td>
            </tr>
        </table>';

    $pdf->SetXY(43, 96);
    $pdf->WriteHTML($html);

    /* $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center">' . $intensidad . '</td>
            </tr>
        </table>';

$pdf->SetXY(12, 106);
$pdf->WriteHTML($html);
 */
    /* $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr align="center">
                <td style="font-size: 11px;">' . $GLOBALS['txt4'] . ' en la ciudad de Villavicencio, meta, el <span style="color:#d40e22">' . $GLOBALS['expide'] . '</span><br>Codigo de validación: ' . $GLOBALS['codid'] . ' </td>
            </tr>
        </table>';

$pdf->SetXY(12, 104);
$pdf->WriteHTML($html); */

    if ($firma != '') {
        $pdf->Image($firma, 80, 115, 35, 20, 'PNG', '');
    }

    $html = '<table cellspacing="0" cellpadding="0" border="0" >
                <tr>
                    <td align="center" style="font-family:' . $fuente_3 . ';font-size: 8.5px;"><strong>' . $entrenador . '</strong></td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 8.5px;">Entrenador ' . $tipo_entrenador . ' Reg. ' . $tsa . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 8.5px;">Evaluador de Competencias laborales Sena Reg ' . $sena . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 8.5px;">Licencia, SST ' . $licencia . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 8.5px;">Gestion del Riesgo Integral Company S.A.S</td>
                </tr>
            </table>';

    $pdf->writeHTMLCell('', '', -74.5, 135, $html, 0, 0, 0, true, 'J', true);


    $html = '<table cellspacing="0" cellpadding="0" border="0" >
                <tr>
                    <td align="center" style="font-family:' . $fuente_3 . ';font-size: 8.5px;"><strong>Lady Marcela Sánchez Martínez</strong></td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 8.5px;">Gerente General</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 8.5px;">Licencia. SST 3752 / 2016</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 8.5px;">Gestion del Riesgo Integral Company S.A.S</td>
                </tr>
            </table>';

    $pdf->writeHTMLCell('', '', 119.5, 135, $html, 0, 0, 0, true, 'J', true);

    /* $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center">' . $intensidad . '</td>
            </tr>
        </table>';

$pdf->SetXY(25, 96);
$pdf->WriteHTML($html); */

    $html = '<table cellspacing="0" cellpadding="5" border="0">
            <tr align="center">
                <td style="font-family:' . $fuente . ';font-size: 11px;"><p>Institución Educativa Para El Talento Y Desarrollo Humano Gri Company Nit: 900 892 983-6</p></td>
            </tr>
        </table>';

    $pdf->SetXY(25, 158);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="5" border="0">
            <tr align="center">
                <td style="font-family:' . $fuente . ';font-size: 11px;"><p>Resolución N° 1500-56.03 1795 de la secretaria de educación Licencia seguridad y salud en el trabajo - </p></td>
            </tr>
        </table>';

    $pdf->SetXY(25, 165);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="5" border="0">
            <tr align="center">
                <td style="font-family:' . $fuente . ';font-size: 11px;"><p>Resolución N° 3753 Secretaria de Salud del Meta - Centro de Entrenamiento Para Trabajo Seguro en </p></td>
            </tr>
        </table>';


    $pdf->SetXY(25, 168.3);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="5" border="0">
            <tr align="center">
                <td style="font-family:' . $fuente . ';font-size: 11px;"><p>Alturas - Autorizada con Radicado 08SE2019220000000045994 Min.Trabajo Res. 1178 de 2017 - </p></td>
            </tr>
        </table>';

    $pdf->SetXY(25, 172);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="5" border="0">
            <tr align="center">
                <td style="font-family:' . $fuente . ';font-size: 11px;"><p>Certificado N° CS-CER709434 Organismo Certificado ICONTEC</p></td>
            </tr>
        </table>';

    $pdf->SetXY(25, 176);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="0" border="0">
            <tr align="justify">
                <td style="font-family:' . $fuente . ';font-size: 7.1px;color:#ffffff"><p>Para verificar la autenticidad del presente</p></td>
            </tr>
        </table>';

    $pdf->SetXY(234, 171.5);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="0" border="0">
            <tr align="justify">
                <td style="font-family:' . $fuente . ';font-size: 6.75px;color:#ffffff"><p>documento dirigirse a la pagina web</p></td>
            </tr>
        </table>';

    $pdf->SetXY(234, 174);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="0" border="0">
            <tr align="justify">
                <td style="font-family:' . $fuente . ';font-size: 6.75px;color:#ffffff"><p>www.gricompany.co o a los telefonos</p></td>
            </tr>
        </table>';

    $pdf->SetXY(234, 176.5);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="0" border="0">
            <tr align="justify">
                <td style="font-family:' . $fuente . ';font-size: 6.75px;color:#ffffff"><p>celulares 3143257703 o 3142114658.</p></td>
            </tr>
        </table>';

    $pdf->SetXY(234, 179);
    // $pdf->WriteHTML($html);

    $html = '<table cellspacing="0" cellpadding="5" border="0">
            <tr align="center">
                <td style="font-family:' . $fuente . ';font-size: 10px;">' . $GLOBALS['txt4'] . ' en la ciudad de ' . $ciudad_expedicion . ', el <span style="color:#d40e22">' . $expedido . '</span><br>' . $direccion . '<br>Codigo de validación: ' . $GLOBALS['codid'] . ' </td>
            </tr>
        </table>';

    $pdf->SetXY(14, 189);
    $pdf->WriteHTML($html);


    $style = array(
        'padding' => 'auto',
        'fgcolor' => array(0, 0, 0),
        'bgcolor' => false,
    );

    $pdf->SetMargins(0, 0, 0);
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);

    $pdf->write2DBarcode($qrcode, 'QRCODE,H', 241, 146.4, 24.5, 24.5, $style, 'N');


    $pdf->Output('CERTIFICACION-' . $identifica . '.pdf', 'I');
/* } else {
    header('Location: ../home');
} */
