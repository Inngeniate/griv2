<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Algeria';
$up=-24;
$ut=12;
$dw=750;
$diff='';
$originalsize=26880;
$enc='';
$file='algeria.z';
$ctg='algeria.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-14 -223 785 746]','ItalicAngle'=>0,'Ascent'=>617,'Descent'=>-103,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>17,'StemH'=>7,'AvgWidth'=>575,'MaxWidth'=>824,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
