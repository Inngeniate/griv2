<?php
require "conexion.php";
@$informacion = array();
$opc          = $_REQUEST['accion'];
$data         = $_POST['empleado'];
$carpeta_firmas = '../Firmas_empleados/';

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

switch ($opc) {
    case 'nuevo_empleado':
        $firma = '';

        if (isset($data['firma']) && $data['firma'] != '' && $data['firma'] != 1) {
            $imgData = base64_decode(substr($data['firma'], 22));
            $file    = $carpeta_firmas . date('YmdHis') . '.png';

            $fp = fopen($file, 'w');
            if (fwrite($fp, $imgData)) {
                $firma = substr($file, 3);
            }
        }

        $datos = [
            'nombre_em' => $data['nombre'],
            'identificacion_em' => $data['identifica'],
            'cargo_em' => $data['cargo'],
            'correo_em' => $data['correo'],
            'telefono_em' => $data['telefono'],
            'firma_em' => $firma,
            'tipo_em' => 'E'
        ];

        $nuevo = $db
            ->insert('empleados', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        @$buscar_nom = $_POST['nom'];

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        $list = $db
            ->where('nombre_em', '%' . $buscar_nom . '%', 'LIKE')
            ->objectBuilder()->get('empleados');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $registros = $db
            ->where('nombre_em', '%' . $buscar_nom . '%', 'LIKE')
            ->orderBy('Id_em', 'DESC')
            ->objectBuilder()->paginate('empleados', $pagina);

        $total     = $db->count;
        $filas     = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $filas .= '<tr id="' . $res->Id_em . '">
                                <td><p>' . $res->nombre_em . '</p></td>
                                <td><p>' . $res->identificacion_em . '</p></td>
                                <td><p>' . $res->cargo_em . '</p></td>
                                <td><p>' . $res->correo_em . '</p></td>
                                <td><p>' . $res->telefono_em . '</p></td>
                                <td>
                                <a href="empleados_edt?empleado=' . $res->Id_em . '" target="_blank" title="Editar" class="editar"><span class="icon-editar"></span></a>
                                <a href="javascript://" title="Eliminar" class="eliminar"><span class="icon-eliminar"></span></a>
                                </td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;
        echo json_encode($informacion);
        break;
    case 'editar_empleado':
        $datos = [
            'nombre_em' => $data['nombre'],
            'identificacion_em' => $data['identifica'],
            'cargo_em' => $data['cargo'],
            'correo_em' => $data['correo'],
            'telefono_em' => $data['telefono'],
        ];

        $act = $db
            ->where('Id_em', $data['idempleado'])
            ->update('empleados', $datos);

        if ($act) {
            if (isset($data['firma']) && $data['firma'] != '' && $data['firma'] != 1) {
                $imgData = base64_decode(substr($data['firma'], 22));
                $file    = $carpeta_firmas . date('YmdHis') . '.png';

                $fp = fopen($file, 'w');
                if (fwrite($fp, $imgData)) {
                    $comprobar = $db
                        ->where('Id_em', $data['idempleado'])
                        ->objectBuilder()->get('empleados');

                    $rg = $comprobar[0];

                    if ($rg->firma_em != '' && file_exists('../' . $rg->firma_em)) {
                        unlink('../' . $rg->firma_em);

                        $registro = $db
                            ->where('Id_em', $data['idempleado'])
                            ->update('empleados', ['firma_em' => '']);
                    }

                    $firma = substr($file, 3);

                    $registro = $db
                        ->where('Id_em', $data['idempleado'])
                        ->update('empleados', ['firma_em' => $firma]);
                } else {
                    $informacion['error'][] = "La firma no pudo ser adjuntada";
                }
            } elseif ($data['firma'] == 1) {
                $registro = $db
                    ->where('Id_em', $data['idempleado'])
                    ->objectBuilder()->get('empleados');

                $rg = $registro[0];

                if ($rg->firma_em != '' && file_exists('../' . $rg->firma_em)) {
                    unlink('../' . $rg->firma_em);
                    $registro = $db
                        ->where('Id_em', $data['idempleado'])
                        ->update('empleados', ['firma_em' => '']);
                }
            }

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'eliminar_empleado':
        $eli = $db
            ->where('Id_em', $data['idempleado'])
            ->delete('empleados');

        if ($elim) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
}

function limpiar($String)
{
    $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
    $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "A", $String);
    $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "I", $String);
    $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
    $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
    $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "E", $String);
    $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
    $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "O", $String);
    $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
    $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "U", $String);
    $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
    $String = str_replace("ç", "c", $String);
    $String = str_replace("Ç", "C", $String);
    $String = str_replace("ñ", "n", $String);
    $String = str_replace("Ñ", "N", $String);
    $String = str_replace("Ý", "Y", $String);
    $String = str_replace("ý", "y", $String);
    $String = preg_replace('/\s+/', '_', $String);
    $String = str_replace(array('(', ')'), '', $String);
    return $String;
}
