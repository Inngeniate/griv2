<?php
require "conexion.php";
@$informacion = array();
$data         = $_REQUEST['registro'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j H:i:s');

switch ($data['accion']) {
    case 'nuevo_reg':
        session_start();

        $datos = [
            'creacion_cl' => $nfecha,
            'nombre_cl' => $data['nombre'],
            'identificacion_cl' => $data['identificacion'],
            'expedida_cl' => $data['expedicion'],
            'labora_cl' => $data['labora'],
            'cargo_cl' => $data['cargo'],
            'salario_cl' => $data['salario']
        ];

        $nuevo = $db
            ->insert('certificados_laborales', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['registro'] = $nuevo;
        } else {
            $informacion['status'] = false;
            $informacion['msg']    = "Ha ocurrido un error, intentelo de nuevo más tarde.";
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        session_start();

        if (empty($data['id'])) {
            $data['id'] = '%';
        }

        if (empty($data['nom'])) {
            $data['nom'] = '%';
        }

        $list = $db
            ->where('nombre_cl', '%' . $data['nom'] . '%', 'LIKE')
            ->where('identificacion_cl', '%' . $data['id'] . '%', 'LIKE')
            ->objectBuilder()->get('certificados_laborales');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('nombre_cl', '%' . $data['nom'] . '%', 'LIKE')
            ->where('identificacion_cl', '%' . $data['id'] . '%', 'LIKE')
            ->orderBy('Id_cl', 'DESC')
            ->objectBuilder()->paginate('certificados_laborales', $pagina);

        $total = $db->count;
        $filas = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $filas .= '<tr id="' . $res->Id_cl . '">
                                <td><p>' . $res->Id_cl . '</p></td>
                                <td><p>' . $res->creacion_cl . '</p></td>
                                <td><p>' . $res->identificacion_cl . '</p></td>
                                <td><p>' . $res->nombre_cl . '</p></td>
                                <td><p>' . $res->cargo_cl . '</p></td>
                                <td><a href="certificado-laboral-editar?ctr=' . $res->Id_cl . '" target="_blank"><span class="icon-editar"></span></a></td>
								<td><a href="javascript://" class="eliminar"><span class="icon-eliminar"></span></a></td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                            <td colspan="4"><p style="text-align:center">No hay registros</p></td>
                        </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;
        echo json_encode($informacion);
        break;
    case 'editar_registro':
        $datos = [
            'nombre_cl' => $data['nombre'],
            'identificacion_cl' => $data['identificacion'],
            'expedida_cl' => $data['expedicion'],
            'labora_cl' => $data['labora'],
            'hasta_cl'=> $data['labora_fin'],
            'cargo_cl' => $data['cargo'],
            'salario_cl' => $data['salario'],
            'edicion_cl' => $nfecha
        ];

        $nuevo = $db
            ->where('Id_cl', $data['id'])
            ->update('certificados_laborales', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
            $informacion['registro'] = $data['id'];
        } else {
            $informacion['status'] = false;
            $informacion['msg']    = "Ha ocurrido un error, intentelo de nuevo más tarde.";
        }

        echo json_encode($informacion);
        break;
    case 'eliminar':
        $db->where('Id_cl', $data['id'])->delete('certificados_laborales');
        echo json_encode(array('success' => true));
        break;
}
