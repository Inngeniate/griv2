<?php
require "conexion.php";

// php tcpdf_addfont.php

$id     = $_REQUEST['id'];
// $fuente = $_REQUEST['fuente'];
$fuente = 'dejavusansb';
$cursos = '';
$cont   = 0;
$meses  = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

$comprobar = $db
    ->where('registro_ced', $id)
    ->where('comprobado_ced', 1)
    ->objectBuilder()->get('codigos_exportados_certificados');

if ($db->count > 0 || $id < 105346) {
    $registros = $db
        ->where('Id', $id)
        ->groupBy('certificado')
        ->orderBy('Id', 'ASC')
        ->objectBuilder()->get('registros', null, ' *, SUM(horas) AS horas');

    $total = $db->count;

    foreach ($registros as $rsg) {
        $nombres   = trim($rsg->nombre_primero) . ' ' . trim($rsg->nombre_segundo);
        $apellidos = trim($rsg->apellidos);

        $nombre     = strtoupper($nombres . ' ' . $apellidos);
        $tipoid     = $rsg->tipo_ident;
        $identifica = $rsg->numero_ident;
        $expedido   = date('d-m-Y', strtotime($rsg->fecha_inicio));
        $inicio     = $rsg->fecha_inicio;
        $finalizo   = $rsg->fecha_fin;

        if ($inicio != '' & $inicio != '0000-00-00') {
            $inicio  = explode('-', $inicio);
            $inicio  = $inicio[2] . ' de ' . $meses[$inicio[1] - 1] . ' de ' . $inicio[0];
        }

        if ($finalizo != '' & $finalizo != '0000-00-00') {
            $finalizo  = explode('-', $finalizo);
            $finalizo  = $finalizo[2] . ' de ' . $meses[$finalizo[1] - 1] . ' de ' . $finalizo[0];
        }

        $GLOBALS['codid']        = $rsg->Id;

        if ($rsg->fecha_vigencia != '0000-00-00') {
            $GLOBALS['vigencia']     = date('d-m-Y', strtotime($rsg->fecha_vigencia));
        } else {
            $GLOBALS['vigencia'] = '';
        }

        $GLOBALS['capacitacion'] = $rsg->capacitacion;

        if ($cont == 0) {
            $c1 = 'border-top:1px solid #000;';
        } else {
            $c1 = '';
        }

        if ($cont == ($total - 1)) {
            $c2 = 'border-bottom:1px solid #000;';
        } else {
            $c2 = '';
        }

        $ls_curso = '';

        $certificaciones = $db
            ->where('Id_ct', $rsg->certificado)
            ->objectBuilder()->get('certificaciones');

        if ($db->count > 0) {
            $rsc      = $certificaciones[0];
            $ls_curso = $rsc->nombre;
        }

        if ($GLOBALS['capacitacion'] != 'ALTURAS' && $GLOBALS['capacitacion'] != 'ESPACIOS CONFINADOS') {
            $txt3 = '';
            $GLOBALS['txt4'] = 'Dado';
            $GLOBALS['expide'] = $expedido;
        } else {
            $txt3 = '<br><span style="font-family:' . $fuente . ';font-size: 11.5px;">Realizado en la ciudad de Villavicencio, del ' . $inicio . ' a ' . $finalizo . '</span>';
            $GLOBALS['expide'] = date('d-m-Y');
            $GLOBALS['txt4'] = 'Expedido';
        }

        $cursos .= '<strong style="font-family:' . $fuente . ';font-size: 24px;color:#d40e22">' . $ls_curso . '</strong><br><span style="font-family:' . $fuente . ';font-size: 11.5px;">Con Una Intensidad De (' . $rsg->horas . ') Horas</span>' . $txt3;

        $cont++;

        $vigencia = '';

        if ($rsg->fecha_vigencia != '0000-00-00' && $rsg->fecha_vigencia != null && $rsg->fecha_vigencia != '') {
            $vigencia = date('d-m-Y', strtotime($rsg->fecha_vigencia));
        }

        $qrcode = $rsg->Id . ' ' . $nombre . ' ' . $tipoid . ' ' . $identifica . ' ' . $ls_curso . ' ' . $vigencia . ' ' . 'http://www.gricompany.co/' . ' ' . '3143257703';

        $tipo_entrenador = 'TSA';

        if ($rsg->entrenador != 0) {
            $entrenadores = $db
                ->where('Id_en', $rsg->entrenador)
                ->objectBuilder()->get('entrenadores');

            if ($db->count > 0) {
                $rent = $entrenadores[0];
                $entrenador = $rent->nombre_en . ' ' . $rent->apellido_en;
                $tsa = $rent->tsa_reg;
                $sena = $rent->sena_reg;
                $licencia = $rent->licencia_sst;

                if ($rent->firma_en != '') {
                    $firma = '../' . $rent->firma_en;
                } else {
                    $firma = '';
                }

                switch ($rent->competencia_en) {
                    case 'alturas':
                        $tipo_entrenador = 'TSA';
                        break;
                    case 'espaciosconfinados':
                        $tipo_entrenador = 'EC';
                        break;
                    case 'cea':
                        $tipo_entrenador = 'CEA';
                        break;
                }
            }
        }

        $ciudad_expedicion = '';

        if ($rsg->ciudad != '') {
            $ciudades = [
                'cucuta' => 'Cúcuta',
                'villavicencio' => 'Villavicencio',
                '' => ''
            ];

            $ciudad_expedicion = $ciudades[$rsg->ciudad];
        }
    }

    include 'TCPDF/tcpdf.php';

    class MYPDF extends TCPDF
    {
        public function Header()
        {
            $bMargin         = $this->getBreakMargin();
            $auto_page_break = $this->AutoPageBreak;
            $this->SetAutoPageBreak(false, 0);
            // if ($GLOBALS['capacitacion'] == 'ALTURAS' || $GLOBALS['capacitacion'] == 'ESPACIOS CONFINADOS' || $GLOBALS['capacitacion'] == 'CEA') {
            $image_file = '../images/certificado_alturas.jpg';

            if ($GLOBALS['capacitacion'] == 'ESPACIOS CONFINADOS') {
                $image_file = '../images/certificado_espacios.jpg';
            }
            // } else {
            //     $image_file = '../images/diplomafondo.jpg';
            // }
            $this->Image($image_file, 0, 0, 220, 280, '', '', '', false, 300, '', false, false, 0);
            $this->SetAutoPageBreak($auto_page_break, $bMargin);
            $this->setPageMark();
        }
        public function Footer()
        {
        }
    }

    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
    $pdf->setPageOrientation('p');

    $pdf->SetProtection(array('modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), '', null, 3, null);

    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // PDF_MARGIN_TOP
    $pdf->SetMargins(35, 20, 25);
    // $pdf->SetPrintHeader(false);
    // $pdf->setPrintFooter(false);

    // set auto page breaks
    // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->SetAutoPageBreak(true, 0);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->AddPage();
    // $poppinsblack = TCPDF_FONTS::addTTFfont('libs/fonts/poppins.black.ttf', 'TrueTypeUnicode', '', 96);

    if ($GLOBALS['capacitacion'] == 'ALTURAS') {
        $txt1 = 'CERTIFICADO DE CAPACITACIÓN Y ENTRENAMIENTO<br>PARA TRABAJO EN ALTURAS';
        $style1 = 'font-family:' . $fuente . ';font-size: 18px;';
        $txt2 = 'Curso y aprobó satisfactoriamente la acción de formación y entrenamiento:';
        $txt3 = '';
    } elseif ($GLOBALS['capacitacion'] == 'ESPACIOS CONFINADOS') {
        $logo = '../images/logoalturas.png';
        $logo_w = 280;
        $txt1 = 'CERTIFICADO DE CAPACITACIÓN Y ENTRENAMIENTO<br>PARA TRABAJO EN ESPACIOS CONFINADOS';
        $style1 = 'font-family:' . $fuente . ';font-size: 18px;';
        $txt2 = 'Curso y aprobó satisfactoriamente la acción de formación y entrenamiento:';
        $txt3 = '';
    } else {
        $logo = '../images/logogri3.png';
        $logo_w = 420;
        $txt1 = 'CERTIFICA';
        $style1 = 'font-family:' . $fuente . ';font-size: 24px;';
        $txt2 = 'Realizo Y Aprobó El Curso De:';
        $txt3 = '';
    }

    $validez = '';

    if ($GLOBALS['vigencia'] != '') {
        $validez = ' ';
    }

    $html = '<table cellspacing="0" cellpadding="5" border="0" >
            <tr>
                <td align="center" style="' . $style1 . '"><strong>' . $txt1 . '</strong></td>
            </tr>
            <tr>
                <td align="center"><strong style="font-family:' . $fuente . ';font-size: 26px;">' . $nombre . '</strong>
                    <br><strong style="font-family:' . $fuente . ';font-size: 18px;">' . $tipoid . ' ' . $identifica . '</strong>
                </td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td align="center"><span style="font-family:' . $fuente . ';font-size: 12px;">' . $txt2 . '</span><br>' . $cursos . '</td>
            </tr>
            <tr align="center">
                <td style="font-family:' . $fuente . ';font-size: 11px;">' . $GLOBALS['txt4'] . ' en la ciudad de ' . $ciudad_expedicion . ', el <span style="color:#d40e22">' . $GLOBALS['expide'] . '</span><br>Codigo de validación: ' . $GLOBALS['codid'] . ' válido hasta <span style="color:#d40e22">' . $GLOBALS['vigencia'] . '</span></td>
            </tr>
        </table>
        <br><br><br><br><br><br><br><br>';

    $pdf->SetXY(25, 70);
    $pdf->WriteHTML($html);

    if ($GLOBALS['capacitacion'] == 'ALTURAS' || $GLOBALS['capacitacion'] == 'ESPACIOS CONFINADOS' || $GLOBALS['capacitacion'] == 'CEA') {

        if ($firma != '') {
            $pdf->Image($firma, 48, 167, 35, 20, 'PNG', '');
        }

        $html = '<table cellspacing="0" cellpadding="0" border="0" >
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;"><strong>' . $entrenador . '</strong></td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Entrenador ' . $tipo_entrenador . ' Reg. ' . $tsa . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Evaluador de Competencias laborales Sena Reg ' . $sena . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Licencia, SST ' . $licencia . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Gestion del Riesgo Integral Company S.A.S</td>
                </tr>
            </table>';

        $pdf->writeHTMLCell('', '', -62.5, 190, $html, 0, 0, 0, true, 'J', true);
    } else {
        $entrenador = 'Anthony Alexis Garay Riaño';
        $tsa = '112019-0630-ETSA Min. Trabajo';
        $sena = '9543001986852';
        $licencia = '1748 / 2013';
        $firma = '../images/firmaanthony.png';

        $pdf->Image($firma, 48, 171, 35, 20, 'PNG', '');

        $html = '<table cellspacing="0" cellpadding="0" border="0" >
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;"><strong>' . $entrenador . '</strong></td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Entrenador TSA Reg. ' . $tsa . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Evaluador de Competencias laborales Sena Reg ' . $sena . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Licencia, SST ' . $licencia . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Gestion del Riesgo Integral Company S.A.S</td>
                </tr>
            </table>';

        $pdf->writeHTMLCell('', '', -62.5, 190, $html, 0, 0, 0, true, 'J', true);
    }

    $html = '<table cellspacing="0" cellpadding="0" border="0" >
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;"><strong>Lady Marcela Sánchez Martínez</strong></td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Gerente General</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Licencia. SST 3752 / 2016</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:' . $fuente . ';font-size: 9px;">Gestion del Riesgo Integral Company S.A.S</td>
                </tr>
            </table>';

    $pdf->writeHTMLCell('', '', 122, 190, $html, 0, 0, 0, true, 'J', true);


    $style = array(
        'padding' => 'auto',
        'fgcolor' => array(255, 255, 255),
        'bgcolor' => false, //array(255,255,255)
    );

    $pdf->SetMargins(0, 0, 0);
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);

    $pdf->write2DBarcode($qrcode, 'QRCODE,H', 180, 250, 28, 28, $style, 'N');


    $pdf->Output('CERTIFICACION-' . $identifica . '.pdf', 'I');
} else {
    header('Location: ../home');
}
