<?php
require "conexion.php";
@$informacion = array();
$data         = $_REQUEST['registro'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j H:i:s');

switch ($data['accion']) {
    case 'nueva_ficha':
        session_start();

        if (!isset($data['inicio'])) {
            $data['inicio'] = '01-01-01';
        }

        if (!isset($data['fin'])) {
            $data['fin'] = '01-01-01';
        }

        if (!isset($data['cantidad'])) {
            $data['cantidad'] = 0;
        }

        if (empty($data['entrenador'])) {
            $data['entrenador'] = 0;
        }

        $datos = [
            'competencia' => $data['competencia'],
            'entrenador' => $data['entrenador'],
            'niveles' => $data['niveles'],
            'codigo' => $data['codigo'],
            'inicio' => $data['inicio'],
            'fin' => $data['fin'],
            'cantidad' => $data['cantidad'],
            'en_uso' => 0,
            'ciudad' => $data['ciudad']
        ];

        $nuevo = $db
            ->insert('fichas_ministerio', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg']    = "Ha ocurrido un error, intentelo de nuevo más tarde.";
        }

        echo json_encode($informacion);
        break;
    case 'editar_ficha':
        session_start();

        if (!isset($data['inicio'])) {
            $data['inicio'] = '01-01-01';
        }

        if (!isset($data['fin'])) {
            $data['fin'] = '01-01-01';
        }

        if (!isset($data['cantidad'])) {
            $data['cantidad'] = 0;
        }

        if (empty($data['entrenador'])) {
            $data['entrenador'] = 0;
        }

        $datos = [
            'competencia' => $data['competencia'],
            'entrenador' => $data['entrenador'],
            'niveles' => $data['niveles'],
            'codigo' => $data['codigo'],
            'inicio' => $data['inicio'],
            'fin' => $data['fin'],
            'cantidad' => $data['cantidad'],
            'ciudad' => $data['ciudad']
        ];

        $nuevo = $db
            ->where('Id', $data['Idficha'])
            ->update('fichas_ministerio', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg']    = "Ha ocurrido un error, intentelo de nuevo más tarde.";
        }

        echo json_encode($informacion);
        break;
    case 'eliminar_ficha':
        $eliminar = $db
            ->where('Id', $data['Idficha'])
            ->delete('fichas_ministerio');

        if($eliminar){
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg']    = "Ha ocurrido un error, intentelo de nuevo más tarde.";
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        session_start();

        if (empty($data['id'])) {
            $data['id'] = '%';
        }

        if (empty($data['nom'])) {
            $data['nom'] = '%';
        }

        $list  = $db
            ->where('codigo', '%' . $data['nom'] . '%', 'LIKE')
            ->objectBuilder()->get('fichas_ministerio');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 50;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';
        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('codigo', '%' . $data['nom'] . '%', 'LIKE')
            ->orderBy('Id', 'DESC')
            ->objectBuilder()->paginate('fichas_ministerio', $pagina);

        $total = $db->count;
        $filas = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $competencia = '';

                $competencias = $db
                    ->where('Id_cp', $res->competencia)
                    ->objectBuilder()->get('competencias');

                foreach ($competencias as $rcp) {
                    $competencia = $rcp->nombre_cp;
                }

                $entrenador = '';

                $entrenadores = $db
                    ->where('Id_en', $res->entrenador)
                    ->objectBuilder()->get('entrenadores');

                foreach ($entrenadores as $rsc) {
                    $entrenador = $rsc->nombre_en . ' ' . $rsc->apellido_en;
                }

                $ciudades = [
                    'cucuta' => 'Cúcuta',
                    'villavicencio' => 'Villavicencio',
                    '' => ''
                ];

                $filas .= '<tr id="' . $res->Id . '">
                                <td><p>' . $res->codigo . '</p></td>
                                <td><p>' . $competencia . '</p></td>
                                <td><p>' . $entrenador . '</p></td>
                                <td><p style="text-align:center">' . $res->cantidad . '</p></td>
                                <td><p style="text-align:center">' . $ciudades[$res->ciudad] . '</p></td>
                                <td align="center">
                                    <a href="fichas-ministerio-editar?ficha=' . $res->Id . '" target="_blank" title="Editar" class="editar"><span class="icon-editar"></span></a>
                                    <a href="javascript://" title="Eliminar" class="eliminar"><span class="icon-eliminar"></span></a>
                                </td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="5"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;
        echo json_encode($informacion);
        break;
}
