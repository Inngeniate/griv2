<?php
require "conexion.php";
@$informacion = array();
$opc          = $_REQUEST['accion'];
$data         = $_POST['entrena'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

switch ($opc) {
    case 'nuevo_entrenador':
        $datos = [
            'nombre_en' => $data['nombre'],
            'apellido_en' => $data['apellido'],
            'tsa_reg' => $data['tsa'],
            'licencia_sst' => $data['licencia'],
            'nfpa_proboard_number' => $data['nfpa'],
            'escon_reg' => $data['escon_reg'],
            'competencia_en' => $data['capacitacion'],
            'estado_en' =>  $data['estado'],
        ];

        $nuevo = $db
            ->insert('entrenadores', $datos);

        if ($nuevo) {
            if (isset($data['firma']) && $data['firma'] != '' && $data['firma'] != 1) {
                $carpeta_firmas = '../Firmas_entrenadores/';
                $imgData = base64_decode(substr($data['firma'], 22));
                $file    = $carpeta_firmas . date('YmdHis') . '.png';

                $fp = fopen($file, 'w');
                if (fwrite($fp, $imgData)) {
                    $firma = substr($file, 3);

                    $registro = $db
                        ->where('Id_en', $nuevo)
                        ->update('entrenadores', ['firma_en' => $firma]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = "La firma no pudo ser adjuntada";
                }
            }

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        @$buscar_nom = $_POST['nom'];

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        $list = $db
            ->where('nombre_en', '%' . $buscar_nom . '%', 'LIKE')
            ->objectBuilder()->get('entrenadores');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';
        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('nombre_en', '%' . $buscar_nom . '%', 'LIKE')
            ->orderBy('Id_en', 'DESC')
            ->objectBuilder()->paginate('entrenadores', $pagina);

        $total = $db->count;
        $filas = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $competencia = '';
                $firma = '';

                $competencias = $db
                    ->where('alias_cp', $res->competencia_en)
                    ->objectBuilder()->get('competencias');

                if ($db->count > 0) {
                    $rcomp = $competencias[0];
                    $competencia = $rcomp->nombre_cp;
                }

                if ($res->firma_en != '') {
                    $firma = '<a href="' . $res->firma_en . '" target="_blank">Ver</a>';
                }

                $filas .= '<tr id="' . $res->Id_en . '">
                                <td><p>' . $res->nombre_en . '</p></td>
                                <td><p>' . $res->apellido_en . '</p></td>
                                <td><p>' . $competencia . '</p></td>
                                <td><p>' . ($res->estado_en == 1 ? "Activo" : "Inactivo") . '</p></td>
                                <td>' . $firma . '</td>
                                <td><a href="entrenadores_edt?entrenador=' . $res->Id_en . '" target="_blank" title="Editar" class="editar"><span class="icon-editar"></span></a></td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;

        echo json_encode($informacion);
        break;
    case 'editar_entrenador':
        $datos = [
            'nombre_en' => $data['nombre'],
            'apellido_en' => $data['apellido'],
            'tsa_reg' => $data['tsa'],
            'licencia_sst' => $data['licencia'],
            'nfpa_proboard_number' => $data['nfpa'],
            'escon_reg' => $data['escon_reg'],
            'competencia_en' => $data['capacitacion'],
            'estado_en' => $data['estado'],
        ];

        $act = $db
            ->where('Id_en', $data['identrenador'])
            ->update('entrenadores', $datos);

        if ($act) {

            if (isset($data['firma']) && $data['firma'] != '' && $data['firma'] != 1) {
                $carpeta_firmas = '../Firmas_entrenadores/';
                $imgData = base64_decode(substr($data['firma'], 22));
                $file    = $carpeta_firmas . date('YmdHis') . '.png';

                $fp = fopen($file, 'w');
                if (fwrite($fp, $imgData)) {
                    $comprobar = $db
                        ->where('Id_en', $data['identrenador'])
                        ->objectBuilder()->get('entrenadores');


                    if ($comprobar[0]->firma_en != '' && file_exists('../' . $comprobar[0]->firma_en)) {
                        unlink('../' . $comprobar[0]->firma_en);

                        $registro = $db
                            ->where('Id_en', $data['identrenador'])
                            ->update('entrenadores', ['firma_en' => '']);
                    }

                    $firma = substr($file, 3);

                    $registro = $db
                        ->where('Id_en', $data['identrenador'])
                        ->update('entrenadores', ['firma_en' => $firma]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = "La firma no pudo ser adjuntada";
                }
            } elseif ($data['firma'] == 1) {
                $registro = $db
                    ->where('Id_en', $data['identrenador'])
                    ->objectBuilder()->get('entrenadores');

                if ($registro[0]->firma_en != '' && file_exists('../' . $registro[0]->firma_en)) {
                    unlink('../' . $registro[0]->firma_en);

                    $registro = $db
                        ->where('Id_en', $data['identrenador'])
                        ->update('entrenadores', ['firma_en' => '']);
                }

                $informacion['status'] = true;
            }

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
}

function limpiar($String)
{
    $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
    $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "A", $String);
    $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "I", $String);
    $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
    $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
    $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "E", $String);
    $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
    $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "O", $String);
    $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
    $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "U", $String);
    $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
    $String = str_replace("ç", "c", $String);
    $String = str_replace("Ç", "C", $String);
    $String = str_replace("ñ", "n", $String);
    $String = str_replace("Ñ", "N", $String);
    $String = str_replace("Ý", "Y", $String);
    $String = str_replace("ý", "y", $String);
    $String = preg_replace('/\s+/', '_', $String);
    $String = str_replace(array('(', ')'), '', $String);
    return $String;
}
