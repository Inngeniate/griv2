<?php
require "conexion.php";
@$informacion = array();
@$data        = $_REQUEST['curso'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

switch ($data['opc']) {
    case 'valor_curso':
        $bus = $db
            ->where('Id', $data['curso'])
            ->objectBuilder()->get('precios');

        if ($db->count > 0) {
            $informacion['precio'] = $bus[0]->total;
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'registrar_pago':
        $fecha_registro = date('Y-m-d h:i:s');

        $datos = [
            'curso' => $data['curso'],
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'identificacion' => $data['identificacion'],
            'email' => $data['correo'],
            'telefono' => $data['telefono'],
            'direccion' => $data['direccion'],
            'creacion' => $fecha_registro,
            'transaccion' => '',
            'estado' => 'Pendiente',
            'invoice' => '',
            'cierre' => '0000:00:00 00:00:00'
        ];

        $pago = $db
            ->insert('pagos_cursos', $datos);

        if ($pago) {
            $bus = $db
                ->where('Id', $data['curso'])
                ->objectBuilder()->get('precios');

            $epayco = $db
                ->objectBuilder()->get('pagos_epayco');

            $informacion['infopago']['referencia']          = 'Gri-' . $pago;
            $informacion['infopago']['curso']               = $bus[0]->nombre;
            $informacion['infopago']['monto']               = $bus[0]->total;
            $informacion['infopago']['email_billing']       = $data['correo'];
            $informacion['infopago']['name_billing']        = $data['nombre'] . ' ' . $data['apellido'];
            $informacion['infopago']['address_billing']     = $data['direccion'];
            $informacion['infopago']['mobilephone_billing'] = $data['telefono'];
            $informacion['infopago']['number_doc_billing']  = $data['identificacion'];
            $informacion['infopago']['pago_key']            = $epayco[0]->epaycoKey;

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
            $informacion['msg']    = 'Lo sentimos, la operación no pudo realizarse, intentalo mas tarde.';
        }

        echo json_encode($informacion);
        break;
    case 'detalle_registro':
        $fecha_registro   = date('Y-m-d h:i:s');
        $transaccion_id   = $data['transaccion'];
        $franchise        = $data['franchise'];
        $token_referencia = $hashids->encode($transaccion_id);

        $referencia_extra = $data['referencia'];
        $referencia       = explode('-', $referencia_extra);
        $reinscripcion    = 0;
        $informacion['msg']      = 'El pago fue exitoso';

        $pago = $db
            ->where('Id', $referencia[1])
            ->objectBuilder()->get('pagos_cursos');

        $email          = $pago[0]->email;
        $identificacion = $pago[0]->identificacion;
        $nombre         = $pago[0]->nombre;
        $apellido       = $pago[0]->apellido;
        $telefono       = $pago[0]->telefono;

        /// actualizacion del pago

        $idplan = $pago[0]->curso;
        $idpago = $pago[0]->Id;

        $precios = $db
            ->where('Id', $idplan)
            ->objectBuilder()->get('precios');

        $nombre_curso  = $precios[0]->nombre;


        $pagar = $db
            ->where('Id', $referencia[1])
            ->update('pagos_cursos', ['transaccion' => $transaccion_id, 'medio' => $franchise, 'estado' => 'Aceptada', 'cierre' => $fecha_registro]);

        /////

        echo json_encode($informacion);
        break;
}
