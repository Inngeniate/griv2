<?php
require "conexion.php";
@$informacion = array();
@$opc         = $_REQUEST['accion'];

switch ($opc) {
    case 'listar':
        session_start();

        $list = $db
            ->where('competencia_ct', 'alturas')
            ->objectBuilder()->get('certificaciones');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 30;
        $pagina       = (int) (isset($_POST['pagina']) ? $_POST['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javsacript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $cursos = $db
            ->where('competencia_ct', 'alturas')
            ->objectBuilder()->paginate('certificaciones', $pagina);

        $$total  = $db->count;
        $filas  = '';

        if ($total > 0) {
            foreach ($cursos as $res) {
                $filas .= '<tr id="' . $res->Id_ta . '">
								<td><p>' . $res->Id_ta . '</p></td>
								<td><p class="cr-nombre">' . $res->nombre . '</p></td>';
                if ($_SESSION['usutipoggri'] == 'administrador') {
                    $filas .= '<td><a href="javascript://" title="Editar" class="editar"><span class="icon-editar"></span></a></td>';
                }
                $filas .= '</tr>';
            }
        } else {
            $filas = '<tr>
							<td colspan="3"><p style="text-align:center">No hay registros</p></td>
						</tr>';
        }
        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;
        echo json_encode($informacion);
        break;
    case 'nuevo_curso':
        $nombre = $_POST['nombre'];

        $comprobar = $db
            ->where('nombre', $nombre)
            ->objectBuilder()->get('ta_cursos');

        if ($db->count > 0) {
            $informacion['status'] = 'error-2';
        } else {
            $nuevo = $db
                ->insert('ta_cursos', ['nombre' => $nombre]);

            if ($nuevo) {
                $informacion['status'] = 'Correcto';
            } else {
                $informacion['status'] = 'error-1';
            }
        }

        echo json_encode($informacion);
        break;
    case 'editar_curso':
        $nombre  = $_POST['nombre'];
        $idcurso = $_POST['idcurso'];

        $comprobar = $db
            ->where('nombre', $nombre)
            ->where('Id_ta', $idcurso, '!=')
            ->objectBuilder()->get('ta_cursos');

        if ($db->count > 0) {
            $informacion['status'] = 'error-2';
        } else {
            $editar = $db
                ->where('Id_ta', $idcurso)
                ->update('ta_cursos', ['nombre' => $nombre]);

            if ($editar) {
                $informacion['status'] = 'Correcto';
            } else {
                $informacion['status'] = 'error-1';
            }
        }

        echo json_encode($informacion);
        break;
}
