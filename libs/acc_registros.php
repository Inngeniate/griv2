<?php
require "conexion.php";
@$informacion = array();
@$opc         = $_REQUEST['accion'];
@$buscar_nom  = $_REQUEST['nom'];
@$buscar_apel = $_POST['apell'];
@$buscar_iden = $_REQUEST['id'];
@$idregistro = $_REQUEST['idregistro'];

@$nombres     = strtoupper($_POST['nombre']);
@$apellidos   = strtoupper($_POST['apellido']);
@$tipo_id     = $_POST['tipo_id'];
@$identifica  = $_POST['identifica'];
@$sexo        = $_POST['sexo'];
@$direccion   = $_POST['direccion'];
@$telefono    = $_POST['telefono'];
@$email       = $_POST['email'];
@$certificado = $_POST['certificado'];
@$inicio      = $_POST['f_inicio'];
@$finaliza    = $_POST['f_fin'];

@$vigencia = $_POST['f_vigencia'];
@$horas    = $_POST['horas'];
@$vendedor = $_POST['vendedor'];
@$precio   = $_POST['precio'];
@$tpago    = $_POST['tpago'];

@$codigo_ficha  = $_POST['codigo_ficha'];

@$vencimiento  = $_POST['vencimiento'];
@$licencia     = $_POST['licencia'];
@$capacitacion = $_POST['capacitacion'];
@$capacitacion_alias = $_POST['capacitacion_alias'];
@$rh           = $_POST['rh'];
@$cargo        = $_POST['cargo'];
@$empresa      = $_POST['empresa'];
@$id           = $_POST['id'];

@$carpeta_fotos = '../Carnet';
@$foto1         = $_FILES['foto']['name'];
@$foto1_tmp     = $_FILES['foto']['tmp_name'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j H:i:s');

switch ($opc) {
    case 'nuevo_reg':
        session_start();

        $entrenador = 0;
        $carga_foto = 0;

        if (isset($_POST['entrenador']) && $_POST['entrenador'] != '') {
            $entrenador = $_POST['entrenador'];
        }

        if (!isset($_POST['licencia'])) {
            $licencia = '';
        }

        if (isset($_POST['carga_foto'])) {
            $carga_foto = $_REQUEST['carga_foto'];
        }

        $primerNombre = strtoupper($_POST['primer_nombre']);
        $segundoNombre = strtoupper($_POST['segundo_nombre']);

        $bus = $db
            ->where('numero_ident', $identifica)
            ->where('certificado', $certificado)
            ->where('fecha_inicio', $inicio)
            ->objectBuilder()->get('registros');

        if ($db->count == 0) {
            $cliente = '';

            if (isset($_REQUEST['cliente']['id'])) {
                $cliente = $_REQUEST['cliente']['id'];
            }

            $datos = [
                'tipo_ident' => $tipo_id,
                'numero_ident' => $identifica,
                'nombre_primero' => $primerNombre,
                'nombre_segundo' => $segundoNombre,
                'apellidos' => $apellidos,
                'sexo' => $sexo,
                'direccion' => $direccion,
                'telefono' => $telefono,
                'correo' => $email,
                'capacitacion' => $capacitacion,
                'entrenador' => $entrenador,
                'certificado' => $certificado,
                'fecha_inicio' => $inicio,
                'fecha_fin' => $finaliza,
                'fecha_vigencia' => $vigencia,
                'horas' => $horas,
                'vencimiento_ct' => $vencimiento,
                'rh' => $rh,
                'cargo' => $cargo,
                'empresa' => $empresa,
                'licencia' => $licencia,
                'cargafoto' => $carga_foto,
                'vendedor' => $vendedor,
                'tpago' => '',
                'precio' => '',
                'cliente' => $cliente,
                'codigo_ficha' => $codigo_ficha,
                'creacion' => $nfecha,
                'creadopor' => $_SESSION['usuloggri']
            ];

            if (isset($_REQUEST['ciudad'])) {
                $datos['ciudad'] = $_REQUEST['ciudad'];
            }

            $registro = $db
                ->insert('registros', $datos);

            if ($registro) {
                $nombre_foto = trim($identifica) . '-' . $id . '-' . $foto1;
                $archivador = $carpeta_fotos . '/' . $nombre_foto;

                if (!empty($foto1)) {
                    if (!move_uploaded_file($foto1_tmp, $archivador)) {
                        $informacion['errores'] = 'foto1';
                    } else {
                        $act = $db
                            ->where('Id', $registro)
                            ->update('registros', ['foto_ct' => $archivador]);
                    }
                }

                if (isset($_REQUEST['capacitacion_ad'])) {
                    for ($i = 0; $i < count($_REQUEST['capacitacion_ad']); $i++) {
                        $datos = [
                            'tipo_ident' => $tipo_id,
                            'numero_ident' => $identifica,
                            'nombre_primero' => $primerNombre,
                            'nombre_segundo' => $segundoNombre,
                            'apellidos' => $apellidos,
                            'sexo' => $sexo,
                            'direccion' => $direccion,
                            'telefono' => $telefono,
                            'correo' => $email,
                            'capacitacion' => $_REQUEST['capacitacion_ad'][$i],
                            'certificado' => $_REQUEST['certificado_ad'][$i],
                            'fecha_inicio' => $_REQUEST['f_inicio_ad'][$i],
                            'fecha_vigencia' => $_REQUEST['f_vigencia_ad'][$i],
                            'horas' => $_REQUEST['horas_ad'][$i],
                            'vencimiento_ct' => $_REQUEST['vencimiento_ad'][$i],
                            'rh' => $rh,
                            'cargo' => $cargo,
                            'empresa' => $empresa,
                            'licencia' => $licencia,
                            'cargafoto' => $carga_foto,
                            'vendedor' => $vendedor,
                            'tpago' => '',
                            'precio' => '',
                            'cliente' => $cliente,
                            'creacion' => $nfecha,
                            'creadopor' => $_SESSION['usuloggri']
                        ];

                        if (isset($_REQUEST['ciudad'])) {
                            $datos['ciudad'] = $_REQUEST['ciudad'];
                        }

                        $adicional = $db
                            ->insert('registros', $datos);

                        if ($adicional) {
                            $nombre_foto = trim($identifica) . '-' . $adicional . '-' . $foto1;

                            $archivador_adicional = $carpeta_fotos . '/' . $nombre_foto;

                            if (!empty($foto1)) {
                                if (!copy($archivador, $archivador_adicional)) {
                                    $informacion['errores'] = 'foto1';
                                } else {
                                    $act = $db
                                        ->where('Id', $adicional)
                                        ->update('registros', ['foto_ct' => $archivador_adicional]);
                                }
                            }
                        }
                    }
                }

                $informacion['registro'] = '';

                if ($capacitacion_alias == 'alturas' || $capacitacion_alias == 'alturasres.4272') {
                    $fecha = date('Y-m-j');
                    // $nombre = $nombres . ' ' . $apellidos;
                    $gs =  '';

                    if (strpos($rh, '+') !== false) {
                        $gs =  '+';
                    }

                    if (strpos($rh, '-') !== false) {
                        $gs =  '-';
                    }

                    /* $rh = str_replace('+', '', $rh);
                    $rh = str_replace('-', '', $rh); */

                    $competencia = '';


                    $competencias = $db
                        ->where('alias_cp', $capacitacion_alias)
                        ->objectBuilder()->get('competencias');

                    if ($db->count > 0) {
                        $rcp = $competencias[0];
                        $competencia = $rcp->Id_cp;
                    }

                    $nacionalidad = $_REQUEST['registro']['nacionalidad'];
                    $educativo = $_REQUEST['registro']['nveducacion'];
                    $sector = $_REQUEST['registro']['sector'];
                    $f_nacimiento = $_REQUEST['registro']['fnacimiento'];
                    $expedicion = $_REQUEST['registro']['identifica_origen'];
                    $edad = $_REQUEST['registro']['edad'];

                    $cliente = '';

                    if (isset($_REQUEST['cliente']['id'])) {
                        $cliente = $_REQUEST['cliente']['id'];
                    }

                    $arl = '';

                    if (isset($_REQUEST['cliente']['arl'])) {
                        $arl = $_REQUEST['cliente']['arl'];
                    }

                    $datos = [
                        'competencia_al' => $competencia,
                        'curso_al' => $certificado,
                        'sector_al' => $sector,
                        'ciudad_al' => $direccion,
                        'fecha_al' => $inicio,
                        'nacionalidad_al' => $nacionalidad,
                        'nombre_primero_al' => $primerNombre,
                        'nombre_segundo_al' => $segundoNombre,
                        'apellidos_al' => $apellidos,
                        'tipo_doc_al' => $tipo_id,
                        'documento_al' => $identifica,
                        'documento_de_al' => $expedicion,
                        'fnacimiento_al' => $f_nacimiento,
                        'edad_al' => $edad,
                        'telefono_al' => $telefono,
                        'rh_al' => $rh,
                        'arl_al' => $arl,
                        'email_al' => $email,
                        'profesion_al' => $cargo,
                        'empresa_al' => $empresa,
                        'nv_educativo_al' => $educativo,
                        'codigo_ficha' => $codigo_ficha,
                        'cliente' => $cliente,
                        'creacion_al' => $nfecha,
                        'creadopor' => $_SESSION['usuloggri']
                    ];

                    $registro_alturas = $db
                        ->insert('registros_alturas', $datos);

                    if ($registro_alturas) {
                        //! Registro ficha
                        $fichas = $db
                            ->where('Id', $codigo_ficha)
                            ->update('fichas_ministerio', ['en_uso' => $db->inc(1)]);
                    }
                }

                if ($capacitacion_alias == 'espaciosconfinados') {
                    $fecha = date('Y-m-j');
                    // $nombre = $nombres . ' ' . $apellidos;
                    $gs =  '';

                    if (strpos($rh, '+') !== false) {
                        $gs =  '+';
                    }

                    if (strpos($rh, '-') !== false) {
                        $gs =  '-';
                    }

                    /* $rh = str_replace('+', '', $rh);
                    $rh = str_replace('-', '', $rh); */

                    $competencia = '';

                    $competencias = $db
                        ->where('alias_cp', $capacitacion_alias)
                        ->objectBuilder()->get('competencias');

                    if ($db->count > 0) {
                        $rcp = $competencias[0];
                        $competencia = $rcp->Id_cp;
                    }

                    $nacionalidad = $_REQUEST['registro']['nacionalidad'];
                    $educativo = $_REQUEST['registro']['nveducacion'];
                    $sector = $_REQUEST['registro']['sector'];
                    $f_nacimiento = $_REQUEST['registro']['fnacimiento'];
                    $expedicion = $_REQUEST['registro']['identifica_origen'];
                    $edad = $_REQUEST['registro']['edad'];

                    $arl = '';

                    if (isset($_REQUEST['cliente']['arl'])) {
                        $arl = $_REQUEST['cliente']['arl'];
                    }

                    $datos = [
                        'competencia_al' => $competencia,
                        'curso_al' => $certificado,
                        'sector_al' => $sector,
                        'ciudad_al' => $direccion,
                        'fecha_al' => $inicio,
                        'nacionalidad_al' => $nacionalidad,
                        'nombre_primero_al' => $primerNombre,
                        'nombre_segundo_al' => $segundoNombre,
                        'apellidos_al' => $apellidos,
                        'tipo_doc_al' => $tipo_id,
                        'documento_al' => $identifica,
                        'documento_de_al' => $expedicion,
                        'fnacimiento_al' => $f_nacimiento,
                        'edad_al' => $edad,
                        'telefono_al' => $telefono,
                        'rh_al' => $rh,
                        'arl_al' => $arl,
                        'email_al' => $email,
                        'profesion_al' => $cargo,
                        'empresa_al' => $empresa,
                        'nv_educativo_al' => $educativo,
                        'codigo_ficha' => $codigo_ficha,
                        'cliente' => $cliente,
                        'creacion_al' => $nfecha,
                        'creadopor' => $_SESSION['usuloggri']
                    ];

                    $registro_espacios = $db
                        ->insert('registros_espacios_confinados', $datos);

                    if ($registro_espacios) {
                        //! Registro ficha
                        $fichas = $db
                            ->where('Id', $codigo_ficha)
                            ->update('fichas_ministerio', ['en_uso' => $db->inc(1)]);
                    }
                }

                if ($capacitacion_alias == 'alturasres.4272') {
                    $fecha = date('Y-m-j');
                    // $nombre = $nombres . ' ' . $apellidos;
                    $gs =  '';

                    if (strpos($rh, '+') !== false) {
                        $gs =  '+';
                    }

                    if (strpos($rh, '-') !== false) {
                        $gs =  '-';
                    }

                    $competencia = '';

                    $competencias = $db
                        ->where('alias_cp', $capacitacion_alias)
                        ->objectBuilder()->get('competencias');

                    if ($db->count > 0) {
                        $rcp = $competencias[0];
                        $competencia = $rcp->Id_cp;
                    }

                    $nacionalidad = $_REQUEST['registro']['nacionalidad'];
                    $educativo = $_REQUEST['registro']['nveducacion'];
                    $sector = $_REQUEST['registro']['sector'];
                    $f_nacimiento = $_REQUEST['registro']['fnacimiento'];
                    $expedicion = $_REQUEST['registro']['identifica_origen'];
                    $edad = $_REQUEST['registro']['edad'];

                    $cliente = $_REQUEST['cliente']['id'];

                    $datos = [
                        'competencia_al' => $competencia,
                        'curso_al' => $certificado,
                        'sector_al' => $sector,
                        'ciudad_al' => $direccion,
                        'fecha_al' => $inicio,
                        'nacionalidad_al' => $nacionalidad,
                        'nombre_primero_al' => $primerNombre,
                        'nombre_segundo_al' => $segundoNombre,
                        'apellidos_al' => $apellidos,
                        'tipo_doc_al' => $tipo_id,
                        'documento_al' => $identifica,
                        'documento_de_al' => $expedicion,
                        'fnacimiento_al' => $f_nacimiento,
                        'edad_al' => $edad,
                        'telefono_al' => $telefono,
                        'rh_al' => $rh,
                        'email_al' => $email,
                        'profesion_al' => $cargo,
                        'empresa_al' => $empresa,
                        'nv_educativo_al' => $educativo,
                        'codigo_ficha' => $codigo_ficha,
                        'cliente' => $cliente,
                        'creacion_al' => $nfecha,
                        'creadopor' => $_SESSION['usuloggri']
                    ];

                    $registro_espacios = $db
                        ->insert('registros_alturas4272', $datos);

                    if ($registro_espacios) {
                        //! Registro ficha
                        /*  $fichas = $db
                            ->where('Id', $codigo_ficha)
                            ->update('fichas_ministerio', ['en_uso' => $db->inc(1)]); */
                    }
                }

                if ($capacitacion_alias == 'rescateindustrial') {
                    $fecha = date('Y-m-j');
                    // $nombre = $nombres . ' ' . $apellidos;
                    $gs =  '';

                    if (strpos($rh, '+') !== false) {
                        $gs =  '+';
                    }

                    if (strpos($rh, '-') !== false) {
                        $gs =  '-';
                    }

                    /* $rh = str_replace('+', '', $rh);
                    $rh = str_replace('-', '', $rh); */

                    $competencia = '';

                    $competencias = $db
                        ->where('alias_cp', $capacitacion_alias)
                        ->objectBuilder()->get('competencias');

                    if ($db->count > 0) {
                        $rcp = $competencias[0];
                        $competencia = $rcp->Id_cp;
                    }

                    $nacionalidad = $_REQUEST['registro']['nacionalidad'];
                    $educativo = $_REQUEST['registro']['nveducacion'];
                    $sector = $_REQUEST['registro']['sector'];
                    $f_nacimiento = $_REQUEST['registro']['fnacimiento'];
                    $expedicion = $_REQUEST['registro']['identifica_origen'];
                    $edad = $_REQUEST['registro']['edad'];

                    $arl = '';

                    if (isset($_REQUEST['cliente']['arl'])) {
                        $arl = $_REQUEST['cliente']['arl'];
                    }

                    $datos = [
                        'competencia_al' => $competencia,
                        'curso_al' => $certificado,
                        'sector_al' => $sector,
                        'ciudad_al' => $direccion,
                        'fecha_al' => $inicio,
                        'nacionalidad_al' => $nacionalidad,
                        'nombre_primero_al' => $primerNombre,
                        'nombre_segundo_al' => $segundoNombre,
                        'apellidos_al' => $apellidos,
                        'tipo_doc_al' => $tipo_id,
                        'documento_al' => $identifica,
                        'documento_de_al' => $expedicion,
                        'fnacimiento_al' => $f_nacimiento,
                        'edad_al' => $edad,
                        'telefono_al' => $telefono,
                        'rh_al' => $rh,
                        'arl_al' => $arl,
                        'email_al' => $email,
                        'profesion_al' => $cargo,
                        'empresa_al' => $empresa,
                        'nv_educativo_al' => $educativo,
                        'codigo_ficha' => $codigo_ficha,
                        'cliente' => $cliente,
                        'creacion_al' => $nfecha,
                        'creadopor' => $_SESSION['usuloggri']
                    ];

                    $registro_espacios = $db
                        ->insert('registros_rescate_industrial', $datos);

                    if ($registro_espacios) {
                        //! Registro ficha
                        $fichas = $db
                            ->where('Id', $codigo_ficha)
                            ->update('fichas_ministerio', ['en_uso' => $db->inc(1)]);
                    }
                }

                $informacion['registro'] = $registro;
                $informacion['status'] = 'Correcto';
            } else {
                $informacion['status'] = 'Error';
                // $informacion['q'] =$db->getLastQuery();
            }
        } else {
            $informacion['status'] = 'Error2';
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        session_start();
        $pagina = (int) (isset($_POST['pagina']) ? $_POST['pagina'] : 1);

        if ($idregistro == '') {
            $idregistro = '%';
        }

        if ($buscar_iden == '') {
            $buscar_iden = '%';
        }

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        if ($buscar_apel == '') {
            $buscar_apel = '%';
        }

        $list = $db
            ->where('Id', $idregistro, 'LIKE')
            ->where('(nombre_primero LIKE "%' . $buscar_nom . '%" OR nombre_segundo LIKE "%' . $buscar_nom . '%")')
            ->where('apellidos', '%' . $buscar_apel . '%', 'LIKE')
            ->where('numero_ident', '%' . $buscar_iden . '%', 'LIKE')
            ->objectBuilder()->get('registros');

        $filas = '';
        $paginacion = '';

        if ($db->count > 0) {
            $total        = $db->count;
            $adyacentes   = 2;
            $registro_pag = 20;

            $pagina       = ($pagina == 0 ? 1 : $pagina);
            $inicio       = ($pagina - 1) * $registro_pag;

            $siguiente  = $pagina + 1;
            $anterior   = $pagina - 1;
            $ultima_pag = ceil($total / $registro_pag);
            $penultima  = $ultima_pag - 1;

            $paginacion = '';

            if ($ultima_pag > 1) {
                if ($pagina > 1) {
                    $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
                } else {
                    $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
                }

                if ($pagina > 1) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
                } else {
                    $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
                }

                if ($ultima_pag < 7 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                    if ($pagina < 1 + ($adyacentes * 2)) {
                        for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                            if ($contador == $pagina) {
                                $paginacion .= "<span class='actual'>$contador</span>";
                            } else {
                                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                            }
                        }
                        $paginacion .= "...";
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                    } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                        $paginacion .= "...";
                        for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                            if ($contador == $pagina) {
                                $paginacion .= "<span class='actual'>$contador</span>";
                            } else {
                                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                            }
                        }
                        $paginacion .= "..";
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                        $paginacion .= "..";
                        for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                            if ($contador == $pagina) {
                                $paginacion .= "<span class='actual'>$contador</span>";
                            } else {
                                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                            }
                        }
                    }
                }
                if ($pagina < $contador - 1) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
                } else {
                    $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
                }

                if ($pagina < $ultima_pag) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
                } else {
                    $paginacion .= "<span class='disabled'>Última &raquo;</span>";
                }

                // $paginacion.= "</div>";
            }

            $db->pageLimit = $registro_pag;

            $registros = $db
                ->where('Id', $idregistro, 'LIKE')
                ->where('(nombre_primero LIKE "%' . $buscar_nom . '%" OR nombre_segundo LIKE "%' . $buscar_nom . '%")')
                ->where('apellidos', '%' . $buscar_apel . '%', 'LIKE')
                ->where('numero_ident', '%' . $buscar_iden . '%', 'LIKE')
                ->orderBy('Id', 'DESC')
                ->objectBuilder()->paginate('registros', $pagina);

            if ($total > 0) {
                foreach ($registros as $registro) {
                    $curso  = '';
                    $capacitacion_alias = '';

                    $cursos = $db
                        ->where('Id_ct', $registro->certificado)
                        ->where('activo_ct', 1)
                        ->objectBuilder()->get('certificaciones');

                    if ($db->count > 0) {
                        $curso = $cursos[0]->nombre;
                        $capacitacion_alias = $cursos[0]->competencia_ct;
                    }

                    $vendedor = $registro->vendedor;
                    $vigencia = $registro->fecha_vigencia;
                    $horas    = $registro->horas;

                    if ($vigencia == '0000-00-00') {
                        $vigencia = '';
                    }

                    if ($horas == '0') {
                        $horas = '';
                    }

                    $ciudad_expedicion = '';
                    $ciudad_class = '';

                    if ($registro->codigo_ficha != '' && $registro->ciudad == '') {
                        $fichas = $db
                            ->where('Id', $registro->codigo_ficha)
                            ->objectBuilder()->get('fichas_ministerio');

                        if ($db->count > 0) {
                            $rf = $fichas[0];

                            $ciudades = [
                                'cucuta' => 'Cúcuta',
                                'villavicencio' => 'Villavicencio',
                                '' => ''
                            ];

                            $ciudad_expedicion = $ciudades[$rf->ciudad];
                            $ciudad_class = $rf->ciudad;
                        }
                    }

                    if ($registro->ciudad != '') {
                        $ciudades = [
                            'cucuta' => 'Cúcuta',
                            'villavicencio' => 'Villavicencio',
                            '' => ''
                        ];

                        $ciudad_expedicion = $ciudades[$registro->ciudad];
                        $ciudad_class = $registro->ciudad;
                    }

                    $check_codigo = '';
                    $btn_generar = '';
                    $link_certificado = '';

                    if ($capacitacion_alias == 'espaciosconfinados') {
                        $link_certificado = 'libs/certificado-espacios?id=' . $registro->Id;
                    } else if ($capacitacion_alias == 'alturasres.4272') {
                        $link_certificado = 'libs/certificado-alturas4272?id=' . $registro->Id;
                    } else if ($capacitacion_alias == 'cbasico') {
                        $link_certificado = 'libs/certificado-basico?id=' . $registro->Id;
                    } else if ($capacitacion_alias == 'rescateindustrial') {
                        $link_certificado = 'libs/certificado-rescate?id=' . $registro->Id;
                    } else {
                        $link_certificado = 'libs/certificado?id=' . $registro->Id;
                    }

                    if ($registro->Id < 105346) {
                        $btn_generar = '<a href="' . $link_certificado . '" target="_blank"><span class="icon-newspaper"></span></a>';
                    } else {
                        /* $comprobar = $db
                            ->where('registro_ced', $registro->Id)
                            ->where('comprobado_ced', 1)
                            ->objectBuilder()->get('codigos_exportados_certificados');

                        if ($db->count > 0) { */
                        $btn_generar = '<a href="' . $link_certificado . '" target="_blank"><span class="icon-newspaper"></span></a>';
                        /*  } else {
                            $check_codigo = '<input type="checkbox" class="check-generar">';
                        } */
                    }


                    $filas .= '<tr id="' . $registro->Id . '" data-tipo="' . $capacitacion_alias . '" data-certificado="' . $registro->certificado . '" class="Sede-' . $ciudad_class . '">
                                <td>' . $check_codigo . '</td>
                                <td><p>' . $registro->Id . '</p></td>
                                <td><p>' . $registro->numero_ident . '</p></td>
                                <td><p>' . $curso . '</p></td>
                                <td><p>' . $registro->nombre_primero . '</p></td>
                                <td><p>' . $registro->nombre_segundo . '</p></td>
                                <td><p>' . $registro->apellidos . '</p></td>
                                <td><p>' . $registro->fecha_inicio . '</p></td>
                                <td><p>' . $vigencia . '</p></td>
                                <td><p>' . $horas . '</p></td>
                                <td><p>' . $vendedor . '</p></td>
                                <td><p>' . $registro->precio . '</p></td>
                                <td><p>' . $ciudad_expedicion . '</p></td>';

                    if ($_SESSION['usutipoggri'] == 'administrador') {
                        $filas .= '<td><a href="registros_edt?registro=' . $registro->Id . '" title="Editar" target="_blank"><span class="icon-editar"></span></a></td>';
                    } else {
                        $filas .= '<td></td>';
                    }

                    $filas .= '<td align="center">' . $btn_generar . '</td>';

                    if ($capacitacion_alias == 'alturas' || $capacitacion_alias == 'espaciosconfinados' || $capacitacion_alias == 'alturasres.4272') {
                        $filas .= '   <td><a href="gen_alt-1?registro=' . $registro->Id . '" target="_blank" title="Carnet-tipo 1"><span class="icon-credit-card"></span></a><a href="gen_alt-2?registro=' . $registro->Id . '" target="_blank" title="Carnet-tipo 2"><span class="icon-credit-card"></span></a></td>
                    <td><a href="javascript://" id="reg-' . $registro->numero_ident . '" class="gen-acta"><span class="icon-profile"></span></a></td>';
                    } else {
                        $filas .= '   <td><a href="gen_carnet-1?registro=' . $registro->Id . '" target="_blank" title="Carnet-tipo 1"><span class="icon-credit-card"></span></a><a href="gen_carnet-2?registro=' . $registro->Id . '" target="_blank" title="Carnet-tipo 2"><span class="icon-credit-card"></span></a></td>
                    <td><a href="javascript://" id="reg-' . $registro->numero_ident . '" class="gen-acta"><span class="icon-profile"></span></a></td>';
                    }

                    if ($_SESSION['usutipoggri'] == 'administrador') {
                        $filas .= '<td><a href="javascript://" title="Editar" class="editar"><span class="icon-editar"></span></a><a href="javascript://" title="Guardar" ><span class="icon-profile"></span></a><a href="javascript://" title="Eliminar" class="eliminar"><span class="icon-eliminar"></span></a></td>';
                    }

                    $filas .= '</tr>';
                }
            } else {
                $filas = '<tr>
                        <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="8"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;

        echo json_encode($informacion);
        break;
    case 'editar':
        $primerNombre = strtoupper($_POST['primer_nombre']);
        $segundoNombre = strtoupper($_POST['segundo_nombre']);

        $datos = [
            'numero_ident' => $identifica,
            'nombre_primero' => $primerNombre,
            'nombre_segundo' => $segundoNombre,
            'apellidos' => $apellidos,
            'fecha_inicio' => $inicio,
            'fecha_vigencia' => $vigencia,
            'horas' => $horas,
            'precio' => $precio,
        ];

        $act = $db
            ->where('Id', $buscar_iden)
            ->update('registros', $datos);

        if ($act) {
            $informacion['status'] = 'Correcto';
        } else {
            $informacion['status'] = 'Error';
        }

        echo json_encode($informacion);
        break;
    case 'editar2':
        session_start();
        $entrenador = '';

        if (isset($_POST['entrenador'])) {
            $entrenador = $_POST['entrenador'];
        }

        $codigo = 0;
        if (isset($_POST['codigocertificado'])) {
            $codigo = $_POST['codigocertificado'];
        }

        if ($capacitacion != 'ALTURAS') {
            $entrenador = 0;
        }

        $datos = [
            'tipo_ident' => $tipo_id,
            'numero_ident' => $identifica,
            'nombres' => $nombres,
            'apellidos' => $apellidos,
            'sexo' => $sexo,
            'direccion' => $direccion,
            'telefono' => $telefono,
            'correo'  => $email,
            'capacitacion' => $capacitacion,
            'entrenador' => $entrenador,
            'certificado' => $certificado,
            'fecha_inicio' => $inicio,
            'fecha_fin' => $finaliza,
            'fecha_vigencia' => $vigencia,
            'horas' => $horas,
            'vencimiento_ct' => $vencimiento,
            'rh' => $rh,
            'cargo' => $cargo,
            'empresa' => $empresa,
            'licencia' => $licencia,
            'vendedor' => $vendedor,
            'tpago' => $tpago,
            'precio' => $precio,
        ];

        if (isset($_REQUEST['ciudad'])) {
            $datos['ciudad'] = $_REQUEST['ciudad'];
        }

        $act = $db
            ->where('Id', $id)
            ->update('registros', $datos);

        if ($act) {
            if (!empty($foto1)) {
                $nombre_foto = trim($identifica) . '-' . $id . '-' . $foto1;

                $archivador = $carpeta_fotos . '/' . $nombre_foto;
                if (!move_uploaded_file($foto1_tmp, $archivador)) {
                    $informacion['errores'] = 'foto1';
                } else {
                    $act = $db
                        ->where('Id', $id)
                        ->update('registros', ['foto_ct' => $archivador]);
                }
            }

            if (isset($_REQUEST['capacitacion_ad'])) {
                $regs = $db
                    ->where('Id', $id)
                    ->objectBuilder()->get('registros');

                $archivador = '';

                if ($regs[0]->foto_ct != '' && file_exists($regs[0]->foto_ct)) {
                    $archivador = $regs[0]->foto_ct;
                }

                for ($i = 0; $i < count($_REQUEST['capacitacion_ad']); $i++) {
                    $datos = [
                        'tipo_ident' => $tipo_id,
                        'numero_ident' => $identifica,
                        'nombres' => $nombres,
                        'apellidos' => $apellidos,
                        'sexo' => $sexo,
                        'direccion' => $direccion,
                        'telefono' => $telefono,
                        'correo' => $email,
                        'capacitacion' => $_REQUEST['capacitacion_ad'][$i],
                        'certificado' => $_REQUEST['certificado_ad'][$i],
                        'fecha_inicio' => $_REQUEST['f_inicio_ad'][$i],
                        'fecha_vigencia' => $_REQUEST['f_vigencia_ad'][$i],
                        'horas' => $_REQUEST['horas_ad'][$i],
                        'vencimiento_ct' => $_REQUEST['vencimiento_ad'][$i],
                        'rh' => $rh,
                        'cargo' => $cargo,
                        'empresa' => $empresa,
                        'licencia' => $licencia,
                        'vendedor' => $vendedor,
                        'tpago' => $tpago,
                        'precio' => $_REQUEST['precio_ad'][$i],
                        'creacion' => $nfecha,
                        'creadopor' => $_SESSION['usuloggri']
                    ];

                    $adicional = $db
                        ->insert('registros', $datos);

                    if ($adicional) {

                        $nombre_foto = trim($identifica) . '-' . $id_ad . '-' . $foto1;
                        $archivador_adicional = $carpeta_fotos . '/' . $nombre_foto;

                        if (!empty($foto1)) {
                            if (!copy($archivador, $archivador_adicional)) {
                                $informacion['errores'] = 'foto1';
                            } else {
                                $act = $db
                                    ->where('Id', $adicional)
                                    ->update('registros', ['foto_ct' => $archivador_adicional]);
                            }
                        }
                    }
                }
            }

            $informacion['status'] = 'Correcto';
        } else {
            $informacion['status'] = 'Error1';

            print_r($db->getLastQuery());
        }

        echo json_encode($informacion);
        break;
    case 'eliminar':
        $eli = $db
            ->where('Id', $buscar_iden)
            ->delete('registros');
        break;
    case 'consulta':
        $id       = $_POST['identifica'];
        $registro = '';

        $bus = $db
            ->where('numero_ident', $id)
            ->where('fecha_vigencia', $db->now(), '>=')
            ->objectBuilder()->get('registros', null, 'Id, certificado, CONCAT(nombres," ",apellidos) as nombre, horas, fecha_vigencia');

        if ($db->count > 0) {
            foreach ($bus as $res) {
                $curso           = '';

                $certificaciones = $db
                    ->where('Id_ct', $res->certificado)
                    ->objectBuilder()->get('certificaciones');

                if ($db->count > 0) {
                    $curso = $certificaciones[0]->nombre;
                }

                $registro .= '<div class="Mostrar-certificado-detalles" id="' . $res->Id . '">';
                $registro .= '<table>
                                    <tbody>
                                        <tr>
                                            <td><h3>Certificado:</h3></td>
                                            <td class="Mayuscula">' . $curso . '</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Nombre:</h3></td>
                                            <td class="Mayuscula">' . $res->nombre . '</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Duración:</h3></td>
                                            <td class="Mayuscula">' . $res->horas . ' Hrs</td>
                                        </tr>
                                        <tr>
                                            <td><h3>Vigencia hasta:</h3></td>
                                            <td class="Mayuscula">' . $res->fecha_vigencia . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                                ';
                $registro .= '</div>';
            }
            $informacion['certificados'] = $registro;
            $informacion['status']       = 'Correcto';
        } else {
            $informacion['status'] = 'Error';
        }

        echo json_encode($informacion);
        break;
    case 'informe':
        $inicio = $_POST['inicio'];
        $fin    = $_POST['final'];

        if (empty($inicio)) {
            $inicio = '1999-01-01';
        }
        if (empty($fin)) {
            $fin = '3000-01-01';
        }

        $bus = $db
            ->where('creacion', array($inicio . ' 00:00:00', $fin . ' 23:59:00'), 'BETWEEN')
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            if (PHP_SAPI == 'cli') {
                die('Este archivo solo se puede ver desde un navegador web');
            }

            require_once 'PHPExcel/PHPExcel.php';

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setCreator("")
                ->setLastModifiedBy("")
                ->setTitle("Informe de Registros")
                ->setSubject("Informe de Registros excel")
                ->setDescription("Informe de Registros")
                ->setKeywords("Informe de Registros")
                ->setCategory("Reporte excel");


            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'N° Identificación')
                ->setCellValue('B1', 'Nombre')
                ->setCellValue('C1', 'Fecha Expedición')
                ->setCellValue('D1', 'Tipo Capacitación')
                ->setCellValue('E1', 'Nombre Capacitación')
                ->setCellValue('F1', 'NIT Institución Educativa')
                ->setCellValue('G1', 'CD Institución Educativa')
                ->setCellValue('H1', 'Entidad Certificadora')
                ->setCellValue('I1', 'DIV CODIGO')
                ->setCellValue('J1', 'ND Sede')
                ->setCellValue('K1', 'Vendedor')
                ->setCellValue('L1', 'Precio')
                ->setCellValue('M1', 'Forma de Pago')
                ->setCellValue('N1', 'Entrenador')
                ->setCellValue('O1', 'Fecha Creación')
                ->setCellValue('P1', 'Creado Por');

            $j = 2;

            foreach ($bus as $fila) {
                $inicio_ex = explode('-', $fila->fecha_inicio);
                $inicio_ex = $inicio_ex[2] . '/' . $inicio_ex[1] . '/' . $inicio_ex[0];
                $final  = explode('-', $fila->fecha_vigencia);
                $final  = $final[2] . '/' . $final[1] . '/' . $final[0];

                $curso = '';

                $certificaciones = $db
                    ->where('Id_ct', $fila->certificado)
                    ->objectBuilder()->get('certificaciones');

                if ($db->count > 0) {
                    $curso = $certificaciones[0]->nombre;
                }

                $vendedor = $fila->vendedor;
                $creador  = '';

                $usuarios = $db
                    ->where('id', $fila->creadopor)
                    ->objectBuilder()->get('usuarios');

                if ($db->count > 0) {
                    $creador = $usuarios[0]->nombre . ' ' . $usuarios[0]->apellido;
                }

                $fpago = '';

                if ($fila->tpago == 'cr') {
                    $fpago = 'Crédito';
                }

                if ($fila->tpago == 'ct') {
                    $fpago = 'Contado';
                }

                $entrenador = '';

                if ($fila->entrenador != 0) {
                    $entrenadores = $db
                        ->where('Id_en', $fila->entrenador)
                        ->objectBuilder()->get('entrenadores');

                    if ($db->count > 0) {
                        $entrenador = $entrenadores[0]->nombre_en . ' ' . $entrenadores[0]->apellido_en;
                    }
                }

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $fila->numero_ident)
                    ->setCellValue('B' . $j, $fila->nombre_primero . ' ' . $fila->nombre_segundo . ' ' . $fila->apellidos)
                    ->setCellValue('C' . $j, $inicio_ex)
                    ->setCellValue('D' . $j, $fila->capacitacion)
                    ->setCellValue('E' . $j, $curso)
                    ->setCellValue('F' . $j, '900892983')
                    ->setCellValue('G' . $j, '0')
                    ->setCellValue('H' . $j, 'MEN')
                    ->setCellValue('I' . $j, '5001')
                    ->setCellValue('J' . $j, '1234')
                    ->setCellValue('K' . $j, $vendedor)
                    ->setCellValue('L' . $j, $fila->precio)
                    ->setCellValue('M' . $j, $fpago)
                    ->setCellValue('N' . $j, $entrenador)
                    ->setCellValue('O' . $j, $fila->creacion)
                    ->setCellValue('P' . $j, $creador);
                $j++;
            }

            /// ensayo

            $bus = $db
                ->where('fecha_en', array($inicio, $fin), 'BETWEEN')
                ->objectBuilder()->get('certificado_ensayo');

            if ($db->count > 0) {
                foreach ($bus as $res) {
                    $tipo = '';

                    switch ($res->tipo_en) {
                        case '1':
                            $tipo = 'QUINTA RUEDA';
                            break;
                        case '2':
                            $tipo = 'LINEA DE VIDA';
                            break;
                        case '3':
                            $tipo = 'KING PIN';
                            break;
                    }

                    $vendedor = '';

                    $vendedores = $db
                        ->where('Id_v', $res->vendedor_en)
                        ->objectBuilder()->get('vendedores');

                    if ($db->count > 0) {
                        $vendedor = $vendedores[0]->nombre_v;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $j, $tipo)
                        ->setCellValue('B' . $j, $res->empresa_en)
                        ->setCellValue('C' . $j, $res->propietario_en)
                        ->setCellValue('D' . $j, $res->fecha_en)
                        ->setCellValue('E' . $j, $res->contacto_en)
                        ->setCellValue('F' . $j, $res->ciudad_en)
                        ->setCellValue('G' . $j, $res->reporte_en)
                        ->setCellValue('H' . $j, $res->placa_en)
                        ->setCellValue('I' . $j, $res->placaremolque_en)
                        ->setCellValue('J' . $j, $res->serie_en)
                        ->setCellValue('K' . $j, $vendedor)
                        ->setCellValue('L' . $j, $res->marca_en)
                        ->setCellValue('M' . $j, $res->modelo_en)
                        ->setCellValue('N' . $j, $res->sistema_en)
                        ->setCellValue('O' . $j, $res->revisado_en)
                        ->setCellValue('P' . $j, $res->recibido_en)
                        ->setCellValue('Q' . $j, $res->vencimiento_en);
                    $j++;
                }
            }

            /// arnes

            $bus = $db
                ->where('creado', array($inicio, $fin), 'BETWEEN')
                ->objectBuilder()->get('certificado_arnes');

            if ($db->count > 0) {
                foreach ($bus as $res) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $j, $res->propietario)
                        ->setCellValue('B' . $j, $res->direccion)
                        ->setCellValue('C' . $j, $res->telefono)
                        ->setCellValue('D' . $j, $res->equipo)
                        ->setCellValue('E' . $j, $res->marca)
                        ->setCellValue('F' . $j, $res->serie)
                        ->setCellValue('G' . $j, $res->modelo)
                        ->setCellValue('H' . $j, $res->ninspeccion)
                        ->setCellValue('I' . $j, $res->ffabricacion)
                        ->setCellValue('J' . $j, $res->referencia)
                        ->setCellValue('K' . $j, $res->lote)
                        ->setCellValue('L' . $j, $res->talla)
                        ->setCellValue('M' . $j, $res->codaprobacion)
                        ->setCellValue('N' . $j, $res->control1)
                        ->setCellValue('O' . $j, $res->control2)
                        ->setCellValue('P' . $j, $res->control3);
                    $j++;
                }
            }

            /// eslinga

            $bus = $db
                ->where('creado', array($inicio, $fin), 'BETWEEN')
                ->objectBuilder()->get('certificado_eslinga');

            if ($db->count > 0) {
                foreach ($bus as $res) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $j, $res->propietario)
                        ->setCellValue('B' . $j, $res->direccion)
                        ->setCellValue('C' . $j, $res->telefono)
                        ->setCellValue('D' . $j, $res->equipo)
                        ->setCellValue('E' . $j, $res->marca)
                        ->setCellValue('F' . $j, $res->serie)
                        ->setCellValue('G' . $j, $res->modelo)
                        ->setCellValue('H' . $j, $res->ninspeccion)
                        ->setCellValue('I' . $j, $res->ffabricacion)
                        ->setCellValue('J' . $j, $res->referencia)
                        ->setCellValue('K' . $j, $res->lote)
                        ->setCellValue('L' . $j, $res->talla)
                        ->setCellValue('M' . $j, $res->codaprobacion)
                        ->setCellValue('N' . $j, $res->control1)
                        ->setCellValue('O' . $j, $res->control2)
                        ->setCellValue('P' . $j, $res->control3);
                    $j++;
                }
            }

            /// reportes

            $bus = $db
                ->where('fecha', array($inicio, $fin), 'BETWEEN')
                ->objectBuilder()->get('certificado_reporte');

            if ($db->count > 0) {
                foreach ($bus as $res) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $j, $res->reporte)
                        ->setCellValue('B' . $j, $res->propietario)
                        ->setCellValue('C' . $j, $res->calibracion)
                        ->setCellValue('D' . $j, $res->fecha)
                        ->setCellValue('E' . $j, $res->ciudad)
                        ->setCellValue('F' . $j, $res->serie)
                        ->setCellValue('G' . $j, $res->placa)
                        ->setCellValue('H' . $j, $res->capacidadtotal)
                        ->setCellValue('I' . $j, $res->materialtanque)
                        ->setCellValue('J' . $j, $res->marca)
                        ->setCellValue('K' . $j, $res->validez)
                        ->setCellValue('L' . $j, $res->restricciones);
                    $j++;
                }
            }

            /// aforo

            $bus = $db
                ->where('fecha', array($inicio, $fin), 'BETWEEN')
                ->objectBuilder()->get('certificado_aforo');

            if ($db->count > 0) {
                foreach ($bus as $res) {
                    $vendedor = '';

                    $vendedores = $db
                        ->where('Id_v', $res->vendedor)
                        ->objectBuilder()->get('vendedores');

                    if ($db->count > 0) {
                        $vendedor = $vendedores[0]->nombre_v;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $j, $res->reporte)
                        ->setCellValue('B' . $j, $res->propietario)
                        ->setCellValue('C' . $j, $res->ciudad)
                        ->setCellValue('D' . $j, $res->placa)
                        ->setCellValue('E' . $j, $res->ciudad)
                        ->setCellValue('F' . $j, $res->capacidadtotal)
                        ->setCellValue('G' . $j, $res->materialtanque)
                        ->setCellValue('H' . $j, $res->marca)
                        ->setCellValue('I' . $j, $res->contenido)
                        ->setCellValue('J' . $j, $res->validez)
                        ->setCellValue('K' . $j, $vendedor)
                        ->setCellValue('L' . $j, $res->restricciones);
                    $j++;
                }
            }

            $estiloTituloColumnas = array(
                'font' => array(
                    'name'  => 'Calibri',
                    'bold'  => true,
                    'size'  => 11,
                    'color' => array(
                        'rgb' => 'ffffff',
                    ),
                ),
                'fill' => array(
                    'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                    'rotation'   => 90,
                    'startcolor' => array(
                        'rgb' => '6085FC',
                    ),
                    'endcolor'   => array(
                        'argb' => '6085FC',
                    ),
                ),
            );

            // $esty = $celdas[0];
            $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->applyFromArray($estiloTituloColumnas);

            // $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "B8:".($fininfo));

            for ($i = 'A'; $i <= 'N'; $i++) {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->getColumnDimension($i)->setAutoSize(true);
            }

            $objPHPExcel->getActiveSheet()->setTitle('Informe');

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Informe-Registros.xlsx"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
        } else {
            print_r('No hay resultados para mostrar');
        }
        break;
    case 'acta-cursos':
        $informacion['cursos'] = '';
        $cursos = '';

        $registros = $db
            ->where('numero_ident', $buscar_iden)
            ->groupBy('certificado')
            ->orderBy('Id', 'ASC')
            ->objectBuilder()->get('registros');

        foreach ($registros as $rsg) {
            $formacion       = '';

            $certificaciones = $db
                ->where('Id_ct', $rsg->certificado)
                ->objectBuilder()->get('certificaciones');

            if ($db->count > 0) {
                $formacion = $certificaciones[0]->nombre;
            }

            $cursos .= '<p><input type="checkbox" name="curso" class="curso_ver" value="' . $rsg->certificado . '" >' . $formacion . '</p>';
        }

        $informacion['cursos'] = $cursos;

        echo json_encode($informacion);
        break;
    case 'cargar-cursos':
        $ls_asignados = "";

        $cursos = $db
            ->where('activo_ct', 1)
            ->where('competencia_ct', $capacitacion)
            ->orderBy('nombre', 'ASC')
            ->objectBuilder()->get('certificaciones');

        foreach ($cursos as $rsc) {
            $asignados = $db
                ->where('vendedor_vc', $vendedor)
                ->where('curso_vc', $rsc->Id_ct)
                ->objectBuilder()->get('vendedores_cursos');

            if ($db->count > 0) {
                $ras = $asignados[0];
                $precio = $ras->precio_vc;

                $ls_asignados .= '<option value="' . $rsc->Id_ct . '" data-precio="' . $precio . '" data-horas="' . $rsc->horas . '">' . $rsc->nombre . '</option>';
            }
        }

        echo json_encode($ls_asignados);
        break;
    case 'Nacionalidades':
        $nacionalidades = $db
            ->where('nombre', '%' . $buscar_nom . '%', 'LIKE')
            ->objectBuilder()->get('nacionalidades');

        foreach ($nacionalidades as $rbs) {
            $nacionalidades = $rbs->Id . '|' . strtoupper($rbs->nombre) . '|' . strtoupper($rbs->pais);
            array_push($informacion, $nacionalidades);
        }

        echo json_encode($informacion);
        break;
    case 'Ciudades':
        $ciudades = $db
            ->where('nombre_ciu', '%' . $buscar_nom . '%', 'LIKE')
            ->objectBuilder()->get('ciudades');

        foreach ($ciudades as $rbs) {
            $ciudades = $rbs->Id_ciu . '|' . strtoupper($rbs->nombre_ciu);
            array_push($informacion, $ciudades);
        }

        echo json_encode($informacion);
        break;
    case 'cargar-registro':
        $informacion['info'] = '';

        $alturas = $db
            ->where('documento_al', $identifica, 'LIKE')
            ->where('codigo_ficha', '4272Inf')
            ->orderBy('creacion_al', 'DESC')
            ->objectBuilder()->get('registros_alturas');

        if ($db->count > 0) {
            $informacion['info'] = $alturas[0];
        }

        echo json_encode($informacion);
        break;
    case 'generar-codigo':
        session_start();

        if (isset($_REQUEST['certificados_exportar'])) {
            $codigo = generateRandomString(6);
            $fecha = date('Y-m-d H:i:s');

            require("class.phpmailer.php");
            $mail = new PHPMailer(true);
            $mail->Host = "localhost";

            $mail->FromName = "GRI COMPANY S.A.S - Certificaciones - " . $fecha;
            $mail->Subject = 'Codigo de exportacion de certificados';
            $mail->AddAddress("validacion.gricompany@gmail.com");
            // $mail->AddAddress("h@inngeniate.com");
            $body = "<strong>Se ha generado el siguiente código para la exportación de certificados</strong><br><br>";
            $body .= "Código: $codigo <br>";
            $mail->Body = $body;
            $mail->IsHTML(true);

            if ($mail->send()) {
                $datos = [
                    'fecha_ce' => $fecha,
                    'usuario_ce' => $_SESSION['usuloggri'],
                    'codigo_ce' => $codigo,
                    'comprobado_ce' => 0
                ];

                $ingreso = $db
                    ->insert('codigos_exportados', $datos);

                if ($ingreso) {
                    foreach ($_REQUEST['certificados_exportar'] as $certificado) {
                        $datos = [
                            'Id_ce' => $ingreso,
                            'registro_ced' => $certificado,
                            'comprobado_ced' => 0
                        ];

                        $db->insert('codigos_exportados_certificados', $datos);
                    }

                    $informacion['status'] = true;
                } else {
                    $informacion['status'] = false;
                }
            } else {
                $informacion['status'] = false;
            }
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'comprobar-codigo':
        $comprobar = $db
            ->where('codigo_ce', $_REQUEST['certificados-codigo'])
            ->where('comprobado_ce', 0)
            ->objectBuilder()->get('codigos_exportados');

        if ($db->count > 0) {
            $db
                ->where('codigo_ce', $_REQUEST['certificados-codigo'])
                ->where('comprobado_ce', 0)
                ->update('codigos_exportados', ['comprobado_ce' => 1]);

            $certificados = $db
                ->where('Id_ce', $comprobar[0]->Id_ce)
                ->objectBuilder()->get('codigos_exportados_certificados');

            foreach ($certificados as $certificado) {
                $db
                    ->where('Id_ced', $certificado->Id_ced)
                    ->update('codigos_exportados_certificados', ['comprobado_ced' => 1]);
            }

            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
}


function generateRandomString($length = 6)
{
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
