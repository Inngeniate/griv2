<?php

require "conexion.php";
$data = $_REQUEST['solicitud'];

$data['inicio'] = '1999-01-01';
$data['fin'] = date('Y-m-d');

$notifica = $db
    ->where('fecha_sa', array($data['inicio'] . ' 00:00:00', $data['fin'] . ' 23:59:00'), 'BETWEEN')
    ->orderBy('fecha_sa', 'DESC')
    ->objectBuilder()->get('solicitud_alturas');

$res = $db->count;

if ($res > 0) {
    if (PHP_SAPI == 'cli') {
        die('Este archivo solo se puede ver desde un navegador web');
    }

    require_once 'PHPExcel/PHPExcel.php';
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("")
        ->setLastModifiedBy("")
        ->setTitle("Solicitudes altura")
        ->setSubject("Solicitudes altura excel")
        ->setDescription("Solicitudes altura")
        ->setKeywords("Solicitudes altura")
        ->setCategory("Reporte excel");

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', '#')
        ->setCellValue('B1', 'No Cedula')
        ->setCellValue('C1', 'Celular')
        ->setCellValue('D1', 'Correo')
        ->setCellValue('E1', 'Fecha Solicitud');

    $cont = 1;
    $j    = 2;

    foreach ($notifica as $fila) {
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $j, $cont)
            ->setCellValue('B' . $j, $fila->identificacion_sa)
            ->setCellValue('C' . $j, $fila->celular_sa)
            ->setCellValue('D' . $j, $fila->correo_sa)
            ->setCellValue('E' . $j, $fila->fecha_sa);
        $j++;
        $cont++;
    }

    $estiloTituloColumnas = array(
        'font' => array(
            'name'  => 'Calibri',
            'bold'  => true,
            'size'  => 11,
            'color' => array(
                'rgb' => 'ffffff',
            ),
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
            'startcolor' => array(
                'rgb' => '6085FC',
            ),
            'endcolor'   => array(
                'argb' => '6085FC',
            ),
        ),
    );

    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($estiloTituloColumnas);
    for ($i = 'A'; $i <= 'E'; $i++) {
        $objPHPExcel->setActiveSheetIndex(0)
            ->getColumnDimension($i)->setAutoSize(true);
    }

    $objPHPExcel->getActiveSheet()->setTitle('Solicitudes altura');

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe-Solicitudes-altura.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
} else {
    print_r('No hay resultados para mostrar');
}
