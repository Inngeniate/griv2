<?php
require "conexion.php";
@$informacion = array();
$data         = $_REQUEST['registro'];

@$carpeta_firmas = '../espacios_confinados/firmas_aprendiz/';
@$carpeta_anexos = '../espacios_confinados/anexos/';

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j H:i:s');

switch ($data['accion']) {
    case 'nuevo_reg':
        session_start();

        $vacios = array('formacionotro', 'lesioncual', 'enfermedadcual');

        foreach ($vacios as $val) {
            if (!isset($data[$val])) {
                $data[$val] = '';
            }
        }

        $vacios = array('rqcedula', 'rqcertificadoant', 'rqvaloracionmedica', 'rqsegsocial', 'rqcertificadolab', 'rqtrabajadorentrante', 'rqvigiaseguridad', 'rqtituloprofesional', 'rqlicencia');

        foreach ($vacios as $val) {
            if (!isset($data[$val])) {
                $data[$val] = '0';
            }
        }

        $identifica = str_replace('.', '', $data['identificacion']);
        $identifica = str_replace(',', '', $identifica);
        $identifica = (int) $identifica;

        if (!isset($data['empresa'])) {
            $data['empresa'] = '';
        }

        if (!isset($data['eps'])) {
            $data['eps'] = '';
        }

        if (!isset($data['arl'])) {
            $data['arl'] = '';
        }

        $datos = [
            'competencia_al' => $data['competencia'],
            'curso_al' => $data['curso'],
            'formacion_otro_al' => $data['formacionotro'],
            'sector_al' => $data['sector'],
            'ciudad_al' => $data['ciudad'],
            'fecha_al' => $data['fecha'],
            'nacionalidad_al' => $data['nacionalidad'],
            'nombre_primero_al' => $data['primer_nombre'],
            'nombre_segundo_al' => $data['segundo_nombre'],
            'apellidos_al' => $data['apellidos'],
            'tipo_doc_al' => $data['tipo_id'],
            'documento_al' => $identifica,
            'documento_de_al' => $data['identifica_origen'],
            'fnacimiento_al' => $data['fnacimiento'],
            'edad_al' => $data['edad'],
            'telefono_al' => $data['tlaprendiz'],
            'rh_al' => $data['rh'],
            'eps_al' => $data['eps'],
            'arl_al' => $data['arl'],
            'contacto_em_al' => $data['ctemergencia'],
            'telcontacto_al' => $data['telefono'],
            'email_al' => $data['email'],
            'lectoescritura_al' => $data['lectoescritura'],
            'profesion_al' => $data['profesion'],
            'estadolaboral_al' => $data['estado_laboral'],
            'empresa_al' => $data['empresa'],
            'alergias_al' => $data['alergias'],
            'medicamentos_al' => $data['medicamentos'],
            'nv_educativo_al' => $data['nveducacion'],
            'reqcedula_al' => $data['rqcedula'],
            'reqcertificado_al' => $data['rqcertificadoant'],
            'reqvaloracion_al' => $data['rqvaloracionmedica'],
            'reqsegsocial_al' => $data['rqsegsocial'],
            'reqcertlaboral_al' => $data['rqcertificadolab'],
            'reqtrabajadorentrante_al' => $data['rqtrabajadorentrante'],
            'reqvigiaseguridad_al' => $data['rqvigiaseguridad'],
            'reqtituloprofesional_al' => $data['rqtituloprofesional'],
            'reqlicencia_al' => $data['rqlicencia'],
            'lesiones_al' => $data['lesiones'],
            'lesioncual_al' => $data['lesioncual'],
            'enfermedades_al' => $data['enfermedades'],
            'enfermedadcual_al' => $data['enfermedadcual'],
            'verificaciondoc_al' => $data['vrfdocumentos'],
            'verificamedio_al' => $data['vrfmedio'],
            'perfil_1_al' => $data['eval1'],
            'perfil_2_al' => $data['eval2'],
            'perfil_3_al' => $data['eval3'],
            'codigo_ficha' => $data['codigo_ficha'],
            'creacion_al' => $nfecha,
            'creadopor' => $_SESSION['usuloggri']
        ];

        $nuevo = $db
            ->insert('registros_espacios_confinados', $datos);

        if ($nuevo) {
            $id_ad = $nuevo;

            if (isset($data['sesion'])) {
                for ($i = 1; $i <= count($data['sesion']); $i++) {
                    for ($i = 1; $i <= count($data['sesion']); $i++) {
                        $datos = [
                            'Id_al' => $id_ad,
                            'sesion_as' => $i,
                            'fecha_as' => $data['sesion'][$i]
                        ];

                        $sesion = $db
                            ->insert('registro_espacios_confinados_sesiones', $datos);
                    }
                }
            }

            if (isset($data['firma']) && $data['firma'] != '' && $data['firma'] != 1) {
                $imgData = base64_decode(substr($data['firma'], 22));
                $file    = $carpeta_firmas . date('YmdHis') . '.png';

                $fp = fopen($file, 'w');
                if (fwrite($fp, $imgData)) {
                    $firma = substr($file, 3);

                    $registro = $db
                        ->where('Id_al', $id_ad)
                        ->update('registros_espacios_confinados', ['firma_al' => $firma]);

                    $informacion['status'] = true;
                } else {
                    $informacion['status'] = 'Error3';
                    $informacion['msg']    = "La firma no pudo ser adjuntada";
                }
            } else {
                $informacion['status'] = true;
            }

            $curso = '';

            /* $cursos = mysql_query("SELECT * FROM certificaciones WHERE Id_ct = '$data[curso]' ");
            if (mysql_num_rows($cursos) > 0) {
                $rc    = mysql_fetch_object($cursos);
                $curso = $rc->nombre;
            }

            require "class.phpmailer.php";
            $mail           = new PHPMailer(true);
            $mail->Host     = "localhost";
            $nombre         = $data['nombre'];
            $mail->FromName = "Nuevo Registro - GRI COMPANY S.A.S";
            $mail->Subject  = 'Mensaje Registro Alturas - Id: ' . $id_ad;
            $mail->AddAddress("registro.gricompany@gmail.com");
            $body = "<strong>Nuevo mensaje desde www.gricompany.co:</strong><br><br>";
            $body .= "Datos Registro <br>";
            $body .= "Nombre: $nombre<br>";
            $body .= "Tipo certificado: $curso<br>";
            $body .= "Fecha: $nfecha<br>";
            $mail->Body = $body;
            $mail->IsHTML(true);

            if ($mail->send()) {
            } */

            $informacion['registro'] = $id_ad;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'editar_reg':
        session_start();

        $vacios = array('formacionotro', 'lesioncual', 'enfermedadcual');

        foreach ($vacios as $val) {
            if (!isset($data[$val])) {
                $data[$val] = '';
            }
        }

        $identifica = str_replace('.', '', $data['identificacion']);
        $identifica = str_replace(',', '', $identifica);
        $identifica = (int) $identifica;

        if (!isset($data['empresa'])) {
            $data['empresa'] = '';
        }

        if (!isset($data['eps'])) {
            $data['eps'] = '';
        }

        if (!isset($data['arl'])) {
            $data['arl'] = '';
        }

        $vacios = array('rqcedula', 'rqcertificadoant', 'rqvaloracionmedica', 'rqsegsocial', 'rqcertificadolab', 'rqtrabajadorentrante', 'rqvigiaseguridad', 'rqtituloprofesional', 'rqlicencia');

        foreach ($vacios as $val) {
            if (!isset($data[$val])) {
                $data[$val] = '0';
            }
        }

        $datos = [
            'competencia_al' => $data['competencia'],
            'curso_al' => $data['curso'],
            'formacion_otro_al' => $data['formacionotro'],
            'sector_al' => $data['sector'],
            'ciudad_al' => $data['ciudad'],
            'fecha_al' => $data['fecha'],
            'nacionalidad_al' => $data['nacionalidad'],
            'nombre_primero_al' => $data['primer_nombre'],
            'nombre_segundo_al' => $data['segundo_nombre'],
            'apellidos_al' => $data['apellidos'],
            'tipo_doc_al' => $data['tipo_id'],
            'documento_al' => $identifica,
            'documento_de_al' => $data['identifica_origen'],
            'fnacimiento_al' => $data['fnacimiento'],
            'edad_al' => $data['edad'],
            'telefono_al' => $data['tlaprendiz'],
            'rh_al' => $data['rh'],
            'eps_al' => $data['eps'],
            'arl_al' => $data['arl'],
            'contacto_em_al' => $data['ctemergencia'],
            'telcontacto_al' => $data['telefono'],
            'email_al' => $data['email'],
            'lectoescritura_al' => $data['lectoescritura'],
            'profesion_al' => $data['profesion'],
            'estadolaboral_al' => $data['estado_laboral'],
            'empresa_al' => $data['empresa'],
            'alergias_al' => $data['alergias'],
            'medicamentos_al' => $data['medicamentos'],
            'nv_educativo_al' => $data['nveducacion'],
            'reqcedula_al' => $data['rqcedula'],
            'reqcertificado_al' => $data['rqcertificadoant'],
            'reqvaloracion_al' => $data['rqvaloracionmedica'],
            'reqsegsocial_al' => $data['rqsegsocial'],
            'reqcertlaboral_al' => $data['rqcertificadolab'],
            'reqtrabajadorentrante_al' => $data['rqtrabajadorentrante'],
            'reqvigiaseguridad_al' => $data['rqvigiaseguridad'],
            'reqtituloprofesional_al' => $data['rqtituloprofesional'],
            'reqlicencia_al' => $data['rqlicencia'],
            'lesiones_al' => $data['lesiones'],
            'lesioncual_al' => $data['lesioncual'],
            'enfermedades_al' => $data['enfermedades'],
            'enfermedadcual_al' => $data['enfermedadcual'],
            'verificaciondoc_al' => $data['vrfdocumentos'],
            'verificamedio_al' => $data['vrfmedio'],
            'perfil_1_al' => $data['eval1'],
            'perfil_2_al' => $data['eval2'],
            'perfil_3_al' => $data['eval3'],
        ];

        $editar = $db
            ->where('Id_al', $data['idregistro'])
            ->update('registros_espacios_confinados', $datos);

        if ($editar) {
            if (isset($data['sesion'])) {
                $comprobar =  $db
                    ->where('Id_al', $data['idregistro'])
                    ->objectBuilder()->get('registro_espacios_confinados_sesiones');

                $total = $db->count;
                if ($total > 0) {
                    if ($total > count($data['sesion'])) {
                        $total_el = $total - count($data['sesion']);
                        $eliminar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->orderBy('Id_as', 'DESC')
                            ->delete('registro_espacios_confinados_sesiones', $total_el);
                    }
                }

                for ($i = 1; $i <= count($data['sesion']); $i++) {
                    $comprobar = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('sesion_as', $i)
                        ->objectBuilder()->get('registro_espacios_confinados_sesiones');

                    if ($db->count > 0) {
                        $sesion = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('sesion_as', $i)
                            ->update('registro_espacios_confinados_sesiones', ['fecha_as' => $data['sesion'][$i]]);
                    } else {
                        $datos = [
                            'Id_al' => $data['idregistro'],
                            'sesion_as' => $i,
                            'fecha_as' => $data['sesion'][$i],
                        ];

                        $sesion = $db
                            ->insert('registro_espacios_confinados_sesiones', $datos);
                    }
                }
            }
            /// firma aprendiz
            if (isset($data['firma']) && $data['firma'] != '' && $data['firma'] != 1) {
                $imgData = base64_decode(substr($data['firma'], 22));
                $file    = $carpeta_firmas . date('YmdHis') . '.png';

                $fp = fopen($file, 'w');
                if (fwrite($fp, $imgData)) {
                    $registro = $db
                        ->where('Id_al', $data['idregistro'])
                        ->objectBuilder()->get('registros_espacios_confinados');

                    if ($registro[0]->firma_al != '' && file_exists('../' . $registro[0]->firma_al)) {
                        unlink('../' . $registro[0]->firma_al);

                        $registro = $db
                            ->where('Id_al', $data['idregistro'])
                            ->update('registros_espacios_confinados', ['firma_al' => '']);
                    }

                    $firma = substr($file, 3);

                    $registro = $db
                        ->where('Id_al', $data['idregistro'])
                        ->update('registros_espacios_confinados', ['firma_al' => $firma]);

                    $informacion['status'] = true;
                } else {
                    $informacion['status'] = 'Error3';
                    $informacion['msg']    = "La firma del aprendiz no pudo ser adjuntada";
                }
            } elseif ($data['firma'] == 1) {
                $registro = $db
                    ->where('Id_al', $data['idregistro'])
                    ->objectBuilder()->get('registros_espacios_confinados');

                if ($registro[0]->firma_al != '' && file_exists('../' . $registro[0]->firma_al)) {
                    unlink('../' . $registro[0]->firma_al);

                    $registro = $db
                        ->where('Id_al', $data['idregistro'])
                        ->update('registros_espacios_confinados', ['firma_al' => '']);
                }

                $informacion['status'] = true;
            } else {
                $informacion['status'] = true;
            }

            /// firma instructor
            if (isset($data['firma_in']) && $data['firma_in'] != '' && $data['firma_in'] != 1) {
                $imgData = base64_decode(substr($data['firma_in'], 22));
                $file    = $carpeta_firmas . 'ins_' . date('YmdHis') . '.png';

                $fp = fopen($file, 'w');
                if (fwrite($fp, $imgData)) {
                    $registro = $db
                        ->where('Id_al', $data['idregistro'])
                        ->objectBuilder()->get('registros_espacios_confinados');

                    if ($registro[0]->finstructor_al != '' && file_exists('../' . $registro[0]->finstructor_al)) {
                        unlink('../' . $registro[0]->finstructor_al);

                        $registro = $db
                            ->where('Id_al', $data['idregistro'])
                            ->update('registros_espacios_confinados', ['finstructor_al' => '']);
                    }

                    $firma_in = substr($file, 3);

                    $registro = $db
                        ->where('Id_al', $data['idregistro'])
                        ->update('registros_espacios_confinados', ['finstructor_al' => $firma_in]);

                    $informacion['status'] = true;
                } else {
                    $informacion['status'] = 'Error3';
                    $informacion['msg']    = "La firma del instructor no pudo ser adjuntada";
                }
            } elseif ($data['firma_in'] == 1) {
                $registro = $db
                    ->where('Id_al', $data['idregistro'])
                    ->objectBuilder()->get('registros_espacios_confinados');

                if ($registro[0]->finstructor_al != '' && file_exists('../' . $registro[0]->finstructor_al)) {
                    unlink('../' . $registro[0]->finstructor_al);

                    $registro = $db
                        ->where('Id_al', $data['idregistro'])
                        ->update('registros_espacios_confinados', ['finstructor_al' => '']);
                }

                $informacion['status'] = true;
            } else {
                $informacion['status'] = true;
            }
        } else {
            $informacion['status'] = false;
            $informacion['msg']    = "Ha ocurrido un error, intentelo de nuevo más tarde.";
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        session_start();

        if (empty($data['id'])) {
            $data['id'] = '%';
        }

        if (empty($data['nom'])) {
            $data['nom'] = '%';
        }

        $list = $db
            ->where('(nombre_primero_al LIKE "%' . $data['nom'] . '%" OR nombre_segundo_al LIKE "%' . $data['nom'] . '%")')
            ->where('documento_al', '%' . $data['id'] . '%', 'LIKE')
            ->objectBuilder()->get('registros_espacios_confinados');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('(nombre_primero_al LIKE "%' . $data['nom'] . '%" OR nombre_segundo_al LIKE "%' . $data['nom'] . '%")')
            ->where('documento_al', '%' . $data['id'] . '%', 'LIKE')
            ->orderBy('Id_al', 'DESC')
            ->objectBuilder()->paginate('registros_espacios_confinados', $pagina);

        $total = $db->count;
        $filas     = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $formacion = $res->formacion_al;

                if ($res->formacion_al == 'OTRO') {
                    $formacion = $res->formacion_otro_al;
                }

                $curso = '';

                $cursos = $db
                    ->where('Id_ct', $res->curso_al)
                    ->objectBuilder()->get('certificaciones');

                if ($db->count > 0) {
                    $curso = $cursos[0]->nombre;
                }

                $no_anexos  = '';
                $ls_anexos  = '';

                // $requeridos = array('Parafiscales', 'Examen Ocupacional', 'Copia Cedula', 'Certificación Anterior', 'Registro - Evidencia');

                $requeridos = array();

                switch ($res->curso_al) {
                    case '35':
                        $requeridos = array('Copia Cedula', 'Parafiscales', 'Certificado alturas vigente', 'Registro - Evidencia');
                        break;
                    case '142':
                        $requeridos = array('Copia Cedula', 'Parafiscales', 'Certificado alturas vigente', 'Examen Ocupacional', 'Formación trabajador entrante', 'Formación vigia de seguridad', 'Registro - Evidencia');
                        break;
                    case '160':
                        $requeridos = array('Copia Cedula', 'Parafiscales', 'Certificado alturas vigente', 'Examen Ocupacional', 'Formación trabajador entrante', 'Registro - Evidencia');
                        break;
                    case '161':
                        $requeridos = array('Copia Cedula', 'Titulo profesional SST', 'Licencia SST', 'Registro - Evidencia');
                        break;
                    case '162':
                        $requeridos = array('Copia Cedula', 'Parafiscales', 'Certificado alturas vigente', 'Examen Ocupacional', 'Registro - Evidencia');
                        break;
                }

                $anexos = $db
                    ->where('Id_al', $res->Id_al)
                    ->objectBuilder()->get('registro_espacios_confinados_anexos');

                $tanexos = $db->count;

                if ($tanexos > 0) {
                    if ($tanexos < 5) {
                        $no_anexos = 'style="background: #ffaeae"';
                    }
                    for ($i = 0; $i < count($requeridos); $i++) {
                        $sql = '';

                        if ($requeridos[$i] == 'Certificado alturas vigente') {
                            $anexos = $db
                                ->where('Id_al', $res->Id_al)
                                ->where('tipo_an', $requeridos[$i])
                                ->orWhere('tipo_an', 'Certificación Anterior')
                                ->objectBuilder()->get('registro_espacios_confinados_anexos');
                        } else {
                            $anexos = $db
                                ->where('Id_al', $res->Id_al)
                                ->where('tipo_an', $requeridos[$i])
                                ->objectBuilder()->get('registro_espacios_confinados_anexos');
                        }

                        if ($db->count == 0) {
                            $ls_anexos .= '<p>' . $requeridos[$i] . '</p>';
                        }
                    }
                } else {
                    for ($i = 0; $i < count($requeridos); $i++) {
                        $ls_anexos .= '<p>' . $requeridos[$i] . '</p>';
                    }
                    $no_anexos = 'style="background: #ffaeae"';
                }

                // <td><p>' . $formacion . '</p></td>

                $ciudad_expedicion = '';
                $ciudad_class = '';

                if ($res->codigo_ficha != '') {
                    $fichas = $db
                        ->where('Id', $res->codigo_ficha)
                        ->objectBuilder()->get('fichas_ministerio');

                    if ($db->count > 0) {
                        $rf = $fichas[0];

                        $ciudades = [
                            'cucuta' => 'Cúcuta',
                            'villavicencio' => 'Villavicencio',
                            '' => ''
                        ];

                        $ciudad_expedicion = $ciudades[$rf->ciudad];
                        $ciudad_class = $rf->ciudad;
                    }
                }

                $filas .= '<tr id="' . $res->Id_al . '" ' . $no_anexos . '>
                                <td><p>' . $res->Id_al . '</p></td>
                                <td><p>' . $res->fecha_al . '</p></td>
                                <td><p>' . $res->documento_al . '</p></td>
                                <td><p>' . $res->nombre_primero_al . ' ' . $res->nombre_segundo_al . ' ' . $res->apellidos_al . '</p></td>
                                <td><p>' . $curso . '</p></td>
                                <td><p>' . $res->sector_al . '</p></td>
                                <td><p>' . $res->telefono_al . '</p></td>
                                <td>' . $ls_anexos . '</td>
                                <td><p>' . $ciudad_expedicion . '</p></td>
                                <td class="ver-ficha"><a href="ficha-aprendiz-ec?id=' . $res->Id_al . '" target="_blank">Ver</a></td>
                                <td align="center"><a href="javascript://" title="Eliminar" class="eliminar"><span class="icon-eliminar"></span></a></td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                            <td colspan="7"><p style="text-align:center">No hay registros</p></td>
                        </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;
        echo json_encode($informacion);
        break;
    case 'anexos':

        $total = 260000;

        $path = realpath($_SERVER["DOCUMENT_ROOT"]);

        $sizeInBytes = getFolderSize($path);

        $en_uso = round($sizeInBytes / (1024 * 1024), 2);

        $en_uso = round($en_uso * 100 / $total, 2) + 1;

        $informacion['error'] = '';

        if ($en_uso < 100) {

            if (isset($_FILES['cedula'])) {
                $nombre_adjunto = $_FILES['cedula']['name'];
                $tmp_adjunto    = $_FILES['cedula']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_cedula_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Copia Cedula')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');

                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Copia Cedula')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Copia Cedula', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Cedula ';
                }
            }

            if (isset($_FILES['parafiscales'])) {
                $nombre_adjunto = $_FILES['parafiscales']['name'];
                $tmp_adjunto    = $_FILES['parafiscales']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_parafiscales_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Parafiscales')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');

                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('tipo_an', 'Parafiscales')
                            ->where('Id_al', $data['idregistro'])
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Parafiscales', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Parafiscales ';
                }
            }

            if (isset($_FILES['ocupacional'])) {
                $nombre_adjunto = $_FILES['ocupacional']['name'];
                $tmp_adjunto    = $_FILES['ocupacional']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_ocupacional_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Examen Ocupacional')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');

                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Examen Ocupacional')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Examen Ocupacional', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Examen ';
                }
            }

            if (isset($_FILES['certificacion'])) {
                $nombre_adjunto = $_FILES['certificacion']['name'];
                $tmp_adjunto    = $_FILES['certificacion']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_certificacion_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Certificación Anterior')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');


                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Certificación Anterior')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Certificación Anterior', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Certificación Anterior ';
                }
            }

            if (isset($_FILES['trabajadorentrante'])) {
                $nombre_adjunto = $_FILES['trabajadorentrante']['name'];
                $tmp_adjunto    = $_FILES['trabajadorentrante']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_trabajadorentrante_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Formación trabajador entrante')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');


                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Formación trabajador entrante')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Formación trabajador entrante', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Formación trabajador entrante ';
                }
            }

            if (isset($_FILES['vigiaseguridad'])) {
                $nombre_adjunto = $_FILES['vigiaseguridad']['name'];
                $tmp_adjunto    = $_FILES['vigiaseguridad']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_vigiaseguridad_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Formación vigia de seguridad')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');


                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Formación vigia de seguridad')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Formación vigia de seguridad', 'adjunto_an' => $archivador]);


                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Formación vigia de seguridad ';
                }
            }

            if (isset($_FILES['tituloprofesional'])) {
                $nombre_adjunto = $_FILES['tituloprofesional']['name'];
                $tmp_adjunto    = $_FILES['tituloprofesional']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_tituloprofesional_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Titulo profesional SST')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');


                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Titulo profesional SST')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Titulo profesional SST', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Titulo profesional SST ';
                }
            }

            if (isset($_FILES['licencia'])) {
                $nombre_adjunto = $_FILES['licencia']['name'];
                $tmp_adjunto    = $_FILES['licencia']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_licencia_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Licencia SST')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');


                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Licencia SST')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Licencia SST', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Licencia SST ';
                }
            }

            if (isset($_FILES['evidencia'])) {
                $nombre_adjunto = $_FILES['evidencia']['name'];
                $tmp_adjunto    = $_FILES['evidencia']['tmp_name'];
                $nombre         = $nombre_adjunto;
                $nombre         = limpiar($nombre);
                @$archivador    = $carpeta_anexos . $data['idregistro'] . '_' . date('YmdHis') . '_evidencia_' . $nombre;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $anexos = $db
                        ->where('Id_al', $data['idregistro'])
                        ->where('tipo_an', 'Registro - Evidencia')
                        ->objectBuilder()->get('registro_espacios_confinados_anexos');


                    if ($db->count > 0) {
                        if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                            unlink('../' . $anexos[0]->adjunto_an);
                        }

                        $borrar = $db
                            ->where('Id_al', $data['idregistro'])
                            ->where('tipo_an', 'Registro - Evidencia')
                            ->delete('registro_espacios_confinados_anexos');
                    }

                    $archivador = substr($archivador, 3);

                    $anexar = $db
                        ->insert('registro_espacios_confinados_anexos', ['Id_al' => $data['idregistro'], 'tipo_an' => 'Registro - Evidencia', 'adjunto_an' => $archivador]);

                    $informacion['status'] = true;
                } else {
                    $informacion['error'][] = 'Registro - Evidencia ';
                }
            }
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'eliminar-anexo':
        $anexos = $db
            ->where('Id_an', $data['idanexo'])
            ->objectBuilder()->get('registro_espacios_confinados_anexos');

        if ($db->count > 0) {
            if ($anexos[0]->adjunto_an != '' && file_exists('../' . $anexos[0]->adjunto_an)) {
                unlink('../' . $anexos[0]->adjunto_an);
            }

            $borrar = $db
                ->where('Id_an', $data['idanexo'])
                ->delete('registro_espacios_confinados_anexos');
        }

        break;
    case 'eliminar':
        $registros = $db
            ->where('Id_al', $data['id'])
            ->objectBuilder()->get('registros_espacios_confinados');

        if ($db->count > 0) {
            if (file_exists('../' . $registros[0]->firma_al)) {
                unlink('../' . $registros[0]->firma_al);
            }

            $eliminar = $db
                ->where('Id_al', $data['id'])
                ->delete('registros_espacios_confinados');

            if ($eliminar) {
                $anexos = $db
                    ->where('Id_al', $data['id'])
                    ->objectBuilder()->get('registro_espacios_confinados_anexos');

                if ($db->count > 0) {
                    foreach ($anexos as $anexo) {
                        if (file_exists('../' . $anexo->adjunto_an)) {
                            unlink('../' . $anexo->adjunto_an);
                        }
                    }
                }

                $anexos = $db
                    ->where('Id_al', $data['id'])
                    ->delete('registro_espacios_confinados_anexos');

                $sesiones = $db
                    ->where('Id_al', $data['id'])
                    ->delete('registro_espacios_confinados_sesiones');

                $informacion['status'] = true;
            } else {
                $informacion['status'] = false;
            }
        }
        echo json_encode($informacion);
        break;
}

function limpiar($String)
{
    $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
    $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "A", $String);
    $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "I", $String);
    $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
    $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
    $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "E", $String);
    $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
    $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "O", $String);
    $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
    $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "U", $String);
    $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
    $String = str_replace("ç", "c", $String);
    $String = str_replace("Ç", "C", $String);
    $String = str_replace("ñ", "n", $String);
    $String = str_replace("Ñ", "N", $String);
    $String = str_replace("Ý", "Y", $String);
    $String = str_replace("ý", "y", $String);
    $String = preg_replace('/\s+/', '_', $String);
    $String = str_replace(array('(', ')'), '', $String);
    return $String;
}


function getFolderSize($directory)
{
    $totalSize = 0;
    $directoryArray = scandir($directory);

    foreach ($directoryArray as $key => $fileName) {
        if ($fileName != ".." && $fileName != ".") {
            if (is_dir($directory . "/" . $fileName)) {
                $totalSize = $totalSize + getFolderSize($directory . "/" . $fileName);
            } else if (is_file($directory . "/" . $fileName)) {
                $totalSize = $totalSize + filesize($directory . "/" . $fileName);
            }
        }
    }
    return $totalSize;
}
