<?php
require "conexion.php";
@$data = $_REQUEST['registro'];

switch ($data['accion']) {
    case 'programas':
        $informacion['cursos'] = '';

        $cursos = $db
            ->where('activo_ct', 1)
            ->objectBuilder()->get('certificaciones');

        foreach ($cursos as $rsc) {
            $informacion['cursos'][] = array('id' => $rsc->Id_ct, 'nombre' => $rsc->nombre);
        }

        if ($data['tipo'] == 'CC') {
        } else if ($data['tipo'] == 'TL') {
        }
        echo json_encode($informacion);
        break;
    case 'registro':
        date_default_timezone_set("America/Bogota");

        $nfecha = date('Y-m-j');

        $no_requeridos = array('icfes_colombia', 'numero_registro', 'tipo_identificacion_icfes', 'numero_identificacion_icfes', 'fecha_presentacion_icfes', 'puntaje_matematicas', 'puntaje_ciencias_sociales', 'puntaje_ingles', 'puntaje_lenguaje', 'puntaje_filosofia', 'puntaje_biologia', 'puntaje_quimica', 'puntaje_fisica', 'puntaje_electiva');

        for ($i = 0; $i < count($no_requeridos); $i++) {
            if (!isset($data[$no_requeridos[$i]])) {
                $data[$no_requeridos[$i]] = '';
            }
        }

        $datos = [
            'registro' => $nfecha,
            'tipo_identificacion' => $data['tipo_identificacion'],
            'numero_identificacion' => $data['numero_identificacion'],
            'fecha_nacimiento' => $data['fecha_nacimiento'],
            'periodo' => $data['periodo'],
            'programa' => $data['programa'],
            'curso' => $data['curso'],
            'horario' => $data['horario'],
            'tipo_solicitud' => $data['tipo_solicitud'],
            'aspirantes_nombres' => $data['aspirantes_nombres'],
            'aspirante_apellidos' => $data['aspirante_apellidos'],
            'lugar_nacimiento' => $data['lugar_nacimiento'],
            'genero' => $data['genero'],
            'estado_civil' => $data['estado_civil'],
            'aspirante_direccion' => $data['aspirante_direccion'],
            'aspirante_ciudad' => $data['aspirante_ciudad'],
            'aspirante_celular' => $data['aspirante_celular'],
            'aspirante_telefono' => $data['aspirante_telefono'],
            'aspirante_correo' => $data['aspirante_correo'],
            'graduado_colombia' => $data['graduado_colombia'],
            'ano_graduacion' => $data['ano_graduacion'],
            'colegio_graduacion' => $data['colegio_graduacion'],
            'ciudad_graduacion' => $data['ciudad_graduacion'],
            'grado_obtenido' => $data['grado_obtenido'],
            'bachiller' => $data['bachiller'],
            'icfes_colombia' => $data['icfes_colombia'],
            'numero_registro' => $data['numero_registro'],
            'tipo_identificacion_icfes' => $data['tipo_identificacion_icfes'],
            'numero_identificacion_icfes' => $data['numero_identificacion_icfes'],
            'fecha_presentacion_icfes' => $data['fecha_presentacion_icfes'],
            'puntaje_matematicas' => $data['puntaje_matematicas'],
            'puntaje_ciencias_sociales' => $data['puntaje_ciencias_sociales'],
            'puntaje_ingles' => $data['puntaje_ingles'],
            'puntaje_lenguaje' => $data['puntaje_lenguaje'],
            'puntaje_filosofia' => $data['puntaje_filosofia'],
            'puntaje_biologia' => $data['puntaje_biologia'],
            'puntaje_quimica' => $data['puntaje_quimica'],
            'puntaje_fisica' => $data['puntaje_fisica'],
            'puntaje_electiva' => $data['puntaje_electiva'],
            'responsable_parentesco' => $data['responsable_parentesco'],
            'responsable_nombres' => $data['responsable_nombres'],
            'responsable_apellidos' => $data['responsable_apellidos'],
            'responsable_telefono' => $data['responsable_telefono'],
            'responsable_celular' => $data['responsable_celular'],
            'responsable_correo' => $data['responsable_correo'],
            'financiacion' => $data['financiacion'],
            'medio_informacion' => $data['medio_informacion'],
            'otro_medio' => $data['otro_medio']
        ];

        $nuevo = $db
            ->insert('aspirantes', $datos);

        if ($nuevo) {
            $nombre_as     = $data['aspirantes_nombres'] . ' ' . $data['aspirante_apellidos'];
            $correo_as     = $data['aspirante_correo'];
            $identifica_as = $data['tipo_identificacion'] . ' ' . $data['numero_identificacion'];

            // $cabeceras = "From: GRI COMPANY S.A.S - Registro aspirantes";
            // $asunto    = "Nuevo registro de aspirante";
            // $email_to  = "registro.gricompany@gmail.com";
            // // $email_to  = "heynerfair@gmail.com";
            // $contenido = "Nuevo mensaje desde www.gricompany.co - Registro aspirantes\n"
            //     . "\n"
            //     . "Datos del aspirante \n\n"
            //     . "Nombre: $nombre_as\n"
            //     . "Identificación: $identifica_as \n"
            //     . "Email: $correo_as \n"
            //     . "\n";

            // @mail($email_to, $asunto, $contenido, $cabeceras);

            require("class.phpmailer.php");
            $mail = new PHPMailer(true);
            $mail->Host = "localhost";

            $mail->FromName = "GRI COMPANY S.A.S - Registro aspirantes";
            $mail->Subject = 'Nuevo registro de aspirante';
            $mail->AddAddress("registro.gricompany@gmail.com");
            $body = "<strong>Nuevo mensaje desde www.gricompany.co - Registro aspirantes</strong><br><br>";
            $body .= "Datos del aspirante <br>";
            $body .= "Nombre: $nombre_as <br>";
            $body .= "Identificación: $identifica_as<br>";
            $body .= "Email: $correo_as<br>";
            $mail->Body = $body;
            $mail->IsHTML(true);

            if ($mail->send()) {
            }

            $informacion['formulario'] = $nuevo;
            $informacion['status']     = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        session_start();

        $list = $db
            ->objectBuilder()->get('aspirantes');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 20;
        $pagina       = (int) (isset($_POST['pagina']) ? $_POST['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }
            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->orderBy('Id_as', 'DESC')
            ->objectBuilder()->paginate('aspirantes', $pagina);

        $total     = $db->count;
        $filas     = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $horario = '';

                if ($res->horario != '') {
                    ($res->horario == 'D' ? $horario = 'Diurno' : $horario = 'Nocturno');
                }

                $programa = '';

                if ($res->programa == 'TL') {
                    $programa = 'Técnica Laboral por competencias';
                } elseif ($res->programa == 'CC') {
                    $programa = 'Cursos Cortos educación informal';
                }

                $curso = '';

                if ($res->programa == 'TL') {
                    $c = $db
                        ->where('Id_ct', $res->curso)
                        ->where('activo_ct', 1)
                        ->objectBuilder()->get('certificaciones');

                    if ($db->count > 0) {
                        $curso = $c[0]->nombre;
                    }
                } elseif ($res->programa == 'CC') {
                    $c = $db
                        ->where('Id_ct', $res->curso)
                        ->where('activo_ct', 1)
                        ->objectBuilder()->get('certificaciones');

                    if ($db->count > 0) {
                        $curso = $c[0]->nombre;
                    }
                }

                $filas .= '<tr id="' . $res->Id_as . '">
                                <td><p>' . $res->Id_as . '</p></td>
                                <td><p>' . $res->registro . '</p></td>
                                <td><p>' . $res->tipo_identificacion . ' ' . $res->numero_identificacion . '</p></td>
                                <td><p>' . $res->aspirantes_nombres . '</p></td>
                                <td><p>' . $res->aspirante_apellidos . '</p></td>
                                <td><p>' . $res->aspirante_ciudad . '</p></td>
                                <td><p>' . $res->aspirante_telefono . ' ' . $res->aspirante_celular . '</p></td>
                                <td><p>' . $programa . '</p></td>
                                <td><p>' . $curso . '</p></td>
                                <td><p>' . $res->periodo . '</p></td>
                                <td><p>' . $horario . '</p></td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="11"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;
        echo json_encode($informacion);
        break;
}
