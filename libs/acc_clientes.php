<?php
require "conexion.php";
@$informacion = array();
$opc          = $_REQUEST['accion'];
$data         = $_REQUEST['cliente'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

switch ($opc) {
    case 'nuevo_cliente':
        $datos = [
            'razon' => $data['razon'],
            'nit' => $data['nit'],
            'representante' => $data['representante'],
            'arl' => $data['arl'],
            'sector' => $data['sector']
        ];

        $nuevo = $db
            ->insert('clientes', $datos);

        if ($nuevo) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'listar':
        @$buscar_nom = $_POST['nom'];

        if ($buscar_nom == '') {
            $buscar_nom = '%';
        }

        $list = $db
            ->where('razon', '%' . $buscar_nom . '%', 'LIKE')
            ->objectBuilder()->get('clientes');

        $total        = $db->count;
        $adyacentes   = 2;
        $registro_pag = 100;
        $pagina       = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina       = ($pagina == 0 ? 1 : $pagina);
        $inicio       = ($pagina - 1) * $registro_pag;

        $siguiente  = $pagina + 1;
        $anterior   = $pagina - 1;
        $ultima_pag = ceil($total / $registro_pag);
        $penultima  = $ultima_pag - 1;

        $paginacion = '';

        if ($ultima_pag > 1) {
            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://'' onClick='cambiar_pag(1);''>&laquo; Primera</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Primera</span>";
            }

            if ($pagina > 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($anterior) . ");'>&laquo; Anterior&nbsp;&nbsp;</a>";
            } else {
                $paginacion .= "<span class='disabled'>&laquo; Anterior&nbsp;&nbsp;</span>";
            }

            if ($ultima_pag < 7 + ($adyacentes * 2)) {
                for ($contador = 1; $contador <= $ultima_pag; $contador++) {
                    if ($contador == $pagina) {
                        $paginacion .= "<span class='actual'>$contador</span>";
                    } else {
                        $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                    }
                }
            } elseif ($ultima_pag > 5 + ($adyacentes * 2)) {
                if ($pagina < 1 + ($adyacentes * 2)) {
                    for ($contador = 1; $contador < 4 + ($adyacentes * 2); $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "...";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'> $penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } elseif ($ultima_pag - ($adyacentes * 2) > $pagina && $pagina > ($adyacentes * 2)) {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "...";
                    for ($contador = $pagina - $adyacentes; $contador <= $pagina + $adyacentes; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                    $paginacion .= "..";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($penultima) . ");'>$penultima</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>$ultima_pag</a>";
                } else {
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(1);'>1</a>";
                    $paginacion .= "<a href='javascript://' onClick='cambiar_pag(2);'>2</a>";
                    $paginacion .= "..";
                    for ($contador = $ultima_pag - (2 + ($adyacentes * 2)); $contador <= $ultima_pag; $contador++) {
                        if ($contador == $pagina) {
                            $paginacion .= "<span class='actual'>$contador</span>";
                        } else {
                            $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($contador) . ");'>$contador</a>";
                        }
                    }
                }
            }

            if ($pagina < $contador - 1) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($siguiente) . ");'>Siguiente &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Siguiente &raquo;</span>";
            }

            if ($pagina < $ultima_pag) {
                $paginacion .= "<a href='javascript://' onClick='cambiar_pag(" . ($ultima_pag) . ");'>Última &raquo;</a>";
            } else {
                $paginacion .= "<span class='disabled'>Última &raquo;</span>";
            }
        }

        $db->pageLimit = $registro_pag;

        $registros = $db
            ->where('razon', '%' . $buscar_nom . '%', 'LIKE')
            ->orderBy('Id', 'DESC')
            ->objectBuilder()->paginate('clientes', $pagina);

        $total     = $db->count;
        $filas     = '';

        if ($total > 0) {
            foreach ($registros as $res) {
                $filas .= '<tr id="' . $res->Id . '">
                                <td><p>' . $res->razon . '</p></td>
                                <td><p>' . $res->nit . '</p></td>
                                <td><p>' . $res->representante . '</p></td>
                                <td><p>' . $res->arl . '</p></td>
                                <td><p>' . $res->sector . '</p></td>
                                <td>
                                <a href="clientes_edt?cliente=' . $res->Id . '" target="_blank" title="Editar" class="editar"><span class="icon-editar"></span></a>
                                </td>
                                <td>
                                <a href="javascript://" title="Eliminar" class="eliminar"><span class="icon-eliminar"></span></a>
                                </td>
                            </tr>';
            }
        } else {
            $filas = '<tr>
                        <td colspan="5"><p style="text-align:center">No hay registros</p></td>
                    </tr>';
        }

        $informacion['registros']  = $filas;
        $informacion['paginacion'] = $paginacion;

        echo json_encode($informacion);
        break;
    case 'editar_cliente':
        $datos = [
            'razon' => $data['razon'],
            'nit' => $data['nit'],
            'representante' => $data['representante'],
            'arl' => $data['arl'],
            'sector' => $data['sector']
        ];

        $act = $db
            ->where('Id', $data['idcliente'])
            ->update('clientes', $datos);

        if ($act) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'eliminar_cliente':
        $elim = $db
            ->where('Id', $data['idcliente'])
            ->delete('clientes');

        if ($elim) {
            $informacion['status'] = true;
        } else {
            $informacion['status'] = false;
        }

        echo json_encode($informacion);
        break;
    case 'buscar_cliente':
        $registros = $db
            ->where('razon', '%' . $data['razon'] . '%', 'LIKE')
            ->objectBuilder()->get('clientes');

        foreach ($registros as $rbs) {
            $clientes = $rbs->Id . '|' . $rbs->razon . '|' . $rbs->nit . '|' . $rbs->representante . '|' . $rbs->arl. '|' . $rbs->sector;
            array_push($informacion, $clientes);
        }

        echo json_encode($informacion);
        break;
}
