<div class="seguridad" style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Crear Certificados</h2>
		<div class="Registro">
			<div class="Registro-cent">
				<label>Tipo *</label>
				<select  id="tipo-seg">
					<option value="1">Arnes de Seguridad</option>
					<option value="2">Eslinga de Seguridad</option>
				</select>
				<!-- <label>Reporte Nº *</label>
				<input type="text" placeholder="Reporte Nº" name="certifica[reporte]" value="<?php echo $nensayo ?>" class="ncertificado" readonly> -->
				<!-- <label>Certificado *</label>
				<select name="certifica[certificado]" id="sel_certificado" required>
					<?php echo $lista_certificado ?>
				</select> -->
			</div>
		</div>
		<form class="cre_certificado cer_arnes">
			<div class="Registro">
				<hr>
				<label>Datos Generales</label>
				<div class="Registro-der">
					<input type="hidden" name="certifica[tipo]" class="tipocer" value="1">
					<label>Propietario</label>
					<input type="text" placeholder="Propietario" name="certifica[propietario]" required>
					<label>Dirección</label>
					<input type="text" placeholder="Dirección" name="certifica[direccion]" required>
					<label>Teléfono</label>
					<input type="text" placeholder="Teléfono" name="certifica[telefono]" required>
				</div>
				<div class="Registro-der">
					<label>Ubicación</label>
					<input type="text" placeholder="Ubicación" name="certifica[ubicacion]" class="auto_ciu" required>
					<label>Área de trabajo</label>
					<input type="text" placeholder="Área de trabajo" name="certifica[atrabajo]" required>
					<label>Nombre Contacto </label>
					<input type="text" placeholder="Nombre contacto" name="certifica[contacto]" >
				</div>
				<hr>
				<label>Descripción del equipo</label>
				<div class="Registro-der">
					<label>Nombre</label>
					<input type="text" placeholder="Nombre" name="certifica[equipo]" >
				</div>
				<div class="Registro-der">
					<label>Marca</label>
					<input type="text" placeholder="Marca" name="certifica[marca]" >
				</div>
				<hr>
				<label>Identificación del equipo</label>
				<div class="Registro-der">
					<label>Serie</label>
					<input type="text" placeholder="Serie" name="certifica[serie]" >
					<label>Modelo</label>
					<input type="text" placeholder="Modelo" name="certifica[modelo]" required>
					<label>N° Inspección</label>
					<input type="text" placeholder="N° Inspección" name="certifica[ninspeccion]" required>
					<label>FF</label>
					<input type="date" placeholder="FF" name="certifica[ff]" required>
					<label>Primer Uso</label>
					<input type="text" placeholder="Primer Uso" name="certifica[puso]" required>
				</div>
				<div class="Registro-der">
					<label>Referencia</label>
					<input type="text" placeholder="Referencia" name="certifica[referencia]" required>
					<label>Lote</label>
					<input type="text" placeholder="Lote" name="certifica[lote]" required>
					<label>Talla</label>
					<input type="text" placeholder="Talla" name="certifica[talla]" required>
					<label>Fecha Compra</label>
					<input type="date" placeholder="Fecha Compra" name="certifica[fcompra]" required>
				</div>
				<hr>
				<label>VERIFICACIÓN VISUAL DE LOS COMPONENTES</label>
				<div class="Registro-der">
					<label>Estado general de las etiquetas</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de las reatas</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de los testigos de impacto</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de las costuras</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<div class="Registro-der">
					<label>Estado general de las argollas en “d”</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de los pasadores y hebillas de ajuste</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general del pad plastico</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<hr>
				<label>VERIFICACIÓN FUNCIONAL DE LOS COMPONENTES</label>
				<div class="Registro-der">
					<label>Las reatas se deslizan facilmente por las hebillas de ajuste</label>
					<select name="certifica[funcional][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Ergonomía y maleabilidad de las reatas</label>
					<select name="certifica[funcional][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<div class="Registro-der">
					<label>Los pasadores cierran y se ajustan correctamente</label>
					<select name="certifica[funcional][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<hr>
				<label>Observaciones </label>
				<textarea name="certifica[observaciones]"></textarea>
				<hr>
				<label>Concepto de inspección</label>
				<div class="Registro-der">
					<label>Aprobado</label>
					<select name="certifica[aprobado]" required>
						<option value="1">SI</option>
						<option value="0">NO</option>
					</select>
				</div>
				<div class="Registro-der">
					<label>Código de aprobacion gri</label>
					<input type="text" placeholder="Código de aprobacion gri" name="certifica[codaprobacion]">
				</div>
				<hr>
				<label>Control de fechas de inspección</label>
				<div class="Registro-der">
					<label>Control 1</label>
					<input type="date" placeholder="Control 1" name="certifica[control1]">
				</div>
				<div class="Registro-der">
					<label>Control 2</label>
					<input type="date" placeholder="Control 2" name="certifica[control2]">
				</div>
				<div class="Registro-der">
					<label>Control 3</label>
					<input type="date" placeholder="Control 3" name="certifica[control3]">
				</div>
				<div class="Registro-der">
				</div>
				<br>
				<br>
				<hr>
				<label>Registro Fotográfico</label>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto1">ETIQUETA</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto2">EST. GENERAL EQUIPO</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto3">PAD PLASTICO</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto4">INSPEC. CERTIFICACIÓN</button>
					</div>
				</div>
				<input type="hidden" name="certifica[accion]" value="seguridad">
				<input type="submit" value="Guardar Certificado">
			</div>
		</form>
		<form class="cre_certificado cer_eslinga" style="display: none">
			<div class="Registro">
				<hr>
				<label>Datos Generales</label>
				<div class="Registro-der">
					<input type="hidden" name="certifica[tipo]" class="tipocer" value="">
					<label>Propietario</label>
					<input type="text" placeholder="Propietario" name="certifica[propietario]" required>
					<label>Dirección</label>
					<input type="text" placeholder="Dirección" name="certifica[direccion]" required>
					<label>Teléfono</label>
					<input type="text" placeholder="Teléfono" name="certifica[telefono]" required>
				</div>
				<div class="Registro-der">
					<label>Ubicación</label>
					<input type="text" placeholder="Ubicación" name="certifica[ubicacion]" class="auto_ciu" required>
					<label>Área de trabajo</label>
					<input type="text" placeholder="Área de trabajo" name="certifica[atrabajo]" required>
					<label>Nombre Contacto </label>
					<input type="text" placeholder="Nombre contacto" name="certifica[contacto]" >
				</div>
				<hr>
				<label>Descripción del equipo</label>
				<div class="Registro-der">
					<label>Nombre</label>
					<input type="text" placeholder="Nombre" name="certifica[equipo]" >
				</div>
				<div class="Registro-der">
					<label>Marca</label>
					<input type="text" placeholder="Marca" name="certifica[marca]" >
				</div>
				<hr>
				<label>Identificación del equipo</label>
				<div class="Registro-der">
					<label>Serie</label>
					<input type="text" placeholder="Serie" name="certifica[serie]" >
					<label>Modelo</label>
					<input type="text" placeholder="Modelo" name="certifica[modelo]" required>
					<label>N° Inspección</label>
					<input type="text" placeholder="N° Inspección" name="certifica[ninspeccion]" required>
					<label>FF</label>
					<input type="date" placeholder="FF" name="certifica[ff]" required>
					<label>Primer Uso</label>
					<input type="text" placeholder="Primer Uso" name="certifica[puso]" required>
					<label>Serie ABS</label>
					<input type="text" placeholder="Serie ABS" name="certifica[serieabs]" required>
				</div>
				<div class="Registro-der">
					<label>Referencia</label>
					<input type="text" placeholder="Referencia" name="certifica[referencia]" required>
					<label>Lote</label>
					<input type="text" placeholder="Lote" name="certifica[lote]" required>
					<label>Talla</label>
					<input type="text" placeholder="Talla" name="certifica[talla]" required>
					<label>Fecha Compra</label>
					<input type="date" placeholder="Fecha Compra" name="certifica[fcompra]" required>
					<label>Ref. ABS</label>
					<input type="text" placeholder="Ref. ABS" name="certifica[refabs]" required>
					<label>FF. ABS</label>
					<input type="text" placeholder="FF. ABS" name="certifica[ffabs]" required>
				</div>
				<hr>
				<label>VERIFICACIÓN VISUAL DE LOS COMPONENTES</label>
				<div class="Registro-der">
					<label>Estado general de las etiquetas</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de la cuerda – reata – guaya</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de los testigos de impacto</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de las costuras</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<div class="Registro-der">
					<label>Estado general de los ganchos</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general de los pasadores y hebillas de ajuste</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Estado general del abs</label>
					<select name="certifica[visual][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<hr>
				<label>VERIFICACIÓN FUNCIONAL DE LOS COMPONENTES</label>
				<div class="Registro-der">
					<label>Las eslinga se deslizan facilmente por las hebillas de ajuste</label>
					<select name="certifica[funcional][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
					<label>Ergonomía y maleabilidad de la eslinga</label>
					<select name="certifica[funcional][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<div class="Registro-der">
					<label>Los pasadores cierran y se ajustan correctamente</label>
					<select name="certifica[funcional][]">
						<option value="C">C</option>
						<option value="NC">NC</option>
						<option value="RM">RM</option>
						<option value="NA">NA</option>
					</select>
				</div>
				<hr>
				<label>Observaciones </label>
				<textarea name="certifica[observaciones]"></textarea>
				<hr>
				<label>Concepto de inspección</label>
				<div class="Registro-der">
					<label>Aprobado</label>
					<select name="certifica[aprobado]" required>
						<option value="1">SI</option>
						<option value="0">NO</option>
					</select>
				</div>
				<div class="Registro-der">
					<label>Código de aprobacion gri</label>
					<input type="text" placeholder="Código de aprobacion gri" name="certifica[codaprobacion]">
				</div>
				<hr>
				<label>Control de fechas de inspección</label>
				<div class="Registro-der">
					<label>Control 1</label>
					<input type="date" placeholder="Control 1" name="certifica[control1]">
				</div>
				<div class="Registro-der">
					<label>Control 2</label>
					<input type="date" placeholder="Control 2" name="certifica[control2]">
				</div>
				<div class="Registro-der">
					<label>Control 3</label>
					<input type="date" placeholder="Control 3" name="certifica[control3]">
				</div>
				<div class="Registro-der">
				</div>
				<br>
				<br>
				<hr>
				<label>Registro Fotográfico</label>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto1">ETIQUETA</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto2">EST. GENERAL EQUIPO</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto3">PAD PLASTICO</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev-sg">
						<button type="button" class="placa" id="foto4">INSPEC. CERTIFICACIÓN</button>
					</div>
				</div>
				<input type="hidden" name="certifica[accion]" value="seguridad">
				<input type="submit" value="Guardar Certificado">
			</div>
		</form>
	</div>
</div>