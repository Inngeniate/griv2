<div class="inspeccion" style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Listar Inspecciones</h2>
		<form id="buscar5">
			<label>Placa Vehículo: </label>
			<input type="text" name="certifica[placa]" id="placav" placeholder="Placa vehículo">
			<input type="hidden" name="certifica[accion]" value="lista_inspeccion">
			<input type="hidden" name="pagina" value="0">
			<input type="submit" value="Buscar">
		</form>
		<div class="Listar-personas">
			<div class="Tabla-listar">
				<table>
					<thead>
						<tr>
							<th>Fecha</th>
							<th>Placa Vehículo</th>
							<th>Tipo Vehículo</th>
							<th>Propietario</th>
							<th>Editar</th>
						</tr>
					</thead>
					<tbody id="resultados5">
					</tbody>
				</table>
			</div>
		</div>
		<div id="lista_loader5">
			<div class="loader2">Cargando...</div>
		</div>
		<div id="paginacion" class="pagina5"></div>
	</div>
</div>
