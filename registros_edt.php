<?php
session_start();
require("libs/conexion.php");
if (!$_SESSION['usuloggri']) header('Location: admin');
else {
	$idre = $_GET['registro'];

	$registro = $db
		->where('Id', $idre)
		->objectBuilder()->get('registros');

	$res = $registro[0];

	switch ($res->tipo_ident) {
		case 'CC':
			$option1 = 'selected';
			$option2 = '';
			$option3 = '';
			$option4 = '';
			$option5 = '';
			$option6 = '';
			break;
		case 'TI':
			$option1 = '';
			$option2 = 'selected';
			$option3 = '';
			$option4 = '';
			$option5 = '';
			$option6 = '';
			break;
		case 'CE':
			$option1 = '';
			$option2 = '';
			$option3 = 'selected';
			$option4 = '';
			$option5 = '';
			$option6 = '';
			break;
		case 'PP':
			$option1 = '';
			$option2 = '';
			$option3 = '';
			$option4 = 'selected';
			$option5 = '';
			$option6 = '';
			break;
		case 'PEP':
			$option1 = '';
			$option2 = '';
			$option3 = '';
			$option4 = '';
			$option5 = 'selected';
			$option6 = '';
			break;
		case 'NIUP':
			$option1 = '';
			$option2 = '';
			$option3 = '';
			$option4 = '';
			$option5 = '';
			$option6 = 'selected';
			break;
	}
	if ($res->sexo == 'M') {
		$optionsx1 = 'selected';
		$optionsx2 = '';
	} elseif ($res->sexo == 'F') {
		$optionsx1 = '';
		$optionsx2 = 'selected';
	}
}

$lista_cetificacion = '';



$lista_capacitacion = '';

$competencias = $db
	->where('activo_cp', 1)
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$lista_capacitacion .= '<option value="' . $rcp->nombre_cp . '" data-nombre="' . $rcp->alias_cp . '" ' . ($rcp->nombre_cp == $res->capacitacion ? "selected" : "") . '>' . $rcp->nombre_cp . '</option>';

	if ($res->capacitacion != '' && $rcp->nombre_cp == $res->capacitacion) {
		$cursos = $db
			->where('activo_ct', 1)
			->where('competencia_ct', $rcp->alias_cp)
			->objectBuilder()->get('certificaciones');

		foreach ($cursos as $rsc) {
			$lista_cetificacion .= '<option value="' . $rsc->Id_ct . '" ' . ($rsc->Id_ct == $res->certificado ? 'selected' : '') . ' >' . $rsc->nombre . '</option>';
		}
	}
}

$ver_entrenadores = 'style="display:none"';

if ($res->capacitacion == 'ALTURAS' || $res->capacitacion == 'CEA' || $res->capacitacion == 'ESPACIOS CONFINADOS') {
	$ver_entrenadores = 'style="display:block"';
}

$ls_entrenadores = '';

$entrenadores = $db
	->where('estado_en', 1)
	->orderBy('nombre_en', 'ASC')
	->objectBuilder()->get('entrenadores');

foreach ($entrenadores as $rsc) {
	$ls_entrenadores .= '<option value="' . $rsc->Id_en . '" ' . ($rsc->Id_en == $res->entrenador ? "selected" : "") . ' data-tipo="' . $rsc->competencia_en . '">' . $rsc->nombre_en . ' ' . $rsc->apellido_en . '</option>';
}

$foto = 'images/marca_agua.jpg';

if ($res->foto_ct != '') {
	$foto = substr($res->foto_ct, 3) . '?' . time();
}

$ls_vendedores = '';

$vendedores = $db
	->orderBy('nombre_v', 'ASC')
	->objectBuilder()->get('vendedores');

if ($db->count > 0) {
	foreach ($vendedores as $rsv) {
		$cursos = $db
			->where('vendedor_vc', $rsv->Id_v)
			->objectBuilder()->get('vendedores_cursos');

		if ($db->count > 0) {
			$ls_vendedores .= '<option value="' . $rsv->nombre_v . '" id="Ve-' . $rsv->Id_v . '" data-direccion="' . $rsv->direccion_v . '"  data-telefono="' . $rsv->telefono_v . '" data-correo="' . $rsv->correo_v . '" ' . ($rsv->nombre_v == $res->vendedor ? "selected" : "") . '>' . $rsv->nombre_v . '</option>';
		}
	}
}

$ls_codigos = '';

$codigos_fichas = $db
	->where('en_uso < cantidad')
	->orWhere('competencia', 1)
	->objectBuilder()->get('fichas_ministerio');

foreach ($codigos_fichas as $rcf) {
	$competencias = $db
		->where('Id_cp', $rcf->competencia)
		->objectBuilder()->get('competencias');

	$rcp = $competencias[0];

	$disponibles = $rcf->cantidad - $rcf->en_uso;

	if ($res->capacitacion == 'CURSO BASICO') {
		$res->codigo_ficha = 4;
	}

	$ls_codigos .= '<option value="' . $rcf->Id . '" data-competencia="' . $rcp->nombre_cp . '" data-curso="' . $rcf->niveles . '" data-entrenador="' . $rcf->entrenador . '" data-inicio="' . $rcf->inicio . '" data-fin="' . $rcf->fin . '" ' . ($res->codigo_ficha == $rcf->Id ? 'selected' : '') . '>' . $rcf->codigo . ' - ' . $disponibles . '</option>';
}

$ls_ciudades = '';

$ciudades = [
	'cucuta' => 'Cúcuta',
	'villavicencio' => 'Villavicencio',
];

foreach ($ciudades as $key => $value) {
	$ls_ciudades .= '<option value="' . $key . '" ' . ($key == $res->ciudad ? 'selected' : '') . '>' . $value . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" type="text/css" href="css/select2.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		.Error-espacio {
			border: 2px solid red !important;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Editar Registro</h2>
				<form id="registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Código del ministerio*</label>
							<select name="codigo_ficha" class="Sel-codigo" disabled>
								<option value="">Seleccione el código</option>
								<?php echo $ls_codigos; ?>
							</select>
						</div>
						<div class="Registro-der">
							<div class="CBasico-ciudad">
								<label>Ciudad *</label>
								<select name="ciudad" class="Sel-ciudad" required>
									<option value="">Seleccione una ciudad</option>
									<?php echo $ls_ciudades; ?>
								</select>
							</div>
						</div>
						<div class="Registro-der">
							<label>Primer Nombre *</label>
							<input type="text" placeholder="Primer Nombre" name="primer_nombre" class="No-espacio" value="<?php echo $res->nombre_primero ?>" required>
							<label>Apellidos *</label>
							<input type="text" placeholder="Apellidos" name="apellido" value="<?php echo $res->apellidos ?>" class="No-espacio" required>
							<label>Tipo de Identificación *</label>
							<select name="tipo_id" required>
								<option <?php echo $option1 ?>>CC</option>
								<option <?php echo $option2 ?>>TI</option>
								<option <?php echo $option3 ?>>CE</option>
								<option <?php echo $option4 ?>>PP</option>
								<option <?php echo $option5 ?>>PEP</option>
								<option <?php echo $option6 ?>>NIUP</option>
							</select>
							<label>Identificación *</label>
							<input type="text" placeholder="Identificación" name="identifica" value="<?php echo $res->numero_ident ?>" required class="No-espacio">
							<label>Sexo *</label>
							<select name="sexo" required>
								<option <?php echo $optionsx1 ?>>M</option>
								<option <?php echo $optionsx2 ?>>F</option>
							</select>
							<label>RH </label>
							<select name="rh" required>
								<option value="">Seleccione</option>
								<option value="O-" <?php echo ('O-' == $res->rh ? "selected" : "") ?>>O-</option>
								<option value="O+" <?php echo ('O+' == $res->rh ? "selected" : "") ?>>O+</option>
								<option value="A-" <?php echo ('A-' == $res->rh ? "selected" : "") ?>>A-</option>
								<option value="A+" <?php echo ('A+' == $res->rh ? "selected" : "") ?>>A+</option>
								<option value="B-" <?php echo ('B-' == $res->rh ? "selected" : "") ?>>B-</option>
								<option value="B+" <?php echo ('B+' == $res->rh ? "selected" : "") ?>>B+</option>
								<option value="AB-" <?php echo ('AB-' == $res->rh ? "selected" : "") ?>>AB-</option>
								<option value="AB+" <?php echo ('AB+' == $res->rh ? "selected" : "") ?>>AB+</option>
							</select>
							<label>Empresa </label>
							<input type="text" placeholder="Empresa" name="empresa" value="<?php echo $res->empresa ?>">
							<label>Vencimiento - Carnet</label>
							<input type="date" placeholder="Vencimiento" name="vencimiento" value="<?php echo $res->vencimiento_ct ?>" required>
							<label>Fecha de inicio *</label>
							<input type="date" name="f_inicio" value="<?php echo $res->fecha_inicio ?>" required>
							<label>Fecha de Vigencia *</label>
							<input type="date" name="f_vigencia" value="<?php echo $res->fecha_vigencia ?>" required>
							<!--<label>Tipo de pago *</label>
							<select name="tpago" required>
								<option value="">Seleccione</option>
								<option value="cr" </?php echo ($res->tpago == 'cr' ? "selected" : "") ?>>Crédito</option>
								<option value="ct" </?php echo ($res->tpago == 'ct' ? "selected" : "") ?>>Contado</option>
							</select>-->
						</div>

						<div class="Registro-der">
							<label>Segundo Nombre</label>
							<input type="text" placeholder="Segundo Nombre" name="segundo_nombre" class="No-espacio" value="<?php echo $res->nombre_segundo ?>">
							<label>Vendedor *</label>
							<select name="vendedor" class="Sel-vendedor" required>
								<option value="">Seleccione</option>
								<?php echo $ls_vendedores ?>
							</select>
							<label>Competencia</label>
							<select name="capacitacion" class="capacitacion" required>
								<option value="">Seleccione</option>
								<?php echo $lista_capacitacion ?>
							</select>
							<div id="ver-entrenadores" <?php echo $ver_entrenadores ?>>
								<label>Entrenadores *</label>
								<select name="entrenador" class="entrenadores">
									<option value="">Seleccione</option>
									<?php echo $ls_entrenadores ?>
								</select>
							</div>
							<label>Certificación a realizar *</label>
							<select name="certificado" class="ls_cursos" required>
								<option value="">Seleccione</option>
								<?php echo $lista_cetificacion ?>
							</select>
							<label>Dirección *</label>
							<input type="text" placeholder="Dirección" name="direccion" class="vendedor_direccion" value="<?php echo $res->direccion ?>" required>
							<label>Email *</label>
							<input type="email" placeholder="Email" name="email" class="vendedor_correo" value="<?php echo $res->correo ?>" required>
							<label>Teléfono de contacto *</label>
							<input type="text" placeholder="Teléfono" name="telefono" class="vendedor_telefono" value="<?php echo $res->telefono ?>" required>
							<label>Cargo </label>
							<input type="text" placeholder="Cargo" name="cargo" value="<?php echo $res->cargo ?>">
							<?php if ($res->capacitacion != 'ALTURAS' && $res->capacitacion != 'ESPACIOS CONFINADOS' && $res->capacitacion != 'ALTURAS RES. 4272') { ?>
								<label>Licencia </label>
								<input type="text" placeholder="Licencia" name="licencia" value="<?php echo $res->licencia ?>">
							<?php } ?>
							<label>Fecha de finalización</label>
							<input type="date" name="f_fin" value="<?php echo $res->fecha_fin ?>">
							<label>Horas *</label>
							<input type="text" placeholder="Horas" name="horas" value="<?php echo $res->horas ?>" required>
							<!--<label>Precio *</label>
							<input type="text" placeholder="Precio" name="precio" value="</?php echo $res->precio ?>" required>-->
						</div>
						<div class="Registro-der">
						</div>
						<div class="Registro-der">
						</div>
						<div class="Registro-der">
							<div class="img-placa">
								<img class="img-prev" src="<?php echo $foto  ?>">
								<button type="button" class="placa" id="foto">FOTO CARNET</button>
							</div>
						</div>
						<div class="Registro-der">
						</div>
						<br><br>
						<div id="cursos_adicionales">
						</div>
						<div class="Registro-cent">
							<button type="button" id="agr_curso">Adicionar Curso</button>
						</div>
						<input type="hidden" name="id" value="<?php echo $idre ?>">
						<br>
						<br>
						<input type="submit" value="Guardar Registro">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/select2.min.js"></script>
	<script type="text/javascript" src="js/registrar_edt.js?<?php echo time()  ?>"></script>
</body>

</html>
