<div class="Contenido-admin-izq listado-precios">
	<h2>Listar</h2>
	<hr>
	<p></p>
	<br>
	<form id="buscar">
		<label>Nombre: </label>
		<input type="text" name="usuario[nom]" class="nom" placeholder="Nombre">
		<input type="submit" value="Buscar">
	</form>
	<br>
	<div class="Listar-personas">
		<div class="Tabla-listar">
			<table>
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Iva</th>
						<th>Total</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody id="resultados">
				</tbody>
			</table>
		</div>
	</div>
	<div id="lista_loader">
		<div class="loader2">Cargando...</div>
	</div>
	<div id="paginacion"></div>
</div>
