<?php
require "libs/conexion.php";
$registro = $_GET['registro'];

$bus = $db
    ->where('Id', $registro)
    ->objectBuilder()->get('registros');

$res = $bus[0];

if ($res->foto_ct != '' || $res->capacitacion == 'ALTURAS' || $res->capacitacion == 'ESPACIOS CONFINADOS' || $res->capacitacion == 'ALTURAS RES. 4272') {
    $nombre = $res->nombre_primero . ' ' . $res->nombre_segundo . ' ' . $res->apellidos;
    $tipoid = $res->tipo_ident;
    $documento  = $tipoid . '. ' . $res->numero_ident;
    $expedicion = $res->fecha_inicio;
    $expedicion = date_create($expedicion);
    $expedicion = date_format($expedicion, 'd-m-Y');
    $vigencia   = '';
    if ($res->fecha_vigencia != '0000-00-00') {
        $vigencia   = $res->fecha_vigencia;
        $vigencia   = date_create($vigencia);
        $vigencia   = date_format($vigencia, 'd-m-Y');
    }

    $formacion = '';

    $cursos = $db
        ->where('Id_ct', $res->certificado)
        ->objectBuilder()->get('certificaciones');

    if ($db->count > 0) {
        $rsc       = $cursos[0];
        $formacion = $rsc->nombre;
    }

    require_once 'libs/tcpdf.php';
    require_once 'libs/fpdi/fpdi.php';

    $exa = new FPDI();

    if ($res->cargafoto == 1 && $res->foto_ct != '') {
        if ($vigencia  != '') {
            $exa->setSourceFile('libs/pl_carnet_alt5-c.pdf');
        } else {
            $exa->setSourceFile('libs/pl_carnet_alt5-d.pdf');
        }
    } else {
        if ($vigencia  != '') {
            $exa->setSourceFile('libs/pl_carnet_alt5-a.pdf');
        } else {
            $exa->setSourceFile('libs/pl_carnet_alt5-b.pdf');
        }
    }

    $tplIdx = $exa->importPage(1, '/MediaBox');
    $exa->SetPrintHeader(false);

    $exa->addFont('conthrax', '', 'conthrax.php');
    $exa->addFont('ubuntucondensed', '', 'ubuntucondensed.php');
    $exa->SetFont('conthrax', '', 8);
    // $poppinsblack = TCPDF_FONTS::addTTFfont('libs/fonts/PoppinsBlack.ttf', 'TrueTypeUnicode', '', 96);
    // $exa->SetFont('arial', '', 6);
    $estilo = '<style>
                .nm{
                    color: #000;
                    font-size: 5.5;
                    font-family: arial;
                }
                .bl{
                    color: #000;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    font-family: arial;
                    font-size: 5;
                }
                .rojo{
                    color: #d40e22;
                }
            </style>';

    $exa->AddPage();
    $exa->useTemplate($tplIdx);
    $exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $exa->setJPEGQuality(100);

    /// girar el contenido
    $exa->SetXY(50, 40);
    // $exa->StartTransform();
    // $exa->Rotate(90);
    /////

    /*  $txt = '<table border="0" width="200px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">MIN.TRABAJO 08SE2019220000000045994</strong></td></tr></table>';

    $exa->SetXY(34, 26);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="200px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">ICONTEC NÂ° CS-CER709434</strong></td></tr></table>';

    $exa->SetXY(34, 29);
    $exa->WriteHTML($estilo . $txt);
    $txt = '<table border="0" width="200px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">MIN.EDUCACION  1795</strong></td></tr></table>';

    $exa->SetXY(34, 32);
    $exa->WriteHTML($estilo . $txt); */

    $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="nm">' . $nombre . '</strong></td></tr></table>';

    $exa->SetXY(46.7, 32.9);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2">' . $documento . '</strong></td></tr></table>';

    $exa->SetXY(36.5, 35.3);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="150px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl rojo">' . $formacion . '</strong></td></tr></table>';

    $exa->SetXY(36.2, 42);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $expedicion . '</strong></td></tr></table>';

    $exa->SetXY(51, 49.2);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $vigencia . '</strong></td></tr></table>';

    $exa->SetXY(80, 49.2);
    $exa->WriteHTML($estilo . $txt);

    /*  $txt = '<table border="0" width="250px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl2">Lic. seguridad y salud en el trabajo Res. 3753 de 2013</strong></td></tr></table>';

    $exa->SetXY(54, 58);
    $exa->WriteHTML($estilo . $txt); */



    if ($res->foto_ct != '') {
        $exa->Image(substr(str_replace(' ', '', $res->foto_ct), 3), 93, 19.5, 20);

        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(222, 48, 53));

        $exa->Line(93, 19.5, 113, 19.5, $style);
        $exa->Line(93, 19.25, 93, 46.75, $style);

        $exa->Line(93, 46.5, 113, 46.5, $style);
        $exa->Line(113, 19.25, 113, 46.75, $style);
    }

    $exa->StopTransform();

    $tplIdx = $exa->importPage(2, '/MediaBox');
    $exa->AddPage();
    $exa->useTemplate($tplIdx);

    $exa->Output('carnet.pdf', 'I');
} else {
    echo 'Error al generar el carnet: Falta foto en el registro.';
}
