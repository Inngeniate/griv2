<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

$ls_sectores = '';

if (isset($_GET['cliente']) && $_GET['cliente'] != '') {
	require("libs/conexion.php");

	$clientes = $db
		->where('Id', $_GET['cliente'])
		->objectBuilder()->get('clientes');

	if ($db->count > 0) {
		$rsc = $clientes[0];
		$razon = $rsc->razon;
		$representante = $rsc->representante;
		$nit = $rsc->nit;
		$arl = $rsc->arl;
		$sector = $rsc->sector;

		$sectores = [
			'AGROPECUARIO',
			'COMERCIO',
			'COMERCIO Y SERVICIOS',
			'COMUNICACIONES',
			'CONSTRUCCIÓN',
			'EDUACION',
			'FINANCIERO',
			'HIDROCARBUROS',
			'INDUSTRIAL',
			'MINERO Y ENERGETICO',
			'TELECOMUNICACIONES',
			'TRANSPORTE'
		];

		foreach ($sectores as $item => $value) {
			$ls_sectores .= "<option value='$value' " . ($value == $sector ? 'selected' : '') . ">$value</option>";
		}
	} else {
		header('Location: clientes');
	}
} else {
	header('Location: clientes');
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Clientes | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Editar Cliente</h2>
				<form id="editar-cliente">
					<div class="Registro">
						<div class="Registro-der">
							<label>Razon Social *</label>
							<input type="text" placeholder="Razón Social" name="cliente[razon]" value="<?php echo $razon ?>" required>
							<label>NIT *</label>
							<input type="text" placeholder="NIT" name="cliente[nit]" value="<?php echo $nit ?>" required>
							<label>Sector Económico</label>
							<select name="cliente[sector]">
								<option value="">Seleccione</option>
								<?php echo $ls_sectores; ?>
							</select>
						</div>
						<div class="Registro-izq">
							<label>Representante Legal *</label>
							<input type="text" placeholder="Representante Legal" name="cliente[representante]" value="<?php echo $representante ?>" required>
							<label>ARL *</label>
							<input type="text" placeholder="ARL" name="cliente[arl]" value="<?php echo $arl ?>" required>
						</div>
						<br>
						<br>
						<input type="hidden" name="cliente[idcliente]" value="<?php echo $_GET['cliente'] ?>">
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/clientes_edt.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
