<?php
require "libs/conexion.php";
$registro = $_GET['registro'];

$bus = $db
    ->where('Id', $registro)
    ->objectBuilder()->get('registros');

$res = $bus[0];

$nombre = $res->nombre_primero . ' ' . $res->nombre_segundo . ' ' . $res->apellidos;
$tipoid = $res->tipo_ident;
$documento = $tipoid . '. ' . $res->numero_ident;
$expedicion = $res->fecha_inicio;
$expedicion = date_create($expedicion);
$expedicion = date_format($expedicion, 'd-m-Y');
$vencimiento = '';

if ($res->vencimiento_ct != '0000-00-00' || $res->vencimiento_ct != '') {
    $vencimiento = $res->vencimiento_ct;
    $vencimiento = date_create($vencimiento);
    $vencimiento = date_format($vencimiento, 'd-m-Y');
}

$rh          = $res->rh;
$licencia    = $res->licencia;

$formacion = '';

$cursos = $db
    ->where('Id_ct', $res->certificado)
    ->objectBuilder()->get('certificaciones');

if ($db->count > 0) {
    $rsc       = $cursos[0];
    $formacion = $rsc->nombre;
}

$horas = $res->horas;

require_once 'libs/tcpdf.php';
require_once 'libs/fpdi/fpdi.php';

$exa = new FPDI();

if ($res->cargafoto == 1 && $res->foto_ct != '') {
    if ($vencimiento != '') {
        $exa->setSourceFile('libs/pl_carnet_a.pdf');
    } else {
        $exa->setSourceFile('libs/pl_carnet_b.pdf');
    }
} else {
    if ($vencimiento != '') {
        $exa->setSourceFile('libs/pl_carnet_c.pdf');
    } else {
        $exa->setSourceFile('libs/pl_carnet_d.pdf');
    }
}



$tplIdx = $exa->importPage(1, '/MediaBox');
$exa->SetPrintHeader(false);

$exa->addFont('conthrax', '', 'conthrax.php');
$exa->addFont('ubuntucondensed', '', 'ubuntucondensed.php');

$estilo = '<style>
                .nm{
                    color: #000;
                    font-size: 5.5;
                    font-family: arial;
                }
                .bl{
                    color: #000;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    color: #000;
                    font-family: arial;
                    font-size: 5;
                }
                .b{
                    font-weight: bold
                }
                .rojo{
                    color: #d40e22;
                }
            </style>';

$exa->SetMargins(0, 0, 0);
$exa->SetAutoPageBreak(true, 0);

$exa->AddPage();
$exa->useTemplate($tplIdx, null, null, 0, 0, true);
$exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
$exa->setJPEGQuality(100);

$exa->SetXY(50, 40);


$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="nm">' . $nombre . '</strong></td></tr></table>';

$exa->SetXY(17.5, 25.8);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $documento . '</strong></td></tr></table>';

$exa->SetXY(7.5, 28);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $rh . '</strong></td></tr></table>';

$exa->SetXY(29, 28);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $licencia . '</strong></td></tr></table>';

$exa->SetXY(50.5, 28);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="150px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl rojo">' . $formacion . '</strong></td></tr></table>';

$exa->SetXY(7.5, 35);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $expedicion . '</strong></td></tr></table>';

$exa->SetXY(18, 41.95);
$exa->WriteHTML($estilo . $txt);

if ($vencimiento != '') {
    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $vencimiento . '</strong></td></tr></table>';

    $exa->SetXY(40, 41.95);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $horas . ' Hrs</strong></td></tr></table>';

    $exa->SetXY(58.5, 41.95);
    $exa->WriteHTML($estilo . $txt);
} else {
    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $horas . ' Hrs</strong></td></tr></table>';

    $exa->SetXY(37, 41.95);
    $exa->WriteHTML($estilo . $txt);
}

if ($res->cargafoto == 1 && $res->foto_ct != '') {
    $exa->Image(substr(str_replace(' ', '', $res->foto_ct), 3), 62, 12, 20);

    $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(222, 48, 53));

    $exa->Line(61.75, 12, 82.25, 12, $style);
    $exa->Line(62, 12, 62, 39, $style);

    $exa->Line(61.75, 39, 82.25, 39, $style);
    $exa->Line(82, 12, 82, 39, $style);
}

$exa->StopTransform();

$tplIdx = $exa->importPage(2, '/MediaBox');
$exa->AddPage();
$exa->useTemplate($tplIdx);

$exa->Output('carnet.pdf', 'I');
// }
