<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<!-- <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" > -->
		<meta name="google-site-verification" content="oc9E4yBVx2u8uUHzYyL0_dqaWXeQipnb0i2jfsMEHaA" />
		<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
		<title>Gricompany Gestión del Riesgo Integral</title>
		<!-- <link rel="stylesheet" href="css/slider.css" /> -->
		<link rel="stylesheet" href="css/landing.css" />
		<link rel="stylesheet" href="css/register.css" />
		<link rel="stylesheet" type="text/css" href="css/msj.css" />
		<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
		<?php include_once("header-new-top2.php") ?>

		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Nav-admin">
					<section class="nav_tabs">
						<div class="nav_tabs_back">
							<nav>
								<ul class="tabs">
									<li class="activa"><a href="#" class="">1</a></li>
									<li class=""><a href="#" class="">2</a></li>
									<li class=""><a href="#" class="">3</a></li>
									<li class=""><a href="#" class="">4</a></li>
									<li class=""><a href="#" class="">4</a></li>
									<li class=""><a href="#" class="">5</a></li>
									<li class=""><a href="#" class="">6</a></li>
									<li class=""><a href="#" class="">7</a></li>
									<li class=""><a href="#" class="">8</a></li>
								</ul>
							</nav>
						</div>
					</section>
				</div>
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.1 Información de Programas. Registré en esta sección Programa, Jornada y el Tipo de Inscripción que va a realizar.</p>
					<div class="Registro-asp-int-sec">
						<form>
							<label>Periodo*</label>
							<select>
								<option>2019-1</option>
							</select>
							<label>Programa*</label>
							<select>
								<option>Seleccione</option>
								<option>Nombre del programa</option>
							</select>
							<label>Horario</label>
							<select>
								<option>Seleccione</option>
								<option>Diurno</option>
								<option>Nocturno</option>
							</select>
							<label>Tipo de solicitud</label>
							<select>
								<option>Seleccione</option>
								<option>Doble programa</option>
								<option>Nuevo</option>
								<option>Transferencia externa</option>
								<option>Transferencia interna</option>
							</select>
							<input type="submit" value="Continuar" name="">
						</form>
					</div>
				</div>
				<!-- Paso 2 -->
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.2 Información de Personal. Registré en esta sección los datos personales del aspirante.</p>
					<div class="Registro-asp-int-sec">
						<form>
							<label>Tipo de identificación</label>
							<select disabled>
								<option>Tipo*</option>
							</select>
							<label>Identificación*</label>
							<input type="text" value="1234" name="" disabled="">
							<label>Lugar de expedición*</label>
							<select>
								<option>Seleccione</option>
							</select>
							<label>Fecha de expedición*</label>
							<input type="text" placeholder="fecha" name="">
							<label>Nacionalidad*</label>
							<input type="text" placeholder="Ingrese nacionalidad" name="">
							<label>Nombres*</label>
							<input type="text" placeholder="Nombres" name="">
							<label>Apellidos*</label>
							<input type="text" placeholder="Apellidos" name="">
						</div>
						<div class="Registro-asp-int-sec">
							<label>Fecha de nacimiento*</label>
							<input type="text" placeholder="Fecha" name="">
							<label>Lugar de nacimiento*</label>
							<select>
								<option>Seleccionar</option>
							</select>
							<label>Genero*</label>
							<select>
								<option>Selecciona</option>
								<option>Femenino</option>
								<option>Masculino</option>
							</select>
							<label>Estado civil*</label>
							<select>
								<option>Seleccionar</option>
								<option>Soltero</option>
								<option>Casado</option>
								<option>Unión libre </option>
								<option>Viudo</option>
								<option>Separado</option>
							</select>
							<label>Condición especial</label>
							<select>
								<option>Selecciona</option>
								<option>Discapacidad sensorial - Sordera profunda</option>
								<option>Discapacidad sensorial - Hipoaucusia</option>
								<option>Discapacidad sensorial - Ceguera</option>
								<option>Discapacidad sensorial - Baja Visión</option>
								<option>Discapacidad sensorial - Sordoceguera</option>
								<option>Discapacidad Intelectual</option>
								<option>Discapacidad Psicosocial</option>
								<option>Discapacidad Multiple</option>
								<option>Discapacidad Física o motora</option>
							</select>
							<label>Grupo Étnico</label>
							<select>
								<option>Selecciona</option>
								<option>Afro-descenndiente</option>
								<option>Indígena</option>
								<option>Racial o palanquero</option>
								<option>ROM o Gitano</option>
								<option>Victima del conflicto armado</option>
								<option>Madre cabeza de familia</option>
							</select>
						</div>
						<div class="Con-siguiente">
							<input type="submit" class="Siguiente" value="Continuar" name="">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/aspirantes.js"></script>
	</body>
	<footer>
		<div class="Footer">
			<div class="Footer-int">
				<p>Copyright © Gricompany S.A.S, 2018. Todos los derechos reservados diseñado por <a href="https://www.inngeniate.com/">Inngeniate.com</a></p>
			</div>
		</div>
	</footer>
</html>