<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
else {
	require("libs/conexion.php");

	$lista_certificado = '';
	$lista_inspectores = '';

	for ($i = 1; $i < 101; $i++) {
		if (strlen((string)$i) < 2)
			$i = '0' . $i;
		$lista_certificado .= '<option>' . $i . '</option>';
	}

	$maximo = array();

	$t = $db
		->objectBuilder()->get('certificado_ensayo', null, 'MAX(reporte_en) AS id');

	$rt = $t[0];
	$maximo[] = $rt->id;

	$t = $db
		->objectBuilder()->get('certificado_reporte', null, 'MAX(reporte) AS id');

	$rt = $t[0];
	$maximo[] = $rt->id;

	$t = $db
		->objectBuilder()->get('certificado_seguridad', null, 'MAX(reporte_sg) AS id');

	$rt = $t[0];
	$maximo[] = $rt->id;

	$t = explode('-', max($maximo));
	$t = $t[1];
	$nensayo =  'GRICOM-' . sprintf('%06d', (int)$t + 1);

	// $nensayo = mysql_query("SELECT MAX(Id_en) as nensayo FROM certificado_ensayo");
	// $res= mysql_fetch_object($nensayo);
	// $nensayo = 'GRICOM-'.sprintf('%06d', $res->nensayo + 1);

	// $nreporte = mysql_query("SELECT MAX(Id_re) as nreporte FROM certificado_reporte");
	// $res= mysql_fetch_object($nreporte);
	// $nreporte = 'GRICOM-'.sprintf('%06d', $res->nreporte + 1);

	$naforo = $db
		->objectBuilder()->get('certificado_aforo', null, 'MAX(Id_reaf) as naforo');

	$res = $naforo[0];
	$naforo = 'GRI TRAC-' . sprintf('%05d', $res->naforo + 1);

	$inspectores = $db
		->objectBuilder()->get('inspectores');

	foreach ($inspectores as $resin) {
		$lista_inspectores .= '<option value="' . $resin->Id_ins . '">' . $resin->nombre_ins . '</option>';
	}

	$lista_vendedores = '';

	$vendedores = $db
		->orderBy('nombre_v', 'ASC')
		->objectBuilder()->get('vendedores');

	if ($db->count > 0) {
		foreach ($vendedores as $rsv) {
			$lista_vendedores .= '<option value="' . $rsv->Id_v . '">' . $rsv->nombre_v . '</option>';
		}
	}
}

$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 5)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
		}

		.img-prev {
			width: 450px;
			height: 100px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.img-prev2 {
			width: 320px;
			height: 220px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.img-prev-sg {
			width: 330px;
			height: 210px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		.ui-autocomplete {
			max-height: 200px;
			overflow-y: auto;
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
			/* add padding to account for vertical scrollbar */
			z-index: 1000 !important;
		}

		table input[type="radio"] {
			height: auto !important;
			width: auto !important;
		}

		table td {
			text-align: left;
		}

		table th {
			background: #004d80;
			font-family: Open-sans-b;
			font-size: 13px;
			text-align: center;
			color: #fff;
			padding: 0.5em 0.3em 0.5em 0.3em;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="ensayo" class="activo">Certificados</a></li>
						<li><a href="javascript://" id="seguridad">Seguridad</a></li>
						<li><a href="javascript://" id="reporte">Reportes</a></li>
						<li><a href="javascript://" id="aforo">Aforo</a></li>
					</ul>
				</div>
			</div>
			<?php include('certificado_ensayo.php') ?>
			<?php include('certificado_reporte.php') ?>
			<?php include('certificado_aforo.php') ?>
			<?php include('certificado_seguridad.php') ?>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/certifica2.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
