<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
    <meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
    <meta name="robots" content="All">
    <meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
    <title>Contáctanos | Gricompany Gestión del Riesgo Integral</title>
<link rel="stylesheet" href="css/stylesheet.css" />
<link rel="stylesheet" href="css/style-menu.css" />
<link rel="stylesheet" href="css/msj.css" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

function mapa(pos)
{
    var contenedor =document.getElementById("mapa2");
    var latitud = '4.114961';
    var Longitud = '-73.611842';
    var centro = new google.maps.LatLng(latitud,Longitud);
    var propiedades =   {

    center: centro,
    draggable:true,
    KeyBoardShortcuts:true,
    mapTypeControl:true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    navigationControl: true,
    scrollWheel:false,
    streetViewControl:false,
    zoom:16,

    };

    var map = new google.maps.Map(contenedor,propiedades);
    var mkr = new google.maps.Marker({
        draggable:false,
        icon: 'images/gps_pos.png',
        position: centro,
        map: map,
        title: 'Gricompany',
    });

}

function error(errorC)
{

    if(errorC.code ==0 )
        alert("Error desconocido");
    else if (errorC.code ==1 )
        alert("No me dejaste ubicar");
    else if (errorC.code ==2 )
        alert("Posicion no disponible");
    else if (errorC.code ==3 )
        alert("me rendi :(");

}
</script>
</head>
<body onload="mapa()">
<?php include_once("analyticstracking.php") ?>
<div class="Contenedor">
	<header>
	<?php include("menu.php"); ?>
	</header>
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="js/script-menu.js"></script>
    <script type="text/javascript" src="js/contacto.js"></script>
	<div class="Mapa">
		<div id="mapa2"></div>
	</div>
	<div class="Contacto">
	<h2>Contáctanos</h2>
	<p>Si hay inquietudes, con mucho gusto las responderemos en un corto plazo.</p>
		<div class="Contacto-interno">
		<form id="contacto">
			<div class="Contacto-interno-izq">
				<label>Nombre:</label>
				<input type="text" placeholder="Nombre" name="nombre_c" required>
			</div>
			<div class="Contacto-interno-der">
				<label>Email:</label>
				<input type="email" placeholder="Email" name="email_c" required>
			</div>
			<label>Mensaje:</label>
			<textarea placeholder="Mensaje" name="mensaje_c" required></textarea>
			<input type="submit" value="Enviar">
		</form>
		</div>
	</div>
</div>
<!-- //<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->

<?php include("redes.php"); ?>
<footer>
    <?php include("footer.php"); ?>
</footer>
</body>
</html>