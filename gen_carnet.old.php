<?php
	require("libs/conexion.php");
	$registro = $_GET['registro'];

	$bus = mysql_query("SELECT * FROM registros WHERE Id= '$registro'");
	$res = mysql_fetch_object($bus);

	if($res->foto_ct != ''){
		$nombre = $res->nombres.' '.$res->apellidos;
		$documento = $res->numero_ident;
		$expedicion = $res->fecha_inicio;
		$expedicion = date_create($expedicion);
		$expedicion = date_format($expedicion,'d-m-Y');
		$vencimiento = '';

		if($res->vencimiento_ct != '0000-00-00'){
			$vencimiento = $res->vencimiento_ct;
			$vencimiento = date_create($vencimiento);
			$vencimiento = date_format($vencimiento,'d-m-Y');
		}

		$rh = $res->rh;
		$licencia = $res->licencia;
		$formacion = $res->certificado;

		$horas = $res->horas;

		require_once('libs/tcpdf.php');
		require_once('libs/fpdi/fpdi.php');

		$pdf = new FPDI();
		$pdf->setSourceFile('libs/pl_carnet.pdf');
		$tplIdx = $pdf->importPage(1, '/MediaBox');
		$pdf->SetPrintHeader(false);
		$pdf->AddPage();
		$pdf->useTemplate($tplIdx);

		$utf8text = 'prueba';

		$pdf->SetFont('calibrib', '', 8);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(31.5, 24.5);
		$pdf->Write(0, $nombre);
		$pdf->SetFont('calibri', '', 10);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(53, 29);
		$pdf->Write(0, $documento);
		$pdf->SetXY(53, 33);
		$pdf->Write(0, $expedicion);
		$pdf->SetXY(53, 37);
		$pdf->Write(0, $vencimiento);
		$pdf->SetXY(53, 40.5);
		$pdf->Write(0, $rh);
		$pdf->SetXY(53, 44.5);
		$pdf->Write(0, $licencia);
		$pdf->SetXY(77, 44.5);
		$pdf->Write(0, $horas.' Hrs');

		$n_formacion = '';

		$l_formacion  = explode(' ', $formacion);
		$t_formacion = count($l_formacion);
		if($t_formacion > 10){
			for ($i=0; $i < 10; $i++) {
				$n_formacion .= $l_formacion[$i].' ';
			}

			$pdf->SetXY(46, 57);
			$pdf->SetFont('calibrib', '', 5.3);
			$pdf->SetTextColor(255, 255, 255);
			$pdf->Write(0, $n_formacion);

			$n_formacion2 = '';
			for ($i=10; $i < $t_formacion; $i++) {
				$n_formacion2 .= $l_formacion[$i].' ';
			}
			$pdf->SetXY(46, 59);
			$pdf->SetFont('calibrib', '', 5.3);
			$pdf->SetTextColor(255, 255, 255);
			$pdf->Write(0, $n_formacion2);

		}else{
			$n_formacion = $formacion;
			$pdf->SetXY(46, 58);
			$pdf->SetFont('calibrib', '', 6);
			$pdf->SetTextColor(255, 255, 255);
			$pdf->Write(0, $n_formacion);
		}


		$pdf->Image(substr($res->foto_ct,3),89.5,21,25);

		$pdf->Output('Carnet-'.$res->Id.'_'.$documento.'.pdf');
	}else{
		echo 'Error al generar el carnet: Falta foto en el registro.';
	}


?>
