<div class="Contenido-admin-izq listado-usuarios">
	<h2>Listar Usuarios</h2>
	<hr>
	<p>En esta sección podrás listar y buscar usuarios.</p>
	<br>
	<form id="buscar">
		<label>Nombre: </label>
		<input type="text" name="usuario[nom]" class="nom" placeholder="Nombre">
		<input type="submit" value="Buscar">
	</form>
	<br>
	<div class="Listar-personas">
		<div class="Tabla-listar">
			<table>
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Correo</th>
						<th>Estado</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody id="resultados">
				</tbody>
			</table>
		</div>
	</div>
	<div id="lista_loader">
		<div class="loader2">Cargando...</div>
	</div>
	<div id="paginacion_usuarios"></div>
</div>
