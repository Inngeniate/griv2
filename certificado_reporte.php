<div class="reporte"  style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Crear Reporte</h2>
		<form class="cre_certificado">
			<div class="Registro">
				<div class="Registro-der">
					<label>Reporte Nº *</label>
					<input type="text" placeholder="Reporte Nº" name="certifica[reporte]" value="<?php echo $nensayo ?>" class="ncertificado" readonly>
					<label>Calibración Nº*</label>
					<input type="text" placeholder="Calibración Nº" name="certifica[calibracion]" required>
					<label>Fecha Calibración *</label>
					<input type="date" placeholder="Fecha Calibración" name="certifica[fechacalibra]" required>
					<label>Serie *</label>
					<input type="text" placeholder="Serie" name="certifica[serie]" required>
					<label>Ciudad *</label>
					<input type="text" placeholder="Ciudad" name="certifica[ciudad]" class="auto_ciu" required>
					<label>Fecha *</label>
					<input type="date" placeholder="Fecha" name="certifica[fecha]" required>
					<label>Placa *</label>
					<input type="text" placeholder="Placa" name="certifica[placa]" required>
					<label>Propietario *</label>
					<input type="text" placeholder="Propietario" name="certifica[propietario]" required>
					<label>Capacidad Total *</label>
					<input type="text" placeholder="Capacidad Total" name="certifica[capacidadtotal]" class="cpt" required>
					<label>Capacidad BLS *</label>
					<input type="text" placeholder="Capacidad BLS" name="certifica[capacidadbls]" class="bls" required>
					<label>Cantidad de rompe olas *</label>
					<input type="text" placeholder="Cantidad de rompe olas" name="certifica[rompeolas]" required>
					<label>Compartimientos *</label>
					<input type="text" placeholder="Compartimientos" name="certifica[compartimientos]" required>
				</div>
				<div class="Registro-der">
					<label>Nº ejes *</label>
					<input type="text" placeholder="Nº ejes" name="certifica[ejes]" required>
					<label>Material de tanque *</label>
					<input type="text" placeholder="Material de tanque" name="certifica[materialtanque]" required>
					<label>Marca *</label>
					<input type="text" placeholder="Marca" name="certifica[marca]" required>
					<label>Valvulas de descarque *</label>
					<input type="text" placeholder="Valvulas de descarque" name="certifica[valvulas]" required>
					<label>Capacidad compartimiento 1 *</label>
					<input type="text" placeholder="Capacidad compartimiento 1"  class="cpt" name="certifica[compartimiento1]">
					<label>Capacidad Bls *</label>
					<input type="text" placeholder="Capacidad Bls"  class="bls" name="certifica[comp1capacidad]" >
					<label>Capacidad compartimiento 2 *</label>
					<input type="text" placeholder="Capacidad compartimiento 2" class="cpt" name="certifica[compartimiento2]">
					<label>Capacidad Bls *</label>
					<input type="text" placeholder="Capacidad Bls" class="bls" name="certifica[comp2capacidad]" >
					<label>Capacidad compartimiento 3 *</label>
					<input type="text" placeholder="Capacidad compartimiento 2"  class="cpt" name="certifica[compartimiento3]">
					<label>Capacidad Bls *</label>
					<input type="text" placeholder="Capacidad Bls" class="bls" name="certifica[comp3capacidad]" >
					<label>Valido hasta *</label>
					<input type="date" placeholder="Valido hasta" name="certifica[validez]" >
					<label>Inspector *</label>
					<select name="certifica[inspector]" required>
						<option>Selecciona</option>
						<?php echo $lista_inspectores ?>
					</select>
				</div>
				<div class="Registro-cent">
					<label>Restricciones *</label>
					<textarea placeholder="Restricciones" name="certifica[restricciones]" required></textarea>
				</div>
				<br>
				<br>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev2">
						<button type="button" class="placa" id="foto1">Placa Cabezote</button>
					</div>
					<div class="img-placa">
						<img class="img-prev2">
						<button type="button" class="placa" id="foto2">Parte Trasera Tanque</button>
					</div>
				</div>
				<div class="Registro-der">
					<div class="img-placa">
						<img class="img-prev2">
						<button type="button" class="placa" id="foto3">Parte Lateral Tanque</button>
					</div>
				</div>
				<input type="hidden" name="certifica[accion]" value="reporte">
				<input type="submit" value="Guardar Reporte">
			</div>
		</form>
	</div>
</div>