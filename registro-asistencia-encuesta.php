<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
require("libs/conexion.php");

$fecha = date('d-m-Y');
$ls_head = '';
$temp_head = '';
$ls_body = '';
$temp_body = '';
$temp_body2 = '';
$empleado = '';

$ls_head = '<tr>
				<th>Preguntas</th>';

$temp_head = '<tr>
					<th colspan="2"></th>';

if (!isset($_REQUEST['as']) ||  $_REQUEST['as'] == '') {
	header('Location: covid');
}

$iasistencia = $_REQUEST['as'];

$asistencias = $db
	->where('Id_as', $_REQUEST['as'])
	->groupBy('DATE(fecha_as)')
	->objectBuilder()->get('asistencias');

if ($db->count > 0) {
	$rsis = $asistencias[0];

	$empleados = $db
		->where('Id_em', $rsis->Id_em)
		->objectBuilder()->get('empleados');

	if ($db->count > 0) {
		$rem = $empleados[0];
		$empleado = $rem->nombre_em;
	}
} else {
	header('Location: covid');
}

$asistencias = $db
	->where('sesion_as', $rsis->sesion_as)
	->groupBy('DATE(fecha_as)')
	->objectBuilder()->get('asistencias');

if ($db->count > 0) {
	foreach ($asistencias as $ras) {
		$fecha = explode(' ',  $ras->fecha_as);
		$ls_head .= '<th>' . $fecha[0] . '</th>';
		$temp_head .= '<th>' . $fecha[0] . '</th>';
	}
}

$preguntas = $db
	->objectBuilder()->get('preguntas_covid');

foreach ($preguntas as $rpr) {
	$ls_body .= '<tr>
						<td><p>' . $rpr->pregunta_pc . '</p></td>';

	$asistencias = $db
		->where('sesion_as', $rsis->sesion_as)
		->groupBy('DATE(fecha_as)')
		->objectBuilder()->get('asistencias');

	if ($db->count > 0) {
		foreach ($asistencias as $ras) {
			$fecha = explode(' ',  $ras->fecha_as);

			$encuestas = $db
				->where('sesion_as', $ras->sesion_as)
				->where('fecha_ae', $fecha[0])
				->where('pregunta_ae', $rpr->Id_pc)
				->objectBuilder()->get('asistencias_encuesta');

			if ($db->count > 0) {
				foreach ($encuestas as $ren) {
					$ls_body .= '<td>
										<select name="asistencia[pregunta][]" data-puntaje="' . $rpr->puntaje_pc . '" >
											<option value="">Seleccione</option>
											<option value="Si" ' . ($ren->respuesta_ae == "Si" ? "selected" : "") . '>Si</option>
											<option value="No" ' . ($ren->respuesta_ae == "No" ? "selected" : "") . '>No</option>
										</select>
									</td>';
				}
			}
		}
		$ls_body .= '</tr>';
	}
}

$ls_head .= '</tr>';

$encuestas = $db
	->where('sesion_as', $rsis->sesion_as)
	->groupBy('DATE(fecha_as)')
	->objectBuilder()->get('asistencias');

if ($db->count > 0) {
	$temp_body = '<tr>
						<td rowspan="2">Temperatura</td>
						<td>Entrada</td>';
	$temp_body2 = '<tr>
						<td>Salida</td>';

	foreach ($encuestas as $ren) {
		$fecha = explode(' ',  $ren->fecha_as);

		$detalle = $db
			->where('fecha_as', $fecha[0] .  '%', 'LIKE')
			->where('sesion_as', $ren->sesion_as)
			->objectBuilder()->get('asistencias');

		$entrada = '';
		$salida = '';
		$hora_entrada = '';
		$hora_salida = '';

		foreach ($detalle as $rdt) {
			$fecha = explode(' ',  $rdt->fecha_as);

			if ($rdt->tipo_as == 'E') {
				$entrada = $rdt->temperatura_as;
				$hora_entrada = $fecha[1];
			} else {
				$salida = $rdt->temperatura_as;
				$hora_salida = $fecha[1];
			}
		}

		$temp_body .= '<td><input type="number" min="1" step="0.1" name="asistencia[temperatura-' . $fecha[0] . '][E]" placeholder="Temperatura" value="' . $entrada . '" ><input type="time" name="asistencia[ingreso-' . $fecha[0] . '][E]" placeholder="Hora Entrada" value="' . $hora_entrada . '" ></td>';
		$temp_body2 .= '<td><input type="number" min="1" step="0.1" name="asistencia[temperatura-' . $fecha[0] . '][S]" placeholder="Temperatura" value="' . $salida . '" ><input type="time" name="asistencia[salida-' . $fecha[0] . '][S]" placeholder="Hora Salida" value="' . $hora_salida . '" ></td>';
	}

	$temp_body .= '</tr>';
	$temp_body2 .= '</tr>';
}

$temp_head .= '</tr>';

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Asistencias | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Registro de encuesta asistencia - <?php echo $empleado; ?></h2>
				<form id="registro-encuesta">
					<div class="Registro">
						<div class="Tabla-listar">
							<table border="0" cellspacing="0" cellpadding="4">
								<thead>
									<?php echo $ls_head; ?>
								</thead>
								<tbody>
									<?php echo $ls_body; ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="Registro">
						<div class="Tabla-listar">
							<table border="0" cellspacing="0" cellpadding="4">
								<thead>
									<?php echo $temp_head; ?>
								</thead>
								<tbody>
									<?php echo $temp_body . $temp_body2; ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="Registro">
						<br>
						<br>
						<input type="hidden" name="asistencia[idas]" value="<?php echo $iasistencia; ?>">
						<input type="submit" value="Guardar Registro">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/registrar_encuesta.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
