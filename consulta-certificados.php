<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="google-site-verification" content="oc9E4yBVx2u8uUHzYyL0_dqaWXeQipnb0i2jfsMEHaA" />
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Gricompany Gestión del Riesgo Integral</title>
	<!-- <link rel="stylesheet" href="css/slider.css" /> -->
	<link rel="stylesheet" href="css/landing.css?v<?php echo date('YmdHis') ?>" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<style type="text/css" media="screen">
		.modal-title {
			background-color: #b42d32 !important;
		}

		table td {
			padding: 4px;
		}

		.modal-inner input[type=text],
		.modal-inner input[type=email] {
			background: #fff;
			border: 1px solid #656571;
		}

		.modal-inner input[type=email] {
			font-family: Roboto-Regular;
			padding: 5px 15px;
			height: 45px;
			width: 100%;
			outline: none;
			margin-bottom: 10px;
			-webkit-transition: 0.3s;
			-moz-transition: 0.3s;
			-o-transition: 0.3s;
			-ms-transition: 0.3s;
			transition: 0.3s;
		}

		.btn-none {
			display: none !important;
		}

		body {
			background: #323141;
			text-align: center;
		}

		.Seccion4-form label {
			color: #cacad3;
		}

		select,
		input[type=text] {
			color: #fff;
			background: #656571;
			font-family: Roboto-Regular;
			border: none;
			padding: 5px 15px;
			height: 45px;
			width: 100%;
			outline: none;
			margin-bottom: 10px;
			-webkit-transition: .3s;
			-moz-transition: .3s;
			-o-transition: .3s;
			-ms-transition: .3s;
			transition: .3s;
		}

		.Consulta-identificacion,
		.Consulta-placa,
		.Consulta-serial,
		.Consulta-ccaltura {
			display: none;
		}
	</style>
</head>

<body>
	<section>
		<div class="Seccion-certificados">
			<div class="Seccion4-int">
				<div class="Seccion4-int-sec">
					<h2><span class="Rojo">\\\</span>Certificados<span class="Rojo">\\\</span></h2>
					<div class="Seccion4-form">
						<p></p>
						<form id="v-certificados">
							<label>Ingresar el número de teléfono.</label>
							<input type="text" placeholder="Teléfono" name="consulta[telefono]" class="Consulta-telefono" required>
							<label for="tipo-certificado">Seleccione el tipo de consulta</label>
							<select name="consulta[tipo]" id="tipo-certificado" required>
								<option value="">Seleccione</option>
								<option value="1">Certificaciones</option>
								<option value="2">Inspecciones</option>
								<option value="3">Certificado de Seguridad</option>
								<option value="4">Certificado de alturas</option>
							</select>
							<div class="Consulta-identificacion">
								<label>Ingresar el número de identificación (Cedula).</label>
								<input type="text" placeholder="Número" name="consulta[identificacion]">
							</div>
							<div class="Consulta-placa">
								<label>Ingresar la placa del vehículo para consultar las inspecciones</label>
								<input type="text" placeholder="Número" name="consulta[placa]">
							</div>
							<div class="Consulta-serial">
								<label>Ingresar el serial de tu equipo para consultar el certificado de seguridad</label>
								<input type="text" placeholder="Serial" name="consulta[serial]">
							</div>
							<div class="Consulta-ccaltura">
								<label>Ingresar el número de identificación (Cedula) para consultar el certificado de alturas</label>
								<input type="text" placeholder="Número" name="consulta[ccaltura]" class="certalturas">
							</div>
							<input type="submit" class="Btn-rojo" value="Consultar" name="">
							<br><br>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/consulta-certificados.js?v<?php echo date('YmdHis') ?>" type="text/javascript" charset="utf-8"></script>
</body>

</html>
