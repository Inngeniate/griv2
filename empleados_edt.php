<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

if (isset($_GET['empleado']) && $_GET['empleado'] != '') {
	require("libs/conexion.php");

	$empleados = $db
		->where('Id_em', $_GET['empleado'])
		->objectBuilder()->get('empleados');

	if ($db->count > 0) {
		$rsc = $empleados[0];
		$nombre   = $rsc->nombre_em;
		$identifica = $rsc->identificacion_em;
		$cargo = $rsc->cargo_em;
		$correo   = $rsc->correo_em;
		$telefono = $rsc->telefono_em;

		$firma = '';
		$firma_ver = 'style="display:none;margin:30px;width: 300px"';
		$firma_btn = '';

		if ($rsc->firma_em != '') {
			$firma = 'src="' . $rsc->firma_em . '"';
			$firma_ver = 'style="display:block;margin:30px;width: 300px"';
			$firma_btn = '<a href="javascript://" class="Btn-gris-normal borrar-firma"><i class="icon-bin"> </i>Borrar Firma</a>';
		}
	} else {
		header('Location: empleados');
	}
} else {
	header('Location: empleados');
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Empleados | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Editar Empleado</h2>
				<form id="editar-empleado">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombre" name="empleado[nombre]" value="<?php echo $nombre ?>" required>
						</div>
						<div class="Registro-izq">
							<label>Identificación *</label>
							<input type="number" placeholder="Identificación" name="empleado[identifica]" value="<?php echo $identifica ?>" required>
						</div>
						<div class="Registro-izq">
							<label>Cargo *</label>
							<input type="text" placeholder="Cargo" name="empleado[cargo]" value="<?php echo $cargo ?>" required>
						</div>
						<div class="Registro-der">
							<label>Correo *</label>
							<input type="text" placeholder="Correo" name="empleado[correo]" value="<?php echo $correo ?>" required>
						</div>
						<div class="Registro-izq">
							<label>Teléfono *</label>
							<input type="text" placeholder="Teléfono" name="empleado[telefono]" value="<?php echo $telefono ?>" required>
						</div>
						<div class="Registro-izq">
							<label>Firma</label>
							<button type="button" class="placa" id="firma">Firma</button>
							<img id="canvasimg" <?php echo $firma . $firma_ver ?>>
							<?php echo $firma_btn; ?>
						</div>
						<br>
						<br>
						<input type="hidden" name="empleado[idempleado]" value="<?php echo $_GET['empleado'] ?>">
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/empleados_edt.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
