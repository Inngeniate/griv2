<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');


require("libs/conexion.php");

$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 6)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany Gestión de Riesgos Integrales</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/jquery.modal.theme-xenon.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<?php include_once("analyticstracking.php") ?>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin" style="width:1200px">
			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="ensayo" class="activo">Listar certificados</a></li>
						<li><a href="javascript://" id="seguridad">Listar C. seguridad</a></li>
						<li><a href="javascript://" id="reporte">Listar reportes</a></li>
						<li><a href="javascript://" id="aforo">Listar aforo</a></li>
						<!-- <li><a href="javascript://" id="inspeccion">Listar inspecciones</a></li> -->
					</ul>
				</div>
			</div>
			<?php include('lista_ensayo.php') ?>
			<?php include('lista_reporte.php') ?>
			<?php include('lista_aforo.php') ?>
			<?php include('lista_seguridad.php') ?>
		</div>
	</section>
	<script type="text/javascript" src="js/listado_certifica2.js?v<?php echo date('YmdHis') ?>"></script>
	<script src="js/jquery.modal.min.js"></script>
</body>

</html>
