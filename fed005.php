<?php

require_once 'libs/tcpdf.php';

class MYPDF extends TCPDF
{
	public function Header()
	{

		$estilo = '<style>
					table{
						font-size: 11px;
						font-family: arial;
					}
					.tmax{
						font-size: 12px;
					}
					.tmin2{
						font-size: 11px;
					}
				</style>';

		$bMargin = $this->getBreakMargin();
		$auto_page_break = $this->AutoPageBreak;
		$this->SetAutoPageBreak(false, 0);
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		$this->setPageMark();
		// $this->SetMargins(15, 50, 15, true);

		$head = '<table cellspacing="0" cellpadding="0" border="0" class="certificado1">
							<tr>
								<td height="20"><br><br><img src="images/logo.png" width="120px"><br></td>
							</tr>
							<tr>
								<td align="center" class="tmax"><b>FORMATO DE EVALUACION ACTIVIDADES PRACTICAS PARA TRABAJO EN ALTURAS</b></td>
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="99%">
							<tr>
								<td colspan="3"></td>
								<td class="tmax" align="right"><br><br><br>FED 005 <br></td>
							</tr>
						</table>';

		$this->writeHTML($estilo . $head, true, false, false, false, '');
	}

	public function Footer()
	{
		$this->SetY(-45);
		$estilo = '<style>
						table{
							color: #999999;
							font-size: 12px;
						}
						.link{
							color: #495de6;
						}
					</style>';

		switch ($this->page) {
			case 1:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td align="right">Versión 01</td>
							</tr>
							<tr>
								<td align="right">	1 de 3 | Página</td>
							</tr>
						</table>';

				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
			case 2:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td align="right">Versión 01</td>
							</tr>
							<tr>
								<td align="right">	2 de 3 | Página</td>
							</tr>
						</table>';
				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
			case 3:
				$foot = '<table  cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td align="right">Versión 01</td>
							</tr>
							<tr>
								<td align="right">	3 de 3 | Página</td>
							</tr>
						</table>';
				$this->writeHTML($estilo . $foot, true, false, false, false, '');
				break;
		}
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
$pdf->setPageOrientation('p');

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// PDF_MARGIN_TOP
$pdf->SetMargins(15, 50, 15);
// $pdf->SetPrintHeader(false);
// $pdf->setPrintFooter(false);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(true, 45);

// set image scale factor
// $pdf->setImageScale(1.53);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
	require_once dirname(__FILE__) . '/lang/eng.php';
	$pdf->setLanguageArray($l);
}

$pdf->AddPage();
$arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 10);

$estilo = '<style>
			table{
				font-size: 11px;
				font-family: arial;
			}
			.tmax{
				font-size: 12px;
			}
			.tmin2{
				font-size: 11px;
			}
		</style>';

// $html = '<table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="110%">
// 			<tr>
// 				<td height="20"><br><br><img src="images/logo.png" width="120px"><br></td>
// 			</tr>
// 			<tr>
// 				<td align="center" class="tmax"><b>FORMATO DE EVALUACION ACTIVIDADES PRACTICAS PARA TRABAJO EN ALTURAS</b></td>
// 			</tr>
// 		</table>
// 		<table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="105%">
// 			<tr>
// 				<td colspan="3"></td>
// 				<td class="tmax" align="right"><br><br><br>FED 005 <br></td>
// 			</tr>
// 		</table>';

$html = '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="100%">
			<tr>
				<td width="50"><b>FECHA</b></td>
				<td width="250"></td>
				<td width="103"><b>ESTUDIANTE</b></td>
				<td width="250"></td>
			</tr>
			<tr>
				<td><b>CURSO</b></td>
				<td></td>
				<td><b>IDENTIFICACION</b></td>
				<td></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="108%">
			<tr>
				<td height="20"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="110%">
			<tr>
				<td width="40" align="center"><b>No</b></td>
				<td width="300" align="center">Practica</td>
				<td width="65" align="center">Aprueba</td>
				<td width="65" align="center">Aun no</td>
				<td width="182.5" align="center">Observaciones</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="justify">El estudiante observa demostración del sistema de protección contra caídas pasivo red de seguridad, comprende su instalación.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">2</td>
				<td align="justify">El estudiante inspecciona y usa el arnés ajustado al cuerpo sin entorches de manera correcta.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">3</td>
				<td align="justify">El estudiante usa eslinga de posicionamiento, tiene buena manipulación de ganchos, se ubica en la estructura y realiza posicionamiento correcto.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">4</td>
				<td align="justify">El estudiante usa la eslinga de restricción de manera correcta.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">5</td>
				<td align="justify">El estudiante instala el TIE OFF, usa eslinga de protección contra caídas simple de manera correcta, teniendo en cuenta los factores de caídas.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">6</td>
				<td align="justify">El estudiante usa la eslinga en Y, y tanto en posicionamiento como protección contra caídas, en argolla dorsal y pectoral no permite que el gancho quede más debajo de la cintura, no hace progresión con gancho en la mano.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">7</td>
				<td align="justify">El estudiante instala línea de vida vertical portátil directamente y a distancia, reconoce el kit línea de vida vertical, sabe ubicar el arrestador de caída y lo prueba antes de realizar ascenso y descenso con línea de vida vertical portátil.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">8</td>
				<td align="justify">El estudiante instala y usa la línea de vida vertical portátil tipo retráctil inercial, comprende su uso maneja el llamador.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">9</td>
				<td align="justify">El estudiante usa correctamente la línea de vida vertical fija, sabe ubicar el arrestador de caída y lo prueba antes de subir.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">10</td>
				<td align="justify">El estudiante instala y usa correctamente la línea de vida horizontal portátil, comprende que este dispositivo requiere el apoyo de la persona calificada.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>';


$pdf->writeHTML($estilo . $html, true, false, false, false, '');
$pdf->AddPage();

$html = '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="110%">
			<tr>
				<td width="40" align="center">11</td>
				<td width="300" align="justify">El estudiante asiste a la demostración del andamio colgante, observa su manejo, verifica el sistema de protección contra caídas mediante líneas de vida vertical portátil.</td>
				<td width="65"></td>
				<td width="65"></td>
				<td width="182.5"></td>
			</tr>
			<tr>
				<td align="center">12</td>
				<td align="justify">El estudiante realiza trabajo bajo techo mediante ascenso a simulador plano inclinado usando línea de vida vertical portátil anclada a línea de vida horizontal portátil, comprende uso de las plataformas de seguridad y sabe de la diferencia entre un techo frágil y un techo fuerte.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">13</td>
				<td align="justify">El estudiante realiza práctica de uso de escalera portátil de extensión y de tijera, comprende su diferencia y uso, sabe hasta cuándo es posible el uso de la escalera y cuando esto debe cambiar por un andamio.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">14</td>
				<td align="justify">El estudiante del sector eléctrico y de telecomunicaciones realiza ascenso y descenso con pretal usando el sistema de protección contra caídas – tercer pretal.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">15</td>
				<td align="justify">El estudiante realiza acceso por cuerdas y descenso con tabla de contramaestre, reconoce el kit de descenso y sabe bloquear en la mitad para realizar el trabajo, usa la línea de vida vertical portátil o sistema retráctil inercial como respaldo de seguridad.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">16</td>
				<td align="justify">El estudiante realiza rescate técnico con apoyo del kit de rescate 4:1, lo arma y lo desarma, realiza la práctica por grupos con apoyo del entrenador. Comprende los conceptos de auto rescate, rescate asistido y rescate avanzado.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">17</td>
				<td align="justify">El estudiante reconoce mediante demostración los sistemas de prevención de caídas, delimitación de áreas, línea de advertencia, señalización, manejo de huecos y desniveles.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td align="center">18</td>
				<td align="justify">El estudiante reconoce los componentes y técnicas básicas de montaje y desmontaje de andamios. Los arma con plataforma y baranda completa incluyendo rodapié.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>';

$pdf->writeHTML($estilo . $html, true, false, false, false, '');

$pdf->AddPage();

$html = '<table cellspacing="0" cellpadding="4" border="1" class="certificado1" width="110%">
			<tr>
				<td width="40" align="center">19</td>
				<td width="300" align="justify">El estudiante realiza maniobras básicas de primeros auxilios para atender personas con politraumatismos por caídas, evaluación primaria, evaluación secundaria, inmovilización y transporte de lesionados.</td>
				<td width="65"></td>
				<td width="65"></td>
				<td width="182.5"></td>
			</tr>
			<tr>
				<td align="center">20</td>
				<td align="justify">El estudiante inspecciona y define técnicas de mantenimiento de equipos, diligencia hoja de vida de los equipos y permisos de trabajo en alturas.</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="0" border="0" width="108%">
			<tr>
				<td height="30"></td>
			</tr>
		</table>';

$html .= '<table cellspacing="0" cellpadding="5" border="1" width="99%">
			<tr>
				<td align="center">Firma entrenador</td>
				<td align="center">Firma estudiante</td>
				<td align="center">Huella digital</td>
			</tr>
			<tr>
				<td height="80"></td>
				<td></td>
				<td></td>
			</tr>
		</table>';

$pdf->writeHTML($estilo . $html, true, false, false, false, '');

$pdf->Output('FED005.pdf', 'I');
