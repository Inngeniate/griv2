<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
else {
	require("libs/conexion.php");
	$idre = $_GET['reporte'];

	$reporte = $db
		->where('Id_reaf', $idre)
		->objectBuilder()->get('certificado_aforo');

	$res = $reporte[0];

	$compartimientos = '';

	$comp = $db
		->where('Id_reaf', $idre)
		->objectBuilder()->get('compartimientos_aforo');

	foreach ($comp as $resco) {
		$compartimientos .= '<div class="temp"><hr>
				<a class="quitar">Eliminar</a><div class="Registro-der">
				<label>Capacidad Operacional GLS *</label>
				<input type="text" placeholder="Capacidad Operacional GLS" name="certifica[capacidadope][]" value="' . $resco->capacidadgls . '" required>
				<label>Capacidad Nominal BLS *</label>
				<input type="text" placeholder="Capacidad Nominal BLS" name="certifica[capacidadnom][]" value="' . $resco->capacidadbls . '" required>
				<label>Altura Operacional  (m)*</label>
				<input type="text" placeholder="Altura Operacional" name="certifica[alturaopera][]" value="' . $resco->alturaop . '" required>
				<label>Altura Referencial (m)*</label>
				<input type="text" placeholder="Altura Referencial " name="certifica[alturarefe][]" value="' . $resco->alturaref . '" required>
				</div>
				<div class="Registro-der">
				<label>Dimensión del tanque </label>
				<label>Diámetro (m3)*</label>
				<input type="text" placeholder="Diámetro" name="certifica[diametro][]" value="' . $resco->diametro . '" required>
				<label>Alto (m)*</label>
				<input type="text" placeholder="Alto" name="certifica[alto][]" value="' . $resco->alto . '" required>
				<label>Ancho (m)*</label>
				<input type="text" placeholder="Ancho" name="certifica[ancho][]" value="' . $resco->ancho . '" required>
				<label>Largo (m)*</label>
				<input type="text" placeholder="Largo" name="certifica[largo][]" value="' . $resco->largo . '" required>
				</div></div>';
	}

	$lista_vendedores = '';

	$vendedores = $db
		->orderBy('nombre_v', 'ASC')
		->objectBuilder()->get('vendedores');

	if ($db->count > 0) {
		foreach ($vendedores as $rsv) {
			$lista_vendedores .= '<option value="' . $rsv->Id_v . '" ' . ($rsv->Id_v == $res->vendedor ? "selected" : "") . ' >' . $rsv->nombre_v . '</option>';
		}
	}
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/msj.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<script src="js/modernizr.custom.js"></script>
	<style>
		.quitar {
			cursor: pointer;
			float: right;
		}

		.Registro-der {
			width: 46%;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="reporte">
				<div class="Contenido-admin-izq">
					<h2>Editar Cerfificado de Aforo</h2>
					<form id="ed_certificado">
						<div class="Registro">
							<div class="Registro-der">
								<label>Reporte Nº *</label>
								<input type="text" placeholder="Reporte Nº" name="certifica[reporte]" value="<?php echo $res->reporte ?>" class="ncertificadoaf" readonly>
								<label>Ciudad *</label>
								<input type="text" placeholder="Ciudad" name="certifica[ciudad]" value="<?php echo $res->ciudad ?>" class="auto_ciu" required>
								<label>Fecha *</label>
								<input type="date" placeholder="Fecha" name="certifica[fecha]" value="<?php echo $res->fecha ?>" required>
								<label>Placa *</label>
								<input type="text" placeholder="Placa" name="certifica[placa]" value="<?php echo $res->placa ?>" required>
								<label>Propietario *</label>
								<input type="text" placeholder="Propietario" name="certifica[propietario]" value="<?php echo $res->propietario ?>" required>
								<label>Capacidad Total *</label>
								<input type="text" placeholder="Capacidad Total" name="certifica[capacidadtotal]" value="<?php echo $res->capacidadtotal ?>" required>
								<label>Capacidad BLS *</label>
								<input type="text" placeholder="Capacidad BLS" name="certifica[capacidadbls]" value="<?php echo $res->capacidadbls ?>" required>
								<label>Cantidad de rompe olas *</label>
								<input type="text" placeholder="Cantidad de rompe olas" name="certifica[rompeolas]" value="<?php echo $res->rompeolas ?>" required>
								<label>Nº Compartimientos *</label>
								<input type="text" placeholder="Compartimientos" name="certifica[compartimientos]" value="<?php echo $res->compartimientos ?>" required>
							</div>
							<div class="Registro-der">
								<label>Nº ejes *</label>
								<input type="text" placeholder="Nº ejes" name="certifica[ejes]" value="<?php echo $res->ejes ?>" required>
								<label>Material de tanque *</label>
								<input type="text" placeholder="Material de tanque" name="certifica[materialtanque]" value="<?php echo $res->materialtanque ?>" required>
								<label>Marca *</label>
								<input type="text" placeholder="Marca" name="certifica[marca]" value="<?php echo $res->marca ?>" required>
								<label>Valvulas de descarque *</label>
								<input type="text" placeholder="Valvulas de descarque" name="certifica[valvulas]" value="<?php echo $res->valvulas ?>" required>
								<label>Contenido *</label>
								<input type="text" placeholder="Contenido" name="certifica[contenido]" value="<?php echo $res->contenido ?>" required>
								<label>Certificador *</label>
								<input type="text" placeholder="Certificador" name="certifica[certificador]" value="<?php echo $res->certificador ?>" required>
								<label>Valido hasta *</label>
								<input type="date" placeholder="Valido hasta" name="certifica[validez]" value="<?php echo $res->validez ?>">
								<?php
								$lista_inspectores = '';

								$inspectores = $db
									->objectBuilder()->get('inspectores');

								foreach ($inspectores as $resin) {
									if ($resin->Id_ins == $res->inspector)
										$lista_inspectores .= '<option value="' . $resin->Id_ins . '" selected>' . $resin->nombre_ins . '</option>';
									else
										$lista_inspectores .= '<option value="' . $resin->Id_ins . '" >' . $resin->nombre_ins . '</option>';
								}
								?>
								<label>Inspector *</label>
								<select name="certifica[inspector]" required>
									<option>Selecciona</option>
									<?php echo $lista_inspectores ?>
								</select>
								<label>Vendedor *</label>
								<select name="certifica[vendedor]" required>
									<option>Selecciona</option>
									<?php echo $lista_vendedores ?>
								</select>
							</div>
							<div class="Registro-cent">
								<label>Restricciones *</label>
								<textarea placeholder="Restricciones" name="certifica[restricciones]" required><?php echo $res->restricciones ?></textarea>
							</div>
							<div id="compartimientos">
								<?php echo $compartimientos ?>
							</div>
							<div class="Registro-der">
								<button type="button" id="agr_comp">Agregar Compartiemiento</button>
							</div>
							<br>
							<br>
							<input type="hidden" name="certifica[id]" value="<?php echo $idre ?>">
							<input type="submit" value="Guardar Certificado">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/aforo_edt.js"></script>
</body>

</html>
