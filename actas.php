<?php
require "libs/conexion.php";
$id        = $_REQUEST['iden'];
$curso_ver = $_REQUEST['curso_ver'];
$cursos    = '';
$cont      = 0;
$meses     = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

$expide      = $_REQUEST['expide'];
$expide      = explode('-', $expide);
$expide      = $expide[2] . ' de ' . $meses[$expide[1] - 1] . ' de ' . $expide[0];
$vencimiento = date('d-m-Y', strtotime($_REQUEST['expide'] . ' + 365 days'));
$vencimiento = explode('-', $vencimiento);
$vencimiento = $vencimiento[0] . ' de ' . $meses[$vencimiento[1] - 1] . ' de ' . $vencimiento[2];

$registros = $db
	->where('numero_ident', $id)
	->groupBy('certificado')
	->orderBy('Id', 'ASC')
	->objectBuilder()->get('registros');

$total = $db->count;

foreach ($registros as $rsg) {
	$nombres = '';
	$apellidos = '';
	$nombre = explode(' ', strtolower(str_replace('Ñ', 'ñ', $rsg->nombre_primero . ' ' . $rsg->nombre_segundo)));

	foreach ($nombre as $key) {
		$nombres .= ucfirst($key) . ' ';
	}

	$apellido = explode(' ', strtolower(str_replace('Ñ', 'ñ', $rsg->apellidos)));

	foreach ($apellido as $key) {
		$apellidos .= ucfirst($key) . ' ';
	}

	$nombre     = $nombres . $apellidos;
	$tipoid     = $rsg->tipo_ident;
	$identifica = $rsg->numero_ident;

	$licencia = '';

	if ($rsg->licencia != '') {
		$licencia = $rsg->licencia;
	}

	if ($cont == 0) {
		$c1 = 'border-top:1px solid #000;';
	} else {
		$c1 = '';
	}

	$c2 = 'border-bottom:1px solid #000;';

	$formacion = '';


	if (in_array($rsg->certificado, $curso_ver)) {
		$certificaciones = $db
			->where('Id_ct', $rsg->certificado)
			->objectBuilder()->get('certificaciones');

		if ($db->count > 0) {
			$formacion = $certificaciones[0]->nombre;
		}

		$cursos .= '<tr>
						<td class="tmin2" width="500" style="border-left:1px solid #000;' . $c1 . $c2 . '">  <strong>' . $formacion . '</strong></td>
						<td  class="tmin2" width="80" align="center" style="border-right:1px solid #000;' . $c1 . $c2 . '"><strong>' . $rsg->horas . ' HORAS</strong></td>
					</tr>';
		$cont++;
	}
}

$tvehiculo = '';

switch (strtolower($licencia)) {
	case 'c1':
		$tvehiculo = 'automóviles, camperos, camionetas y microbuses';
		break;
	case 'c2':
		$tvehiculo = 'camiones, rígidos, busetas y buses';
		break;
	case 'c3':
		$tvehiculo = 'vehículos articulados';
		break;
}

require_once 'libs/tcpdf.php';

class MYPDF extends TCPDF
{
	public function Header()
	{
		$bMargin         = $this->getBreakMargin();
		$auto_page_break = $this->AutoPageBreak;
		$this->SetAutoPageBreak(false, 0);
		$image_file = 'images/fondo_acta.jpg';
		$this->Image($image_file, 0, 0, 220, 280, '', '', '', false, 300, '', false, false, 0);
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		$this->setPageMark();
	}
	public function Footer()
	{
		// $this->SetY(-45);
		$this->SetY(-40);
		$estilo = '<style>
						table{
							color: #999999;
							font-size: 12px;
						}
						.link{
							color: #495de6;
						}
					</style>';

		$foot = '<table  cellspacing="0" cellpadding="0" border="0">
					<!--<tr>
						<td align="center"><i>GRI COMPANY SAS / Villavicencio - meta carrera 20 # 37 - 23 oficina 02 Cel: 314-3257703</i></td>
					</tr>
					<tr>
						<td align="center"><i>Villanueva Casanare parqueadero "la campirana" Cel: 320-8748320 / alto del trigo contiguo</i></td>
					</tr>
					<tr>
						<td align="center"><i>"destino seguro" Cel: 311-4607263 / yopa l- Casanare Parqueadero "don prospero"</i></td>
					</tr>
					<tr>
						<td align="center"><i>Cel: 314-2114658 / Barrancabermeja Cel: 311-4607263</i></td>
					</tr>
					<tr>
						<td align="center"><i>Email: <span class="link">gricompanysas@gmail.com</span> - <span class="link">www.gricompany.co</span></i></td>
					</tr>-->
					<tr>
						<td align="center"><i>GRI COMPANY SAS / Villavicencio - meta Carrera 37 # 26c – 57 Barrio 7 de Agosto</i></td>
					</tr>
					<tr>
						<td align="center"><i>Cel:  314-3257703 - 300-2026742</i></td>
					</tr>
					<tr>
						<td align="center"><i>Email: gricompanysas@gmail.com - www.gricompany.co</i></td>
					</tr>
				</table>';

		$this->writeHTML($estilo . $foot, true, false, false, false, '');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
$pdf->setPageOrientation('p');

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// PDF_MARGIN_TOP
$pdf->SetMargins(25, 30, 25);
// $pdf->SetPrintHeader(false);
// $pdf->setPrintFooter(false);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(true, 45);

// set image scale factor
// $pdf->setImageScale(1.53);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
	require_once dirname(__FILE__) . '/lang/eng.php';
	$pdf->setLanguageArray($l);
}

$pdf->AddPage();
$arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 11);

$estilo = '<style>
				table{
					font-size: 14px;
					font-family: arial;
				}
				.tmin{
					font-size: 12px;
				}
				.tmin2{
					font-size: 11px;
				}
			</style>';

$html = '<table cellspacing="0" cellpadding="5" border="0" >
			<tr>
				<td align="center"><strong>INSTITUTO PARA EL TALENTO Y DESARROLLO HUMANO GESTIÓN DEL RIESGO</strong>
				<br>
				<strong>INTEGRAL GRICOMPANY S.A.S</strong>
				<br>
				<span class="tmin">Nit. 900 892 983-6</span>
				<br>
				<!--<span class="tmin">Resolución no. 3753 secretaria de salud del meta</span>
				<br>
				<span class="tmin">Convenio autorizado mediante resolución Sena 189 - 2015</span>-->
				<span class="tmin">Resolución N° 1500-56.03 1795 de la secretaria de educación Licencia seguridad y salud en el trabajo Resolución N° 3753 Secretaria de Salud del Meta </span>
				</td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td align="center"><strong>Hace contar que</strong></td>
			</tr>
			<tr>
				<td align="justify">Que el(la) señor(a) <strong>' . $nombre . '</strong>identificado(a) con cedula de ciudadania <strong>' . $tipoid . ' ' . $identifica . '</strong> realizo en la <strong>INSTITUCION EDUCATIVA PARA EL TALENTO Y DESARRROLLO HUMANO GRI COMPANY S.A.S.</strong> los cursos básicos de actualización de los siguientes temas:</td>
			</tr>
		</table>
		<br><br><br>
		<table cellspacing="0" cellpadding="1" border="0">
			' . $cursos . '
		</table>
		<br><br><br>
		<table cellspacing="0" cellpadding="5" border="0">
			<tr>
				<td>Además se certifica que posee licencia de conducción categoría <strong>(' . $licencia . ')</strong> que lo habilita para conducir ' . $tvehiculo . ' según la ley 1383 de marzo 16 de 2010.</td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td>Se expide en Villavicencio – Meta el ' . $expide . ' y su vencimiento es el ' . $vencimiento . '.</td>
			</tr>
			<tr>
				<td height="20"></td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="-15" border="0">
			<tr>
				<td><img src="images/firma_marcela2.png" width="150"></td>
			</tr>
			<tr>
				<td><strong>Lady Marcela Sánchez Martínez<br>Representante legal Gri Company S.A.S</strong>
				</td>
			</tr>
		</table>';

$pdf->writeHTML($estilo . $html, true, false, false, false, '');
$pdf->Output('ACTA CERTIFICACION - ' . $identifica . '.pdf', 'I');
