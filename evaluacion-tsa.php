<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="google-site-verification" content="oc9E4yBVx2u8uUHzYyL0_dqaWXeQipnb0i2jfsMEHaA" />
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Gricompany Gestión del Riesgo Integral</title>

	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/landing.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		input[type=radio] {
			width: auto;
			padding: 0;
			margin: 0 5px 10px 0;
			height: auto;
		}

		.eval-respuesta {
			text-align: left;
			color: #000;
			padding-top: 20px;
		}

		.Contenido-admin {
			padding: 20px 0.8em 0.8em 0.8em;
		}
	</style>
</head>

<body>
	<?php include_once("header-new-top2.php") ?>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>EVALUACION NIVEL TSA AVANZADO</h2>
				<div class="Registro comprobar-registro">
					<p>Para realizar la evaluación, ingrese a continuación su número de identificación.</p>
					<form id="registro-comprobar">
						<label>No. de Identificación</label>
						<input type="text" name="evaluacion[identificacion]" class="ntxt" value="" placeholder="No. de Identificación" required>
						<input type="submit" value="Realizar Evaluación">
					</form>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="js/registrar_evaluaciontsa.js?<?php echo time()  ?>"></script>
</body>

</html>
