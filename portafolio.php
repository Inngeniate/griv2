<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Nuestros Servicios | Gricompany Gestión del Riesgo Integral</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<?php include_once("analyticstracking.php") ?>
	<div class="Contenedor">
		<header>
			<?php include("menu.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<div class="Masshead"></div>
	<div class="Top-imagen-fondo1">
	</div>
	<section>
		<div class="Principal-text">
			<div class="Principal-text-parrafo">
				<h2>Nuestros Servicios</h2>
			</div>
		</div>

		<ul class="grid cs-style-3" id="ns-servicios">
			<div class="Serivicio-principal">
				<h3>Curso básico para el transporte de mercancías peligrosas en vehículos automotores de carga (resolución 1223 de 2014)</h3>
				<a href="javascript://" class="serv11">Saber Mas</a>
			</div>
			<li>
				<figure>
					<img src="images/9.png" alt="img04">
					<figcaption>
						<h3>Transporte de sustancias peligrosas, basico</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv9">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/4.png" alt="img04">
					<figcaption>
						<h3>Trabajo en Alturas</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv1">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/1.png" alt="img01">
					<figcaption>
						<h3>Rescate en Espacios Confinados</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv2">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/2.png" alt="img02">
					<figcaption>
						<h3>Brigada Integral de Emergencias</h3>
						<span>Primeros Auxilios</span>
						<a href="javascript://" class="serv3">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/10.png" alt="img02">
					<figcaption>
						<h3>Primeros auxilios y primer respondiente</h3>
						<span>Primeros Auxilios</span>
						<a href="javascript://" class="serv10">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/7.png" alt="img05">
					<figcaption>
						<h3>Control de incendios y manejo de extintores</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv4">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/5.png" alt="img03">
					<figcaption>
						<h3>Control de Derrame de Hidrocarburos</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv5">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/6.png" alt="img06">
					<figcaption>
						<h3>Manejo Defensivo</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv6">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/8.png" alt="img06">
					<figcaption>
						<h3>Rescate en alturas</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv7">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
			<li>
				<figure>
					<img src="images/3.png" alt="img06">
					<figcaption>
						<h3>Mecánica Básica</h3>
						<span>Capacitación</span>
						<a href="javascript://" class="serv8">Saber Mas</a>
					</figcaption>
				</figure>
			</li>
		</ul>
	</section>
	<section>
		<div class="Servicios-dentro">
			<div class="Servicios-dentro-texto">
				<div id="serv1">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Trabajo en alturas</h2>
					<hr class="divi2">
					<p>El trabajo en alturas es una actividad de alto riesgo y Conforme a las estadísticas nacionales, representa la primera causa de accidentalidad y muerte en el trabajo por lo que se requiere de la plaeación de actividades para su intervención.</p>
					<h3>JEFE DE ÁREA</h3>
					<p>El trabajo en alturas es una actividad de alto riesgo y Conforme a las estadísticas nacionales, representa la primera causa de accidentalidad y muerte en el trabajo por lo que se requiere de la planeación de actividades para su intervención. </p>
					<h3>BÁSICO OPERATIVO </h3>
					<p>Todo trabajador cuya labor sea de baja exposición en alturas, donde la altura de su trabajo no supere los 1.5 m o trabaje en plataformas de acceso a los sitios de alturas, protegidas por barandas, debe estar certificado como mínimo en el Nivel
						Básico.</p>
					<h3>AVANZADO</h3>
					<p>Todo trabajador que labore en actividades de alto riesgo y que realice trabajo en alturas con desplazamiento horzontal y vertical, debe estar certificado en el NIVEL AVANZADO.</p>
					<p>Proceso obligatorio anual en donde se actualizan conocimientos y se entrenan habilidades y destrezas en prevención y protección contra caídas.</p>
					<h3>COORDINADOR EN T.S.A</h3>
					<p>Trabajador designado por el empleador. Capaz de identificar peligros en el sitio en donde se realiza trabajo en alturas y que tiene la autorización para aplicar medidas correctivas inmediatas para controlar los riesgos asociados a dichos peligros, Debe tener certificado en la norma de competencia laboral vigente para trabajo seguro en alturas. Capacitación en el nivel coordinador de trabajo en alturas y experiencia certificada mínima de un año relaciona con trabajos en alturas. En cumplimiento a lo establecido en la Resolución 1409/2012, que estableció el Reglamento de Seguridad para Protección Contra Caídas en Trabajo en Alturas.</p>
				</div>
				<div id="serv2">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Rescate en Espacios Confinados</h2>
					<hr class="divi2">
					<p>Tiene medios limitados para entrar y salir. Se entiende por medios limitados, a todos aquellos que no permiten una entrada ni una salida en forma segura y rápida de todos sus ocupantes, por ejemplo, alcantarillas, tanques, espacios cuyo ingreso o egreso sea a través de una escalera, silleta o arnés con sistema de Elevación.</p>
				</div>
				<div id="serv3">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Brigada Integral de Emergencias</h2>
					<hr class="divi2">
					<p>GRI COMPANY SAS desarrolla programas de capacitación y entrenamiento con el objetivo de conformar Brigadas de Emergencias dentro de las organizaciones y empresas clientes, debidamente capacitadas para prevenir o controlar diversas situaciones de emergencias y ante todo realizar actividades de prevención cuando ello sea posible.</p>
					<p>Según como se enmarca en La constitución de la Brigada de Emergencia Empresarial tiene sus fundamentos legales en la Resolución 1016 de 1989, en el numeral 11 artículo 18 y su objetivo es tener un grupo de respuesta primaria que pueda atender una situación de emergencia en su etapa inicial.</p>
					<p>Existe una estructura organizacional en la Brigada de Emergencia, que generalmente es Jefe de Brigada, Líderes de Área y Brigadistas. Las funciones específicas de cada uno de ellos son contempladas en el Plan de Emergencias y Evacuación que cada empresa debe tener.</p>
					<p>Los temas de formación de los Brigadistas van desde lo básico hasta lo especializado, según el tipo de riesgo específico de la empresa.</p>
					<img src="images/brigasdista.png" alt="GRI">
					<img src="images/brigadista2.png" alt="GRI">
					<img src="images/brigadista3.png" alt="GRI">

					<p>GRI COMPANY SAS ofrece capacitación y entrenamiento con el objetivo Fortalecer la capacidad de respuesta de la Comunidad con relación a la atención básica en primeros auxilios, seguros de atender de una manera eficiente y eficaz a los lesionados, evitando en algunos casos traumas más severos.</p>
					<p><strong>CONTENIDOS DEL CURSO</strong></p>
					<ul>
						<p><strong>Fundamentos de los Primeros Auxilios</strong></p>
						<li>Definición de Primeros Auxilios</li>
						<li>Principios fundamentales para el aprendizaje de los PP.AA</li>
						<p><strong>Aspectos legales de los Primeros Auxilios</strong></p>
						<li>Omisión del deber de socorro</li>
						<li>Denegación de Auxilio</li>
						<li>Qué hacer en caso de emergencias</li>
						<p><strong>Los Signos Vitales</strong></p>
						<li>Definición de Pulso, Respiración y Temperatura</li>
						<li>Formas y Lugares de medición de signos vitales</li>
						<li>Registro e interpretación de los signos vitales.</li>
						<p><strong>ABC de la Emergencia</strong></p>
						<li>Normas Básicas de Actuación ante una Emergencia</li>
						<p><strong>Lesiones en las Partes Blandas</strong></p>
						<li>Contusiones</li>
						<li>Heridas y sus Tipos</li>
						<li>H. Cortante, Punzante, Erosiva y Contusas</li>
						<p><strong>Hemorragias</strong></p>
						<li>Tipos de hemorragias.</li>
						<li>Medidas de contención de hemorragias.</li>
						<p><strong>Quemaduras y Shock</strong></p>
						<li>Tipos de Quemaduras y su manejo</li>
						<li>Definición y Causas de Shock</li>
						<li>Manejo de Shock</li>
						<p><strong>Lesiones en las partes Duras</strong></p>
						<li>Esguince Definición y Manejo</li>
						<li>Luxación Definición y Manejo</li>
						<li>Fracturas Definición y Manejo</li>
						<p><strong>Intoxicaciones</strong></p>
						<li>Reconocimiento y Manejo</li>
						<p><strong>Accidentes Comunes</strong></p>
						<li>Atragantamiento</li>
						<li>Picaduras y Mordeduras</li>
						<li>Reconocimiento de fatigas, extenuaciones e insolación.</li>
						<li>Reconocimiento de lesiones por frío: Hipotermia y congelamiento.</li>
						<li>Histeria</li>
						<li>Epilepsia</li>
						<li>Reconocimiento de la Epilepsia</li>
						<li>Medidas de atención primarias</li>
						<p><strong>Botiquines</strong></p>
						<li>Tipos de Botiquines Caseros, y elementos que deben contener</li>
						<p><strong>Vendaje y Transporte de víctimas</strong></p>
						<li>Tipos de vendajes</li>
						<li>Técnicas de Traslado y Transporte de Victimas</li>
						<p><strong>Reanimación Cardio Pulmonar RCP</strong></p>
						<li>Normativas vigentes</li>
						<li>Protocolos de actuación en emergencias</li>
						<li>RCP Adulto</li>
						<li>RCP Pediátrico</li>
						<li>Uso de Desfibrilador Externo Automático</li>
					</ul>
				</div>
				<div id="serv4">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Control de Incendios</h2>
					<hr class="divi2">
					<p><strong>GRI COMPANY SAS</strong> ofrece capacitación y entrenamiento con el objetivo Fortalecer la respuesta de los trabajadores ante las posibles situaciones de emergencia en la empresa u/o sitio de trabajo. Uno de los riesgos más importantes, debido a sus fatales consecuencias es el de incendio, sobre todo si no se tienen unos conocimientos básicos tanto para prevenirlo como para controlarlo. </p>


					<p><strong>Dirigido a:</strong></p>
					<ul>
						<li>Trabajadores pertenecientes a los equipos de primera intervención (EPI) de cualquier sector.</li>
						<li>Toda persona con interés en adquirir unos conocimientos mínimos en la lucha contra incendios. </li>

						<p><strong>Objetivos:</strong></p>
						<li>Que el alumno se conciencie de la necesidad de la prevención y la planificación de emergencias. </li>
						<li>Que el alumno conozca los conceptos básicos sobre el fuego, su propagación y su control. </li>
						<li>Que el alumno sea capaz de usar los medios de extinción de incendios a su alcance con seguridad y eficacia. </li>
						<p>Programa teórico:</p>
						<p>1. Química y Física del Fuego, conceptos básicos.</p>
						<p>2. Comportamiento del fuego en interiores (fuegos estructurales)</p>
						<p>3. Métodos de extinción y agentes extintores.</p>
						<p>4. Extintores Portátiles.</p>
						<p>5. Bocas de Incendio Equipadas (B.I.E.s)</p>

						<p>Programa práctico:</p>
						<p>1. Uso de Extintores.</p>
						<p>2. Uso de B.I.E.s. e Hidrantes.</p>

				</div>
				<div id="serv5">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Control de Derrame de Hidrocarburos</h2>
					<hr class="divi2">
					<p>El presente programa tiene un balance teórico – práctico en marcado en la resolución 1223 de 2014 (Por la cual se establecen los requisitos del curso básico obligatorio de capacitación para los conductores de vehículos de carga que transportan mercancías peligrosas y se dicta una disposición), orientado a que los participantes se focalicen en la toma de control ante una situación de derrames de crudo y sus derivados. Desarrolle las bases para una capacidad analítica y estratega con una efectiva velocidad de respuesta orientada a atacar el problema de raíz y activar las medidas que permitan controlar el derrame y no cause mayores impactos operacionales y ambientales, mediante la elaboración de casos prácticos reales de los participantes, casos de estudios relativos a planes de control de derrames.</p>
					<p>NOTA: contamos con un convenio con la empresa <strong>ARKOFF INGENIERIA</strong> en donde se tiene pista totalmente dotada para la práctica de la atención, manejo y control dde sustancias peligrosas con énfasis en hidrocarburos, esta base se encuentra en el alto de la guala via cuaral – paratebueno/meta (punto crítico por el número de accidentes de tránsito con transporte de sustancias peligrosas).</p>
				</div>
				<div id="serv6">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Manejo Defensivo</h2>
					<hr class="divi2">
					<p>Recibir un entrenamiento que lo acondicione física, sicológica y técnicamente en la conducción defensiva.</p>
				</div>
				<div id="serv7">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Rescate en alturas</h2>
					<hr class="divi2">
					<p>Proporcionar a los participantes los conocimientos y técnicas necesarias para rescatar, atender correctamente en el propio lugar del incidente a un lesionado en medio acuático, para estabilizarlo y darle un transporte adecuado a un centro Asistencial.</p>
				</div>
				<div id="serv8">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Mecánica Básica</h2>
					<hr class="divi2">
					<p>La mecánica automotriz hace referencia al estudio, diagnóstico y reparación de los mecanismos externos e internos que producen la energía necesaria para el funcionamiento del motor a gasolina y su rodaje. Entre estos mecanismos o conjunto de elementos están el sistema de carburación, sistema de frenos, sistema de transmisión, etc. capaces de transmitir movimiento de un automotor</p>
				</div>
				<div id="serv9">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Transporte de sustancias peligrosas, basico</h2>
					<hr class="divi2">
					<p>GRI COMPANY SAS desarrolla programas de capacitación y entrenamiento con el objetivo Sensibilizar e instruir al conductor en aspectos legales y normativos relacionados con el transporte de dichas sustancias y estipulados en el Código Nacional de Tránsito Terrestre y demás regulaciones relacionadas con esta práctica.</p>
					<p><strong>Temario:</strong></p>
					<ul>
						<li>Introducción a los materiales peligrosos.</li>
						<li>Legislación Colombiana aplicada al transporte de mercancías peligrosas.</li>
						<li>Sistema de clasificación: sistema DOT, NFPA 704, ONU, SGA.</li>
						<li>Guía de Respuesta a emergencias GRE.</li>
						<li>Libro naranja de la ONU.</li>
						<li>La protección en incidentes con materiales peligrosos (vías de exposición).</li>
						<li>Ropa de protección personal en manejo de materiales peligrosos.</li>
						<li>Sistema de comando incidentes SCI.</li>
						<li>Resolución 1223 de 2014</li>

					</ul>
					<p><strong>Dirigido a:</strong></p>
					<p>Conductores de vehículos que transportan sustancias peligrosas.</p>
					<br>
					<img src="images/tran1.jpg" alt="GRI">
					<img src="images/tran2.jpg" alt="GRI">
					<br>
				</div>

				<div id="serv10">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>primeros auxilios y primer respondiente</h2>
					<hr class="divi2">
					<p>GRI COMPANY SAS ofrece capacitación y entrenamiento con el objetivo Fortalecer la capacidad de respuesta de la Comunidad con relación a la atención básica en primeros auxilios, seguros de atender de una manera eficiente y eficaz a los lesionados, evitando en algunos casos traumas más severos.</p>

					<p><strong>CONTENIDOS DEL CURSO</strong></p>
					<p>Fundamentos de los Primeros Auxilios</p>
					<ul>
						<li>Definición de Primeros Auxilios</li>
						<li>Principios fundamentales para el aprendizaje de los PP.AA</li>
						<p><strong>Aspectos legales de los Primeros Auxilios</strong></p>
						<li>Omisión del deber de socorro</li>
						<li>Denegación de Auxilio</li>
						<li>Qué hacer en caso de emergencias</li>
						<li>Los Signos Vitales</li>
						<li>Definición de Pulso, Respiración y Temperatura</li>
						<li>Formas y Lugares de medición de signos vitales</li>
						<li>Registro e interpretación de los signos vitales.</li>
						<p><strong>ABC de la Emergencia</strong></p>
						<li>Normas Básicas de Actuación ante una Emergencia</li>
						<p><strong>Lesiones en las Partes Blandas</strong></p>
						<li>Contusiones</li>
						<li>Heridas y sus Tipos</li>
						<li>H. Cortante, Punzante, Erosiva y Contusas</li>
						<p><strong>Hemorragias</strong></p>
						<li>Tipos de hemorragias.</li>
						<li>Medidas de contención de hemorragias.</li>
						<p><strong>Quemaduras y Shock</strong></p>
						<li>Tipos de Quemaduras y su manejo</li>
						<li>Definición y Causas de Shock</li>
						<li>Manejo de Shock</li>
						<p><strong>Lesiones en las partes Duras</strong></p>
						<li>Esguince Definición y Manejo</li>
						<li>Luxación Definición y Manejo</li>
						<li>Fracturas Definición y Manejo</li>
						<p><strong>Intoxicaciones</strong></p>
						<li>Reconocimiento y Manejo</li>
						<p><strong>Accidentes Comunes</strong></p>
						<li>Atragantamiento</li>
						<li>Picaduras y Mordeduras</li>
						<li>Reconocimiento de fatigas, extenuaciones e insolación.</li>
						<li>Reconocimiento de lesiones por frío: Hipotermia y congelamiento.</li>
						<li>Histeria</li>
						<li>Epilepsia</li>
						<li>Reconocimiento de la Epilepsia</li>
						<li>Medidas de atención primarias</li>
						<p><strong>Botiquines</strong></p>
						<li>Tipos de Botiquines Caseros, y elementos que deben contener</li>
						<p><strong>Vendaje y Transporte de víctimas</strong></p>
						<li>Tipos de vendajes</li>
						<li>Técnicas de Traslado y Transporte de Victimas</li>
						<p><strong>Reanimación Cardio Pulmonar RCP</strong></p>
						<li>Normativas vigentes</li>
						<li>Protocolos de actuación en emergencias</li>
						<li>RCP Adulto</li>
						<li>RCP Pediátrico</li>
						<li>Uso de Desfibrilador Externo Automático</li>
					</ul>
					<br>
				</div>
				<div id="serv11">
					<a href="javascript://" class="list-serv">Volver</a>
					<h2>Curso básico para el transporte de mercancías peligrosas en vehículos automotores de carga (resolución 1223 de 2014)</h2>
					<hr class="divi2">
					<p>Gracias al convenio entre <strong>GRI COMPANY SAS – RC COMPETENCIAS OEC SAS - la CORPORACIÓN UNISYSTEM DE COLOMBIA</strong>, se han fortalecido los programas de formación complementaria, dando respuesta a las necesidades de formación y actualización en diferentes áreas teniendo en cuenta EL SABER, EL SABER HACER y EL SABER SER para una formación integral; contando con instructores idóneos y certificados en las áreas específicas de formación.</p>
					<p>En Cumplimiento con la Resolución 1223 de Mayo de 2014 (Certificación Curso Básico Obligatorio y Certificación Por Competencias, Como Único Requisito). La Corporación Unisystem de Colombia, institución de educación para el Trabajo y el Desarrollo Humano cuenta con licencia de iniciación de labores No. 0202 de abril de 1999, Resolución No. 0845 de 2007: cambio de razón social “UNISYSTEM DE COLOMBIA”, por “CORPORACION UNISYSTEM DE COLOMBIA”, Acuerdo No. 074 de 2007 expedido por el Ministerio de la Protección Social para los programas del área de Auxiliares en salud, aprobados por competencias laborales y Registros de aprobación de programas Nos. 0620, 0621, 0622, 0623, 0624, 0625 y 0626 expedidos por la Secretaria de Educación Municipal para las carreras de formación técnica laboral por competencias, con Resolución No. 2265 en los programas por competencias laborales del área de transporte y reconocimiento de programas SENA según Resolución No. 545 de 2013. Contamos con certificación de calidad ISO 9001:2008, certificación de calidad NTC 5555:2011, NTC 5663:2011, NTC 5581:2011 y NTC 5666:2011. Cumpliendo con lo establecido en la ley 1064 de 2006 y el decreto 4904 de 2009, donde El Estado reconoce la Educación para el Trabajo y el Desarrollo Humano como factor esencial del proceso educativo de la persona y componente dinamizador en la formación de técnicos laborales y expertos en las artes y oficios, en consecuencia hacen parte integral del servicio público educativo y no podrá ser discriminada, como se establece en el literal 5.8: “EDUCACION INFORMAL. La oferta de educación informal tiene como objetivo brindar oportunidades para complementar, actualizar, perfeccionar, renovar o profundizar conocimientos, habilidades, técnicas y prácticas.</p>
					<p>OBJETIVO</p>
					<p>Cumplir y formar con base en Competencias Laborales, logrando el mejoramiento continuo, creando un nivel de conciencia y responsabilidad en los candidatos que asisten a la formación, cumpliendo con la normatividad vigente; de acuerdo a lo exigido en la Resolución 1223 del 14 de mayo de 2014 expedida por el Ministerio de Transporte; el cual en su artículo 4 establece las instituciones que pueden impartir dicha formación y el personal de Instructores Competentes debidamente certificados en su área. Los artículos 3, 5 y 6 establecen los lineamientos para el curso básico de formación para los conductores que</p>
					<p>transportan mercancías peligrosas y lo establecido en el artículo 7 literal 2 y los artículos 8 y 9 donde se establece la certificación con base en las normas de competencia laboral según tipo de vehículo y clase de mercancía de la titulación correspondiente, homologando los estudios para dar continuidad a la formación, teniendo como referencia las titulaciones pertinentes y registrando la información según lo establecido en el artículo 10 de la presente resolución.</p>

				</div>
			</div>
		</div>
	</section>
	<section>
		<?php include("convenios.php"); ?>
	</section>
	<script src="js/toucheffects.js"></script>

	<?php include("redes.php"); ?>
	<footer>
		<?php include("footer.php"); ?>
	</footer>
	<script>
		$('#ns-servicios a').on('click', function() {
			serv = $(this).attr('class');
			$('#' + serv).fadeIn();
			$('#ns-servicios ').fadeOut(0);
		});
		$('.list-serv').on('click', function() {
			$('#' + serv).fadeOut(0);
			$('#ns-servicios ').fadeIn();
		});
	</script>
</body>

</html>
