<?php
require "libs/conexion.php";
$registro = $_GET['registro'];

$bus = $db
    ->where('Id', $registro)
    ->objectBuilder()->get('registros');

$res = $bus[0];

$nombre = $res->nombre_primero . ' ' . $res->nombre_segundo . ' ' . $res->apellidos;
$tipoid = $res->tipo_ident;
$documento = $tipoid . '. ' . $res->numero_ident;
$expedicion = $res->fecha_inicio;
$expedicion = date_create($expedicion);
$expedicion = date_format($expedicion, 'd-m-Y');
$vencimiento = '';

if ($res->vencimiento_ct != '0000-00-00' || $res->vencimiento_ct != '') {
    $vencimiento = $res->vencimiento_ct;
    $vencimiento = date_create($vencimiento);
    $vencimiento = date_format($vencimiento, 'd-m-Y');
}

$rh          = $res->rh;
$licencia    = $res->licencia;

$formacion = '';

$cursos = $db
    ->where('Id_ct', $res->certificado)
    ->objectBuilder()->get('certificaciones');

if ($db->count > 0) {
    $rsc       = $cursos[0];
    $formacion = $rsc->nombre;
}

$horas = $res->horas;

require_once 'libs/tcpdf.php';
require_once 'libs/fpdi/fpdi.php';

$exa = new FPDI();

/* if ($res->cargafoto == 1 || $res->cargafoto == '') {
    $exa->setSourceFile('libs/pl_carnet_6.pdf');

    $tplIdx = $exa->importPage(1, '/MediaBox');
    $exa->SetPrintHeader(false);

    $exa->addFont('conthrax', '', 'conthrax.php');
    $exa->addFont('ubuntucondensed', '', 'ubuntucondensed.php');
    $exa->SetFont('conthrax', '', 8);

    $estilo = '<style>
                .nm{
                    color: #000;
                    font-size: 6;
                    font-family: arial;
                }
                .bl{
                    color: #000;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    color: #000;
                    font-family: arial;
                    font-size: 5;
                }
                .bl3{
                    color: #000;
                    font-family: arial;
                    font-size: 7;
                }
                .bl4{
                    color: #000;
                    font-family: arial;
                    font-size: 7;
                }
                .bl5{
                    color: #000;
                    font-family: ubuntucondensed;
                    font-size: 8;
                }
                .bl6{
                    color: #000;
                    font-family: arial;
                    font-size: 6;
                }
            </style>';

    $exa->AddPage();
    $exa->useTemplate($tplIdx);
    $exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $exa->setJPEGQuality(100);

    $exa->SetXY(48, 40);
    $exa->StartTransform();
    $exa->Rotate(90);


    if ($vencimiento == '') {
        $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td align="center"><strong class="bl">ExpediciÃ³n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RH:</strong></td></tr><tr><td align="center"><strong class="bl">' . $expedicion . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $vencimiento . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $rh . '</strong></td></tr></table>';
    } else {
        $txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td align="center"><strong class="bl">ExpediciÃ³n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vencimiento:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RH:</strong></td></tr><tr><td align="center"><strong class="bl">' . $expedicion . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $vencimiento . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $rh . '</strong></td></tr></table>';
    }

    $exa->SetXY(27.6, 90.5);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="170px" cellpadding="-2" cellspacing="0"><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DuraciÃ³n:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Licencia:</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $horas . ' Hrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $licencia . '</strong></td></tr></table>';

    $exa->SetXY(27.6, 96.5);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="170px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl6">FormaciÃ³n:</strong><strong class="bl6"> ' . $formacion . '</strong></td></tr></table>';

    $exa->SetXY(27.6, 104.5);
    $exa->WriteHTML($estilo . $txt);

    // if ($res->foto_ct != '') {
    //     $exa->Image(substr(str_replace(' ', '', $res->foto_ct), 3), 43.6, 46.5, 20);
    // }

    $exa->StopTransform();
    if ($res->capacitacion != 'ALTURAS') {
        $exa->cleanUp();
        $exa->setSourceFile('libs/pl_carnet_6.pdf');
    }

    $tplIdx = $exa->importPage(2, '/MediaBox');
    $exa->AddPage();
    $exa->useTemplate($tplIdx);

    $exa->Output('carnet.pdf', 'I');
} else { */

if ($res->cargafoto == 1 && $res->foto_ct != '') {
    if ($vencimiento != '') {
        $exa->setSourceFile('libs/pl_carnet_6-a.pdf');
    } else {
        $exa->setSourceFile('libs/pl_carnet_6-c.pdf');
    }
} else {
    if ($vencimiento != '') {
        $exa->setSourceFile('libs/pl_carnet_6.pdf');
    } else {
        $exa->setSourceFile('libs/pl_carnet_6-b.pdf');
    }
}


$tplIdx = $exa->importPage(1, '/MediaBox');
$exa->SetPrintHeader(false);

$exa->addFont('conthrax', '', 'conthrax.php');
$exa->addFont('ubuntucondensed', '', 'ubuntucondensed.php');

$estilo = '<style>
                .nm{
                    color: #000;
                    font-size: 5.5;
                    font-family: arial;
                }
                .bl{
                    color: #000;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    color: #000;
                    font-family: arial;
                    font-size: 5;
                }
                .b{
                    font-weight: bold
                }
                .rojo{
                    color: #d40e22;
                }
            </style>';

$exa->AddPage();
$exa->useTemplate($tplIdx);
$exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
$exa->setJPEGQuality(100);

$exa->SetXY(50, 40);


$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="nm">' . $nombre . '</strong></td></tr></table>';

$exa->SetXY(46.7, 32.8);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $documento . '</strong></td></tr></table>';

$exa->SetXY(36.5, 35);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $rh . '</strong></td></tr></table>';

$exa->SetXY(59, 35);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="170px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl">' . $licencia . '</strong></td></tr></table>';

$exa->SetXY(81, 35);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="150px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl rojo">' . $formacion . '</strong></td></tr></table>';

$exa->SetXY(36.2, 42);
$exa->WriteHTML($estilo . $txt);

$txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $expedicion . '</strong></td></tr></table>';

$exa->SetXY(47.5, 49.2);
$exa->WriteHTML($estilo . $txt);

if ($vencimiento != '') {
    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $vencimiento . '</strong></td></tr></table>';

    $exa->SetXY(70, 49.2);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $horas . ' Hrs</strong></td></tr></table>';

    $exa->SetXY(89.5, 49.2);
    $exa->WriteHTML($estilo . $txt);
} else {
    $txt = '<table border="0" width="100px" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl2 rojo">' . $horas . ' Hrs</strong></td></tr></table>';

    $exa->SetXY(67, 49.2);
    $exa->WriteHTML($estilo . $txt);
}

if ($res->foto_ct != '') {
    $exa->Image(substr(str_replace(' ', '', $res->foto_ct), 3), 93, 19.5, 20);

    $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(222, 48, 53));

    $exa->Line(93, 19.5, 113, 19.5, $style);
    $exa->Line(93, 19.25, 93, 46.75, $style);

    $exa->Line(93, 46.5, 113, 46.5, $style);
    $exa->Line(113, 19.25, 113, 46.75, $style);
}

$exa->StopTransform();

$tplIdx = $exa->importPage(2, '/MediaBox');
$exa->AddPage();
$exa->useTemplate($tplIdx);

$exa->Output('carnet.pdf', 'I');
// }
