<?php
require "libs/conexion.php";
@$informacion = array();

if (!isset($_REQUEST['ev'])) {
    header('Location: trabajo-altura');
}

$ideva = $_REQUEST['ev'];

date_default_timezone_set("America/Bogota");

$nfecha = date('Y-m-j');

$evaluacion = $db
    ->where('Id_ev', $ideva)
    ->objectBuilder()->get('evaluaciones_tsa');

if ($db->count > 0) {
    $rev = $evaluacion[0];
    $fecha_ev = $rev->fecha_ev;

    $registro = $db
        ->where('documento_al', $rev->identificacion_ev)
        ->objectBuilder()->get('registros_alturas');

    if ($db->count > 0) {
        $rg = $registro[0];

        $nombre = $rg->nombre_al;
        $cedula = $rg->documento_al;
        $expedida = $rg->documento_de_al;
        $telefono = $rg->telefono_al;
        $empresa = $rg->empresa_al;
        $firma = $rg->firma_al;

        for ($i = 1; $i < 31; $i++) {
            $pregunta = 'pregunta' . $i . '_ev';
            $pregunta = $rev->$pregunta;
            ${"respuesta" . $i} = $pregunta;
        }

        require_once('libs/tcpdf.php');

        class MYPDF extends TCPDF
        {
            public function Header()
            {

                $estilo = '<style>
                            table{
                                font-size: 11px;
                            }
                            .tmax{
                                font-size: 12px;
                            }
                            .tmin2{
                                font-size: 11px;
                            }
                        </style>';

                $bMargin = $this->getBreakMargin();
                $auto_page_break = $this->AutoPageBreak;
                $this->SetAutoPageBreak(false, 0);
                $this->SetAutoPageBreak($auto_page_break, $bMargin);
                $this->setPageMark();

                $head = '<table cellspacing="0" cellpadding="0" border="0" class="certificado1">
                                    <tr>
                                        <td height="20"><br><br><img src="images/logo.png" width="120px"><br></td>
                                    </tr>
                                    <tr>
                                        <td class="tmax"><b>HOJA DE RESPUESTAS EVALUACION AVANZADO</b></td>
                                    </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0" border="0" class="certificado1" width="99%">
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="tmax" align="right"><br><br><br><strong>FED 009</strong><br></td>
                                    </tr>
                                </table>';

                $this->writeHTML($estilo . $head, true, false, false, false, '');
            }
        }

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        $pdf->setPageOrientation('p');

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // PDF_MARGIN_TOP
        $pdf->SetMargins(20, 50, 20);
        // $pdf->SetPrintHeader(false);
        // $pdf->setPrintFooter(false);

        // set auto page breaks
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetAutoPageBreak(true, 45);

        // set image scale factor
        // $pdf->setImageScale(1.53);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

        $pdf->AddPage();
        $arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 11);

        $estilo = '<style>
                        table{
                            font-size: 11px;
                        }
                        .tmax{
                            font-size: 12px;
                        }
                        .tmin2{
                            font-size: 11px;
                        }
                        .gris{
                            background-color: #dadada;
                        }
                    </style>';

        $html = '<table cellspacing="0" cellpadding="3" border="1" class="certificado1" width="99%">
                    <tr>
                        <td class="gris" align="center"><strong>FECHA</strong></td>
                        <td>' . $fecha_ev . '</td>
                        <td class="gris" align="center"><strong>SEDE</strong></td>
                        <td>Villavicencio</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><strong><br>DATOS DEL ESTUDIANTE<br></strong></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="gris" align="center"><strong>NOMBRE</strong></td>
                    </tr>
                     <tr>
                        <td colspan="4">' . $nombre . '</td>
                    </tr>
                    <tr>
                        <td class="gris" align="center"><strong>Numero de cedula</strong></td>
                        <td class="gris" align="center"><strong>Lugar de expedición</strong></td>
                        <td class="gris" align="center"><strong>Numero celular</strong></td>
                        <td class="gris" align="center"><strong>Empresa</strong></td>
                    </tr>
                    <tr>
                        <td>' . $cedula . '</td>
                        <td>' . $expedida . '</td>
                        <td>' . $telefono . '</td>
                        <td>' . $empresa . '</td>
                    </tr>
                </table>';


        $html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
                    <tr>
                        <td align="center" height="30"></td>
                    </tr>
                    <tr>
                        <td><strong>EVALUACION FINAL</strong></td>
                    </tr>
                    <tr>
                        <td><p>Con base en el cuestionario leído y comprendido, usted debe inscribir frente a cada numeral la letra de la respuesta que usted considere correcta. Tenga en cuenta que para cada pregunta solo existe una sola repuesta verdadera.<br>El instructor deberá colocar si aprobó o no la evaluación en el espacio respectivo.<br>En caso de no aprobar el 90%. Puede presentar la prueba de oportunidad si se ha obtenido un valor mayor al 60%. De lo contrario deberá asistir de nuevo a cada clase de teoría con una intensidad horaria de mínimo 4 horas como plan de recuperación.</p></td>
                    </tr>
                     <tr>
                        <td align="center" height="10"></td>
                    </tr>
                </table>';

        $html .= '<table cellspacing="0" cellpadding="3" border="1" width="99%">
                    <tr>
                        <td class="gris" align="center"><strong>1</strong></td>
                        <td align="center">' . $respuesta1 . '</td>
                        <td class="gris" align="center"><strong>2</strong></td>
                        <td align="center">' . $respuesta2 . '</td>
                        <td class="gris" align="center"><strong>3</strong></td>
                        <td align="center">' . $respuesta3 . '</td>
                        <td class="gris" align="center"><strong>4</strong></td>
                        <td align="center">' . $respuesta4 . '</td>
                        <td class="gris" align="center"><strong>5</strong></td>
                        <td align="center">' . $respuesta5 . '</td>
                        <td class="gris" align="center"><strong>6</strong></td>
                        <td align="center">' . $respuesta6 . '</td>
                        <td class="gris" align="center"><strong>7</strong></td>
                        <td align="center">' . $respuesta7 . '</td>
                        <td class="gris" align="center"><strong>8</strong></td>
                        <td align="center">' . $respuesta8 . '</td>
                        <td class="gris" align="center"><strong>9</strong></td>
                        <td align="center">' . $respuesta9 . '</td>
                        <td class="gris" align="center"><strong>10</strong></td>
                        <td align="center">' . $respuesta10 . '</td>
                    </tr>
                    <tr>
                        <td class="gris" align="center"><strong>11</strong></td>
                        <td align="center">' . $respuesta11 . '</td>
                        <td class="gris" align="center"><strong>12</strong></td>
                        <td align="center">' . $respuesta12 . '</td>
                        <td class="gris" align="center"><strong>13</strong></td>
                        <td align="center">' . $respuesta13 . '</td>
                        <td class="gris" align="center"><strong>14</strong></td>
                        <td align="center">' . $respuesta14 . '</td>
                        <td class="gris" align="center"><strong>15</strong></td>
                        <td align="center">' . $respuesta15 . '</td>
                        <td class="gris" align="center"><strong>16</strong></td>
                        <td align="center">' . $respuesta16 . '</td>
                        <td class="gris" align="center"><strong>17</strong></td>
                        <td align="center">' . $respuesta17 . '</td>
                        <td class="gris" align="center"><strong>18</strong></td>
                        <td align="center">' . $respuesta18 . '</td>
                        <td class="gris" align="center"><strong>19</strong></td>
                        <td align="center">' . $respuesta19 . '</td>
                        <td class="gris" align="center"><strong>20</strong></td>
                        <td align="center">' . $respuesta20 . '</td>
                    </tr>
                    <tr>
                        <td class="gris" align="center"><strong>21</strong></td>
                        <td align="center">' . $respuesta21 . '</td>
                        <td class="gris" align="center"><strong>22</strong></td>
                        <td align="center">' . $respuesta22 . '</td>
                        <td class="gris" align="center"><strong>23</strong></td>
                        <td align="center">' . $respuesta23 . '</td>
                        <td class="gris" align="center"><strong>24</strong></td>
                        <td align="center">' . $respuesta24 . '</td>
                        <td class="gris" align="center"><strong>25</strong></td>
                        <td align="center">' . $respuesta25 . '</td>
                        <td class="gris" align="center"><strong>26</strong></td>
                        <td align="center">' . $respuesta26 . '</td>
                        <td class="gris" align="center"><strong>27</strong></td>
                        <td align="center">' . $respuesta27 . '</td>
                        <td class="gris" align="center"><strong>28</strong></td>
                        <td align="center">' . $respuesta28 . '</td>
                        <td class="gris" align="center"><strong>29</strong></td>
                        <td align="center">' . $respuesta29 . '</td>
                        <td class="gris" align="center"><strong>30</strong></td>
                        <td align="center">' . $respuesta30 . '</td>
                    </tr>
                </table>';


        $html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
                    <tr>
                        <td align="center" height="40"></td>
                    </tr>
                </table>';

        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="99%">
                    <tr>
                        <td>Aprobó: ____________________ &nbsp;&nbsp; No aprobó: ____________________</td>
                    </tr>
                    <tr>
                        <td>Requiere presentar prueba de oportunidad:&nbsp;SI_________  NO__________</td>
                    </tr>
                    <tr>
                        <td align="center" height="50"></td>
                    </tr>
                </table>';

        $html .= '<table cellspacing="0" cellpadding="0" border="0" width="99%">
                    <tr>
                        <td width="250">
                            <img src="' . $firma . '" alt="" width="400">
                        </td>
                        <td colspan="3" width="320"></td>
                    </tr>
                    <tr>
                        <td width="250"><strong>___________________________________</strong></td>
                        <td width="70"></td>
                        <td width="250" align="right"><strong>___________________________________</strong></td>
                    </tr>
                    <tr>
                        <td><strong>FIRMA DEL ESTUDIANTE</strong></td>
                        <td width="70"></td>
                        <td align="right"><strong>FIRMA DEL INSTRUCTOR</strong></td>
                    </tr>
                </table>';


        $pdf->writeHTML($estilo . $html, true, false, false, false, '');

        $pdf->Output('Registro_de_asistencia.pdf', 'I');
    } else {
        echo 'No se encontraron resultados';
    }
} else {
    echo 'No se encontraron resultados';
}
