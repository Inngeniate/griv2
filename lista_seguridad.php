<div class="seguridad" style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Listar Certificados</h2>
		<div class="Registro">
			<form id="buscar4">
				<label>Código: </label>
				<input type="text" name="certifica[codigo]" id="bseguridad" placeholder="Código">
				<input type="hidden" name="certifica[accion]" value="lista_seguridad">
				<label>Tipo:</label>
				<select name="certifica[reporte]" id="ls_tipo">
					<option value="1">Arnes</option>
					<option value="2">Eslinga</option>
				</select>
				<input type="hidden" name="pagina" value="0">
				<input type="submit" value="Buscar">
			</form>
		</div>
		<div class="Listar-personas">
			<div class="Tabla-listar">
				<table>
					<thead>
						<tr>
							<th>Código</th>
							<th>Fecha</th>
							<th>Propietario</th>
							<th>Serie</th>
							<th>Modelo</th>
							<th>Editar</th>
							<th>Eliminar</th>
						</tr>
					</thead>
					<tbody id="resultados4">
					</tbody>
				</table>
			</div>
		</div>
		<div id="lista_loader4">
			<div class="loader2">Cargando...</div>
		</div>
		<div id="paginacion" class="pagina4"></div>
	</div>
</div>
