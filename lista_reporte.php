<div class="reporte" style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Listar Reportes</h2>
		<form id="buscar2">
			<label>N° Reporte: </label>
			<input type="text" name="certifica[reporte]" id="breporte2" placeholder="N° Reporte">
			<label>Placa: </label>
			<input type="text" name="certifica[placa]" id="bplaca2" placeholder="Placa">
			<input type="hidden" name="certifica[accion]" value="lista_reporte">
			<input type="hidden" name="pagina" value="0">
			<input type="submit" value="Buscar">
		</form>
		<div class="Listar-personas">
			<div class="Tabla-listar">
				<table>
					<thead>
						<tr>
							<th>N° Reporte</th>
							<th>Fecha</th>
							<th>Placa</th>
							<th>Propietario</th>
							<th>Marca</th>
							<th>Editar</th>
							<th>Eliminar</th>
						</tr>
					</thead>
					<tbody id="resultados2">
					</tbody>
				</table>
			</div>
		</div>
		<div id="lista_loader2">
			<div class="loader2">Cargando...</div>
		</div>
		<div id="paginacion" class="pagina2"></div>
	</div>
</div>
