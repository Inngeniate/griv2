<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");

$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 10)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}

$ls_competencias = '';

$competencias = $db
	->where('activo_cp', 1)
	->orderBy('nombre_cp', 'ASC')
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$ls_competencias .= '<option value="' . $rcp->alias_cp . '">' . $rcp->nombre_cp . '</option>';
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Entrenadores | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		.disabled {
			background-color: #e6e6e6;
		}

		.Content-rescate {
			display: none;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<?php
			//if ($_SESSION['usutipoggri'] == 'administrador') {
			?>
			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="listar" class="activo">Listar Entrenadores</a></li>
						<li><a href="javascript://" id="crear">Crear Entrenadores</a></li>
					</ul>
				</div>
			</div>
			<div class="Contenido-admin-izq listado-usuarios">
				<h2>Listar Entrenadores</h2>
				<hr>
				<p>En esta sección podrás listar y buscar entrenadores.</p>
				<br>
				<form id="buscar">
					<label>Nombre: </label>
					<input type="text" name="nom" placeholder="Nombre">
					<input type="submit" value="Buscar">
				</form>
				<br>
				<div class="Listar-personas">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Apellido</th>
									<th>Competencia</th>
									<th>Estado</th>
									<th>Firma</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody id="resultados">
							</tbody>
						</table>
					</div>
				</div>
				<div id="lista_loader">
					<div class="loader2">Cargando...</div>
				</div>
				<div id="paginacion"></div>
			</div>
			<div class="Contenido-admin-izq crear-usuarios" style="display: none">
				<h2>Crear Entrenador</h2>
				<form id="nuevo-entrenador">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombre" name="entrena[nombre]" required>
							<label>Entrenador <span class="Tipo-entrenador">TA</span> Reg.*</label>
							<input type="text" placeholder="Entrenador Reg." name="entrena[tsa]" required>
							<label>Licencia SST*</label>
							<input type="text" placeholder="Licencia SST" name="entrena[licencia]" required>
							<div class="Content-rescate">
								<label>NFPA PROBOARD NUMBER</label>
								<input type="text" placeholder="NFPA PROBOARD NUMBER" name="entrena[nfpa]">
							</div>
							<label>Firma (imagen)</label>
							<input type="file" id="firma">
						</div>
						<div class="Registro-der">
							<label>Apellido *</label>
							<input type="text" placeholder="Apellido" name="entrena[apellido]" required>
							<label>Estado *</label>
							<select name="entrena[estado]" required>
								<option value="">Seleccione</option>
								<option value="1">Activo</option>
								<option value="0">Inactivo</option>
							</select>
							<label>Competencia*</label>
							<select name="entrena[capacitacion]" class="capacitacion" required="">
								<option value="">Seleccione</option>
								<?php echo $ls_competencias; ?>
							</select>
							<div class="Content-rescate">
								<label>Entrenador ESCON REG</label>
								<input type="text" placeholder="Entrenador ESCON REG" name="entrena[escon_reg]">
							</div>
							<label>Firma por pantalla</label>
							<button type="button" class="placa" id="firma-digital">Firma Entrenador</button>
							<img id="canvasimg" style="display:none;margin:30px;width: 300px">
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
			<?php
			//  } else {
			// 	echo '<p>No tiene permiso para acceder a esta sección.</p>';
			// }
			?>
		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script type="text/javascript" src="js/entrenadores.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
