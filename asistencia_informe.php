<?php
require "libs/conexion.php";
require_once 'libs/tcpdf.php';

class MYPDF extends TCPDF
{
    public function Header()
    {
    }
    public function Footer()
    {
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
$pdf->setPageOrientation('p');

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// PDF_MARGIN_TOP
$pdf->SetMargins(15, 15, 15);
// $pdf->SetPrintHeader(false);
// $pdf->setPrintFooter(false);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(true, 45);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once dirname(__FILE__) . '/lang/eng.php';
    $pdf->setLanguageArray($l);
}

$pdf->AddPage();
$arial = $pdf->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 11);

$nombre_encuestado = '';
$cedula_encuestado = '';
$cargo_encuestado = '';
$firma_encuestado = '';

$nombre_responsable = '';
$cedula_responsable = '';
$cargo_responsable = '';
$firma_responsable = '';

$ls_tomas = '';

$data = $_REQUEST['asistencia'];

$semana = explode(' - ', $data['semana']);
$inicio = explode('/', $semana[0]);
$inicio = $inicio[2] . '-' . $inicio[1] . '-' . $inicio[0];
$fin = explode('/', $semana[1]);
$fin = $fin[2] . '-' . $fin[1] . '-' . $fin[0];

$empleados = $db
    ->where('identificacion_em', $data['usuario'])
    ->objectBuilder()->get('empleados');

if ($db->count == 0) {
    echo 'El usuario no existe';
} else {
    $rem = $empleados[0];

    $asistencias = $db
        ->where('fecha_as', array($inicio . ' 00:00:00', $fin . ' 23:59:00'), 'BETWEEN')
        ->where('Id_em', $rem->Id_em)
        ->orderBy('Id_as', 'ASC')
        ->objectBuilder()->get('asistencias');

    if ($db->count > 0) {
        foreach ($asistencias as $ras) {
            $responsables = $db
                ->where('Id_em', $ras->responsable_as)
                ->objectBuilder()->get('empleados');

            if ($db->count > 0) {
                $resp = $responsables[0];

                $nombre_responsable = $resp->nombre_em;
                $cedula_responsable = $resp->identificacion_em;
                $cargo_responsable = $resp->cargo_em;
                if ($resp->firma_em != '') {
                    $firma_responsable = '<img src="' . $resp->firma_em . '" width="200px">';
                }
            }

            $fechahora = explode(' ', $ras->fecha_as);
            $fecha = $fechahora[0];
            $hora = $fechahora[1];
            $tipo = '';

            switch ($ras->tipo_as) {
                case 'E':
                    $tipo = 'Ingreso';
                    break;
                case 'D':
                    $tipo = 'Durante';
                    break;
                case 'S':
                    $tipo = 'Salida';
                    break;
            }

            $temperatura = ($ras->temperatura_as != '' ?  $ras->temperatura_as . ' °C' : "");

            $ls_tomas .= '<tr>
                            <td align="center">' . date('d-m-Y', strtotime($fecha)) . '</td>
                            <td align="center">' . date('h:i A', strtotime($hora)) . '</td>
                            <td align="center">' . $temperatura . '</td>
                            <td align="center">' . $tipo . '</td>
                        </tr>';
        }

        $asistencias = $db
            ->where('fecha_as', array($inicio . ' 00:00:00', $fin . ' 23:59:00'), 'BETWEEN')
            ->where('Id_em', $rem->Id_em)
            ->orderBy('Id_as', 'ASC')
            ->objectBuilder()->get('asistencias');

        $ra = $asistencias[0];

        $empleado = $db
            ->where('Id_em', $ra->Id_em)
            ->objectBuilder()->get('empleados');

        $re = $empleado[0];
        $nombre_encuestado = $re->nombre_em;
        $cedula_encuestado = $re->identificacion_em;
        $cargo_encuestado = $re->cargo_em;

        $firma_encuestado = '<img src="' . $ra->firma_as . '" width="200px">';
    } else {
        echo 'No existen registros para la semana: ' . $data['semana'] . ' para el usuario: ' . $data['usuario'];
        exit;
    }


    $estilo = '<style>
            table{
                font-size: 12px;
            }
        </style>';

    $html = '<table cellspacing="0" cellpadding="4" border="0" >
            <tr>
                <td align="center"  width="150"><br><img src="images/logogri3.png" width="150"></td>
                <td width="400" align="center"><br><br><br><strong>ENCUESTA, CONSULTA DE ESTADO DE SALUD DEL EMPLEADO</strong></td>
                <td align="center" width="100"><br><br><br><strong>F COV 008</strong></td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
        </table>';

    $html .= '<table cellspacing="0" cellpadding="4" border="1" >
            <tr>
                <td><strong>Nombres y apellidos:</strong></td>
                <td colspan="3">' . $nombre_encuestado . '</td>
            </tr>
            <tr>
                <td><strong>Cedula:</strong></td>
                <td>' . $cedula_encuestado  . '</td>
                <td width="50"><strong>Cargo:</strong></td>
                <td width="279">' . $cargo_encuestado . '</td>
            </tr>';

    if ($rem->tipo_em == 'E') {

        $html .= '<tr>
                <td colspan="4"><strong>Firma del Trabajador:</strong><br><br>' . $firma_encuestado . '</td>
            </tr>';
    }
    $html .= '<tr>
                <td colspan="4"></td>
            </tr>
        </table>';

    if ($rem->tipo_em == 'E') {
        $html .= '<table cellspacing="0" cellpadding="4" border="1" >
                <tr>
                    <td align="center" colspan="4"><strong>Datos De Quien Realiza La Encuesta</strong></td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td><strong>Nombres y apellidos:</strong></td>
                    <td colspan="3">' . $nombre_responsable . '</td>
                </tr>
                <tr>
                    <td><strong>Cedula:</strong></td>
                    <td>' . $cedula_responsable . '</td>
                    <td width="50"><strong>Cargo:</strong></td>
                    <td width="279">' . $cargo_responsable . '</td>
                </tr>
                <tr>
                    <td colspan="4"><strong>Firma del Responsable de la Encuesta:</strong><br><br>' . $firma_responsable . '</td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                </tr>
            </table>';
    }

    $html .= '<table cellspacing="0" cellpadding="4" border="1" >
            <tr>
                <td align="center"><strong>Fecha</strong></td>
                <td align="center"><strong>Hora</strong></td>
                <td align="center"><strong>Temperatura</strong></td>
                <td align="center"><strong>Tipo</strong></td>
            </tr>
            ' . $ls_tomas . '
            <tr>
                <td colspan="4"></td>
            </tr>
        </table>';

    $encuesta = $db
        ->where('fecha_ae', array($inicio, $fin), 'BETWEEN')
        ->where('Id_em', $rem->Id_em)
        ->groupBy('fecha_ae')
        ->orderBy('Id_ae', 'ASC')
        ->objectBuilder()->get('asistencias_encuesta');

    if ($db->count > 0) {
        foreach ($encuesta as $ren) {
            $html .= '<table cellspacing="0" cellpadding="4" border="1" >
                    <tr>
                        <td colspan="3" align="center"><strong>Fecha encuesta: ' . $ren->fecha_ae . '</strong></td>
                    </tr>
                </table>';

            $html .= '<table cellspacing="0" cellpadding="4" border="1" >
                    <tr>
                        <td align="center"><strong>Preguntas</strong></td>
                        <td align="center"><strong>Puntaje</strong></td>
                        <td align="center"><strong>Respuesta</strong></td>
                    </tr>
                </table>';

            $preguntas = $db
                ->objectBuilder()->get('preguntas_covid');

            foreach ($preguntas as $rpr) {
                $respuestas = $db
                    ->where('fecha_ae', $ren->fecha_ae)
                    ->where('Id_em', $rem->Id_em)
                    ->where('pregunta_ae', $rpr->Id_pc)
                    ->orderBy('Id_ae', 'ASC')
                    ->objectBuilder()->get('asistencias_encuesta');

                $res = $respuestas[0];

                $html .= '<table cellspacing="0" cellpadding="4" border="1" >
                        <tr>
                            <td>' . $rpr->pregunta_pc . '</td>
                            <td align="center">' . $rpr->puntaje_pc . ' Punto</td>
                            <td align="center">' . $res->respuesta_ae . '</td>
                        </tr>
                    </table>';
            }
        }
    }

    $pdf->writeHTML($estilo . $html, true, false, false, false, '');

    $pdf->SetMargins(0, 0, 0);
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);

    $pdf->Output('ENCUESTA_ESTADO_SALUD.pdf', 'I');
}
