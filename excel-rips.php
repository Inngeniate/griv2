<?php
require "libs/conexion.php";
$data = $_REQUEST['registro'];

if (empty($data['codigo'])) {
    $data['codigo'] = '%';
} else {
    $fichas = $db
        ->where('codigo', $data['codigo'])
        ->objectBuilder()->get('fichas_ministerio');

    $res = $fichas[0];
    $data['codigo'] = $res->Id;
}

// $inicio = $data['inicio'];
// $fin    = $data['fin'];

// if (empty($inicio)) {
//     $inicio = '1999-01-01';
// }
// if (empty($fin)) {
//     $fin = '3000-01-01';
// }

$bus = $db
    ->where('codigo_ficha', $data['codigo'])
    ->where('(capacitacion = "ALTURAS" OR capacitacion = "ESPACIOS CONFINADOS" OR capacitacion = "ALTURAS RES. 4272")')
    ->orderBy('Id', 'DESC')
    ->objectBuilder()->get('registros');

$res = $db->count;

if ($res > 0) {
    if (PHP_SAPI == 'cli') {
        die('Este archivo solo se puede ver desde un navegador web');
    }

    $res = $bus[0];

    $tipo = '';

    $fichas = $db
        ->where('Id', $res->codigo_ficha)
        ->objectBuilder()->get('fichas_ministerio');

    if ($db->count > 0) {
        $rsficha = $fichas[0];

        if ($rsficha->competencia == 6) {
            $tipo = '4272';
        }
    }

    require_once 'libs/PHPExcel/PHPExcel.php';

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("")
        ->setLastModifiedBy("")
        ->setTitle("Informe de Registros")
        ->setSubject("Informe de Registros excel")
        ->setDescription("Informe de Registros")
        ->setKeywords("Informe de Registros")
        ->setCategory("Reporte excel");

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Tipo de documento')
        ->setCellValue('B1', 'Documento')
        ->setCellValue('C1', 'Primer Nombre')
        ->setCellValue('D1', 'Segundo Nombre')
        ->setCellValue('E1', 'Primer Apellido')
        ->setCellValue('F1', 'Segundo Apellido')
        ->setCellValue('G1', 'Genero')
        ->setCellValue('H1', 'País nacimiento')
        ->setCellValue('I1', 'Fecha nacimiento')
        ->setCellValue('J1', 'Nivel educativo')
        ->setCellValue('K1', 'Area de trabajo')
        ->setCellValue('L1', 'Cargo actual');


    if ($tipo == '4272') {
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('M1', 'Sector')
            ->setCellValue('N1', 'Empleador')
            ->setCellValue('O1', 'ARL');
    }

    $j = 2;

    foreach ($bus as $fila) {
        // $nombres = explode(' ', trim($fila->nombres));
        $apellidos = explode(' ', trim($fila->apellidos));
        $genero = $fila->sexo;

        $primer_nombre = '';
        $segundo_nombre = '';
        $primer_apellido = '';
        $segundo_apellido = '';

        $primer_nombre = $fila->nombre_primero;
        $segundo_nombre = $fila->nombre_segundo;
        $primer_apellido = $apellidos[0];

        // if (count($nombres) > 1) {
        //     $segundo_nombre = $nombres[1];
        // }

        if (count($apellidos) > 1) {
            $segundo_apellido = $apellidos[1];
        }

        if ($fila->capacitacion == 'ALTURAS' || $fila->capacitacion == 'ALTURAS RES. 4272') {
            $alturas = $db
                ->where('documento_al', $fila->numero_ident)
                ->where('codigo_ficha', $fila->codigo_ficha)
                ->orderBy('Id_al', 'DESC')
                ->objectBuilder()->get('registros_alturas');

            if ($db->count > 0) {
                $ral = $alturas[0];
                $pais = '';

                $nacionalidades = $db
                    ->where('nombre', $ral->nacionalidad_al)
                    ->objectBuilder()->get('nacionalidades');

                if ($db->count > 0) {
                    $rn = $nacionalidades[0];
                    $pais = $rn->pais;
                }

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $ral->tipo_doc_al)
                    ->setCellValue('B' . $j, $ral->documento_al)
                    ->setCellValue('C' . $j, $primer_nombre)
                    ->setCellValue('D' . $j, $segundo_nombre)
                    ->setCellValue('E' . $j, $primer_apellido)
                    ->setCellValue('F' . $j, $segundo_apellido)
                    ->setCellValue('G' . $j, $genero)
                    ->setCellValue('H' . $j, $pais)
                    ->setCellValue('I' . $j, $ral->fnacimiento_al)
                    ->setCellValue('J' . $j, $ral->nv_educativo_al)
                    ->setCellValue('K' . $j, $ral->sector_al)
                    ->setCellValue('L' . $j, $ral->profesion_al);

                if ($tipo == '4272') {
                    $empleador = '';
                    $arl = '';

                    $clientes = $db
                        ->where('Id', $ral->cliente)
                        ->objectBuilder()->get('clientes');

                    if ($db->count > 0) {
                        $rcl = $clientes[0];
                        $empleador = $rcl->razon;
                        $arl = $rcl->arl;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('M' . $j, $ral->sector_al)
                        ->setCellValue('N' . $j, $empleador)
                        ->setCellValue('O' . $j, $arl);
                }

                $j++;
            }
        }

        if ($fila->capacitacion == 'ESPACIOS CONFINADOS') {
            $alturas = $db
                ->where('documento_al', $fila->numero_ident)
                ->orderBy('Id_al', 'DESC')
                ->objectBuilder()->get('registros_espacios_confinados');

            if ($db->count > 0) {
                $ral = $alturas[0];
                $pais = '';

                $nacionalidades = $db
                    ->where('nombre', $ral->nacionalidad_al)
                    ->objectBuilder()->get('nacionalidades');

                if ($db->count > 0) {
                    $rn = $nacionalidades[0];
                    $pais = $rn->pais;
                }

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $ral->tipo_doc_al)
                    ->setCellValue('B' . $j, $ral->documento_al)
                    ->setCellValue('C' . $j, $primer_nombre)
                    ->setCellValue('D' . $j, $segundo_nombre)
                    ->setCellValue('E' . $j, $primer_apellido)
                    ->setCellValue('F' . $j, $segundo_apellido)
                    ->setCellValue('G' . $j, $genero)
                    ->setCellValue('H' . $j, $pais)
                    ->setCellValue('I' . $j, $ral->fnacimiento_al)
                    ->setCellValue('J' . $j, $ral->nv_educativo_al)
                    ->setCellValue('K' . $j, $ral->sector_al)
                    ->setCellValue('L' . $j, $ral->profesion_al);

                $j++;
            }
        }
    }

    $estiloTituloColumnas = array(
        'font' => array(
            'name'  => 'Calibri',
            'bold'  => true,
            'size'  => 11,
            'color' => array(
                'rgb' => 'ffffff',
            ),
        ),
        'fill' => array(
            'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
            'rotation'   => 90,
            'startcolor' => array(
                'rgb' => '6085FC',
            ),
            'endcolor'   => array(
                'argb' => '6085FC',
            ),
        ),
    );

    $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($estiloTituloColumnas);

    for ($i = 'A'; $i <= 'H'; $i++) {
        $objPHPExcel->setActiveSheetIndex(0)
            ->getColumnDimension($i)->setAutoSize(true);
    }

    $objPHPExcel->getActiveSheet()->setTitle('Informe');

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Rips-ministerio.csv"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('php://output');
    exit;
} else {
    print_r('No hay resultados para mostrar');
}
