<?php

if (!isset($_REQUEST['formulario'])) {
    echo 'Error';
} else {
    require "libs/conexion.php";
    require_once 'libs/tcpdf.php';

    class MYPDF extends TCPDF
    {

        public function Header()
        {
            switch ($this->page) {
                case 1:
                    $bMargin         = $this->getBreakMargin();
                    $auto_page_break = $this->AutoPageBreak;
                    $this->SetAutoPageBreak(false, 0);
                    $this->setPageMark();
                    $this->SetMargins(5, 5, 5, true);
                    break;
                case 2:
                    global $imgfondo;
                    $bMargin         = $this->getBreakMargin();
                    $auto_page_break = $this->AutoPageBreak;
                    $this->SetAutoPageBreak(false, 0);
                    $this->setPageMark();
                    $this->SetMargins(5, 5, 5, true);
                    break;
            }
        }
    }

    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, 'cm', 'LETTER', true, 'UTF-8', false);
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(1.5, 1.5, 1.5, true);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->setImageScale(1.53);
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once dirname(__FILE__) . '/lang/eng.php';
        $pdf->setLanguageArray($l);
    }

    $form = $_REQUEST['formulario'];

    // $form = 1;

    $registro = $db
        ->where('Id_as', $form)
        ->objectBuilder()->get('aspirantes');

    $rs = $registros[0];

    $an = explode('-', $rs->registro);
    $an = $an[0];

    $TL = $CC = '';

    if ($rs->programa == 'CC') {
        $CC = 'X';
    } else if ($rs->programa == 'TL') {
        $TL = 'X';
    }

    $n_inscripcion = $rs->Id_as;
    $periodo       = $rs->periodo;

    $horario = '';

    if ($rs->horario == 'N') {
        $horario = 'NOCTURNO';
    } else {
        $horario = 'SABATINO';
    }

    $apellido1 = $apellido2 = '';

    $apelidos  = explode(' ', $rs->aspirante_apellidos);
    $apellido1 = $apelidos[0];
    if (count($apelidos) > 1) {
        $apellido2 = $apelidos[1];
    }

    $nombre = $rs->aspirantes_nombres;

    $ID_CC = $ID_TI = $ID_RC = $ID_CE = '';

    switch ($rs->tipo_identificacion) {
        case 'CC':
            $ID_CC = 'X';
            break;
        case 'TI':
            $ID_TI = 'X';
            break;
        case 'RC':
            $ID_RC = 'X';
            break;
        case 'CE':
            $ID_CE = 'X';
            break;
    }

    $identificacion = $rs->numero_identificacion;

    $f_nacimiento = explode('-', $rs->fecha_nacimiento);
    $n_dia        = str_split($f_nacimiento[2]);
    $n_dia1       = $n_dia[0];
    $n_dia2       = $n_dia[1];
    $n_mes        = str_split($f_nacimiento[1]);
    $n_mes1       = $n_mes[0];
    $n_mes2       = $n_mes[1];
    $n_ano        = $f_nacimiento[0];

    $lugar_nacimiento  = explode(' - ', $rs->lugar_nacimiento);
    $lugar_nacimiento1 = $lugar_nacimiento[0];
    $lugar_nacimiento2 = $lugar_nacimiento[1];
    if ($lugar_nacimiento2 == 'Bogotá D.C.') {
        $lugar_nacimiento2 = 'Cundinamarca';
    }

    $S_M = $S_F = '';

    if ($rs->genero == 'M') {
        $S_M = 'X';
    } else {
        $S_F = 'X';
    }

    $EC_soltero = $EC_casado = $EC_libre = $EC_otro = $EC_onombre = '';

    switch ($rs->estado_civil) {
        case 'S':
            $EC_soltero = 'X';
            break;
        case 'C':
            $EC_casado = 'X';
            break;
        case 'UL':
            $EC_libre = 'X';
            break;
        case 'V':
            $EC_otro    = 'X';
            $EC_onombre = 'Viudo';
            break;
        case 'SP':
            $EC_otro    = 'X';
            $EC_onombre = 'Separado';
            break;
    }

    $direc_actual = $rs->aspirante_direccion;
    $mun_actual   = explode(' - ', $rs->aspirante_ciudad);
    $mun_actual   = $mun_actual[1];
    if ($mun_actual == 'Bogotá D.C.') {
        $mun_actual = 'Cundinamarca';
    }

    $tel_actual  = $rs->aspirante_telefono;
    $cel_actual  = $rs->aspirante_celular;
    $mail_actual = $rs->aspirante_correo;

    $parentesco = '';

    switch ($rs->responsable_parentesco) {
        case 'AA':
            $parentesco = 'Acudiente Amigo';
            break;
        case 'AO':
            $parentesco = 'Acudiente Otro';
            break;
        case 'C':
            $parentesco = 'Conyugue';
            break;
        case 'H':
            $parentesco = 'Hermano (a)';
            break;
        case 'M':
            $parentesco = 'Madre';
            break;
        case 'P':
            $parentesco = 'Padre';
            break;
    }

    $respon_nombres   = $rs->responsable_nombres;
    $respon_apellidos = $rs->responsable_apellidos;
    $respon_telefono  = $rs->responsable_telefono;
    $respon_celular   = $rs->responsable_celular;
    $respon_mail      = $rs->responsable_correo;

    $grado_colombia = $rs->graduado_colombia;

    if ($grado_colombia == 'S') {
        $grado_colombia = 'SI';
    } else {
        $grado_colombia = 'NO';
    }

    $ano_graduacion = $rs->ano_graduacion;
    $grado_obtenido = $rs->grado_obtenido;

    $colegio_grado = $rs->colegio_graduacion;
    $ciudad_grado  = explode(' - ', $rs->ciudad_graduacion);
    $ciudad_grado  = $ciudad_grado[1];
    if ($ciudad_grado == 'Bogotá D.C.') {
        $ciudad_grado = 'Cundinamarca';
    }

    $bachiller = $rs->bachiller;

    $financiacion = '';

    switch ($rs->financiacion) {
        case '1':
            $financiacion = 'Ud. Mismo Semestre';
            break;
        case '2':
            $financiacion = 'Sus Padres';
            break;
        case '3':
            $financiacion = 'Otro Familiar';
            break;
        case '4':
            $financiacion = 'Cred. FUKL';
            break;
        case '5':
            $financiacion = 'Cred. ICETEX';
            break;
        case '6':
            $financiacion = 'Entidad Financiera';
            break;
        case '7':
            $financiacion = 'Apoyo de la Empresa';
            break;
        case '8':
            $financiacion = 'Otro';
            break;
    }

    $medio = $rs->medio_informacion;

    $content = '<table>
    				<tr>
    					<td align="center">Resolucion Nº 1500-56.03 1795 de la secretaria de eduacion municipal villavicencio Meta.</td>
    				</tr>
    				<tr>
                        <td><div style="font-size:5pt">&nbsp;</div></td>
                    </tr>
    				<tr>
    					<td align="center">NOTA: POR FAVOR LEA DETENIDAMENTE TODO LOS CAMPOS ANTES DE DILIGENCIAR EL FORMULARIO, NO SE RECIBIRA EN CASO DE ENCONTRAR TACHONES O ENMENDADURAS EN LOS CAMPOS QUE NO REQUIERA DE SU DILIGENCIAMIENTO DE SU PARTE POR FAVOR ESCRIBA “NA” (NO APLICA)</td>
    				</tr>
    				<tr>
                        <td><div style="font-size:10pt">&nbsp;</div></td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5">
                    <tr>
                    	<td width="60">AÑO</td>
                    	<td style="border:1px solid #000" align="center">' . $an . '</td>
                    	<td width="60"></td>
                    	<td width="175">SOLICITUD DE INSCRIPCION No.</td>
                    	<td width="175" style="border:1px solid #000" align="center">' . $n_inscripcion . '</td>
                    	<td width="85"></td>
                    	<td width="125px" style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000"></td>
                    </tr>
                    <tr>
                        <td colspan="6"><div style="font-size:4pt">&nbsp;</div></td>
                        <td width="125px" style="border-left:1px solid #000;border-right:1px solid #000"></td>
                    </tr>
                    <tr>
                    	<td colspan="2">INFORMACION PROGRAMA</td>
                    	<td colspan="4"></td>
                    	<td width="125px" style="border-left:1px solid #000;border-right:1px solid #000"></td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="50px"></td>
                    	<td width="50px" align="center" style="border:1px solid #000">' . $TL . '</td>
                    	<td width="220">TECNICO LABORAL POR COMPETENCIAS</td>
                    	<td width="50px" align="center" style="border:1px solid #000">' . $CC . '</td>
                    	<td width="225">CURSOS CORTOS EDUCACION INFORMAL</td>
                    	<td width="75px"></td>
                    	<td width="125px" style="border-left:1px solid #000;border-right:1px solid #000"></td>
                    </tr>
                    <tr>
                        <td colspan="6" width="670px"><div style="font-size:10pt">&nbsp;</div></td>
                        <td width="125px" align="center" style="border-left:1px solid #000;border-right:1px solid #000">FOTO 3X4</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="50px"></td>
                    	<td>PERIODO</td>
                    	<td width="160">' . $periodo . '</td>
                    	<td width="100">HORARIO</td>
                    	<td width="150">' . $horario . '</td>
                    	<td colspan="2" width="109.3"></td>
                    	<td width="125px" align="center" style="border-left:1px solid #000;border-right:1px solid #000;border-bottom:1px solid #000">FONDO AZUL O BLANCO</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td>INFORMACION PERSONAL</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="250" style="border-top: 1px solid #000;border-left: 1px solid #000">PRIMER APELLIDO</td>
                    	<td width="250" style="border-top: 1px solid #000">SEGÚNDO APELLIDO</td>
                    	<td width="300" style="border-top: 1px solid #000;border-right: 1px solid #000">NOMBRES</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-bottom: 1px solid #000">' . $apellido1 . '</td>
                    	<td style="border-bottom: 1px solid #000">' . $apellido2 . '</td>
                    	<td style="border-right: 1px solid #000;border-bottom: 1px solid #000">' . $nombre . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="215" style="border-left: 1px solid #000">DOCUMENTO DE IDENTIFICACION</td>
                    	<td width="35">C.C</td>
                    	<td width="35" align="center" style="border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000">' . $ID_CC . '</td>
                    	<td width="35">T.I</td>
                    	<td width="35" align="center" style="border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000">' . $ID_TI . '</td>
                    	<td width="35">R.C</td>
                    	<td width="35" align="center" style="border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000">' . $ID_RC . '</td>
                    	<td width="35">C.E</td>
                    	<td width="35" align="center" style="border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000">' . $ID_CE . '</td>
                    	<td width="35"></td>
                    	<td width="270" style="border-left: 1px solid #000;border-right: 1px solid #000">NUMERO</td>
                    </tr>
                    <tr>
                    	<td colspan="10" style="border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000"></td>
                    	<td style="border-right: 1px solid #000;border-bottom: 1px solid #000">' . $identificacion . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="320" style="border-left: 1px solid #000">FECHA Y LUGAR DE NACIMIENTO</td>
                    	<td width="210" style="border-left: 1px solid #000;border-right: 1px solid #000">CIUDAD</td>
                    	<td width="170" style="border-right: 1px solid #000">DEPARTAMENTO</td>
                    	<td width="100" style="border-right: 1px solid #000">SEXO</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="35" style="border-left: 1px solid #000;border-bottom: 1px solid #000;">DIA</td>
                    	<td width="25" style="border-left: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $n_dia1 . '</td>
                    	<td width="25" style="border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $n_dia2 . '</td>
                    	<td width="20" style="border-bottom: 1px solid #000;"></td>
                    	<td width="40" style="border-bottom: 1px solid #000;">MES</td>
                    	<td width="25" style="border-left: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $n_mes1 . '</td>
                    	<td width="25" style="border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $n_mes2 . '</td>
                    	<td width="50" style="border-bottom: 1px solid #000;">AÑO</td>
                    	<td width="50" style="border-left: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000;">' . $n_ano . '</td>
                    	<td width="25" style="border-left: 1px solid #000;border-right: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000;"></td>
                    	<td width="210" style="border-bottom: 1px solid #000;border-right: 1px solid #000">' . $lugar_nacimiento1 . '</td>
                    	<td width="170" style="border-bottom: 1px solid #000;border-right: 1px solid #000">' . $lugar_nacimiento2 . '</td>
                    	<td width="25" style="border-right: 1px solid #000;border-bottom: 1px solid #000;">M</td>
                    	<td width="25" style="border-right: 1px solid #000;border-bottom: 1px solid #000;border-top: 1px solid #000;">' . $S_M . '</td>
                    	<td width="25" style="border-right: 1px solid #000;border-bottom: 1px solid #000;">F</td>
                    	<td width="25" style="border-right: 1px solid #000;border-bottom: 1px solid #000;border-top: 1px solid #000;">' . $S_F . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="800" colspan="12" style="border-left: 1px solid #000;border-right: 1px solid #000">ESTADO CIVIL</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-bottom: 1px solid #000;">SOLTERO</td>
                    	<td width="25" style="border-left: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $EC_soltero . '</td>
                    	<td style="border-bottom: 1px solid #000;">CASADO</td>
                    	<td width="25" style="border-left: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $EC_casado . '</td>
                    	<td width="80" style="border-bottom: 1px solid #000;">UNION LIBRE</td>
                    	<td width="25" style="border-left: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $EC_libre . '</td>
                    	<td width="60" style="border-bottom: 1px solid #000;">OTRO</td>
                    	<td width="25" style="border-left: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-bottom: 1px solid #000;">' . $EC_otro . '</td>
                    	<td style="border-bottom: 1px solid #000;">¿Cuál?</td>
                    	<td width="180" style="border-bottom: 1px solid #000;">' . $EC_onombre . '</td>
                    	<td width="100" style="border-bottom: 1px solid #000;">No DE HIJOS</td>
                    	<td width="80" style="border-right: 1px solid #000;border-bottom: 1px solid #000;"></td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td><br><br>INFORMACION DE CONTACTO</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="400" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">DIRECCION ACTUAL</td>
                    	<td width="400" style="border-top: 1px solid #000;border-right: 1px solid #000;">MUNICIPIO</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $direc_actual . '</td>
                    	<td style="border-right: 1px solid #000;border-bottom: 1px solid #000">' . $mun_actual . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="250" style="border-left: 1px solid #000;border-right: 1px solid #000;">TELEFONO</td>
                    	<td width="250" style="border-right: 1px solid #000;">CELULAR</td>
                    	<td width="300" style="border-right: 1px solid #000">E-MAIL</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $tel_actual . '</td>
                    	<td style="border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $cel_actual . '</td>
                    	<td style="border-right: 1px solid #000;border-bottom: 1px solid #000">' . $mail_actual . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td><br><br>INFORMACION ACUDIENTE</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="250" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">PARENTESCO</td>
                    	<td width="250" style="border-top: 1px solid #000;border-right: 1px solid #000;">NOMBRES</td>
                    	<td width="300" style="border-top: 1px solid #000;border-right: 1px solid #000">APELLIDOS</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-right: 1px solid #000;">' . $parentesco . '</td>
                    	<td style="border-right: 1px solid #000;">' . $respon_nombres . '</td>
                    	<td style="border-right: 1px solid #000;">' . $respon_apellidos . '</td>
                    </tr>
                    <tr>
                    	<td width="250" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">TELEFONO</td>
                    	<td width="250" style="border-top: 1px solid #000;border-right: 1px solid #000;">CELULAR</td>
                    	<td width="300" style="border-top: 1px solid #000;border-right: 1px solid #000">E-MAIL</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $respon_telefono . '</td>
                    	<td style="border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $respon_celular . '</td>
                    	<td style="border-right: 1px solid #000;border-bottom: 1px solid #000">' . $respon_mail . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td><br><br>INFORMACION ACADEMICA</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="250" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">GRADUADO EN COLOMBIA </td>
                    	<td width="250" style="border-top: 1px solid #000;border-right: 1px solid #000;">AÑO DE GRADUACION</td>
                    	<td width="300" style="border-top: 1px solid #000;border-right: 1px solid #000">GRADO OBTENIDO</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-right: 1px solid #000;">' . $grado_colombia . '</td>
                    	<td style="border-right: 1px solid #000;">' . $ano_graduacion . '</td>
                    	<td style="border-right: 1px solid #000;">' . $grado_obtenido . '</td>
                    </tr>
                    <tr>
                    	<td width="250" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">COLEGIO</td>
                    	<td width="250" style="border-top: 1px solid #000;border-right: 1px solid #000;">CIUDAD</td>
                    	<td width="300" style="border-top: 1px solid #000;border-right: 1px solid #000">TIPO DE BACHILLER</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $colegio_grado . '</td>
                    	<td style="border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $ciudad_grado . '</td>
                    	<td style="border-right: 1px solid #000;border-bottom: 1px solid #000">' . $bachiller . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                   <tr>
                    	<td><br><br>INFORMACION ADICIONAL</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                    <tr>
                    	<td width="400" style="border-top: 1px solid #000;border-left: 1px solid #000;border-right: 1px solid #000;">FUENTE DE FINANCIACION</td>
                    	<td width="400" style="border-top: 1px solid #000;border-right: 1px solid #000;">MEDIO DE INFORMACION</td>
                    </tr>
                    <tr>
                    	<td style="border-left: 1px solid #000;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . $financiacion . '</td>
                    	<td style="border-right: 1px solid #000;border-bottom: 1px solid #000">' . $medio . '</td>
                    </tr>
    			</table>';

    $content .= '<table cellpadding="5" border="0">
                   <tr>
                    	<td align="center"><br><br>MANIFIESTO QUE LOS DATOS AQUÍ CONSIGNADOS CORRESPONDEN A LA REALIDAD Y QUE LA SUSCRIPCION DEL PRESENTE FORMATO, NO IMPLICA MI VINCULACION AL INSTITUTO EDUCATIVO PARA EL TALENTO Y DESARROLLO HUMANO GRI COMPANY SAS.</td>
                    </tr>
    			</table>';

    $pdf->AddPage();
    $fontname  = $pdf->addTTFfont('libs/fonts/calibri.ttf', 'TrueTypeUnicode', '', 32);
    $fontnameb = $pdf->addTTFfont('libs/fonts/calibrib.ttf', 'TrueTypeUnicode', '', 32);
    $pdf->SetFont($fontname, '', 8);

    $pdf->writeHTML($content, true, false, false, false, '');

    $pdf->Output('formulario-registro.pdf', 'I');
}
