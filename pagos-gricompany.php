<?php
require("libs/conexion.php");
$ls_cursos = '';

$cursos = $db
	->orderBy('nombre', 'ASC')
	->objectBuilder()->get('precios');

foreach ($cursos as $curso) {
	$ls_cursos .= '<option value="' . $curso->Id . '">' . $curso->nombre . '</option>';
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/msj.css" />
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.label-check {
			/* display: inline-block; */
			float: left;
		}

		.Error-espacio {
			border: 2px solid red !important;
		}

		input[type="checkbox"] {
			/* display: inline-block; */
			width: 20px;
			height: 20px;
			float: left;
			margin: 0 10px 0px 20px;
		}

		.Valor-curso,
		a {
			color: #000;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<link rel="stylesheet" href="css/menu.css" />
			<div class="Top">
				<div class="Top-izq">
					<a href="/"><img src="images/logo.png" alt="Logo Gricompany" style="width:120px"></a>
				</div>
			</div>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Crear Registro</h2>
				<form id="registro-pago">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombre" name="curso[nombre]" class="Nombre-primero required" required>
							<label>Numero de identificación *</label>
							<input type="text" placeholder="Numero de identificación" class="correo required" name="curso[identificacion]" required>
							<label>Correo *</label>
							<input type="email" placeholder="Correo" class="correo required" name="curso[correo]" required>
							<label>Curso *</label>
							<select name="curso[curso]" class="Sel-curso required" required>
								<option value="">Seleccione</option>
								<?php echo $ls_cursos ?>
							</select>
						</div>
						<div class="Registro-der">
							<label>Apellidos *</label>
							<input type="text" placeholder="Apellidos" name="curso[apellido]" required class="Apellidos">
							<label>Teléfono </label>
							<input type="text" placeholder="Teléfono" name="curso[telefono]" class="Cargo">
							<label>Dirección *</label>
							<input type="text" placeholder="Dirección" class="direccion required" name="curso[direccion]" required>
							<label>Precio Curso</label>
							<span class="Valor-curso">$ 0</span>
						</div>
						<div class="Registro-der" style="text-align: left;">
							<input type="checkbox" name="curso[check]" class="check required"><a href="https://gricompany.co/wp-content/uploads/2021/10/POLITICA-DE-TRATAMIENTO-DE-DATOS.pdf" target="_blank">Acepta términos y condiciones</a>
						</div>
						<div class="Registro-der"></div>
						<br>
						<br>
						<input type="submit" value="Realizar pago">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/wNumb.js"></script>
	<script type="text/javascript" src="https://checkout.epayco.co/checkout.js"></script>
	<script type="text/javascript" src="js/pagos.js?<?php echo time()  ?>"></script>
</body>

</html>
