<div class="ensayo">
	<div class="Contenido-admin-izq">
		<h2>Listar Certificados</h2>
		<form id="buscar1">
			<label>N° Reporte: </label>
			<input type="text" name="certifica[reporte]" id="breporte" placeholder="N° Reporte">
			<label>Placa: </label>
			<input type="text" name="certifica[placa]" id="bplaca" placeholder="Placa">
			<input type="hidden" name="certifica[accion]" value="lista_ensayo">
			<input type="hidden" name="pagina" value="0">
			<input type="submit" value="Buscar">
		</form>
		<div class="Listar-personas">
			<div class="Tabla-listar">
				<table>
					<thead>
						<tr>
							<th>N° Reporte</th>
							<th>Fecha</th>
							<th>Placa</th>
							<th>Propietario</th>
							<th>Marca</th>
							<th>Modelo</th>
							<th>Tipo</th>
							<th>Vendedor</th>
							<th>Editar</th>
							<th>Eliminar</th>
						</tr>
					</thead>
					<tbody id="resultados1">
					</tbody>
				</table>
			</div>
		</div>
		<div id="lista_loader1">
			<div class="loader2">Cargando...</div>
		</div>
		<div id="paginacion" class="pagina1"></div>
	</div>
</div>
