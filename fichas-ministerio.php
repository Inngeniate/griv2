<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");


$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 19)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}

$ls_competencias = '';

$competencias = $db
	->where('activo_cp', 1)
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$ls_competencias .= '<option value="' . $rcp->Id_cp . '" data-alias="' . $rcp->alias_cp . '">' . $rcp->nombre_cp . '</option>';
}

$ls_entrenadores = '';

$entrenadores = $db
	->where('estado_en', 1)
	->orderBy('nombre_en', 'ASC')
	->objectBuilder()->get('entrenadores');

foreach ($entrenadores as $rsc) {
	$ls_entrenadores .= '<option value="' . $rsc->Id_en . '" data-tipo="' . $rsc->competencia_en . '" >' . $rsc->nombre_en . ' ' . $rsc->apellido_en . '</option>';
}


$ls_cursos = '';

$cursos = $db
	->where('activo_ct', 1)
	->orderBy('nombre', 'ASC')
	->objectBuilder()->get('certificaciones');

foreach ($cursos as $rsc) {
	$ls_cursos .= '<option value="' . $rsc->Id_ct . '" data-competencia="' . $rsc->competencia_ct . '" style="display:none">' . $rsc->nombre . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Administración Fichas Ministerio | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.Sel-entrenador option:not(:first-child) {
			display: none;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">

			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="listar" class="activo">Listado</a></li>
						<li><a href="javascript://" id="crear">Crear</a></li>
					</ul>
				</div>
			</div>
			<div class="Contenido-admin-izq listado-usuarios">
				<h2>Administración Fichas Ministerio</h2>
				<hr>
				<p></p>
				<br>
				<form id="buscar">
					<label>Código: </label>
					<input type="text" name="nom" placeholder="Código">
					<input type="submit" value="Buscar">
				</form>
				<br>
				<div class="Listar-personas">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th>Código</th>
									<th>Competencia</th>
									<th>Entrenador</th>
									<th>Cantidad</th>
									<th>Ciudad</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody id="resultados">
							</tbody>
						</table>
					</div>
				</div>
				<div id="lista_loader">
					<div class="loader2">Cargando...</div>
				</div>
				<div id="paginacion"></div>
			</div>
			<div class="Contenido-admin-izq crear-usuarios" style="display: none">
				<h2>Crear Ficha</h2>
				<form id="registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Competencia *</label>
							<select name="registro[competencia]" class="Sel-competencia" required>
								<option value="">Seleccione</option>
								<?php echo $ls_competencias ?>
							</select>
							<label>Niveles *</label>
							<select name="registro[niveles]" class="Sel-curso" required>
								<option value="">Seleccione</option>
								<?php echo $ls_cursos; ?>
							</select>
							<label>Fecha inicio *</label>
							<input type="date" placeholder="Fecha inicio" name="registro[inicio]" class="Fecha-inicio" required>
							<label>Cantidad *</label>
							<input type="number" step="1" min="1" placeholder="Cantidad" name="registro[cantidad]" class="Cantidad"
							required>
						</div>
						<div class="Registro-der">
							<label>Entrenador *</label>
							<select name="registro[entrenador]" class="Sel-entrenador" required>
								<option value="">Seleccione</option>
								<?php echo $ls_entrenadores ?>
							</select>
							<label>Código *</label>
							<input type="text" placeholder="Código" name="registro[codigo]" required>
							<label>Fecha finalización *</label>
							<input type="date" placeholder="Fecha finalización" name="registro[fin]" class="Fecha-fin" required>
							<label>Ciudad *</label>
							<select name="registro[ciudad]" class="Sel-ciudad" required>
								<option value="">Seleccione</option>
								<option value="cucuta">Cúcuta</option>
								<option value="villavicencio">Villavicencio</option>
							</select>
						</div>
						<hr>
						<br>
						<br>
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>

		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/administrar-fichas-ministerio.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
