<?php
require_once 'libs/conexion.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<!-- <meta name="keywords" lang="es" content="">
		<meta name="robots" content="All"> -->
	<!-- <meta name="description" lang="es" content="Con nuestra aplicación web podrás administrar tus inmuebles y encontrar el arrendatario ideal de una manera mas fácil, económica y confiable."> -->
	<title>Respuesta de transacción | Gricpompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
</head>

<body>
	<header>
		<link rel="stylesheet" href="css/menu.css" />
		<div class="Top">
			<div class="Top-izq">
				<a href="/"><img src="images/logo.png" alt="Logo Gricompany" style="width:120px"></a>
			</div>
		</div>
	</header>
	<section>
		<div class="Contenido-admin" style="opacity: 0">
			<div class="Contenedor-global-pago-int">
				<div class="Contenedor-global-pago-info">
					<div class="Contenedor-table">
						<h2 class="Titulo-gris">Información de la transacción</h2>
						<table class="Contenedor-table-info">
							<tbody>
								<tr>
									<td class="Contenedor-table-derecha Contenedor-table-bold">Referencia:</td>
									<td id="referencia"></td>
								</tr>
								<tr>
									<td class="Contenedor-table-derecha Contenedor-table-bold">Fecha:</td>
									<td id="fecha" class=""></td>
								</tr>
								<tr>
									<td class="Contenedor-table-derecha Contenedor-table-bold">Respuesta:</td>
									<td id="respuesta"></td>
								</tr>
								<tr>
									<td class="Contenedor-table-derecha Contenedor-table-bold">Motivo:</td>
									<td id="motivo"></td>
								</tr>
								<tr>
									<td class="Contenedor-table-derecha Contenedor-table-bold">Banco:</td>
									<td class="" id="banco"></td>
								</tr>
								<tr>
									<td class="Contenedor-table-derecha Contenedor-table-bold">Recibo:</td>
									<td id="recibo"></td>
								</tr>
								<tr>
									<td class="Contenedor-table-derecha Contenedor-table-bold">Total:</td>
									<td class="" id="total"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- Aca finaliza el contenedor de la tabla -->
					<div class="Contenedor-respuesta Oculto">
						<div class="Contenedor-respuesta-info">
							<div id="Contenedor-exitoso">
								<h2 class="Titulo-gris"><span class="Msg-respuesta"></span></h2>
								<div class="Contenedor-respuesta-btn Mob-registro"></div>
							</div>
							<div id="Contenedor-pendiente">
								<div class="Contenedor-respuesta-pendiente">
									<p>Solicitud en espera por respuesta de la entidad bancaria.</p>
								</div>
							</div>
							<div id="Contenedor-negativo">
								<div class="Contenedor-respuesta-negativa">
									<p>No se puede completar la transacción: utiliza otro método de pago.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script>
		function getQueryParam(param) {
			location.search.substr(1)
				.split("&")
				.some(function(item) {
					return item.split("=")[0] == param && (param = item.split("=")[1])
				})
			return param
		}
		$(document).ready(function() {
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' +
				'<div class="la-ball-scale">' +
				'<div></div>' +
				'</div>' +
				'</div>');
			setTimeout(function() {
				$('#fondo').fadeIn('fast');
			}, 400);

			var ref_payco = getQueryParam('ref_payco');

			var urlapp = "https://secure.epayco.co/validation/v1/reference/" + ref_payco;
			$.get(urlapp, function(response) {
				$('#fondo').remove();
				$('.Contenido-admin').css('opacity', 1);
				if (response.success) {
					if (response.data.x_cod_response == 1) {
						//Codigo personalizado
						$('#Contenedor-pendiente').remove();
						$('#Contenedor-negativo').remove();
						$('.Contenedor-respuesta').show();
						// alert("Transaccion Aprobada");
						$.post('libs/acc_pagos', {
							'curso[accion]': 'detalle_registro',
							'curso[transaccion]': response.data.x_transaction_id,
							'curso[franchise]': response.data.x_franchise,
							'curso[referencia]': response.data.x_extra1
						}, function(data) {
							$('.Msg-respuesta').html(data.msg);
						}, 'json');
						console.log('transacción aceptada');
					}
					//Transaccion Rechazada
					if (response.data.x_cod_response == 2) {
						$('#Contenedor-pendiente').remove();
						$('#Contenedor-exitoso').remove();
						$('.Contenedor-respuesta').show();
						console.log('transacción rechazada');
					}
					//Transaccion Pendiente
					if (response.data.x_cod_response == 3) {
						$('#Contenedor-negativo').remove();
						$('#Contenedor-exitoso').remove();
						$('.Contenedor-respuesta').show();
						console.log('transacción pendiente');
					}
					//Transaccion Fallida
					if (response.data.x_cod_response == 4) {
						$('#Contenedor-pendiente').remove();
						$('#Contenedor-exitoso').remove();
						$('.Contenedor-respuesta').show();
						console.log('transacción fallida');
					}
					$('#fecha').html(response.data.x_transaction_date);
					$('#respuesta').html(response.data.x_response);
					$('#referencia').text(response.data.x_id_invoice);
					motivo = response.data.x_response_reason_text.split('-');
					$('#motivo').text(motivo[1]);
					$('#recibo').text(response.data.x_transaction_id);
					$('#banco').text(response.data.x_bank_name);
					$('#autorizacion').text(response.data.x_approval_code);
					$('#total').text(response.data.x_amount + ' ' + response.data.x_currency_code);
				} else {
					alert("Error consultando la información");
				}
			});
		});
	</script>
</body>

</html>
