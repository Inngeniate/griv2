<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");

$ls_menu = '';

$menu = $db
	->where('Id_m', 4, '!=')
	->objectBuilder()->get('menu');

foreach ($menu as $rmn) {
	$ls_menu .= '<tr>
					<td>
						' . $rmn->nombre_m . '
					</td>
					<td>
						<input type="checkbox" name="usuario[permiso][' . $rmn->Id_m . ']" value="1" >
					</td>
				</tr>';
}

$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 9)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		table tbody {
			text-align: left;
		}

		input[type="checkbox"] {
			width: 20px;
			height: 20px;
			margin: 0 10px 0px 20px;
		}

		.Registro input[type="checkbox"]:focus {
			border: none;
			-webkit-box-shadow: none;
			box-shadow: none;
		}
	</style>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="listar" class="activo">Listar Usuarios</a></li>
						<?php
						if ($_SESSION['usutipoggri'] == 'administrador') {
						?>
							<li><a href="javascript://" id="eliminados">Usuarios Eliminados</a></li>
						<?php } ?>
						<li><a href="javascript://" id="crear">Crear Usuarios</a></li>
					</ul>
				</div>
			</div>
			<?php include('listar-usuarios.php') ?>
			<?php include('listar-eliminados.php') ?>
			<?php include('usuarios.php') ?>
		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/usuarios.js?<?php echo time() ?>"></script>
</body>

</html>
