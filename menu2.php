<link rel="stylesheet" href="css/menu.css" />
<div class="Top">
	<div class="Top-izq">
		<a href="javascript://" id="Drop"><img src="images/menu_hamburgesa.png" alt="Menu" style="width:50px"></a>
		<a href="/"><img src="images/logo.png" alt="Logo Gricompany" style="width:120px"></a>
	</div>
</div>
<?php
$listado_menu = '';

$lista_menu = $db
	->where('Id_m', 4, '!=')
	->objectBuilder()->get('menu');

foreach ($lista_menu as $item) {

	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('modulo_up', $item->Id_m)
		->where('permiso_up', 1)
		->objectBuilder()->get('usuarios_permisos');

	if ($db->count > 0) {
		$listado_menu .= '<li class="Cn-mn-ptincipal">
							<a href="' . $item->link_m . '" class="Contenido-sub-menu">' . $item->nombre_m . '</a>
						</li>';
	}
}

// $path = $_SERVER['argv'][1];

$total = 260000;

// $path = 'D:\laragon\www';

$path = realpath($_SERVER["DOCUMENT_ROOT"]);


$sizeInBytes = getFolderSize($path);

function getFolderSize($directory)
{
	$totalSize = 0;
	$directoryArray = scandir($directory);

	foreach ($directoryArray as $key => $fileName) {
		if ($fileName != ".." && $fileName != ".") {
			if (is_dir($directory . "/" . $fileName)) {
				$totalSize = $totalSize + getFolderSize($directory . "/" . $fileName);
			} else if (is_file($directory . "/" . $fileName)) {
				$totalSize = $totalSize + filesize($directory . "/" . $fileName);
			}
		}
	}
	return $totalSize;
}

$en_uso = getFormattedSize($sizeInBytes);
$en_uso = 208000;

$en_uso = round($en_uso * 100 / $total, 2) + 1;

if ($en_uso > 100) {
	$en_uso = 100;
}

$en_uso = $en_uso . '%';

function getFormattedSize($sizeInBytes)
{

	return round($sizeInBytes / (1024 * 1024), 2);

	/* if($sizeInBytes < 1024) {
            return round($sizeInBytes, 2) . " bytes";
        } else if($sizeInBytes < 1024*1024) {
            return round($sizeInBytes/1024, 2) . " KB";
        } else if($sizeInBytes < 1024*1024*1024) {
            return round($sizeInBytes/(1024*1024), 2) . " MB";
        } else if($sizeInBytes < 1024*1024*1024*1024) {
            return round($sizeInBytes/(1024*1024*1024), 2) . " GB";
        } else if($sizeInBytes < 1024*1024*1024*1024*1024) {
            return round($sizeInBytes/(1024*1024*1024*1024), 2) . " TB";
        } else {
            return "Greater than 1024 TB";
        } */
}
?>
<div class="Contenedor-principal-izq">
	<div class="Contenedor-principal-izq-menu">
		<ol class="Contenedor-menu-izq">
			<?php echo $listado_menu ?>
			<li class="Cn-mn-ptincipal">
				<a href="javascript:void(0)" class="Contenido-sub-menu">Uso Disco: <?php echo $en_uso; ?></a>
				<div class="barra-progreso-fondo" data-uso="<?php echo getFormattedSize($sizeInBytes) ?>" data-t="<?php echo $sizeInBytes ?>">
					<div class="barra-progreso-color" style="width:<?php echo $en_uso; ?>"></div>
				</div>
			</li>
			<li class="Cn-mn-ptincipal">
				<a href="libs/logout" class="Contenido-sub-menu">Cerrar Sesión</a>
			</li>
		</ol>
	</div>
</div>
