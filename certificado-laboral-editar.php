<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
require("libs/conexion.php");

$nombre = '';
$identificacion = '';
$lugar_expedicion = '';
$labora_desde = '';
$labora_hasta = '';
$cargo = '';
$salario = '';

$bus = $db
	->where('Id_cl', $_REQUEST['ctr'])
	->objectBuilder()->get('certificados_laborales');

if ($db->count > 0) {
	$nombre = $bus[0]->nombre_cl;
	$identificacion = $bus[0]->identificacion_cl;
	$lugar_expedicion = $bus[0]->expedida_cl;
	$labora_desde = $bus[0]->labora_cl;
	$labora_hasta = $bus[0]->hasta_cl;
	$cargo = $bus[0]->cargo_cl;
	$salario = $bus[0]->salario_cl;
} else {
	header('Location: certificados-laborales.php');
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificado Laboral | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Certificado Laboral - Editar</h2>
				<form id="registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombre" name="registro[nombre]" class="nnumb" value="<?php echo $nombre ?>" required>
							<label>Lugar expedición cedula de ciudadania *</label>
							<input type="text" placeholder="Lugar expedición cedula de ciudadania" name="registro[expedicion]" value="<?php echo $lugar_expedicion ?>" required>
							<label>Labora desde *</label>
							<input type="date" placeholder="Labora desde" name="registro[labora]" value="<?php echo $labora_desde ?>" required>
							<label>Salario *</label>
							<input type="text" placeholder="Salario" name="registro[salario]" class="ntext" value="<?php echo $salario ?>" required>
						</div>
						<div class="Registro-der">
							<label>Documento de Identidad *</label>
							<input type="text" placeholder="Documento de Identidad" name="registro[identificacion]" value="<?php echo $identificacion ?>" class="ntext" required>
							<label>Cargo *</label>
							<input type="text" placeholder="Cargo" name="registro[cargo]" value="<?php echo $cargo ?>" required>
							<label>Laboró hasta</label>
							<input type="date" placeholder="Labora hasta" name="registro[labora_fin]" value="<?php echo $labora_hasta ?>">
						</div>
						<hr>
						<br>
						<br>
						<input type="hidden" name="registro[id]" value="<?php echo $_REQUEST['ctr'] ?>">
						<input type="submit" value="Editar Certificado">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script type="text/javascript" src="js/editar_certificado_laboral.js?<?php echo time()  ?>"></script>
</body>

</html>
