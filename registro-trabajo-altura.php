<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');
require("libs/conexion.php");

$ls_competencias = '';

$competencias = $db
	->where('activo_cp', 1)
	->objectBuilder()->get('competencias');

foreach ($competencias as $rcp) {
	$ls_competencias .= '<option value="' . $rcp->Id_cp . '" data-nombre="' . $rcp->alias_cp . '">' . $rcp->nombre_cp . '</option>';
}


$ls_cursos = '';

$cursos = $db
	->where('activo_ct', 1)
	->orderBy('nombre', 'ASC')
	->objectBuilder()->get('certificaciones');

foreach ($cursos as $rsc) {
	$ls_cursos .= '<option value="' . $rsc->Id_ct . '" data-competencia="' . $rsc->competencia_ct . '" style="display:none">' . $rsc->nombre . '</option>';
}

$ls_sesiones = '';
for ($i = 1; $i < 51; $i++) {
	$ls_sesiones .= '<option value="' . $i . '">' . $i . '</option>';
}

$primerNombre = $segundoNombre = $apellidos = $identificacion = $rh = $telefono = $email = $empresa = '';

if (isset($_REQUEST['registro'])) {
	$registros = $db
		->where('Id', $_REQUEST['registro'])
		->objectBuilder()->get('registros');

	if ($db->count > 0) {
		$rsg = $registros[0];
		$primerNombre = $rsg->nombre_primero;
		$segundoNombre = $rsg->nombre_segundo;
		$apellidos = $rsg->apellidos;
		$identificacion = $rsg->numero_ident;
		$rh = $rsg->rh;
		$telefono = $rsg->telefono;
		$email = $rsg->correo;
		$empresa = $rsg->empresa;
	}
}

$ls_eps = '';
$ls_arl = '';

$entidades = $db
	->objectBuilder()->get('eps');

foreach ($entidades as $rent) {
	$ls_eps .= '<option value="' . $rent->nombre_eps . '">' . $rent->nombre_eps . '</option>';
}

$entidades = $db
	->where('activo_arl', 1)
	->orderBy('orden_arl', 'ASC')
	->objectBuilder()->get('arl');

foreach ($entidades as $rent) {
	$ls_arl .= '<option value="' . $rent->nombre_arl . '">' . $rent->nombre_arl . '</option>';
}


$fecha_actual = date('Y-m-d');
$ls_codigos = '';

$codigos_fichas = $db
	->where('competencia', array(1, 2), 'NOT IN')
	->where('en_uso < cantidad')
	->objectBuilder()->get('fichas_ministerio');

foreach ($codigos_fichas as $rcp) {
	$ciudades = [
		'cucuta' => 'Cúcuta',
		'villavicencio' => 'Villavicencio',
		'' => ''
	];

	$ls_codigos .= '<option value="' . $rcp->Id . '" data-competencia="' . $rcp->competencia . '" data-curso="' . $rcp->niveles . '" data-ciudad="' . $ciudades[$rcp->ciudad] . '">' . $rcp->codigo . '</option>';
}

//* Código infinito alturas4272

$ls_codigos .= '<option value="4272Inf" data-competencia="6" data-curso="0" data-ciudad="0">4272 inf</option>';

// UPDATE registros_alturas SET rh_al = REPLACE(rh_al, '-', '');
// $registros = mysql_query("UPDATE registros_alturas SET rh_al = UPPER(rh_al)");
// select * from registros_alturas WHERE LENGTH(rh_al) = 1
/* $registros = mysql_query("SELECT * FROM registros_alturas WHERE gs_al != '' AND Id_al > 4366 AND Id_al < 7676 ");
while($rsg = mysql_fetch_object($registros)){
	$act = mysql_query("UPDATE registros_alturas SET rh_al = '$rsg->rh_al$rsg->gs_al' WHERE Id_al = '$rsg->Id_al' ");
}
 */


?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/cropper.css" />
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.btn-zoom {
			position: relative;
			right: 40%;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}

		input[type=checkbox] {
			width: auto;
			padding: 0;
			margin: 0 5px 10px 0;
			height: auto;
		}

		ul {
			text-align: left;
			list-style: none;
			color: #000;
		}

		.disabled {
			background-color: #e6e6e6;
		}

		.ui-autocomplete {
			max-height: 200px;
			overflow-y: auto;
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
			/* add padding to account for vertical scrollbar */
			z-index: 1000 !important;
		}

		.Ficha-ciudad-select {
			display: none;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Ficha de Inscripción</h2>
				<form id="registro">
					<div class="Registro">
						<div class="Registro-der">
							<label>Código del ministerio*</label>
							<select name="registro[codigo_ficha]" class="Sel-codigo" required>
								<option value="">Seleccione el código</option>
								<?php echo $ls_codigos; ?>
							</select>
						</div>
						<div class="Registro-der">
						</div>
						<div class="Registro-der">
							<label>Competencia</label>
							<select class="competencia" disabled>
								<option value="">Seleccione</option>
								<?php echo $ls_competencias ?>
							</select>
							<input type="hidden" name="registro[competencia]" class="h-competencia">
						</div>
						<div class="Registro-der">
							<label>Curso</label>
							<select class="curso" disabled>
								<option value="">Seleccione</option>
								<?php echo $ls_cursos; ?>
							</select>
							<input type="hidden" name="registro[curso]" class="h-curso">
						</div>
						<div class="Registro-der">
							<label>Ciudad *</label>
							<input type="text" placeholder="Ciudad" name="registro[ciudad]" class="Ficha-ciudad-input">
							<select class="Ficha-ciudad-select">
								<option value="">Seleccione</option>
								<option value="cucuta">Cúcuta</option>
								<option value="villavicencio" selected>Villavicencio</option>
							</select>
							<label>Fecha de Inicio*</label>
							<input type="date" placeholder="Fecha" name="registro[fecha]" required>
							<label>Segundo Nombre</label>
							<input type="text" placeholder="Segundo Nombre" name="registro[segundo_nombre]" value="<?php echo $segundoNombre ?>">
							<label>País de nacimiento</label>
							<input type="text" placeholder="País de nacimiento" name="registro[nacionalidad]" class="nacionalidad">
							<label>Documento de Identidad *</label>
							<input type="text" placeholder="Documento de Identidad" name="registro[identificacion]" class="ntxt" value="<?php echo $identificacion ?>" required>
							<label>Fecha de Nacimiento*</label>
							<input type="date" placeholder="Fecha de nacimiento" name="registro[fnacimiento]" class="Fecha-nacimiento" required>
							<label>RH </label>
							<select name="registro[rh]">
								<option value="">Seleccione</option>
								<option value="O-" <?php echo ('O-' == $rh ? "selected" : "") ?>>O-</option>
								<option value="O+" <?php echo ('O+' == $rh ? "selected" : "") ?>>O+</option>
								<option value="A-" <?php echo ('A-' == $rh ? "selected" : "") ?>>A-</option>
								<option value="A+" <?php echo ('A+' == $rh ? "selected" : "") ?>>A+</option>
								<option value="B-" <?php echo ('B-' == $rh ? "selected" : "") ?>>B-</option>
								<option value="B+" <?php echo ('B+' == $rh ? "selected" : "") ?>>B+</option>
								<option value="AB-" <?php echo ('AB-' == $rh ? "selected" : "") ?>>AB-</option>
								<option value="AB+" <?php echo ('AB+' == $rh ? "selected" : "") ?>>AB+</option>
							</select>
							<label>Teléfono *</label>
							<input type="text" placeholder="Teléfono" name="registro[telefono]" required>
							<label>Email *</label>
							<input type="text" placeholder="Email" name="registro[email]" value="<?php echo $email ?>" required>
							<label>Nivel Educativo *</label>
							<select name="registro[nveducacion]" required>
								<option value="">Seleccione</option>
								<option value="Primaria">Primaria</option>
								<option value="Bachiller">Bachiller</option>
								<option value="Profesional">Profesional</option>
								<option value="Especialización">Especialización</option>
								<option value="Maestría">Maestría</option>
							</select>
							<label>Estado Laboral *</label>
							<select name="registro[estado_laboral]" class="estado-laboral" required>
								<option value="">Seleccione</option>
								<option value="EMPLEADO">EMPLEADO</option>
								<option value="DESEMPLEADO">DESEMPLEADO</option>
							</select>
							<label>ARL</label>
							<select name="registro[arl]" class="arl disabled" disabled>
								<option value="">Seleccione</option>
								<?php echo $ls_arl ?>
							</select>
							<label>Alergias *</label>
							<select name="registro[alergias]" required>
								<option value="">Seleccione</option>
								<option value="SI">SI</option>
								<option value="NO">NO</option>
							</select>
							<label>Lesiones Recientes *</label>
							<select name="registro[lesiones]" id="lesiones" required>
								<option value="">Seleccione</option>
								<option value="SI">SI</option>
								<option value="NO">NO</option>
							</select>
							<label>Enfermedades *</label>
							<select name="registro[enfermedades]" id="enfermedad" required>
								<option value="">Seleccione</option>
								<option value="SI">SI</option>
								<option value="NO">NO</option>
							</select>
							<label>Verificación de documentos *</label>
							<input type="text" placeholder="Verificación de documentos" name="registro[vrfdocumentos]">
							<label>Cumple con los requisitos de perfil de ingreso aprendiz: </label>
							<ul>
								<li><input type="checkbox" placeholder="Fot cedula" name="registro[rqcedula]" value="1" class="reqcedula">Fot. Cedula</li>
								<li><input type="checkbox" placeholder="Certificado anterior si aplica" name="registro[rqcertificadoant]" value="1" class="reqcertificado">Soportes adicionales</li>
								<li><input type="checkbox" placeholder="Valoración medica" name="registro[rqvaloracionmedica]" value="1" class="reqvaloracion">Valoración medica</li>
								<li><input type="checkbox" placeholder="Seguridad social" name="registro[rqsegsocial]" value="1" class="reqsegsocial">Seguridad social</li>
								<!-- <li><input type="checkbox" placeholder="Certificación laboral si aplica" name="registro[rqcertificadolab]" value="1" class="reqcertlaboral">Certificación laboral si aplica</li> -->
								<li style="height: 12px"></li>
								<li style="height: 12px"></li>
							</ul>
						</div>

						<div class="Registro-der">
							<label>Sector Económico *</label>
							<select name="registro[sector]">
								<option value="">Seleccione</option>
								<option>AGROPECUARIO</option>
								<option>COMERCIO</option>
								<option>COMERCIO Y SERVICIOS</option>
								<option>COMUNICACIONES</option>
								<option>CONSTRUCCIÓN</option>
								<option>EDUACION</option>
								<option>FINANCIERO</option>
								<option>HIDROCARBUROS</option>
								<option>INDUSTRIAL</option>
								<option>MINERO Y ENERGETICO</option>
								<option>TELECOMUNICACIONES</option>
								<option>TRANSPORTE</option>
							</select>
							<label>Primer Nombre *</label>
							<input type="text" placeholder="Primer Nombre" name="registro[primer_nombre]" value="<?php echo $primerNombre ?>" required>
							<label>Apellidos *</label>
							<input type="text" placeholder="Apellidos" name="registro[apellidos]" value="<?php echo $apellidos ?>" required>
							<label>Tipo de Identificación *</label>
							<select name="registro[tipo_id]" required>
								<option>CC</option>
								<option>TI</option>
								<option>CE</option>
								<option>PP</option>
								<option>PEP</option>
								<option>NIUP</option>
							</select>
							<label>Lugar de Expedición *</label>
							<input type="text" placeholder="De" name="registro[identifica_origen]" class="nacionalidad" required>
							<label>Edad </label>
							<input type="text" placeholder="Edad" name="registro[edad]" class="Edad" required>
							<label>Teléfono del Aprendiz *</label>
							<input type="text" placeholder="Teléfono del aprendiz" name="registro[tlaprendiz]" value="<?php echo $telefono ?>" required>
							<label>Contacto de Emergencia *</label>
							<input type="text" placeholder="Contacto de Emergencia" name="registro[ctemergencia]" required>
							<label>Nivel Lectoescritura *</label>
							<select name="registro[lectoescritura]" required>
								<option value="">Seleccione</option>
								<option value="ANALFABETA">ANALFABETA</option>
								<option value="ALFABETA">ALFABETA</option>
							</select>
							<label>Profesión u Oficio *</label>
							<input type="text" placeholder="Profesión u Oficio" name="registro[profesion]" required>
							<label>Razón social</label>
							<input type="text" placeholder="Empresa" name="registro[empresa]" class="empresa cliente-razon disabled" value="<?php echo $empresa ?>" disabled>
							<input type="hidden" name="cliente[id]" class="cliente-id">
							<label>EPS</label>
							<select name="registro[eps]" class="eps disabled" disabled>
								<option value="">Seleccione</option>
								<?php echo $ls_eps ?>
							</select>
							<label>Consumo reciente de medicamentos *</label>
							<select name="registro[medicamentos]" required>
								<option value="">Seleccione</option>
								<option value="SI">SI</option>
								<option value="NO">NO</option>
							</select>
							<label>Cual *</label>
							<input type="text" placeholder="Cual" name="registro[lesioncual]" class="lesiones-cual disabled" disabled>
							<label>Cual *</label>
							<input type="text" placeholder="Cual" name="registro[enfermedadcual]" class="enfermedad-cual disabled" disabled>
							<!-- <label>Medio de verificación </label>
							<input type="text" placeholder="Medio de verificación " name="registro[vrfmedio]"> -->
						</div>

						<hr>
						<label>PERFIL DE INGRESO</label>
						<div class="Registro-cent">
							<label>¿Qué piensa usted acerca de que las personas que trabajan realizando labores en Alturas se capaciten?</label>
							<select name="registro[eval1]" required>
								<option value="">Seleccione</option>
								<option value="A">A) No es importante porque las personas nunca se accidentan</option>
								<option value="B">B) Es importante ya que la capacitación es una medida preventiva que ayuda a disminuir la probabilidad de accidentarse</option>
								<option value="C">C) Es muy importante porque tomando la capacitación a las personas no les pasa nada</option>
								<option value="D">D) Ninguna de las anteriores</option>
							</select>
							<label>¿Que sabe usted de la resolución 4272 del 2021?</label>
							<select name="registro[eval2]" required>
								<option value="">Seleccione</option>
								<option value="A">A) Es un conjunto de palabras y comportamientos que se deben poner en función una vez ocurra un accidente.</option>
								<option value="B">B) Es un juego de mesa</option>
								<option value="C">C) Es un reglamento de seguridad para realizar labores en Alturas que incluye las medidas de prevención y de Protección contra caídas</option>
								<option value="D">D) Ninguna de las anteriores</option>
							</select>
							<label>¿En Colombia que se considera trabajo en Alturas?</label>
							<select name="registro[eval3]" required>
								<option value="">Seleccione</option>
								<option value="A">A) Toda actividad que realiza un trabajador que ocasione la suspensión y/o desplazamiento, en el que se vea expuesto a un riesgo de caída, mayor a 2.0 metros, con relación del plano de los pies del trabajador al plano horizontal inferior más cercano a él.</option>
								<option value="B">B) Actividades que se realizan por encima de 20 metros</option>
								<option value="C">C) Actividades que se realizan a partir de 5 metros</option>
							</select>
						</div>
						<hr>
						<div class="Registro-der">
							<label>Sesiones</label>
							<select name="registro[sesiones]" id="sesiones" required>
								<option value="">Seleccione</option>
								<?php echo $ls_sesiones; ?>
							</select>
						</div>
						<div class="Registro-der">
						</div>
						<div class="Registro-der tmp-der">
						</div>
						<div class="Registro-izq tmp-izq">
						</div>
						<div class="Registro-der">
							<br><br>
							<label>Firma por pantalla</label>
							<button type="button" class="placa" id="firma">Firma Aprendiz</button>
							<img id="canvasimg" style="display:none;margin:30px;width: 300px">
						</div>
						<div class="Registro-der">
							<!-- <br><br>
							<label>Adjuntar firma (imagen)</label>
							<input type="file" id="firma_adjunta" value=""> -->
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar Registro">
					</div>
				</form>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/cropper.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/moment-2.18.min.js"></script>
	<script type="text/javascript" src="js/registrar_alturas.js?<?php echo time()  ?>"></script>
</body>

</html>
