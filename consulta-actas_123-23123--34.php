<?php
require "libs/conexion.php";
$registro = $_REQUEST['registro'];
$registro = explode('-', $registro);
$id = $registro[0];
$identifi = $registro[1];

$cursos = '';
$cont   = 0;
$meses  = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

$registros = $db
    ->where('Id', $id)
    ->where('numero_ident', $identifi)
    ->groupBy('certificado')
    ->orderBy('Id', 'ASC')
    ->objectBuilder()->get('registros', null, '*, SUM(horas) AS horas');

$total = $db->count;

foreach ($registros as $rsg) {
    $nombres   = '';
    $apellidos = '';
    $nombre    = explode(' ', str_replace('Ñ', 'ñ', $rsg->nombres));
    foreach ($nombre as $key) {
        $nombres .= $key . ' ';
    }
    $apellido = explode(' ', str_replace('Ñ', 'ñ', $rsg->apellidos));
    foreach ($apellido as $key) {
        $apellidos .= $key . ' ';
    }

    $nombre                  = $nombres . $apellidos;
    $tipoid                  = $rsg->tipo_ident;
    $identifica              = $rsg->numero_ident;
    $expedido                = date('d-m-Y', strtotime($rsg->fecha_inicio));
    $GLOBALS['expide']       = $expedido;
    $GLOBALS['codid']        = $rsg->Id;
    if ($rsg->fecha_vigencia != '0000-00-00') {
        $GLOBALS['vigencia']     = date('d-m-Y', strtotime($rsg->fecha_vigencia));
    }
    $GLOBALS['vigencia'] = '';
    $GLOBALS['capacitacion'] = $rsg->capacitacion;

    if ($cont == 0) {
        $c1 = 'border-top:1px solid #000;';
    } else {
        $c1 = '';
    }
    if ($cont == ($total - 1)) {
        $c2 = 'border-bottom:1px solid #000;';
    } else {
        $c2 = '';
    }

    $ls_curso = '';

    $certificaciones = $db
        ->where('Id_ct', $rsg->certificado)
        ->objectBuilder()->get('certificaciones');

    if ($db->count > 0) {
        $ls_curso = $certificaciones[0]->nombre;
    }

    $cursos .= '<strong style="font-family:conthrax;font-size: 19px;color:#A01717">' . $ls_curso . '</strong><br><span style="font-family:ubuntucondensed;font-size: 16px;">Con Una Intensidad De (' . $rsg->horas . ') Horas</span>';
    $cont++;

    $vigencia = '';

    if ($rsg->fecha_vigencia != '0000-00-00') {
        $vigencia = date('d-m-Y', strtotime($rsg->fecha_vigencia));
    }

    $qrcode = $rsg->Id . ' ' . $nombre . ' ' . $tipoid . ' ' . $identifica . ' ' . $ls_curso . ' ' . $vigencia . ' ' . 'http://www.gricompany.co/' . ' ' . '3143257703';

    $entrenador = 'Anthony Alexis Garay Riaño';
    $tsa = '112019-0630-ETSA Min. Trabajo';
    $sena = '9543001986852';
    $licencia = '1748 / 2013';
    $firma = 'images/firmaanthony.png';

    if ($rsg->entrenador != 0) {
        $entrenadores = mysql_query("SELECT * FROM entrenadores WHERE Id_en = '$rsg->entrenador' ");
        if (mysql_num_rows($entrenadores) > 0) {
            $rent = mysql_fetch_object($entrenadores);
            $entrenador = $rent->nombre_en . ' ' . $rent->apellido_en;
            $tsa = $rent->tsa_reg;
            $sena = $rent->sena_reg;
            $licencia = $rent->licencia_sst;
            $firma = $rent->firma_en;
        }
    }
    // $firma = '../Firmas_entrenadores/rrr.png';
}

require_once 'libs/tcpdf.php';

class MYPDF extends TCPDF
{
    public function Header()
    {
        $bMargin         = $this->getBreakMargin();
        $auto_page_break = $this->AutoPageBreak;
        $this->SetAutoPageBreak(false, 0);
        if ($GLOBALS['capacitacion'] != 'ALTURAS') {
            $image_file = 'images/diplomafondo.jpg';
        } else {
            $image_file = 'images/diplomafondoalturas.jpg';
        }
        $this->Image($image_file, 0, 0, 220, 280, '', '', '', false, 300, '', false, false, 0);
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->setPageMark();
    }
    public function Footer()
    {
        $this->addFont('ubuntucondensed', '', 'ubuntucondensed.php');

        // if($GLOBALS['capacitacion'] != 'ALTURAS'){
        //     $this->SetY(-60);

        //     $foot = '<table  cellspacing="0" cellpadding="0" border="0">
        //                     <tr>
        //                         <td align="center" style="font-family:ubuntucondensed;font-size: 13px;">Institución Educativa Para El Talento Y Desarrollo Humano<br>Gri Company<br>Nit: 900 892 983-6<br>Resolución N° 1500-56.03 1795 de la secretaria de educación<br>Licencia seguridad y salud en el trabajo Resolución<br> N° 3753 Secretaria de Salud del Meta<br><br>Dado en la ciudad de Villavicencio, meta, el <span style="color:#A01717">' . $GLOBALS['expide'] . '</span><br>Codigo de validación:' . $GLOBALS['codid'] . ' válido hasta <span style="color:#A01717">' . $GLOBALS['vigencia'] . '</span></td>
        //                     </tr>
        //                 </table>';
        // }else{
        $this->SetY(-67);

        $foot = '<table  cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-family:ubuntucondensed;font-size: 13px;">Institución Educativa Para El Talento Y Desarrollo Humano<br>Gri Company<br>Nit: 900 892 983-6<br>Resolución N° 1500-56.03 1795 de la secretaria de educación<br>Licencia seguridad y salud en el trabajo Resolución N° 3753 Secretaria de Salud del Meta<br>Centro de Entrenamiento Para Trabajo Seguro en Alturas<br>Autorizada con Radicado 08SE2019220000000045994 Min.Trabajo Res. 1178 de 2017<br>Certificado N° CS-CER709434 Organismo Certificado ICONTEC<br><br>Dado en la ciudad de Villavicencio, meta, el <span style="color:#A01717">' . $GLOBALS['expide'] . '</span><br>Codigo de validación:' . $GLOBALS['codid'] . ' válido hasta <span style="color:#A01717">' . $GLOBALS['vigencia'] . '</span></td>
                        </tr>
                    </table>';
        // }
        $this->writeHTML($foot, true, false, false, false, '');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
$pdf->setPageOrientation('p');

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// PDF_MARGIN_TOP
$pdf->SetMargins(35, 20, 25);
// $pdf->SetPrintHeader(false);
// $pdf->setPrintFooter(false);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(true, 45);

// set image scale factor
// $pdf->setImageScale(1.53);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once dirname(__FILE__) . '/lang/eng.php';
    $pdf->setLanguageArray($l);
}

$pdf->AddPage();
$arial = $pdf->addTTFfont('libs/fonts/arial.ttf', 'TrueTypeUnicode', '', 11);

//    $connt = $pdf->addTTFfont('fonts/conthraxsb.ttf', 'TrueTypeUnicode', '', 11);
// $pdf->SetFont($connt, '', 14, '', false);

$pdf->addFont('conthrax', '', 'conthrax.php');
//$pdf->SetFont('conthrax', '', 9, '', 'true');
$pdf->addFont('ubuntucondensed', '', 'ubuntucondensed.php');

if ($GLOBALS['capacitacion'] != 'ALTURAS') {
    $logo = 'images/logogri3.png';
    $logo_w = 420;
} else {
    $logo = 'images/logoalturas.png';
    $logo_w = 280;
}

$html = '<table cellspacing="0" cellpadding="5" border="0" >
                <tr>
                    <td align="center"><br><img src="' . $logo . '" width="' . $logo_w . '"></td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
                <tr>
                    <td align="center" style="font-family:conthrax;font-size: 24px;"><strong>CERTIFICA</strong></td>
                </tr>
                <tr>
                    <td align="center"><strong style="font-family:conthrax;font-size: 20px;">' . $nombre . '</strong>
                        <br><strong style="font-family:conthrax;font-size: 18px;">' . $tipoid . ' ' . $identifica . '</strong>
                    </td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
                <tr>
                    <td align="center"><strong style="font-family:ubuntucondensed;font-size: 15px;">Realizo Y Aprobó El Curso De:</strong><br>' . $cursos . '</td>
                </tr>
            </table>
            <br><br><br><br><br><br><br><br>
           ';

/*  <tr>
<td><img src="images/firma_marcela2.png" width="150"></td>
</tr>*/

$pdf->writeHTML($html, true, false, false, false, '');

if ($GLOBALS['capacitacion'] == 'ALTURAS') {

    // $pdf->Image($firma, 55, 163, 35, 20, '', '', '', true, 300, '', false);
    $pdf->Image($firma, 55, 163, 35, 20, 'PNG', '');

    $html = '<table cellspacing="0" cellpadding="0" border="0" >
                <tr>
                    <td align="center" style="font-family:ubuntucondensed;font-size: 13px;"><strong>' . $entrenador . '</strong></td>
                </tr>
                <tr>
                    <td align="center" style="font-family:ubuntucondensed;font-size: 13px;">Entrenador TSA Reg. ' . $tsa . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:ubuntucondensed;font-size: 13px;">Evaluador de Competencias laborales Sena Reg ' . $sena . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:ubuntucondensed;font-size: 13px;">Licencia, SST ' . $licencia . '</td>
                </tr>
                <tr>
                    <td align="center" style="font-family:ubuntucondensed;font-size: 13px;">Gestion del Riesgo Integral Company S.A.S</td>
                </tr>
            </table>';

    $pdf->writeHTMLCell('', '', -32, 179.6, $html, 0, 0, 0, true, 'J', true);
}


$style = array(
    'padding' => 'auto',
    'fgcolor' => array(0, 0, 0),
    'bgcolor' => false, //array(255,255,255)
    // 'position' => 'R'
);

$pdf->SetMargins(0, 0, 0);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

$pdf->write2DBarcode($qrcode, 'QRCODE,H', 188, 10.5, 20, 20, $style, 'N');


$pdf->Output('ACTA CERTIFICACION - ' . $identifica . '.pdf', 'I');
