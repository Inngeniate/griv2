<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");

$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 20)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Rips Ministerio | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/msj.css" />
	<link rel="stylesheet" href="css/daterangepicker.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq listado-empleados">
				<h2>Listado registros</h2><br>
				<hr>
				<p></p>
				<br>
				<form action="excel-rips" target="_blank" method="post">
					<label>Código: </label>
					<input type="text" name="registro[codigo]" placeholder="Código">
					<!-- <input type="date" name="registro[inicio]" placeholder="Fecha Inicio">
						<input type="date" name="registro[fin]" placeholder="Fecha Fin"> -->
					<input type="submit" value="Exportar">
				</form>
				<br>
				<!-- <div class="Listar-personas">
						<div class="Tabla-listar">
							<table>
								<thead>
									<tr>
										<th>Identificación</th>
										<th>Nombre</th>
										<th>Correo</th>
										<th>Teléfono</th>
										<th>Fecha</th>
										<th>Temperatura</th>
									</tr>
								</thead>
								<tbody id="resultados">
								</tbody>
							</table>
						</div>
					</div>
					<div id="lista_loader">
						<div class="loader2">Cargando...</div>
					</div>
					<div id="paginacion"></div> -->
			</div>
		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/moment-2.18.min.js"></script>
	<script src="js/daterangepicker.js"></script>
	<!-- <script type="text/javascript" src="js/rips-ministerio.js?v<?php echo date('YmdHis') ?>"></script> -->
</body>

</html>
