<?php
/*En esta página se reciben las variables enviadas desde ePayco hacia el servidor.
Antes de realizar cualquier movimiento en base de datos se deben comprobar algunos valores
Es muy importante comprobar la firma enviada desde ePayco
Ingresar  el valor de p_cust_id_cliente lo encuentras en la configuración de tu cuenta ePayco
Ingresar  el valor de p_key lo encuentras en la configuración de tu cuenta ePayco
 */
require_once 'libs/conexion.php';
spl_autoload_register(function ($class_name) {
	require_once __DIR__ . '/libs/' . str_replace('\\', '/', $class_name) . '.php';
});

date_default_timezone_set("America/Bogota");
$fecha_registro = date('Y-m-d h:i:s');

$p_cust_id_cliente = '1261714';
$p_key             = '05e28cd025e9837874ef2b12166b85dd';

$x_ref_payco      = $_REQUEST['x_ref_payco'];
$x_transaction_id = $_REQUEST['x_transaction_id'];
$x_amount         = $_REQUEST['x_amount'];
$x_currency_code  = $_REQUEST['x_currency_code'];
$x_signature      = $_REQUEST['x_signature'];
$x_franchise      = $_REQUEST['x_franchise'];

$signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);

$x_response     = $_REQUEST['x_response'];
$x_motivo       = $_REQUEST['x_response_reason_text'];
$x_id_invoice   = $_REQUEST['x_id_invoice'];
$x_autorizacion = $_REQUEST['x_approval_code'];

$referencia_extra = $_REQUEST['x_extra1'];
$referencia       = explode('-', $referencia_extra);

//Validamos la firma
if ($x_signature == $signature) {
	/*Si la firma esta bien podemos verificar los estado de la transacción*/
	$x_cod_response = $_REQUEST['x_cod_response'];
	switch ((int) $x_cod_response) {
		case 1:
			# code transacción aceptada
			//echo "transacción aceptada";

			$pago = $db
				->where('Id', $referencia[1])
				->objectBuilder()->get('pagos_cursos');

			$identificacion = $pago[0]->identificacion;
			$nombre         = $pago[0]->nombre;
			$apellido       = $pago[0]->apellido;
			$telefono       = $pago[0]->telefono;
			$email          = $pago[0]->email;
			$idpago         = $pago[0]->Id;
			$idplan         = $pago[0]->curso;

			$precios = $db
				->where('Id', $idplan)
				->objectBuilder()->get('precios');

			/* $tipoplan   = $planes[0]->tipo_pl;
			$tiempoplan = $planes[0]->periodo_pl;
			$valorplan  = $planes[0]->valor_pl;
			$totalpago  = $valorplan; */


			$pagar = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'medio' => $x_franchise, 'estado' => 'Aceptada', 'cierre' => $fecha_registro]);

			/* require 'libs/mandrill.php';
			$mandrill = new Mandrill('_n2yZ5xJxL6rL9OvOnUrZg');

			$template = 'mobitco-plan-pago';
			$message  = array(
				'from_email' => 'noreply@mobitco.co',
				'to'         => array(array('email' => $email, 'name' => $nombre . ' ' . $apellido)),
				'subject'    => 'Mobitco Pago Realizado',
			);

			$token_referencia = $hashids->encode($x_transaction_id);

			if ($reinscripcion == 0) {
				$btn_registro = '<br><a href="https://www.mobitco.co/registro-propietario?ref_token=' . $token_referencia . '" target="_blank" class="Continuar-btn">Registrar</a><br><br>';
			} else {
				$btn_registro = '<br><a href="https://www.mobitco.co/" target="_blank" class="Continuar-btn">Ingresar</a><br><br>';
			}

			$template_content = array(
				array(
					'name'    => 'nombre_propietario',
					'content' => $nombre . ' ' . $apellido
				),
				array(
					'name'    => 'tipo_plan',
					'content' => $tipoplan
				),
				array(
					'name'    => 'tiempo_plan',
					'content' => $tiempoplan . ' Meses'
				),
				array(
					'name'    => 'fechapago_plan',
					'content' => $fecha_registro
				),
				array(
					'name'    => 'valor_plan',
					'content' => number_format($valorplan, 0, '', '.')
				),
				array(
					'name'    => 'adicionales_plan',
					'content' => $adicionales
				),
				array(
					'name'    => 'suma_total',
					'content' => '$' . number_format($totalpago, 0, '', '.')
				),
				array(
					'name'    => 'btn_registro',
					'content' => $btn_registro
				),
			);

			$response = $mandrill->messages->sendTemplate($template, $template_content, $message); */

			break;
		case 2:
			# code transacción rechazada
			//echo "transacción rechazada";
			$pago = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'estado' => 'Rechazada', 'cierre' => $fecha_registro]);
			break;
		case 3:
			# code transacción pendiente
			//echo "transacción pendiente";
			$pago = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'estado' => 'Pendiente']);
			break;
		case 4:
			# code transacción fallida
			//echo "transacción fallida";
			$pago = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'estado' => 'Fallida', 'cierre' => $fecha_registro]);
			break;
		case 9:
			$pago = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'estado' => 'Expirada', 'cierre' => $fecha_registro]);
			break;
		case 10:
			$pago = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'estado' => 'Abandonada', 'cierre' => $fecha_registro]);
			break;
		case 11:
			$pago = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'estado' => 'Cancelada', 'cierre' => $fecha_registro]);
			break;
		case 12:
			$pago = $db
				->where('Id', $referencia[1])
				->update('pagos_cursos', ['transaccion' => $x_transaction_id, 'estado' => 'Antifraude', 'cierre' => $fecha_registro]);
			break;
	}
} else {
	die("Firma no valida");
}
