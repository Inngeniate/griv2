<div class="Contenido-admin-izq crear-curso" style="display: none">
	<h2>Crear</h2>
	<form id="nuevo-curso">
		<div class="Registro">
			<div class="Registro-der">
				<label>Nombre del curso*</label>
				<input type="text" placeholder="Nombre del curso" name="curso[nombre]" required>
				<label>Valor sin IVA *</label>
				<input type="text" placeholder="Valor sin IVA" name="curso[sinIva]" required>
			</div>
			<div class="Registro-der">
				<label>IVA *</label>
				<input type="text" placeholder="IVA" name="curso[iva]" required>
				<label>Valor total</label>
				<input type="text" placeholder="Valor total" name="curso[total]" required>
			</div>
			<hr>
			<br>
			<br>
			<input type="submit" value="Guardar">
		</div>
	</form>
</div>
