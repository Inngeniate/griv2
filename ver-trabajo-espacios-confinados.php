<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

else {
	require("libs/conexion.php");
	$ls_anexos = '';

	$registros = $db
		->where('Id_al', $_POST['idregistro'])
		->objectBuilder()->get('registros_espacios_confinados');

	if ($db->count > 0) {
		$rg = $registros[0];

		$formacion = $rg->formacion_al;
		if ($rg->formacion_otro_al != '') {
			$formacion = $rg->formacion_otro_al;
		}

		$curso = '';

		$cursos = $db
			->where('Id_ct', $rg->curso_al)
			->objectBuilder()->get('certificaciones');

		if ($db->count > 0) {
			$curso = $cursos[0]->nombre;
		}

		$evaluacion = $db
			->where('identificacion_ev', $rg->documento_al)
			->objectBuilder()->get('evaluaciones_tsa');

		if ($db->count > 0) {
			$rev = $evaluacion[0];
			$ls_anexos .= '<p><i class="icon-attachment"></i> <a href="respuestas-evaluaciontsa?ev=' . $rev->Id_ev . '" title="Hoja de respuestas" target="_blank">Evaluación Avanzado</p>';
		}

		$req_cedula = '';
		$req_ocupacional = '';
		$req_parafiscales = '';
		$req_certificacion = '';
		$req_trabajadorentrante = '';
		$req_vigiaseguridad = '';
		$req_tituloprofesional = '';
		$req_licencia = '';

		if ($rg->curso_al == '35') {
			$req_cedula = 'required';
			$req_certificacion = 'required';
			$req_parafiscales = 'required';
		}

		if ($rg->curso_al == '142') {
			$req_cedula = 'required';
			$req_parafiscales = 'required';
			$req_certificacion = 'required';
			$req_ocupacional = 'required';
			$req_trabajadorentrante = 'required';
			$req_vigiaseguridad = 'required';
		}

		if ($rg->curso_al == '160') {
			$req_cedula = 'required';
			$req_parafiscales = 'required';
			$req_certificacion = 'required';
			$req_ocupacional = 'required';
			$req_trabajadorentrante = 'required';
		}

		if ($rg->curso_al == '161') {
			$req_cedula = 'required';
			$req_tituloprofesional = 'required';
			$req_licencia = 'required';
		}

		if ($rg->curso_al == '162') {
			$req_cedula = 'required';
			$req_parafiscales = 'required';
			$req_certificacion = 'required';
			$req_ocupacional = 'required';
		}

		$anexos = $db
			->where('Id_al', $_POST['idregistro'])
			->objectBuilder()->get('registro_espacios_confinados_anexos');

		if ($db->count > 0) {
			foreach ($anexos as $ran) {
				switch ($ran->tipo_an) {
					case 'Parafiscales':
						$req_parafiscales = '';
						break;
					case 'Examen Ocupacional':
						$req_ocupacional = '';
						break;
					case 'Copia Cedula':
						$req_cedula = '';
						break;
					case 'Certificación Anterior':
					case 'Certificado alturas vigente':
						$req_certificacion = '';
						$ran->tipo_an = 'Certificado alturas vigente';
						break;
					case 'Formación trabajador entrante':
						$req_trabajadorentrante = '';
						break;
					case 'Formación vigia de seguridad':
						$req_vigiaseguridad = '';
						break;
					case 'Titulo profesional SST':
						$req_tituloprofesional = '';
						break;
					case 'Licencia SST':
						$req_licencia = '';
						break;
				}

				$ls_anexos .= '<p><i class="icon-attachment"></i> <a href="' . $ran->adjunto_an . '" title="' . $ran->tipo_an . '" target="_blank">' . $ran->tipo_an . '</a> - <a href="javascript://" title="Eliminar Anexo" class="el-anexo" id="' . $ran->Id_an . '"><i class="icon-bin"></i></a></p>';
			}

			$ls_anexos .= '<hr>';
		}
	} else {
		header('Location: trabajo-espacio-confinados');
	}
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany Gestión de Riesgos Integrales</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/jquery.modal.theme-xenon.css" />
	<script src="js/modernizr.custom.js"></script>
	<style type="text/css" media="screen">
		.oculto {
			display: none;
		}

		#ver-anexos {
			cursor: pointer;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Registros Trabajo en Espacios Confinados</h2>
				<?php
				if ($_SESSION['usutipoggri'] != 'auditor') {
				?>
					<a href="editar-espacios-confinados?reg=<?php echo $_POST['idregistro']; ?>" target="_blank" class="btn">Editar Registro</a>
				<?php } ?>
				<hr>
				<div class="Registro-der">
					<p><label>Fecha: </label><?php echo $rg->fecha_al; ?></p>
					<p><label>Nombre: </label><?php echo $rg->nombre_primero_al . ' ' . $rg->nombre_segundo_al . ' ' . $rg->apellidos_al; ?></p>
					<p><label>Nivel de Formación: </label><?php echo $formacion; ?></p>
					<p><label>Teléfono: </label><?php echo $rg->telefono_al; ?></p>
					<p><label>Profesión: </label><?php echo $rg->profesion_al; ?></p>
				</div>
				<div class="Registro-izq">
					<p><label>Identificación: </label><?php echo $rg->documento_al; ?></p>
					<p><label>Curso: </label><?php echo $curso; ?></p>
					<p><label>Sector: </label><?php echo $rg->sector_al; ?></p>
					<p><label>Edad: </label><?php echo $rg->edad_al; ?></p>
					<p><label>Empresa: </label><?php echo $rg->empresa_al; ?></p>
				</div>
				<div id="ver-anexos">
					<hr>
					<label><i class="icon-listar"></i> Anexos <i class="flecha flecha-abajo"></i></label>
					<hr>
				</div>
				<div class="anexos oculto">
					<?php echo $ls_anexos; ?>
					<br>
					<?php
					if ($_SESSION['usutipoggri'] != 'auditor') {
					?>
						<form id="anexar">
							<div class="Registro-der">
								<label>Copia Cedula</label>
								<input type="file" id="cedula" class="adjunto" name="" <?php echo $req_cedula ?>>
							</div>
							<div class="Registro-der">
								<label>Parafiscales</label>
								<input type="file" id="parafiscales" class="adjunto" name="" <?php echo $req_parafiscales ?>>
							</div>
							<div class="Registro-izq">
								<label>Examen Ocupacional</label>
								<input type="file" id="ocupacional" class="adjunto" name="" <?php echo $req_ocupacional ?>>
							</div>
							<div class="Registro-izq">
								<label>Certificado Alturas Vigente</label>
								<input type="file" id="certificacion" class="adjunto" name="" <?php echo $req_certificacion ?>>
							</div>
							<div class="Registro-izq">
								<label>Form. Trabajador Entrante</label>
								<input type="file" id="trabajadorentrante" class="adjunto" name="" <?php echo $req_trabajadorentrante ?>>
							</div>
							<div class="Registro-izq">
								<label>Form. Vigia de Seguridad</label>
								<input type="file" id="vigiaseguridad" class="adjunto" name="" <?php echo $req_vigiaseguridad ?>>
							</div>
							<div class="Registro-izq">
								<label>Titulo Profesional SST</label>
								<input type="file" id="tituloprofesional" class="adjunto" name="" <?php echo $req_tituloprofesional ?>>
							</div>
							<div class="Registro-izq">
								<label>Licencia SST</label>
								<input type="file" id="licencia" class="adjunto" name="" <?php echo $req_licencia ?>>
							</div>
							<div class="Registro-der">
								<label>Registro - Evidencia</label>
								<input type="file" id="evidencia" class="adjunto" name="">
							</div>
							<div class="Registro-izq">
							</div>
							<input type="hidden" id="idregistro" value="<?php echo $_POST['idregistro'] ?>">
							<input type="submit" value="Guardar Anexos">
						</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/ver-espacios-confinados.js?v<?php echo date('YmdHis') ?>"></script>
	<script src="js/jquery.modal.min.js"></script>
</body>

</html>
