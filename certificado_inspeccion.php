<div class="inspeccion" style="display: none">
	<div class="Contenido-admin-izq">
		<h2>Crear Certificados</h2>
		<form class="cre_certificado">
			<div class="Registro">
				<div class="Registro-der">
					<label>Fecha de Inspección *</label>
					<input type="date" placeholder="Fecha de Inspección" name="certifica[inspeccion]" required="">
					<label>Placa Vehículo *</label>
					<input type="text" name="certifica[placav]" placeholder="Placa Vehículo" required>
					<label>Placa Tráiler *</label>
					<input type="text" name="certifica[placat]" placeholder="Placa Tráiler" required>
					<label>Tipo de Vehículo *</label>
					<input type="text" name="certifica[tipov]" placeholder="Tipo de Vehículo" required>
				</div>
				<div class="Registro-der">
					<label>Nombre Conductor *</label>
					<input type="text" name="certifica[conductor]" placeholder="Nombre Conductor" required>
					<label>C.C *</label>
					<input type="text" name="certifica[cc]" placeholder="C.C" required>
					<label>Nombre del propietario/empresa *</label>
					<input type="text" name="certifica[propietario]" placeholder="Nombre del propietario/empresa" required>
				</div>
				<div class="Registro-cent">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th rowspan="2">1. Zona y/o elemento del vehículo a inspeccionar</th>
									<th colspan="2">Estado</th>
								</tr>
								<tr>
									<th>B</th>
									<th>M</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th colspan="3">Cabina Vehicular</th>
								</tr>
								<tr>
									<td>Estado de la cabina, limpio y ordenado</td>
									<td><input type="radio" name="certifica[cab1]" value="B" required></td>
									<td><input type="radio" name="certifica[cab1]" value="M" required></td>
								</tr>
								<tr>
									<td>Cinturón de seguridad y apoya cabezas</td>
									<td><input type="radio" name="certifica[cab2]" value="B" required></td>
									<td><input type="radio" name="certifica[cab2]" value="M" required></td>
								</tr>
								<tr>
									<td>Cabina limpia y en orden</td>
									<td><input type="radio" name="certifica[cab3]" value="B" required></td>
									<td><input type="radio" name="certifica[cab3]" value="M" required></td>
								</tr>
								<tr>
									<td>Silla suficientemente alta, sin superficies duras o salientes</td>
									<td><input type="radio" name="certifica[cab4]" value="B" required></td>
									<td><input type="radio" name="certifica[cab4]" value="M" required></td>
								</tr>
								<tr>
									<td>Seguridad en puertas</td>
									<td><input type="radio" name="certifica[cab5]" value="B" required></td>
									<td><input type="radio" name="certifica[cab5]" value="M" required></td>
								</tr>
								<tr>
									<td>Vidrio Panorámico y limpia Brisas</td>
									<td><input type="radio" name="certifica[cab6]" value="B" required></td>
									<td><input type="radio" name="certifica[cab6]" value="M" required></td>
								</tr>
								<tr>
									<td>Sistema de luces (altas, medias, bajas) *</td>
									<td><input type="radio" name="certifica[cab7]" value="B" required></td>
									<td><input type="radio" name="certifica[cab7]" value="M" required></td>
								</tr>
								<tr>
									<td>Sistema de luces traseras Stop *</td>
									<td><input type="radio" name="certifica[cab8]" value="B" required></td>
									<td><input type="radio" name="certifica[cab8]" value="M" required></td>
								</tr>
								<tr>
									<td>Sistema de luces direccionales</td>
									<td><input type="radio" name="certifica[cab9]" value="B" required></td>
									<td><input type="radio" name="certifica[cab9]" value="M" required></td>
								</tr>
								<tr>
									<td>Sistema de frenos</td>
									<td><input type="radio" name="certifica[cab10]" value="B" required></td>
									<td><input type="radio" name="certifica[cab10]" value="M" required></td>
								</tr>
								<tr>
									<td>Luces de Freno</td>
									<td><input type="radio" name="certifica[cab11]" value="B" required></td>
									<td><input type="radio" name="certifica[cab11]" value="M" required></td>
								</tr>
								<tr>
									<td>Luces Estacionarias</td>
									<td><input type="radio" name="certifica[cab12]" value="B" required></td>
									<td><input type="radio" name="certifica[cab12]" value="M" required></td>
								</tr>
								<tr>
									<td>Luces de reversa</td>
									<td><input type="radio" name="certifica[cab13]" value="B" required></td>
									<td><input type="radio" name="certifica[cab13]" value="M" required></td>
								</tr>
								<tr>
									<td>Sistema de encendido</td>
									<td><input type="radio" name="certifica[cab14]" value="B" required></td>
									<td><input type="radio" name="certifica[cab14]" value="M" required></td>
								</tr>
								<tr>
									<td>Sistema Airbags (bolsas de aire)</td>
									<td><input type="radio" name="certifica[cab15]" value="B" required></td>
									<td><input type="radio" name="certifica[cab15]" value="M" required></td>
								</tr>
								<tr>
									<th colspan="3">Carrocería</th>
								</tr>
								<tr>
									<td>Estado de la carpa* (si aplica) *</td>
									<td><input type="radio" name="certifica[carr1]" value="B" required></td>
									<td><input type="radio" name="certifica[carr1]" value="M" required></td>
								</tr>
								<tr>
									<td>Estado de la carrocería</td>
									<td><input type="radio" name="certifica[carr2]" value="B" required></td>
									<td><input type="radio" name="certifica[carr2]" value="M" required></td>
								</tr>
								<tr>
									<td>Estado del remolque (verifique el estado de cinchas, palos, lazos y cadenas) En caso que aplique</td>
									<td><input type="radio" name="certifica[carr3]" value="B" required></td>
									<td><input type="radio" name="certifica[carr3]" value="M" required></td>
								</tr>
								<tr>
									<td>Estado de  tornamesa, King Ping y quinta rueda</td>
									<td><input type="radio" name="certifica[carr4]" value="B" required></td>
									<td><input type="radio" name="certifica[carr4]" value="M" required></td>
								</tr>
								<tr>
									<td>Malla de seguridad (entre el espacio del pasajero y la carga)</td>
									<td><input type="radio" name="certifica[carr5]" value="B" required></td>
									<td><input type="radio" name="certifica[carr5]" value="M" required></td>
								</tr>
								<tr>
									<th colspan="3">Exteriores</th>
								</tr>
								<tr>
									<td>Placa visible</td>
									<td><input type="radio" name="certifica[ext1]" value="B" required></td>
									<td><input type="radio" name="certifica[ext1]" value="M" required></td>
								</tr>
								<tr>
									<td>Estado de llantas (en vehículos pesados 3 mm)</td>
									<td><input type="radio" name="certifica[ext2]" value="B" required></td>
									<td><input type="radio" name="certifica[ext2]" value="M" required></td>
								</tr>
								<tr>
									<td>2 llantas de repuesto (gravado, presión y estado en general)</td>
									<td><input type="radio" name="certifica[ext3]" value="B" required></td>
									<td><input type="radio" name="certifica[ext3]" value="M" required></td>
								</tr>
								<tr>
									<td>Presenta el vehículo escapes de aire *</td>
									<td><input type="radio" name="certifica[ext4]" value="B" required></td>
									<td><input type="radio" name="certifica[ext4]" value="M" required></td>
								</tr>
								<tr>
									<td>Espejo lateral izquierdo, lateral derecho y retrovisores *</td>
									<td><input type="radio" name="certifica[ext5]" value="B" required></td>
									<td><input type="radio" name="certifica[ext5]" value="M" required></td>
								</tr>
								<tr>
									<td>Espejos convexos para puntos ciegos (vehículos pesados)</td>
									<td><input type="radio" name="certifica[ext6]" value="B" required></td>
									<td><input type="radio" name="certifica[ext6]" value="M" required></td>
								</tr>
								<tr>
									<td>Alarma Auditiva de reversa</td>
									<td><input type="radio" name="certifica[ext7]" value="B" required</td>
									<td><input type="radio" name="certifica[ext7]" value="M" required></td>
								</tr>
								<tr>
									<td>Reencauche de llantas (curado en frio, solo se puede usar en los ejes traseros)</td>
									<td><input type="radio" name="certifica[ext8]" value="B" required></td>
									<td><input type="radio" name="certifica[ext8]" value="M" required></td>
								</tr>
								<tr>
									<td>Caja de herramientas</td>
									<td><input type="radio" name="certifica[ext9]" value="B" required></td>
									<td><input type="radio" name="certifica[ext9]" value="M" required></td>
								</tr>
								<tr>
									<td>Señalización</td>
									<td><input type="radio" name="certifica[ext10]" value="B" required></td>
									<td><input type="radio" name="certifica[ext10]" value="M" required></td>
								</tr>
								<tr>
									<td>Equipo de carretera *</td>
									<td><input type="radio" name="certifica[ext11]" value="B" required></td>
									<td><input type="radio" name="certifica[ext11]" value="M" required></td>
								</tr>
								<tr>
									<th colspan="3">2. EPP (Elementos de protección personal)</th>
								</tr>
								<tr>
									<td>Casco de seguridad</td>
									<td><input type="radio" name="certifica[epp1]" value="B" required></td>
									<td><input type="radio" name="certifica[epp1]" value="M" required></td>
								</tr>
								<tr>
									<td>Protector auditivo</td>
									<td><input type="radio" name="certifica[epp2]" value="B" required></td>
									<td><input type="radio" name="certifica[epp2]" value="M" required></td>
								</tr>
								<tr>
									<td>Gafas de seguridad</td>
									<td><input type="radio" name="certifica[epp3]" value="B" required></td>
									<td><input type="radio" name="certifica[epp3]" value="M" required></td>
								</tr>
								<tr>
									<td>Mascarilla</td>
									<td><input type="radio" name="certifica[epp4]" value="B" required></td>
									<td><input type="radio" name="certifica[epp4]" value="M" required></td>
								</tr>
								<tr>
									<td>Chaleco Reflectivo</td>
									<td><input type="radio" name="certifica[epp5]" value="B" required></td>
									<td><input type="radio" name="certifica[epp5]" value="M" required></td>
								</tr>
								<tr>
									<td>Botas de seguridad</td>
									<td><input type="radio" name="certifica[epp6]" value="B" required></td>
									<td><input type="radio" name="certifica[epp6]" value="M" required></td>
								</tr>
								<tr>
									<td>Guantes (Neopreno o Vaqueta según aplique)</td>
									<td><input type="radio" name="certifica[epp7]" value="B" required></td>
									<td><input type="radio" name="certifica[epp7]" value="M" required></td>
								</tr>
								<tr>
									<td>Mencione si falta algún EPP:</td>
									<td colspan="2"><textarea name="certifica[epp8]"></textarea></td>
								</tr>
								<tr>
									<th>3. Se practicó prueba de Alcohol?</th>
									<td><input type="radio" name="certifica[alco1]" value="si" required>Si</td>
									<td><input type="radio" name="certifica[alco1]" value="no" required>No</td>
								</tr>
								<tr>
									<th colspan="3">4. Medio Ambiente</th>
								</tr>
								<tr>
									<td>El vehículo presenta fuga de Aceite?</td>
									<td><input type="radio" name="certifica[fuga1]" value="si" required>Si</td>
									<td><input type="radio" name="certifica[fuga1]" value="no" required>No</td>
								</tr>
								<tr>
									<td>Fecha de Vencimiento Extintor 1</td>
									<td colspan="2"><input type="date" name="certifica[extin1]" required></td>
								</tr>
								<tr>
									<td>Fecha de Vencimiento Extintor 2</td>
									<td colspan="2"><input type="date" name="certifica[extin2]" required></td>
								</tr>
								<tr>
									<th>5. El Botiquín presenta algún elemento vencido?</th>
									<td><input type="radio" name="certifica[boti1]" value="si" required>Si</td>
									<td><input type="radio" name="certifica[boti1]" value="no" required>No</td>
								</tr>
								<tr>
									<th>6. Transporta mercancías peligrosas?</th>
									<td><input type="radio" name="certifica[pel1]" value="si" required>Si</td>
									<td><input type="radio" name="certifica[pel1]" value="no" required>No</td>
								</tr>
								<tr>
									<td colspan="3">En caso que la respuesta sea Si, responder las siguiente preguntas:</td>
								</tr>
								<tr>
									<td>El conductor cuenta con curso de mercancías peligrosas?*</td>
									<td><input type="radio" name="certifica[pel2]" value="si">Si</td>
									<td><input type="radio" name="certifica[pel2]" value="no">No</td>
								</tr>
								<tr>
									<td>El conductor cuenta con la hoja de seguridad del producto a transportar?*</td>
									<td><input type="radio" name="certifica[pel3]" value="si">Si</td>
									<td><input type="radio" name="certifica[pel3]" value="no">No</td>
								</tr>
								<tr>
									<td>El vehículo se encuentra debidamente señalizado y rotulado de acuerdo a la hoja de seguridad del producto?*</td>
									<td><input type="radio" name="certifica[pel4]" value="si">Si</td>
									<td><input type="radio" name="certifica[pel4]" value="no">No</td>
								</tr>
								<tr>
									<td>El vehículo cuenta con Kit anti derrame?*</td>
									<td><input type="radio" name="certifica[pel5]" value="si">Si</td>
									<td><input type="radio" name="certifica[pel5]" value="no">No</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<br>
				<br>
				<input type="hidden" name="certifica[accion]" value="inspeccion">
				<input type="submit" value="Guardar Certificado">
			</div>
		</form>
	</div>
</div>