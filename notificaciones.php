<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

else {
	if ($_SESSION['usutipoggri'] == 'auditor') {
		header('Location: trabajo-altura');
	}

	require("libs/conexion.php");
	$ls_notificaciones = '';
	$cont = 1;

	$notifica = $db
		->orderBy('tiempo_v', 'ASC')
		->objectBuilder()->get('vencimientos');


	foreach ($notifica as $rnt) {
		$registro = $db
			->where('Id', $rnt->Id_rg)
			->objectBuilder()->get('registros');

		$rreg = $registro[0];

		$vendedor = $rreg->vendedor;
		// $usuarios = mysql_query("SELECT * FROM usuarios WHERE id = '$rreg->vendedor' ");
		// if (mysql_num_rows($usuarios) > 0) {
		//     $rsu      = mysql_fetch_object($usuarios);
		//     $vendedor = $rsu->nombre . ' ' . $rsu->apellido;
		// }

		$ls_notificaciones .= '<tr>
									<td>' . $cont . '</td>
									<td>' . $rreg->certificado . '</td>
									<td>' . $rreg->nombre_primero . ' ' . $rreg->nombre_segundo . ' ' . $rreg->apellidos . '</td>
									<td>' . $rreg->numero_ident . '</td>
									<td nowrap>' . $rreg->telefono . '</td>
									<td nowrap>' . $vendedor . '</td>
									<td nowrap>' . ($rreg->fecha_vigencia == '0000-00-00' ? "" : $rreg->fecha_vigencia) . '</td>
									<td nowrap style="text-align:center">' . $rnt->tiempo_v . ' d</td>
								</tr>';
		$cont++;
	}
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Certificaciones | Gricompany Gestión de Riesgos Integrales</title>
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/jquery.modal.theme-xenon.css" />
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<?php include_once("analyticstracking.php") ?>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<div class="Contenido-admin-izq">
				<h2>Listar Registros Pronto a vencer</h2>
				<a href="libs/vencidos" target="_blank" class="btn">Exportar Excel</a>
				<hr>
				<p>En esta sección podrás ver cuales registros se encuentras por vencer en un lapso hasta de 5 dias.</p>
				<br>
				<div class="Listar-personas">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th>#</th>
									<th>Certificado</th>
									<th>Cliente</th>
									<th>Cédula</th>
									<th>Teléfono</th>
									<th>Vendedor</th>
									<th>Fecha vigencia</th>
									<th>Tiempo restante</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<?php echo $ls_notificaciones ?>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript" src="js/listado.js"></script>
	<script src="js/jquery.modal.min.js"></script>
</body>

</html>
