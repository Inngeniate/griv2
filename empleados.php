<?php
session_start();
if (!$_SESSION['usuloggri']) header('Location: admin');

require("libs/conexion.php");

$permisos_usuario = $db
	->where('usuario_up', $_SESSION['usuloggri'])
	->where('modulo_up', 12)
	->where('permiso_up', 1)
	->objectBuilder()->get('usuarios_permisos');

if ($db->count == 0) {
	$permisos_usuario = $db
		->where('usuario_up', $_SESSION['usuloggri'])
		->where('permiso_up', 1)
		->orderBy('Id_up', 'ASC')
		->objectBuilder()->get('usuarios_permisos', 1);

	$permisos = $permisos_usuario[0];

	$menu = $db
		->where('Id_m', $permisos->modulo_up)
		->objectBuilder()->get('menu');

	header('Location: ' . $menu[0]->link_m);
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Empleados | Gricompany</title>
	<link rel="stylesheet" href="css/stylesheet.css" />
	<link rel="stylesheet" href="css/style-menu.css" />
	<link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/paginacion.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="css/msj.css" />
	<script src="js/modernizr.custom.js"></script>
	<style>
		.Registro-cent {
			width: 95%;
			margin-left: auto;
			margin-right: auto;
		}

		.contenedor-imagen img {
			max-width: 100%;
		}

		.img-placa {
			margin-bottom: 30px;
			text-align: left;
			text-align: center;
		}

		.img-prev {
			width: 113px;
			height: 152px;
			background: #f2f2f2;
			display: block;
			margin-bottom: 10px;
			margin-left: auto;
			margin-right: auto;
		}

		.quitar {
			cursor: pointer;
			float: right;
		}
	</style>
</head>

<body>
	<div class="Contenedor">
		<header>
			<?php include("menu2.php"); ?>
		</header>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/script-menu.js"></script>
	</div>
	<section>
		<div class="Contenido-admin">
			<?php
			//if($_SESSION['usutipoggri'] == 'administrador'){
			?>
			<div class="Tab-slide">
				<div class="Tab-slide-inside">
					<ul id="navegador">
						<li><a href="javascript://" id="listar" class="activo">Listar Empleados</a></li>
						<li><a href="javascript://" id="crear">Crear Empleados</a></li>
					</ul>
				</div>
			</div>
			<div class="Contenido-admin-izq listado-empleados">
				<h2>Listar Empleados</h2>
				<hr>
				<p>En esta sección podrás listar y buscar empleados.</p>
				<br>
				<form id="buscar">
					<label>Nombre: </label>
					<input type="text" name="nom" placeholder="Nombre">
					<input type="submit" value="Buscar">
				</form>
				<br>
				<div class="Listar-personas">
					<div class="Tabla-listar">
						<table>
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Identificación</th>
									<th>Cargo</th>
									<th>Correo</th>
									<th>Teléfono</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody id="resultados">
							</tbody>
						</table>
					</div>
				</div>
				<div id="lista_loader">
					<div class="loader2">Cargando...</div>
				</div>
				<div id="paginacion"></div>
			</div>
			<div class="Contenido-admin-izq crear-empleados" style="display: none">
				<h2>Crear Empleado</h2>
				<form id="nuevo-empleado">
					<div class="Registro">
						<div class="Registro-der">
							<label>Nombre *</label>
							<input type="text" placeholder="Nombre" name="empleado[nombre]" required>
						</div>
						<div class="Registro-izq">
							<label>Identificación *</label>
							<input type="number" placeholder="Identificación" name="empleado[identifica]" required>
						</div>
						<div class="Registro-izq">
							<label>Cargo *</label>
							<input type="text" placeholder="Cargo" name="empleado[cargo]" required>
						</div>
						<div class="Registro-der">
							<label>Correo *</label>
							<input type="text" placeholder="Correo" name="empleado[correo]" required>
						</div>
						<div class="Registro-izq">
							<label>Teléfono *</label>
							<input type="text" placeholder="Teléfono" name="empleado[telefono]" required>
						</div>
						<div class="Registro-izq">
							<label>Firma</label>
							<button type="button" class="placa" id="firma">Firma</button>
							<img id="canvasimg" style="display:none;margin:30px;width: 300px">
						</div>
						<br>
						<br>
						<input type="submit" value="Guardar">
					</div>
				</form>
			</div>
			<?php
			//  }else{
			// 	echo '<p>No tiene permiso para acceder a esta sección.</p>';
			// }
			?>
		</div>
	</section>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/empleados.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
