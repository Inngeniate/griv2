<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<!-- <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" > -->
	<meta name="google-site-verification" content="oc9E4yBVx2u8uUHzYyL0_dqaWXeQipnb0i2jfsMEHaA" />
	<meta name="keywords" lang="es" content="Resolución 1223 de 2014, transporte, seguridad, salud, trabajo seguro en alturas, manejo defensivo, transito, movilidad, brigadas de emergencias, hidrocarburos, primeros auxilios, asesorías, capacitaciones, implementación ssta, competencias laborales, espacios confinados, brec, extintores, carga seca y liquida, institución educativa para el desarrollo humano, maquinaria amarilla, gpl, plan estratégico seguridad vial, pesv.">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="GRI Company, es una empresa líder en la prestación de servicios especializados de Transito, transporte, movilidad y seguridad Vial, pioneros en la asesoría y capacitaciones de temas relacionados con seguridad y salud en el trabajo e implementación de sistemas de gestión integrados">
	<title>Gricompany Gestión del Riesgo Integral</title>
	<!-- <link rel="stylesheet" href="css/slider.css" /> -->
	<link rel="stylesheet" href="css/landing.css" />
	<link rel="stylesheet" href="css/register.css" />
	<link rel="stylesheet" type="text/css" href="css/msj.css" />
	<link href="css/jquery.modal.css" type="text/css" rel="stylesheet" />
	<link href="js/jquery-ui-1.12.1.custom/jquery-ui.min.css" type="text/css" rel="stylesheet" />
	<link href="js/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="js/flatpickr/dist/flatpickr.min.css" />
	<style type="text/css" media="screen">
		input[type=email] {
			width: 100%;
			padding: 10px 10px;
			background: #fff;
			border: 1px solid #333;
			outline: none;
			font-size: 14px;
			margin-bottom: 10px;
			-webkit-transition: 0.3s;
			-moz-transition: 0.3s;
			-o-transition: 0.3s;
			-ms-transition: 0.3s;
			transition: 0.3s;
		}

		input[type=submit] {
			width: auto !important;
		}

		.Siguiente {
			margin-right: 50px;
		}
	</style>
</head>

<body>
	<?php include_once("header-new-top2.php") ?>
	<form class="inscripcion-aspirantes inscripcion-inicio">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<div class="Registro-asp-int-sec">
						<h2>Formulario de inscripción</h2>
						<label>Tipo Identificación*</label>
						<select id="tipo-identificacion" name="registro[tipo_identificacion]" class="requiere" required>
							<option value="">Seleccione</option>
							<option value="TI">Tarjeta Identidad</option>
							<option value="CC">Cédula de Ciudadanía</option>
							<option value="CE">Cédula Extranjería</option>
							<option value="V">Visa</option>
							<option value="RC">Registro Civil</option>
							<option value="P">Pasaporte</option>
						</select>
						<label>Numero de identificación*</label>
						<input type="text" placeholder="Numero" id="numero-identificacion" class="requiere" name="registro[numero_identificacion]" required>
						<label>Fecha de nacimiento*</label>
						<input type="text" placeholder="Fecha" class="fecha requiere" id="fecha-nacimiento" name="registro[fecha_nacimiento]" required>
						<input type="submit" value="Continuar" name="">
					</div>
				</div>
			</div>
		</div>
	</form>
	<form class="inscripcion-aspirantes" style="display: none">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.1 Información de Programas. Registré en esta sección Programa, Jornada y el Tipo de Inscripción que va a realizar.</p>
					<div class="Registro-asp-int-sec">
						<label>Periodo*</label>
						<select name="registro[periodo]" class="requiere" required>
							<option>2019-1</option>
						</select>
						<label>Programa*</label>
						<select name="registro[programa]" id="programa" class="requiere" required>
							<option value="">Seleccione</option>
							<option value="TL">Técnica Laboral por competencias</option>
							<option value="CC">Cursos Cortos educación informal</option>
						</select>
						<div id="cursos">
						</div>
						<label>Horario*</label>
						<select name="registro[horario]" class="requiere" required>
							<option value="">Seleccione</option>
							<option value="N">Nocturno</option>
							<option value="S">Sabatino</option>
						</select>
						<label>Tipo de solicitud</label>
						<select name="registro[tipo_solicitud]">
							<option value="">Seleccione</option>
							<option value="DT">Doble programa</option>
							<option value="NP">Nuevo</option>
							<option value="TE">Transferencia externa</option>
							<option value="TI">Transferencia interna</option>
						</select>
						<div class="Con-siguiente">
							<input type="button" class="Siguiente anterior" value="Anterior" name="">
							<input type="submit" class="Siguiente" value="Siguiente" name="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<form class="inscripcion-aspirantes" style="display: none">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.2 Información de Personal. Registré en esta sección los datos personales del aspirante.</p>
					<div class="Registro-asp-int-sec">
						<label>Tipo de identificación*</label>
						<input type="text" id="tipo_identificacion2" name="" disabled="">
						<label>Identificación*</label>
						<input type="text" id="numero_identificacion2" name="" disabled="">
						<!-- <label>Lugar de expedición*</label>
							<input type="text" placeholder="Lugar de expedición" class="mayus-1 municipio requiere" name="registro[lugar_expedicion]" required>
							<label>Fecha de expedición*</label>
							<input type="text" placeholder="Fecha de expedición" class="fecha requiere" name="registro[fecha_expedicion]" required>
							<label>Nacionalidad*</label>
							<input type="text" placeholder="Ingrese nacionalidad" class="mayus requiere" name="registro[nacionalidad]" required> -->
						<label>Nombres*</label>
						<input type="text" placeholder="Nombres" class="mayus requiere" name="registro[aspirantes_nombres]" required>
						<label>Apellidos*</label>
						<input type="text" placeholder="Apellidos" class="mayus requiere" name="registro[aspirante_apellidos]" required>
					</div>
					<div class="Registro-asp-int-sec">
						<label>Fecha de nacimiento*</label>
						<input type="text" placeholder="Fecha" id="fecha_nacimiento2" name="" disabled>
						<label>Lugar de nacimiento*</label>
						<input type="text" placeholder="Lugar de nacimiento" class="mayus-1 municipio requiere" name="registro[lugar_nacimiento]" required>
						<label>Genero*</label>
						<select name="registro[genero]" class="requiere" required>
							<option value="">Selecciona</option>
							<option value="F">Femenino</option>
							<option value="M">Masculino</option>
						</select>
						<label>Estado civil*</label>
						<select name="registro[estado_civil]" class="requiere" required>
							<option value="">Seleccionar</option>
							<option value="S">Soltero</option>
							<option value="C">Casado</option>
							<option value="UL">Unión libre </option>
							<option value="V">Viudo</option>
							<option value="SP">Separado</option>
						</select>
						<!-- <label>Condición especial</label>
							<select name="registro[condicion_especial]">
								<option value="">Selecciona</option>
								<option>Discapacidad sensorial - Sordera profunda</option>
								<option>Discapacidad sensorial - Hipoaucusia</option>
								<option>Discapacidad sensorial - Ceguera</option>
								<option>Discapacidad sensorial - Baja Visión</option>
								<option>Discapacidad sensorial - Sordoceguera</option>
								<option>Discapacidad Intelectual</option>
								<option>Discapacidad Psicosocial</option>
								<option>Discapacidad Multiple</option>
								<option>Discapacidad Física o motora</option>
							</select>
							<label>Grupo Étnico</label>
							<select name="registro[grupo_etnico]">
								<option value="">Selecciona</option>
								<option>Afro-descenndiente</option>
								<option>Indígena</option>
								<option>Racial o palanquero</option>
								<option>ROM o Gitano</option>
								<option>Victima del conflicto armado</option>
								<option>Madre cabeza de familia</option>
							</select> -->
					</div>
					<div class="Con-siguiente">
						<input type="button" class="Siguiente anterior" value="Anterior" name="">
						<input type="submit" class="Siguiente" value="Siguiente" name="">
					</div>
				</div>
			</div>
		</div>
	</form>
	<form class="inscripcion-aspirantes" style="display: none">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.3 Información de Personal. Registré en esta sección los datos de ubicación y contacto del aspirante.</p>
					<div class="Registro-asp-int-sec">
						<label>Dirección*</label>
						<input type="text" placeholder="Dirección" class="mayus requiere" name="registro[aspirante_direccion]" required>
						<label>Ciudad Dirección*</label>
						<input type="text" placeholder="Ciudad Dirección" class="mayus-1 municipio requiere" name="registro[aspirante_ciudad]" required>
						<!-- <label>Localidad</label>
							<input type="text" placeholder="Localidad" name="registro[aspirante_localidad]" > -->
						<!-- <label>Barrio*</label>
							<input type="text" placeholder="Barrio" class="mayus requiere" name="registro[aspirante_barrio]" required>
							<label>Estrato*</label>
							<select name="registro[aspirante_estrato]" class="requiere" required>
								<option value="">Seleccione</option>
								<option value="1">Estrato Uno</option>
								<option value="2">Estrato Dos</option>
								<option value="3">Estrato Tres</option>
								<option value="4">Estrato Cuatro</option>
								<option value="5">Estrato Cinco</option>
								<option value="6">Estrato Seis</option>
							</select> -->
						<label>Celular*</label>
						<input type="text" placeholder="Celular" name="registro[aspirante_celular]" class="requiere" required>
						<label>Teléfono</label>
						<input type="text" placeholder="Teléfono" name="registro[aspirante_telefono]" class="requiere" required>
					</div>
					<div class="Registro-asp-int-sec">
						<label>Correo Electrónico*</label>
						<input type="email" placeholder="Correo Electrónico" class="correo_1 requiere" name="registro[aspirante_correo]" required>
						<label>Confirmar Correo Electrónico*</label>
						<input type="email" placeholder="Confirmar Correo Electrónico" class="correo_2 requiere" name="" required>
					</div>
					<div class="Con-siguiente">
						<input type="button" class="Siguiente anterior" value="Anterior" name="">
						<input type="submit" class="Siguiente val-correo" value="Siguiente" name="">
					</div>
				</div>
			</div>
		</div>
	</form>
	<form class="inscripcion-aspirantes" style="display: none">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.4 Información Académica. Registré en esta sección la información académica requerida en el proceso de admisión.</p>
					<div class="Registro-asp-int-sec">
						<label>Graduado en Colombia*</label>
						<select name="registro[graduado_colombia]" class="requiere" required>
							<option value="S">Si</option>
							<option value="N">No</option>
						</select>
						<label>Año de Graduación*</label>
						<input type="text" placeholder="Año de Graduación" class="ntxt mx-4 requiere" name="registro[ano_graduacion]" required>
						<label>Colegio*</label>
						<input type="text" placeholder="Colegio" class="mayus colegio requiere" name="registro[colegio_graduacion]" required>
						<label>Ciudad*</label>
						<input type="text" placeholder="Ciudad" class="mayus-1 municipio requiere" name="registro[ciudad_graduacion]" required>
						<label>Grado Obtenido*</label>
						<input type="text" placeholder="Grado Obtenido" class="requiere" name="registro[grado_obtenido]" required>
						<label>Tipo Bachiller*</label>
						<select name="registro[bachiller]" class="requiere" required>
							<option value="">Seleccione</option>
							<option value="BA">Bachiller Académico</option>
							<option value="TA">Técnico Agrícola</option>
							<option value="TC">Técnico Comercial</option>
							<option value="TI">Técnico Industrial</option>
							<option value="TR">Técnico Artes</option>
						</select>
						<!-- <label>Tipo Colegio*</label>
							<select name="registro[tipo_colegio]" class="requiere" required>
								<option value="">Seleccione</option>
								<option value="O">OFICIAL</option>
								<option value="P">PRIVADO</option>
							</select> -->
						<!-- <label>Calendario*</label>
							<select name="registro[calendario]" class="requiere" required>
								<option value="">Seleccione</option>
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
							</select> -->
					</div>
					<div class="Con-siguiente">
						<input type="button" class="Siguiente anterior" value="Anterior" name="">
						<input type="submit" class="Siguiente" value="Siguiente" name="">
					</div>
				</div>
			</div>
		</div>
	</form>
	<form class="inscripcion-aspirantes inscripcion-icfes" style="display: none">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.5 Resultados Examen de Estado Saber 11 (ICFES).</p>
					<p>Se tomarán en cuenta los puntajes por prueba: el aspeirante debera haber optenido al menos 50 puntos en cada asignatura.</p>
					<p><strong>Nota.</strong> Quien haya obtenido un puntaje por debajo de 50 en alguan de las aignaturas, podrá; presentarse, adjuntando el informe de calificaciones de grado 11, como un insumo de analisis adicional a tomar en cuenta en su proceso de admisión</p>
					<div class="Registro-icfes">
						<div class="Registro-asp-int-sec">
							<label>Presentado en Colombia*</label>
							<select name="registro[icfes_colombia]" class="requiere" required>
								<option value="S">Si</option>
								<option value="N">No</option>
							</select>
							<label>Número de Registro*</label>
							<input type="text" placeholder="Número de Registro" class="requiere" name="registro[numero_registro]" required>
							<label>Tipo Identificación*</label>
							<select name="registro[tipo_identificacion_icfes]" class="requiere" required>
								<option value="">Seleccione</option>
								<option value="TI">Tarjeta Identidad</option>
								<option value="CC">Cédula de Ciudadanía</option>
								<option value="CE">Cédula Extranjería</option>
								<option value="V">Visa</option>
								<option value="RC">Registro Civil</option>
								<option value="P">Pasaporte</option>
							</select>
							<label>Identificación*</label>
							<input type="text" placeholder="Identificación" class="requiere" name="registro[numero_identificacion_icfes]" required>
							<label>Fecha Presentación*</label>
							<input type="text" placeholder="Fecha Presentación" class="fecha requiere" name="registro[fecha_presentacion_icfes]" required>
							<label>Matemáticas*</label>
							<input type="text" placeholder="Matemáticas" class="ntxt mx-2 requiere" name="registro[puntaje_matematicas]" required>
							<label>Ciencias Sociales y Ciudadanas*</label>
							<input type="text" placeholder="Ciencias Sociales y Ciudadanas" class="ntxt mx-2 requiere" name="registro[puntaje_ciencias_sociales]" required>
						</div>
						<div class="Registro-asp-int-sec">
							<label>Inglés*</label>
							<input type="text" placeholder="Inglés" class="ntxt mx-2 requiere" name="registro[puntaje_ingles]" required>
							<label>Puntaje Lenguaje*</label>
							<input type="text" placeholder="Puntaje Lenguaje" class="ntxt mx-2 requiere" name="registro[puntaje_lenguaje]" required>
							<label>Filosofía*</label>
							<input type="text" placeholder="Filosofía" class="ntxt mx-2 requiere" name="registro[puntaje_filosofia]" required>
							<label>Biología*</label>
							<input type="text" placeholder="Biología" class="ntxt mx-2 requiere" name="registro[puntaje_biologia]" required>
							<label>Química*</label>
							<input type="text" placeholder="Química" class="ntxt mx-2 requiere" name="registro[puntaje_quimica]" required>
							<label>Física*</label>
							<input type="text" placeholder="Física" class="ntxt mx-2 requiere" name="registro[puntaje_fisica]" required>
							<label>Componente Flexible o Electiva*</label>
							<input type="text" placeholder="Componente Flexible o Electiva" class="ntxt mx-2 requiere" name="registro[puntaje_electiva]" required>
						</div>
					</div>
					<div class="Con-siguiente">
						<input type="button" class="Siguiente anterior" value="Anterior" name="">
						<input type="submit" class="Siguiente" value="Siguiente" name="">
					</div>
				</div>
			</div>
		</div>
	</form>
	<form class="inscripcion-aspirantes" style="display: none">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.6 Información de Responsable o Acudeintes. Registre en esta sección los datos personales y de contacto de su responsable o acudiente.</p>
					<div class="Registro-asp-int-sec">
						<label>Parentesco*</label>
						<select name="registro[responsable_parentesco]" class="requiere" required>
							<option value="">Seleccione</option>
							<option value="AA">Acudiente Amigo</option>
							<option value="AO">Acudiente Otro</option>
							<option value="C">Conyugue</option>
							<option value="H">Hermano (a)</option>
							<option value="M">Madre</option>
							<option value="P">Padre</option>
						</select>
						<label>Nombres*</label>
						<input type="text" placeholder="Nombres" class="mayus requiere" name="registro[responsable_nombres]" required>
						<label>Apellidos*</label>
						<input type="text" placeholder="Apellidos" class="mayus requiere" name="registro[responsable_apellidos]" required>
						<label>Telefono*</label>
						<input type="text" placeholder="Telefono" class="requiere" name="registro[responsable_telefono]" required>
						<label>Celular*</label>
						<input type="text" placeholder="Celular" class="requiere" name="registro[responsable_celular]" required>
						<label>Email*</label>
						<input type="text" placeholder="Email" class="requiere" name="registro[responsable_correo]" required>
					</div>
					<div class="Con-siguiente">
						<input type="button" class="Siguiente anterior" value="Anterior" name="">
						<input type="submit" class="Siguiente" value="Siguiente" name="">
					</div>
				</div>
			</div>
		</div>
	</form>
	<form class="inscripcion-aspirantes inscripcion-final" style="display: none">
		<div class="Registro-asp">
			<div class="Registro-asp-int">
				<div class="Registro-asp-glob">
					<h2>Formulario de inscripción</h2>
					<p>1.7 Información Adicional. Registré en esta sección la información correspondiente a las fuentes de financiación de sus estudios y los medios de comunicación por medio de los cuales se enteró del programa.</p>
					<div class="Registro-asp-int-sec">
						<label>Fuente Financiación*</label>
						<select name="registro[financiacion]" class="requiere" required>
							<option value="">Seleccione</option>
							<option value="1">Ud. Mismo Semestre</option>
							<option value="2">Sus Padres</option>
							<option value="3">Otro Familiar</option>
							<option value="4">Cred. FUKL</option>
							<option value="5">Cred. ICETEX</option>
							<option value="6">Entidad Financiera</option>
							<option value="7">Apoyo de la Empresa</option>
							<option value="8">Otra</option>
						</select>
						<label>Medio de Información*</label>
						<select name="registro[medio_informacion]" class="requiere" required>
							<option value="">Seleccione</option>
							<option>Convenio</option>
							<option>Fachada</option>
							<option>Guía de Universidades de La Nota Económica</option>
							<option>Guia Estudiantil de El Espectador</option>
							<option>Llegó directo a nuestra Web</option>
							<option>MSN - Hotmail</option>
							<option>Páginas Amarillas</option>
							<option>Revista 'Vía U' de El Tiempo</option>
							<option>Ser Egresado de la Konrad</option>
							<option>Un Amigo o Allegado (No de la Konrad)</option>
							<option>Un anuncio en ADN</option>
							<option>Un anuncio en El Tiempo</option>
							<option>Un anuncio en Transmilenio</option>
							<option>Un anuncio en un Paradero de Bus</option>
							<option>Un anuncio publicitario en Facebook</option>
							<option>Un anuncio publicitario en Google</option>
							<option>Un anuncio publicitario en TV</option>
							<option>Un contenido de Facebook</option>
							<option>Un E-mail de la Universidad</option>
							<option>Un Egresado de la Konrad</option>
							<option>Un empleado o profesor de la Konrad</option>
							<option>Un Estudiante de la Universidad</option>
							<option>Un profesor u orientador del Colegio</option>
							<option>Una búsqueda en Google</option>
							<option>Una charla o visita a su Colegio</option>
							<option>Una conferencia en la Universidad</option>
							<option>Una Feria de Universidades</option>
							<option>Web del Ministerio</option>
						</select>
						<label>Otro Medio</label>
						<input type="text" name="registro[otro_medio]">
					</div>
					<div class="Con-siguiente">
						<input type="button" class="Siguiente anterior" value="Anterior" name="">
						<input type="submit" class="Siguiente" value="Guardar" name="">
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="Registro-asp">
		<div class="Registro-asp-int">
			<!-- <div class="Nav-admin">
					<section class="nav_tabs">
						<div class="nav_tabs_back">
							<nav>
								<ul class="tabs">
									<li class="activa"><a href="#" class="">1</a></li>
									<li class=""><a href="#" class="">2</a></li>
									<li class=""><a href="#" class="">3</a></li>
									<li class=""><a href="#" class="">4</a></li>
									<li class=""><a href="#" class="">5</a></li>
									<li class=""><a href="#" class="">6</a></li>
									<li class=""><a href="#" class="">7</a></li>
									<li class=""><a href="#" class="">8</a></li>
								</ul>
							</nav>
						</div>
					</section>
				</div> -->
		</div>
	</div>
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script src="js/flatpickr/dist/flatpickr.min.js" type="text/javascript"></script>
	<script src="js/flatpickr/dist/l10n/es.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/registro-aspirantes.js"></script>
</body>
<footer>
	<div class="Footer">
		<div class="Footer-int">
			<p>Copyright © Gricompany S.A.S, 2018. Todos los derechos reservados diseñado por <a href="https://www.inngeniate.com/">Inngeniate.com</a></p>
		</div>
	</div>
</footer>

</html>
