<?php
require "libs/conexion.php";
$registro = $_GET['registro'];

$bus = $db
    ->where('Id', $registro)
    ->objectBuilder()->get('registros');

$res = $bus[0];

if ($res->foto_ct != '') {
    $nombre      = $res->nombres . ' ' . $res->apellidos;
    $documento   = $res->numero_ident;
    $expedicion  = $res->fecha_inicio;
    $expedicion  = date_create($expedicion);
    $expedicion  = date_format($expedicion, 'd-m-Y');
    $vencimiento = $res->vencimiento_ct;
    $vencimiento = date_create($vencimiento);
    $vencimiento = date_format($vencimiento, 'd-m-Y');
    $rh          = $res->rh;
    $licencia    = $res->licencia;
    $formacion   = $res->certificado;

    $horas = $res->horas;

    require_once 'libs/tcpdf.php';
    require_once 'libs/fpdi/fpdi.php';

    $exa = new TCPDF();
    $exa->SetPrintHeader(false);
    // $exa->setPageOrientation('l');
    $exa->addFont('conthrax', '', 'conthrax.php');
    $exa->addFont('ubuntucondensed', '', 'ubuntucondensed.php');
    $exa->SetFont('conthrax', '', 8);
    // $exa->SetFont('arial', '', 6);
    $estilo = '<style>
                .nm{
                    color: #9e090f;
                    font-size: 6.5
                }
                .bl{
                    color: #fff;
                    font-family: arial;
                    font-size: 6
                }
                .bl2{
                    color: #fff;
                    font-family: arial;
                    font-size: 6;
                }
                .bl3{
                    color: #fff;
                    font-family: arial;
                    font-size: 7;
                }
                .bl4{
                    color: #000;
                    font-family: arial;
                    font-size: 7;
                }
                .bl5{
                    color: #000;
                    font-family: ubuntucondensed;
                    font-size: 8;
                }
                .bl6{
                    color: #fff;
                    font-family: arial;
                    font-size: 6;
                }
            </style>';

    // $exa->setImageScale(1.42);
    //$exa->setpageUnit('cm');
    $exa->AddPage();
    $exa->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $exa->setJPEGQuality(100);

    //girar imagen fondo
    // $exa->SetXY(20, 73);
    $exa->StartTransform();
    $exa->Rotate(90);

    $exa->Image("images/carnetfondo.jpg", -30, 30, '', '', 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
    $exa->StopTransform();
    ///

    /// girar el contenido
    $exa->SetXY(46, 40);
    $exa->StartTransform();
    $exa->Rotate(90);
    /////

    $txt = '<table border="1" width="230px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl2">&nbsp;&nbsp;&nbsp;Resolución N° 1500-56.03 1795 de la Secretaria de<br>&nbsp;&nbsp;&nbsp;Educación</strong></td></tr><tr><td align="center"><strong class="nm">' . $nombre . '</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Documento: ' . $documento . '</strong></td></tr></table>';

    $exa->SetXY(13.5, 75);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="40%" cellpadding="0" cellspacing="0"><tr><td><strong class="bl">&nbsp;&nbsp;Expedición:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vencimiento:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RH:</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;' . $expedicion . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $vencimiento . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $rh . '</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duración:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Licencia:</strong></td></tr><tr><td><strong class="bl">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $horas . ' Hrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $licencia . '</strong></td></tr></table>';

    $exa->SetXY(13.5, 91);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" width="230px" cellpadding="0" cellspacing="0"><tr><td><strong class="bl2">&nbsp;&nbsp;&nbsp;Lic. seguridad y salud en el trabajo Res. 3753 de 2013</strong></td></tr></table>';

    $exa->SetXY(13.5, 104.5);
    $exa->WriteHTML($estilo . $txt);

    $txt = '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="50"><strong class="bl6">&nbsp;&nbsp;&nbsp;Formación:</strong></td><td width="190"><strong class="bl6"> ' . $formacion . '</strong></td></tr></table>';

    $exa->SetXY(13.5, 107);
    $exa->WriteHTML($estilo . $txt);

    $exa->Image(substr($res->foto_ct, 3), 32, 42, 23);

    $exa->StopTransform();

    $exa->setImageScale(1.42);
    $exa->AddPage();
    $exa->setJPEGQuality(100);

    //girar imagen fondo
    $exa->SetXY(20, 73);
    $exa->StartTransform();
    $exa->Rotate(90);
    $exa->Image("images/carnetfondo2.jpg");
    $exa->StopTransform();
    ///

    $exa->SetXY(25, 68);
    $exa->StartTransform();
    $exa->Rotate(90);

    $txt = '<table border="0" width="40%" cellpadding="-1" cellspacing="0"><tr><td><strong class="bl3">Este carnet certifica única y exclusivamente la</strong></td></tr><tr><td><strong class="bl3">actividad ejercida por el titular</strong></td></tr></table>';

    $exa->WriteHTML($estilo . $txt);
    $exa->StopTransform();

    $exa->SetXY(67, 75);
    $exa->StartTransform();
    $exa->Rotate(90);

    $txt = '<table border="0" width="40%" cellpadding="-2" cellspacing="0"><tr><td align="right"><strong class="bl4">En caso de perdida informar</strong></td></tr><tr><td align="right"><strong class="bl4">inmediatamente a la empresa</strong></td></tr></table>';

    $exa->WriteHTML($estilo . $txt);
    $exa->StopTransform();

    $exa->SetXY(87, 73);
    $exa->StartTransform();
    $exa->Rotate(90);

    $txt = '<table border="0" width="50%" cellpadding="0" cellspacing="0"><tr><td align="center"><i class="bl5">Representante Legal</i></td></tr><tr><td align="center"><strong class="bl5">Gestión del Riesgo Integral Company sas</strong></td></tr></table>';

    $exa->WriteHTML($estilo . $txt);
    $exa->StopTransform();

    $exa->SetXY(107, 67);
    $exa->StartTransform();
    $exa->Rotate(90);

    $txt = '<table border="0" width="50%" cellpadding="0" cellspacing="0"><tr><td align="center"><strong class="bl5" style="font-size:10">www.gricompany.co</strong></td></tr><tr><td align="center"><strong class="bl5" style="font-size:6.5">Cel: 314 325 7703 - 314 211 4658</strong></td></tr></table>';

    $exa->WriteHTML($estilo . $txt);
    $exa->StopTransform();

    $exa->Output('carnet.pdf', 'I');
} else {
    echo 'Error al generar el carnet: Falta foto en el registro.';
}
